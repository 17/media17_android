##---------------Begin: proguard configuration for Fabric Crashlytics  ----------
-keepattributes SourceFile, LineNumberTable
##---------------End: proguard configuration for Fabric Crashlytics  ----------

##---------------Begin: proguard configuration for Flurry  ----------
-keep class com.flurry.** { *; }
-dontwarn com.flurry.**
-keepattributes *Annotation*,EnclosingMethod,Signature
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

# Google Play Services library
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *

-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

#If you are using the Google Mobile Ads SDK, add the following:
# Preserve GMS ads classes
-keep class com.google.android.gms.ads.** { *;
}
-dontwarn com.google.android.gms.ads.**


#If you are using the InMobi SDK, add the following:
# Preserve InMobi Ads classes
-keep class com.inmobi.** { *;
}
-dontwarn com.inmobi.**
#If you are using the Millennial Media SDK, add the following:
# Preserve Millennial Ads classes
-keep class com.millennialmedia.** { *;
}
-dontwarn com.millennialmedia.**
##---------------End: proguard configuration for Flurry  ----------

## Important: avoid to obfuscate data model ##
-keep class com.machipopo.media17.model.** { *; }

-keep class twitter4j.** { *; }
-dontwarn twitter4j.**

-keep class okio.** { *; }
-dontwarn okio.**

-keep class com.baidu.android.pushservice.** { *;}
-dontwarn com.baidu.android.pushservice.**

-keep class com.baidu.frontia.** { *;}
-dontwarn com.baidu.frontia.**

-keep class com.facebook.ads.internal.** { *;}
-dontwarn com.facebook.ads.internal.**

-keep class com.helpshift.** { *;}
-dontwarn com.helpshift.**

-keep class com.umeng.analytics.** { *;}
-dontwarn com.umeng.analytics.**

-keep class org.apache.** { *;}
-dontwarn org.apache.**

-keep class com.payeco.android.plugin.** { *; }
-dontwarn com.payeco.android.plugin.**

-keep class com.unionpay.mobile.android.** { *; }
-dontwarn com.unionpay.mobile.android.**

-keep class com.alipay.** { *; }
-dontwarn com.alipay.**

-keep class com.iapppay.** { *; }
-dontwarn com.iapppay.**

-keep class com.tencent.mm.sdk.** { *; }

-keep class com.tencent.** { *; }
-dontwarn com.tencent.**

-keep class com.baidu.** { *; }
-dontwarn com.baidu.**
