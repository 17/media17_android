#include <jni.h>
#include <jni.h>
#include <android/log.h>

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <android/log.h>

#define QUOTE(...) #__VA_ARGS__

JNIEXPORT jstring JNICALL Java_com_machipopo_media_ShaderLoader_getVertexShader(JNIEnv *env, jobject obj)
{
    const char* shaderString = QUOTE(
            precision highp float;

            attribute vec4 position;
            attribute vec4 inputTextureCoordinate;

            uniform mat4 rotationMatrix;
//            uniform float pixelStepSize;

            varying vec2 textureCoordinate;
            varying vec2 sharpCoordinates[4];
            varying vec2 tap9BlurCoordinates[9];

            void main()
            {
                float pixelStepSize = 1.0 / 1080.0;

                gl_Position = position * rotationMatrix;
                textureCoordinate = inputTextureCoordinate.xy;

                sharpCoordinates[0] = inputTextureCoordinate.xy + vec2(0.0, -pixelStepSize);
                sharpCoordinates[1] = inputTextureCoordinate.xy + vec2(-pixelStepSize, 0.0);
                sharpCoordinates[2] = inputTextureCoordinate.xy + vec2(0.0, pixelStepSize);
                sharpCoordinates[3] = inputTextureCoordinate.xy + vec2(pixelStepSize, 0.0);

                tap9BlurCoordinates[0] = inputTextureCoordinate.xy;
                tap9BlurCoordinates[1] = inputTextureCoordinate.xy + vec2(0.0, -pixelStepSize) * 1.3846153846 * 4.0;
                tap9BlurCoordinates[2] = inputTextureCoordinate.xy + vec2(-pixelStepSize, 0.0) * 1.3846153846 * 4.0;
                tap9BlurCoordinates[3] = inputTextureCoordinate.xy + vec2(0.0, pixelStepSize) * 1.3846153846 * 4.0;
                tap9BlurCoordinates[4] = inputTextureCoordinate.xy + vec2(pixelStepSize, 0.0) * 1.3846153846 * 4.0;
                tap9BlurCoordinates[5] = inputTextureCoordinate.xy + vec2(0.0, -pixelStepSize) * 3.2307692308 * 4.0;
                tap9BlurCoordinates[6] = inputTextureCoordinate.xy + vec2(-pixelStepSize, 0.0) * 3.2307692308 * 4.0;
                tap9BlurCoordinates[7] = inputTextureCoordinate.xy + vec2(0.0, pixelStepSize) * 3.2307692308 * 4.0;
                tap9BlurCoordinates[8] = inputTextureCoordinate.xy + vec2(pixelStepSize, 0.0) * 3.2307692308 * 4.0;
            }
    );

    return (*env)->NewStringUTF(env, shaderString);
}

JNIEXPORT jstring JNICALL Java_com_machipopo_media_ShaderLoader_getFragmentShader(JNIEnv *env, jobject obj)
{
    const char* shaderString = QUOTE(
            precision highp float;

            varying highp vec2 textureCoordinate;
            varying vec2 sharpCoordinates[4];
            varying vec2 tap9BlurCoordinates[9];

            uniform lowp float imageWidth;
            uniform lowp float imageHeight;

            uniform lowp int imageSourceInputType; // 0: RGBA, 1: Y + U + V, 2: Y + UV (NV21)
            uniform lowp int renderOutputType; // 0: RGBA, 1: YUV

            uniform lowp float pixelStepSize;

            uniform lowp int colorMap1Enabled;
            uniform lowp int overlay1Enabled;
            uniform lowp int overlay2Enabled;

            uniform sampler2D inputImageTexture; // mandatory

            uniform sampler2D sharedColorMapTexture; // mandatory => this texture contains 20 1X256 color maps, for highlights, shadows color transform

            uniform sampler2D colorMapTexture1; // optional
            uniform sampler2D overlayTexture1; // optional
            uniform sampler2D overlayTexture2; // optional

            uniform lowp float filterMixPercentage; // mandatory

            // tools
            uniform lowp float brightness; // -100 ~ 0 ~ 100, default 0
            uniform lowp float contrast; // -100 ~ 0 ~ 100, default 0
            uniform lowp float unsharpMask; // 0 ~ 100, default 0
            uniform lowp float skinSmooth; // 0 ~ 100, default 0
            uniform lowp float warmth; // -100 ~ 0 ~ 100, default 0
            uniform lowp float saturation; // -100 ~ 0 ~ 100, default 0
            uniform lowp float fade; // 0 ~ 100, default 0
            uniform lowp float shadows; // -100 ~ 0 ~ 100, default 0
            uniform lowp float highlights; // -100 ~ 0 ~ 100, default 0
            uniform lowp float sharpen; // 0 ~ 100, default 0
            uniform lowp float vignette; // 0 ~ 100, default 0
            uniform lowp float autoEnhance; // 0 ~ 100, default 0

            // color map: 0.0 means no color map, 1.0~8.0: highlight colors, 9.0~16.0 shadow colors, 17.0: highlight minus, 18.0: highlight plus, 19.0: shadow minus, 20.0: shadow highlight
            uniform lowp float shadowsColorMapIndex;
            uniform lowp float highlightsColorMapIndex;
            uniform lowp float colorShadows; // 0 ~ 100, default 0
            uniform lowp float colorHighlights; // 0 ~ 100, default 0

            // tilt shift
            uniform lowp float tiltShiftMode;
            uniform lowp float tiltShiftRadialCenterX;
            uniform lowp float tiltShiftRadialCenterY;
            uniform lowp float tiltShiftRadialRadius;
            uniform lowp float tiltShiftLinearCenterX;
            uniform lowp float tiltShiftLinearCenterY;
            uniform lowp float tiltShiftLinearRadius;
            uniform lowp float tiltShiftLinearAngle; // in radians: 0 as horizontal


            const mat3 yuv2rgbMatrix = mat3( 1,       1,         1,
                                             0,       -0.39465,  2.03211,
                                             1.13983, -0.58060,  0);

            const mat3 rgb2yuvMatrix = mat3( 0.2990020091240603,       -0.14713869284835024,         0.6150022291709639,
                                             0.5869987255595551,       -0.28886168837294957,  -0.5149879592215989,
                                             0.11399926531638466, 0.4360003812212998,  -0.10001426994936496);

            // overlay
            float overlayCal(float a, float b){
                if(a<.5)
                    return 2.*a*b;
                else
                    return 1.-2.*(1.-a)*(1.-b);

                return 0.;
            }

            // softLight
            float softLightCal(float a, float b){
                if(b<.5)
                    return 2.*a*b+a*a*(1.-2.*b);
                else
                    return 2.*a*(1.-b)+sqrt(a)*(2.*b-1.);

                return 0.;
            }

            // soft overlay
            vec3 softOverlayBlend(vec3 a, float mag) {
                return pow(a, vec3(1.0 / (1.0 - mag)));
            }

            // hsv rgb color space transformation
            vec3 rgb2hsv(vec3 c)
            {
                vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
                vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

                /*
                    vec4 p;
                    if(c.g < c.b)
                      p = vec4(c.bg, K.wz);
                    else
                      p = vec4(c.gb, K.xy);

                    vec4 q;
                    if(c.r < p.x)
                      q = vec4(p.xyw, c.r);
                    else
                      q = vec4(c.r, p.yzx);
                */
                float d = q.x - min(q.w, q.y);
                float e = 1.0e-10;
                return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
            }

            vec3 hsv2rgb(vec3 c)
            {
                vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
                vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
                return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
            }

            float getLuma(vec3 inputColor) {
                return  (0.299 * inputColor.r) +
                        (0.587 * inputColor.g) +
                        (0.114 * inputColor.b);
            }

            vec3 rgbToYuv(vec3 inP) {
                vec3 outP;
                outP.r = getLuma(inP);
                outP.g = (1.0/1.772)*(inP.b - outP.r);
                outP.b = (1.0/1.402)*(inP.r - outP.r);
                return outP;
            }

            vec3 yuvToRgb(vec3 inP) {
                float y = inP.r;
                float u = inP.g;
                float v = inP.b;
                vec3 outP;
                outP.r = 1.402 * v + y;
                outP.g = (y - (0.299 * 1.402 / 0.587) * v -
                          (0.114 * 1.772 / 0.587) * u);
                outP.b = 1.772 * u + y;
                return outP;
            }

            float easeInOutSigmoid(float value, float strength) {
                float t = 1.0 / (1.0 - strength);
                if (value > 0.5) {
                    return 1.0 - pow(2.0 - 2.0 * value, t) * 0.5;
                } else {
                    return pow(2.0 * value, t) * 0.5;
                }
            }

            vec3 bowRgbChannels(vec3 inVal, float mag) {

                vec3 outVal;
                float power = 1.0 + abs(mag);

                if (mag < 0.0) {
                    power = 1.0 / power;
                }

                outVal.r = 1.0 - pow((1.0 - inVal.r), power);
                outVal.g = 1.0 - pow((1.0 - inVal.g), power);
                outVal.b = 1.0 - pow((1.0 - inVal.b), power);

                return outVal;
            }

            vec3 fadeFunc(vec3 color) {
                vec3 co1 = vec3(-0.9772);
                vec3 co2 = vec3(1.708);
                vec3 co3 = vec3(-0.1603);
                vec3 co4 = vec3(0.2878);

                vec3 comp1 = co1 * pow(color, vec3(3.0));
                vec3 comp2 = co2 * pow(color, vec3(2.0));
                vec3 comp3 = co3 * color;
                vec3 comp4 = co4;

                return comp1 + comp2 + comp3 + comp4;
            }

            vec3 adjustTemperature(float tempDelta, vec3 inRgb) {
                vec3 yuvVec;

                if (tempDelta > 0.0 ) {
                    // "warm" midtone change
                    yuvVec =  vec3(0.1765, -0.1255, 0.0902);
                } else {
                    // "cool" midtone change
                    yuvVec = -vec3(0.0588,  0.1569, -0.1255);
                }

                vec3 yuvColor = rgbToYuv(inRgb);

                float luma = yuvColor.r;

                float curveScale = sin(luma * 3.14159); // a hump

                yuvColor += 0.375 * tempDelta * curveScale * yuvVec;
                inRgb = yuvToRgb(yuvColor);
                return inRgb;
            }
  float computeBilateralWeight(float originalWeight, vec3 centerColor, vec3 targetColor, float strength) {
      float colorDiff = length(centerColor-targetColor) / 1.73205080757; // divide by sqrt(3.0) to normalize
      float threshold = strength / 20.0;

      if(colorDiff>threshold) {
          return 0.0;
      }

      return originalWeight * (1.0 - colorDiff) * strength * 2.0;
  }

            vec3 applyColorMap(vec3 inputTexture, sampler2D colorMap) {
                float size = 33.0;

                float sliceSize = 1.0 / size;
                float slicePixelSize = sliceSize / size;
                float sliceInnerSize = slicePixelSize * (size - 1.0);
                float xOffset = 0.5 * sliceSize + inputTexture.x * (1.0 - sliceSize);
                float yOffset = 0.5 * slicePixelSize + inputTexture.y * sliceInnerSize;
                float zOffset = inputTexture.z * (size - 1.0);
                float zSlice0 = floor(zOffset);
                float zSlice1 = zSlice0 + 1.0;
                float s0 = yOffset + (zSlice0 * sliceSize);
                float s1 = yOffset + (zSlice1 * sliceSize);
                vec4 sliceColor0 = texture2D(colorMap, vec2(xOffset, s0));
                vec4 sliceColor1 = texture2D(colorMap, vec2(xOffset, s1));

                return mix(sliceColor0, sliceColor1, zOffset - zSlice0).rgb;
            }

            void main()
            {
                vec3 output_result = vec3(0.0);
                vec4 gaussianResult = texture2D(inputImageTexture, tap9BlurCoordinates[0]) * 0.2270270270;
                                    gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[1]) * 0.1581081081;
                                    gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[2]) * 0.1581081081;
                                    gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[3]) * 0.1581081081;
                                    gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[4]) * 0.1581081081;
                                    gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[5]) * 0.03513513515;
                                    gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[6]) * 0.03513513515;
                                    gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[7]) * 0.03513513515;
                                    gaussianResult += texture2D(inputImageTexture, tap9BlurCoordinates[8]) * 0.03513513515;

                output_result = texture2D(inputImageTexture, textureCoordinate).rgb;

                /* tool part */
                // sharpen
                if(sharpen!=0.0) {
                    /* SHARPEN KERNEL
                     0 -1  0
                     -1  5 -1
                     0 -1  0
                     */

                    vec3 sharpen_result = output_result.rgb * 5.0;

                    sharpen_result -= texture2D(inputImageTexture, sharpCoordinates[0]).rgb;
                    sharpen_result -= texture2D(inputImageTexture, sharpCoordinates[1]).rgb;
                    sharpen_result -= texture2D(inputImageTexture, sharpCoordinates[2]).rgb;
                    sharpen_result -= texture2D(inputImageTexture, sharpCoordinates[3]).rgb;

                    output_result = mix(output_result, sharpen_result, sharpen/100.0);

                    output_result = clamp(output_result, 0.0, 1.0);
                }


                    // tilt shift
                    if(tiltShiftMode>0.0)
                    {
                      if(tiltShiftMode==1.0) { // radial blur: determine radius
                          float dist = length(textureCoordinate-vec2(tiltShiftRadialCenterX, tiltShiftRadialCenterY));
                          float normalizedDist = dist/tiltShiftRadialRadius;


                          if(normalizedDist<0.5) {

                          } else if(normalizedDist<0.75) { // original -> blur 2: 0.5 ~ 0.75
                              output_result = mix(output_result, gaussianResult.rgb, (normalizedDist-0.75)/0.25);
                          } else if(normalizedDist<1.25) { // blur 3 -> blur 4: 1.0 ~ 1.25
                              output_result = mix(output_result, gaussianResult.rgb, (normalizedDist-1.0)/0.25);
                          } else { // blur 4
                              output_result = gaussianResult.rgb;
                          }
                      } else if(tiltShiftMode==2.0) { // linear blur: determine radius
                                  float dist = abs(dot(textureCoordinate-vec2(tiltShiftLinearCenterX, tiltShiftLinearCenterY), vec2(sin(tiltShiftLinearAngle), cos(tiltShiftLinearAngle))));
                                  float normalizedDist = dist/tiltShiftLinearRadius;

                                  if(normalizedDist<0.5) {

                                  } else if(normalizedDist<0.75) { // original -> blur 2: 0.5 ~ 0.75
                                      output_result = mix(output_result, gaussianResult.rgb, (normalizedDist-0.75)/0.25);
                                  } else if(normalizedDist<1.25) { // blur 3 -> blur 4: 1.0 ~ 1.25
                                      output_result = mix(output_result, gaussianResult.rgb, (normalizedDist-1.0)/0.25);
                                  } else { // blur 4
                                      output_result = gaussianResult.rgb;
                                  }
                            }
                        output_result = clamp(output_result, 0.0, 1.0);
                      }


                // skin smoothing: bilateral filtering
             if(skinSmooth!=0.0) {
             float strength = skinSmooth/100.0;

              vec3 bilateralResult = vec3(0.0);

                           vec3 centerColor = texture2D(inputImageTexture, textureCoordinate).rgb;
                           vec3 targetColor = vec3(0.0);
                           float weight = 0.0;
                           float weightSum = 0.0;

                           bilateralResult += texture2D(inputImageTexture, textureCoordinate).rgb * 0.2270270270;
                           weightSum += 0.2270270270;

//                           targetColor = texture2D(inputImageTexture, tap9BlurCoordinates[1]).rgb;
//                           weight = computeBilateralWeight(0.1581081081, centerColor, targetColor, strength);
//                           bilateralResult += targetColor * weight;
//                           weightSum += weight;
//
//                           targetColor = texture2D(inputImageTexture, tap9BlurCoordinates[2]).rgb;
//                           weight = computeBilateralWeight(0.1581081081, centerColor, targetColor, strength);
//                           bilateralResult += targetColor * weight;
//                           weightSum += weight;
//
//                           targetColor = texture2D(inputImageTexture, tap9BlurCoordinates[3]).rgb;
//                           weight = computeBilateralWeight(0.1581081081, centerColor, targetColor, strength);
//                           bilateralResult += targetColor * weight;
//                           weightSum += weight;
//
//                           targetColor = texture2D(inputImageTexture, tap9BlurCoordinates[4]).rgb;
//                           weight = computeBilateralWeight(0.1581081081, centerColor, targetColor, strength);
//                           bilateralResult += targetColor * weight;
//                           weightSum += weight;

                           targetColor = texture2D(inputImageTexture, tap9BlurCoordinates[5]).rgb;
                           weight = computeBilateralWeight(0.03513513515, centerColor, targetColor, strength);
                           bilateralResult += targetColor * weight;
                           weightSum += weight;

                           targetColor = texture2D(inputImageTexture, tap9BlurCoordinates[6]).rgb;
                           weight = computeBilateralWeight(0.03513513515, centerColor, targetColor, strength);
                           bilateralResult += targetColor * weight;
                           weightSum += weight;

                           targetColor = texture2D(inputImageTexture, tap9BlurCoordinates[7]).rgb;
                           weight = computeBilateralWeight(0.03513513515, centerColor, targetColor, strength);
                           bilateralResult += targetColor * weight;
                           weightSum += weight;

                           targetColor = texture2D(inputImageTexture, tap9BlurCoordinates[8]).rgb;
                           weight = computeBilateralWeight(0.03513513515, centerColor, targetColor, strength);
                           bilateralResult += targetColor * weight;
                           weightSum += weight;

                output_result = bilateralResult/weightSum;
                output_result = clamp(output_result, 0.0, 1.0);
             }

                // unsharp mask
                if(unsharpMask!=0.0) {

//                        vec3 gaussinan_blur_result = gaussianResult.rgb;
                          output_result = output_result*(1.0+unsharpMask/100.0) - gaussianResult.rgb*(unsharpMask/100.0);
                          output_result = clamp(output_result, 0.0, 1.0);
                }

                // auto enhance
                if(autoEnhance!=0.0) {
                    output_result = output_result*(1.0+autoEnhance/100.0) - gaussianResult.rgb*(autoEnhance/100.0);
                    output_result = clamp(output_result, 0.0, 1.0);
                }

                // fade
                if (fade!=0.0) {
                    vec3 faded = fadeFunc(output_result);
                    output_result = (output_result * (1.0 - fade/100.0)) + (faded * fade/100.0);

                    output_result = clamp(output_result, 0.0, 1.0);
                }

                // highlights and shadows
                if(highlights>0.0) {
                    vec3 highlight_result = vec3(
                            texture2D(sharedColorMapTexture, vec2(output_result.r, 0.875)).r,
                            texture2D(sharedColorMapTexture, vec2(output_result.g, 0.875)).g,
                            texture2D(sharedColorMapTexture, vec2(output_result.b, 0.875)).b);
                    output_result = mix(output_result, highlight_result, highlights/100.0);

                    output_result = clamp(output_result, 0.0, 1.0);
                } else if(highlights<0.0) {
                    vec3 highlight_result = vec3(
                            texture2D(sharedColorMapTexture, vec2(output_result.r, 0.825)).r,
                            texture2D(sharedColorMapTexture, vec2(output_result.g, 0.825)).g,
                            texture2D(sharedColorMapTexture, vec2(output_result.b, 0.825)).b);

                    output_result = mix(output_result, highlight_result, -highlights/100.0);

                    output_result = clamp(output_result, 0.0, 1.0);
                }

                if(shadows>0.0) {
                    vec3 shadow_result = vec3(
                            texture2D(sharedColorMapTexture, vec2(output_result.r, 0.975)).r,
                            texture2D(sharedColorMapTexture, vec2(output_result.g, 0.975)).g,
                            texture2D(sharedColorMapTexture, vec2(output_result.b, 0.975)).b);
                    output_result = mix(output_result, shadow_result, shadows/100.0);

                    output_result = clamp(output_result, 0.0, 1.0);
                } else if(shadows<=0.0) {
                    vec3 shadow_result = vec3(
                            texture2D(sharedColorMapTexture, vec2(output_result.r, 0.925)).r,
                            texture2D(sharedColorMapTexture, vec2(output_result.g, 0.925)).g,
                            texture2D(sharedColorMapTexture, vec2(output_result.b, 0.925)).b);
                    output_result = mix(output_result, shadow_result, -shadows/100.0);

                    output_result = clamp(output_result, 0.0, 1.0);
                }

                // saturation
                if(saturation!=0.0) {
                    vec3 hsv = rgb2hsv(output_result);
                    float saturationFactor = 1.0 + saturation/100.0;
                    hsv.y = hsv.y * saturationFactor;
                    hsv.y = clamp(hsv.y, 0.0, 1.0);
                    output_result = hsv2rgb(hsv);

                    output_result = clamp(output_result, 0.0, 1.0);
                }

                // contrast
                if(contrast!=0.0) {
                    output_result = clamp(output_result, 0.0, 1.0);

                    float strength = contrast/100.0 * 0.5; // adjust range to useful values

                    vec3 yuv = rgbToYuv(output_result.rgb);
                    yuv.x = easeInOutSigmoid(yuv.x, strength);
                    yuv.y = easeInOutSigmoid(yuv.y + 0.5, strength * 0.65) - 0.5;
                    yuv.z = easeInOutSigmoid(yuv.z + 0.5, strength * 0.65) - 0.5;
                    output_result.rgb = yuvToRgb(yuv);

                    output_result = clamp(output_result, 0.0, 1.0);
                }


                // brightness
                if(brightness!=0.0) {
                    output_result.rgb = clamp(output_result.rgb, 0.0, 1.0);
                    output_result.rgb = bowRgbChannels(output_result.rgb, brightness/100.0 * 1.1);

                    output_result = clamp(output_result, 0.0, 1.0);
                }
                // warmth
                if(warmth!=0.0) {
                    output_result.rgb = adjustTemperature(warmth/100.0, output_result.rgb);
                    output_result = clamp(output_result, 0.0, 1.0);
                }

                // color map shadows and highlights
                if(shadowsColorMapIndex>0.0) {
                    float colorMapPosY = shadowsColorMapIndex/20.0-0.025;

                    vec3 color_map_result = vec3(
                            texture2D(sharedColorMapTexture, vec2(output_result.r, colorMapPosY)).r,
                            texture2D(sharedColorMapTexture, vec2(output_result.g, colorMapPosY)).g,
                            texture2D(sharedColorMapTexture, vec2(output_result.b, colorMapPosY)).b);
                    output_result = mix(output_result, color_map_result, colorShadows/100.0);

                    output_result = clamp(output_result, 0.0, 1.0);
                }

                if(highlightsColorMapIndex>0.0) {
                    float colorMapPosY = highlightsColorMapIndex/20.0-0.025;

                    vec3 color_map_result = vec3(
                            texture2D(sharedColorMapTexture, vec2(output_result.r, colorMapPosY)).r,
                            texture2D(sharedColorMapTexture, vec2(output_result.g, colorMapPosY)).g,
                            texture2D(sharedColorMapTexture, vec2(output_result.b, colorMapPosY)).b);
                    output_result = mix(output_result, color_map_result, colorHighlights/100.0);

                    output_result = clamp(output_result, 0.0, 1.0);
                }


                // vignette
                if(vignette!=0.0) {
                    const float midpoint = 0.7;
                    const float fuzziness = 0.62;

                    float radDist = length(textureCoordinate - 0.5) / sqrt(0.5);
                    float mag = easeInOutSigmoid(radDist * midpoint, fuzziness) * vignette/100.0 * 0.645;
                    output_result.rgb = mix(softOverlayBlend(output_result.rgb, mag), vec3(0.0), mag * mag);

                    output_result = clamp(output_result, 0.0, 1.0);
                }



                /* filter part */
                vec3 filter_result = output_result;

                // color curve 1: optional
                if(colorMap1Enabled==1) {
                    filter_result = applyColorMap(filter_result, colorMapTexture1);

                    output_result = clamp(output_result, 0.0, 1.0);
                }


                // overlay 1: optional
                if(overlay1Enabled==1) {
                    vec3 overlay_image1 = texture2D(overlayTexture1, textureCoordinate).rgb;

                    filter_result = vec3(softLightCal(filter_result.r, overlay_image1.r),
                                         softLightCal(filter_result.g, overlay_image1.g),
                                         softLightCal(filter_result.b, overlay_image1.b));

                    output_result = clamp(output_result, 0.0, 1.0);
                }

                // overlay 2: optional
                if(overlay2Enabled==1) {
                    vec3 overlay_image2 = texture2D(overlayTexture2, textureCoordinate).rgb;

                    filter_result = vec3(overlayCal(filter_result.r, overlay_image2.r),
                                         overlayCal(filter_result.g, overlay_image2.g),
                                         overlayCal(filter_result.b, overlay_image2.b));

                    output_result = clamp(output_result, 0.0, 1.0);
                }

                // filter mix percentage
                vec3 final_result = mix(output_result, filter_result, filterMixPercentage);

                /* output result */
                if(renderOutputType==0) { // output RGBA
                    gl_FragColor = vec4(final_result, 1.);
                } else if(renderOutputType==1) { // RGB to YUV
                    vec3 yuv = rgb2yuvMatrix * final_result + vec3(0.0, 0.5, 0.5);

                    gl_FragColor = vec4(vec3(yuv.g, yuv.b, yuv.r), 1.0);
                }
            }
    );

    return (*env)->NewStringUTF(env, shaderString);
}