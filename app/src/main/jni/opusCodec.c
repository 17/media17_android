#include <jni.h>
#include <android/log.h>

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <android/log.h>

#include <opus.h>

#define LOG_ERROR(message) __android_log_write(ANDROID_LOG_ERROR, "story17", message)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,"story17",__VA_ARGS__)

// encoder and decoder state
static OpusEncoder* encoder;
static unsigned char* audioEncodeBuffer;
static int targetBitRate;
static OpusDecoder* decoder;
static opus_int16* audioDecodeBuffer;

JNIEXPORT void JNICALL Java_com_machipopo_media_OpusCodec_setup(JNIEnv *env, jobject obj, jint bitRate)
{
    avcodec_register_all();
//    av_log_set_level(AV_LOG_QUIET);

    /* Release Previous Codec Instance (If Exist) */
    if(encoder!=NULL) {
        opus_encoder_destroy(encoder);
        encoder = NULL;
    }

    if(decoder!=NULL) {
        opus_decoder_destroy(decoder);
        decoder = NULL;
    }

    if(audioEncodeBuffer!=NULL) {
        free(audioEncodeBuffer);
        audioEncodeBuffer = NULL;
    }

    if(audioDecodeBuffer!=NULL) {
        free(audioDecodeBuffer);
        audioDecodeBuffer = NULL;
    }

    /* Setup New Codec Instance */
    targetBitRate = bitRate;

    if(audioEncodeBuffer==NULL) {
        audioEncodeBuffer = malloc(512);
    }
        
    int errCode;
    
    // Init Encoder
    if(targetBitRate>0) {
        encoder = opus_encoder_create(48000, 1, OPUS_APPLICATION_AUDIO, &errCode);

        errCode = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(targetBitRate));
        errCode = opus_encoder_ctl(encoder, OPUS_SET_COMPLEXITY(10));
        errCode = opus_encoder_ctl(encoder, OPUS_SET_VBR(1));
        errCode = opus_encoder_ctl(encoder, OPUS_SET_VBR_CONSTRAINT(0));
        errCode = opus_encoder_ctl(encoder, OPUS_SET_PACKET_LOSS_PERC(0));
        errCode = opus_encoder_ctl(encoder, OPUS_SET_MAX_BANDWIDTH(OPUS_BANDWIDTH_FULLBAND));
        errCode = opus_encoder_ctl(encoder, OPUS_SET_EXPERT_FRAME_DURATION(OPUS_FRAMESIZE_40_MS));
    }

    // Init Decoder
    audioDecodeBuffer = malloc(sizeof(opus_int16) * 1920);

    decoder = opus_decoder_create(48000, 1, &errCode);
    errCode = opus_decoder_ctl(decoder, OPUS_SET_EXPERT_FRAME_DURATION(OPUS_FRAMESIZE_40_MS));

    LOG_ERROR("SETUP OPUS AUDIO CODEC COMPLETED!");
}

JNIEXPORT void JNICALL Java_com_machipopo_media_OpusCodec_release(JNIEnv *env, jobject obj)
{
    /* Release Previous Codec Instance (If Exist) */
    if(encoder!=NULL) {
        opus_encoder_destroy(encoder);
        encoder = NULL;
    }

    if(decoder!=NULL) {
        opus_decoder_destroy(decoder);
        decoder = NULL;
    }

    if(audioEncodeBuffer!=NULL) {
        free(audioEncodeBuffer);
        audioEncodeBuffer = NULL;
    }

    if(audioDecodeBuffer!=NULL) {
        free(audioDecodeBuffer);
        audioDecodeBuffer = NULL;
    }

    LOG_ERROR("RELEASE OPUS AUDIO CODEC COMPLETED!");
}

JNIEXPORT jint JNICALL Java_com_machipopo_media_OpusCodec_encodeAudio(JNIEnv *env, jobject obj, jobject inputDataBuffer, jobject outputDataBuffer)
{
    jbyte* inputDataPtr = (*env)->GetDirectBufferAddress(env, inputDataBuffer);
    jbyte* outputDataPtr = (*env)->GetDirectBufferAddress(env, outputDataBuffer);

    int encodedDataLength = opus_encode(encoder, (const opus_int16 *) inputDataPtr, 1920, audioEncodeBuffer, 512); // 1920 samples
    
    if(encodedDataLength>0) {
        memcpy(outputDataPtr, audioEncodeBuffer, encodedDataLength);
    }

    return encodedDataLength;
}

JNIEXPORT jint JNICALL Java_com_machipopo_media_OpusCodec_decodeAudio(JNIEnv *env, jobject obj, jobject inputDataBuffer, jint inputDataLength, jobject outputDataBuffer)
{
    jbyte* inputDataPtr = (*env)->GetDirectBufferAddress(env, inputDataBuffer);
    jbyte* outputDataPtr = (*env)->GetDirectBufferAddress(env, outputDataBuffer);

    int numOfDecodedSamples = opus_decode(decoder, inputDataPtr, (opus_int32) inputDataLength, audioDecodeBuffer, 1920, 0);
        
    if (numOfDecodedSamples>0) {
        memcpy(outputDataPtr, audioDecodeBuffer, numOfDecodedSamples*2);
    }

    return numOfDecodedSamples;
}
