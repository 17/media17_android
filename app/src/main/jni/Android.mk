LOCAL_PATH := $(call my-dir)

APP_ABI := all

include $(CLEAR_VARS)
LOCAL_MODULE := weibosdkcore
LOCAL_SRC_FILES := $(LOCAL_PATH)/weibo/libweibosdkcore.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := bdpush_V2_3
LOCAL_SRC_FILES := $(LOCAL_PATH)/bdpush/libbdpush_V2_3.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := 17media

LOCAL_STATIC_LIBRARIES := cpufeatures

LOCAL_CFLAGS += -Iopus/include -Ilibvpx/include -Iopenh264/include -Iffmpeg/include -Icpu-features
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog -lz -lc -lm -lstdc++ -landroid $(LOCAL_PATH)/ffmpeg/lib/libavformat.a $(LOCAL_PATH)/ffmpeg/lib/libavcodec.a $(LOCAL_PATH)/ffmpeg/lib/libavutil.a $(LOCAL_PATH)/openh264/lib/libopenh264.a $(LOCAL_PATH)/libvpx/lib/libvpx.a $(LOCAL_PATH)/opus/lib/libopus.a

# source files
LOCAL_SRC_FILES := vp8Codec.c
LOCAL_SRC_FILES += opusCodec.c
LOCAL_SRC_FILES += mp4Recorder.c
LOCAL_SRC_FILES += cpu-features/cpu-features.c
LOCAL_SRC_FILES += shaderLoader.c
LOCAL_SRC_FILES += main.c

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/cpufeatures)
