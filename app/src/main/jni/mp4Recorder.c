#include <jni.h>
#include <android/log.h>

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/dict.h>
#include <libavutil/eval.h>
#include <libavutil/fifo.h>
#include <libavutil/pixfmt.h>
#include <libavutil/rational.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>

#define LOG_ERROR(message) __android_log_write(ANDROID_LOG_ERROR, "story17", message)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,"story17",__VA_ARGS__)

#define MOVIE_WIDTH 480
#define MOVIE_HEIGHT 480
#define MOVIE_FRAME_RATE 30
#define MOVIE_GOP_SIZE 30
#define MOVIE_VIDEO_BIT_RATE 1024000
#define MOVIE_AUDIO_BIT_RATE 96000
#define MOVIE_AUDIO_SAMPLE_RATE 44100

#define LIVE_STREAM_WIDTH 640
#define LIVE_STREAM_HEIGHT 360
#define LIVE_STREAM_FRAME_RATE 30
#define LIVE_STREAM_GOP_SIZE 30
#define LIVE_STREAM_VIDEO_BIT_RATE 1024000
#define LIVE_STREAM_AUDIO_BIT_RATE 96000
#define LIVE_STREAM_AUDIO_SAMPLE_RATE 44100

#define LIVE_STREAM_MODE 0
#define MOVIE_MODE 1

// encoder and decoder state
static AVOutputFormat* avOutputFormat;
static AVFormatContext* avFormatContext;
static AVStream* audioStream;
static AVStream* videoStream;
static AVCodecContext* videoCodecContext;
static AVCodecContext* audioCodecContext;
static AVCodec* audioCodec;
static AVCodec* videoCodec;
static AVFrame* videoFrame;
static AVFrame* audioFrame;

static int recorderMode;
static int frameWidth;
static int frameHeight;
static int frameRate;
static int gopSize;
static int videoBitRate;
static int audioBitRate;
static int audioSampleRate;

JNIEXPORT void JNICALL Java_com_machipopo_media_Mp4Recorder_init(JNIEnv *env, jobject obj, jstring filePath, jint mode)
{
    recorderMode = mode;

    if(recorderMode==LIVE_STREAM_MODE) {
        frameWidth = LIVE_STREAM_WIDTH;
        frameHeight = LIVE_STREAM_HEIGHT;
        frameRate = LIVE_STREAM_FRAME_RATE;
        gopSize = LIVE_STREAM_GOP_SIZE;
        videoBitRate = LIVE_STREAM_VIDEO_BIT_RATE;
        audioBitRate = LIVE_STREAM_AUDIO_BIT_RATE;
        audioSampleRate = LIVE_STREAM_AUDIO_SAMPLE_RATE;
    } else if(recorderMode==MOVIE_MODE) {
        frameWidth = MOVIE_WIDTH;
        frameHeight = MOVIE_HEIGHT;
        frameRate = MOVIE_FRAME_RATE;
        gopSize = MOVIE_GOP_SIZE;
        videoBitRate = MOVIE_VIDEO_BIT_RATE;
        audioBitRate = MOVIE_AUDIO_BIT_RATE;
        audioSampleRate = MOVIE_AUDIO_SAMPLE_RATE;
    }

    const char *movieFilePath = (*env)->GetStringUTFChars(env, filePath, 0);
    int result;

    av_register_all();

    result = avformat_alloc_output_context2(&avFormatContext, NULL, NULL, movieFilePath);
    avFormatContext->oformat->video_codec = CODEC_ID_H264;
    avFormatContext->oformat->audio_codec = CODEC_ID_AAC;
    avOutputFormat = avFormatContext->oformat;

    // add video stream
    videoCodec = avcodec_find_encoder(avOutputFormat->video_codec);
    videoStream = avformat_new_stream(avFormatContext, videoCodec);
    videoStream->id = avFormatContext->nb_streams-1;
    videoStream->time_base = (AVRational) {1, frameRate};
    videoCodecContext = videoStream->codec;
    videoCodecContext->time_base = (AVRational) {1, frameRate};
    videoCodecContext->codec_id = avOutputFormat->video_codec;
    videoCodecContext->bit_rate = videoBitRate;
    videoCodecContext->width = frameWidth;
    videoCodecContext->height = frameHeight;
    videoCodecContext->pix_fmt = PIX_FMT_YUV420P;
    videoCodecContext->thread_count = 2;
    videoCodecContext->rc_strategy = 1;

    // rotate video stream 90 degrees
    av_dict_set(&videoStream->metadata, "rotate", "90", 0);

    // add audio stream
    audioCodec = avcodec_find_encoder(avOutputFormat->audio_codec);
    audioStream = avformat_new_stream(avFormatContext, audioCodec);
    audioStream->id = avFormatContext->nb_streams-1;
    audioCodecContext = audioStream->codec;
    audioCodecContext->codec_id = avOutputFormat->audio_codec;
    audioCodecContext->bit_rate = audioBitRate;
    audioCodecContext->sample_rate = audioSampleRate;
    audioCodecContext->sample_fmt = AV_SAMPLE_FMT_FLTP;
    audioCodecContext->channels = 1;
    audioCodecContext->channel_layout = AV_CH_LAYOUT_MONO;
    audioCodecContext->thread_count = 2;

    // check if need add global header
    if (avFormatContext->oformat->flags & AVFMT_GLOBALHEADER) {
        videoCodecContext->flags |= CODEC_FLAG_GLOBAL_HEADER;
        audioCodecContext->flags |= CODEC_FLAG_GLOBAL_HEADER;
    }

    // open video codec
    result = avcodec_open2(videoCodecContext, videoCodec, NULL);

    // alloc reusable video frame buffer
    videoFrame = av_frame_alloc();
    videoFrame->format = videoCodecContext->pix_fmt;
    videoFrame->width  = videoCodecContext->width;
    videoFrame->height = videoCodecContext->height;
    videoFrame->linesize[0] = videoCodecContext->width;
    videoFrame->linesize[1] = videoCodecContext->width/2;
    videoFrame->linesize[2] = videoCodecContext->width/2;

    // open audio coded
    audioCodecContext->strict_std_compliance = -2; // turn off experimental codec warning
    result = avcodec_open2(audioCodecContext, audioCodec, NULL);

    // alloc reusable audio frame buffer
    audioFrame = av_frame_alloc();

    audioFrame->nb_samples = audioCodecContext->frame_size; // 1024
    audioFrame->format = AV_SAMPLE_FMT_FLTP;
    audioFrame->sample_rate = audioSampleRate;
    audioFrame->channel_layout = AV_CH_LAYOUT_MONO;
    audioFrame->channels = 1;

    av_dump_format(avFormatContext, 0, movieFilePath, 1);

    if (!(avOutputFormat->flags & AVFMT_NOFILE)) {
        avio_open(&avFormatContext->pb, movieFilePath, AVIO_FLAG_WRITE);
    }

    // write header
    result = avformat_write_header(avFormatContext, NULL);
}

JNIEXPORT void JNICALL Java_com_machipopo_media_Mp4Recorder_finish(JNIEnv *env, jobject obj)
{
    av_write_trailer(avFormatContext);

    if (!(avOutputFormat->flags & AVFMT_NOFILE)) {
        avio_close(avFormatContext->pb);
    }
    
    // close video
    av_frame_free(&videoFrame);
    avcodec_close(videoCodecContext);
    
    // close audio
    av_frame_free(&audioFrame);
    avcodec_close(audioCodecContext);

    avformat_free_context(avFormatContext);
}

JNIEXPORT void JNICALL Java_com_machipopo_media_Mp4Recorder_encodeVideoData(JNIEnv *env, jobject obj, jobject yuvDataBuffer)
{
    int result;
    jbyte* yuvData = (*env)->GetDirectBufferAddress(env, yuvDataBuffer);

    AVPacket videoPacket;
    av_init_packet(&videoPacket);
    videoPacket.data = NULL;
    videoPacket.size = 0;
    
    videoFrame->data[0] = yuvData;
    videoFrame->data[1] = yuvData + frameWidth*frameHeight;
    videoFrame->data[2] = yuvData + frameWidth*frameHeight*5/4;

    /* encode the image */
    int gotPacket;

    result = avcodec_encode_video2(videoCodecContext, &videoPacket, videoFrame, &gotPacket);

    if(result<0) {
        LOGE("ENCODE VIDEO FRAME FAIL!");
        return;
    }
    
    videoPacket.stream_index = videoStream->index;
    
    result = av_interleaved_write_frame(avFormatContext, &videoPacket);
    av_free_packet(&videoPacket);

    if(result<0) {
//         LOGE("WRITE VIDEO ERROR!");
    }
}

JNIEXPORT void JNICALL Java_com_machipopo_media_Mp4Recorder_encodeAudioData(JNIEnv *env, jobject obj, jbyteArray audioDataBuffer)
{
    jbyte* audioData = (*env)->GetDirectBufferAddress(env, audioDataBuffer);
//    jlong audioDataLength = (*env)->GetDirectBufferCapacity(env, audioDataBuffer);

    int result;

    result = avcodec_fill_audio_frame(audioFrame, audioCodecContext->channels, audioCodecContext->sample_fmt, audioData, 4096, 0);
    
    if(result<0) {
        // LOGE("FILL AUDIO FRAME FAIL!");
        return;
    }
    
    AVPacket audioPacket = {0};
    av_init_packet(&audioPacket);
    audioPacket.data = NULL;
    audioPacket.size = 0;
    
    int gotPacket;
    result = avcodec_encode_audio2(audioCodecContext, &audioPacket, audioFrame, &gotPacket);
    
    if(gotPacket==0 || result<0) {
        // LOGE("ENCODE AUDIO FRAME FAIL!");
        return;
    }
    
    audioPacket.stream_index = audioStream->index;
    
    result = av_interleaved_write_frame(avFormatContext, &audioPacket);
        
    if(result<0) {
        // LOGE("WRITE AUDIO ERROR!");
    }
}
