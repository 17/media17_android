//
// Created by POPO Innovation iMac on 16/1/4.
//

#include "main.h"

#include "jni.h"
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <android/log.h>


#include <jni.h>

// transfer Java string from c string
char* Jstring2CStr(JNIEnv* env, jstring jstr)
{
    char* rtn = NULL;
    jclass clsstring = (*env)->FindClass(env,"java/lang/String");
    jstring strencode = (*env)->NewStringUTF(env,"GB2312");
    jmethodID mid = (*env)->GetMethodID(env,clsstring, "getBytes", "(Ljava/lang/String;)[B");
    jbyteArray barr= (jbyteArray)(*env)->CallObjectMethod(env,jstr,mid,strencode); // String .getByte("GB2312");
    jsize alen = (*env)->GetArrayLength(env,barr);
    jbyte* ba = (*env)->GetByteArrayElements(env,barr,JNI_FALSE);
    if(alen > 0)
    {
        rtn = (char*)malloc(alen+1); //new char[alen+1]; "\0"
        memcpy(rtn,ba,alen);
        rtn[alen]=0;
    }
    (*env)->ReleaseByteArrayElements(env,barr,ba,0); //釋放記憶體
    return rtn;
}

char key[37]="e599301626ce798-86-9a-4b-ac54cb6ce7c";
char captchaURL[] = "http://52.11.159.139:8000/getHumanTestParams";
char validateURL[] = "http://52.11.159.139:8000/validateHumanTestResponse";
char validate[]="validate";
char captura[]="captura";

void transfer(char* final_key)
{
    final_key[0]=key[32];
    final_key[1]=key[33];
    final_key[2]=key[34];
    final_key[3]=key[35];

    final_key[4]=key[28];
    final_key[5]=key[29];
    final_key[6]=key[30];
    final_key[7]=key[31];

    final_key[8]=key[24];
    final_key[9]=key[25];
    final_key[10]=key[26];
    final_key[11]=key[27];

    final_key[12]=key[20];
    final_key[13]=key[21];
    final_key[14]=key[22];
    final_key[15]=key[23];

    final_key[16]=key[16];
    final_key[17]=key[17];
    final_key[18]=key[18];
    final_key[19]=key[19];

    final_key[20]=key[12];
    final_key[21]=key[13];
    final_key[22]=key[14];
    final_key[23]=key[15];

    final_key[24]=key[8];
    final_key[25]=key[9];
    final_key[26]=key[10];
    final_key[27]=key[11];

    final_key[28]=key[4];
    final_key[29]=key[5];
    final_key[30]=key[6];
    final_key[31]=key[7];
    final_key[32]=key[0];
    final_key[33]=key[1];
    final_key[34]=key[2];
    final_key[35]=key[3];

}

JNIEXPORT jstring JNICALL Java_com_machipopo_media17_BindingPhoneNumberActivity_StringFromJNI(JNIEnv *env,jobject obj, jstring str)
{
    char* p = Jstring2CStr(env,str);
    int input_size =1;
    char* q;
    int i =0;

    //get the length of input string
    while(*p != '\0')
    {
        p++;
        input_size++;
    }

    q = Jstring2CStr(env,str);
    if(input_size == sizeof(captura))
    {
        if(!memcmp(captura,q, sizeof(captura)-1))
        {
            return (*env)->NewStringUTF(env, captchaURL);
        }
    }

    if(input_size == sizeof(validate))
    {
        if(!memcmp(validate,q, sizeof(validate)-1))
        {
            return (*env)->NewStringUTF(env, validateURL);
        }
    }

    return (*env)->NewStringUTF(env, "incorrect captura or validate name");
}


JNIEXPORT jstring JNICALL Java_com_machipopo_media17_ForgotPasswordActivity_1V2_StringFromJNI(JNIEnv *env,jobject obj, jstring str)
{
    char* p = Jstring2CStr(env,str);
    int input_size =1;
    char* q;
    int i =0;

    //get the length of input string
    while(*p != '\0')
    {
        p++;
        input_size++;
    }

    q = Jstring2CStr(env,str);
    if(input_size == sizeof(captura))
    {
        if(!memcmp(captura,q, sizeof(captura)-1))
        {
            return (*env)->NewStringUTF(env, captchaURL);
        }
    }

    if(input_size == sizeof(validate))
    {
        if(!memcmp(validate,q, sizeof(validate)-1))
        {
            return (*env)->NewStringUTF(env, validateURL);
        }
    }

    return (*env)->NewStringUTF(env, "incorrect captura or validate name");



}

JNIEXPORT jstring JNICALL Java_com_machipopo_media17_BindingPhoneNumberActivity_00024DataProvider_sayHellolnC(JNIEnv * env, jobject obj,jstring str)
{

    char final_key[37]="000000000000000000000000000000000000";
    transfer(final_key);

    return (*env)->NewStringUTF(env,final_key);

}

JNIEXPORT jstring JNICALL Java_com_machipopo_media17_EnterSMSCertificationActivity_00024DataProvider_sayHellolnC(JNIEnv * env, jobject obj,jstring str)
{
    char final_key[37]="000000000000000000000000000000000000";
    transfer(final_key);

    return (*env)->NewStringUTF(env,final_key);
}

//  char key[37]="e599301626ce798-86-9a-4b-ac54cb6ce7c";
JNIEXPORT jstring JNICALL Java_com_machipopo_media17_ForgotPasswordActivity_1V2_00024DataProvider_sayHellolnC(JNIEnv * env, jobject obj,jstring str)
{

    char final_key[37]="000000000000000000000000000000000000";//=Jstring2CStr(env,str);
    //final_key[37] = '\0';

    transfer(final_key);

       //final_key= transfer();

//    final_key[0]=key[32];
//    final_key[1]=key[33];
//    final_key[2]=key[34];
//    final_key[3]=key[35];
//
//    final_key[4]=key[28];
//    final_key[5]=key[29];
//    final_key[6]=key[30];
//    final_key[7]=key[31];
//
//    final_key[8]=key[24];
//    final_key[9]=key[25];
//    final_key[10]=key[26];
//    final_key[11]=key[27];
//
//    final_key[12]=key[20];
//    final_key[13]=key[21];
//    final_key[14]=key[22];
//    final_key[15]=key[23];
//
//    final_key[16]=key[16];
//    final_key[17]=key[17];
//    final_key[18]=key[18];
//    final_key[19]=key[19];
//
//    final_key[20]=key[12];
//    final_key[21]=key[13];
//    final_key[22]=key[14];
//    final_key[23]=key[15];
//
//    final_key[24]=key[8];
//    final_key[25]=key[9];
//    final_key[26]=key[10];
//    final_key[27]=key[11];
//
//    final_key[28]=key[4];
//    final_key[29]=key[5];
//    final_key[30]=key[6];
//    final_key[31]=key[7];
//    final_key[32]=key[0];
//    final_key[33]=key[1];
//    final_key[34]=key[2];
//    final_key[35]=key[3];
    //final_key[36]=key[1];

    return (*env)->NewStringUTF(env,final_key);

}
