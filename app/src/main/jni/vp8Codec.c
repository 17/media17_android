#include <jni.h>
#include <android/log.h>

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <android/log.h>

#include <vpx/vp8.h>

#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/dict.h>
#include <libavutil/eval.h>
#include <libavutil/fifo.h>
#include <libavutil/pixfmt.h>
#include <libavutil/rational.h>
#include <libavutil/opt.h>

#define LOG_ERROR(message) __android_log_write(ANDROID_LOG_ERROR, "story17", message)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,"story17",__VA_ARGS__)

#define CODEC_FLAG_TRUNCATED 0x00010000

#define FRAME_WIDTH 640
#define FRAME_HEIGHT 360
#define FRAME_RATE 30

// encoder and decoder state
static AVCodec* encoderCodec;
static AVCodec* decoderCodec;
static AVCodecContext* encoderContext;
static AVCodecContext* decoderContext;
static AVFrame* encodeVideoFrame;
static int targetBitRate;

JNIEXPORT void JNICALL Java_com_machipopo_media_VP8Codec_setup(JNIEnv *env, jobject obj, jint bitRate)
{
    avcodec_register_all();
//    av_log_set_level(AV_LOG_QUIET);

    /* Release Previous Codec Instance (If Exist) */
    if(encodeVideoFrame!=NULL) {
        av_frame_free(&encodeVideoFrame);
    }

    if(encoderContext!=NULL) {
        avcodec_close(encoderContext);
        av_free(encoderContext);
    }

    if(decoderContext!=NULL) {
        avcodec_close(decoderContext);
        av_free(decoderContext);
    }

    encoderCodec = NULL;
    encoderContext = NULL;

    decoderCodec = NULL;
    decoderContext = NULL;

    /* Setup New Codec Instance */
    targetBitRate = bitRate;

    // setup encoder
    if(targetBitRate>0) {
        encoderCodec = avcodec_find_encoder(CODEC_ID_VP8);

        encoderContext = avcodec_alloc_context3(encoderCodec);

        encoderContext->bit_rate = targetBitRate;
        encoderContext->width = FRAME_WIDTH;
        encoderContext->height = FRAME_HEIGHT;
        encoderContext->gop_size = 30;
        encoderContext->time_base = (AVRational) {1, FRAME_RATE};
        encoderContext->pix_fmt = PIX_FMT_YUV420P;
//        encoderContext->rc_strategy = 0;

        av_opt_set(encoderContext->priv_data, "quality", "realtime", 0);
        av_opt_set_int(encoderContext->priv_data, "cpu-used", 2, 0);
//        av_opt_set_int(encoderContext->priv_data, "deadline", 15, 0);

        avcodec_open2(encoderContext, encoderCodec, NULL);

        // alloc reusable video frame buffer
        encodeVideoFrame = av_frame_alloc();
        encodeVideoFrame->format = encoderContext->pix_fmt;
        encodeVideoFrame->width  = encoderContext->width;
        encodeVideoFrame->height = encoderContext->height;
        encodeVideoFrame->linesize[0] = encoderContext->width;
        encodeVideoFrame->linesize[1] = encoderContext->width/2;
        encodeVideoFrame->linesize[2] = encoderContext->width/2;
    }

    // setup decoder
    decoderCodec = avcodec_find_decoder(CODEC_ID_VP8);
    decoderContext = avcodec_alloc_context3(decoderCodec);
    
    decoderContext->flags|= CODEC_FLAG_TRUNCATED;
    decoderContext->width = FRAME_WIDTH;
    decoderContext->height = FRAME_HEIGHT;
    decoderContext->pix_fmt = PIX_FMT_YUV420P;
    decoderContext->thread_count = 2;

    avcodec_open2(decoderContext, decoderCodec, NULL);
    
    LOG_ERROR("SETUP VP8 VIDEO CODEC COMPLETED!");
}

JNIEXPORT void JNICALL Java_com_machipopo_media_VP8Codec_release(JNIEnv *env, jobject obj)
{
    /* Release Previous Codec Instance (If Exist) */
    if(encodeVideoFrame!=NULL) {
        av_frame_free(&encodeVideoFrame);
    }

    if(encoderContext!=NULL) {
        avcodec_close(encoderContext);
        av_free(encoderContext);
    }

    if(decoderContext!=NULL) {
        avcodec_close(decoderContext);
        av_free(decoderContext);
    }

    encoderCodec = NULL;
    encoderContext = NULL;

    decoderCodec = NULL;
    decoderContext = NULL;

    LOG_ERROR("RELEASE VP8 VIDEO CODEC COMPLETED!");
}

JNIEXPORT jint JNICALL Java_com_machipopo_media_VP8Codec_encodeFrame(JNIEnv *env, jobject obj, jobject yuvDataBuffer, jobject outputBuffer)
{
    jbyte* yuvData = (*env)->GetDirectBufferAddress(env, yuvDataBuffer);
    jbyte* outputData = (*env)->GetDirectBufferAddress(env, outputBuffer);
    jlong outputBufferSize = (*env)->GetDirectBufferCapacity(env, outputBuffer);

    AVPacket framePacket;
    av_init_packet(&framePacket);
    framePacket.data = NULL;
    framePacket.size = 0;

    encodeVideoFrame->data[0] = yuvData;
    encodeVideoFrame->data[1] = yuvData + FRAME_WIDTH*FRAME_HEIGHT;
    encodeVideoFrame->data[2] = yuvData + FRAME_WIDTH*FRAME_HEIGHT * 5/4;

    int gotPacket;
    int result = avcodec_encode_video2(encoderContext, &framePacket, encodeVideoFrame, &gotPacket);
    int encodedDataLength = framePacket.size;

    // check if encoding success
    if(result<0 || gotPacket==0) { // fail
        av_free_packet(&framePacket);

        return result;
    } else { // success
        memcpy(outputData, framePacket.data, framePacket.size);

        av_free_packet(&framePacket);

        return encodedDataLength;
    }
}

JNIEXPORT jint JNICALL Java_com_machipopo_media_VP8Codec_decodeFrame(JNIEnv *env, jobject obj, jobject inputDataBuffer, jint inputDataLength, jint cropToSquare, jobject yDataOutputBuffer, jobject uDataOutputBuffer, jobject vDataOutputBuffer)
{
    if(inputDataLength==0) {
        return 0;
    }

    jbyte* inputData = (*env)->GetDirectBufferAddress(env, inputDataBuffer);

    jbyte* yDataBuffer = (*env)->GetDirectBufferAddress(env, yDataOutputBuffer);
    jbyte* uDataBuffer = (*env)->GetDirectBufferAddress(env, uDataOutputBuffer);
    jbyte* vDataBuffer = (*env)->GetDirectBufferAddress(env, vDataOutputBuffer);

    AVFrame* frame = av_frame_alloc();

    AVPacket framePacket;
    av_init_packet(&framePacket);

    // fill frame packet with data
    framePacket.size = inputDataLength;
    framePacket.data = inputData;

    int frameDataSize = FRAME_WIDTH * FRAME_HEIGHT * 3 / 2;
    int len, gotFrame;

    len = avcodec_decode_video2(decoderContext, frame, &gotFrame, &framePacket);

    if (len>0 && gotFrame) {
        if(cropToSquare) {
            // copy y data
            int x, y;

            for(y=0;y<frame->height;y++) {
                memcpy(yDataBuffer + y*frame->height, frame->data[0] + y*frame->linesize[0] + (frame->width-frame->height)/2, frame->height);
            }

            // copy uv data
            int uvPlaneOffset = frame->height * frame->height;

            int halfFrameWidth = frame->width / 2;
            int halfFrameHeight = frame->height / 2;

            for(y=0;y<halfFrameHeight;y++) {
                memcpy(uDataBuffer + y*halfFrameHeight, frame->data[1] + y*frame->linesize[1] + (halfFrameWidth-halfFrameHeight)/2, halfFrameHeight);
                memcpy(vDataBuffer + y*halfFrameHeight, frame->data[2] + y*frame->linesize[2] + (halfFrameWidth-halfFrameHeight)/2, halfFrameHeight);
            }
        } else {
            // copy y data
            int x, y;

            for(y=0;y<frame->height;y++) {
                memcpy(yDataBuffer + y*frame->width, frame->data[0] + y*frame->linesize[0], frame->width);
            }

            // copy uv data
            int uvPlaneOffset = frame->width * frame->height;

            int halfFrameWidth = frame->width / 2;
            int halfFrameHeight = frame->height / 2;

            for(y=0;y<halfFrameHeight;y++) {
                memcpy(uDataBuffer + y*halfFrameWidth, frame->data[1] + y*frame->linesize[1], halfFrameWidth);
                memcpy(vDataBuffer + y*halfFrameWidth, frame->data[2] + y*frame->linesize[2], halfFrameWidth);
            }
        }
    }
    
    av_frame_free(&frame);
    av_free_packet(&framePacket);
    
    return gotFrame;
}

