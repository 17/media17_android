package com.iapppay.pay.mobile.iapppaysecservice.utils;

/**
 *应用接入iAppPay云支付平台sdk集成信息 
 */
public class IAppPaySDKConfig{

	/**
	 * 应用名称：
	 * 应用在iAppPay云支付平台注册的名称
	 */
	public final static  String APP_NAME = "17";

	/**
	 * 应用编号：
	 * 应用在iAppPay云支付平台的编号，此编号用于应用与iAppPay云支付平台的sdk集成 
	 */
	public final static  String APP_ID = "3003177570";

	/**
	 * 商品编号：
	 * 应用的商品在iAppPay云支付平台的编号，此编号用于iAppPay云支付平台的sdk到iAppPay云支付平台查找商品详细信息（商品名称、商品销售方式、商品价格）
	 * 编号对应商品名称为：17币
	 */
	public final static  int WARES_ID_1=1;

	/**
	 * 应用私钥：
	 * 用于对商户应用发送到平台的数据进行加密
	 */
	public final static String APPV_KEY = "MIICXAIBAAKBgQC/wsadS9aGCcUBaqClfQlp+PE57DmfKoxe+yvuWzjBDD4Cq65SmNxO7AVfdU9UivRcFmau5Mh2lGgF7I9CtbRq3a9e5TrDJ0V1TXqgdusvrpiiclgUJO2rUM9HAWDT2PMprCtjMZG1/bc/ZfzmC4UrBzCo1csEgA0+RVRGFLb3mQIDAQABAoGAcQl32rEhu9F2d+dK20Jhkj5UtDgzn0eNh0+4DcFoKyP0Sc6566Z/92XZpPOZ19QxRzdNQDtMj9nbl+JicIh72J2VemG1f2OoqvEOEDnARVAcK5ymmkDw/xSDCgUy69VuyjgCyvdllabLUtNXrJuNuQPXr+4BRt2w5sWQoQElGyECQQDjqhQMn8BlEMuQsw1s7UxnlMCFS/YH5JJzn3tYhzUsXNcqokgGVBThLkYHo/feg9TTdtSkUDZdUIfngzLTmhWvAkEA16C6EqfVKORAY5rgOi1kyScBaQe5jPpA2jQ0kiC0Fs7WJTTm1J0gGgl3jGYvGGu0IXwYwSMW4F3KGPTr6CthNwJBAMxMpN9LQICxWKvKMgMM06KkFzvHFcqxcEb3191yv+HGkoO8yzt6mp4UQ12548Y3sXXuThYBsTd9++dikerXfBsCQCWn7vbVEneqrMf4QxJF3ST3x9pEvBWvmmXA2jFuvqSBCIrE207kBbgmURdWvQDBxlpWpqgL7bHPDz0egJgyAy8CQFrh+Rco3Nb4pfcW8YROg58rfia8iZyXXHbD35Uuu7HFMU3rtYBYIz6BLMefOGvvZPnYhuAVxP8oJ41Re7UxpF0=";

	/**
	 * 平台公钥：
	 * 用于商户应用对接收平台的数据进行解密
	
	 */
	public final static String PLATP_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEzN22h3ImdflP/WZpuyJADAS+2RjYxcal0OIHG8uhMB/YsJdxi91CrLCvg031UOwmaw1CMIWDIHoXlb6wWbU+v2pGId34/bH5ghsIdU6JbFLYkim7Dw7Kzq4XATiQ8TNciqi+7cyYrJw7Kkr5Wt+o+qhbjGGOHFhNntwa1gkZrwIDAQAB";

}