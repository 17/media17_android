package com.machipopo.media;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.os.Handler;
import android.os.Looper;

import com.machipopo.media17.Singleton;

public class PCMAudioRecorder {
	public interface PCMAudioRecorderDelegate {
		void didGetAudioData(byte[] buffer, int readBytes);
	}
	
	int bufferSize = 0;
	boolean isRecording = false;
	AudioRecord audioRecorder;
	public PCMAudioRecorderDelegate delegate;
	
	public PCMAudioRecorder() {
		bufferSize = AudioRecord.getMinBufferSize(48000, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);
		audioRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 48000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);

		// check if echo canceller available
		if(Integer.valueOf(android.os.Build.VERSION.SDK)>=16) {
			if (AcousticEchoCanceler.isAvailable()) {
				AcousticEchoCanceler aec = AcousticEchoCanceler.create(audioRecorder.getAudioSessionId());
				if(!aec.getEnabled())
					aec.setEnabled(true);
			}
		}
	}

	public void startRecording() {
		isRecording = true;
		
		final Handler handler = new Handler(Looper.getMainLooper());
		
		new Thread(new Runnable() {
			public void run() {
				audioRecorder.startRecording();

				while (isRecording) {
					try{
						final byte[] buffer = new byte[bufferSize];
						final int readBytes = audioRecorder.read(buffer, 0, bufferSize);

						try {
							handler.post(new Runnable() {
								public void run() {
									delegate.didGetAudioData(buffer, readBytes);
								}
							});
						} catch (Exception e) {
							// TODO: handle exception
							Singleton.log("PCMAudioRecorder e: " + e.getMessage());
						}
					}
					catch (OutOfMemoryError o){
//						Singleton.log("PCMAudioRecorder e: " + o.getMessage());
					}
					catch (Exception e){
//						Singleton.log("PCMAudioRecorder e: " + e.getMessage());
					}
				}
			}
		}).start();
	}
	
	public void stopRecording() {
		isRecording = false;

		try {
			audioRecorder.stop();
		} catch (Exception e) {

		}

		try {
			audioRecorder.release();
		} catch (Exception e) {

		}
	}
}
