package com.machipopo.media;

import java.nio.ByteBuffer;

public class OpusCodec {

    static private OpusCodec INSTANCE;

	private OpusCodec() {
		System.loadLibrary("17media");
	}
	
	public native void setup(int bitRate);
	public native void release();

	public native int encodeAudio(ByteBuffer inputDataBuffer, ByteBuffer outputDataBuffer); // return > 0 if no error
	public native int decodeAudio(ByteBuffer inputDataBuffer, int inputDataLength, ByteBuffer outputDataBuffer); // return > 0 if no error
	
	static public OpusCodec instance() {
		if(INSTANCE==null) {
			INSTANCE = new OpusCodec();
		}
		
        return INSTANCE;
    }
}
