package com.machipopo.media;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Looper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public class FloatPCMAudioRecorder {
	public interface FloatPCMAudioRecorderDelegate {
		void didGetAudioData(float[] pcmData, int numOfSamples);
	}
	
	int bufferSize = 0;
	boolean isRecording = false;
	AudioRecord audioRecorder;
	public FloatPCMAudioRecorderDelegate delegate;
	
	public FloatPCMAudioRecorder() {
		setupRecorder();
    }

	private void setupRecorder() {
		stopRecording();

		bufferSize = AudioRecord.getMinBufferSize(44100, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);
		audioRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);
	}

	public void startRecording() {
		isRecording = true;
		
		final Handler handler = new Handler(Looper.getMainLooper());
		
		new Thread(new Runnable() {
			public void run() {
				final ByteBuffer pcmDataBuffer = ByteBuffer.allocateDirect(bufferSize);
				pcmDataBuffer.order(ByteOrder.nativeOrder());

				if(audioRecorder!=null && audioRecorder.getState()==AudioRecord.STATE_UNINITIALIZED) {
					setupRecorder();
				}

				if(audioRecorder!=null && audioRecorder.getState()==AudioRecord.STATE_INITIALIZED) {
					audioRecorder.startRecording();

					while (isRecording) {
						pcmDataBuffer.rewind();

						final int readBytes = audioRecorder.read(pcmDataBuffer, bufferSize);

						if(readBytes<=0) {
							continue;
						}

						final int numOfSamples = readBytes/2;
						if(numOfSamples>0)
						{
							final float[] pcmData = new float[numOfSamples];
							final ShortBuffer shortPcmDataBuffer = pcmDataBuffer.asShortBuffer();

							// convert to floating point pcm
							for(int i=0;i<numOfSamples;i++) {
								pcmData[i] = ((float) shortPcmDataBuffer.get()) / 32768.0f;
							}

							try {
								handler.post(new Runnable() {
									public void run() {
										delegate.didGetAudioData(pcmData, numOfSamples);
									}
								});
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					}
				}
			}
		}).start();
	}
	
	public void stopRecording() {
		isRecording = false;

		if(audioRecorder!=null && audioRecorder.getState()==AudioRecord.STATE_INITIALIZED) {
			audioRecorder.stop();
		}

		audioRecorder = null;
	}
}
