package com.machipopo.media;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Handler;
import android.os.Looper;

import com.machipopo.media17.Constants;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PCMAudioPlayer {
	private class AudioMarker extends Object {

		public AudioMarker() {

		}

		public long absTimestamp;
		public long scheduledPlaybackTime;
	}

	int bufferSize = 0;
	AudioTrack audioTrack;
    ByteBuffer audioSampleBuffer;
	ArrayList<AudioMarker> audioMarkers;

	Handler handler;

	private static int NUMBER_OF_SAMPLES_IN_AUDIO_BUFFER = 1920; // => 40 ms
	private static int NUMBER_OF_AUDIO_BUFFERS = 40;

	long numberOfSamplesInAudioTrack = 0;
	long lastAudioTrackHeadPosition = 0;

	public PCMAudioPlayerDelegate delegate;

	Timer callbackTimer;

	public interface PCMAudioPlayerDelegate {
		void didPlayAudio(long absTime, int numOfAudioBufferInQueue);
	}

	public PCMAudioPlayer() {
//		bufferSize = AudioTrack.getMinBufferSize(Constants.LIVE_STREAM_AUDIO_SAMPLE_RATE, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

		bufferSize = NUMBER_OF_SAMPLES_IN_AUDIO_BUFFER * 2 * NUMBER_OF_AUDIO_BUFFERS;

		audioSampleBuffer = ByteBuffer.allocate(bufferSize);
	}

	public void playAudio(byte[] audioData, int offset, int size, final long absTime)
	{
		if(audioTrack!=null) {
			if(audioTrack.getState() == AudioTrack.STATE_INITIALIZED) {
                try {
                    audioTrack.write(audioData, offset, size);

                    numberOfSamplesInAudioTrack += NUMBER_OF_SAMPLES_IN_AUDIO_BUFFER;

                    long currentAudioTrackHeadPosition = audioTrack.getPlaybackHeadPosition();

                    if(lastAudioTrackHeadPosition!=currentAudioTrackHeadPosition) {
                        numberOfSamplesInAudioTrack -= (currentAudioTrackHeadPosition-lastAudioTrackHeadPosition);
                        lastAudioTrackHeadPosition = currentAudioTrackHeadPosition;
                    }

                    AudioMarker marker = new AudioMarker();
                    marker.absTimestamp = absTime;
                    marker.scheduledPlaybackTime = currentAudioTrackHeadPosition + numberOfSamplesInAudioTrack;
                    audioMarkers.add(marker);
                } catch (Exception e) {

                }
			} else {
                reset();
			}
		}
	}

	public void startPlaying() {
		stopPlaying();

		handler = new Handler(Looper.getMainLooper());

		audioMarkers = new ArrayList<AudioMarker>();

		audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, Constants.LIVE_STREAM_AUDIO_SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize, AudioTrack.MODE_STREAM);

        try {
            audioTrack.play();
        } catch (Exception e) {

        }

        callbackTimer = new Timer();
        callbackTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					exeuteTimerTask();
				}
			}, 0, 20L);
	}

	private void exeuteTimerTask() {
		if(audioTrack==null) {
			return;
		}

		try {
			long currentAudioTrackHeadPosition = audioTrack.getPlaybackHeadPosition();

			ArrayList<AudioMarker> newAudioMarkers = new ArrayList<AudioMarker>();
			long playbackAbsTime = 0;

			for (AudioMarker marker : audioMarkers) {
				if (marker.scheduledPlaybackTime <= (currentAudioTrackHeadPosition-NUMBER_OF_SAMPLES_IN_AUDIO_BUFFER)) {
					playbackAbsTime = marker.absTimestamp;
				} else {
					newAudioMarkers.add(marker);
				}
			}

			audioMarkers = newAudioMarkers;

			if (playbackAbsTime == 0) {
				return;
			}

			final long finalPlaybackAbsTime = playbackAbsTime;

			handler.post(new Runnable() {
				@Override
				public void run() {
					delegate.didPlayAudio(finalPlaybackAbsTime, audioMarkers.size());
				}
			});
		} catch (Exception e) {

		}
	}

	public void stopPlaying() {
        numberOfSamplesInAudioTrack = 0;
		lastAudioTrackHeadPosition = 0;

        try {
            audioTrack.stop();
        } catch (Exception e) {

        }

        try {
            audioTrack.release();
        } catch (Exception e) {

        }

        audioTrack = null;

        if(callbackTimer!=null) {
            callbackTimer.cancel();
            callbackTimer.purge();
            callbackTimer = null;
        }
	}

	public void reset() {
		stopPlaying();
		startPlaying();
	}

	public void setGain(float g) {
		audioTrack.setStereoVolume(g, g);
	}
}
