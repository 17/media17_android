package com.machipopo.media;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import com.machipopo.media17.Constants;
import com.machipopo.media17.Singleton;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class VideoRenderer implements GLSurfaceView.Renderer {
	
	Context context;
	
	String vertexShaderCode;
	String fragmentShaderCode;
	
    private float[] rotationMatrix = new float[16];

	int[] textures = new int[3]; // y, u, v textures
	public ByteBuffer yuvDataBuffer = ByteBuffer.allocateDirect(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT * 3/2);;
//	ByteBuffer yDataBuffer;
//	ByteBuffer uDataBuffer;
//	ByteBuffer vDataBuffer;

	int vertexShader;
	int fragmentShader;
	int program;
	int positionHandle;
	int texCoordHandle;
	int yTextureHandle;
	int uTextureHandle;
	int vTextureHandle;
	int rotationMatrixHandle;
	public boolean dataLoaded = false;

	FloatBuffer vertexBuffer;
	FloatBuffer textureCoordBuffer;
	
	float vertices[] =  {-1.0f,  -1.0f,
            1.0f, -1.0f,
            -1.0f, 1.0f,
            1.0f, 1.0f};
	
	float textureCoords[] =   {0.0f, 1.0f,
                   1.0f, 1.0f,
                   0.0f, 0.0f,
                   1.0f, 0.0f};

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		// make yuv data buffer

		// make vertex buffer and draw list buffer
		ByteBuffer vbf = ByteBuffer.allocateDirect(vertices.length * 4);
		vbf.order(ByteOrder.nativeOrder());
		vertexBuffer = vbf.asFloatBuffer();
		vertexBuffer.put(vertices);
		vertexBuffer.position(0);
		
		ByteBuffer tbf = ByteBuffer.allocateDirect(textureCoords.length * 4);
		tbf.order(ByteOrder.nativeOrder());
        textureCoordBuffer = tbf.asFloatBuffer();
        textureCoordBuffer.put(textureCoords);
        textureCoordBuffer.position(0);
		
		// make shader program
        vertexShaderCode = Singleton.loadAssetFile(context, "vertexShader.txt");
        fragmentShaderCode = Singleton.loadAssetFile(context, "fragmentShader.txt");
        
		vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
		fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

		program = GLES20.glCreateProgram();             // create empty OpenGL Program
		GLES20.glAttachShader(program, vertexShader);   // add the vertex shader to program
		GLES20.glAttachShader(program, fragmentShader); // add the fragment shader to program
		GLES20.glLinkProgram(program);                  // create OpenGL program executables
        GLES20.glUseProgram(program);

		positionHandle = GLES20.glGetAttribLocation(program, "in_Position");
        texCoordHandle = GLES20.glGetAttribLocation(program, "in_TexCoordinate");
        yTextureHandle = GLES20.glGetUniformLocation(program, "y_Texture");
        uTextureHandle = GLES20.glGetUniformLocation(program, "u_Texture");
        vTextureHandle = GLES20.glGetUniformLocation(program, "v_Texture");
        rotationMatrixHandle = GLES20.glGetUniformLocation(program, "rotationMatrix");
        
        // bind vertex buffer and texture buffer
        GLES20.glEnableVertexAttribArray(texCoordHandle);
        GLES20.glEnableVertexAttribArray(positionHandle);

        GLES20.glVertexAttribPointer(positionHandle, 2, GLES20.GL_FLOAT, false, 8, vertexBuffer);
        GLES20.glVertexAttribPointer(texCoordHandle, 2, GLES20.GL_FLOAT, false, 8, textureCoordBuffer);

		// generate texture
		GLES20.glGenTextures(3, textures, 0);
		
        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	}
	
	@Override
	public void onDrawFrame(GL10 gl) {
		// Draw background color
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		
		if(!dataLoaded) {
			return;
		}
		
        // y texture
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);
        
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);             

		if(yuvDataBuffer!=null) {
			yuvDataBuffer.position(0);
			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, Constants.LIVE_STREAM_FRAME_WIDTH, Constants.LIVE_STREAM_FRAME_HEIGHT, 0,
					GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, yuvDataBuffer);
		} else {
//			yDataBuffer.position(0);
//			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, Constants.LIVE_STREAM_FRAME_WIDTH, Constants.LIVE_STREAM_FRAME_HEIGHT, 0,
//					GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, yDataBuffer);
		}

		GLES20.glUniform1i(yTextureHandle, 0);

		// u texture
		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[1]);

	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

		if(yuvDataBuffer!=null) {
			yuvDataBuffer.position(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT);
			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, Constants.LIVE_STREAM_FRAME_WIDTH / 2, Constants.LIVE_STREAM_FRAME_HEIGHT / 2, 0,
					GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, yuvDataBuffer);
		} else {
//			uDataBuffer.position(0);
//			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, Constants.LIVE_STREAM_FRAME_WIDTH / 2, Constants.LIVE_STREAM_FRAME_HEIGHT / 2, 0,
//					GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, uDataBuffer);
		}

		GLES20.glUniform1i(uTextureHandle, 1);

		// v texture
		GLES20.glActiveTexture(GLES20.GL_TEXTURE2);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[2]);
        
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
	    GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

		if(yuvDataBuffer!=null) {
			yuvDataBuffer.position(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT * 5 / 4);
			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, Constants.LIVE_STREAM_FRAME_WIDTH / 2, Constants.LIVE_STREAM_FRAME_HEIGHT / 2, 0,
					GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, yuvDataBuffer);
		} else {
//			vDataBuffer.position(0);
//			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, Constants.LIVE_STREAM_FRAME_WIDTH / 2, Constants.LIVE_STREAM_FRAME_HEIGHT / 2, 0,
//					GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, vDataBuffer);
		}

		GLES20.glUniform1i(vTextureHandle, 2);
		
		// rotation
        Matrix.setRotateM(rotationMatrix, 0, 90, 0, 0, 1.0f);
        GLES20.glUniformMatrix4fv(rotationMatrixHandle, 1, false, rotationMatrix, 0);

        // draw video frame
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
	}

	public void setContext(Context c) {
		context = c;
	}
	
	public void loadYUVData(ByteBuffer dataBuffer) {
		dataLoaded = true;

		yuvDataBuffer = dataBuffer;

//		yDataBuffer = null;
//		uDataBuffer = null;
//		vDataBuffer = null;
	}

//	public void loadYUVData(ByteBuffer yBuffer, ByteBuffer uBuffer, ByteBuffer vBuffer) {
//		dataLoaded = true;
//
//		yuvDataBuffer = null;
//
//		yDataBuffer = yBuffer;
//		uDataBuffer = uBuffer;
//		vDataBuffer = vBuffer;
//	}
	
	// helper method to compile openGL shader
	public static int loadShader(int type, String shaderCode){
        int shader = GLES20.glCreateShader(type);

        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }
}
