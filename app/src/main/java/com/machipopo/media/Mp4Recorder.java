package com.machipopo.media;

import java.nio.ByteBuffer;

import android.R.integer;

public class Mp4Recorder {

	public static int LIVE_STREAM_MODE = 0; // 640 * 360
    public static int MOVIE_MODE = 1; // 480 * 480

    static private Mp4Recorder INSTANCE;

	private Mp4Recorder() {
        System.loadLibrary("17media");
	}
	
	public native void init(String filePath, int mode);
	public native void finish();
	
	public native void encodeVideoData(ByteBuffer yuvDataBuffer);
	public native void encodeAudioData(ByteBuffer audioDataBuffer);
	
	static public Mp4Recorder instance() {
		if(INSTANCE==null) {
			INSTANCE = new Mp4Recorder();
		}
		
        return INSTANCE;
    }
}
