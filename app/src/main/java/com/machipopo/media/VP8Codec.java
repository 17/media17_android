package com.machipopo.media;

import java.nio.ByteBuffer;

public class VP8Codec {

    static private VP8Codec INSTANCE;

	private VP8Codec() {
        System.loadLibrary("17media");
	}
	
	public native void setup(int bitRate);
	public native void release();

	public native int encodeFrame(ByteBuffer yuvDataBuffer, ByteBuffer outputBuffer); // return encodedDataLength if no error, return negative error code if have error
	public native int decodeFrame(ByteBuffer inputDataBuffer, int inputDataLength, int cropToSquare, ByteBuffer yDataOutputBuffer, ByteBuffer uDataOutputBuffer, ByteBuffer vDataOutputBuffer); // return 0 if got no decoded frame, return 1 if success
	
	static public VP8Codec instance() {
		if(INSTANCE==null) {
			INSTANCE = new VP8Codec();
		}
		
        return INSTANCE;
    }
}
