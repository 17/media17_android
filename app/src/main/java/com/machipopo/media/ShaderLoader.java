package com.machipopo.media;

import java.nio.ByteBuffer;

public class ShaderLoader {

    static private ShaderLoader INSTANCE;

	private ShaderLoader() {
        System.loadLibrary("17media");
	}
	
	public native String getVertexShader();
	public native String getFragmentShader();

	static public ShaderLoader instance() {
		if(INSTANCE==null) {
			INSTANCE = new ShaderLoader();
		}
		
        return INSTANCE;
    }
}
