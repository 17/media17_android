package com.machipopo.media17.wxapi;

import android.util.Log;

import com.machipopo.media17.BuildConfig;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Zack on 2/15/16.
 */
public class WechatAPI {

    public String getAccessToken(String appId, String secret, String code){
        String urlStr = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="
                + appId + "&secret="
                + secret + "&code=" + code
                + "&grant_type=authorization_code";
        StringBuilder sb = sendRequest(urlStr);
//        try {
//            String accessToken = "";
//            String openId = "";
//            try {
//                JSONObject jsonObj = new JSONObject(sb.toString());
//                accessToken = jsonObj.getString("access_token");
//                openId = jsonObj.getString("openid");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            LOG("WechatAPI", "getAccessToken =" + sb.toString());
//            LOG("WechatAPI", "accessToken=" + accessToken);
//
//            getUserInfo(accessToken, openId);
//            getTokenValid(accessToken, openId);
//            getFriend(accessToken);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return sb.toString();
    }

    public String getFriend(String access_token){
        String urlStr = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token="+access_token;
        StringBuilder sb = sendRequest(urlStr);
        LOG("WechatAPI", "getFriend =" + sb.toString());
        return sb.toString();
    }

    public String getTokenValid(String access_token, String openid){
        String urlStr = "https://api.weixin.qq.com/sns/auth?access_token="+access_token+"&openid="+openid;
        final StringBuilder sb = sendRequest(urlStr);
        LOG("WechatAPI", "getTokenValid =" + sb.toString());
        return sb.toString();
    }

    public String getUserInfo(String access_token, String openid){
        String urlStr = "https://api.weixin.qq.com/sns/userinfo?access_token="+ access_token + "&openid=" + openid;
        final StringBuilder sb = sendRequest(urlStr);
        LOG("WechatAPI", "getUserInfo =" + sb.toString());
        return sb.toString();
    }


    private StringBuilder sendRequest(String urlStr){
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        try {
            URL url = new URL(urlStr);
            HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb;
    }

    private void LOG(String tag, String info){
        if(BuildConfig.DEBUG){
            Log.e(tag, info);
        }
    }
}
