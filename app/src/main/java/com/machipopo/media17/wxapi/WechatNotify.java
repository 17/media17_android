package com.machipopo.media17.wxapi;

import com.tencent.mm.sdk.openapi.BaseResp;

/**
 * Created by Zack on 2/16/16.
 */
public class WechatNotify {

    public int errorCode = -1;

    public WechatNotify(int errorCode){
        this.errorCode = errorCode;
    }

}
