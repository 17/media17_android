package com.machipopo.media17.wxapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.machipopo.media17.ApiManager;
import com.machipopo.media17.BaseActivity;
import com.machipopo.media17.Constants;
import com.machipopo.media17.MenuActivity;
import com.machipopo.media17.R;
import com.machipopo.media17.SignupActivityV2;
import com.machipopo.media17.Singleton;
import com.machipopo.media17.Story17Application;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.notify.NotifyProvider;
import com.machipopo.media17.utils.DevUtils;
import com.machipopo.media17.utils.LogEventUtil;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.tencent.mm.sdk.openapi.BaseReq;
import com.tencent.mm.sdk.openapi.BaseResp;
import com.tencent.mm.sdk.openapi.ConstantsAPI;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

public class WXEntryActivity extends BaseActivity implements IWXAPIEventHandler
{
    private Activity mCtx = this;
    private static final int TIMELINE_SUPPORTED_VERSION = 0x21020001;

    private Button regBtn;
    private IWXAPI api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.main_color));
            }
        }
        catch (Exception e)
        {
        }

        api = WXAPIFactory.createWXAPI(this, Constants.WECHAT_APP_KEY, false);

        regBtn = (Button) findViewById(R.id.reg_btn);
        regBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                api.registerApp(Constants.WECHAT_APP_KEY);

                final SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "none";
                api.sendReq(req);
            }
        });

        api.handleIntent(getIntent(), this);
    }

    @Override
    public void onReq(BaseReq req) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onResp(BaseResp resp) {
        int result = 0;
        if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
            auth(resp);
        }
        else if (resp.getType() == ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX) {
            mCtx.finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    private void auth(BaseResp resp) {
        final String code = ((SendAuth.Resp) resp).token;

        NotifyProvider.getInstance().post(new WechatNotify(resp.errCode));

        new Thread() {
            public void run() {
                URL url;
                BufferedReader reader = null;
                String s = "";
                try {
                    url = new URL(
                            "https://api.weixin.qq.com/sns/oauth2/access_token?appid="
                                    + Constants.WECHAT_APP_KEY + "&secret="
                                    + Constants.WECHAT_SECRET + "&code=" + code
                                    + "&grant_type=authorization_code");
                    URLConnection con = url.openConnection();
                    reader = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String line = reader.readLine().toString();
                    s += line;
                    while ((line = reader.readLine()) != null) {
                        s = s + line;
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
//                Log.i("123", "response: " + s);
                JSONObject jsonObj;
                String accessToken = "";
                String openId = "";
                try {
                    jsonObj = new JSONObject(s);
                    accessToken = jsonObj.getString("access_token");
                    openId = jsonObj.getString("openid");
                    ;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                BufferedReader br = null;
                String str = "";
                final String token = accessToken;
                try {
                    URL userUrl = new URL(
                            "https://api.weixin.qq.com/sns/userinfo?access_token="
                                    + accessToken + "&openid=" + openId);
                    URLConnection conn = userUrl.openConnection();
                    br = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    String tmpStr = br.readLine();
                    str = tmpStr;
                    if ((tmpStr = br.readLine()) != null) {
                        str += tmpStr;
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                Log.i("123", str);

                try
                {
                    try{
                        HashMap<String,String> mHashMap = new HashMap<String,String>();
                        mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                        mHashMap.put("version", Singleton.getVersion());
                        mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                        mHashMap.put("u", Singleton.getPhoneIMEI());
                        mHashMap.put("dn", DevUtils.getDevInfo());
                        LogEventUtil.sendSDKLogEvent(mCtx, "v26_wechat_get_token", mHashMap);
                    }
                    catch (Exception e){
                    }

                    JSONObject mJSONObject = new JSONObject(str);
                    final String openID = mJSONObject.getString("openid");
                    final String name = mJSONObject.getString("nickname");
                    final String pic = mJSONObject.getString("headimgurl");

                    DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, pic);
                    downloadFBProfileImage.execute();

                    ApiManager.checkWechatIDAvailable(mCtx, openID, new ApiManager.checkFacebookIDAvailableCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (success) {
                                sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                sharedPreferences.edit().putString(Constants.FACEBOOK_ID, openID).commit();
                                sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                sharedPreferences.edit().putString(Constants.NAME, name).commit();
                                sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                sharedPreferences.edit().putString("is_wechat", "wechat").commit();

                                sharedPreferences.edit().putString("signup_type", "wechat").commit();
                                sharedPreferences.edit().putString("signup_token", token).commit();

                                hideProgressDialog();
                                startActivity(new Intent(mCtx, SignupActivityV2.class));
                                mCtx.finish();
                            } else {
                                ApiManager.loginActionChina(mCtx, "", "", "wechatID",openID, new ApiManager.LoginAction2Callback() {
                                    @Override
                                    public void onResult(boolean success, String message, UserModel user) {
                                        hideProgressDialog();

                                        if (success) {
                                            if (message.equals("ok")) {

                                                Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                mStory17Application.setUser(user);

                                                Intent intent = new Intent();
                                                intent.setClass(mCtx, MenuActivity.class);
                                                startActivity(intent);
                                                mCtx.finish();
                                            } else if (message.equals("freezed"))
                                                showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                            else
                                                showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                        } else {
                                            showNetworkUnstableToast();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            };
        }.start();
    }

    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

//            Log.d("123","userID : " + userID);
            try{
                this.url = userID.substring(0,userID.length()-2) + 640;
            }
            catch (Exception e){
                this.url = userID;
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {
//                    java.net.URL url = new java.net.URL(this.url);
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setDoInput(true);
//                    connection.connect();

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }
    }

    private SharedPreferences sharedPreferences;
    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }
}
