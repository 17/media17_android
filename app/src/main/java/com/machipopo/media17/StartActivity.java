package com.machipopo.media17;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Trace;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.machipopo.media17.utils.LogEventUtil;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.fabric.sdk.android.Fabric;

public class StartActivity extends Activity
{
    private StartActivity mCtx = this;
    private ProgressBar mProgress;
    private Story17Application mApplication;
    private Branch branch;
    private Handler mHandler = new Handler();
    private Boolean mState = false;
    private int uc_time = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.main_color));
            }
        }
        catch (Exception e)
        {
        }

        Singleton.getInstance(mCtx);

//        try
//        {
//            ApiManager.getSelfInfo(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), new ApiManager.GetSelfInfoCallback()
//            {
//                @Override
//                public void onResult(boolean success, String message, UserModel userModel)
//                {
//                    if (success && userModel != null)
//                    {
//                        if (userModel.getUnLockUser() == 1)
//                        {
//                            File mFreezed = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/is/17.login");
//                            if(mFreezed.exists())
//                            {
//                                mFreezed.delete();
//
//                                File mFileIs = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/is/");
//                                if(mFileIs.exists()) mFileIs.delete();
//
//                                File mFileFreezed = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/");
//                                if(mFileFreezed.exists()) mFileFreezed.delete();
//
//                                File mFileUser = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/");
//                                if(mFileUser.exists()) mFileUser.delete();
//
//                                File mFileSetting = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/");
//                                if(mFileSetting.exists()) mFileSetting.delete();
//                            }
//                        }
//                    }
//                }
//            });
//        }
//        catch (Exception e)
//        {
//
//        }

        ImageView mUc = (ImageView) findViewById(R.id.uc_logo);

        if(!Constants.INTERNATIONAL_VERSION)
        {
            if(BuildConfig.FLAVOR.compareTo("uc")==0)
            {
                mUc.setImageResource(R.drawable.uc_logo);
                mUc.setVisibility(View.VISIBLE);
            }
            else if(BuildConfig.FLAVOR.compareTo("sougou")==0)
            {
                mUc.setImageResource(R.drawable.sougou_logo);
                mUc.setVisibility(View.VISIBLE);
            }
            else if(BuildConfig.FLAVOR.compareTo("lenovo")==0)
            {
                mUc.setImageResource(R.drawable.lenovo_logo);
                mUc.setVisibility(View.VISIBLE);
            }
            else if(BuildConfig.FLAVOR.compareTo("huawei")==0)
            {
                mUc.setImageResource(R.drawable.huawei_logo);
                mUc.setVisibility(View.VISIBLE);
            }
            else if(BuildConfig.FLAVOR.compareTo("letv")==0)
            {
                mUc.setImageResource(R.drawable.letv_logo);
                mUc.setVisibility(View.VISIBLE);
            }
        }

        File mFreezed = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/is/17.login");
        if(mFreezed.exists())
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
            builder.setMessage(getString(R.string.your_account_freezed));
            builder.setTitle(getString(R.string.system_notif));
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    mCtx.finish();
                }
            }).setCancelable(false);

            builder.create().show();

            return;
        }
        else
        {
            if(isEmulator())
            {
                mCtx.finish();
                return;
            }

            mApplication = (Story17Application) getApplication();

            try
            {
                PackageManager packageManager = this.getPackageManager();
                List<PackageInfo> packageInfoList = packageManager.getInstalledPackages(0);

                for(int i = 0 ; i < packageInfoList.size() ;i++)
                {
                    if(packageInfoList.get(i).packageName.contains("org.sbtools"))
                    {
                        mApplication.setIsSbtools(true);
                    }
                }
            }
            catch (Exception e)
            {

            }

            Fabric.with(this, new Crashlytics());

            File mFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName());
            if(!mFile.exists())
            {
                mFile.mkdir();
            }

            mProgress = (ProgressBar) findViewById(R.id.progress);
            mProgress.setVisibility(View.GONE);

            DisplayMetrics mDisplayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
            setConfig(Constants.PHONE_WIDTH_HEIGHT_SIZE,mDisplayMetrics.widthPixels*mDisplayMetrics.heightPixels);

            try{
                Singleton.preferenceEditor.putInt(Constants.LOG_EVENT_OPEN_TIME, Singleton.getCurrentTimestamp()).commit();
                LogEventUtil.OpenApp(mCtx,mApplication);
            }
            catch (Exception e){
            }
            int open_count = Singleton.preferences.getInt(Constants.OPEN_APP_COUNT, 0);
            Singleton.preferenceEditor.putInt(Constants.OPEN_APP_COUNT, open_count+1).commit();

//            try
//            {
//                Signature[] mSignature = mCtx.getPackageManager().getPackageInfo(mCtx.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
//                for (Signature sig : mSignature)
//                {
//                    Log.d("123","Signature hashcode : " + sig.hashCode());
//                }
//            }
//            catch (Exception e)
//            {
//
//            }

            try
            {
                mApplication.setMenuFinish(false);
//                if(getConfig("about").length()==0 && Singleton.preferences.getString(Constants.USER_ID, "").length()==0)
//                {
//                    Intent intent = new Intent();
//                    intent.setClass(mCtx, AboutAppActivity_V2.class);
//                    startActivity(intent);
//                    mCtx.finish();
//                }
//                else
//                {
                    if(!Singleton.preferences.getString(Constants.USER_ID, "").equals(""))
                    {
                        if(getConfig("notifi").compareTo("1")==0)
                        {
                            try
                            {
                                if(getConfig("page").length()!=0 && getConfig("ID").length()!=0)
                                {
                                    mApplication.setFromBranch(true);
                                    mApplication.setFromPage(getConfig("page"));
                                    mApplication.setFromID(getConfig("ID"));
                                }
                            }
                            catch (Exception e)
                            {
                            }

                            setConfig(this,"notifi","0");
                            setConfig(this,"page","");
                            setConfig(this,"ID","");

                            if(!Constants.INTERNATIONAL_VERSION)
                            {
                                if(BuildConfig.FLAVOR.compareTo("uc")==0 || BuildConfig.FLAVOR.compareTo("sougou")==0 || BuildConfig.FLAVOR.compareTo("lenovo")==0 || BuildConfig.FLAVOR.compareTo("huawei")==0 || BuildConfig.FLAVOR.compareTo("letv")==0)
                                {
                                    try {
                                        mHandler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent intent = new Intent();
                                                intent.setClass(mCtx, MenuActivity.class);
                                                startActivity(intent);
                                                mCtx.finish();
                                            }
                                        }, uc_time);
                                    }
                                    catch (Exception e) {
                                    }
                                }
                                else
                                {
                                    Intent intent = new Intent();
                                    intent.setClass(mCtx, MenuActivity.class);
                                    startActivity(intent);
                                    mCtx.finish();
                                }
                            }
                            else
                            {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, MenuActivity.class);
                                startActivity(intent);
                                mCtx.finish();
                            }
                            return;
                        }

                        try {
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                   public void run() {
                                    try {
                                        if(mState) return;
                                        mState = true;
                                        Intent intent = new Intent();
                                        intent.setClass(mCtx, MenuActivity.class);
                                        startActivity(intent);
                                        mCtx.finish();
                                    }
                                    catch (Exception e){
                                    }
                                }
                            }, 2500);
                        }
                        catch (Exception e) {
                        }

                        loadBranch();
                    }
                    else
                    {

//                        Map<String, String> articleParams = new HashMap<String, String>();
//                        articleParams.put("OpenID", "John Q");
//                        articleParams.put("User_Status", "Registered");
//                        FlurryAgent.logEvent("Article_Read", articleParams);


                        if(!Constants.INTERNATIONAL_VERSION)
                        {
                            if(BuildConfig.FLAVOR.compareTo("uc")==0 || BuildConfig.FLAVOR.compareTo("sougou")==0 || BuildConfig.FLAVOR.compareTo("lenovo")==0 || BuildConfig.FLAVOR.compareTo("huawei")==0 || BuildConfig.FLAVOR.compareTo("letv")==0)
                            {
                                try {
                                    mHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mApplication.setGuestModeState(true);
                                            mApplication.setSignOutState(false);
                                            mApplication.setGuestFinish(false);
                                            Intent intent = new Intent();
                                            intent.setClass(mCtx, GuestActivity.class);
                                            startActivity(intent);
                                            mCtx.finish();
                                        }
                                    }, uc_time);
                                }
                                catch (Exception e) {
                                }
                            }
                            else
                            {
                                mApplication.setGuestModeState(true);
                                mApplication.setSignOutState(false);
                                mApplication.setGuestFinish(false);
                                Intent intent = new Intent();
                                intent.setClass(mCtx, GuestActivity.class);
                                startActivity(intent);
                                mCtx.finish();
                            }
                        }
                        else
                        {
                            mApplication.setGuestModeState(true);
                            mApplication.setSignOutState(false);
                            mApplication.setGuestFinish(false);
                            Intent intent = new Intent();
                            intent.setClass(mCtx, GuestActivity.class);
                            startActivity(intent);
                            mCtx.finish();
                        }
                    }
//                }
            }
            catch(Exception e)
            {
            }
        }
    }

    public String getConfig(String key)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, "");
    }

    public void setConfig(String key, int value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putInt(key, value);
        PE.commit();
    }

    public void setConfig(Context context,String key, String value)
    {
        try
        {
            SharedPreferences settings = context.getSharedPreferences("settings", 0);
            SharedPreferences.Editor PE = settings.edit();
            PE.putString(key, value);
            PE.commit();
        }
        catch (Exception e)
        {
        }
    }

    public Boolean isEmulator()
    {
        Boolean inEmulator = false;
        if(Build.PRODUCT.contains("sdk_google") && Build.MODEL.contains("sdk_google") && Build.BRAND.contains("generic")) inEmulator = true;
        else inEmulator = false;
        return inEmulator;
    }

    private void loadBranch()
    {
        branch = Branch.getInstance();
        branch.initSession(new Branch.BranchReferralInitListener()
        {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error)
            {
                if (error == null)
                {
                    try
                    {
                        if(referringParams.has("page") && referringParams.has("ID"))
                        {
                            mApplication.setFromBranch(true);
                            mApplication.setFromPage(referringParams.getString("page"));
                            mApplication.setFromID(referringParams.getString("ID"));
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }

                if(mState) return;
                mState = true;

                if(!Constants.INTERNATIONAL_VERSION)
                {
                    if(BuildConfig.FLAVOR.compareTo("uc")==0 || BuildConfig.FLAVOR.compareTo("sougou")==0 || BuildConfig.FLAVOR.compareTo("lenovo")==0 || BuildConfig.FLAVOR.compareTo("huawei")==0 || BuildConfig.FLAVOR.compareTo("letv")==0)
                    {
                        try {
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent();
                                    intent.setClass(mCtx, MenuActivity.class);
                                    startActivity(intent);
                                    mCtx.finish();
                                }
                            }, uc_time);
                        }
                        catch (Exception e) {
                        }
                    }
                    else
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, MenuActivity.class);
                        startActivity(intent);
                        mCtx.finish();
                    }
                }
                else
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, MenuActivity.class);
                    startActivity(intent);
                    mCtx.finish();
                }
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent)
    {
        this.setIntent(intent);
    }
}
