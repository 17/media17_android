package com.machipopo.media17;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.dd.plist.PropertyListParser;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

/**
 * Created by POPO on 6/22/15.
 */
public class RevenueUpdateActivity extends BaseNewActivity
{
    private RevenueUpdateActivity mCtx = this;
    private Story17Application mApplication;
    private InputMethodManager inputMethodManager = null;
    private LayoutInflater inflater;

    private String type = "";
    private String title = "";
    private String default_text = "";
    private String bank = "";

    private EditText mEdit;
    private TextView mNum;
    private ListView mList;
    private ProgressBar mProgress;

    private int MAX = 50;
    private String TEXT = "";

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_update_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("type")) type = mBundle.getString("type");
            if(mBundle.containsKey("title")) title = mBundle.getString("title");
            if(mBundle.containsKey("default_text")) default_text = mBundle.getString("default_text");
            if(mBundle.containsKey("bank")) bank = mBundle.getString("bank");
        }

        initTitleBar();

        mEdit = (EditText) findViewById(R.id.edit);
        mNum = (TextView) findViewById(R.id.num);
        mList = (ListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        if(type.compareTo(Constants.REVENUE_NAME)==0)
        {
            mEdit.setVisibility(View.VISIBLE);
            mNum.setVisibility(View.VISIBLE);
            mList.setVisibility(View.GONE);
            mProgress.setVisibility(View.GONE);

            mEdit.setText(default_text);
            MAX = 50;
            mNum.setText(String.valueOf(MAX));
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        else if(type.compareTo(Constants.REVENUE_ID)==0)
        {
            mEdit.setVisibility(View.VISIBLE);
            mNum.setVisibility(View.VISIBLE);
            mList.setVisibility(View.GONE);
            mProgress.setVisibility(View.GONE);

            mEdit.setText(default_text);
            MAX = 50;
            mNum.setText(String.valueOf(MAX));
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        else if(type.compareTo(Constants.REVENUE_BANK_NUMBER)==0)
        {
            mEdit.setVisibility(View.VISIBLE);
            mNum.setVisibility(View.VISIBLE);
            mList.setVisibility(View.GONE);
            mProgress.setVisibility(View.GONE);

            mEdit.setText(default_text);
            MAX = 50;
            mNum.setText(String.valueOf(MAX));
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        else if(type.compareTo(Constants.REVENUE_ACCOUNT_NAME)==0)
        {
            mEdit.setVisibility(View.VISIBLE);
            mNum.setVisibility(View.VISIBLE);
            mList.setVisibility(View.GONE);
            mProgress.setVisibility(View.GONE);

            mEdit.setText(default_text);
            MAX = 50;
            mNum.setText(String.valueOf(MAX));
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        else if(type.compareTo(Constants.REVENUE_BANK_NAME)==0)
        {
            mEdit.setVisibility(View.GONE);
            mNum.setVisibility(View.GONE);
            mList.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

            try
            {
                NSDictionary rootDict = (NSDictionary) PropertyListParser.parse(getAssets().open("bank.plist"));
                final NSObject[] parameters = ((NSArray)rootDict.objectForKey("bankName")).getArray();

                mList.setAdapter(new MyAdapter(parameters));
                mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
                    {
                        mProgress.setVisibility(View.VISIBLE);

                        try
                        {
                            JSONObject params = new JSONObject();
                            params.put("bankName", parameters[i].toString());

                            final String name = parameters[i].toString();

                            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                            {
                                @Override
                                public void onResult(boolean success, String message)
                                {
                                    mProgress.setVisibility(View.GONE);
                                    if(success)
                                    {
                                        setConfig(Constants.REVENUE_BANK_NAME,name);
                                        mCtx.finish();
                                    }
                                    else {
                                        try{
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                    }
                                }
                            });
                        }
                        catch(Exception e)
                        {
                            try{
//                          showToast(getString(R.string.error_failed));
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                            mCtx.finish();
                        }
                    }
                });
            }
            catch(Exception e)
            {
                try{
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
                mCtx.finish();
            }
        }
        else if(type.compareTo(Constants.REVENUE_BANK_BRANCH)==0)
        {
            mEdit.setVisibility(View.GONE);
            mNum.setVisibility(View.GONE);
            mList.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

            try
            {
                NSDictionary rootDict = (NSDictionary) PropertyListParser.parse(getAssets().open("bank.plist"));
                final NSObject[] parameters = ((NSArray)rootDict.objectForKey(bank)).getArray();

                mList.setAdapter(new MyAdapter(parameters));
                mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
                    {
                        mProgress.setVisibility(View.VISIBLE);

                        try
                        {
                            JSONObject params = new JSONObject();
                            params.put("branchName", parameters[i].toString());

                            final String name = parameters[i].toString();

                            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                            {
                                @Override
                                public void onResult(boolean success, String message)
                                {
                                    mProgress.setVisibility(View.GONE);
                                    if(success)
                                    {
                                        setConfig(Constants.REVENUE_BANK_BRANCH,name);
                                        mCtx.finish();
                                    }
                                    else {
                                        try{
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                    }
                                }
                            });
                        }
                        catch(Exception e)
                        {
                            try{
//                          showToast(getString(R.string.error_failed));
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                            mCtx.finish();
                        }
                    }
                });
            }
            catch(Exception e)
            {
                try{
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
                mCtx.finish();
            }
        }

        mEdit.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                if(type.compareTo(Constants.REVENUE_NAME)==0)
                {
                    mNum.setText(String.valueOf(MAX-charSequence.toString().length()));
                    if(charSequence.toString().length()==MAX) TEXT = charSequence.toString();
                    else if(charSequence.toString().length()>MAX) mEdit.setText(TEXT);
                }
                else if(type.compareTo(Constants.REVENUE_ID)==0)
                {
                    mNum.setText(String.valueOf(MAX-charSequence.toString().length()));
                    if(charSequence.toString().length()==MAX) TEXT = charSequence.toString();
                    else if(charSequence.toString().length()>MAX) mEdit.setText(TEXT);
                }
                else if(type.compareTo(Constants.REVENUE_BANK_NUMBER)==0)
                {
                    mNum.setText(String.valueOf(MAX-charSequence.toString().length()));
                    if(charSequence.toString().length()==MAX) TEXT = charSequence.toString();
                    else if(charSequence.toString().length()>MAX) mEdit.setText(TEXT);
                }
                else if(type.compareTo(Constants.REVENUE_ACCOUNT_NAME)==0)
                {
                    mNum.setText(String.valueOf(MAX-charSequence.toString().length()));
                    if(charSequence.toString().length()==MAX) TEXT = charSequence.toString();
                    else if(charSequence.toString().length()>MAX) mEdit.setText(TEXT);
                }
            }

            @Override
            public void afterTextChanged(Editable editable)
            {
            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();

        ViewGroup view = (ViewGroup) mCtx.getWindow().getDecorView();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(title);
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.done));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                if(type.compareTo(Constants.REVENUE_NAME)==0 || type.compareTo(Constants.REVENUE_ID)==0 || type.compareTo(Constants.REVENUE_BANK_NUMBER)==0 || type.compareTo(Constants.REVENUE_ACCOUNT_NAME)==0)
                {
                    if(mEdit.getText().toString().length()==0)
                    {
                        try{
//                          showToast(getString(R.string.edit));
                            Toast.makeText(mCtx, getString(R.string.edit), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                        return;
                    }
                }

                mProgress.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();

                    if(type.compareTo(Constants.REVENUE_NAME)==0) params.put("idCardName", mEdit.getText().toString());
                    else if(type.compareTo(Constants.REVENUE_ID)==0) params.put("idCardNumber", mEdit.getText().toString());
                    else if(type.compareTo(Constants.REVENUE_BANK_NUMBER)==0) params.put("accountNumber", mEdit.getText().toString());
                    else if(type.compareTo(Constants.REVENUE_ACCOUNT_NAME)==0) params.put("beneficiaryName", mEdit.getText().toString());

                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            mProgress.setVisibility(View.GONE);
                            if(success)
                            {
                                if(type.compareTo(Constants.REVENUE_NAME)==0) setConfig(Constants.REVENUE_NAME,mEdit.getText().toString());
                                else if(type.compareTo(Constants.REVENUE_ID)==0) setConfig(Constants.REVENUE_ID,mEdit.getText().toString());
                                else if(type.compareTo(Constants.REVENUE_BANK_NUMBER)==0) setConfig(Constants.REVENUE_BANK_NUMBER,mEdit.getText().toString());
                                else if(type.compareTo(Constants.REVENUE_ACCOUNT_NAME)==0) setConfig(Constants.REVENUE_ACCOUNT_NAME, mEdit.getText().toString());

                                mCtx.finish();
                            }
                            else {
                                try{
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                    });
                }
                catch(Exception e)
                {
                    try{
//                          showToast(getString(R.string.error_failed));
                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                    mCtx.finish();
                }
            }
        });

        if(type.compareTo(Constants.REVENUE_BANK_NAME)==0 || type.compareTo(Constants.REVENUE_BANK_BRANCH)==0)
        {
            btn.setVisibility(View.GONE);
        }
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    private class MyAdapter extends BaseAdapter
    {
        private NSObject[] mArray;

        public MyAdapter(NSObject[] array)
        {
            mArray = array;
        }

        @Override
        public int getCount()
        {
            return mArray.length;
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.only_text_row, null);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.name.setText(mArray[i].toString());

            return convertView;
        }
    }

    private class ViewHolder
    {
        TextView name;
    }
}
