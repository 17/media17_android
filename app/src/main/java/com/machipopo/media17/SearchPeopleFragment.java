package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.fragment.BaseFragment;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by POPO on 6/1/15.
 */
public class SearchPeopleFragment extends BaseFragment
{
    private PullToRefreshListView mList;
    private LayoutInflater inflater;
    private ArrayList<UserModel> mUserModel = new ArrayList<UserModel>();
    private TabAdapter mTabAdapter;

    private String mText = "";

    private InputMethodManager inputMethodManager;
    private DisplayImageOptions SelfOptions;
    private Story17Application mApplication;

    public SearchPeopleFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.search_people, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        mList = (PullToRefreshListView) getView().findViewById(R.id.list);
        mApplication = (Story17Application)getActivity().getApplication();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    public void setSearchText(String text)
    {
        if(text.length()==0)
        {
            mText = "";
            mUserModel.clear();

            mTabAdapter = new TabAdapter();

            if(mList!=null) {
                mList.setAdapter(mTabAdapter);
            }

            return;
        }

        if(mText.compareTo(text)==0)
            return;

        mText = text;
        ApiManager.getSearchUsers(getActivity(), text, 0, 100, 0, new ApiManager.GetSearchUsersCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<UserModel> userModel)
            {
                if (success && userModel!=null)
                {

                    mUserModel.clear();
                    mUserModel.addAll(userModel);

                    mTabAdapter = new TabAdapter();
                    mList.setAdapter(mTabAdapter);

//                    ViewGroup view = (ViewGroup) getActivity().getWindow().getDecorView();
//                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                else
                {
                    try
                    {
                        if(getActivity()!=null)
                        {
                            try{
//                              ((SearchInfoActivity)getActivity()).showToast(getString(R.string.search_failed));
                                Toast.makeText(getActivity(), getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        });
    }

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart("SearchPeopleFragment");
    }

    @Override
    public void onPause()
    {
        super.onPause();

        ViewGroup view = (ViewGroup) getActivity().getWindow().getDecorView();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        MobclickAgent.onPageEnd("SearchPeopleFragment");
    }

    private class TabAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mUserModel.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderUser holder = new ViewHolderUser();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.search_user_row, null);
                holder.user_layout = (LinearLayout) convertView.findViewById(R.id.user_layout);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.follow = (Button) convertView.findViewById(R.id.follow);
                holder.open = (TextView) convertView.findViewById(R.id.open);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderUser) convertView.getTag();

            holder.open.setText(mUserModel.get(position).getOpenID());
            holder.open.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeUserActivity.class);
                    intent.putExtra("title", mUserModel.get(position).getName());
                    intent.putExtra("picture", mUserModel.get(position).getPicture());
                    intent.putExtra("isfollowing", mUserModel.get(position).getIsFollowing());
                    intent.putExtra("post", mUserModel.get(position).getPostCount());
                    intent.putExtra("follow", mUserModel.get(position).getFollowerCount());
                    intent.putExtra("following", mUserModel.get(position).getFollowingCount());
                    intent.putExtra("open", mUserModel.get(position).getOpenID());
                    intent.putExtra("bio", mUserModel.get(position).getBio());
                    intent.putExtra("targetUserID", mUserModel.get(position).getUserID());
                    intent.putExtra("web", mUserModel.get(position).getWebsite());
                    startActivity(intent);
                }
            });

            holder.name.setText(mUserModel.get(position).getName());
            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeUserActivity.class);
                    intent.putExtra("title", mUserModel.get(position).getName());
                    intent.putExtra("picture", mUserModel.get(position).getPicture());
                    intent.putExtra("isfollowing", mUserModel.get(position).getIsFollowing());
                    intent.putExtra("post", mUserModel.get(position).getPostCount());
                    intent.putExtra("follow", mUserModel.get(position).getFollowerCount());
                    intent.putExtra("following", mUserModel.get(position).getFollowingCount());
                    intent.putExtra("open", mUserModel.get(position).getOpenID());
                    intent.putExtra("bio", mUserModel.get(position).getBio());
                    intent.putExtra("targetUserID", mUserModel.get(position).getUserID());
                    intent.putExtra("web", mUserModel.get(position).getWebsite());
                    startActivity(intent);
                }
            });

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUserModel.get(position).getPicture()), holder.img,SelfOptions);
            holder.img.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeUserActivity.class);
                    intent.putExtra("title", mUserModel.get(position).getName());
                    intent.putExtra("picture", mUserModel.get(position).getPicture());
                    intent.putExtra("isfollowing", mUserModel.get(position).getIsFollowing());
                    intent.putExtra("post", mUserModel.get(position).getPostCount());
                    intent.putExtra("follow", mUserModel.get(position).getFollowerCount());
                    intent.putExtra("following", mUserModel.get(position).getFollowingCount());
                    intent.putExtra("open", mUserModel.get(position).getOpenID());
                    intent.putExtra("bio", mUserModel.get(position).getBio());
                    intent.putExtra("targetUserID", mUserModel.get(position).getUserID());
                    intent.putExtra("web", mUserModel.get(position).getWebsite());
                    startActivity(intent);
                }
            });

            final Button btn = holder.follow;
            if(mUserModel.get(position).getIsFollowing()==0)
            {
                if(mUserModel.get(position).getFollowRequestTime()!=0)
                {
                    holder.follow.setText(getString(R.string.private_mode_request_send));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    holder.follow.setText("+ " + getString(R.string.user_profile_follow));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
            }
            else
            {
                holder.follow.setText(getString(R.string.user_profile_following));
                holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
                holder.follow.setTextColor(Color.WHITE);
            }

            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mUserModel.get(position).getIsFollowing()==1)
                    {
                        try {
                            LogEventUtil.UnfollowUser(getActivity(), mApplication, mUserModel.get(position).getUserID());
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mUserModel.get(position).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {
                                    btn.setText("+ " + getString(R.string.user_profile_follow));
                                    btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                } else{
                                    try{
//                              ((SearchInfoActivity)getActivity()).showToast(getString(R.string.error_failed));
                                        Toast.makeText(getActivity(), getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mUserModel.get(position).getFollowRequestTime()!=0)
                        {
                            btn.setText("+ " + getString(R.string.user_profile_follow));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                            mUserModel.get(position).setIsFollowing(0);
                            mUserModel.get(position).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(getActivity(), mUserModel.get(position).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                              ((SearchInfoActivity)getActivity()).showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(getActivity(), getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                                return ;
                            }

                            if(mUserModel.get(position).getPrivacyMode().compareTo("private")==0)
                            {
                                btn.setText(getString(R.string.private_mode_request_send));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                mUserModel.get(position).setIsFollowing(0);
                                mUserModel.get(position).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(getActivity(), mUserModel.get(position).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mUserModel.get(position).setIsFollowing(0);
                                            mUserModel.get(position).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                try{
                                LogEventUtil.FollowUser(getActivity(), mApplication, mUserModel.get(position).getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mUserModel.get(position).getUserID(), new ApiManager.FollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {
                                            btn.setText(getString(R.string.user_profile_following));
                                            btn.setBackgroundResource(R.drawable.btn_green_selector);
                                            btn.setTextColor(Color.WHITE);

                                        } else {
                                            if (!isAdded()) {
                                                return;
                                            }

                                            try{
//                              ((SearchInfoActivity)getActivity()).showToast(getString(R.string.error_failed));
                                                Toast.makeText(getActivity(), getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            if(mUserModel.get(position).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            return convertView;
        }
    };

    private class ViewHolderUser
    {
        LinearLayout user_layout;
        ImageView img;
        TextView name;
        TextView open;
        Button follow;
        ImageView verifie;
    }
}
