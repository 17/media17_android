package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.View.TouchImage;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.LiveFeedModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.machipopo.media17.utils.ShareUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tencent.connect.UserInfo;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;
import com.warnyul.android.widget.FastVideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by dean on 16/1/4.
 */
public class GuestFeedActivity extends BaseActivity
{
    private GuestFeedActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;
    private DisplayMetrics mDisplayMetrics;

    private int mGridHeight;
    private PullToRefreshListView mListView;
    private listAdapter mListAdapter;
    private ArrayList<FeedModel> mFeedModels = new ArrayList<FeedModel>();
    private Boolean isFetchingData = false;
    private Boolean noMoreData = false;

    private Boolean mGuest = false;
    private int mPos = 0;

    private DisplayImageOptions BigOptions, SelfOptions;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());

        if(mApplication!=null){
            if(mApplication.getGuestFinish()) mCtx.finish();
            else if(mApplication.getMenuFinish()) mCtx.finish();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guest_feed_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("guest"))
            {
                mGuest = mBundle.getBoolean("guest");
            }

            if(mBundle.containsKey("pos"))
            {
                mPos = mBundle.getInt("pos");
            }
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        mGridHeight = (int)((float)mDisplayMetrics.widthPixels);

        initTitleBar();

        mListView = (PullToRefreshListView) findViewById(R.id.listView);

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                ApiManager.getHotPost(mCtx, Integer.MAX_VALUE, 18, new ApiManager.GetHotPostsCallback() {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                        mListView.onRefreshComplete();

                        if (success && feedModel != null) {
                            if (feedModel.size() != 0) {
                                mFeedModels.clear();
                                mFeedModels.addAll(feedModel);

                                if (mListAdapter != null) mListAdapter = null;
                                mListAdapter = new listAdapter();
                                mListView.setAdapter(mListAdapter);

                                isFetchingData = false;
                                noMoreData = false;

                                if (mFeedModels.size() < 18) {
                                    noMoreData = true;
                                }
                            }
                        }
                    }
                });
            }
        });

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_FLING) {
                    ImageLoader.getInstance().pause();
                }

                if (scrollState == SCROLL_STATE_IDLE) {
                    ImageLoader.getInstance().resume();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        mFeedModels.addAll(mApplication.getGuestFeed());
        mListAdapter = new listAdapter();
        mListView.setAdapter(mListAdapter);

        if(mFeedModels.size() < 18)
        {
            noMoreData = true;
        }

        mListView.getRefreshableView().setSelection(mPos);
        mListAdapter.notifyDataSetChanged();

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.menu_home));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setVisibility(View.VISIBLE);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            hideKeyboard();
//            Intent intent = new Intent();
//            intent.setClass(mCtx, GuestActivity.class);
//            startActivity(intent);
            mCtx.finish();

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

    private class listAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.titleCell = (LinearLayout) convertView.findViewById(R.id.titleCell);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.restream_open = (Button) convertView.findViewById(R.id.restream_open);

                holder.view_layout = (LinearLayout) convertView.findViewById(R.id.view_layout);
                holder.ad_layout = (LinearLayout) convertView.findViewById(R.id.ad_layout);

                holder.ad_btn = (Button) convertView.findViewById(R.id.ad_btn);
                holder.social = (TextView) convertView.findViewById(R.id.social);
                holder.live_more = (ImageView) convertView.findViewById(R.id.live_more);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.social.setVisibility(View.GONE);
            holder.view_layout.setVisibility(View.VISIBLE);
            holder.ad_layout.setVisibility(View.GONE);
            holder.restream_open.setVisibility(View.GONE);
            holder.live_more.setVisibility(View.GONE);

            if(mFeedModels.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.down_layout.setVisibility(View.VISIBLE);
            holder.live.setVisibility(View.GONE);

            holder.name.setText(mFeedModels.get(position).getUserInfo().getOpenID());
            holder.day.setText(Singleton.getElapsedTimeString(mFeedModels.get(position).getTimestamp()));

            if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.money_text.setVisibility(View.VISIBLE);

                String c = Singleton.getCurrencyType();
                double m = (double) mFeedModels.get(position).getTotalRevenue()*(double)Singleton.getCurrencyRate();
                DecimalFormat df=new DecimalFormat("#.####");
                holder.money_text.setText(df.format(m) + " " + c);
                holder.view_text.setText(String.format(getString(R.string.home_views),String.valueOf(mFeedModels.get(position).getViewCount())));
                holder.view_text.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.money_text.setVisibility(View.GONE);
                holder.view_text.setVisibility(View.GONE);
            }

            if(mFeedModels.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mFeedModels.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.GONE);

            holder.like_text.setText(String.format(getString(R.string.home_like),String.valueOf(mFeedModels.get(position).getLikeCount())));
            holder.comment_text.setText(String.format(getString(R.string.home_comment), String.valueOf(mFeedModels.get(position).getCommentCount())));

            final String thisUserId = mFeedModels.get(position).getUserID();

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.like_text.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        if(mGuest)
                        {
                            showGuestLogin();

                            return;
                        }
                    }
                });
            }
            else
            {
                holder.like_text.setOnClickListener(null);
            }

            holder.comment_text.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }
                }
            });

            if(mFeedModels.get(position).getLiked()==0)
            {
                holder.btn_like.setImageResource(R.drawable.btn_like_selector);
            }
            else holder.btn_like.setImageResource(R.drawable.like_down);

            final TextView likeText = holder.like_text;
            final ImageView like = holder.btn_like;
            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_like.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        if(mGuest)
                        {
                            showGuestLogin();

                            return;
                        }
                    }
                });
                holder.btn_like.setOnTouchListener(null);
            }
            else
            {
                holder.btn_like.setOnClickListener(null);
                holder.btn_like.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {
                        if (event.getAction() == MotionEvent.ACTION_UP)
                        {
                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }
                        }

                        return true;
                    }
                });
            }

            holder.btn_comment.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }
                }
            });

            holder.btn_more.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }
                }
            });


            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getUserInfo().getPicture()), holder.self, SelfOptions);
            holder.titleCell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFeedModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFeedModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            holder.photo.setOnClickListener(null);
            if(mFeedModels.get(position).getType().compareTo("image")==0)
            {
                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.GONE);
                holder.vcon.setVisibility(View.GONE);

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedModels.get(position).getPicture()), holder.photo,BigOptions);
                holder.photo.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }
                        }
                        return true;
                    }
                });
            }
            else
            {
                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.VISIBLE);
                holder.vcon.setVisibility(View.VISIBLE);

                holder.video.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.video.getLayoutParams().height = mDisplayMetrics.widthPixels ;
                try {
                    holder.video.setVideoPath(Singleton.getS3FileUrl(mFeedModels.get(position).getVideo()));
                }
                catch (Exception e) {
                }

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedModels.get(position).getPicture()), holder.photo,BigOptions);
                final ImageView mPhotoV = holder.photo;
                holder.video.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer)
                    {
                        mPhotoV.setVisibility(View.GONE);
                    }
                });

                final FastVideoView mVideoV = holder.video;
                holder.video.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
                {
                    public void onCompletion(MediaPlayer mp)
                    {
                        mVideoV.start();
                    }
                });

                holder.video.start();

                holder.video.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }
                        }

                        return true;
                    }
                });
            }

            holder.dio.linkify(new FeedTagActionHandler()
            {
                @Override
                public void handleHashtag(String hashtag)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }
                }

                @Override
                public void handleMention(String mention)
                {
                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, UserModel user)
                        {
                            if (success && user != null)
                            {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                intent.putExtra("guest", mGuest);
                                startActivity(intent);
//                                mCtx.finish();
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email)
                {

                }

                @Override
                public void handleUrl(String url)
                {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    };

    private class ViewHolder
    {
        LinearLayout titleCell;

        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
        Button restream_open;
        LinearLayout view_layout;
        LinearLayout ad_layout;

        Button ad_btn;
        TextView social;

        ImageView live_more;
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    public void LoadData(final boolean refresh)
    {
        if(isFetchingData)
        {
            return;
        }

        if(refresh)
        {
            noMoreData = false;
        }

        if(noMoreData)
        {
            return;
        }

        isFetchingData = true;

        ApiManager.getHotPost(mCtx, mFeedModels.get(mFeedModels.size() - 1).getTimestamp(), 18, new ApiManager.GetHotPostsCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
            {
                isFetchingData = false;

                if (success && feedModel!=null)
                {
                    if (feedModel.size() != 0)
                    {
                        mFeedModels.addAll(feedModel);

                        if (feedModel.size() < 18)
                        {
                            noMoreData = true;
                        }
                        else noMoreData = false;

                        if (mListAdapter != null)
                        {
                            mListAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    try{
                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    Dialog GuestDialog;
    private SharedPreferences sharedPreferences;
    private CallbackManager callbackManager;
    private void showGuestLogin()
    {
        if(GuestDialog!=null) GuestDialog = null;

        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        GuestDialog.setContentView(R.layout.guest_login_dialog);
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        Button mSingup = (Button)GuestDialog.findViewById(R.id.singup);
        mSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();
                dialog_singup_view();
            }
        });

        Button mLogin = (Button)GuestDialog.findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(mCtx,LoginActivity_V3.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.btnFBLogin);
        mFB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        GuestDialog.show();
    }

    private void dialog_singup_view()
    {
        if(GuestDialog!=null) GuestDialog = null;
        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        if(Constants.INTERNATIONAL_VERSION){
            GuestDialog.setContentView(R.layout.guest_singup_dialog_google);
        }
        else{
            GuestDialog.setContentView(R.layout.guest_singup_dialog_china);
        }
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout mWeibo = (LinearLayout)GuestDialog.findViewById(R.id.weibo_layout);
        mWeibo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupWeibo";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                showWebio();
            }
        });

        LinearLayout mQQ = (LinearLayout)GuestDialog.findViewById(R.id.qq_layout);
        mQQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupQQ";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                mTencent = Tencent.createInstance(Constants.QQAppid, mCtx);
                mTencent.login(mCtx, "all", loginListener);
            }
        });

        LinearLayout mWechat = (LinearLayout)GuestDialog.findViewById(R.id.wechat_layout);
        mWechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                if (ShareUtil.isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI"))
                {
                //Umeng monitor
                String Umeng_id="GuestModeSignupWechat";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                IWXAPI api;
                api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
                api.registerApp(Constants.WECHAT_APP_KEY);
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "wechat_sdk_demo";
                api.sendReq(req);
                }
                else {
                    Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                }
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.fb_layout);
        mFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                Intent intent;

                //Umeng monitor
                String Umeng_id_FB="FBlogin";
                HashMap<String,String> mHashMap_FB = new HashMap<String,String>();
                mHashMap_FB.put(Umeng_id_FB, Umeng_id_FB);
                MobclickAgent.onEventValue(mCtx, Umeng_id_FB, mHashMap_FB, 0);

                FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(final LoginResult loginResult) {
                                final String token = loginResult.getAccessToken().getToken();

                                GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject user, GraphResponse response) {
                                        if (user != null) {
                                            try {
                                                showProgressDialog();
                                                final String id = user.optString("id");
                                                final String name = user.optString("name");
//                                                    Log.e("DDD", "ID: " + id);

                                                // Download Profile photo
                                                DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, id);
                                                downloadFBProfileImage.execute();

                                                ApiManager.checkFacebookIDAvailable(mCtx, id, new ApiManager.checkFacebookIDAvailableCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String message) {
                                                        if (success) {

                                                            // Download user profile image
//                                                                DownloadFBProfileImage download = new DownloadFBProfileImage(mCtx, id);
//                                                                download.execute();

                                                            //Signup
                                                            GraphRequest request = GraphRequest.newMeRequest(
                                                                    loginResult.getAccessToken(),
                                                                    new GraphRequest.GraphJSONObjectCallback() {
                                                                        @Override
                                                                        public void onCompleted(
                                                                                JSONObject object,
                                                                                GraphResponse response) {
                                                                            // Application code
                                                                            String email = object.optString("email");
//                                                                                Log.e("DDD", "EMAIL: " + email);


                                                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, id).commit();
                                                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                            sharedPreferences.edit().putString("signup_type", "fb").commit();
                                                                            sharedPreferences.edit().putString("signup_token", token).commit();

                                                                            if (null != email) {
                                                                                if (email.contains("@")) {
                                                                                    String username = email.substring(0, email.indexOf("@"));
                                                                                    sharedPreferences.edit().putString(Constants.EMAIL, email).commit();
                                                                                    sharedPreferences.edit().putString(Constants.NAME, username).commit();
                                                                                }
                                                                            }
                                                                            hideProgressDialog();
                                                                            startActivity(new Intent(mCtx, SignupActivityV2.class));
//                                                                            mCtx.finish();
                                                                        }
                                                                    });

                                                            Bundle parameters = new Bundle();
                                                            parameters.putString("fields", "email");
                                                            request.setParameters(parameters);
                                                            request.executeAsync();

                                                        } else {
                                                            //login directly
                                                            ApiManager.loginAction2(mCtx, "", "", id, new ApiManager.LoginAction2Callback() {
                                                                @Override
                                                                public void onResult(boolean success, String message, UserModel user) {
                                                                    hideProgressDialog();

                                                                    if (success) {
                                                                        if (message.equals("ok")) {

                                                                            Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                            mStory17Application.setUser(user);

                                                                            Intent intent = new Intent();
                                                                            intent.setClass(mCtx, MenuActivity.class);
                                                                            startActivity(intent);
                                                                            mCtx.finish();
                                                                        } else if (message.equals("freezed"))
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                        else
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                    } else {
                                                                        showNetworkUnstableToast();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });


                                            } catch (Exception e) {
                                                hideProgressDialog();
                                                try {
//                              showToast(getString(R.string.failed));
                                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                } catch (Exception x) {
                                                }
                                            }
                                        }
                                    }
                                }).executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        });
            }
        });

        LinearLayout mMessage = (LinearLayout)GuestDialog.findViewById(R.id.messag_layout);
        mMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupSMS";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                sharedPreferences.edit().putString("signup_type", "message").commit();
                sharedPreferences.edit().putString("signup_token", "").commit();
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(mCtx, SignupActivityV2.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        Button mEnd = (Button)GuestDialog.findViewById(R.id.end);
        mEnd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();

            }
        });

        GuestDialog.show();
    }

    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        private final String fbImageDownlaodUrl = "https://graph.facebook.com/";
        private final String attrs = "/picture?width=1080&height=1080";
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

            this.url = fbImageDownlaodUrl + userID + attrs;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {

        }

    }

    private Dialog mWeiboDialog;
    private void showWebio()
    {
        if(mWeiboDialog!=null) mWeiboDialog = null;
        mWeiboDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        mWeiboDialog.setContentView(R.layout.instagram_dialog);
        final WebView mWeb = (WebView) mWeiboDialog.findViewById(R.id.web);
        final ProgressBar mProgress = (ProgressBar) mWeiboDialog.findViewById(R.id.progress);
        mWeb.getSettings().setJavaScriptEnabled(true);
        mWeiboDialog.setCancelable(true);
        mWeb.loadUrl("https://api.weibo.com/oauth2/authorize?client_id=" + Constants.WEIBO_APP_KEY + "&response_type=code&redirect_uri=" + Constants.WEIBO_REDIRECT_URL);
        mWeb.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                super.onPageStarted(view, url, favicon);
                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);

                if(url.contains(Constants.IG_REDIRECT_URI) && url.contains("code="))
                {
                    mWeiboDialog.dismiss();
                    showProgressDialog();

                    int pos = url.indexOf("=") + 1;
                    String token = url.substring(pos,url.length());
                    try
                    {
                        OkHttpClient mClient = new OkHttpClient();

                        RequestBody body = new FormEncodingBuilder()
                                .add("client_id", Constants.WEIBO_APP_KEY)
                                .add("client_secret", Constants.WEIBO_SECRET)
                                .add("grant_type", "authorization_code")
                                .add("code", token)
                                .add("redirect_uri", Constants.WEIBO_REDIRECT_URL)
                                .build();

                        Request request = new Request.Builder().url("https://api.weibo.com/oauth2/access_token").post(body).build();
                        mClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful())
                                {
                                    try
                                    {
                                        JSONObject data = new JSONObject(response.body().string());
                                        final String token = data.getString("access_token");
                                        final String uid = data.getString("uid");

                                        OkHttpClient client = new OkHttpClient();
                                        Request request = new Request.Builder().url("https://api.weibo.com/2/users/show.json?access_token=" + token + "&uid=" + uid).build();
                                        client.newCall(request).enqueue(new Callback()
                                        {
                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                hideProgressDialog();
                                                showToast(getString(R.string.failed));
                                            }

                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if(response.isSuccessful())
                                                {
                                                    try
                                                    {
                                                        JSONObject info = new JSONObject(response.body().string());
                                                        final int id = info.getInt("id");
                                                        final String screen_name = info.getString("screen_name");
                                                        final String pic = info.getString("profile_image_url");

                                                        DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                                        downloadFBProfileImage.execute();

                                                        ApiManager.checkWeiboIDAvailable(mCtx, uid/*String.valueOf(id)*/, new ApiManager.checkFacebookIDAvailableCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                if (success) {
                                                                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, String.valueOf(id)).commit();
                                                                    sharedPreferences.edit().putString(Constants.FULL_NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putString(Constants.NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                    sharedPreferences.edit().putString("is_weibo", "weibo").commit();

                                                                    sharedPreferences.edit().putString("signup_type", "weibo").commit();
                                                                    sharedPreferences.edit().putString("signup_token", token).commit();

                                                                    hideProgressDialog();
                                                                    Intent intent = new Intent();
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                    intent.setClass(mCtx, SignupActivityV2.class);
                                                                    startActivity(intent);
//                                                                    mCtx.finish();
                                                                } else {
                                                                    ApiManager.loginActionChina(mCtx, "", "", "weiboID", uid, new ApiManager.LoginAction2Callback() {
                                                                        @Override
                                                                        public void onResult(boolean success, String message, UserModel user) {
                                                                            hideProgressDialog();

                                                                            if (success) {
                                                                                if (message.equals("ok")) {

                                                                                    mApplication.setUser(user);

                                                                                    Intent intent = new Intent();
                                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                                    intent.setClass(mCtx, MenuActivity.class);
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                } else if (message.equals("freezed"))
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                                else
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                            } else {
                                                                                showNetworkUnstableToast();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                    catch (JSONException e)
                                                    {
                                                        hideProgressDialog();
                                                        showToast(getString(R.string.failed));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    catch (JSONException e)
                                    {
                                        hideProgressDialog();
                                        showToast(getString(R.string.failed));
                                    }
                                }
                                else
                                {
                                    hideProgressDialog();
                                    showToast(getString(R.string.failed));
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        hideProgressDialog();
                        showToast(getString(R.string.failed));
                    }
                }
                else
                {
//                            mWeiboDialog.dismiss();
//                            hideProgressDialog();
//                            showToast(getString(R.string.failed));
                }
            }
        });
        try{
            mWeb.clearCache(true);
            mWeb.getSettings().setCacheMode(WebView.PERSISTENT_NO_CACHE);
        }
        catch (Exception e){
        }
        mWeiboDialog.show();
    }

    //QQ-------

    public static Tencent mTencent;

    private class BaseUiListener implements IUiListener
    {

        @Override
        public void onComplete(Object response) {
            if (null == response) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            if (null != jsonResponse && jsonResponse.length() == 0) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }

            doComplete((JSONObject)response);
        }

        protected void doComplete(JSONObject values) {

        }

        @Override
        public void onError(UiError e) {
            hideProgressDialog();
            showToast(getString(R.string.failed));
        }

        @Override
        public void onCancel() {
            hideProgressDialog();
        }
    }

    private IUiListener loginListener = new BaseUiListener()
    {
        @Override
        protected void doComplete(JSONObject jsonObject)
        {
            try
            {
                final String token = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_ACCESS_TOKEN);
                String expires = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_EXPIRES_IN);
                final String openId = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_OPEN_ID);

                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);

                if (mTencent != null && mTencent.isSessionValid())
                {
                    IUiListener listener = new IUiListener()
                    {
                        @Override
                        public void onError(UiError e) {
                            hideProgressDialog();
                            showToast(getString(R.string.failed));
                        }

                        @Override
                        public void onComplete(final Object response)
                        {
                            try
                            {
                                JSONObject json = (JSONObject)response;

                                final String name = json.getString("nickname");
                                String pic = json.getString("figureurl_qq_1");

                                DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                downloadFBProfileImage.execute();

                                ApiManager.checkQQIDAvailable(mCtx, openId, new ApiManager.checkFacebookIDAvailableCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, openId).commit();
                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                            sharedPreferences.edit().putString(Constants.NAME, name).commit();
                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();
                                            sharedPreferences.edit().putString("signup_type", "qq").commit();
                                            sharedPreferences.edit().putString("signup_token", token).commit();
                                            hideProgressDialog();
                                            Intent intent = new Intent();
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setClass(mCtx, SignupActivityV2.class);
                                            startActivity(intent);
//                                            mCtx.finish();
                                        }
                                        else
                                        {
                                            ApiManager.loginActionChina(mCtx, "", "", "qqID", openId, new ApiManager.LoginAction2Callback() {
                                                @Override
                                                public void onResult(boolean success, String message, UserModel user) {
                                                    hideProgressDialog();

                                                    if (success) {
                                                        if (message.equals("ok")) {

                                                            mApplication.setUser(user);

                                                            Intent intent = new Intent();
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            intent.setClass(mCtx, MenuActivity.class);
                                                            startActivity(intent);
                                                            mCtx.finish();
                                                        } else if (message.equals("freezed"))
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                        else
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                    } else {
                                                        showNetworkUnstableToast();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }
                        }

                        @Override
                        public void onCancel() {
                            hideProgressDialog();
                        }
                    };

                    UserInfo mInfo = new UserInfo(mCtx, mTencent.getQQToken());
                    mInfo.getUserInfo(listener);
                }
            }
            catch(Exception e) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
            }
        }
    };

    class DownloadProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        String url = "";

        public DownloadProfileImage(Context context, String userID) {
            this.context = context;

            this.url = userID.substring(0,userID.length()-2) + 640;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {
//                    java.net.URL url = new java.net.URL(this.url);
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setDoInput(true);
//                    connection.connect();

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if(mTencent!=null) mTencent.logout(mCtx);
        }
        catch (Exception e) {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == com.tencent.connect.common.Constants.REQUEST_LOGIN || requestCode == com.tencent.connect.common.Constants.REQUEST_APPBAR)
        {
            Tencent.onActivityResultData(requestCode, resultCode, data, loginListener);
        }

        try {
            if(callbackManager!=null) callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        catch (Exception e) {
        }
    }
}
