package com.machipopo.media17;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.View.TouchImage;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.warnyul.android.widget.FastVideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by POPO on 7/25/15.
 */
public class NowLiveStreamActivity extends BaseNewActivity
{
    private Story17Application mApplication;
    private ViewPager mViewPager;
    private View mViewHot, mViewLatest;//mViewSuggest,
    private List<View> mViewList;

    private TextView mTab1,mTab3;//mTab2,

    private NowLiveStreamActivity mCtx = this;
    private LayoutInflater inflater;
    private DisplayMetrics mDisplayMetrics;

    private PullToRefreshListView mListViewHot;
    private HotListAdapter mListAdapterHot;
    private ProgressBar mProgressHot;
    private ImageView mNoDataHot;
    private Boolean isFetchingDataHot = false;
    private Boolean noMoreDataHot = false;
    public ArrayList<LiveModel> mLiveHotModels = new ArrayList<LiveModel>();

//    private PullToRefreshListView mListViewSuggest;
//    private SuggestListAdapter mListAdapterSuggest;
//    private ProgressBar mProgressSuggest;
//    private ImageView mNoDataSuggest;
//    private Boolean isFetchingDataSuggest = false;
//    private Boolean noMoreDataSuggest = false;
//    public ArrayList<LiveModel> mLiveSuggestModels = new ArrayList<LiveModel>();

    private PullToRefreshListView mListViewLatest;
    private LatestListAdapter mListAdapterLatest;
    private ProgressBar mProgressLatest;
    private ImageView mNoDataLatest;
    private Boolean isFetchingDataLatest = false;
    private Boolean noMoreDataLatest = false;
    public ArrayList<LiveModel> mLiveLatestModels = new ArrayList<LiveModel>();

    private DisplayImageOptions BigOptions,SmaleSelfOptions;
    private String mUserId = "";
    private ImageView mImgRight;
    private Button mBtnRight;
    private PagerBaseAdapter mPagerAdapter;

    //event tracking
    private int previousPage = 0;
    private int hotPageStart=0;
    private int latestPageStart=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.now_livestream_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("user_id")) mUserId = mBundle.getString("user_id");
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        if(mApplication.getNowRegion().length()==0) mApplication.setNowRegion(getString(R.string.local));

        initTitleBar();

        LayoutInflater lf = mCtx.getLayoutInflater().from(mCtx);
        mViewHot = lf.inflate(R.layout.notifi_follow, null);
//        mViewSuggest = lf.inflate(R.layout.notifi_follow, null);
        mViewLatest = lf.inflate(R.layout.notifi_follow, null);

        mViewList = new ArrayList<View>();
        mViewList.add(mViewHot);
//        mViewList.add(mViewSuggest);
        mViewList.add(mViewLatest);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mPagerAdapter = new PagerBaseAdapter();
        mViewPager.setAdapter(mPagerAdapter);

        SmartTabLayout mPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        mPagerTab.setViewPager(mViewPager);
        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                //when switching between hot and latest tab, we can get event here 
                switch (position)
                {
                    case 0 :
                        //event tracking
                        hotPageStart = Singleton.getCurrentTimestamp();
                        try{
                            LogEventUtil.EnterHotLiveTrendingPage(mCtx,mApplication);
                            LogEventUtil.LeaveHotLiveLatestPage(mCtx, mApplication, hotPageStart - latestPageStart, mLiveLatestModels.size());
                        }catch (Exception x)
                        {

                        }

                        mTab1.setTextColor(getResources().getColor(R.color.main_color));
//                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));

                        previousPage = position;

                        break;
//                    case 1 :
//                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab2.setTextColor(getResources().getColor(R.color.main_color));
//                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        break;
                    case 1 :
                        latestPageStart = Singleton.getCurrentTimestamp();
                        try{
                            LogEventUtil.EnterHotLiveLatestPage(mCtx,mApplication);
                            LogEventUtil.LeaveHotLiveTrendingPage(mCtx,mApplication,latestPageStart-hotPageStart,mLiveHotModels.size());
                        }catch (Exception x)
                        {

                        }

                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab3.setTextColor(getResources().getColor(R.color.main_color));

                        previousPage = position;
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        mTab1 = (TextView) findViewById(R.id.tab1);
        mTab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0);
            }
        });
//        mTab2 = (TextView) findViewById(R.id.tab2);
//        mTab2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mViewPager.setCurrentItem(1);
//            }
//        });
        mTab3 = (TextView) findViewById(R.id.tab3);
        mTab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(2);
            }
        });



//        mListViewHot = (PullToRefreshListView) findViewById(R.id.list);
//        mProgressHot = (ProgressBar) findViewById(R.id.progress);
//        mNoDataHot = (ImageView) findViewById(R.id.nodata);
//
//        mProgressHot.setVisibility(View.VISIBLE);
//        mListViewHot.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
//        {
//            @Override
//            public void onRefresh(PullToRefreshBase<ListView> refreshView)
//            {
//                ApiManager.getHotLiveStreams(mCtx, mUserId, new ApiManager.GetHotLiveStreamsCallback()
//                {
//                    @Override
//                    public void onResult(boolean success,  String message, ArrayList<LiveModel> liveModels)
//                    {
//                        mListViewHot.onRefreshComplete();
//                        if (success && liveModels != null)
//                        {
//                            if (liveModels.size() != 0)
//                            {
//                                mNoDataHot.setVisibility(View.GONE);
//                                mLiveHotModels.clear();
//                                mLiveHotModels.addAll(liveModels);
//
//                                if(mListAdapterHot!=null) mListAdapterHot = null;
//                                mListAdapterHot = new HotListAdapter();
//                                mListViewHot.setAdapter(mListAdapterHot);
//                            }
//                            else
//                            {
//                                mLiveHotModels.clear();
//                                if(mListAdapterHot!=null) mListAdapterHot = null;
//                                mListAdapterHot = new HotListAdapter();
//                                mListViewHot.setAdapter(mListAdapterHot);
//                                mNoDataHot.setVisibility(View.VISIBLE);
//                            }
//                        }
//                        else
//                        {
//                            mLiveHotModels.clear();
//                            if(mListAdapterHot!=null) mListAdapterHot = null;
//                            mListAdapterHot = new HotListAdapter();
//                            mListViewHot.setAdapter(mListAdapterHot);
//                            mNoDataHot.setVisibility(View.VISIBLE);
//                        }
//                    }
//                });
//            }
//        });
//
//        ApiManager.getHotLiveStreams(mCtx, mUserId, new ApiManager.GetHotLiveStreamsCallback()
//        {
//            @Override
//            public void onResult(boolean success,  String message, ArrayList<LiveModel> liveModels)
//            {
//                mProgressHot.setVisibility(View.GONE);
//                if (success && liveModels != null)
//                {
//                    if (liveModels.size() != 0)
//                    {
//                        mLiveHotModels.clear();
//                        mLiveHotModels.addAll(liveModels);
//
//                        mListAdapterHot = new HotListAdapter();
//                        mListViewHot.setAdapter(mListAdapterHot);
//                    }
//                    else
//                    {
//                        mNoDataHot.setVisibility(View.VISIBLE);
//                    }
//                }
//                else
//                {
//                    mNoDataHot.setVisibility(View.VISIBLE);
//                }
//            }
//        });

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SmaleSelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_c)
        .showImageForEmptyUri(R.drawable.placehold_c)
        .showImageOnFail(R.drawable.placehold_c)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.search_hot_live_title));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });

        mImgRight = (ImageView) findViewById(R.id.img_right);
        mBtnRight = (Button) findViewById(R.id.btn_right);

        mImgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mCtx,LiveHotCountryActivity.class);
                startActivity(intent);
            }
        });

        mBtnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mCtx, LiveHotCountryActivity.class);
                startActivity(intent);
            }
        });

        if(Constants.INTERNATIONAL_VERSION) {
            if(mApplication.getNowRegion().compareTo(getString(R.string.local))==0)
            {
                mImgRight.setImageResource(R.drawable.local);
                mImgRight.setVisibility(View.VISIBLE);
                mBtnRight.setVisibility(View.GONE);
            }
            else if(mApplication.getNowRegion().compareTo(getString(R.string.global))==0)
            {
                mImgRight.setImageResource(R.drawable.globa);
                mImgRight.setVisibility(View.VISIBLE);
                mBtnRight.setVisibility(View.GONE);
            }
            else
            {
                mImgRight.setVisibility(View.GONE);
                mBtnRight.setVisibility(View.VISIBLE);
                mBtnRight.setText(mApplication.getNowRegion());
            }
        }
        else {
            mImgRight.setVisibility(View.GONE);
            mBtnRight.setVisibility(View.GONE);
        }
    }

    private class PagerBaseAdapter extends PagerAdapter
    {
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            return mViewList.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mViewList.get(position));
        }

        @Override
        public int getItemPosition(Object object)
        {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            if(position==0)
            {
                container.addView(mViewList.get(position));

                //event tracking to record first timestamp of first entering hot page
                // when entering the activity, will start from hotpage, so we start getting timestampe here
                hotPageStart = Singleton.getCurrentTimestamp();
                try{
                    LogEventUtil.EnterHotLiveTrendingPage(mCtx,mApplication);
                }catch (Exception x)
                {

                }

                View view = mViewList.get(position);

                mNoDataHot = (ImageView) view.findViewById(R.id.nodata);
                mProgressHot = (ProgressBar) view.findViewById(R.id.progress);
                mListViewHot = (PullToRefreshListView) view.findViewById(R.id.list);

                mListViewHot.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> refreshView)
                    {
                        String regin = "";
                        if(Constants.INTERNATIONAL_VERSION){
                            if(mApplication.getNowRegion().compareTo(getString(R.string.global))==0) regin = "global";
                            else if(mApplication.getNowRegion().compareTo(getString(R.string.local))==0) regin = "local";
                            else regin = mApplication.getNowRegion();
                        }
                        else{
                            regin = "global";
                        }

                        ApiManager.getHotLiveStreams2(mCtx, regin,Integer.MAX_VALUE, 15, new ApiManager.GetHotLiveStreamsCallback() {
                            @Override
                            public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
                                mListViewHot.onRefreshComplete();

                                if (success && liveModels != null) {
                                    if (liveModels.size() != 0) {
                                        mNoDataHot.setVisibility(View.GONE);
                                        mLiveHotModels.clear();
                                        mLiveHotModels.addAll(liveModels);

                                        if (mListAdapterHot != null) mListAdapterHot = null;
                                        mListAdapterHot = new HotListAdapter();
                                        mListViewHot.setAdapter(mListAdapterHot);

                                        isFetchingDataHot = false;
                                        noMoreDataHot = false;

                                        if (liveModels.size() < 15) {
                                            noMoreDataHot = true;
                                        }
                                    } else {
                                        mNoDataHot.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    mNoDataHot.setVisibility(View.VISIBLE);
                                    try{
//                                      showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                });

                mListViewHot.setOnScrollListener(new AbsListView.OnScrollListener()
                {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState)
                    {
                        if (scrollState == SCROLL_STATE_FLING) {
                            ImageLoader.getInstance().pause();
                        }

                        if (scrollState == SCROLL_STATE_IDLE) {
                            ImageLoader.getInstance().resume();
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });

                if(mLiveHotModels.size()==0)
                {
                    mProgressHot.setVisibility(View.VISIBLE);

                    String regin = "";

                    if(Constants.INTERNATIONAL_VERSION){
                        if(mApplication.getNowRegion().compareTo(getString(R.string.global))==0) regin = "global";
                        else if(mApplication.getNowRegion().compareTo(getString(R.string.local))==0) regin = "local";
                        else regin = mApplication.getNowRegion();
                    }
                    else{
                        regin = "global";
                    }

                    ApiManager.getHotLiveStreams2(mCtx, regin,Integer.MAX_VALUE, 15, new ApiManager.GetHotLiveStreamsCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
                            mProgressHot.setVisibility(View.GONE);

                            if (success && liveModels != null) {
                                if (liveModels.size() != 0) {
                                    mNoDataHot.setVisibility(View.GONE);
                                    mLiveHotModels.clear();
                                    mLiveHotModels.addAll(liveModels);

                                    mListAdapterHot = new HotListAdapter();
                                    mListViewHot.setAdapter(mListAdapterHot);

                                    if (liveModels.size() < 15) {
                                        noMoreDataHot = true;
                                    }
                                } else {
                                    mNoDataHot.setVisibility(View.VISIBLE);
                                }
                            } else {
                                try {
                                    mNoDataHot.setVisibility(View.VISIBLE);
                                    try{
//                                      showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }
                    });
                }
                else mProgressHot.setVisibility(View.GONE);

                return mViewList.get(position);
            }
//            else if(position==1)
//            {
//                container.addView(mViewList.get(position));
//
//                View view = mViewList.get(position);
//
//                mNoDataSuggest = (ImageView) view.findViewById(R.id.nodata);
//                mProgressSuggest = (ProgressBar) view.findViewById(R.id.progress);
//                mListViewSuggest = (PullToRefreshListView) view.findViewById(R.id.list);
//
//                mListViewSuggest.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
//                {
//                    @Override
//                    public void onRefresh(PullToRefreshBase<ListView> refreshView)
//                    {
//                        String regin = "";
//                        if(mApplication.getNowRegion().compareTo(getString(R.string.global))==0) regin = "global";
//                        else if(mApplication.getNowRegion().compareTo(getString(R.string.local))==0) regin = "local";
//                        else regin = mApplication.getNowRegion();
//
//                        ApiManager.getSuggestedLiveStreams(mCtx, regin, Integer.MAX_VALUE, 15, new ApiManager.GetHotLiveStreamsCallback() {
//                            @Override
//                            public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
//                                mListViewSuggest.onRefreshComplete();
//
//                                if (success && liveModels != null) {
//                                    if (liveModels.size() != 0) {
//                                        mNoDataSuggest.setVisibility(View.GONE);
//                                        mLiveSuggestModels.clear();
//                                        mLiveSuggestModels.addAll(liveModels);
//
//                                        if (mListAdapterSuggest != null) mListAdapterSuggest = null;
//                                        mListAdapterSuggest = new SuggestListAdapter();
//                                        mListViewSuggest.setAdapter(mListAdapterSuggest);
//
//                                        isFetchingDataSuggest = false;
//                                        noMoreDataSuggest = false;
//
//                                        if (liveModels.size() < 15) {
//                                            noMoreDataSuggest = true;
//                                        }
//                                    } else {
//                                        mNoDataSuggest.setVisibility(View.VISIBLE);
//                                    }
//                                } else {
//                                    mNoDataSuggest.setVisibility(View.VISIBLE);
//                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        });
//                    }
//                });
//
//                mListViewSuggest.setOnScrollListener(new AbsListView.OnScrollListener()
//                {
//                    @Override
//                    public void onScrollStateChanged(AbsListView view, int scrollState)
//                    {
//                        if (scrollState == SCROLL_STATE_FLING) {
//                            ImageLoader.getInstance().pause();
//                        }
//
//                        if (scrollState == SCROLL_STATE_IDLE) {
//                            ImageLoader.getInstance().resume();
//                        }
//                    }
//
//                    @Override
//                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//
//                    }
//                });
//
//                if(mLiveSuggestModels.size()==0)
//                {
//                    mProgressSuggest.setVisibility(View.VISIBLE);
//
//                    String regin = "";
//                    if(mApplication.getNowRegion().compareTo(getString(R.string.global))==0) regin = "global";
//                    else if(mApplication.getNowRegion().compareTo(getString(R.string.local))==0) regin = "local";
//                    else regin = mApplication.getNowRegion();
//
//                    ApiManager.getSuggestedLiveStreams(mCtx, regin, Integer.MAX_VALUE, 15, new ApiManager.GetHotLiveStreamsCallback() {
//                        @Override
//                        public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
//                            mProgressSuggest.setVisibility(View.GONE);
//
//                            if (success && liveModels != null) {
//                                if (liveModels.size() != 0) {
//                                    mNoDataSuggest.setVisibility(View.GONE);
//                                    mLiveSuggestModels.clear();
//                                    mLiveSuggestModels.addAll(liveModels);
//                                    mListAdapterSuggest = new SuggestListAdapter();
//                                    mListViewSuggest.setAdapter(mListAdapterSuggest);
//
//                                    if (liveModels.size() < 15) {
//                                        noMoreDataSuggest = true;
//                                    }
//                                } else {
//                                    mNoDataSuggest.setVisibility(View.VISIBLE);
//                                }
//                            } else {
//                                try {
//                                    mNoDataSuggest.setVisibility(View.VISIBLE);
//                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
//                                } catch (Exception e) {
//
//                                }
//                            }
//                        }
//                    });
//                }
//                else mProgressSuggest.setVisibility(View.GONE);
//
//                return mViewList.get(position);
//            }
            else
            {
                container.addView(mViewList.get(position));

                View view = mViewList.get(position);

                mNoDataLatest = (ImageView) view.findViewById(R.id.nodata);
                mProgressLatest = (ProgressBar) view.findViewById(R.id.progress);
                mListViewLatest = (PullToRefreshListView) view.findViewById(R.id.list);

                mListViewLatest.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> refreshView)
                    {
                        String regin = "";

                        if(Constants.INTERNATIONAL_VERSION){
                            if(mApplication.getNowRegion().compareTo(getString(R.string.global))==0) regin = "global";
                            else if(mApplication.getNowRegion().compareTo(getString(R.string.local))==0) regin = "local";
                            else regin = mApplication.getNowRegion();
                        }
                        else{
                            regin = "global";
                        }

                        ApiManager.getLatestLiveStreams(mCtx, regin, Integer.MAX_VALUE, 15, new ApiManager.GetHotLiveStreamsCallback() {
                            @Override
                            public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
                                mListViewLatest.onRefreshComplete();

                                if (success && liveModels != null) {
                                    if (liveModels.size() != 0) {
                                        mNoDataLatest.setVisibility(View.GONE);
                                        mLiveLatestModels.clear();
                                        mLiveLatestModels.addAll(liveModels);

                                        if (mListAdapterLatest != null) mListAdapterLatest = null;
                                        mListAdapterLatest = new LatestListAdapter();
                                        mListViewLatest.setAdapter(mListAdapterLatest);

                                        isFetchingDataLatest = false;
                                        noMoreDataLatest = false;

                                        if (liveModels.size() < 15) {
                                            noMoreDataLatest = true;
                                        }
                                    } else {
                                        mNoDataLatest.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    mNoDataLatest.setVisibility(View.VISIBLE);
                                    try{
//                                      showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                });

                mListViewLatest.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                        if (scrollState == SCROLL_STATE_FLING) {
                            ImageLoader.getInstance().pause();
                        }

                        if (scrollState == SCROLL_STATE_IDLE) {
                            ImageLoader.getInstance().resume();
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });

                if(mLiveLatestModels.size()==0)
                {
                    mProgressLatest.setVisibility(View.VISIBLE);

                    String regin = "";

                    if(Constants.INTERNATIONAL_VERSION){
                        if(mApplication.getNowRegion().compareTo(getString(R.string.global))==0) regin = "global";
                        else if(mApplication.getNowRegion().compareTo(getString(R.string.local))==0) regin = "local";
                        else regin = mApplication.getNowRegion();
                    }
                    else{
                        regin = "global";
                    }

                    ApiManager.getLatestLiveStreams(mCtx, regin, Integer.MAX_VALUE, 15, new ApiManager.GetHotLiveStreamsCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
                            mProgressLatest.setVisibility(View.GONE);

                            if (success && liveModels != null) {
                                if (liveModels.size() != 0) {
                                    mNoDataLatest.setVisibility(View.GONE);
                                    mLiveLatestModels.clear();
                                    mLiveLatestModels.addAll(liveModels);
                                    mListAdapterLatest = new LatestListAdapter();
                                    mListViewLatest.setAdapter(mListAdapterLatest);

                                    if (liveModels.size() < 15) {
                                        noMoreDataLatest = true;
                                    }
                                } else {
                                    mNoDataLatest.setVisibility(View.VISIBLE);
                                }
                            } else {
                                try {
                                    mNoDataLatest.setVisibility(View.VISIBLE);
                                    try{
//                                      showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }
                    });
                }
                else mProgressLatest.setVisibility(View.GONE);

                return mViewList.get(position);
            }
        }
    };

    private class HotListAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mLiveHotModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderHot holder = new ViewHolderHot();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.live_more = (ImageView) convertView.findViewById(R.id.live_more);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolderHot) convertView.getTag();
            }

            holder.name.setText(mLiveHotModels.get(position).getUserInfo().getOpenID());

            try
            {
                holder.day.setText(Singleton.getElapsedTimeString(mLiveHotModels.get(position).getBeginTime()));
            }
            catch (Exception e)
            {
                holder.day.setText("");
            }

            holder.view_text.setText(mLiveHotModels.get(position).getLiveViewerCount() + " " + getString(R.string.live_viewing));
            holder.view_text.setVisibility(View.VISIBLE);
            holder.money_text.setVisibility(View.GONE);

            if(mLiveHotModels.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mLiveHotModels.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.INVISIBLE);

            holder.down_layout.setVisibility(View.GONE);
            holder.live.setVisibility(View.VISIBLE);

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveHotModels.get(position).getUserInfo().getPicture()), holder.self, SmaleSelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mLiveHotModels.get(position).getUserID().compareTo(mUserId)!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mLiveHotModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mLiveHotModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mLiveHotModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mLiveHotModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mLiveHotModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mLiveHotModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mLiveHotModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mLiveHotModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mLiveHotModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mLiveHotModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("live", true);

                        intent.putExtra("live_id", mLiveHotModels.get(position).getLiveStreamID());
                        intent.putExtra("live_openid", mLiveHotModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("live_caption", mLiveHotModels.get(position).getCaption());
                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveHotModels.get(position).getUserInfo().getPicture());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mLiveHotModels.get(position).getUserID().compareTo(mUserId)!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mLiveHotModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mLiveHotModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mLiveHotModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mLiveHotModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mLiveHotModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mLiveHotModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mLiveHotModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mLiveHotModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mLiveHotModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mLiveHotModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("live", true);

                        intent.putExtra("live_id", mLiveHotModels.get(position).getLiveStreamID());
                        intent.putExtra("live_openid", mLiveHotModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("live_caption", mLiveHotModels.get(position).getCaption());
                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveHotModels.get(position).getUserInfo().getPicture());
                        startActivity(intent);
                    }
                }
            });

            holder.photo.setVisibility(View.VISIBLE);
            holder.video.setVisibility(View.GONE);
            holder.vcon.setVisibility(View.GONE);

            holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
            holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

            try
            {
                if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1 && Singleton.preferences.getString(Constants.OPEN_ID, "").compareTo("hi.dean")==0) ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mLiveHotModels.get(position).getCoverPhoto()), holder.photo,BigOptions);
                else ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mLiveHotModels.get(position).getUserInfo().getPicture()), holder.photo,BigOptions);
            }
            catch (OutOfMemoryError e)
            {
                System.gc();
                holder.photo.setImageResource(R.drawable.placehold_s);
            }
            catch(Exception f)
            {
                System.gc();
                holder.photo.setImageResource(R.drawable.placehold_s);
            }

            holder.photo.setOnTouchListener(null);
            holder.photo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(mLiveHotModels.size()!=0 && mLiveHotModels.size() > position)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, LiveStreamActivity.class);
                        intent.putExtra("liveStreamID", mLiveHotModels.get(position).getLiveStreamID());
                        intent.putExtra("name", mLiveHotModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("caption", mLiveHotModels.get(position).getCaption());
                        intent.putExtra("picture", Constants.THUMBNAIL_PREFIX + mLiveHotModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("user", mUserId);
                        intent.putExtra("myopen", Singleton.preferences.getString(Constants.OPEN_ID, ""));
                        //event tracking: to track what page user enter livestream
                        intent.putExtra("enterLiveFrom","NowLive");
                        startActivity(intent);
                    }
                }
            });

            if(mLiveHotModels.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.dio.linkify(new FeedTagActionHandler() {
                @Override
                public void handleHashtag(String hashtag) {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention) {
                    mProgressHot.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message, UserModel user) {
                            mProgressHot.setVisibility(View.GONE);
                            if (success && user != null) {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                startActivity(intent);
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email) {

                }

                @Override
                public void handleUrl(String url) {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                                      showToast(getString(R.string.open_uri_error));
                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            holder.live_more.setVisibility(View.VISIBLE);
            holder.live_more.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    String[] items = {getString(R.string.live_share),getString(R.string.report_inappropriate)};
                    new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(items, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(which == 0)
                            {
                                mProgressHot.setVisibility(View.VISIBLE);
                                Branch.getInstance(mCtx).getContentUrl("facebook", BranchShareLiveData(mLiveHotModels.get(position).getLiveStreamID(), mLiveHotModels.get(position).getUserInfo().getOpenID(), mLiveHotModels.get(position).getCaption(), mLiveHotModels.get(position).getUserInfo().getPicture()), new Branch.BranchLinkCreateListener() {
                                    @Override
                                    public void onLinkCreate(String url, BranchError error) {
                                        mProgressHot.setVisibility(View.GONE);
                                        String mTitle = String.format(getString(R.string.live_relive2), mLiveHotModels.get(position).getUserInfo().getOpenID());
                                        String mDescription = mTitle + "!" + mLiveHotModels.get(position).getCaption() + "  " + url;

                                        DownloadTask mDownloadTask = new DownloadTask();
                                        mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mLiveHotModels.get(position).getUserInfo().getPicture());
                                    }
                                });
                            }
                            else if(which == 1)
                            {
                                final String[] report = {getString(R.string.live_report_message1),getString(R.string.live_report_message2),getString(R.string.live_report_message3),getString(R.string.live_report_message4)};
                                new AlertDialog.Builder(mCtx).setTitle(getString(R.string.report_reason)).setItems(report, new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {

                                        try {
                                            LogEventUtil.ReportLive(mApplication, mApplication);
                                        }
                                        catch (Exception x)
                                        {

                                        }

                                        ApiManager.reportLiveAction(mCtx, mLiveHotModels.get(position).getUserInfo().getUserID(), mLiveHotModels.get(position).getLiveStreamID(), report[which], new ApiManager.RequestCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success)
                                            {
                                                if (success)
                                                {
                                                    try{
//                                      showToast(getString(R.string.done));
                                                        Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                                else {
                                                    try{
//                                      showToast(getString(R.string.error_failed));
                                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                            }
                                        });
                                    }
                                })
                                .show();
                            }
                        }
                    }).show();
                }
            });

            return convertView;
        }
    }

    private class ViewHolderHot
    {
        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
        ImageView live_more;
    }

//    private class SuggestListAdapter extends BaseAdapter
//    {
//        @Override
//        public int getCount()
//        {
//            return mLiveSuggestModels.size();
//        }
//
//        @Override
//        public Object getItem(int position)
//        {
//            return null;
//        }
//
//        @Override
//        public long getItemId(int position)
//        {
//            return 0;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent)
//        {
//            ViewHolderSuggest holder = new ViewHolderSuggest();
//
//            if(convertView==null)
//            {
//                convertView = inflater.inflate(R.layout.home_list_row, null);
//                holder.self = (ImageView) convertView.findViewById(R.id.self);
//                holder.name = (TextView) convertView.findViewById(R.id.name);
//                holder.day = (TextView) convertView.findViewById(R.id.day);
//                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
//                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);
//
//                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
//                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
//                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
//                holder.live = (ImageView) convertView.findViewById(R.id.live);
//                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
//                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
//                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);
//
//                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
//                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
//                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);
//
//                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
//                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
//
//                convertView.setTag(holder);
//            }
//            else
//            {
//                holder = (ViewHolderSuggest) convertView.getTag();
//            }
//
//            holder.name.setText(mLiveSuggestModels.get(position).getUserInfo().getOpenID());
//
//            try
//            {
//                holder.day.setText(Singleton.getElapsedTimeString(mLiveSuggestModels.get(position).getBeginTime()));
//            }
//            catch (Exception e)
//            {
//                holder.day.setText("");
//            }
//
//            holder.view_text.setText(mLiveSuggestModels.get(position).getLiveViewerCount() + " " + getString(R.string.live_viewing));
//            holder.view_text.setVisibility(View.VISIBLE);
//            holder.money_text.setVisibility(View.GONE);
//
//            if(mLiveSuggestModels.get(position).getCaption().length()!=0)
//            {
//                holder.dio.setVisibility(View.VISIBLE);
//                holder.dio.setText(mLiveSuggestModels.get(position).getCaption());
//            }
//            else holder.dio.setVisibility(View.GONE);
//
//            holder.down_layout.setVisibility(View.GONE);
//            holder.live.setVisibility(View.VISIBLE);
//
//            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveSuggestModels.get(position).getUserInfo().getPicture()), holder.self, SmaleSelfOptions);
//            holder.self.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(mLiveSuggestModels.get(position).getUserID().compareTo(mUserId)!=0) {
//                        Intent intent = new Intent();
//                        intent.setClass(mCtx, HomeUserActivity.class);
//                        intent.putExtra("title", mLiveSuggestModels.get(position).getUserInfo().getName());
//                        intent.putExtra("picture", mLiveSuggestModels.get(position).getUserInfo().getPicture());
//                        intent.putExtra("isfollowing", mLiveSuggestModels.get(position).getUserInfo().getIsFollowing());
//                        intent.putExtra("post", mLiveSuggestModels.get(position).getUserInfo().getPostCount());
//                        intent.putExtra("follow", mLiveSuggestModels.get(position).getUserInfo().getFollowerCount());
//                        intent.putExtra("following", mLiveSuggestModels.get(position).getUserInfo().getFollowingCount());
//                        intent.putExtra("open", mLiveSuggestModels.get(position).getUserInfo().getOpenID());
//                        intent.putExtra("bio", mLiveSuggestModels.get(position).getUserInfo().getBio());
//                        intent.putExtra("targetUserID", mLiveSuggestModels.get(position).getUserInfo().getUserID());
//                        intent.putExtra("web", mLiveSuggestModels.get(position).getUserInfo().getWebsite());
//                        intent.putExtra("live", true);
//
//                        intent.putExtra("live_id", mLiveSuggestModels.get(position).getLiveStreamID());
//                        intent.putExtra("live_openid", mLiveSuggestModels.get(position).getUserInfo().getOpenID());
//                        intent.putExtra("live_caption", mLiveSuggestModels.get(position).getCaption());
//                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveSuggestModels.get(position).getUserInfo().getPicture());
//                        startActivity(intent);
//                    }
//                }
//            });
//
//            holder.name.setOnClickListener(new View.OnClickListener()
//            {
//                @Override
//                public void onClick(View v)
//                {
//                    if(mLiveSuggestModels.get(position).getUserID().compareTo(mUserId)!=0) {
//                        Intent intent = new Intent();
//                        intent.setClass(mCtx, HomeUserActivity.class);
//                        intent.putExtra("title", mLiveSuggestModels.get(position).getUserInfo().getName());
//                        intent.putExtra("picture", mLiveSuggestModels.get(position).getUserInfo().getPicture());
//                        intent.putExtra("isfollowing", mLiveSuggestModels.get(position).getUserInfo().getIsFollowing());
//                        intent.putExtra("post", mLiveSuggestModels.get(position).getUserInfo().getPostCount());
//                        intent.putExtra("follow", mLiveSuggestModels.get(position).getUserInfo().getFollowerCount());
//                        intent.putExtra("following", mLiveSuggestModels.get(position).getUserInfo().getFollowingCount());
//                        intent.putExtra("open", mLiveSuggestModels.get(position).getUserInfo().getOpenID());
//                        intent.putExtra("bio", mLiveSuggestModels.get(position).getUserInfo().getBio());
//                        intent.putExtra("targetUserID", mLiveSuggestModels.get(position).getUserInfo().getUserID());
//                        intent.putExtra("web", mLiveSuggestModels.get(position).getUserInfo().getWebsite());
//                        intent.putExtra("live", true);
//
//                        intent.putExtra("live_id", mLiveSuggestModels.get(position).getLiveStreamID());
//                        intent.putExtra("live_openid", mLiveSuggestModels.get(position).getUserInfo().getOpenID());
//                        intent.putExtra("live_caption", mLiveSuggestModels.get(position).getCaption());
//                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveSuggestModels.get(position).getUserInfo().getPicture());
//                        startActivity(intent);
//                    }
//                }
//            });
//
//            holder.photo.setVisibility(View.VISIBLE);
//            holder.video.setVisibility(View.GONE);
//            holder.vcon.setVisibility(View.GONE);
//
//            holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
//            holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;
//
//            try
//            {
//                if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1) ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mLiveSuggestModels.get(position).getCoverPhoto()), holder.photo,BigOptions);
//                else ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mLiveSuggestModels.get(position).getUserInfo().getPicture()), holder.photo,BigOptions);
//            }
//            catch (OutOfMemoryError e)
//            {
//                System.gc();
//                holder.photo.setImageResource(R.drawable.placehold_s);
//            }
//            catch(Exception f)
//            {
//                System.gc();
//                holder.photo.setImageResource(R.drawable.placehold_s);
//            }
//
//            holder.photo.setOnTouchListener(null);
//            holder.photo.setOnClickListener(new View.OnClickListener()
//            {
//                @Override
//                public void onClick(View view)
//                {
//                    if(mLiveSuggestModels.size()!=0 && mLiveSuggestModels.size() > position)
//                    {
//                        Intent intent = new Intent();
//                        intent.setClass(mCtx, LiveStreamActivity.class);
//                        intent.putExtra("liveStreamID", mLiveSuggestModels.get(position).getLiveStreamID());
//                        intent.putExtra("name", mLiveSuggestModels.get(position).getUserInfo().getOpenID());
//                        intent.putExtra("caption", mLiveSuggestModels.get(position).getCaption());
//                        intent.putExtra("picture", Constants.THUMBNAIL_PREFIX + mLiveSuggestModels.get(position).getUserInfo().getPicture());
//                        intent.putExtra("user", mUserId);
//                        intent.putExtra("myopen", Singleton.preferences.getString(Constants.OPEN_ID, ""));
//                        startActivity(intent);
//                    }
//                }
//            });
//
//            if(mLiveSuggestModels.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
//            else holder.verifie.setVisibility(View.GONE);
//
//            holder.dio.linkify(new FeedTagActionHandler() {
//                @Override
//                public void handleHashtag(String hashtag) {
//                    Intent intent = new Intent();
//                    intent.setClass(mCtx, TagPostActivity.class);
//                    intent.putExtra("tag", hashtag);
//                    startActivity(intent);
//                }
//
//                @Override
//                public void handleMention(String mention) {
//                    mProgressHot.setVisibility(View.VISIBLE);
//                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback() {
//                        @Override
//                        public void onResult(boolean success, String message, UserModel user) {
//                            mProgressHot.setVisibility(View.GONE);
//                            if (success && user != null) {
//                                Intent intent = new Intent();
//                                intent.setClass(mCtx, HomeUserActivity.class);
//                                intent.putExtra("title", user.getName());
//                                intent.putExtra("picture", user.getPicture());
//                                intent.putExtra("isfollowing", user.getIsFollowing());
//                                intent.putExtra("post", user.getPostCount());
//                                intent.putExtra("follow", user.getFollowerCount());
//                                intent.putExtra("following", user.getFollowingCount());
//                                intent.putExtra("open", user.getOpenID());
//                                intent.putExtra("bio", user.getBio());
//                                intent.putExtra("targetUserID", user.getUserID());
//                                intent.putExtra("web", user.getWebsite());
//                                startActivity(intent);
//                            }
//                        }
//                    });
//                }
//
//                @Override
//                public void handleEmail(String email) {
//
//                }
//
//                @Override
//                public void handleUrl(String url) {
//                    try
//                    {
//                        Uri uri = Uri.parse(url);
//                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                        startActivity(intent);
//                    }
//                    catch (Exception e)
//                    {
//                        Toast.makeText(mCtx,getString(R.string.open_uri_error),Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//
//            removeLine(holder.dio);
//
//            return convertView;
//        }
//    }
//
//    private class ViewHolderSuggest
//    {
//        ImageView self;
//        TextView name;
//        TextView day;
//        TextView view_text;
//        TextView money_text;
//
//        TouchImage photo;
//        FastVideoView video;
//        ImageView vcon;
//        ImageView live;
//        FeedTagTextView dio;
//        TextView like_text;
//        TextView comment_text;
//        ImageView verifie;
//
//        ImageView btn_like;
//        ImageView btn_comment;
//        ImageView btn_more;
//
//        LinearLayout down_layout;
//    }

    private class LatestListAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mLiveLatestModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderLatest holder = new ViewHolderLatest();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.live_more = (ImageView) convertView.findViewById(R.id.live_more);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolderLatest) convertView.getTag();
            }

            holder.name.setText(mLiveLatestModels.get(position).getUserInfo().getOpenID());

            try
            {
                holder.day.setText(Singleton.getElapsedTimeString(mLiveLatestModels.get(position).getBeginTime()));
            }
            catch (Exception e)
            {
                holder.day.setText("");
            }

            holder.view_text.setText(mLiveLatestModels.get(position).getLiveViewerCount() + " " + getString(R.string.live_viewing));
            holder.view_text.setVisibility(View.VISIBLE);
            holder.money_text.setVisibility(View.GONE);

            if(mLiveLatestModels.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mLiveLatestModels.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.INVISIBLE);

            holder.down_layout.setVisibility(View.GONE);
            holder.live.setVisibility(View.VISIBLE);

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveLatestModels.get(position).getUserInfo().getPicture()), holder.self, SmaleSelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mLiveLatestModels.get(position).getUserID().compareTo(mUserId)!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mLiveLatestModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mLiveLatestModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mLiveLatestModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mLiveLatestModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mLiveLatestModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mLiveLatestModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mLiveLatestModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mLiveLatestModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mLiveLatestModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mLiveLatestModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("live", true);

                        intent.putExtra("live_id", mLiveLatestModels.get(position).getLiveStreamID());
                        intent.putExtra("live_openid", mLiveLatestModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("live_caption", mLiveLatestModels.get(position).getCaption());
                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveLatestModels.get(position).getUserInfo().getPicture());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mLiveLatestModels.get(position).getUserID().compareTo(mUserId)!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mLiveLatestModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mLiveLatestModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mLiveLatestModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mLiveLatestModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mLiveLatestModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mLiveLatestModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mLiveLatestModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mLiveLatestModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mLiveLatestModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mLiveLatestModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("live", true);

                        intent.putExtra("live_id", mLiveLatestModels.get(position).getLiveStreamID());
                        intent.putExtra("live_openid", mLiveLatestModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("live_caption", mLiveLatestModels.get(position).getCaption());
                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveLatestModels.get(position).getUserInfo().getPicture());
                        startActivity(intent);
                    }
                }
            });

            holder.photo.setVisibility(View.VISIBLE);
            holder.video.setVisibility(View.GONE);
            holder.vcon.setVisibility(View.GONE);

            holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
            holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

            try
            {
                if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1 && Singleton.preferences.getString(Constants.OPEN_ID, "").compareTo("hi.dean")==0) ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mLiveLatestModels.get(position).getCoverPhoto()), holder.photo,BigOptions);
                else ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mLiveLatestModels.get(position).getUserInfo().getPicture()), holder.photo,BigOptions);
            }
            catch (OutOfMemoryError e)
            {
                System.gc();
                holder.photo.setImageResource(R.drawable.placehold_s);
            }
            catch(Exception f)
            {
                System.gc();
                holder.photo.setImageResource(R.drawable.placehold_s);
            }

            holder.photo.setOnTouchListener(null);
            holder.photo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(mLiveLatestModels.size()!=0 && mLiveLatestModels.size() > position)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, LiveStreamActivity.class);
                        intent.putExtra("liveStreamID", mLiveLatestModels.get(position).getLiveStreamID());
                        intent.putExtra("name", mLiveLatestModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("caption", mLiveLatestModels.get(position).getCaption());
                        intent.putExtra("picture", Constants.THUMBNAIL_PREFIX + mLiveLatestModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("user", mUserId);
                        intent.putExtra("myopen", Singleton.preferences.getString(Constants.OPEN_ID, ""));
                        //event tracking: to track what page user enter livestream
                        intent.putExtra("enterLiveFrom","NowLive");
                        startActivity(intent);
                    }
                }
            });

            if(mLiveLatestModels.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.dio.linkify(new FeedTagActionHandler() {
                @Override
                public void handleHashtag(String hashtag) {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention) {
                    mProgressHot.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message, UserModel user) {
                            mProgressHot.setVisibility(View.GONE);
                            if (success && user != null) {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                startActivity(intent);
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email) {

                }

                @Override
                public void handleUrl(String url) {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                                      showToast(getString(R.string.open_uri_error));
                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            holder.live_more.setVisibility(View.VISIBLE);
            holder.live_more.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    String[] items = {getString(R.string.live_share),getString(R.string.report_inappropriate)};
                    new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(items, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(which == 0)
                            {
                                mProgressLatest.setVisibility(View.VISIBLE);
                                Branch.getInstance(mCtx).getContentUrl("facebook", BranchShareLiveData(mLiveLatestModels.get(position).getLiveStreamID(),mLiveLatestModels.get(position).getUserInfo().getOpenID(),mLiveLatestModels.get(position).getCaption(),mLiveLatestModels.get(position).getUserInfo().getPicture()), new Branch.BranchLinkCreateListener()
                                {
                                    @Override
                                    public void onLinkCreate(String url, BranchError error)
                                    {
                                        mProgressLatest.setVisibility(View.GONE);
                                        String mTitle = String.format(getString(R.string.live_relive2), mLiveLatestModels.get(position).getUserInfo().getOpenID());
                                        String mDescription = mTitle + "!" + mLiveLatestModels.get(position).getCaption() + "  " + url;

                                        DownloadTask mDownloadTask = new DownloadTask();
                                        mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mLiveLatestModels.get(position).getUserInfo().getPicture());
                                    }
                                });
                            }
                            else if(which == 1)
                            {
                                final String[] report = {getString(R.string.live_report_message1),getString(R.string.live_report_message2),getString(R.string.live_report_message3),getString(R.string.live_report_message4)};
                                new AlertDialog.Builder(mCtx).setTitle(getString(R.string.report_reason)).setItems(report, new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        try {
                                            LogEventUtil.ReportLive(mApplication, mApplication);
                                        }
                                        catch (Exception x)
                                        {

                                        }
                                        ApiManager.reportLiveAction(mCtx, mLiveLatestModels.get(position).getUserInfo().getUserID(), mLiveLatestModels.get(position).getLiveStreamID(), report[which], new ApiManager.RequestCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success)
                                            {
                                                if (success)
                                                {
                                                    try{
//                                      showToast(getString(R.string.done));
                                                        Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                                else {
                                                    try{
//                                      showToast(getString(R.string.error_failed));
                                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                            }
                                        });
                                    }
                                })
                                .show();
                            }
                        }
                    }).show();
                }
            });

            return convertView;
        }
    }

    private class ViewHolderLatest
    {
        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
        ImageView live_more;
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private class DownloadTask extends AsyncTask<String, Integer, Boolean>
    {
        private String subject = "";
        private String body = "";
        private String chooserTitle = "";
        private String uri = "";

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            if(mProgressHot!=null && mProgressLatest!=null)
            {
                mProgressHot.setVisibility(View.VISIBLE);
                mProgressLatest.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            try
            {
                subject = text[0];
                body = text[1];
                chooserTitle = text[2];
                uri = text[3];

                URL url = new URL(Singleton.getS3FileUrl(uri));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "17app.jpg";
                FileOutputStream outStream = new FileOutputStream(file);
                BitmapFactory.decodeStream(input).compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.close();
                return true;
            }
            catch (OutOfMemoryError o)
            {
                return false;
            }
            catch (IOException e)
            {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);
            if(mProgressHot!=null && mProgressLatest!=null)
            {
                mProgressHot.setVisibility(View.GONE);
                mProgressLatest.setVisibility(View.GONE);
            }

            if(result) shareTo(subject,body,chooserTitle);
            else {
                try{
//                                      showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
            }
        }
    }

    private void shareTo(String subject, String body, String chooserTitle)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
//        String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
//        File imageFileToShare = new File(imagePath);
//        Uri uri = Uri.fromFile(imageFileToShare);
//        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(previousPage == 0)
        {
            try{
                LogEventUtil.LeaveHotLiveTrendingPage(mCtx,mApplication,Singleton.getCurrentTimestamp()-hotPageStart,mLiveHotModels.size());
            }catch (Exception x)
            {

            }
        }
        else
        {
            try{
                LogEventUtil.LeaveHotLiveLatestPage(mCtx,mApplication,Singleton.getCurrentTimestamp()-latestPageStart,mLiveLatestModels.size());
            }catch (Exception x)
            {

            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mApplication.getshowToast())
        {
            try{
//                                      showToast(getString(R.string.liveend));
                Toast.makeText(mCtx, getString(R.string.liveend), Toast.LENGTH_SHORT).show();
            }
            catch (Exception x){
            }
            mApplication.setshowToast(false);
        }

//        if(mApplication.getReLoad())
//        {
////            mProgress.setVisibility(View.VISIBLE);
//            ApiManager.getHotLiveStreams(mCtx, mUserId, new ApiManager.GetHotLiveStreamsCallback() {
//                @Override
//                public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
//                    mProgressHot.setVisibility(View.GONE);
//                    mApplication.setReLoad(false);
//                    if (success && liveModels != null) {
//                        if (liveModels.size() != 0) {
//                            mLiveHotModels.clear();
//                            mLiveHotModels.addAll(liveModels);
//
//                            if (mListAdapterHot == null) {
//                                mListAdapterHot = new HotListAdapter();
//                                mListViewHot.setAdapter(mListAdapterHot);
//                            }
//                            else{
//                                mListAdapterHot.notifyDataSetChanged();
//                            }
//
//                        } else {
//                            mLiveHotModels.clear();
//                            if (mListAdapterHot == null) {
//                                mListAdapterHot = new HotListAdapter();
//                                mListViewHot.setAdapter(mListAdapterHot);
//                            }
//                            else{
//                                mListAdapterHot.notifyDataSetChanged();
//                            }
//                            mNoDataHot.setVisibility(View.VISIBLE);
//                        }
//                    } else {
//                        mLiveHotModels.clear();
//                        if (mListAdapterHot == null) {
//                            mListAdapterHot = new HotListAdapter();
//                            mListViewHot.setAdapter(mListAdapterHot);
//                        }
//                        else{
//                            mListAdapterHot.notifyDataSetChanged();
//                        }
//                        mNoDataHot.setVisibility(View.VISIBLE);
//                    }
//                }
//            });
//        }

        if(mApplication.getReLoadRegion() || mApplication.getReLoad())
        {
            mApplication.setReLoad(false);
            mApplication.setReLoadRegion(false);

            if(Constants.INTERNATIONAL_VERSION) {
                if(mApplication.getNowRegion().compareTo(getString(R.string.local))==0)
                {
                    mImgRight.setImageResource(R.drawable.local);
                    mImgRight.setVisibility(View.VISIBLE);
                    mBtnRight.setVisibility(View.GONE);
                }
                else if(mApplication.getNowRegion().compareTo(getString(R.string.global))==0)
                {
                    mImgRight.setImageResource(R.drawable.globa);
                    mImgRight.setVisibility(View.VISIBLE);
                    mBtnRight.setVisibility(View.GONE);
                }
                else
                {
                    mImgRight.setVisibility(View.GONE);
                    mBtnRight.setVisibility(View.VISIBLE);
                    mBtnRight.setText(mApplication.getNowRegion());
                }
            }
            else {
                mImgRight.setVisibility(View.GONE);
                mBtnRight.setVisibility(View.GONE);
            }

            int pos = mViewPager.getCurrentItem();
            mLiveHotModels.clear();
//            mLiveSuggestModels.clear();
            mLiveLatestModels.clear();

            if(mPagerAdapter!=null) mPagerAdapter = null;
            mPagerAdapter = new PagerBaseAdapter();
            mViewPager.setAdapter(mPagerAdapter);
            mViewPager.setCurrentItem(pos);
        }
    }

    private JSONObject BranchShareLiveData(int liveStreamID, String OpenID, String caption, String Picture)
    {
        JSONObject mShareData = new JSONObject();
        try
        {
            mShareData.put("page", "live");
            mShareData.put("ID", liveStreamID);
            mShareData.put("$og_title", String.format(getString(R.string.live_relive2), OpenID));
            mShareData.put("$og_description", caption);
            mShareData.put("$og_image_url", Singleton.getS3FileUrl(Picture));
            mShareData.put("$og_type", "website");
            mShareData.put("$fallback_url", Constants.MEDIA17_WEBSITE_SHARE + "live/" + liveStreamID);
        }
        catch (JSONException ex)
        {
        }

        return mShareData;
    }
}
