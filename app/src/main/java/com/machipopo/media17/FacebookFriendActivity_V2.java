package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by POPO on 5/21/15.
 */
public class FacebookFriendActivity_V2 extends BaseActivity
{
    private FacebookFriendActivity_V2 mCtx = this;
    private LayoutInflater inflater;
    private String mJSONArray = "";
    private ArrayList<UserModel> mModel;
    private Button mBtn, mNext;
    private TextView mText;
    private ListView mList;
    private FriendAdapter mFriendAdapter;
    private ArrayList<Boolean> mState = new ArrayList<Boolean>();
    private Story17Application mApplication;

    private ImageView mNoData;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook_friend_activity_v2);

        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("facebook")) mJSONArray = mBundle.getString("facebook");
        }

        initTitleBar();

        mText = (TextView) findViewById(R.id.have);
        mBtn = (Button) findViewById(R.id.all);
        mList = (ListView) findViewById(R.id.list);
        mNoData = (ImageView) findViewById(R.id.nodata);
        mNext = (Button) findViewById(R.id.btn_next);
        mApplication = (Story17Application) getApplication();

        showProgressDialog();
        ApiManager.findFriends(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mJSONArray, "facebook", new ApiManager.FindFriendsCallback()
        {
            @Override
            public void onResult(boolean success, ArrayList<UserModel> model)
            {
                hideProgressDialog();
                if(success)
                {
                    if(model.size()!=0)
                    {
                        mModel = model;
                        mText.setText(getString(R.string.have) + mModel.size() + getString(R.string.have_people));
                        mBtn.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                showProgressDialog();
                                JSONArray mJSONArray = new JSONArray();
                                for(int i = 0 ; i < mModel.size() ; i++ )
                                {
                                    if(mModel.get(i).getPrivacyMode().compareTo("private")!=0) mJSONArray.put(mModel.get(i).getUserID());
                                }

                                //event tracking
                                try{
                                    LogEventUtil.FollowAllSuggestUser(mCtx,mApplication);
                                }catch (Exception X)
                                {

                                }

                                ApiManager.followAllAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mJSONArray.toString(), new ApiManager.FollowAllActionCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        hideProgressDialog();
                                        if (success)
                                        {
                                            for(int i = 0 ; i < mModel.size() ; i++ )
                                            {
                                                mState.set(i, true);
                                            }
//                                            mBtn.setText("全部追蹤");
                                            mFriendAdapter.notifyDataSetChanged();
                                        }
                                        else {
                                            try{
//                                                showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception e){
                                            }
                                        }
                                    }
                                });
                            }
                        });

                        for(int i = 0 ; i < mModel.size() ; i++)
                        {
                            mState.add(false);
                        }

                        mFriendAdapter = new FriendAdapter();
                        mList.setAdapter(mFriendAdapter);
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                        mBtn.setVisibility(View.GONE);
//                        Toast.makeText(mCtx, "無朋友", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    mNoData.setVisibility(View.VISIBLE);
                    mBtn.setVisibility(View.GONE);
//                    Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.fb_friend));
        mTitle.setTextColor(Color.WHITE);

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.next));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,FollowFriendActivityV2.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    private class FriendAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mModel.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.follow_friend_row, null);
                holder.img = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.follow = (Button) convertView.findViewById(R.id.follow);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            //http://story17.machipopo.com/THUMBNAIL_744FE2D7-9317-41D2-AC2C-5D276E075F8F.jpg
            //Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModel.get(position).getPicture())
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModel.get(position).getPicture()), holder.img);

            holder.name.setText(mModel.get(position).getOpenID());
            holder.dio.setText(mModel.get(position).getName());
            final int pos = position;

            if(mState.get(position))
            {
                holder.follow.setText(getString(R.string.notifi_follow));
                holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
                holder.follow.setTextColor(Color.WHITE);
            }
            else
            {
                if(mModel.get(pos).getFollowRequestTime()!=0)
                {
                    holder.follow.setText(getString(R.string.private_mode_request_send));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    holder.follow.setText("+ " + getString(R.string.user_profile_follow));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
            }

            final Button btn = holder.follow;
            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    showProgressDialog();

                    if(mState.get(pos))
                    {
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mModel.get(pos).getUserID());
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mModel.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                hideProgressDialog();

                                if (success)
                                {
                                    mState.set(pos, false);
                                    btn.setText("+ " + getString(R.string.user_profile_follow));
                                    btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                }
                                else {
                                    try{
//                                                showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception e){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mModel.get(pos).getFollowRequestTime()!=0)
                        {
                            hideProgressDialog();
                            btn.setText("+ " + getString(R.string.user_profile_follow));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                            mState.set(pos, false);
                            mModel.get(pos).setIsFollowing(0);
                            mModel.get(pos).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mModel.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                                                showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }
                                return ;
                            }

                            if(mModel.get(pos).getPrivacyMode().compareTo("private")==0)
                            {
                                hideProgressDialog();
                                btn.setText(getString(R.string.private_mode_request_send));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                mModel.get(pos).setIsFollowing(0);
                                mState.set(pos, false);
                                mModel.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mModel.get(pos).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mState.set(pos, false);
                                            mModel.get(pos).setIsFollowing(0);
                                            mModel.get(pos).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                try {
                                    LogEventUtil.FollowUser(mCtx,mApplication,mModel.get(pos).getUserID());
                                    LogEventUtil.FollowSuggestUser(mCtx,mApplication,mModel.get(pos).getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mModel.get(pos).getUserID(), new ApiManager.FollowUserActionCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        hideProgressDialog();
                                        if (success)
                                        {
                                            mState.set(pos,true);
                                            btn.setText(getString(R.string.user_profile_following));
                                            btn.setBackgroundResource(R.drawable.btn_green_selector);
                                            btn.setTextColor(Color.WHITE);
                                        }
                                        else {
                                            try{
//                                                showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception e){
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView img;
        TextView name;
        TextView dio;
        Button follow;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {


            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
