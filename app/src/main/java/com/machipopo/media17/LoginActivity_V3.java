package com.machipopo.media17;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.DevUtils;
import com.machipopo.media17.utils.LogEventUtil;
import com.machipopo.media17.utils.ShareUtil;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tencent.connect.UserInfo;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;


public class LoginActivity_V3 extends BaseActivity {
    private LoginActivity_V3 mCtx = this;
    private EditText mAccount, mPasswoird;
    private ConnectivityManager mConnectivityManager;
    private NetworkInfo mNetworkInfo;
    private LinearLayout btnLogin;
    private boolean focus = false;
    private TextView mForgot;
//    private LinearLayout btnFBLogin;
    private SharedPreferences sharedPreferences;
    private CallbackManager callbackManager;
    public final static String tag = "signup_setting";

//    private ImageView mLoginIcon;
//    private TextView mLoginText;
    private Story17Application mApplication;
    private TextView Or;

    private LinearLayout mWeiboLayout, mQQLayout, mWeChatLayout, mFBLayout;
//    private Boolean mClick = true;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());

        if(mApplication!=null){
            if(mApplication.getGuestFinish()) mCtx.finish();
            else if(mApplication.getMenuFinish()) mCtx.finish();
        }
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());

        hideKeyboard();

//        mClick = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.longin_activity_v3);
        }
        catch (OutOfMemoryError o){
            Toast.makeText(mCtx, getString(R.string.live_watch_memory_error), Toast.LENGTH_SHORT).show();
            System.gc();
            mCtx.finish();
            return;
        }
        catch (Exception e){
            Toast.makeText(mCtx, getString(R.string.live_watch_memory_error), Toast.LENGTH_SHORT).show();
            System.gc();
            mCtx.finish();
            return;
        }

        mApplication = (Story17Application) getApplication();

        initTitleBar();

        mAccount = (EditText) findViewById(R.id.account);
        mPasswoird = (EditText) findViewById(R.id.password);
        btnLogin = (LinearLayout) findViewById(R.id.btnLogin);
//        btnFBLogin = (LinearLayout) findViewById(R.id.btnFBLogin);
        btnLogin.setOnClickListener(ButtonClickEventListener);

//        mLoginIcon = (ImageView) findViewById(R.id.login_icon);
//        mLoginText = (TextView) findViewById(R.id.login_text);
        Or = (TextView) findViewById(R.id.or);

//        if(Constants.INTERNATIONAL_VERSION) {
//            btnFBLogin.setOnClickListener(FBButtonClickEventListener);
//        }
//        else {
//            mLoginIcon.setImageResource(R.drawable.login_wechat);
//            mLoginText.setText(getString(R.string.wechat_login));
//            btnFBLogin.setOnClickListener(WechatButtonClickEventListener);
//            btnFBLogin.setVisibility(View.GONE);
//            Or.setVisibility(View.GONE);
//        }

        mWeiboLayout = (LinearLayout) findViewById(R.id.weibo_layout);
        mQQLayout = (LinearLayout) findViewById(R.id.qq_layout);
        mWeChatLayout = (LinearLayout) findViewById(R.id.wechat_layout);
        mFBLayout = (LinearLayout) findViewById(R.id.fb_layout);

        if(Constants.INTERNATIONAL_VERSION) {
            mWeiboLayout.setVisibility(View.GONE);
            mQQLayout.setVisibility(View.GONE);
            mWeChatLayout.setVisibility(View.GONE);
            mFBLayout.setVisibility(View.VISIBLE);
        }
        else{
            mWeiboLayout.setVisibility(View.VISIBLE);
            mQQLayout.setVisibility(View.VISIBLE);
            mWeChatLayout.setVisibility(View.VISIBLE);
            mFBLayout.setVisibility(View.GONE);
        }

        mPasswoird.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                focus = hasFocus;
            }
        });

        mForgot = (TextView) findViewById(R.id.forgot);
        mForgot.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,ForgotPasswordActivity_V2.class);
                startActivity(intent);
                mCtx.finish();
            }
        });

        showKeyboard();

        mWeiboLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                try{
                    //Umeng monitor
                    String Umeng_id="GuestModeLoginWeibo";
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                }
                catch (Exception e){
                }

                try{
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                    mHashMap.put("version", Singleton.getVersion());
                    mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                    mHashMap.put("u", Singleton.getPhoneIMEI());
                    mHashMap.put("dn", DevUtils.getDevInfo());
                    LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_webio_login", mHashMap);
                }
                catch (Exception e){
                }

                showWebio();
            }
        });

        mQQLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
//                if(!mClick) return;
//                mClick = false;
                try{
                    //Umeng monitor
                    String Umeng_id="GuestModeLoginQQ";
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                }
                catch (Exception e){
                }

                try{
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                    mHashMap.put("version", Singleton.getVersion());
                    mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                    mHashMap.put("u", Singleton.getPhoneIMEI());
                    mHashMap.put("dn", DevUtils.getDevInfo());
                    LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_qq_login", mHashMap);
                }
                catch (Exception e){
                }

                mTencent = Tencent.createInstance(Constants.QQAppid, mCtx);
                mTencent.login(mCtx, "all", loginListener);
            }
        });

        mWeChatLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
//                if(!mClick) return;
//                mClick = false;

                if (ShareUtil.isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI"))
                {
                try{
                    //Umeng monitor
                    String Umeng_id="GuestModeLoginWechat";
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                }
                catch (Exception e){
                }

                    try{
                        HashMap<String,String> mHashMap = new HashMap<String,String>();
                        mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                        mHashMap.put("version", Singleton.getVersion());
                        mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                        mHashMap.put("u", Singleton.getPhoneIMEI());
                        mHashMap.put("dn", DevUtils.getDevInfo());
                        LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_wechat_login", mHashMap);
                    }
                    catch (Exception e){
                    }

                IWXAPI api;
                api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
                api.registerApp(Constants.WECHAT_APP_KEY);
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "wechat_sdk_demo";
                api.sendReq(req);
                }
                else
                {
                    Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mFBLayout.setOnClickListener(FBButtonClickEventListener);
        hideKeyboard(100);
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.log_in_only));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_back_black);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            hideKeyboard();
//            Intent intent = new Intent();
//            intent.setClass(mCtx, GuestActivity.class);
//            startActivity(intent);
            mCtx.finish();

            return true;
        }

        if(keyCode == KeyEvent.KEYCODE_ENTER && focus){
            processLogin();
        }

        return super.onKeyUp(keyCode, event);
    }

    private View.OnClickListener ButtonClickEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //Umeng monitor
            String Umeng_id="GuestModeLoginSMS";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

            processLogin();
        }
    };

    private void processLogin(){
        if(mAccount.getText().toString().length()!=0 && mPasswoird.getText().toString().length()!=0)
        {
            mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

            if (mNetworkInfo != null && mNetworkInfo.isConnected())
            {
                hideKeyboard();
                showProgressDialog();
                ApiManager.loginAction(mCtx,mAccount.getText().toString(), mPasswoird.getText().toString(), new ApiManager.LoginActionCallback()
                {
                    @Override
                    public void onResult(boolean success, String message, UserModel user)
                    {
                        hideProgressDialog();

                        if(success)
                        {
                            if (message.equals("ok"))
                            {
                                try{
                                    String Umeng_id="login_type";
                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                    mHashMap.put("Umeng_id", "17");
                                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                                }
                                catch (Exception e){
                                }

                                setConfig(Constants.ACCOUNT,mAccount.getText().toString());
                                setConfig(Constants.PASSWORD,mPasswoird.getText().toString());

                                sharedPreferences = getSharedPreferences(tag, 0);
                                sharedPreferences.edit().putString(Constants.NAME, mAccount.getText().toString()).commit();
                                sharedPreferences.edit().putString(Constants.PASSWORD, mPasswoird.getText().toString()).commit();

//                                Log.d("17_gift", "NAME :" + sharedPreferences.getString(Constants.NAME, "")+ " password :" + sharedPreferences.getString(Constants.PASSWORD, ""));

                                Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                mStory17Application.setUser(user);

                                Intent intent = new Intent();
                                intent.setClass(mCtx, MenuActivity.class);
                                startActivity(intent);
                                mCtx.finish();
                            }
                            else if (message.equals("freezed")) showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                            else showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                        }
                        else{
                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_fail));
                        }
                    }
                });
            }
            else
            {
                try{
                    Toast.makeText(mCtx, getString(R.string.login_internet), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
            }
        }
        else{
            try{
                Toast.makeText(mCtx, getString(R.string.login_enter), Toast.LENGTH_SHORT).show();
            }
            catch (Exception x){
            }
        }
    }

    View.OnClickListener FBButtonClickEventListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            hideKeyboard();
            Intent intent;

            try{
                String Umeng_id="login_type";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put("Umeng_id", "facebook");
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
            }
            catch (Exception e){
            }

            try{
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                mHashMap.put("version", Singleton.getVersion());
                mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                mHashMap.put("u", Singleton.getPhoneIMEI());
                mHashMap.put("dn", DevUtils.getDevInfo());
                LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_fb_login", mHashMap);
            }
            catch (Exception e){
            }

            //Umeng monitor
            String Umeng_id_FB="FBlogin";
            HashMap<String,String> mHashMap_FB = new HashMap<String,String>();
            mHashMap_FB.put(Umeng_id_FB, Umeng_id_FB);
            MobclickAgent.onEventValue(mCtx,Umeng_id_FB,mHashMap_FB,0);

            FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
            callbackManager = CallbackManager.Factory.create();
            LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>()
                    {
                        @Override
                        public void onSuccess(final LoginResult loginResult)
                        {
                            final String token = loginResult.getAccessToken().getToken();

                            GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject user, GraphResponse response) {
                                    if (user != null) {
                                        try {
                                            showProgressDialog();
                                            final String id = user.optString("id");
                                            final String name = user.optString("name");
//                                                    Log.e("DDD", "ID: " + id);

                                            // Download Profile photo
                                            DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, id);
                                            downloadFBProfileImage.execute();

                                            ApiManager.checkFacebookIDAvailable(mCtx, id, new ApiManager.checkFacebookIDAvailableCallback() {
                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {

                                                        // Download user profile image
//                                                                DownloadFBProfileImage download = new DownloadFBProfileImage(mCtx, id);
//                                                                download.execute();

                                                        //Signup
                                                        GraphRequest request = GraphRequest.newMeRequest(
                                                                loginResult.getAccessToken(),
                                                                new GraphRequest.GraphJSONObjectCallback() {
                                                                    @Override
                                                                    public void onCompleted(
                                                                            JSONObject object,
                                                                            GraphResponse response) {
                                                                        // Application code
                                                                        String email = object.optString("email");
//                                                                                Log.e("DDD", "EMAIL: " + email);


                                                                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                        sharedPreferences.edit().putString(Constants.FACEBOOK_ID, id).commit();
                                                                        sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                                                        sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                        sharedPreferences.edit().putString("signup_type", "fb").commit();
                                                                        sharedPreferences.edit().putString("signup_token", token).commit();

                                                                        if (null != email) {
                                                                            if (email.contains("@")) {
                                                                                String username = email.substring(0, email.indexOf("@"));
                                                                                sharedPreferences.edit().putString(Constants.EMAIL, email).commit();
                                                                                sharedPreferences.edit().putString(Constants.NAME, username).commit();
                                                                            }
                                                                        }
                                                                        hideProgressDialog();
                                                                        Intent intent = new Intent();
                                                                        intent.setClass(mCtx, SignupActivityV2.class);
                                                                        intent.putExtra("from",true);
                                                                        startActivity(intent);

                                                                        mCtx.finish();
                                                                    }
                                                                });

                                                        Bundle parameters = new Bundle();
                                                        parameters.putString("fields", "email");
                                                        request.setParameters(parameters);
                                                        request.executeAsync();

                                                    } else {
                                                        //login directly
                                                        ApiManager.loginAction2(mCtx, "", "", id, new ApiManager.LoginAction2Callback() {
                                                            @Override
                                                            public void onResult(boolean success, String message, UserModel user) {
                                                                hideProgressDialog();

                                                                if (success) {
                                                                    if (message.equals("ok")) {

                                                                        Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                        mStory17Application.setUser(user);

                                                                        Intent intent = new Intent();
                                                                        intent.setClass(mCtx, MenuActivity.class);
                                                                        startActivity(intent);
                                                                        mCtx.finish();
                                                                    } else if (message.equals("freezed"))
                                                                        showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                    else
                                                                        showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                } else {
                                                                    showNetworkUnstableToast();
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });


                                        } catch (Exception e) {
                                            hideProgressDialog();
                                            try {
//                              showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            } catch (Exception x) {
                                            }
                                        }
                                    }
                                }
                            }).executeAsync();
                        }

                        @Override
                        public void onCancel()
                        {
                            hideProgressDialog();
                            try{
//                              showToast(getString(R.string.failed));
                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                        }

                        @Override
                        public void onError(FacebookException exception)
                        {
                            hideProgressDialog();
                            try{
//                              showToast(getString(R.string.failed));
                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                        }
                    });



        }
    };

    View.OnClickListener WechatButtonClickEventListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
        if (ShareUtil.isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI"))
        {
            IWXAPI api;
            api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
            api.registerApp(Constants.WECHAT_APP_KEY);
            SendAuth.Req req = new SendAuth.Req();
            req.scope = "snsapi_userinfo";
            req.state = "wechat_sdk_demo";
            api.sendReq(req);
        }
        else
        {
            Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
        }
        }
    };

    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        private final String fbImageDownlaodUrl = "https://graph.facebook.com/";
        private final String attrs = "/picture?width=1080&height=1080";
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

            this.url = fbImageDownlaodUrl + userID + attrs;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {

        }

    }

    public static Tencent mTencent;

    private class BaseUiListener implements IUiListener
    {

        @Override
        public void onComplete(Object response) {
            if (null == response) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            if (null != jsonResponse && jsonResponse.length() == 0) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }

            doComplete((JSONObject)response);
        }

        protected void doComplete(JSONObject values) {

        }

        @Override
        public void onError(UiError e) {
            hideProgressDialog();
            showToast(getString(R.string.failed));
        }

        @Override
        public void onCancel() {
            hideProgressDialog();
        }
    }

    private IUiListener loginListener = new BaseUiListener()
    {
        @Override
        protected void doComplete(JSONObject jsonObject)
        {
            try
            {
                final String token = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_ACCESS_TOKEN);
                String expires = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_EXPIRES_IN);
                final String openId = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_OPEN_ID);

                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);

                if (mTencent != null && mTencent.isSessionValid())
                {
                    IUiListener listener = new IUiListener()
                    {
                        @Override
                        public void onError(UiError e) {
                            hideProgressDialog();
                            showToast(getString(R.string.failed));
                        }

                        @Override
                        public void onComplete(final Object response)
                        {
                            try
                            {
                                JSONObject json = (JSONObject)response;

                                final String name = json.getString("nickname");
                                String pic = json.getString("figureurl_qq_1");

                                DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                downloadFBProfileImage.execute();

                                ApiManager.checkQQIDAvailable(mCtx, openId, new ApiManager.checkFacebookIDAvailableCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, openId).commit();
                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                            sharedPreferences.edit().putString(Constants.NAME, name).commit();
                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();
                                            sharedPreferences.edit().putString("signup_type", "qq").commit();
                                            sharedPreferences.edit().putString("signup_token", token).commit();
                                            hideProgressDialog();
                                            Intent intent = new Intent();
                                            intent.setClass(mCtx, SignupActivityV2.class);
                                            intent.putExtra("from", true);
                                            startActivity(intent);

                                            mCtx.finish();
                                        }
                                        else
                                        {
                                            ApiManager.loginActionChina(mCtx, "", "", "qqID", openId, new ApiManager.LoginAction2Callback() {
                                                @Override
                                                public void onResult(boolean success, String message, UserModel user) {
                                                    hideProgressDialog();

                                                    if (success) {
                                                        if (message.equals("ok")) {

                                                            Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                            mStory17Application.setUser(user);

                                                            Intent intent = new Intent();
                                                            intent.setClass(mCtx, MenuActivity.class);
                                                            startActivity(intent);
                                                            mCtx.finish();
                                                        } else if (message.equals("freezed"))
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                        else
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                    } else {
                                                        showNetworkUnstableToast();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }
                        }

                        @Override
                        public void onCancel() {
                            hideProgressDialog();
                        }
                    };

                    UserInfo mInfo = new UserInfo(mCtx, mTencent.getQQToken());
                    mInfo.getUserInfo(listener);
                }
            }
            catch(Exception e) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
            }
        }
    };

    class DownloadProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        String url = "";

        public DownloadProfileImage(Context context, String userID) {
            this.context = context;

            this.url = userID.substring(0,userID.length()-2) + 640;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {
//                    java.net.URL url = new java.net.URL(this.url);
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setDoInput(true);
//                    connection.connect();

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if(mTencent!=null) mTencent.logout(mCtx);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == com.tencent.connect.common.Constants.REQUEST_LOGIN || requestCode == com.tencent.connect.common.Constants.REQUEST_APPBAR)
        {
            Tencent.onActivityResultData(requestCode,resultCode,data,loginListener);
        }

        try
        {
            if(callbackManager!=null) callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        catch (Exception e)
        {
        }
    }

    //-----

    private Dialog mWeiboDialog;
    private void showWebio()
    {
        if(mWeiboDialog!=null) mWeiboDialog = null;
        mWeiboDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        mWeiboDialog.setContentView(R.layout.instagram_dialog);
        final WebView mWeb = (WebView) mWeiboDialog.findViewById(R.id.web);
        final ProgressBar mProgress = (ProgressBar) mWeiboDialog.findViewById(R.id.progress);
        mWeb.getSettings().setJavaScriptEnabled(true);
        mWeiboDialog.setCancelable(true);
        mWeb.loadUrl("https://api.weibo.com/oauth2/authorize?client_id=" + Constants.WEIBO_APP_KEY + "&response_type=code&redirect_uri=" + Constants.WEIBO_REDIRECT_URL);
        mWeb.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);

                if (url.contains(Constants.IG_REDIRECT_URI) && url.contains("code=")) {
                    mWeiboDialog.dismiss();
                    showProgressDialog();

                    int pos = url.indexOf("=") + 1;
                    String token = url.substring(pos, url.length());
                    try {
                        OkHttpClient mClient = new OkHttpClient();

                        RequestBody body = new FormEncodingBuilder()
                                .add("client_id", Constants.WEIBO_APP_KEY)
                                .add("client_secret", Constants.WEIBO_SECRET)
                                .add("grant_type", "authorization_code")
                                .add("code", token)
                                .add("redirect_uri", Constants.WEIBO_REDIRECT_URL)
                                .build();

                        Request request = new Request.Builder().url("https://api.weibo.com/oauth2/access_token").post(body).build();
                        mClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject data = new JSONObject(response.body().string());
                                        final String token = data.getString("access_token");
                                        final String uid = data.getString("uid");

                                        OkHttpClient client = new OkHttpClient();
                                        Request request = new Request.Builder().url("https://api.weibo.com/2/users/show.json?access_token=" + token + "&uid=" + uid).build();
                                        client.newCall(request).enqueue(new Callback() {
                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                hideProgressDialog();
                                                showToast(getString(R.string.failed));
                                            }

                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if (response.isSuccessful()) {
                                                    try {
                                                        JSONObject info = new JSONObject(response.body().string());
                                                        final int id = info.getInt("id");
                                                        final String screen_name = info.getString("screen_name");
                                                        final String pic = info.getString("profile_image_url");

                                                        DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                                        downloadFBProfileImage.execute();

                                                        ApiManager.checkWeiboIDAvailable(mCtx, uid /*String.valueOf(id)*/, new ApiManager.checkFacebookIDAvailableCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                if (success) {
                                                                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, String.valueOf(id)).commit();
                                                                    sharedPreferences.edit().putString(Constants.FULL_NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putString(Constants.NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                    sharedPreferences.edit().putString("is_weibo", "weibo").commit();

                                                                    sharedPreferences.edit().putString("signup_type", "weibo").commit();
                                                                    sharedPreferences.edit().putString("signup_token", token).commit();

                                                                    hideProgressDialog();
                                                                    Intent intent = new Intent();
                                                                    intent.setClass(mCtx, SignupActivityV2.class);
                                                                    intent.putExtra("from",true);
                                                                    startActivity(intent);
                                                                    mCtx.finish();
                                                                } else {
                                                                    ApiManager.loginActionChina(mCtx, "", "", "weiboID", uid, new ApiManager.LoginAction2Callback() {
                                                                        @Override
                                                                        public void onResult(boolean success, String message, UserModel user) {
                                                                            hideProgressDialog();

                                                                            if (success) {
                                                                                if (message.equals("ok")) {

                                                                                    Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                                    mStory17Application.setUser(user);

                                                                                    Intent intent = new Intent();
                                                                                    intent.setClass(mCtx, MenuActivity.class);
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                } else if (message.equals("freezed"))
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                                else
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                            } else {
                                                                                showNetworkUnstableToast();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    } catch (JSONException e) {
                                                        hideProgressDialog();
                                                        showToast(getString(R.string.failed));
                                                    }
                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                        hideProgressDialog();
                                        showToast(getString(R.string.failed));
                                    }
                                } else {
                                    hideProgressDialog();
                                    showToast(getString(R.string.failed));
                                }
                            }
                        });
                    } catch (Exception e) {
                        hideProgressDialog();
                        showToast(getString(R.string.failed));
                    }
                } else {
//                            mWeiboDialog.dismiss();
//                            hideProgressDialog();
//                            showToast(getString(R.string.failed));
                }
            }
        });
        try{
            mWeb.clearCache(true);
            mWeb.getSettings().setCacheMode(WebView.PERSISTENT_NO_CACHE);
        }
        catch (Exception e){
        }
        mWeiboDialog.show();
    }
}
