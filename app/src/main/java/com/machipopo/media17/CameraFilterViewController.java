package com.machipopo.media17;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.machipopo.media17.View.FilterControlSeekBar;
import com.machipopo.media17.View.TiltEffectDrawingView;
import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class CameraFilterViewController extends BaseNewActivity{
    public interface RenderEventListener{
        void onDrawFrameFinish();
        void onErrorOccurred();
    }
    public interface TiltEventListener{
        void updateTiltEffect(int type, float centerX, float centerY, float radius, float angle);
    }

    private final int[] TabFilterItemText = new int[] {
        R.string.filter_normal,
        R.string.filter_rich,
        R.string.filter_warm,
        R.string.filter_soft,
        R.string.filter_rose,
        R.string.filter_morning,
        R.string.filter_sunshine,
        R.string.filter_sunset,
        R.string.filter_cool,
        R.string.filter_freeze,
        R.string.filter_ocean,
        R.string.filter_dream,
        R.string.filter_violet,
        R.string.filter_mellow,
        R.string.filter_bleak,
        R.string.filter_memory,
        R.string.filter_pure,
        R.string.filter_calm,
        R.string.filter_autumn,
        R.string.filter_fantasy,
        R.string.filter_freedom,
        R.string.filter_mild,
        R.string.filter_prairie,
        R.string.filter_deep,
        R.string.filter_glow,
        R.string.filter_memoir,
        R.string.filter_mist,
        R.string.filter_vivid,
        R.string.filter_chill,
        R.string.filter_pinky,
        R.string.filter_adventure
    };

    private final int[] TabToolItemText = new int[] {
            R.string.tool_adjust,
            R.string.tool_brightness,
            R.string.tool_contrast,
            R.string.tool_beauty,
            R.string.tool_structure,
            R.string.tool_warmth,
            R.string.tool_saturation,
            R.string.tool_color,
            R.string.tool_fade,
            R.string.tool_highlights,
            R.string.tool_shadows,
            R.string.tool_vignette,
            R.string.tool_tilt_shift,
            R.string.tool_sharpen
    };

    private final int[] TabToolItemImg = new int[]{
            R.drawable.tool_adjust,
            R.drawable.tool_brightness,
            R.drawable.tool_contrast,
            R.drawable.tool_beauty,
            R.drawable.tool_sharpen_2,
            R.drawable.tool_warmth,
            R.drawable.tool_saturation,
            R.drawable.tool_color,
            R.drawable.tool_fade,
            R.drawable.tool_highlights,
            R.drawable.tool_shadows,
            R.drawable.tool_vignette,
            R.drawable.tool_blur,
            R.drawable.tool_sharpen
    };

    private final int[] thumbResources = new int[]{
            R.drawable.filter_original,
            R.drawable.filter_rich,
            R.drawable.filter_warm,
            R.drawable.filter_soft,
            R.drawable.filter_rose,
            R.drawable.filter_morning,
            R.drawable.filter_sunshine,
            R.drawable.filter_sunset,
            R.drawable.filter_cool,
            R.drawable.filter_freeze,
            R.drawable.filter_ocean,
            R.drawable.filter_dream,
            R.drawable.filter_violet,
            R.drawable.filter_mellow,
            R.drawable.filter_bleak,
            R.drawable.filter_memory,
            R.drawable.filter_pure,
            R.drawable.filter_calm,
            R.drawable.filter_autumn,
            R.drawable.filter_fantasy,
            R.drawable.filter_freedom,
            R.drawable.filter_mild,
            R.drawable.filter_prairie,
            R.drawable.filter_deep,
            R.drawable.filter_glow,
            R.drawable.filter_memoir,
            R.drawable.filter_mist,
            R.drawable.filter_vivid,
            R.drawable.filter_chill,
            R.drawable.filter_pink,
            R.drawable.filter_adventure
    };


    private DisplayMetrics mDisplayMetrics;
    private Story17Application mApplication;

    private FrameLayout myFilterViewLayout;
    private GLSurfaceView filterGLSurfaceView;
    private MyFilter filterRenderer;
    private LinearLayout bottomContainer, effectCategoryBar, filterContainerLayout, toolContainerLayout, ToolBottomContainer;
    private LinearLayout sliderContainer;
    private HorizontalScrollView svFilter;
    public static Bitmap oldBmp, finalBmp;
    private Map<String, ImageView> filterPreviewImgList = new HashMap<>();
    private boolean isShowFilterControlBar = false;
    private int filterScrollPosition = 0;
    private int toolScrollPosition = 0;

    private int imageDisplaySize;
    private int yOffSet = 0;
    private int wViewPort = 0; // ViewPort width
    private int hViewPort = 0; // viewPort height
    private boolean isCreateFinalPic = false;
    private boolean isFirstTimeIn = true;

    //===============================================
    // View Component
    //===============================================
    private View mFilterBottomView;
    //===============================================
    // Title Bar
    //===============================================
    private ImageView OriginalImg, imgTabFilters, imgTabTool, imgTabAutoEnhance;//
    private RelativeLayout mTitleBar;
    private TextView mTitle;
    private ImageView lImg;
    private Button rBtn;
    //===============================================
    // Slider
    //===============================================
    private View sliderControlView;
//    private Slider sliderControlTool;
    private FilterControlSeekBar filterControlBar;
    private TextView sliderValue;
    private ImageView imgSliderBtnCancel, imgSliderBtnCheck;
    private RelativeLayout ControlBoxAreaCancel, ControlBoxAreaCheck;
    //===============================================
    // Color Map
    //===============================================
    private LinearLayout colorTool_Shadow, colorTool_Highlight;
    private TextView tool_shadow, tool_highlight;
    private ImageView imgLastShadowView, imgLastHighlightView;
    //===============================================
    // Tilt Effect Drawing Component
    //===============================================
    private TiltEffectDrawingView tiltEffectDrawingView;
    private boolean tiltFlag = false;
    //===============================================
    // Progress
    //===============================================
    private ProgressDialog progressBar;
    //===============================================

    private float filterMixPercentage = 1.0f;
    private float autoEnhanceValue = 0.0f;
    private float brightnessValue = 0.0f;
    private float contrastValue = 0.0f;
    private float warmthValue = 0.0f;
    private float saturationValue = 0.0f;
    private float highlightsValue = 0.0f;
    private float shadowsValue = 0.0f;
    private float vignetteValue = 0.0f;
    private float sharpenValue = 0.0f;
    private float shadowsColorMapIndex = 0.0f;
    private float highlightsColorMapIndex = 0.0f;
    private float colorShadowsValue = 0.0f;
    private float colorHighlightsValue = 0.0f;
    private float tiltShiftMode = 0.0f;
    private float tiltShiftRadialCenterX = 0.0f;
    private float tiltShiftRadialCenterY = 0.0f;
    private float tiltShiftRadialRadius = 0.0f;
    private float tiltShiftLinearCenterX = 0.0f;
    private float tiltShiftLinearCenterY = 0.0f;
    private float tiltShiftLinearRadius = 0.0f;
    private float tiltShiftLinearAngle = 0.0f;
    private float currentEffectValue = 0.0f;
    private float currentFilterValue = 1.0f;
    private String currentEffectName = "";
    private String currentFilterName = "";
    private Map<String, Float> currentEffect = new HashMap();
    private Map<String, Float> currentFilter = new HashMap();
    private Bundle bundle;
//    private float autoEnhanceValue = 0.0f;
    //============================
    // Image
    //============================
    private boolean rotageImage = true;
    //============================
    private int isCame = 0;
    private Boolean STATE = true;

    //event tracking
    private int startFilterPage = 0;
    private int effect1_index =0;
    private int effect3_index =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_filter_view_controller);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = CameraFilterViewController.this.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e) {

        }

        // Create Pictures Folder if it's not exist
        File PictureFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath());
        if(!PictureFolder.exists()){
            PictureFolder.mkdir();
        }

        mApplication = (Story17Application) getApplication();
        // Get Display size
        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        // Resize FrameLayout
        imageDisplaySize = mDisplayMetrics.widthPixels;
        OriginalImg = (ImageView) findViewById(R.id.OriginalImg);
        myFilterViewLayout = (FrameLayout) findViewById(R.id.myFilterView);
        myFilterViewLayout.getLayoutParams().height = imageDisplaySize;
        myFilterViewLayout.getLayoutParams().width = imageDisplaySize;

        // Setup Tilt Effect Drawing View
        tiltEffectDrawingView = (TiltEffectDrawingView)findViewById(R.id.tiltSurfaceView);
        tiltEffectDrawingView.addListener(tiltEventCallBack);

        // Setup bottomContainer
        bottomContainer = (LinearLayout) findViewById(R.id.BottomContainer);

        // Init glsurfaceview
        filterGLSurfaceView = (GLSurfaceView) findViewById(R.id.FilterGLSurfaceView);
        filterGLSurfaceView.setPreserveEGLContextOnPause(true);
        filterGLSurfaceView.setOnTouchListener(myTouchEvent);

        if(imageDisplaySize < filterRenderer.TARGET_IMAGE_SIZE) {

            filterGLSurfaceView.getLayoutParams().width = wViewPort = 1080;
            filterGLSurfaceView.getLayoutParams().height = hViewPort = 1080;
            yOffSet = 1080 - imageDisplaySize;

        }else{

            filterGLSurfaceView.getLayoutParams().width = wViewPort = imageDisplaySize;
            filterGLSurfaceView.getLayoutParams().height = hViewPort = imageDisplaySize;
            yOffSet = 0;

        }

//        filterGLSurfaceView.setX(imageDisplaySize);

        // Setup Imgage
//        OriginalImg = (ImageView) findViewById(R.id.OriginalImg);
        String lFilePath = getIntent().getExtras().getString("file");
        isCame = getIntent().getExtras().getInt("isCame");

        try {
            oldBmp = BitmapFactory.decodeFile(lFilePath);

            filterRenderer = new MyFilter(this, imageDisplaySize, imageDisplaySize,isCame);

//            OriginalImg.setImageBitmap(oldBmp);
            //==================================
            // Test code : img crop a wrong size
            //==================================
//            final String _file = "/sdcard/Pictures/oldBmp.jpg";
//            FileOutputStream outStream = new FileOutputStream(_file);
//            oldBmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
//            outStream.close();
            //==================================
////            newBmp = Bitmap.createScaledBitmap(oldBmp, 640, 640, true);
//            //newBmp = Bitmap.createScaledBitmap(oldBmp, imageDisplaySize, imageDisplaySize, true);
//            //previewBmp = BitmapFactory.decodeFile(getIntent().getExtras().getString("file2"));
//            previewBmp = Bitmap.createScaledBitmap(oldBmp, 100, 100, false);
//            filterRenderer.setPreviewImg(previewBmp);
//            filterRenderer.setDisplaySize(imageDisplaySize, imageDisplaySize);
            filterGLSurfaceView.setEGLContextClientVersion(2);
            filterGLSurfaceView.setRenderer(filterRenderer);
            filterGLSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
            filterRenderer.loadInputImage(oldBmp);//, "inputImageTexture");
            if(isCame == 1) filterRenderer.enqueueSetRotation(0, 1);
            else if(isCame == 0) filterRenderer.enqueueSetRotation(180, 1);
            else filterRenderer.enqueueSetRotation(-90, 1);
            filterRenderer.setViewPort(imageDisplaySize,imageDisplaySize, yOffSet);
            filterRenderer.setRenderListener(renderCallback);
//            //oldBmp.recycle();
//
//            //newBmp.recycle();

        } catch(Exception ee){
            Singleton.log("CameraFilterViewController Error : " + ee.getMessage());
        }

        initTitleBar();
//                previewGLSurfaceview = new GLSurfaceView(CameraFilterViewController.this);
//                previewRender = new MyFilter(this);
//        new Runnable() {
//            @Override
//            public void run() {
//                previewRender.initWithImageSize(previewBmp);
//
//                previewRender.setPreviewImgFlag(true);
//                previewRender.addListener(renderNotify);
//                previewRender.doCreatePreviewImage();
//            }
//        }.run();

        //event tracking
        startFilterPage = Singleton.getCurrentTimestamp();
        try{
            LogEventUtil.EnterFilterPage(CameraFilterViewController.this,mApplication);
        }catch (Exception x)
        {

        }

        bottomContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                bottomContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                //must initialize here
                initBottomContainer();
                initSliderControlTool();
                initValue();
                initColorMapTool();
            }
        });

        new Runnable(){

            @Override
            public void run() {
                progressBar = ProgressDialog.show(CameraFilterViewController.this, "", getText(R.string.processing),true);
            }
        }.run();
        isFirstTimeIn = true;

        // If filter renderer crash, can use it to send data to next activity.
        bundle = getIntent().getExtras();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        filterGLSurfaceView.setAlpha(1f);
        OriginalImg.setImageBitmap(filterRenderer.getOutputBitmap());
        filterGLSurfaceView.setAlpha(1f);
        filterGLSurfaceView.requestRender();
        if(resultCode == 2)
        {
            mApplication.setReLoad(true);
            finish();
        }
        else if(resultCode == 3){
            mApplication.setShowLoader(true);
            mApplication.setReLoad(true);
            finish();
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        filterGLSurfaceView.onResume();

        MobclickAgent.onPageStart(CameraFilterViewController.this.getClass().getSimpleName());
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        filterGLSurfaceView.onPause();

        MobclickAgent.onPageEnd(CameraFilterViewController.this.getClass().getSimpleName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            filterRenderer.dealloc();
            System.gc();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void finish() {
        try{
            filterRenderer.dealloc();
            System.gc();
        }catch (Exception e){
            e.printStackTrace();
        }
        super.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(CameraFilterViewController.this, CameraActivity.class));
        this.finish();
    }

    private void initValue(){
        currentFilter.put(MyFilter.FilterName.NORMAL_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.RICH_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.WARM_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.SOFT_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.ROSE_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.MORNING_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.SUNSHINE_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.SUNSET_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.COOL_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.FREEZE_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.OCEAN_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.DREAM_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.VIOLET_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.MELLOW_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.BLEAK_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.MEMORY_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.PURE_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.CALM_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.AUTUMN_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.FANTASY_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.FREEDOM_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.MILD_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.PRAIRIE_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.DEEP_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.GLOW_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.MEMOIR_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.MIST_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.VIVID_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.CHILL_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.PINKY_FILTER, 1.0f);
        currentFilter.put(MyFilter.FilterName.ADVENTURE_FILTER, 1.0f);

        currentEffect.put(MyFilter.ParameterName.AUTO_ENHANCE_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.BRIGHTNESS_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.CONTRAST_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.AUTO_ENHANCE_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.UNSHARPMASK_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.SKINSMOOTH_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.WARMTH_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.SATURATION_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.FADE_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.HIGHLIGHTS_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.SHADOWS_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.VIGNETTE_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.SHARPEN_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.TILT_SHIFT_MODE_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.TILT_SHIFT_RADIAL_CENTER_X_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.TILT_SHIFT_RADIAL_CENTER_Y_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.TILT_SHIFT_RADIAL_RADIUS_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.TILT_SHIFT_LINEAR_CENTER_X_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.TILT_SHIFT_LINEAR_CENTER_Y_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.TILT_SHIFT_LINEAR_RADIUS_PARAMETER, 0.0f);
        currentEffect.put(MyFilter.ParameterName.TILT_SHIFT_LINEAR_ANGLE_PARAMETER, 0.0f);
    }

    private void initTitleBar()
    {
        mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.title_filter_name));
        mTitle.setTextColor(Color.WHITE);

        lImg = (ImageView) findViewById(R.id.img_left);
        lImg.setImageResource(R.drawable.btn_rrow_selector);
        lImg.setVisibility(View.VISIBLE);
        lImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //event tracking
                try{
                    LogEventUtil.LeaveFilterPage(CameraFilterViewController.this,mApplication,Singleton.getCurrentTimestamp()-startFilterPage,false);
                }catch (Exception x)
                {

                }

                startActivity(new Intent(CameraFilterViewController.this, CameraActivity.class));
                finish();
            }
        });

        rBtn = (Button) findViewById(R.id.btn_right);
        rBtn.setText(getString(R.string.next));
        rBtn.setTextColor(Color.WHITE);
        rBtn.setVisibility(View.VISIBLE);
        rBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(!STATE) return;
                    mHandler.sendEmptyMessage(6);
                    isCreateFinalPic = true;
                    if(isCame == 1) filterRenderer.enqueueSetRotation(180, 0);
                    else if(isCame == 0) filterRenderer.enqueueSetRotation(0, 0);
                    else filterRenderer.enqueueSetRotation(90, 0);
                    filterRenderer.setViewPort(imageDisplaySize, imageDisplaySize, 0);
                    filterGLSurfaceView.requestRender();
                    STATE = false;
                    if(Build.VERSION.SDK_INT > 20) progressBar = ProgressDialog.show(CameraFilterViewController.this,"", getString(R.string.processing),true);

                    //event tracking
                    try{
                        if((effect1_index == 0) && (effect3_index == 0)) {
                            LogEventUtil.LeaveFilterPage(CameraFilterViewController.this, mApplication, Singleton.getCurrentTimestamp() - startFilterPage, false);
                        }
                        else{
                            LogEventUtil.LeaveFilterPage(CameraFilterViewController.this, mApplication, Singleton.getCurrentTimestamp() - startFilterPage, true);
                        }
                    }catch (Exception x)
                    {

                    }

                } catch(Exception e){
//                    Log.e(dTag, "prepare upload image Error : " + e.getMessage());
                }
            }
        });
    }

    private void initBottomContainer(){
        LayoutInflater mFilterViewInflater = LayoutInflater.from(this);
        mFilterBottomView = mFilterViewInflater.inflate(R.layout.filter_bottom_container, null);
        bottomContainer.addView(mFilterBottomView);

        //Image Button & touch area
        imgTabFilters = (ImageView)mFilterBottomView.findViewById(R.id.imgFilter);
        imgTabAutoEnhance =(ImageView)mFilterBottomView.findViewById(R.id.imgAutoEnhance);
        imgTabTool = (ImageView)mFilterBottomView.findViewById(R.id.imgTool);
        RelativeLayout FilterButtonArea = (RelativeLayout)mFilterBottomView.findViewById(R.id.FilterButtonArea);
        RelativeLayout AutoEnhanceButtonArea = (RelativeLayout)mFilterBottomView.findViewById(R.id.AutoEnhanceButtonArea);
        RelativeLayout ToolButtonArea = (RelativeLayout)mFilterBottomView.findViewById(R.id.ToolButtonArea);
        FilterButtonArea.setOnClickListener(FilterEffectClickEvent);
        AutoEnhanceButtonArea.setOnClickListener(FilterEffectClickEvent);
        ToolButtonArea.setOnClickListener(FilterEffectClickEvent);

        //Scroll View
        svFilter = (HorizontalScrollView) mFilterBottomView.findViewById(R.id.FilterScrollView);
        svFilter.setHorizontalScrollBarEnabled(false);

        // Filter img and text
        filterContainerLayout = (LinearLayout)mFilterBottomView.findViewById(R.id.FilterItem);
        LayoutInflater mInflater;
        mInflater = LayoutInflater.from(this);
        for (int i = 0 ; i < TabFilterItemText.length ; i++){
            View view = mInflater.inflate(R.layout.filter_item_view, null);
            ImageView img = (ImageView) view.findViewById(R.id.FilterImg);
            img.getLayoutParams().height = (int)Math.round(0.3 * bottomContainer.getHeight());
            img.getLayoutParams().width = (int)Math.round(0.3 * bottomContainer.getHeight());
            img.setImageResource(thumbResources[i]);
            TextView txt = (TextView) view.findViewById(R.id.FilterText);
            filterPreviewImgList.put(MyFilter.FilterName.allNames().get(i), img);
            txt.setText(getText(TabFilterItemText[i]));
//            txt.setTextAppearance(this, R.style.FilterName);
            txt.setTextColor(getResources().getColor(R.color.pk_white));

            //LinearLayout showSelectColor = (LinearLayout) view.findViewById(R.id.show_select_color);

            view.setId(11100 + i);

            final int index = i;
            //view.setOnClickListener(FilterEffectClickEvent);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!currentFilterName.equals(MyFilter.FilterName.allNames().get(index))){
                        currentFilterName = MyFilter.FilterName.allNames().get(index);

                        filterRenderer.setCurrentFilter(currentFilterName, currentFilter.get(currentFilterName));
//                        filterRenderer.loadFilter(currentFilterName);
                        filterGLSurfaceView.requestRender();

                        if(!isShowFilterControlBar && !currentFilterName.equals(MyFilter.FilterName.NORMAL_FILTER)){
                            isShowFilterControlBar = true;
                        }
                    }
                    else{
                        if(!currentFilterName.equals(MyFilter.FilterName.NORMAL_FILTER)) {
                            currentFilterValue = currentFilter.get(currentFilterName);
                            setSliderMode(true, true, currentFilter.get(currentFilterName) * 100f);
                            isShowFilterControlBar = true;
                            setTitlebar(true, getText(TabFilterItemText[index]).toString());
                        }
                    }
                    effect1_index = index;
                }
            });
            filterContainerLayout.addView(view);
        }

        filterContainerLayout.setVisibility(View.VISIBLE);

        // Tool img and text
        toolContainerLayout = (LinearLayout)mFilterBottomView.findViewById(R.id.ToolItem);
//        toolContainerLayout.setGravity((Gravity.CENTER_VERTICAL));
        for (int i = 0 ; i < TabToolItemText.length ; i++){
            View view = mInflater.inflate(R.layout.filter_item_view, null);
            ImageView img = (ImageView) view.findViewById(R.id.FilterImg);
            img.getLayoutParams().height = (int)Math.round(0.3 * bottomContainer.getHeight());
            img.getLayoutParams().width = (int)Math.round(0.3 * bottomContainer.getHeight());
            TextView txt = (TextView) view.findViewById(R.id.FilterText);
            img.setImageBitmap(getFilterImg(TabToolItemImg[i]));
            txt.setText(getText(TabToolItemText[i]));
            txt.setTextColor(getResources().getColor(R.color.pk_white));
            view.setId(11200 + i);
            view.setOnClickListener(FilterEffectClickEvent);

            if(i==0 || i==12){
                view.setVisibility(View.GONE);
            }

            toolContainerLayout.addView(view);
        }

/*
        Bitmap FilterBmp = getToolBarImg(R.drawable.tab_filters_active);
        Bitmap BrightnessBmp = getToolBarImg(R.drawable.tab_lux);
        Bitmap ToolBmp = getToolBarImg(R.drawable.tab_tool);

        effectCategoryBar = new LinearLayout(this);
        effectCategoryBar.setOrientation(LinearLayout.HORIZONTAL);
        effectCategoryBar.setBackgroundColor(Color.WHITE);
        bottomContainer.addView(effectCategoryBar);

        ViewGroup.LayoutParams EffectLayoutParams = effectCategoryBar.getLayoutParams();
        EffectLayoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        EffectLayoutParams.height = FilterBmp.getHeight()+50;
        effectCategoryBar.setLayoutParams(EffectLayoutParams);
        effectCategoryBar.setGravity(Gravity.CENTER_VERTICAL);


        imgTabFilters = new ImageView(this);
        imgTabAutoEnhance = new ImageView(this);
        imgTabTool = new ImageView(this);

        imgTabFilters.setImageBitmap(FilterBmp);
        imgTabAutoEnhance.setImageBitmap(BrightnessBmp);
        imgTabTool.setImageBitmap(ToolBmp);

        effectCategoryBar.addView(imgTabFilters);
        effectCategoryBar.addView(imgTabAutoEnhance);
        effectCategoryBar.addView(imgTabTool);

        // Set Tab Bar Icons Position
        int avgWidth = mDisplayMetrics.widthPixels/4;

        LinearLayout.LayoutParams lFilterParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lFilterParams.setMargins(avgWidth - FilterBmp.getWidth() / 2, 0, 0, 0);
        imgTabFilters.setLayoutParams(lFilterParams);

        LinearLayout.LayoutParams lBrightnessParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lBrightnessParams.setMargins(avgWidth - BrightnessBmp.getWidth(), 0, 0, 0);
        imgTabAutoEnhance.setLayoutParams(lBrightnessParams);

        LinearLayout.LayoutParams lToolParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lToolParams.setMargins(avgWidth - ToolBmp.getWidth(), 0, 0, 0);
        imgTabTool.setLayoutParams(lToolParams);

        // Set Click Listener
        imgTabFilters.setId(10000);
        imgTabFilters.setOnClickListener(FilterEffectClickEvent);
        imgTabAutoEnhance.setId(10001);
        imgTabAutoEnhance.setOnClickListener(FilterEffectClickEvent);
        imgTabTool.setId(10002);
        imgTabTool.setOnClickListener(FilterEffectClickEvent);

        svFilter = new HorizontalScrollView(this);
        svFilter.setBackgroundColor(Color.argb(255, 227, 227, 227));
        LinearLayout.LayoutParams ScrollParams = new LinearLayout.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, ScrollView.LayoutParams.MATCH_PARENT);
        ScrollParams.gravity = Gravity.CENTER_VERTICAL;
        svFilter.setLayoutParams(ScrollParams);
        svFilter.setHorizontalScrollBarEnabled(false);

        bottomContainer.addView(svFilter);
        filterContainerLayout = new LinearLayout(this);
        filterContainerLayout.setGravity(Gravity.CENTER_VERTICAL);
        svFilter.addView(filterContainerLayout);

        // Filter img and text
        LayoutInflater mInflater;
        mInflater = LayoutInflater.from(this);
        for (int i = 0 ; i < TabFilterItemText.length ; i++){
            View view = mInflater.inflate(R.layout.filter_item_view, filterContainerLayout, false);
            ImageView img = (ImageView) view.findViewById(R.id.FilterImg);
            TextView txt = (TextView) view.findViewById(R.id.FilterText);
            filterPreviewImgList.put(MyFilter.FilterName.allNames.get(i), img);
            txt.setText(getText(TabFilterItemText[i]));
            txt.setTextAppearance(this, R.style.FilterName);
            view.setId(11100 + i);
            final int index = i;
            //view.setOnClickListener(FilterEffectClickEvent);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!currentFilterName.equals(MyFilter.FilterName.allNames.get(index))){
                        currentFilterName = MyFilter.FilterName.allNames.get(index);

                        filterRenderer.setCurrentFilter(currentFilterName, currentFilter.get(currentFilterName));
                        filterGLSurfaceView.requestRender();

                        if(!isShowFilterControlBar && !currentFilterName.equals(MyFilter.FilterName.NORMAL_FILTER)){
                            isShowFilterControlBar = true;
                        }
                    }
                    else{
                        if(!currentFilterName.equals(MyFilter.FilterName.NORMAL_FILTER)) {
                            setSliderMode(true, true, currentFilter.get(currentFilterName) * 100f);
                            isShowFilterControlBar = true;
                            setTitlebar(true, getText(TabFilterItemText[index]).toString());
                        }
                    }


                }
            });
            filterContainerLayout.addView(view);
        }
        filterContainerLayout.setVisibility(View.INVISIBLE);

        // Tool img and text
        toolContainerLayout = new LinearLayout(this);
        toolContainerLayout.setGravity((Gravity.CENTER_VERTICAL));
        for (int i = 0 ; i < TabToolItemText.length ; i++){
            View view = mInflater.inflate(R.layout.filter_item_view, toolContainerLayout, false);
            ImageView img = (ImageView) view.findViewById(R.id.FilterImg);
            TextView txt = (TextView) view.findViewById(R.id.FilterText);
            img.setImageBitmap(getFilterImg(TabToolItemImg[i]));
            txt.setText(getText(TabToolItemText[i]));
            txt.setTextAppearance(this, R.style.FilterName);
            view.setId(11200 + i);
            view.setOnClickListener(FilterEffectClickEvent);

            if(i==0 || i==12){
                view.setVisibility(View.GONE);
            }

            toolContainerLayout.addView(view);
        }

*/
    }

    private void initSliderControlTool(){
        LayoutInflater mInflater;
        mInflater = LayoutInflater.from(this);
        sliderControlView = mInflater.inflate(R.layout.filter_seekbar_control_box, null);

        // Seek Bar
        filterControlBar = (FilterControlSeekBar)sliderControlView.findViewById(R.id.filterControlBar);
        sliderValue = (TextView)sliderControlView.findViewById(R.id.slider_tv_continuous);
        filterControlBar.setRange(0f, 100f);
        filterControlBar.setOnValueChanged(new FilterControlSeekBar.OnValueChangeListener() {
            @Override
            public void onValueChanged(float newValue) {
                if (!isShowFilterControlBar) {
//                    currentEffectValue = newValue;
                    filterRenderer.setCurrentParameter(currentEffectName, newValue);
                    currentEffect.put(currentEffectName, newValue);
                    if(currentEffect.equals(MyFilter.ParameterName.AUTO_ENHANCE_PARAMETER)){
                        autoEnhanceValue = newValue;
                    }
                } else {
                    // adjust filter mix percentage
//                    currentFilterValue = newValue / 100.0f;
                    filterRenderer.setCurrentFilter(currentFilterName, newValue / 100.0f);
                    currentFilter.put(currentFilterName, newValue / 100.0f);
                }
                filterGLSurfaceView.requestRender();
            }
        });

        // Cancel Button
        ControlBoxAreaCancel = (RelativeLayout)sliderControlView.findViewById(R.id.FilterControlBoxAreaCancel);
        ControlBoxAreaCancel.setOnClickListener(FilterEffectClickEvent);
        // Check Button
        ControlBoxAreaCheck = (RelativeLayout)sliderControlView.findViewById(R.id.FilterControlBoxAreaCheck);
        ControlBoxAreaCheck.setOnClickListener(FilterEffectClickEvent);

        ToolBottomContainer = new LinearLayout(this);
        View view = mInflater.inflate(R.layout.filter_buttom_view, ToolBottomContainer, false);

        imgSliderBtnCancel = (ImageView) view.findViewById(R.id.imgSliderCancel);
        imgSliderBtnCancel.setId(10003);
        imgSliderBtnCancel.setOnClickListener(FilterEffectClickEvent);
        imgSliderBtnCheck = (ImageView) view.findViewById(R.id.imgSliderOK);
        imgSliderBtnCheck.setId(10004);
        imgSliderBtnCheck.setOnClickListener(FilterEffectClickEvent);

        LinearLayout.LayoutParams ToolBottomParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        ToolBottomParams.gravity = Gravity.CENTER_VERTICAL|Gravity.BOTTOM;
        ToolBottomParams.weight = 7;
        ToolBottomContainer.setLayoutParams(ToolBottomParams);
        ToolBottomContainer.setOrientation(LinearLayout.HORIZONTAL);
        ToolBottomContainer.addView(view);

        /*
        LinearLayout.LayoutParams ToolParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        ToolParams.gravity = Gravity.CENTER|Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
        ToolParams.weight = 3;
        sliderControlView = mInflater.inflate(R.layout.slider_control_tool, null);
        sliderControlView.setLayoutParams(ToolParams);
        sliderControlView.setBackgroundColor(Color.argb(255, 227, 227, 227));

        //=====================================
        // Seek Bar
        //=====================================
        filterControlBar = (FilterControlSeekBar)sliderControlView.findViewById(R.id.filterControlBar);
        sliderValue = (TextView)sliderControlView.findViewById(R.id.slider_tv_continuous);
        sliderContainer = (LinearLayout)sliderControlView.findViewById(R.id.slider_parent);
        filterControlBar.setRange(0f, 100f);



        ToolBottomContainer = new LinearLayout(this);
        View view = mInflater.inflate(R.layout.filter_buttom_view, ToolBottomContainer, false);

//        imgSliderBtnCancel = new ImageView(this);
        imgSliderBtnCancel = (ImageView) view.findViewById(R.id.imgSliderCancel);
//        imgSliderBtnCancel.setImageResource(R.drawable.nav_cancel);
        imgSliderBtnCancel.setId(10003);
        imgSliderBtnCancel.setOnClickListener(FilterEffectClickEvent);
//        imgSliderBtnCheck = new ImageView(this);
        imgSliderBtnCheck = (ImageView) view.findViewById(R.id.imgSliderOK);
//        imgSliderBtnCheck.setImageResource(R.drawable.nav_check);
        imgSliderBtnCheck.setId(10004);
        imgSliderBtnCheck.setOnClickListener(FilterEffectClickEvent);
//        ToolBottomContainer.addView(imgSliderBtnCancel);
//        ToolBottomContainer.addView(imgSliderBtnCheck);


        LinearLayout.LayoutParams ToolBottomParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        ToolBottomParams.gravity = Gravity.CENTER_VERTICAL|Gravity.BOTTOM;
//        ToolBottomParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
//        ToolBottomParams.height = imgSliderBtnCancel.getHeight() + 60;
        ToolBottomParams.weight = 7;
        ToolBottomContainer.setLayoutParams(ToolBottomParams);
        ToolBottomContainer.setOrientation(LinearLayout.HORIZONTAL);
        ToolBottomContainer.addView(view);


        filterControlBar.setOnValueChanged(new FilterControlSeekBar.OnValueChangeListener() {
            @Override
            public void onValueChanged(float newValue) {
                if(!isShowFilterControlBar) {
                    currentEffectValue = newValue;
                    filterRenderer.setCurrentParameter(currentEffectName, currentEffectValue);
                }
                else{
                    // adjust filter mix percentage
                    currentFilterValue = newValue / 100.0f;
                    filterRenderer.setCurrentFilter(currentFilterName, currentFilterValue);
                }
                filterGLSurfaceView.requestRender();
            }
        });

        sliderContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //sliderControlTool.onTouchEvent(event);
                return true;
            }
        });
        //((LinearLayout)sliderControlView.getParent()).removeAllViews();

        */

    }

    private void initColorMapTool(){
        LayoutInflater mInflater;
        mInflater = LayoutInflater.from(this);
        LinearLayout.LayoutParams ColorMapParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        ColorMapParams.gravity = Gravity.CENTER|Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
        ColorMapParams.weight = 3;

        colorTool_Shadow = new LinearLayout(this);
        colorTool_Shadow.setLayoutParams(ColorMapParams);
        colorTool_Shadow.setBackgroundColor(getResources().getColor(R.color.filter_bg));
        colorTool_Shadow.setGravity((Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL|Gravity.CENTER));

        View shadowView = mInflater.inflate(R.layout.tool_color_map_view, colorTool_Shadow, false);
        LinearLayout colorMapContainer = (LinearLayout)shadowView.findViewById(R.id.colormap_contrainer);

        tool_highlight = (TextView)shadowView.findViewById(R.id.tool_highlight);
//        tool_highlight.setId(11300);
        tool_highlight.setTextColor(getResources().getColor(R.color.primary_content_color));
        tool_highlight.setOnClickListener(ToolEffectClickEvent);

        for(int i = 1 ; i<= 9 ; i++){
            ImageView img = new ImageView(this);
            switch (i){
                case 1 :
                    img.setImageResource(R.drawable.color_dark_01_active);
                    img.setId(11302);
                    imgLastShadowView = img;
                    imgLastShadowView.setTag(R.drawable.color_dark_01);
                    //imgLastShadowView.setTag(1, R.drawable.color_dark_01_active);
                    break;
                case 2 :
                    img.setImageResource(R.drawable.color_dark_02);
                    img.setId(11303);
                    break;
                case 3 :
                    img.setImageResource(R.drawable.color_dark_03);
                    img.setId(11304);
                    break;
                case 4 :
                    img.setImageResource(R.drawable.color_dark_04);
                    img.setId(11305);
                    break;
                case 5 :
                    img.setImageResource(R.drawable.color_dark_05);
                    img.setId(11306);
                    break;
                case 6 :
                    img.setImageResource(R.drawable.color_dark_06);
                    img.setId(11307);
                    break;
                case 7 :
                    img.setImageResource(R.drawable.color_dark_07);
                    img.setId(11308);
                    break;
                case 8 :
                    img.setImageResource(R.drawable.color_dark_08);
                    img.setId(11309);
                    break;
                case 9 :
                    img.setImageResource(R.drawable.color_dark_09);
                    img.setId(11310);
                    break;
                default:
                    break;
            }
            img.setOnClickListener(ToolEffectClickEvent);
            colorMapContainer.addView(img);
        }
        colorTool_Shadow.addView(shadowView);

        colorTool_Highlight = new LinearLayout(this);
        colorTool_Highlight.setLayoutParams(ColorMapParams);
        colorTool_Highlight.setBackgroundColor(getResources().getColor(R.color.filter_bg));
        colorTool_Highlight.setGravity((Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL|Gravity.CENTER));
        View highlightView = mInflater.inflate(R.layout.tool_color_map_view, colorTool_Highlight, false);
        colorMapContainer = (LinearLayout)highlightView.findViewById(R.id.colormap_contrainer);
        tool_shadow = (TextView) highlightView.findViewById(R.id.tool_shadow);
//        tool_shadow.setId(11301);
        tool_shadow.setTextColor(getResources().getColor(R.color.primary_content_color));
        tool_shadow.setOnClickListener(ToolEffectClickEvent);

        for(int i = 1 ; i<= 9 ; i++){
            ImageView img = new ImageView(this);
            switch (i){
                case 1 :
                    img.setImageResource(R.drawable.color_light_01_active);
                    img.setId(11311);
                    imgLastHighlightView = img;
                    imgLastHighlightView.setTag(R.drawable.color_light_01);
                    break;
                case 2 :
                    img.setImageResource(R.drawable.color_light_02);
                    img.setId(11312);
                    break;
                case 3 :
                    img.setImageResource(R.drawable.color_light_03);
                    img.setId(11313);
                    break;
                case 4 :
                    img.setImageResource(R.drawable.color_light_04);
                    img.setId(11314);
                    break;
                case 5 :
                    img.setImageResource(R.drawable.color_light_05);
                    img.setId(11315);
                    break;
                case 6 :
                    img.setImageResource(R.drawable.color_light_06);
                    img.setId(11316);
                    break;
                case 7 :
                    img.setImageResource(R.drawable.color_light_07);
                    img.setId(11317);
                    break;
                case 8 :
                    img.setImageResource(R.drawable.color_light_08);
                    img.setId(11318);
                    break;
                case 9 :
                    img.setImageResource(R.drawable.color_light_09);
                    img.setId(11319);
                    break;
                default:
                    break;
            }
            img.setOnClickListener(ToolEffectClickEvent);
            colorMapContainer.addView(img);
        }
        colorTool_Highlight.addView(highlightView);

    }

    //type true : start from 0
    //type false : start from -100
    public void setSliderMode(boolean mode, boolean type, float value){
        bottomContainer.removeAllViews();
        if(mode){ //on
            bottomContainer.addView(sliderControlView);
//            bottomContainer.addView(ToolBottomContainer);
            if(type){
                filterControlBar.setRange(0f, 100f);
            }else{
                filterControlBar.setRange(-100f, 100f);
            }

            filterControlBar.setValue(value);
        }else{ //off
            bottomContainer.addView(mFilterBottomView);
//            bottomContainer.addView(effectCategoryBar);
//            bottomContainer.addView(svFilter);
//            svFilter.setVerticalScrollbarPosition(0);
        }
    }

    private void setTitlebar(boolean hideButton){
        setTitlebar(hideButton, getString(R.string.title_filter_name));
    }

    public void setTitlebar(boolean hideButton, String titleName){
        mTitle.setText(titleName);
        if(hideButton){ //hide
            lImg.setVisibility(View.INVISIBLE);
            rBtn.setVisibility(View.INVISIBLE);
        }else{ //show
            lImg.setVisibility(View.VISIBLE);
            rBtn.setVisibility(View.VISIBLE);
        }
    }

    private RenderEventListener renderCallback = new RenderEventListener(){

        @Override
        public void onDrawFrameFinish() {
//            if (rotageImage) {
//                rotageImage = false;
//                finalBmp = filterRenderer.getOutputBitmap();
//                mHandler.sendEmptyMessage(8);
//                filterRenderer.enqueueSetRotation(180, 1);
//                filterRenderer.setViewPort(1080, 1080, yOffSet);
//                filterGLSurfaceView.requestRender();
//            }
            if(isFirstTimeIn){
                isFirstTimeIn = false;
                try {
                    progressBar.dismiss();
                }
                catch(Exception e){}
            }
            if(isCreateFinalPic){
                isCreateFinalPic = false;
                finalBmp = filterRenderer.getOutputBitmap();
                mHandler.sendEmptyMessage(8);

                final String name = Singleton.getUUIDFileName("jpg");

                try {
                        final String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/." + Singleton.applicationContext.getPackageName() + "/" + name;

                        FileOutputStream outStream = new FileOutputStream(file);
                        finalBmp.compress(Bitmap.CompressFormat.JPEG, 95, outStream);
                        outStream.close();

                        Bitmap thumbBitmap = Bitmap.createScaledBitmap(finalBmp, 640, 640, true);


                        final String thumbImageFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/." + Singleton.applicationContext.getPackageName() + "/" + "THUMBNAIL_" + name;

                        outStream = new FileOutputStream(thumbImageFileName);
                        thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 95, outStream);
                        outStream.close();

                        thumbBitmap.recycle();
                        thumbBitmap = null;

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if(progressBar!=null) progressBar.dismiss();
                            }
                        });

//                        filterGLSurfaceView.setAlpha(0f);

                        STATE = true;
                        Intent intent = new Intent();
                        intent.setClass(CameraFilterViewController.this, CameraPostActivity.class);
                        intent.putExtra("file", file);
                        intent.putExtra("file2", thumbImageFileName);
                        intent.putExtra("name", name);
                        intent.putExtra("name2", "THUMBNAIL_" + name);
                        intent.putExtra("filter", true);
                        startActivityForResult(intent, 1);

                    //============================================
                    // Write to Pictures
                    //============================================

                        final String _file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + name;
                        outStream = new FileOutputStream(_file);
                        finalBmp.compress(Bitmap.CompressFormat.JPEG, 95, outStream);
                        outStream.close();

//                        final String _file2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + "THUMBNAIL_" + name;
//                        outStream2 = new FileOutputStream(_file2);
//                        bit.compress(Bitmap.CompressFormat.JPEG, 95, outStream2);
//                        outStream2.close();
                        //============================================
                        Bundle bundle = new Bundle();
                        bundle.putString("name", name);
                        Message msg = new Message();
                        msg.what = 4;
                        msg.setData(bundle);
                        mHandler.sendMessage(msg);


                        finalBmp.recycle();
                        finalBmp = null;
                }catch(Exception e)
                {

                }
            }

        }

        @Override
        public void onErrorOccurred() {

        }
    };


    private Bitmap getToolBarImg(int resourceID){
        Bitmap bm = BitmapFactory.decodeResource(getResources(), resourceID);
//        int new_size = (int) (0.2 * bottomContainer.getHeight());
//        Bitmap bmm = Bitmap.createScaledBitmap(bm, new_size, new_size, false);
        return bm;
    }

    private Bitmap getFilterImg(int resourceID){
        Bitmap bm = BitmapFactory.decodeResource(getResources(), resourceID);
//        int new_size = (int)(0.3 * bottomContainer.getHeight());
//        Bitmap bmm = Bitmap.createScaledBitmap(bm, new_size, new_size, false);
        return bm;
    }

    private Bitmap getFilterImg(String path){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        int new_size = (int)(0.3 * bottomContainer.getHeight());
        return bitmap; //Bitmap.createScaledBitmap(bitmap, new_size, new_size, false);
    }

    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            switch(msg.what){
                case 1:
//                    File file = null;
//                    Uri uriPath = null;
                    String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/." + Singleton.applicationContext.getPackageName() + "/FILTER_PREVIEW_";

                    for(String filterName : filterPreviewImgList.keySet()){
//                        file = new File(filePath + filterName + ".jpg");
//                        uriPath = Uri.fromFile(file);
                        ((ImageView)filterPreviewImgList.get(filterName)).setImageBitmap(getFilterImg(filePath + filterName + ".jpg"));
                    }
                    filterGLSurfaceView.setX(0);
                    filterContainerLayout.setVisibility(View.VISIBLE);
                    if(progressBar!=null) progressBar.dismiss();

                    break;
                case 2:
//                    filterGLSurfaceView.setTranslationY((float) imageDisplaySize);
//                    filterGLSurfaceView.animate().setDuration(10).alpha(0f).start();
                    //reset All
                    filterRenderer.enqueueResetToolParams();
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.NORMAL_FILTER, 1f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 3:
//                    filterGLSurfaceView.setTranslationY(0f);
//                    filterGLSurfaceView.animate().setDuration(50).alpha(1.0f).start();
                    //restore
                    filterRenderer.setCurrentFilter(currentFilterName, currentFilterValue);
                    for(String key : currentEffect.keySet()){
                        filterRenderer.setCurrentParameter(key, currentEffect.get(key));
                        filterGLSurfaceView.requestRender();
//                        Log.e("DDD", "effecr Name : " + key + ", Value : " + currentEffect.get(key));
                    }
                    break;
                case 4:
                    Bundle _bundle = msg.getData();
                    String name = _bundle.getString("name");
                    ArrayList<String> filesToUpload = new ArrayList<String>();
                    filesToUpload.add(name);
                    filesToUpload.add("THUMBNAIL_" + name);
                    mApplication.setUpLoad(CameraFilterViewController.this,filesToUpload);
                    break;
                case 5:
                    if(imageDisplaySize < filterRenderer.TARGET_IMAGE_SIZE) {
                        filterGLSurfaceView.getLayoutParams().width = imageDisplaySize;
                        filterGLSurfaceView.getLayoutParams().height = imageDisplaySize;
                    }
//                    ViewGroup.LayoutParams layoutParams=filterGLSurfaceView.getLayoutParams();
//                    layoutParams.width=imageDisplaySize;
//                    layoutParams.height=imageDisplaySize;
//                    filterGLSurfaceView.setLayoutParams(layoutParams);
                    break;
                case 6:
//                    if(imageDisplaySize < filterRenderer.TARGET_IMAGE_SIZE) {
//                        filterGLSurfaceView.getLayoutParams().width = MyFilter.TARGET_IMAGE_SIZE;
//                        filterGLSurfaceView.getLayoutParams().height = MyFilter.TARGET_IMAGE_SIZE;
//                    }
                    // Restore GLSurfaceview display size
//                    ViewGroup.LayoutParams layoutParams1=filterGLSurfaceView.getLayoutParams();
//                    layoutParams1.width=1080;
//                    layoutParams1.height=1080;
//                    filterGLSurfaceView.setLayoutParams(layoutParams1);
                    break;
                case 7:

                    if(progressBar!=null) progressBar.dismiss();
                    FileOutputStream outStream = null;
                    STATE = true;
                    Intent intent = new Intent();
                    intent.setClass(CameraFilterViewController.this, CameraPostActivity.class);
                    intent.putExtra("file", bundle.getString("file"));
                    intent.putExtra("file2", bundle.getString("file2"));
                    intent.putExtra("name", bundle.getString("name"));
                    intent.putExtra("name2", "THUMBNAIL_" + bundle.getString("name"));
                    intent.putExtra("filter", false);
                    startActivity(intent);

                    //============================================
                    // Write to Pictures
                    //============================================

                    try {
                        final String _file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + bundle.getString("name");
                        outStream = new FileOutputStream(_file);
                        oldBmp.compress(Bitmap.CompressFormat.JPEG, 95, outStream);
                        outStream.close();
                    }
                    catch(Exception e){}
                    //            final String _file2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + "THUMBNAIL_" + name;
                    //            outStream2 = new FileOutputStream(_file2);
                    //            bit.compress(Bitmap.CompressFormat.JPEG, 95, outStream2);
                    //            outStream2.close();
                    //============================================
                    ArrayList<String> _filesToUpload = new ArrayList<String>();
                    _filesToUpload.add(bundle.getString("name"));
                    _filesToUpload.add("THUMBNAIL_" + bundle.getString("name"));
                    mApplication.setUpLoad(CameraFilterViewController.this, _filesToUpload);

                    filterGLSurfaceView.onPause();
                    finish();
                    break;
                case 8:
                    if(null != finalBmp) {
                        OriginalImg.setImageBitmap(finalBmp);
                    }
                    filterGLSurfaceView.setAlpha(0f);
                    break;
            }
        }
    };


    View.OnClickListener FilterEffectClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            effect3_index = 1;

            switch (v.getId()){
                case R.id.FilterButtonArea:
                    isShowFilterControlBar = true;
                    toolScrollPosition = svFilter.getScrollX();

                    try{
//                        svFilter.removeAllViews();
//                        svFilter.addView(filterContainerLayout);
                        toolContainerLayout.setVisibility(View.GONE);
                        filterContainerLayout.setVisibility(View.VISIBLE);

                        imgTabFilters.setImageBitmap(getToolBarImg(R.drawable.tab_filters_active));
//                        imgTabAutoEnhance.setImageBitmap(getMediumImg(R.drawable.tab_lux));
                        imgTabTool.setImageBitmap(getToolBarImg(R.drawable.tab_tool));
                    }catch(Exception e){}


                    svFilter.setScrollX(filterScrollPosition);
                    setTitlebar(false);
                    break;
                case R.id.AutoEnhanceButtonArea:
                    isShowFilterControlBar = false;
                    currentEffectName = MyFilter.ParameterName.AUTO_ENHANCE_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, true, currentEffectValue);
                    setTitlebar(true, getText(R.string.tool_auto_enhance).toString());
                    break;
                case R.id.ToolButtonArea:
                    isShowFilterControlBar = false;
                    filterScrollPosition = svFilter.getScrollX();
                    try{
//                        svFilter.removeAllViews();
//                        svFilter.addView(toolContainerLayout);
                        toolContainerLayout.setVisibility(View.VISIBLE);
                        filterContainerLayout.setVisibility(View.GONE);

                        imgTabFilters.setImageBitmap(getToolBarImg(R.drawable.tab_filters));
//                        imgTabAutoEnhance.setImageBitmap(getMediumImg(R.drawable.tab_lux));
                        imgTabTool.setImageBitmap(getToolBarImg(R.drawable.tab_tool_active));
                    }catch(Exception e){}

                    svFilter.setScrollX(toolScrollPosition);
                    setTitlebar(false);
                    break;
                case 10003:
                case R.id.FilterControlBoxAreaCancel: //imgSliderBtnCancel
                    setSliderMode(false, false, 0);
                    if(!isShowFilterControlBar) {
                        try {
                            if (currentEffect != null) {
                                filterRenderer.setCurrentParameter(currentEffectName, currentEffectValue);
                                currentEffect.put(currentEffectName, currentEffectValue);
                            }
                        } catch (Exception e) {
                        }
                    }
                    else{
//                        currentFilterValue = currentFilter.get(currentFilterName);
                        if(currentEffectName.equals(MyFilter.ParameterName.AUTO_ENHANCE_PARAMETER))
                        {
                            currentEffectValue = autoEnhanceValue;
                        }
                        filterRenderer.setCurrentFilter(currentFilterName, currentFilterValue);
                        currentFilter.put(currentFilterName, currentFilterValue);
                        isShowFilterControlBar = false;
                    }
                    // reset color map
                    filterRenderer.resetColorMap();
                    filterGLSurfaceView.requestRender();
                    setTitlebar(false);
                    break;
                case 10004:
                case R.id.FilterControlBoxAreaCheck: //imgSliderBtnCheck
                    setSliderMode(false, false, 0);

                    if(!isShowFilterControlBar) {
//                        currentEffect.put(currentEffectName, currentEffectValue);
                        currentEffectValue = currentEffect.get(currentEffectName);
                        //filterRenderer.setCurrentParameter(currentEffectName, currentEffectValue);
                    }
                    else{
                        // finish filter edit mode
//                        currentFilter.put(currentFilterName, currentFilterValue);
                        //filterRenderer.setCurrentFilter(currentFilterName, currentFilterValue);
                        currentFilterValue = currentFilter.get(currentFilterName);
                        isShowFilterControlBar = false;
                    }

                    filterRenderer.updateColopMapRecord();
                    filterGLSurfaceView.requestRender();
                    setTitlebar(false);
                    break;
                /*
                case 11100: //原始
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.NORMAL_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11101: //薄霧
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.RICH_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11102: //晨光
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.WARM_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11103: //鮮明
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.VIVID_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11104: //平靜
//                    filterRenderer.setCurrentFilter(MyFilter.FilterName.PIECE_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11105: //暖陽
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.WARM_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11106: //1917
                    //filterRenderer.setCurrentFilter(MyFilter.FilterName.MY_1911_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11107: //微甜
                    //filterRenderer.setCurrentFilter(MyFilter.FilterName.SWEET_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11108: //溫柔
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.SOFT_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11109: //觸動
                    //filterRenderer.setCurrentFilter(MyFilter.FilterName.TOUCH_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11110: //微寒
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.CHILL_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11111: //海洋
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.OCEAN_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11112: //冒險
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.ADVENTURE_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11113: //冰霜
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.FREEZE_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11114: //記憶
                    filterRenderer.setCurrentFilter(MyFilter.FilterName.MEMORY_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11115: //純粹
                    //filterRenderer.setCurrentFilter(MyFilter.FilterName.PURITY_FILTER);
                    filterGLSurfaceView.requestRender();
                    break;
                */
                case 11200: //ADJUST
                    setSliderMode(true, false, contrastValue);
                    setTitlebar(true, getString(R.string.tool_adjust));
                    break;
                case 11201: //BRIGHTNESS
                    currentEffectName = MyFilter.ParameterName.BRIGHTNESS_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, false, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_brightness));
                    break;
                case 11202: //CONTRAST
                    currentEffectName = MyFilter.ParameterName.CONTRAST_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, false, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_contrast));
                    break;
                case 11203: //BEAUTY
                    currentEffectName = MyFilter.ParameterName.SKINSMOOTH_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, true, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_beauty));
                    break;
                case 11204: //STRUCTURE
                    currentEffectName = MyFilter.ParameterName.UNSHARPMASK_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, true, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_structure));
                    break;
                case 11205: //WARMTH
                    currentEffectName = MyFilter.ParameterName.WARMTH_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, false, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_warmth));
                    break;
                case 11206: //SATURATION
                    currentEffectName = MyFilter.ParameterName.SATURATION_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, false, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_saturation));
                    break;
                case 11207: //COLOR
                    currentEffectName = MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER;
                    currentEffectValue = 1.0f;
                    bottomContainer.removeAllViews();
                    bottomContainer.addView(colorTool_Shadow);
                    bottomContainer.addView(ToolBottomContainer);
                    tool_highlight.setTextColor(getResources().getColor(R.color.primary_content_color));
                    tool_shadow.setTextColor(getResources().getColor(R.color.pk_white));
                    setTitlebar(true, getString(R.string.tool_color));
                    break;
                case 11208: //FADE
                    currentEffectName = MyFilter.ParameterName.FADE_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, true, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_fade));
                    break;
                case 11209: //HIGHLIGHTS
                    currentEffectName = MyFilter.ParameterName.HIGHLIGHTS_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, false, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_highlights));
                    break;
                case 11210: //SHADOWS
                    currentEffectName = MyFilter.ParameterName.SHADOWS_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, false, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_shadows));
                    break;
                case 11211: //VIGNETTE
                    currentEffectName = MyFilter.ParameterName.VIGNETTE_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, true, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_vignette));
                    break;
                case 11212: //TILT SHIFT
                    if(!tiltFlag){
                        tiltEffectDrawingView.setVisibility(View.VISIBLE);
                        tiltFlag = true;
                    }else{
                        tiltEffectDrawingView.setVisibility(View.INVISIBLE);
                        tiltFlag = false;
                    }
//                    setTitlebar(true, TabToolItemText[9]);
                    break;
                case 11213: //SHARPEN
                    currentEffectName = MyFilter.ParameterName.SHARPEN_PARAMETER;
                    currentEffectValue = currentEffect.get(currentEffectName);
                    setSliderMode(true, true, currentEffectValue);
                    setTitlebar(true, getString(R.string.tool_sharpen));
                    break;
                default:
                    effect3_index = 0;
//                    Log.d("17_g","effect3_index = "+effect3_index);
                    break;
            }
        }
    };

    View.OnClickListener ToolEffectClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.tool_highlight: //text highlight
                    currentEffectName = MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER;
                    bottomContainer.removeAllViews();
                    bottomContainer.addView(colorTool_Highlight);
                    bottomContainer.addView(ToolBottomContainer);
                    tool_highlight.setTextColor(getResources().getColor(R.color.primary_color));
                    tool_shadow.setTextColor(getResources().getColor(R.color.primary_content_color));
                    break;
                case R.id.tool_shadow: //text shadow
                    bottomContainer.removeAllViews();
                    bottomContainer.addView(colorTool_Shadow);
                    bottomContainer.addView(ToolBottomContainer);
                    tool_highlight.setTextColor(getResources().getColor(R.color.primary_content_color));
                    tool_shadow.setTextColor(getResources().getColor(R.color.primary_color));
                    break;
                case 11302: //color_dark_01
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_01_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_01);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 0.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 0.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11303: //color_dark_02
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_02_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_02);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 1.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11304: //color_dark_03
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_03_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_03);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 2.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11305: //color_dark_04
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_04_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_04);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 3.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11306: //color_dark_05
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_05_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_05);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 4.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11307: //color_dark_06
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_06_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_06);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 5.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11308: //color_dark_07
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_07_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_07);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 6.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11309: //color_dark_08
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_08_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_08);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 7.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11310: //color_dark_09
                    imgLastShadowView.setImageResource((int) imgLastShadowView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_dark_09_active);

                    imgLastShadowView = (ImageView) v;
                    imgLastShadowView.setTag(R.drawable.color_dark_09);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 8.0f,
                            MyFilter.ParameterName.COLOR_SHADOWS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11311: //color_light_01
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_01_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_01);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 0.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 0.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11312: //color_light_02
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_02_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_02);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 1.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11313: //color_light_03
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_03_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_03);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 2.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11314: //color_light_04
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_04_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_04);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 3.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11315: //color_light_05
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_05_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_05);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 4.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11316: //color_light_06
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_06_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_06);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 5.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11317: //color_light_07
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_07_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_07);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 6.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11318: //color_light_08
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_08_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_08);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 7.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                case 11319: //color_light_09
                    imgLastHighlightView.setImageResource((int) imgLastHighlightView.getTag());
                    ((ImageView)v).setImageResource(R.drawable.color_light_09_active);

                    imgLastHighlightView = (ImageView) v;
                    imgLastHighlightView.setTag(R.drawable.color_light_09);

                    filterRenderer.setColorMapParameter(
                            MyFilter.ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 8.0f,
                            MyFilter.ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 50.0f);
                    filterGLSurfaceView.requestRender();
                    break;
                default:
                    break;
            }
        }
    };

    // Hide or show original image
    private View.OnTouchListener myTouchEvent = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()){
                case MotionEvent.ACTION_DOWN:
//                    Log.d(dTag, "Touch DOWN");
                    mHandler.sendEmptyMessage(2);
                    break;
                case MotionEvent.ACTION_UP:
//                    Log.d(dTag, "Touch UP");
                    mHandler.sendEmptyMessage(3);
                    break;
                default:
                    break;
            }
            return true;
        }
    };

    TiltEventListener tiltEventCallBack = new TiltEventListener(){

        @Override
        public void updateTiltEffect(int type, float centerX, float centerY, float radius, float angle) {
            filterRenderer.setTiltParameter(type, centerX, centerY, radius, angle);
            filterGLSurfaceView.requestRender();


        }

    };


}