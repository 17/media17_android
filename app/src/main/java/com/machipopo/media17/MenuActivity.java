package com.machipopo.media17;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.flurry.android.FlurryAgent;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.model.RevenueModel;
import com.machipopo.media17.utils.DevUtils;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by POPO on 5/26/15.
 */
public class MenuActivity extends FragmentActivity
{
    private MenuActivity mCtx = this;
    private FragmentTabHost tabHost;
    private ImageView mTab1, mTab2, mTab3, mTab4, mTab5;

    private Boolean mLoad = false;

    private int pos = 0;

    public Story17Application mApplication;
    private ArrayList<RevenueModel> mRevenueModel = new ArrayList<RevenueModel>();

    private long lastPress = 0;

    private Handler mHandler;
    private TextView mToTop;
    private HomeFragment mHomeFragment;
    private PullToRefreshListView mListView;

    private ArrayList<HashMap<String, String>> mData;

    private Dialog CamDialog;
    public Boolean mOpen = false;
    public ImageView mBadge;

//    private LocationManager mLocationManager;
//    private MyLocationListener mMyLocationListener;

    private ImageView mXmas;

    public void onStart()
    {
        super.onStart();
        FlurryAgent.onStartSession(this, Constants.FLURRY_API_KEY);
    }
    public void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.main_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();

        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(mCtx, getSupportFragmentManager(), R.id.tabcontent);

        tabHost.getTabWidget().setPadding(0, 0, 0, -10);

        mHomeFragment = new HomeFragment();
        tabHost.addTab(tabHost.newTabSpec("Home").setIndicator("Home"), mHomeFragment.getClass(), null);
        tabHost.addTab(tabHost.newTabSpec("Search").setIndicator("Search"), SearchFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("Camera").setIndicator("Camera"), CameraFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("Notifi").setIndicator("Notifi"), NotifiFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("Profile").setIndicator("Profile"), ProfileFragment.class, null);

        mTab1 = (ImageView) findViewById(R.id.tab1);
        mTab2 = (ImageView) findViewById(R.id.tab2);
        mTab3 = (ImageView) findViewById(R.id.tab3);
        mTab4 = (ImageView) findViewById(R.id.tab4);
        mTab5 = (ImageView) findViewById(R.id.tab5);
        mBadge = (ImageView) findViewById(R.id.badge);
        mToTop = (TextView) findViewById(R.id.to_top);

        mXmas = (ImageView) findViewById(R.id.xmas);

        tabHost.setCurrentTab(0);
        setTab(tabHost.getCurrentTab());

        tabHost.setOnTabChangedListener(new FragmentTabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if (tabId.contains("Home")) {
                    pos = 0;
                    setTab(0);
                } else if (tabId.contains("Search")) {
                    pos = 1;
                    setTab(1);
                } else if (tabId.contains("Camera")) setTab(2);
                else if (tabId.contains("Notifi")) {
                    pos = 3;
                    setTab(3);
                } else if (tabId.contains("Profile")) {
                    pos = 4;
                    setTab(4);
                }
            }
        });

        mToTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    if(mListView!=null) mListView.getRefreshableView().setSelection(0);
//                }
//                catch (Exception e) {
//                }
            }
        });
//        mTab1.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                mHomeFragment.mListView.scrollTo(0, mHomeFragment.mListView.getHeight());
//            }
//        });

        mXmas.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(CamDialog!=null) CamDialog = null;
                CamDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                CamDialog.setContentView(R.layout.dialog_camera_menu);
                Window window = CamDialog.getWindow();
                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                ImageView mCamera = (ImageView) CamDialog.findViewById(R.id.camera);
                ImageView mVideo = (ImageView) CamDialog.findViewById(R.id.video);
                ImageView mLive = (ImageView) CamDialog.findViewById(R.id.live);
                ImageView mGrop = (ImageView) CamDialog.findViewById(R.id.livegrop);
                mGrop.setVisibility(View.GONE);
                mCamera.startAnimation(getTopShowAnimation());
                mVideo.startAnimation(getTopShowAnimation());
                mLive.startAnimation(getDownShowAnimation());
                mGrop.startAnimation(getDownShowAnimation());

                mCamera.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        CamDialog.dismiss();

                        mOpen = true;
                        Intent intent = new Intent();
                        intent.setClass(mCtx, CameraActivity.class);
                        startActivity(intent);
                    }
                });

                mVideo.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        CamDialog.dismiss();

                        mOpen = true;
                        Intent intent = new Intent();
                        intent.setClass(mCtx, VideoActivity.class);
                        startActivity(intent);
                    }
                });

                mLive.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        CamDialog.dismiss();

                        //event tracking
                        try {
                            LogEventUtil.EnterPublishLivePage(mCtx, mApplication);
                        }catch (Exception x)
                        {

                        }

                        mOpen = true;
                        Intent intent = new Intent();
                        intent.setClass(mCtx,LiveBroadcastActivity.class);
                        intent.putExtra("user",Singleton.preferences.getString(Constants.USER_ID, ""));
                        startActivity(intent);
                    }
                });

//                String day = "123 %s 456";
//                String end = String.format(day,"aaa");
//                %s string int
//                %d int
                mGrop.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        CamDialog.dismiss();
                        mOpen = true;
                        Intent intent = new Intent();
                        intent.setClass(mCtx,LiveStreamGroupActivity.class);
                        startActivity(intent);
                    }
                });

                CamDialog.show();


//                Intent intent = new Intent();
//                intent.setClass(mCtx, CameraActivity.class);
//                startActivity(intent);
            }
        });

//        mTab2.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                AnimationSet mAnimationSet = new AnimationSet(true);
//
//                ScaleAnimation scale = new ScaleAnimation(1.0f, 1.3f, 1.0f, 1.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                scale.setDuration(100);
//                mAnimationSet.addAnimation(scale);
//                mAnimationSet.setAnimationListener(new Animation.AnimationListener()
//                {
//                    @Override
//                    public void onAnimationStart(Animation animation)
//                    {
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation)
//                    {
//                        AnimationSet mAnimationSet = new AnimationSet(true);
//
//                        ScaleAnimation scale = new ScaleAnimation(1.3f, 1.0f, 1.3f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                        scale.setDuration(200);
//
//                        mAnimationSet.addAnimation(scale);
//                        mTab2.startAnimation(mAnimationSet);
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation)
//                    {
//                    }
//                });
//
//                mTab2.startAnimation(mAnimationSet);
//
//                pos = 1;
//                setTab(1);
//            }
//        });

        mApplication.getSelfInfo(this);//,mBadge
        mApplication.getRevenueMoney(this,Singleton.preferences.getString(Constants.USER_ID, ""));
        mApplication.GoLogin(this,getConfig(Constants.ACCOUNT, ""),getConfig(Constants.PASSWORD, ""));

//        try{
//            if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(mCtx)==ConnectionResult.SUCCESS) gcm();
//        }
//        catch(Exception e){
//        }

        uploadPhoneNumber();
        updateDevice();
        checkUserIdentity();

        baidu();
        mApplication.updateGiftAmimation(this);
        mApplication.updatePointProduct(this);
        mApplication.checkIAPreUpload(this);

        Singleton.preferenceEditor.putInt(Constants.LOGOUT, 0).commit();

//        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        mMyLocationListener = new MyLocationListener();
//        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 180000, 0, mMyLocationListener);

//        mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if(mApplication.getFromBranch())
        {
            mApplication.setFromBranch(false);
            if(mApplication.getFromPage().compareTo("live")==0)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, LiveStreamActivity.class);
                intent.putExtra("liveStreamID", Integer.valueOf(mApplication.getFromID()));
                //event tracking: to track what page user enter livestream
                intent.putExtra("enterLiveFrom","Menu");
                startActivity(intent);
            }
            else if(mApplication.getFromPage().compareTo("u")==0)
            {
                if(mApplication.getFromID().compareTo(Singleton.preferences.getString(Constants.OPEN_ID, ""))!=0)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, HomeUserActivity.class);
                    intent.putExtra("open_id", mApplication.getFromID());
                    startActivity(intent);
                }
            }
            else if(mApplication.getFromPage().compareTo("p")==0)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, PhotoActivity.class);
                intent.putExtra("post_id", mApplication.getFromID());
                startActivity(intent);
            }
        }

        if(Build.VERSION.SDK_INT >= 23)
        {
            ArrayList<String> mPermission = new ArrayList<String>();

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.CAMERA);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.RECORD_AUDIO);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.READ_CONTACTS);
            }

            final String[] permission = new String[mPermission.size()];
            for(int i = 0 ; i < mPermission.size() ; i++)
            {
                permission[i] = mPermission.get(i);
            }

            if(permission.length!=0)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                builder.setMessage(getString(R.string.check_permission_title));
                builder.setTitle(getString(R.string.system_notif));
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(mCtx, permission, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
                    }
                }).setCancelable(false);
                builder.create().show();
            }
        }

        try {
            if(!Constants.INTERNATIONAL_VERSION)
            {
                Singleton.preferenceEditor.putInt(Constants.CURRENCY_RATE_SETTED,1);
                Singleton.preferenceEditor.putString(Constants.CURRENCY_RATE,"CNY");
                Singleton.preferenceEditor.commit();
            }
        }
        catch (Exception e) {

        }

        if(mApplication.showUpdateDialog) {
            mApplication.showUpdateDialog = false;
            ApiManager.getNewestAppVersion(mCtx, new ApiManager.GetNewestAppVersionCallback() {
                @Override
                public void onResult(boolean success, String newestVersion, final String downloadURL) {
                    if(success && newestVersion.length()!=0 && downloadURL.length()!=0)
                    {
                        if(newestVersion.compareTo(Singleton.getVersion())!=0)
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                            if(Constants.INTERNATIONAL_VERSION) builder.setMessage("17 App Have New Version, Update？");
                            else builder.setMessage("17有新版本，是否进行更新？");
                            builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        Uri uri = Uri.parse(downloadURL);
                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                    catch (Exception e) {
                                    }
                                }
                            });
                            builder.setNegativeButton(R.string.cancel,null);
                            builder.create().show();
                        }
                    }
                }
            });
        }

        mApplication.setSignOutState(false);
        mApplication.setGuestModeState(false);
        mApplication.setGuestFinish(false);
        mApplication.setMenuFinish(false);

        try{
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
            mHashMap.put("version", Singleton.getVersion());
            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
            mHashMap.put("u", Singleton.getPhoneIMEI());
            mHashMap.put("dn", DevUtils.getDevInfo());
            LogEventUtil.sendSDKLogEvent(mCtx, "v26_menu_activity", mHashMap);
        }
        catch (Exception e){
        }

        if(!Constants.INTERNATIONAL_VERSION){
            updateIMEI();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        doNext(requestCode, grantResults);
    }

    private final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 1;

    private void doNext(int requestCode, int[] grantResults)
    {
        if (requestCode == WRITE_EXTERNAL_STORAGE_REQUEST_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                // Permission Granted
            }
            else
            {
                // Permission Denied
            }
        }
    }

    private class MyLocationListener implements LocationListener
    {
        @Override
        public void onLocationChanged(Location location)
        {
            if(location!=null)
            {
                mApplication.setLongitude(location.getLongitude());
                mApplication.setLatitude(location.getLatitude());
            }
        }

        @Override
        public void onProviderDisabled(String provider)
        {
        }

        @Override
        public void onProviderEnabled(String provider)
        {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
        }
    }

    private List<Address> getAddressbyGeoPoint(Location location)
    {
        List<Address> result = null ;

        try
        {
            if(location != null)
            {
                Geocoder gc = new Geocoder( this , Locale.getDefault());
                result= gc.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            }
        }
        catch (Exception e)
        {

        }

        return result;
    }

    AnimationSet mTopShowAnimationSet;
    private AnimationSet getTopShowAnimation()
    {
        if(mTopShowAnimationSet==null)
        {
            mTopShowAnimationSet = new AnimationSet(true);
            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            scale.setDuration(300);
            AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
            alpha.setDuration(300);
            RotateAnimation rotate = new RotateAnimation(-90.0f, 0.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);

//        TranslateAnimation translate = new TranslateAnimation(0, 0, -960, 0);
//        translate.setDuration(500);
//        mAnimationSet.addAnimation(translate);

            mTopShowAnimationSet.addAnimation(scale);
            mTopShowAnimationSet.addAnimation(rotate);
            mTopShowAnimationSet.addAnimation(alpha);

            return mTopShowAnimationSet;
        }
        else
        {
            return mTopShowAnimationSet;
        }
    }

    AnimationSet mDownShowAnimationSet;
    private AnimationSet getDownShowAnimation()
    {
        if(mDownShowAnimationSet==null)
        {
            mDownShowAnimationSet = new AnimationSet(true);
            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            scale.setDuration(300);
            AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
            alpha.setDuration(300);
            RotateAnimation rotate = new RotateAnimation(-90.0f, 0.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);

//        TranslateAnimation translate = new TranslateAnimation(0, 0, -960, 0);
//        translate.setDuration(500);
//        mAnimationSet.addAnimation(translate);

            mDownShowAnimationSet.addAnimation(scale);
            mDownShowAnimationSet.addAnimation(rotate);
            mDownShowAnimationSet.addAnimation(alpha);

            return mDownShowAnimationSet;
        }
        else
        {
            return mDownShowAnimationSet;
        }
    }

    private void checkUserIdentity()
    {
        try {
            ApiManager.verifyUserIdentity(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), Singleton.preferences.getString(Constants.ACCESS_TOKEN, ""), new ApiManager.VerifyUserIdentityCallback()
            {
                @Override
                public void onResult(boolean networkSuccess, boolean success)
                {
                    if(!networkSuccess) {
                        return;
                    }

                    if (!success)
                    {
                        SharedPreferences Mysettings = mCtx.getSharedPreferences("settings", 0);
                        Mysettings.edit().clear().commit();

                        Singleton.preferences.edit().clear().commit();

                        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                        builder.setMessage(getString(R.string.login_again_prompt));
                        builder.setTitle(getString(R.string.system_notif));
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                //intent.setClass(mCtx, LoginMenuActivity_V2.class);
                                intent.setClass(mCtx, LoginActivity_V3.class);
                                startActivity(intent);

                                mCtx.finish();
                            }
                        }).setCancelable(false);

                        builder.create().show();
                    }
                }
            });
        } catch (Exception e) {

        }
    }

    public String getConfig(String key)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, "");
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    private void updateIMEI(){
        try {
            JSONObject params = new JSONObject();
            params.put("IMEI", Singleton.getPhoneIMEI());

            ApiManager.updateUserInfo(this, params, new ApiManager.UpdateUserInfoCallback() {
                @Override
                public void onResult(boolean success, String message) {

                }
            });
        }
        catch (Exception e) {
        }
    }

    private void updateDevice()
    {
        // upload device model
        try {
            JSONObject params = new JSONObject();
            params.put("deviceModel", DevUtils.getDevInfo());
            params.put("deviceType", "ANDROID");
            if(Singleton.preferences.getString("baidu", "").length()!=0) params.put("pushToken", Singleton.preferences.getString("baidu", ""));
            params.put("packageName", mCtx.getClass().getPackage().getName());

            ApiManager.updateUserInfo(this, params, new ApiManager.UpdateUserInfoCallback() {
                @Override
                public void onResult(boolean success, String message) {

                }
            });
        } catch (Exception e) {

        }
    }

    public void uploadPhoneNumber()
    {
        SimpleDateFormat mDateFormat = new SimpleDateFormat("MM/dd");
        String date = mDateFormat.format(new Date((Singleton.getCurrentTimestamp())*1000L));

        if(date.compareTo(getConfig("load_phone"))!=0)
        {
            mData = new ArrayList<HashMap<String, String>>();
            try
            {
                Cursor c = mCtx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                prepareData(c);
            }
            catch (Exception e)
            {
            }

            final JSONArray mJSONArray = new JSONArray();

            for(int i = 0 ; i < mData.size() ; i++)
            {
                String num = "";

                try
                {
                    if(mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length()!=0)
                    {
                        if(mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(0,1).contains("0")) num = mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(1,mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length());
                        else num  = mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER);

                        try
                        {
                            JSONObject mObject = new JSONObject();
                            mObject.put("phone",num.replaceAll("[^\\d]",""));
                            mObject.put("name", mData.get(i).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            mJSONArray.put(mObject);

                            JSONObject mObject2 = new JSONObject();
                            mObject2.put("phone",getConfig("countryCode","886") + num.replaceAll("[^\\d]", ""));
                            mObject2.put("name", mData.get(i).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            mJSONArray.put(mObject2);
                        }
                        catch (JSONException e)
                        {

                        }
                    }
                }
                catch (Exception e)
                {

                }
            }

            if(mJSONArray.length()!=0)
            {
                String phone = getConfig("phone","");

                ApiManager.uploadPhoneNumbers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), phone, getConfig("countryCode","886"), mJSONArray, new ApiManager.UploadPhoneNumbersCallback()
                {
                    @Override
                    public void onResult(boolean success, String message)
                    {

                    }
                });
            }

            setConfig("load_phone",date);
        }
    }

    private void prepareData(Cursor c)
    {
        try
        {
            c.moveToFirst();
            while (!c.isAfterLast())
            {
                HashMap hm = new HashMap();
                hm.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                hm.put(ContactsContract.CommonDataKinds.Phone.NUMBER, c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                hm.put(ContactsContract.CommonDataKinds.Phone.PHOTO_URI, c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
                mData.add(hm);
                c.moveToNext();
            }
        }
        catch (Exception e)
        {
        }
    }

    private void setTab(int tab)
    {
//        if(tab==0)
//        {
//            mTab1.setClickable(true);
//        }
//        else mTab1.setClickable(false);

        mToTop.setVisibility(View.GONE);
        if(tab==2)
        {
            tabHost.setCurrentTab(pos);
            return ;
        }

        mTab1.setImageResource(R.drawable.dock_home);
        mTab2.setImageResource(R.drawable.dock_explore);
        mTab3.setImageResource(R.drawable.dock_camera_down);
        mTab4.setImageResource(R.drawable.dock_news);
        mTab5.setImageResource(R.drawable.dock_profile);

        switch(tab)
        {
            case 0: mTab1.setImageResource(R.drawable.dock_home_active);
                    mToTop.setVisibility(View.VISIBLE);
                break;
            case 1: mTab2.setImageResource(R.drawable.dock_explore_active);
                break;
            case 2: mTab3.setImageResource(R.drawable.dock_camera_down);
                break;
            case 3: mTab4.setImageResource(R.drawable.dock_news_active);
                break;
            case 4: mTab5.setImageResource(R.drawable.dock_profile_active);
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
//                long currentTime = System.currentTimeMillis();
//                if(currentTime - lastPress > 3000)
//                {
//                    Toast.makeText(getBaseContext(), getString(R.string.exit), Toast.LENGTH_SHORT).show();
//                    lastPress = currentTime;
//                }
//                else
//                {
                    try
                    {
//                        if(mLocationManager!=null && mMyLocationListener!=null) mLocationManager.removeUpdates(mMyLocationListener);

                        ImageLoader.getInstance().clearDiskCache();
                        ImageLoader.getInstance().clearMemoryCache();
                        if(mApplication!=null) mApplication.setMenuFinish(true);
                        try{
                            int duration = Singleton.getCurrentTimestamp() - Singleton.preferences.getInt(Constants.LOG_EVENT_OPEN_TIME, 0);
                            LogEventUtil.CloseApp(mCtx, mApplication, duration, 0);
                        }
                        catch (Exception e){
                        }
                    }
                    catch(Exception e)
                    {

                    }

                    mCtx.finish();
//                }

                return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public String getConfig(String key , String def)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, def);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        Singleton.detectIpGeoLocation(mCtx,mApplication,true);
        updateDevice();

        if(mApplication.getReLoad())
        {
            tabHost.setCurrentTab(0);
            setHomeLoadding(true);
            mApplication.setReLoad(false);
        }

        if(mApplication.getOpenLive())
        {
//            try
//            {
//                if(mHandler!=null) mHandler = null;
//                mHandler = new Handler();
//                mHandler.postDelayed(new Runnable()
//                {
//                    @Override
//                    public void run()
//                    {
//                        Intent intent = new Intent();
//                        intent.setClass(mCtx,LiveBroadcastActivity.class);
//                        intent.putExtra("mode",LiveStreamActivity.LIVE_STREAM_MODE_BROADCAST);
//                        intent.putExtra("user",mApplication.getUser().getUserID());
//                        startActivity(intent);
//                    }
//                },200);
//            }
//            catch (Exception e)
//            {
                Intent intent = new Intent();
                intent.setClass(mCtx,LiveBroadcastActivity.class);
                intent.putExtra("user",mApplication.getUser().getUserID());
                startActivity(intent);
//            }

            mApplication.setOpenLive(false);
        }

        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        Singleton.detectIpGeoLocation(mCtx,mApplication,false);
        updateDevice();

        MobclickAgent.onPause(this);
    }

    public void setHomeLoadding(Boolean load)
    {
        mLoad = load;
    }

    public Boolean getHomeLoadding()
    {
        return mLoad;
    }

    public int getTabPos()
    {
        return 0;
    }

    private void baidu()
    {
//        if(getRegistrationId(mCtx).length()==0)
//        {
            PushManager.startWork(getApplicationContext(),PushConstants.LOGIN_TYPE_API_KEY, Constants.BAIDU_ID);
//        }
    }

    public void setListView(PullToRefreshListView mListView)
    {
        this.mListView = mListView;
    }
}
