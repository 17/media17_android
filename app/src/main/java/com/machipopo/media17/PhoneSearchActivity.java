package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by POPO on 5/21/15.
 */
public class PhoneSearchActivity extends BaseActivity
{
    private PhoneSearchActivity mCtx = this;
    private ArrayList<HashMap<String, String>> mData;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_search_activity);

        initTitleBar();

        Button mPhone = (Button) findViewById(R.id.btn_phone);
        mPhone.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showProgressDialog();
//                Log.e("DDD", "before");

                mData = new ArrayList<HashMap<String, String>>();

                try {
                    Cursor c = mCtx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                    prepareData(c);
                }
                catch (Exception e){
                    hideProgressDialog();
                    try{
                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }

                final JSONArray mJSONArray = new JSONArray();
                final JSONArray mFindJSONArray = new JSONArray();

                for(int i = 0 ; i < mData.size() ; i++)
                {
                    String num = "";
                    if(mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length()!=0)
                    {
                        if(mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(0,1).contains("0")) num = mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(1,mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length());
                        else num  = mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER);

                        try
                        {
                            JSONObject mObject = new JSONObject();
                            mObject.put("phone",num.replaceAll("[^\\d]", ""));
                            mObject.put("name", mData.get(i).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            mJSONArray.put(mObject);

                            JSONObject mObject2 = new JSONObject();
                            mObject2.put("phone",getConfig("countryCode","886") + num.replaceAll("[^\\d]", ""));
                            mObject2.put("name", mData.get(i).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                            mJSONArray.put(mObject2);
                            mFindJSONArray.put(num.replaceAll("[^\\d]", ""));
                        }
                        catch (JSONException e)
                        {

                        }
                    }
                }

//                Log.e("DDD", "before api");
                if(mJSONArray.length()!=0)
                {
                    String phone = getConfig("phone","");
//                    Log.e("DDD", "before api2");
                    ApiManager.uploadPhoneNumbers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), phone, getConfig("countryCode","886"), mJSONArray, new ApiManager.UploadPhoneNumbersCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            hideProgressDialog();
                            if (success)
                            {
//                                Log.e("DDD", "upload success");
                                Intent intent = new Intent();
                                intent.setClass(mCtx,PhoneFriendActivity.class);
                                intent.putExtra("phone",mFindJSONArray.toString());
                                startActivity(intent);
                                mCtx.finish();
                            }
                            else{
//                                Log.e("DDD", "upload fail");
                                try{
//                                                showToast(getString(R.error_failed.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                    });
                }

//                String number = mData.get(0).get(ContactsContract.CommonDataKinds.Phone.NUMBER);
//                String name = mData.get(0).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
//                String photo = mData.get(0).get(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
            }
        });

        hideKeyboard();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.contact_friend));
        mTitle.setTextColor(Color.WHITE);

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.skip));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
//                intent.setClass(mCtx, FollowFriendActivity.class);
                if(Constants.INTERNATIONAL_VERSION) intent.setClass(mCtx,FacebookActivity.class);
                else intent.setClass(mCtx, FollowFriendActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    private void prepareData(Cursor c)
    {
        c.moveToFirst();
        while (!c.isAfterLast())
        {
            HashMap hm = new HashMap();
            hm.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
            hm.put(ContactsContract.CommonDataKinds.Phone.NUMBER, c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            hm.put(ContactsContract.CommonDataKinds.Phone.PHOTO_URI, c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
            mData.add(hm);
            c.moveToNext();
        }
    }

    public String getConfig(String key , String def)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, def);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
//            Intent intent = new Intent();
//            intent.setClass(mCtx,FacebookActivity.class);
//            startActivity(intent);
//            mCtx.finish();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
