package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.utils.DevUtils;
import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

/**
 * Created by POPO on 5/21/15.
 */
public class SignupPasswordActivityV2 extends BaseActivity
{
    private SharedPreferences sharedPreferences;

    private SignupPasswordActivityV2 mCtx = this;
    private EditText mPassword;
    private Button btnNext;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity_password_v2);

        initTitleBar();

        btnNext = (Button)findViewById(R.id.btn_next);
        mPassword = (EditText) findViewById(R.id.password);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNext();
            }
        });
        showKeyboard();

        //log last step that user start quiting registration
        if(SingupLastStepLog.getInstance().getmSignupLastStep().ordinal() < SingupLastStepLog.SignupLastStep.Password.ordinal()) {
            SingupLastStepLog.getInstance().setmSignupLastStep(SingupLastStepLog.SignupLastStep.Password);
        }

        try{
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
            mHashMap.put("version", Singleton.getVersion());
            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
            mHashMap.put("u", Singleton.getPhoneIMEI());
            mHashMap.put("dn", DevUtils.getDevInfo());
            LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_password_activity", mHashMap);
        }
        catch (Exception e){
        }
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.password));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        //title bar的back key
        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_back_black);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();

                Intent intent = new Intent();
                intent.setClass(mCtx, SignupActivityV2.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    private void goNext(){
        hideKeyboard();
        //account 2-20
        //passwoud 5-20
        if(mPassword.getText().toString().length()>=5)
        {
            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
            sharedPreferences.edit().putString(Constants.PASSWORD, mPassword.getText().toString().trim()).commit();

            Intent intent = new Intent();
            intent.setClass(mCtx, BindingPhoneNumberActivity.class);
            intent.putExtra("Binding", 0);  //Binding =0, 是由註冊頁面跳至綁定手機流程; Binding=1, 是由setting->rebinding phone
            startActivity(intent);
            mCtx.finish();
        }
        else {
            try{
//                          showToast(getString(R.string.password_size));
                Toast.makeText(mCtx, getString(R.string.password_size), Toast.LENGTH_SHORT).show();
            }
            catch (Exception x){
            }
        }
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intent = new Intent();
            intent.setClass(mCtx, SignupActivityV2.class);
            startActivity(intent);
            mCtx.finish();

            return true;
        }

        if(keyCode == KeyEvent.KEYCODE_ENTER){
            goNext();
        }
        return super.onKeyUp(keyCode, event);
    }
}
