package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

/**
 * Created by POPO on 15/8/21.
 */
public class RevenuePaySetActivity extends BaseActivity
{
    private RevenuePaySetActivity mCtx = this;
    private Boolean mPayState = true;
    private String uri = "https://www.paypal.com";
    private EditText mEdit;
    private ProgressBar mProgress;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_pay_set);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        initTitleBar();

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("paypal")) mPayState = mBundle.getBoolean("paypal");
        }

        ImageView mChoose = (ImageView) findViewById(R.id.choose);
        ImageView mIcon = (ImageView) findViewById(R.id.icon);
        mEdit = (EditText) findViewById(R.id.edit);
        TextView mGoweb = (TextView) findViewById(R.id.goweb);
        mGoweb.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        if(mPayState)
        {
            mChoose.setImageResource(R.drawable.paypal);
            mIcon.setImageResource(R.drawable.profile_mail);
            mEdit.setHint(getString(R.string.revenue_pay_set_paypal));
            mGoweb.setText(getString(R.string.revenue_pay_to_paypal));
            uri = "https://www.paypal.com";
            mEdit.setText(getConfig(Constants.REVENUE_PAYPAL));
        }
        else
        {
            mChoose.setImageResource(R.drawable.alipay);
            mIcon.setImageResource(R.drawable.profile_phone);
            mEdit.setHint(getString(R.string.revenue_pay_set_alipay));
            mGoweb.setText(getString(R.string.revenue_pay_to_alipay));
            uri = "https://www.alipay.com/";
            mEdit.setText(getConfig(Constants.REVENUE_ALIPAY));
        }

        mGoweb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri mUri = Uri.parse(uri);
                Intent intent = new Intent(Intent.ACTION_VIEW, mUri);
                startActivity(intent);
            }
        });

        showKeyboard();
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_account));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.INTERNATIONAL_VERSION)
                {
                    if(getConfig(Constants.REVENUE_PAYPAL).length()!=0 || getConfig(Constants.REVENUE_ALIPAY).length()!=0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, RevenuePayChooseActivity.class);
                        startActivity(intent);
                        mCtx.finish();
                    }
                    else
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, RevenuePayActivity.class);
                        startActivity(intent);
                        mCtx.finish();
                    }
                }
                else
                {
                    mCtx.finish();
                }
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.done));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();

                if (mEdit.getText().length() != 0) {
                    try {
                        JSONObject params = new JSONObject();

                        if (mPayState) {
                            params.put("paypalEmail", mEdit.getText().toString());
                            params.put("alipayPhone", "");
                        } else {
                            params.put("paypalEmail", "");
                            params.put("alipayPhone", mEdit.getText().toString());
                        }

                        mProgress.setVisibility(View.VISIBLE);
                        ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                mProgress.setVisibility(View.GONE);
                                if (success) {
                                    if (mPayState) {
                                        setConfig(Constants.REVENUE_PAYPAL, mEdit.getText().toString());
                                        setConfig(Constants.REVENUE_ALIPAY, "");
                                    } else {
                                        setConfig(Constants.REVENUE_PAYPAL, "");
                                        setConfig(Constants.REVENUE_ALIPAY, mEdit.getText().toString());
                                    }

                                    mCtx.finish();
                                } else {
                                    try {
//                                             showToast(getString(R.string.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {
                                    }
                                }
                            }
                        });
                    } catch (Exception e) {
                        try {
//                                             showToast(getString(R.string.error_failed));
                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                        } catch (Exception x) {
                        }
                        mCtx.finish();
                    }
                } else {
                    try {
//                                             showToast(getString(R.string.enter_query));
                        Toast.makeText(mCtx, getString(R.string.enter_query), Toast.LENGTH_SHORT).show();
                    } catch (Exception x) {
                    }
                }
            }
        });
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    public String getConfig(String key)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, "");
    }

    @Override
    public void onPause()
    {
        super.onPause();
        hideKeyboard();

        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if(Constants.INTERNATIONAL_VERSION)
            {
                if(getConfig(Constants.REVENUE_PAYPAL).length()!=0 || getConfig(Constants.REVENUE_ALIPAY).length()!=0)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, RevenuePayChooseActivity.class);
                    startActivity(intent);
                    mCtx.finish();
                }
                else
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, RevenuePayActivity.class);
                    startActivity(intent);
                    mCtx.finish();
                }
            }
            else
            {
                mCtx.finish();
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
