package com.machipopo.media17;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;


public class FindFriendDuringLoginActivity extends BaseActivity {
    private FindFriendDuringLoginActivity mCtx = this;
    private EditText mPhone;
    private ConnectivityManager mConnectivityManager;
    private NetworkInfo mNetworkInfo;
    private RelativeLayout btnNext;
    private TextView mCountryCode, mCountry;

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_friend_phone);

        initTitleBar();

        mPhone = (EditText) findViewById(R.id.phone);
        mCountryCode = (TextView) findViewById(R.id.country_code);
        mCountry = (TextView) findViewById(R.id.country);
        btnNext = (RelativeLayout) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(ButtonClickEventListener);

        mPhone.setText(getConfig(Constants.PROFILE_PHONE,""));
        mCountry.setText(getConfig(Constants.PROFILE_COUNTRY, ""));
        mCountryCode.setText(getConfig(Constants.PROFILE_COUNTRY_CODE,""));


    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.signup_title_picture));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.skip));
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, PhoneFriendActivity_V2.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }
    private View.OnClickListener ButtonClickEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPhone.getText().toString().length() != 0) {

                Intent intent = new Intent();
                intent.setClass(mCtx, PhoneFriendActivity_V2.class);
                intent.putExtra("SMS", 0);
                startActivity(intent);
                mCtx.finish();

            }
        }
    };

    public String getConfig(String key, String def)
    {
        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
        return settings.getString(key, def);
    }

}
