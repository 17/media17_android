package com.machipopo.media17;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.flurry.android.FlurryAgent;
import com.machipopo.media.Mp4Recorder;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.GiftModel;
import com.machipopo.media17.model.ProductModel;
import com.machipopo.media17.model.RevenueModel;
import com.machipopo.media17.model.SuggestedUsersModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.ZipUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tencent.connect.UserInfo;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.AnalyticsConfig;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import io.branch.referral.Branch;

/**
 * Created by POPO on 5/20/15.
 */
public class Story17Application extends Application
{
    private UserModel mUser;
    private Boolean mReLoad = false;
    private Boolean mLogout = false;

//    private Boolean mLogin = false;

//    private float mTotal = 0;
//    private float mToday_Total = 0;

    private Boolean isgetMoney = false;
    private ArrayList<RevenueModel> mRevenueModel = new ArrayList<RevenueModel>();

    private int mUpLoadFile1 = 0;
    private int mUpLoadFile2 = 0;
    private int mUpLoadFile3 = 0;
    private String mUpLoadFileName = "";
    private String mUpLoadFileName2 = "";
    private Boolean mUpLoadState = true;

    private int mLoader = 0;
    private Boolean mLoadEnd = true;
    private ArrayList<String> mLoaderFiles;
    private Boolean mShowLoader = false;

    private String mPostType = "image";
    private Boolean mEncodeState = false;

    private int mCommentCount = 0;

    private Boolean mGripFollow = false;
    private Boolean mToastState = false;

    private int mIsFollow = 0;
    private Boolean mOpenLive = false;

    public Boolean isVerifie = false;
    public Boolean isUserLive = false;

    private ArrayList<SuggestedUsersModel> mSuggested = new ArrayList<SuggestedUsersModel>();
    private ArrayList<FeedModel> mFeedModel = new ArrayList<FeedModel>();

    private Boolean isSbtools = false;
    public int mRequestBadge = 0;

    private String NowRegion = "";
    private Boolean ReLoadRegion = false;

    private Double mLongitude;
    private Double mLatitude;
    private Boolean mNoShowToast = true;

    private Boolean fromBranch = false;
    private String fromPage = "";
    private String fromID = "";

    public Boolean showUpdateDialog = true;

    @Override
    public void onCreate()
    {
        super.onCreate();

        AnalyticsConfig.setAppkey(this,Constants.UMENG_ID);
        AnalyticsConfig.setChannel("");
        AnalyticsConfig.enableEncrypt(true);

        // init Flurry
        FlurryAgent.init(this, Constants.FLURRY_API_KEY);
        // configure Flurry
//        FlurryAgent.setLogEnabled(false);

        DisplayImageOptions options;
        ImageLoader imageLoader = ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder()
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
        .defaultDisplayImageOptions(options)
        .build();

        imageLoader.init(config);

        Branch.getAutoInstance(this);
    }

    public void setIsSbtools(Boolean sbtools)
    {
        this.isSbtools = sbtools;
    }

    public Boolean getIsSbtools()
    {
        return isSbtools;
    }


    public void setUser(UserModel user)
    {
        mUser = user;
    }

    public UserModel getUser()
    {
        if(mUser==null)
        {
            UserModel user = new UserModel();

            Singleton.getInstance(this);

            user.setUserID(Singleton.preferences.getString(Constants.USER_ID, ""));
            user.setName(Singleton.preferences.getString(Constants.NAME, ""));
            user.setPicture(Singleton.preferences.getString(Constants.PICTURE, ""));
            user.setGender(Singleton.preferences.getString(Constants.GENDER, ""));
            user.setBio(Singleton.preferences.getString(Constants.BIO, ""));
            user.setWebsite(Singleton.preferences.getString(Constants.WEBSITE, ""));
            user.setAge(0);
            user.setIsVerified(0);
            user.setFollowerCount(0);
            user.setFollowingCount(0);
            user.setPostCount(0);
            user.setRepostCount(0);
            user.setLikePostCount(0);
            user.setIsFollowing(0);
            user.setIsBlocked(0);
            user.setPrivacyMode(Singleton.preferences.getString(Constants.PRIVACY_MODE, ""));
            user.setOpenID(Singleton.preferences.getString(Constants.OPEN_ID, ""));
            user.setPhoneNumber(Singleton.preferences.getString(Constants.PHONE_NUMBER, ""));
            user.setCoverPhoto(Singleton.preferences.getString(Constants.COVER_PHOTO, ""));
            user.setIsChoice(0);
            user.setLastLogin(0);
            user.setCountryCode(Singleton.preferences.getString(Constants.COUNTRY_CODE, ""));
            user.setEmail(Singleton.preferences.getString(Constants.EMAIL, ""));
            user.setReceivedLikeCount(0);

            return user;
        }
        else
        {
            return mUser;
        }
    }

//    public void setLogin(Boolean mLogin)
//    {
//        this.mLogin = mLogin;
//    }
//
//    public Boolean getLogin()
//    {
//        return mLogin;
//    }

    public void setReLoad(Boolean road)
    {
        mReLoad = road;
    }

    public Boolean getReLoad()
    {
        return mReLoad;
    }

    public void setLogout(Boolean logout)
    {
        mLogout = logout;
    }

    public Boolean getLogout()
    {
        return mLogout;
    }

//    public void setTotal(float total)
//    {
//        mTotal = total;
//    }

//    public float getTotal()
//    {
//        return mTotal;
//    }

//    public void setTodayTotal(float today_total)
//    {
//        mToday_Total = today_total;
//    }

//    public float getTodayTotal()
//    {
//        return mToday_Total;
//    }

    public void setIsgetMoney(Boolean isgetMoney)
    {
        this.isgetMoney = isgetMoney;
    }

    public Boolean getIsgetMoney()
    {
        return isgetMoney;
    }

    public void setRevenueModels(ArrayList<RevenueModel> revenue)
    {
        mRevenueModel.clear();
        mRevenueModel.addAll(revenue);
    }

    public ArrayList<RevenueModel> getRevenueModels()
    {
        return mRevenueModel;
    }


    public void setUpLoad(Context context, ArrayList<String> files)
    {
        mLoader = 0;
        mUpLoadState = true;
        mLoadEnd = true;
        mLoaderFiles = files;

        mUpLoadFile1 = 0;
        mUpLoadFile2 = 0;
        mUpLoadFileName = "";

        MultiFileUploader uploader = new MultiFileUploader(files);

        uploader.startUpload(context, new MultiFileUploader.MultiFileUploaderPhotoCallback()
        {
            @Override
            public void didComplete()
            {
                mLoadEnd = true;
            }

            @Override
            public void didFail()
            {
                mLoadEnd = false;
            }

            @Override
            public void onProgress(String id, int pos)
            {
                if(mLoaderFiles.size()==2)
                {
                    if(!mUpLoadState) return;

                    if(mUpLoadFileName.length()==0)
                    {
                        mUpLoadFileName = id;
                    }
                    else if(mUpLoadFileName.compareTo(id)==0)
                    {
                        mUpLoadFile1 = pos;
                    }
                    else
                    {
                        mUpLoadFile2 = pos;
                    }

//                new runAsyncTask().execute(null,null,null);

                    double Toatle = (double)(((double)mUpLoadFile1+(double)mUpLoadFile2) / (double)200);
                    mLoader = (int)(Toatle * (double)100);

                    if(mUpLoadFile1==100 && mUpLoadFile2==100)
                    {
                        mUpLoadState = false;

                        mUpLoadFile1 = 0;
                        mUpLoadFile2 = 0;
                        mUpLoadFileName = "";
                    }
                }
                else
                {
                    if(!mUpLoadState) return;

                    if(mUpLoadFileName.length()==0)
                    {
                        mUpLoadFileName = id;
                    }
                    else if(mUpLoadFileName.length()!=0 && mUpLoadFileName2.length()==0 && mUpLoadFileName.compareTo(id)!=0)
                    {
                        mUpLoadFileName2 = id;
                    }

                    if(mUpLoadFileName.compareTo(id)==0)
                    {
                        mUpLoadFile1 = pos;
                    }
                    else if(mUpLoadFileName2.compareTo(id)==0)
                    {
                        mUpLoadFile2 = pos;
                    }
                    else
                    {
                        mUpLoadFile3 = pos;
                    }

//                new runAsyncTask().execute(null,null,null);

//                    double Toatle = (double)(((double)mUpLoadFile1+(double)mUpLoadFile2+(double)mUpLoadFile3) / (double)300);
                    double toatle1 = (double)mUpLoadFile1 / (double) 100 * (double) 0.1;
                    double toatle2 = (double)mUpLoadFile2 / (double) 100 * (double) 0.1;
                    double toatle3 = (double)mUpLoadFile3 / (double) 100 * (double) 0.8;

//                    mLoader = (int)(Toatle * (double)100);
                    mLoader = (int)((toatle1+toatle2+toatle3) * (double)100);

                    if(mUpLoadFile1==100 && mUpLoadFile2==100 && mUpLoadFile3==100)
                    {
                        mUpLoadState = false;

                        mUpLoadFile1 = 0;
                        mUpLoadFile2 = 0;
                        mUpLoadFile3 = 0;
                        mUpLoadFileName = "";
                        mUpLoadFileName2 = "";
                    }
                }
            }
        });
    }

    private class runAsyncTask extends AsyncTask<String, Integer, Boolean>
    {
        @Override
        protected Boolean doInBackground(String... text)
        {
            while(mLoader<100)
            {
                mLoader++;
                try
                {
                    Thread.sleep(100);
                }
                catch (InterruptedException e)
                {
                }
            }
            return true;
        }
    }

    public void setLoader(int loader)
    {
        mLoader = loader;
    }

    public int getUpLoader()
    {
        return mLoader;
    }

    public void setUpLoaderState(Boolean loadEnd)
    {
        mLoadEnd = loadEnd;
    }

    public Boolean getUpLoaderState()
    {
        return mLoadEnd;
    }

    public void setShowLoader(Boolean state)
    {
        mShowLoader = state;
    }

    public Boolean getShowLoader()
    {
        return mShowLoader;
    }

    public void uploadVideo(Context context, int mVideoCount,String mVideoPath,String mVideoName,ByteBuffer yuvDataBuffer,ByteBuffer audioDataBuffer,ArrayList<float[]> mAudios,String movieFilePath,Handler handler,String movieName,int audioSize)
    {
        new EncodeTask(context, mVideoCount,mVideoPath,mVideoName,yuvDataBuffer,audioDataBuffer,mAudios,movieFilePath,handler,movieName,audioSize).execute(null,null,null);
    }

    public byte[] readFile(File file) throws IOException
    {
        if(mRandomAccessFile!=null) mRandomAccessFile = null;
        mRandomAccessFile = new RandomAccessFile(file, "r");

        try
        {
            long longlength = mRandomAccessFile.length();
            int length = (int) longlength;
            if (length != longlength) throw new IOException("File size >= 2 GB");

            byte[] data = new byte[length];
            mRandomAccessFile.readFully(data);

            return data;
        }
        finally
        {
            mRandomAccessFile.close();
        }
    }

    private File mFile;
    private RandomAccessFile mRandomAccessFile;

    private class EncodeTask extends AsyncTask<String, Integer, Boolean>
    {
        Context context;
        int mVideoCount;
        String mVideoPath;
        String mVideoName;
        ByteBuffer yuvDataBuffer;
        ByteBuffer audioDataBuffer;
        ArrayList<float[]> mAudios;
        String movieFilePath;
        Handler handler;
        String movieName;
        int audioSize;
        int Apos = 0;

        public EncodeTask(Context context, int mVideoCount,String mVideoPath,String mVideoName,ByteBuffer yuvDataBuffer,ByteBuffer audioDataBuffer,ArrayList<float[]> mAudios,String movieFilePath,Handler handler,String movieName,int audioSize)
        {
            mEncodeState = true;
            this.context = context;
            this.mVideoCount = mVideoCount;
            this.mVideoPath = mVideoPath;
            this.mVideoName = mVideoName;
            this.yuvDataBuffer = yuvDataBuffer;
            this.audioDataBuffer = audioDataBuffer;
            this.mAudios = mAudios;
            this.movieFilePath = movieFilePath;
            this.handler = handler;
            this.movieName = movieName;
            this.audioSize = audioSize;
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            try
            {
                for(int i = 1 ; i <= mVideoCount ; i++)
                {
                    if(mFile!=null) mFile = null;
                    mFile = new File(mVideoPath + mVideoName + i);
                    byte[] tt = readFile(mFile);
                    yuvDataBuffer.rewind();
                    yuvDataBuffer.put(tt, 0, tt.length);
                    Mp4Recorder.instance().encodeVideoData(yuvDataBuffer);
                }

                audioDataBuffer.rewind();
                for(int k = 0 ; k < mAudios.size() ; k++)
                {
                    if(Apos > 650) break;//675

                    for(int i=0;i<audioSize;i++)
                    {
                        audioDataBuffer.putFloat(mAudios.get(k)[i]);

                        if(!audioDataBuffer.hasRemaining())
                        {
                            Mp4Recorder.instance().encodeAudioData(audioDataBuffer);
                            audioDataBuffer.rewind();
                            Apos++;
                        }
                    }
                }
                mAudios.clear();
                Apos=0;
            }
            catch (IOException e)
            {
            }

            Mp4Recorder.instance().finish();

            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    ContentValues values = new ContentValues(2);
//                        values.put(MediaStore.Video.Media.TITLE, "My Movie");
                    values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                    values.put(MediaStore.Video.Media.DATA, movieFilePath);
                    getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);

                    for(int i = 1 ; i <= 450 ; i++)
                    {
                        File mFile = new File(mVideoPath + mVideoName + i);
                        if(mFile.exists()) mFile.delete();
                        else break;
                    }

                    new File(mVideoPath).delete();

                    mEncodeState = false;

                    ArrayList<String> filesToUpload = new ArrayList<String>();
                    filesToUpload.add(movieName);
                    filesToUpload.add(movieName.substring(0,movieName.length()-3) + "jpg");
                    filesToUpload.add("THUMBNAIL_" + movieName.substring(0,movieName.length()-3) + "jpg");
                    setUpLoad(context,filesToUpload);
                }
            });

            return true;
        }
    }

    public void setmPostType(String postType)
    {
        mPostType = postType;
    }

    public String getPostType()
    {
        return mPostType;
    }

    public Boolean getEncodeState()
    {
        return mEncodeState;
    }

    public void setEncodeState(Boolean EncodeState)
    {
        mEncodeState = EncodeState;
    }

    public void setCommentCount(int commentCount)
    {
        mCommentCount = commentCount;
    }

    public int getCommentCount()
    {
        return mCommentCount;
    }

    public void setGripFollow(Boolean gripFollow)
    {
        this.mGripFollow = gripFollow;
    }

    public Boolean getGripFollow()
    {
        return mGripFollow;
    }

    public void setshowToast(Boolean toastState)
    {
        this.mToastState = toastState;
    }

    public Boolean getshowToast()
    {
        return mToastState;
    }

    public void sendLikeCountV2(Context c, String postID, int likeCount, Boolean mGodHand)
    {
        if(!mGodHand)
        {
            if(!getIsSbtools())
            {
                ApiManager.likePostBatchUpdate(c, postID, likeCount, new ApiManager.LikePostBatchUpdateCallback()
                {
                    @Override
                    public void onResult(boolean success, String message) {
                        if (success) {

                        }
                    }
                });
            }
        }
    }

    public void setIsFollow(int isFollow)
    {
        this.mIsFollow = isFollow;
    }

    public int getIsFollow()
    {
        return mIsFollow;
    }

    public void setOpenLive(Boolean open)
    {
        mOpenLive = open;
    }

    public Boolean getOpenLive()
    {
        return mOpenLive;
    }

    public void getRevenueMoney(final Context mCtx, String userID)
    {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        final int max =  (int) (calendar.getTimeInMillis()/1000L);

        ApiManager.getRevenueReport(mCtx, userID, 0, max, new ApiManager.GetRevenueReportCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<RevenueModel> revenueModel)
            {
                if (success && revenueModel != null)
                {
                    if (revenueModel.size() != 0)
                    {
                        setRevenueModels(revenueModel);

                        float total = 0;

                        for (int i = 0; i < revenueModel.size(); i++) {
                            total = total + revenueModel.get(i).getRevenue();
                        }

                        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
                        SharedPreferences.Editor PE = settings.edit();
                        PE.putFloat(Constants.REVENUE_MONEY_TOTAL, total);

                        SimpleDateFormat mDateFormatTo = new SimpleDateFormat("dd/MM/yyyy");
                        String first_day = mDateFormatTo.format(new Date(mRevenueModel.get(0).getTimestamp() * 1000L));
                        String now_day = mDateFormatTo.format(new Date(max * 1000L));
                        if(first_day.compareTo(now_day)==0)
                        {
                            PE.putFloat(Constants.REVENUE_TODAY, mRevenueModel.get(0).getRevenue());
                        }
                        else
                        {
                            PE.putFloat(Constants.REVENUE_TODAY, 0f);
                        }

                        PE.commit();
                    }
                    else
                    {
                        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
                        SharedPreferences.Editor PE = settings.edit();
                        PE.putFloat(Constants.REVENUE_TODAY, 0f);
                        PE.putFloat(Constants.REVENUE_MONEY_TOTAL, 0f);
                        PE.commit();
                    }
                }
            }
        });
    }

    public void GoLogin(Context mCtx, String account ,String password)
    {
        if(account.length()!=0 && password.length()!=0)
        {
            ApiManager.loginAction(mCtx,account, password, new ApiManager.LoginActionCallback()
            {
                @Override
                public void onResult(boolean success, String message, UserModel user)
                {
                    if(success)
                    {
                        if (message.equals("ok"))
                        {
                            setUser(user);
                        }
                    }
                }
            });
        }
    }

    public void getSelfInfo(final Activity mCtx/*, final ImageView badge*/)
    {
        ApiManager.getSelfInfo(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), new ApiManager.GetSelfInfoCallback()
        {
            @Override
            public void onResult(boolean success, String message, UserModel userModel)
            {
                if(success && userModel!=null)
                {
                    if(userModel.getIsFreezed()==1)
                    {
                        File mFileSetting = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/");
                        mFileSetting.mkdir();

                        File mFileUser = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/");
                        mFileUser.mkdir();

                        File mFileFreezed = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/");
                        mFileFreezed.mkdir();

                        File mFileIs = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/is/");
                        mFileIs.mkdir();

                        File mFileLogin = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/is/17.login");
                        mFileLogin.mkdir();

                        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                        builder.setMessage(getString(R.string.your_account_freezed));
                        builder.setTitle(getString(R.string.system_notif));
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                mCtx.finish();
                            }
                        }).setCancelable(false);

                        builder.create().show();

                        return ;
                    }

                    if(userModel.getNotifBadge()!=0 || userModel.getMessageBadge()!=0)
                    {
//                        badge.setVisibility(View.VISIBLE);
                    }

                    if(userModel.getRequestBadge() > 99) mRequestBadge = 99;
                    else mRequestBadge = userModel.getRequestBadge();

                    try
                    {
                        if(userModel.getVerifiedPhoneNumber().length()!=0) Singleton.preferenceEditor.putString(Constants.VERIFIED_PHONE_NUMBER, userModel.getVerifiedPhoneNumber()).commit();
                        else
                        {
                            if(Singleton.preferences.getString(Constants.VERIFIED_PHONE_NUMBER, "").length()!=0)
                            {
                                updateVerifiedPhoneNumber(mCtx, Singleton.preferences.getString(Constants.VERIFIED_PHONE_NUMBER, ""));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }

                    try {
                        Singleton.preferenceEditor.putInt(Constants.GIFT_MODULE_STATE, userModel.getGiftModuleState()).commit();
                        Singleton.preferenceEditor.putInt(Constants.HAS_SHOWN_GIFT_MODULE_TUTORIAL, userModel.getHasShownGiftModuleTutorial()).commit();
                        Singleton.preferenceEditor.putString(Constants.APP_UPDATE_LINK, userModel.getAppUpdateLink()).commit();
                        Singleton.preferenceEditor.putInt(Constants.FORCE_UPDATE_APP, userModel.getForceUpdateApp()).commit();
                        Singleton.preferenceEditor.putString(Constants.LATEST_APP_VERSION, userModel.getLatestAppVersion()).commit();
                    }
                    catch(Exception e){
                    }
                }
            }
        });
    }

    public void setSuggested(ArrayList<SuggestedUsersModel> suggested)
    {
        mSuggested.clear();
        mSuggested.addAll(suggested);
    }

    public ArrayList<SuggestedUsersModel> getSuggested()
    {
        return mSuggested;
    }

    public void updateUserIP(Context mCtx, String ip)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userIP", ip);

            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                @Override
                public void onResult(boolean success, String message) {
                    if (success) {
                    }
                }
            });
        }
        catch (JSONException e)
        {

        }
    }

    public void updateUserBadgeFirst(Context mCtx/*, ImageView badge*/)
    {
        try
        {
//            badge.setVisibility(View.GONE);

            JSONObject params = new JSONObject();
            params.put("notifBadge", 0);
            params.put("messageBadge", 0);

            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                @Override
                public void onResult(boolean success, String message) {
                    if (success) {
                    }
                }
            });
        }
        catch (JSONException e)
        {

        }
    }

    public void updateUserBadge(Context mCtx)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("systemNotifBadge", 0);

            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                @Override
                public void onResult(boolean success, String message) {
                    if (success) {
                    }
                }
            });
        }
        catch (JSONException e)
        {

        }
    }

    public void updateUserRequestBadge(Context mCtx)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("requestBadge", 0);

            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                @Override
                public void onResult(boolean success, String message) {
                    if (success) {
                    }
                }
            });
        }
        catch (JSONException e)
        {

        }
    }

    public void setNowRegion(String NowRegion)
    {
        this.NowRegion = NowRegion;
    }

    public String getNowRegion()
    {
        return NowRegion;
    }

    public void setReLoadRegion(Boolean ReLoadRegion)
    {
        this.ReLoadRegion = ReLoadRegion;
    }

    public Boolean getReLoadRegion()
    {
        return ReLoadRegion;
    }

    public void updateVerifiedPhoneNumber(Context mCtx, String phone)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("verifiedPhoneNumber", phone);

            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                @Override
                public void onResult(boolean success, String message) {
                    if (success) {
                    }
                }
            });
        }
        catch (JSONException e)
        {

        }
    }

    public void setLongitude(Double mLongitude)
    {
        this.mLongitude = mLongitude;
    }

    public Double getLongitude()
    {
        return mLongitude;
    }

    public void setLatitude(Double mLatitude)
    {
        this.mLatitude = mLatitude;
    }

    public Double getLatitude()
    {
        return mLatitude;
    }

    public void setNoShowToast(Boolean mNoShowToast)
    {
        this.mNoShowToast = mNoShowToast;
    }

    public Boolean getNoShowToast()
    {
        return mNoShowToast;
    }

    public void setFromBranch(Boolean fromBranch)
    {
        this.fromBranch = fromBranch;
    }

    public Boolean getFromBranch()
    {
        return fromBranch;
    }

    public void setFromPage(String fromPage)
    {
        this.fromPage = fromPage;
    }

    public String getFromPage()
    {
        return fromPage;
    }

    public void setFromID(String fromID)
    {
        this.fromID = fromID;
    }

    public String getFromID()
    {
        return fromID;
    }

    public void updateGiftAmimation(final Context mCtx)
    {
        try
        {
            ApiManager.getGiftList(mCtx, new ApiManager.GetGiftListCallback()
            {
                @Override
                public void onResult(boolean success, final ArrayList<GiftModel> giftModel)
                {
                    if(success && giftModel!=null)
                    {
                        if(giftModel.size()!=0)
                        {
                            try
                            {
                                new Thread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        for (int i = 0; i < giftModel.size(); i++)
                                        {
                                            try {
                                                File file = new File(Singleton.getExternalMediaFolderPath() + giftModel.get(i).getArchiveFileName().substring(0, giftModel.get(i).getArchiveFileName().length() - 4));

                                                if (!file.exists()) {
                                                    new DownloadFile().execute(Constants.NO_CHT_ALL_URL + giftModel.get(i).getArchiveFileName(), giftModel.get(i).getArchiveFileName());
                                                }

                                                if (giftModel.get(i).getSoundTrack().length() != 0) {
                                                    File sound = new File(Singleton.getExternalMediaFolderPath() + giftModel.get(i).getSoundTrack());

                                                    if (!sound.exists()) {
                                                        new DownloadMp3().execute(Constants.NO_CHT_ALL_URL + giftModel.get(i).getSoundTrack(), giftModel.get(i).getSoundTrack());
                                                    }
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }).start();
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                    else
                    {
                        updateGiftAmimation(mCtx);
                    }
                }
            });
        }
        catch (Exception e)
        {

        }
    }

    private class DownloadFile extends AsyncTask<String, String, String>
    {
        private String zip = "";

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... download_url)
        {
            int count;

            try
            {
                zip = download_url[1];

                URL url = new URL(download_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(),8192);

                OutputStream output = new FileOutputStream(Singleton.getExternalMediaFolderPath() + download_url[1]);
                byte data[] = new byte[1024];
                long total = 0;

                while ((count = input.read(data)) != -1)
                {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();

                File file = new File(Singleton.getExternalMediaFolderPath() + download_url[1]);
                ZipUtils.upZipFile(file, Environment.getExternalStorageDirectory().getAbsolutePath() + "/." + Singleton.applicationContext.getPackageName() + "/");
            }
            catch (Exception e)
            {
            }

            return null;
        }

        protected void onProgressUpdate(String... progress)
        {

        }

        @Override
        protected void onPostExecute(String file_url)
        {
            File file = new File(Singleton.getExternalMediaFolderPath() + zip);
            if(file.exists()) file.delete();
        }
    }

    private class DownloadMp3 extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... download_url)
        {
            int count;

            try
            {
                URL url = new URL(download_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(),8192);

                OutputStream output = new FileOutputStream(Singleton.getExternalMediaFolderPath() + download_url[1]);
                byte data[] = new byte[1024];
                long total = 0;

                while ((count = input.read(data)) != -1)
                {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
            }
            catch (Exception e)
            {
            }

            return null;
        }
    }

    private ArrayList<ProductModel> mProductModels = new ArrayList<ProductModel>();

    public void setProductModels(ArrayList<ProductModel> productModels)
    {
        mProductModels.clear();
        mProductModels.addAll(productModels);
    }

    public ArrayList<ProductModel> getProductModels()
    {
        return mProductModels;
    }

    public void updatePointProduct(final Context mCtx)
    {
        ApiManager.getPointProductList(mCtx, new ApiManager.ProductListCallback() {
            @Override
            public void onResult(boolean success, ArrayList<ProductModel> productModels) {
                if (success && productModels != null) {
                    if (productModels.size() != 0) {
                        mProductModels.clear();
                        mProductModels.addAll(productModels);
                    }
                }
            }
        });
    }

    public void checkIAPreUpload(Context mCtx)
    {
        if(Singleton.preferences.getString(Constants.IAP_USER, "").length()!=0)
        {
            if(Singleton.preferences.getString(Constants.IAP_USER, "").compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                String Sku = Singleton.preferences.getString(Constants.IAP_SKU, "");
                String OrderId = Singleton.preferences.getString(Constants.IAP_ORDERID, "");
                String Token = Singleton.preferences.getString(Constants.IAP_TOKEN, "");
                String Payload = Singleton.preferences.getString(Constants.IAP_PAYLOAD, "");

                ApiManager.purchaseProduct(mCtx, Sku, OrderId, Token, Payload, new ApiManager.PurchaseProductCallback()
                {
                    @Override
                    public void onResult(boolean success, int point)
                    {
                        if(success)
                        {
                            int now = Singleton.preferences.getInt(Constants.GIFT_POINT, 0) + point;
                            Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, now).commit();

                            Singleton.preferenceEditor.putString(Constants.IAP_USER, "").commit();
                            Singleton.preferenceEditor.putString(Constants.IAP_SKU, "").commit();
                            Singleton.preferenceEditor.putString(Constants.IAP_ORDERID, "").commit();
                            Singleton.preferenceEditor.putString(Constants.IAP_TOKEN, "").commit();
                            Singleton.preferenceEditor.putString(Constants.IAP_PAYLOAD, "").commit();
                        }
                    }
                });
            }
        }
    }

    public void setGuestFeed(ArrayList<FeedModel> feed)
    {
        mFeedModel.clear();
        mFeedModel.addAll(feed);
    }

    public ArrayList<FeedModel> getGuestFeed()
    {
        return mFeedModel;
    }

    public HashMap mHashMap = new HashMap<String,String>();

    public JSONArray mEventArray = new JSONArray();

    private Boolean mGuestModeState = false;
    public void setGuestModeState(Boolean state)
    {
        this.mGuestModeState = state;
    }

    public Boolean getGuestModeState()
    {
        return mGuestModeState;
    }

    private Boolean mSignOutState = false;
    public void setSignOutState(Boolean state)
    {
        this.mSignOutState = state;
    }

    public Boolean getSignOutState()
    {
        return mSignOutState;
    }

    private Boolean mGuestFinish = false;
    public void setGuestFinish(Boolean state)
    {
        this.mGuestFinish = state;
    }

    public Boolean getGuestFinish()
    {
        return mGuestFinish;
    }

    private Boolean mMenuFinish = false;
    public void setMenuFinish(Boolean state)
    {
        this.mMenuFinish = state;
    }

    public Boolean getMenuFinish()
    {
        return mMenuFinish;
    }
}
