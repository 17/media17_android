package com.machipopo.media17;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;

public class CropImageView extends ImageView implements android.view.GestureDetector.OnGestureListener {

	private boolean isZooming;
	
    private GestureDetectorCompat panGestureDetector; 
    private ScaleGestureDetector scaleGestureDetector; 

    Paint borderPaint = new Paint();
    Paint maskPaint = new Paint();

	private int viewWidth;
	private int viewHeight;

	private int imageWidth;
	private int imageHeight;
	private int leftBoundary;
	private int topBoundary;
	private int rightBoundary;
	private int bottomBoundary;
	
	private int x1;
	private int y1;
	private int x2;
	private int y2;

	Bitmap bitmap;

	public CropImageView(Context context) {
		super(context);
		initialize();
	}

	public CropImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	public CropImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}
	
	private int zoomCenterX;
	private int zoomCenterY;
	private int zoomBeginHalfSideLength;

	public void initialize() {
		panGestureDetector = new GestureDetectorCompat(getContext(), this);
		
		scaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleGestureDetector.OnScaleGestureListener() {
			public void onScaleEnd(ScaleGestureDetector detector) {
				isZooming = false;
				
				invalidate();
			}
			
			public boolean onScaleBegin(ScaleGestureDetector detector) {
				isZooming = true;
				
				zoomCenterX = (x1 + x2) / 2;
				zoomCenterY = (y1 + y2) / 2;
				zoomBeginHalfSideLength = (x2 - x1) / 2;
				
				return true;
			}
			
			public boolean onScale(ScaleGestureDetector detector) {
				float zoomScale = detector.getScaleFactor();
				
				int newHalfSideLength = (int) ((float) zoomBeginHalfSideLength * zoomScale);
				newHalfSideLength = Math.min(newHalfSideLength, Math.min(imageWidth/2, imageHeight/2));
				
//				Singleton.log("zoomScale: "+zoomScale);
				
				int newX1 = zoomCenterX - newHalfSideLength;
				int newX2 = zoomCenterX + newHalfSideLength;
				int newY1 = zoomCenterY - newHalfSideLength;
				int newY2 = zoomCenterY + newHalfSideLength;
				int newCenterX = zoomCenterX;
				int newCenterY = zoomCenterY;
				
				if(newX1<leftBoundary) {
					newX1 = leftBoundary;
					newCenterX = leftBoundary + newHalfSideLength;
					newX2 = newCenterX + newHalfSideLength;
				}
				
				if(newX2>rightBoundary) {
					newX2 = rightBoundary;
					newCenterX = rightBoundary - newHalfSideLength;
					newX1 = newCenterX - newHalfSideLength;
				}
				
				if(newY1<topBoundary) {
					newY1 = topBoundary;
					newCenterY = topBoundary + newHalfSideLength;
					newY2 = newCenterY + newHalfSideLength;
				}
				
				if(newY2>bottomBoundary) {
					newY2= bottomBoundary;
					newCenterY = bottomBoundary - newHalfSideLength;
					newY1 = newCenterY - newHalfSideLength;
				}
				
				if(newX1>=leftBoundary&&newX2<=rightBoundary&&newY1>=topBoundary&&newY2<=bottomBoundary) {
					x1 = newX1;
					x2 = newX2;
					y1 = newY1;
					y2 = newY2;
					
					zoomCenterX = newCenterX;
					zoomCenterY = newCenterY;
				}

				invalidate();
				
				return false;
			}
		});
	}

	public void loadImage(Bitmap b) {
        borderPaint.setColor(Color.rgb(255, 255, 255));
        borderPaint.setAntiAlias(true);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(Singleton.dpToPixel(2));

        maskPaint.setColor(Color.argb(70, 0, 0, 0));
        maskPaint.setAntiAlias(true);
        maskPaint.setStyle(Paint.Style.FILL);
        maskPaint.setStrokeWidth(Singleton.dpToPixel(0));
		
		bitmap = b;
		setImageBitmap(bitmap);

		// check for aspect ratio
		calculateParameters();
	}
	
	private void calculateParameters() {
		float imageAspectRatio = (float) bitmap.getWidth() / (float) bitmap.getHeight();
		float viewAspectRatio = (float) viewWidth / (float) viewHeight;

		if(imageAspectRatio>=viewAspectRatio) {
			imageWidth = viewWidth;
			imageHeight = (int) ((float) imageWidth / imageAspectRatio);
		} else {
			imageHeight = viewHeight;
			imageWidth = (int) ((float) imageHeight * imageAspectRatio);
		}
		
		topBoundary = (viewHeight-imageHeight) / 2;
		leftBoundary = (viewWidth-imageWidth) / 2;
		rightBoundary = viewWidth - leftBoundary;
		bottomBoundary = viewHeight - topBoundary;
		
//		Singleton.log("viewWidth: "+viewWidth);
//		Singleton.log("viewHeight: "+viewHeight);
//		Singleton.log("imageWidth: "+imageWidth);
//		Singleton.log("imageHeight: "+imageHeight);
//		Singleton.log("topBoundary: "+topBoundary);
//		Singleton.log("leftBoundary: "+leftBoundary);
		
		// choose 80% width as initial crop region
		int initialCropSideLength = Math.min(imageWidth, imageHeight) * 8 / 10;
		
		x1 = (viewWidth-initialCropSideLength)/2;
		y1 = (viewHeight-initialCropSideLength)/2;
		x2 = viewWidth-x1;
		y2 = viewHeight-y1;
		
//		Singleton.log("x1: "+x1);
//		Singleton.log("y1: "+y1);
//		Singleton.log("x2: "+x2);
//		Singleton.log("y2: "+y2);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		viewWidth = w;
		viewHeight = h;
		
		calculateParameters();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		panGestureDetector.onTouchEvent(event);
		scaleGestureDetector.onTouchEvent(event);

//		if (event.getAction() == MotionEvent.ACTION_DOWN) {
//			Singleton.log("DOWN");
//		} else if (event.getAction() == MotionEvent.ACTION_UP) {
//			Singleton.log("UP");
//		} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
//			Singleton.log("MOVE");
//		}

		float x = event.getX();
		float y = event.getY();

//		Singleton.log("X: "+x);
//		Singleton.log("Y: "+y);
		super.onTouchEvent(event);
		return true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		try {
            /* Draw Mask */
            Path maskPath = new Path();

            maskPath.moveTo(0, 0);
            maskPath.lineTo(viewWidth, 0);
            maskPath.lineTo(viewWidth, y1);
            maskPath.lineTo(0, y1);
            maskPath.close();

            canvas.drawPath(maskPath, maskPaint);

            maskPath = new Path();

            maskPath.moveTo(0, y1);
            maskPath.lineTo(x1, y1);
            maskPath.lineTo(x1, y2);
            maskPath.lineTo(0, y2);
            maskPath.close();

            canvas.drawPath(maskPath, maskPaint);

            maskPath = new Path();

            maskPath.moveTo(x2, y1);
            maskPath.lineTo(viewWidth, y1);
            maskPath.lineTo(viewWidth, y2);
            maskPath.lineTo(x2, y2);
            maskPath.close();

            canvas.drawPath(maskPath, maskPaint);

            maskPath = new Path();

            maskPath.moveTo(0, y2);
            maskPath.lineTo(viewWidth, y2);
            maskPath.lineTo(viewWidth, viewHeight);
            maskPath.lineTo(0, viewHeight);
            maskPath.close();

            canvas.drawPath(maskPath, maskPaint);

            /* Draw Border */
			Path borderPath = new Path();
			
			int offset = Singleton.dpToPixel(1);

            borderPath.moveTo(x1 + offset, y1 + offset);
            borderPath.lineTo(x2 - offset, y1 + offset);
            borderPath.lineTo(x2 - offset, y2 - offset);
            borderPath.lineTo(x1 + offset, y2 - offset);
            borderPath.close();
			
			canvas.drawPath(borderPath, borderPaint);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public Bitmap getCroppedImage() {
		float scaleFactor = (float)bitmap.getWidth() / (float)imageWidth;

        int x = (int) ((float) (x1-leftBoundary) * scaleFactor);
		int y = (int) ((float) (y1-topBoundary) * scaleFactor);
		int w = (int) ((float) (x2-x1) * scaleFactor);
		int h = (int) ((float) (y2-y1) * scaleFactor);

//        Singleton.log("x: "+x);
//        Singleton.log("y: "+x);
//        Singleton.log("w: "+x);
//        Singleton.log("h: "+x);

        w = Math.min(bitmap.getWidth()-x, w);
        h = Math.min(bitmap.getHeight()-y, h);

        Bitmap b = Bitmap.createBitmap(bitmap, x, y, w, h);

        return b;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		if(isZooming==true) {
			return true;
		}
		
		int newX1 = (int) ((float) x1 - distanceX);
		int newX2 = (int) ((float) x2 - distanceX);
		
		if(newX1>=leftBoundary&&newX2<=rightBoundary) {
			x1 = newX1;
			x2 = newX2;
		}
		
		int newY1 = (int) ((float) y1 - distanceY);
		int newY2 = (int) ((float) y2 - distanceY);
		
		if(newY1>=topBoundary&&newY2<=bottomBoundary) {
			y1 = newY1;
			y2 = newY2;
		}
		
		invalidate();

//		Singleton.log("distanceX: "+distanceX);
//		Singleton.log("distanceY: "+distanceY);
		
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return true;
	}
}
