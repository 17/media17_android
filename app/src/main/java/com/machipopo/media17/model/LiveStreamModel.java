package com.machipopo.media17.model;

/**
 * Created by POPO on 5/15/15.
 */
public class LiveStreamModel
{
    public int liveStreamID;
    public String userID;
    public String coverPhoto;
    public UserModel user;
    public String caption;
    public String locationName;
    public float latitude;
    public float longitude;
    public int shareLocation;
    public int followerOnlyChat;
    public int chatAvailable;
    public int replayAvailable;
    public int beginTime;
    public int endTime;
    public int duration;
    public int viewerCount;
    public int liveViewerCount;
    public int receivedLikeCount;
    public int replayCount;
    public int totalViewTime;
    public float revenue;
    public int audioOnly;
    public int numberOfChunks;
    public int canSendGift;
    public float giftRevenue;
    public String videoCodec;
    public int canSendGiftWithTextAndVoice;
}
