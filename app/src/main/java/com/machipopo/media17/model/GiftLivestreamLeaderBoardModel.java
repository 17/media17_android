package com.machipopo.media17.model;

import java.util.ArrayList;

/**
 * Created by POPO_INNOVATION_IMAC on 15/12/21.
 */
public class GiftLivestreamLeaderBoardModel {

//        private GiftLeaderboardModel giftLeaderboardInfo;
        private ArrayList<GiftLeaderboardModel> mGiftLeaderboardInfo = new ArrayList<GiftLeaderboardModel>();
        private int myPoint;
        private int totalPoint;


        public void setGiftLeaderboardInfo(ArrayList<GiftLeaderboardModel> mGiftLeaderboardInfo)
        {
            this.mGiftLeaderboardInfo.addAll(mGiftLeaderboardInfo);
        }

        public ArrayList<GiftLeaderboardModel> getGiftLeaderboardInfo()
        {
            return mGiftLeaderboardInfo;
        }

        public void setMyPoint(int myPoint)
        {
            this.myPoint = myPoint;
        }

        public int getMyPoint()
        {
            return myPoint;
        }

        public void setTotalPoint(int totalPoint)
        {
            this.totalPoint = totalPoint;
        }

        public int getTotalPoint()
        {
            return totalPoint;
        }

};
