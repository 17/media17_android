package com.machipopo.media17.model;

/**
 * Created by POPO on 7/22/15.
 */
public class LiveComment
{
    private UserModel userInfo;
    private int timestamp;
    private int absTimestamp;
    private String message;
    private String colorCode;
    private String liveStreamChatID;
    private int isSystem;

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setAbsTimestamp(int absTimestamp)
    {
        this.absTimestamp = absTimestamp;
    }

    public int getAbsTimestamp()
    {
        return absTimestamp;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setColorCode(String colorCode)
    {
        this.colorCode = colorCode;
    }

    public String getColorCode()
    {
        return colorCode;
    }

    public void setLiveStreamChatID(String liveStreamChatID)
    {
        this.liveStreamChatID = liveStreamChatID;
    }

    public String getLiveStreamChatID()
    {
        return liveStreamChatID;
    }

    public void setIsSystem(int isSystem)
    {
        this.isSystem = isSystem;
    }

    public int getIsSystem()
    {
        return isSystem;
    }
}
