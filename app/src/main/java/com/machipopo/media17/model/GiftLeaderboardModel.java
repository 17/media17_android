package com.machipopo.media17.model;

/**
 * Created by POPO on 2015/11/24.
 */
public class GiftLeaderboardModel
{
    private UserModel userInfo;
    private int point;

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public int getPoint()
    {
        return point;
    }
}
