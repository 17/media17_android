package com.machipopo.media17.model;

/**
 * Created by POPO on 2015/12/10.
 */
public class ReadGiftModel
{
    private GiftModel giftInfo;
    private UserModel userInfo;
    private int timestamp;
    private int giftToken;

    public void setGiftInfo(GiftModel giftInfo)
    {
        this.giftInfo = giftInfo;
    }

    public GiftModel getGiftInfo()
    {
        return giftInfo;
    }

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setGiftToken(int giftToken)
    {
        this.giftToken = giftToken;
    }

    public int getGiftToken()
    {
        return giftToken;
    }
}
