package com.machipopo.media17.model;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

/**
 * Created by POPO on 15/8/19.
 */
public class MyEditText extends EditText
{
    public EditKeyBackListener mEditKeyBackListener;

    public MyEditText(Context context) {
        super(context);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            mEditKeyBackListener.OnEditKeyBack();
        }
        return false;
    }

    public interface EditKeyBackListener
    {
        void OnEditKeyBack();
    }
}
