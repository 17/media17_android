package com.machipopo.media17.model;

/**
 * Created by POPO on 7/20/15.
 */
public class LiveModel
{
    private String userID;
    private UserModel userInfo;
    private String coverPhoto;
    private int liveStreamID;
    private String caption;
    private String locationName;
    private double latitude;
    private double longitude;
    private double shareLocation;
    private int followerOnlyChat;
    private int chatAvailable;
    private int beginTime;
    private int endTime;
    private int duration;
    private int viewerCount;
    private int liveViewerCount;
    private int receivedLikeCount;
    private int replayCount;
    private int replayAvailable;
    private int totalViewTime;
    private float revenue;
    private int audioOnly;
    private int numberOfChunks;
    private String restreamerOpenID;
    private int canSendGift;
    private float giftRevenue;
    private String videoCodec;
    private int canSendGiftWithTextAndVoice;

    public void setCanSendGift(int canSendGift)
    {
        this.canSendGift = canSendGift;
    }

    public int getCanSendGift()
    {
        return canSendGift;
    }

    public void setUserID(String userID)
    {
        this.userID = userID;
    }

    public String getUserID()
    {
        return userID;
    }

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setCoverPhoto(String coverPhoto)
    {
        this.coverPhoto = coverPhoto;
    }

    public String getCoverPhoto()
    {
        return coverPhoto;
    }

    public void setLiveStreamID(int liveStreamID)
    {
        this.liveStreamID = liveStreamID;
    }

    public int getLiveStreamID()
    {
        return liveStreamID;
    }

    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    public String getCaption()
    {
        return caption;
    }

    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    public String getLocationName()
    {
        return locationName;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setShareLocation(double shareLocation)
    {
        this.shareLocation = shareLocation;
    }

    public double getShareLocation()
    {
        return shareLocation;
    }

    public void setFollowerOnlyChat(int followerOnlyChat)
    {
        this.followerOnlyChat = followerOnlyChat;
    }

    public int getFollowerOnlyChat()
    {
        return followerOnlyChat;
    }

    public void setChatAvailable(int chatAvailable)
    {
        this.chatAvailable = chatAvailable;
    }

    public int getChatAvailable()
    {
        return chatAvailable;
    }

    public void setBeginTime(int beginTime)
    {
        this.beginTime = beginTime;
    }

    public int getBeginTime()
    {
        return beginTime;
    }

    public void setEndTime(int endTime)
    {
        this.endTime = endTime;
    }

    public int getEndTime()
    {
        return endTime;
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    public int getDuration()
    {
        return duration;
    }

    public void setViewerCount(int viewerCount)
    {
        this.viewerCount = viewerCount;
    }

    public int getViewerCount()
    {
        return viewerCount;
    }

    public void setLiveViewerCount(int liveViewerCount)
    {
        this.liveViewerCount = liveViewerCount;
    }

    public int getLiveViewerCount()
    {
        return liveViewerCount;
    }

    public void setReceivedLikeCount(int receivedLikeCount)
    {
        this.receivedLikeCount = receivedLikeCount;
    }

    public int getReceivedLikeCount()
    {
        return receivedLikeCount;
    }

    public void setReplayCount(int replayCount)
    {
        this.replayCount = replayCount;
    }

    public int getReplayCount()
    {
        return replayCount;
    }

    public void setReplayAvailable(int replayAvailable)
    {
        this.replayAvailable = replayAvailable;
    }

    public int getReplayAvailable()
    {
        return replayAvailable;
    }

    public void setTotalViewTime(int totalViewTime)
    {
        this.totalViewTime = totalViewTime;
    }

    public int getTotalViewTime()
    {
        return totalViewTime;
    }

    public void setRevenue(float revenue)
    {
        this.revenue = revenue;
    }

    public float getRevenue()
    {
        return revenue;
    }

    public void setAudioOnly(int audioOnly)
    {
        this.audioOnly = audioOnly;
    }

    public int getAudioOnly()
    {
        return audioOnly;
    }

    public void setNumberOfChunks(int numberOfChunks)
    {
        this.numberOfChunks = numberOfChunks;
    }

    public int getNumberOfChunks()
    {
        return numberOfChunks;
    }

    public void setRestreamerOpenID(String restreamerOpenID)
    {
        this.restreamerOpenID = restreamerOpenID;
    }

    public String getRestreamerOpenID()
    {
        return restreamerOpenID;
    }

    public void setGiftRevenue(float giftRevenue)
    {
        this.giftRevenue = giftRevenue;
    }

    public float getGiftRevenue()
    {
        return giftRevenue;
    }

    public void setVideoCodec(String videoCodec)
    {
        this.videoCodec = videoCodec;
    }

    public String getVideoCodec()
    {
        return videoCodec;
    }

    public void setCanSendGiftWithTextAndVoice(int canSendGiftWithTextAndVoice)
    {
        this.canSendGiftWithTextAndVoice = canSendGiftWithTextAndVoice;
    }

    public int getCanSendGiftWithTextAndVoice()
    {
        return canSendGiftWithTextAndVoice;
    }
}
