package com.machipopo.media17.model;

/**
 * Created by POPO on 5/27/15.
 */
public class FeedModel
{
    private UserModel userInfo;
    private String postID;
    private String userID;
    private String caption;
    private int timestamp;
    private String picture;
    private int[] taggedUsers;
    private String type;
    private int canComment;
    private String video;
    private String reachability;
    private float totalRevenue;
    private String locationName;
    private String locationID;
    private float latitude;
    private float longitude;
    private int likeCount;
    private int commentCount;
    private int viewCount;
    private int liked;

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setPostID(String postID)
    {
        this.postID = postID;
    }

    public String getPostID()
    {
        return postID;
    }

    public void setUserID(String userID)
    {
        this.userID = userID;
    }

    public String getUserID()
    {
        return userID;
    }

    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    public String getCaption()
    {
        return caption;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        return picture;
    }

    public void setTaggedUsers(int[] taggedUsers)
    {
        this.taggedUsers = taggedUsers;
    }

    public int[] getTaggedUsers()
    {
        return taggedUsers;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setCanComment(int canComment)
    {
        this.canComment = canComment;
    }

    public int getCanComment()
    {
        return canComment;
    }

    public void setVideo(String video)
    {
        this.video = video;
    }

    public String getVideo()
    {
        return video;
    }

    public void setReachability(String reachability)
    {
        this.reachability = reachability;
    }

    public String getReachability()
    {
        return reachability;
    }

    public void setTotalRevenue(float totalRevenue)
    {
        this.totalRevenue = totalRevenue;
    }

    public float getTotalRevenue()
    {
        return totalRevenue;
    }

    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    public String getLocationName()
    {
        return locationName;
    }

    public void setLocationID(String locationID)
    {
        this.locationID = locationID;
    }

    public String getLocationID()
    {
        return locationID;
    }

    public void setLatitude(float latitude)
    {
        this.latitude = latitude;
    }

    public float getLatitude()
    {
        return latitude;
    }

    public void setLongitude(float longitude)
    {
        this.longitude = longitude;
    }

    public void setLikeCount(int likeCount)
    {
        this.likeCount = likeCount;
    }

    public int getLikeCount()
    {
        return likeCount;
    }

    public void setCommentCount(int commentCount)
    {
        this.commentCount = commentCount;
    }

    public int getCommentCount()
    {
        return commentCount;
    }

    public void setViewCount(int viewCount)
    {
        this.viewCount = viewCount;
    }

    public int getViewCount()
    {
        return viewCount;
    }

    public void setLiked(int liked)
    {
        this.liked = liked;
    }

    public int getLiked()
    {
        return liked;
    }

    public String toString()
    {
        return "userInfo : " + userInfo.toString() + "\n" +
                "postID : " + postID + "\n" +
                "caption : " + caption + "\n" +
                "timestamp : " + timestamp + "\n" +
                "picture : " + picture + "\n" +
                "taggedUsers : " + taggedUsers + "\n" +
                "type : " + type + "\n" +
                "canComment : " + canComment + "\n" +
                "video : " + video + "\n" +
                "reachability : " + reachability + "\n" +
                "totalRevenue : " + totalRevenue + "\n" +
                "locationName : " + locationName + "\n" +
                "locationID : " + locationID + "\n" +
                "latitude : " + latitude + "\n" +
                "longitude : " + longitude + "\n" +
                "likeCount : " + likeCount + "\n" +
                "commentCount : " + commentCount + "\n" +
                "viewCount : " + viewCount + "\n" +
                "liked : " + liked ;
    }
}
