package com.machipopo.media17.model;

/**
 * Created by POPO on 5/13/15.
 */
public class PaymentModel
{
    private int year;
    private int month;
    private float amount;
    private String status;
    private int paymentTime;
    private String bankName;
    private String bankAddress;
    private String accountNumber;
    private String beneficiaryName;
    private String swiftCode;
    private String bankCountry;
    private String idCardNumber;
    private String address;
    private String bankCode;
    private String bankBranchCode;

    public void setYear(int year)
    {
        this.year = year;
    }

    public int getYear()
    {
        return year;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }

    public int getMonth()
    {
        return month;
    }

    public void setAmount(float amount)
    {
        this.amount = amount;
    }

    public float getAmount()
    {
        return amount;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    public void setPaymentTime(int paymentTime)
    {
        this.paymentTime = paymentTime;
    }

    public int getPaymentTime()
    {
        return paymentTime;
    }

    public void setBankName(String bankName)
    {
        this.bankName = bankName;
    }

    public String getBankName()
    {
        return bankName;
    }

    public void setBankAddress(String bankAddress)
    {
        this.bankAddress = bankAddress;
    }

    public String getBankAddress()
    {
        return bankAddress;
    }

    public void setAccountNumber(String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setBeneficiaryName(String beneficiaryName)
    {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryName()
    {
        return beneficiaryName;
    }

    public void setSwiftCode(String swiftCode)
    {
        this.swiftCode = swiftCode;
    }

    public String getSwiftCode()
    {
        return swiftCode;
    }

    public void setBankCountry(String bankCountry)
    {
        this.bankCountry = bankCountry;
    }

    public String getBankCountry()
    {
        return bankCountry;
    }

    public void setIdCardNumber(String idCardNumber)
    {
        this.idCardNumber = idCardNumber;
    }

    public String getIdCardNumber()
    {
        return idCardNumber;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }

    public void setBankCode(String bankCode)
    {
        this.bankCode = bankCode;
    }

    public String getBankCode()
    {
        return bankCode;
    }

    public void setBankBranchCode(String bankBranchCode)
    {
        this.bankBranchCode = bankBranchCode;
    }

    public String getBankBranchCode()
    {
        return bankBranchCode;
    }

    public String toString()
    {
        return "year : " + year + "\n" +
               "month : " + month + "\n" +
               "amount : " + amount + "\n" +
               "status : " + status + "\n" +
               "paymentTime : " + paymentTime + "\n" +
               "bankName : " + bankName + "\n" +
               "bankAddress : " + bankAddress + "\n" +
               "accountNumber : " + accountNumber + "\n" +
               "beneficiaryName : " + beneficiaryName + "\n" +
               "swiftCode : " + swiftCode + "\n" +
               "bankCountry : " + bankCountry + "\n" +
               "idCardNumber : " + idCardNumber + "\n" +
               "address : " + address + "\n" +
               "bankCode : " + bankCode + "\n" +
               "bankBranchCode : " + bankBranchCode ;
    }
}
