package com.machipopo.media17.model;

/**
 * Created by POPO_INNOVATION_IMAC on 15/12/29.
 */
public class SubscribeOptionModel {

    private String subscriptionProductID;
    private String name;
    private String icon;
    private int point;
    private int discountPoint;
    private float discountPercentage;

    public void setSubscriptionProductID(String subscriptionProductID)
    {
        this.subscriptionProductID = subscriptionProductID;
    }

    public String getSubscriptionProductID()
    {
        return subscriptionProductID;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public int getPoint()
    {
        return point;
    }

    public void setDiscountPoint(int discountPoint)
    {
        this.discountPoint = discountPoint;
    }

    public int getDiscountPoint()
    {
        return discountPoint;
    }

    public void setDiscountPercentage(float discountPercentage)
    {
        this.discountPercentage = discountPercentage;
    }

    public float getDiscountPercentage()
    {
        return discountPercentage;
    }

}
