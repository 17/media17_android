package com.machipopo.media17.model;

/**
 * Created by POPO on 5/15/15.
 */
public class CommentModel
{
    private String userID;
    private String commentID;
    private String comment;
    private int timestamp;
    private UserModel userInfo;

    public void setUserID(String userID)
    {
        this.userID = userID;
    }

    public String getUserID()
    {
        return userID;
    }

    public void setCommentID(String commentID)
    {
        this.commentID = commentID;
    }

    public String getCommentID()
    {
        return commentID;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public String toString()
    {
        return "userID : " + userID + "\n" +
               "commentID : " + commentID + "\n" +
               "comment : " + comment + "\n" +
               "timestamp : " + timestamp + "\n" +
               "userInfo : " + userInfo.toString() ;
    }
}
