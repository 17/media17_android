package com.machipopo.media17.model;

/**
 * Created by POPO on 5/14/15.
 */
public class PostModel
{
    private String postID;
    private String caption;
    private String picture;
    private int likeCount;
    private int viewCount;
    private int liked;
    private int timestamp;
    private int[] taggedUsers;
    private String locationID;
    private String locationName;
    private float latitude;
    private float longitude;
    private String type;
    private int commentCount;
    private int canComment;
    private String reachability;
    private float totalRevenue;

    private String userid;
    private String video;

    private UserModel user;
//    private ArrayList videoSegments;
//    private int repostCount;
//    private String channelID;

    public void setPostID(String postID)
    {
        this.postID = postID;
    }

    public String getPostID()
    {
        return postID;
    }

    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    public String getCaption()
    {
        return caption;
    }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        return picture;
    }

    public void setLikeCount(int likeCount)
    {
        this.likeCount = likeCount;
    }

    public int getLikeCount()
    {
        return likeCount;
    }

    public void setViewCount(int viewCount)
    {
        this.viewCount = viewCount;
    }

    public int getViewCount()
    {
        return viewCount;
    }

    public void setIsLiked(int liked)
    {
        this.liked = liked;
    }

    public int getIsLiked()
    {
        return liked;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setUser(UserModel user)
    {
        this.user = user;
    }

    public UserModel getUser()
    {
        return user;
    }

//    public void setVideoSegments(ArrayList videoSegments)
//    {
//        this.videoSegments = videoSegments;
//    }
//
//    public ArrayList getVideoSegments()
//    {
//        return videoSegments;
//    }

    public void setTaggedUsers(int[] taggedUsers)
    {
        this.taggedUsers = taggedUsers;
    }

    public int[] getTaggedUsers()
    {
        return taggedUsers;
    }

    public void setLocationID(String locationID)
    {
        this.locationID = locationID;
    }

    public String getLocationID()
    {
        return locationID;
    }

    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    public String getLocationName()
    {
        return locationName;
    }

    public void setLatitude(float latitude)
    {
        this.latitude = latitude;
    }

    public float getLatitude()
    {
        return latitude;
    }

    public void setLongitude(float longitude)
    {
        this.longitude = longitude;
    }

    public float getLongitude()
    {
        return longitude;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setCommentCount(int commentCount)
    {
        this.commentCount = commentCount;
    }

    public int getCommentCount()
    {
        return commentCount;
    }

//    public void setRepostCount(int repostCount)
//    {
//        this.repostCount = repostCount;
//    }
//
//    public int getRepostCount()
//    {
//        return repostCount;
//    }
//
//    public void setChannelID(String channelID)
//    {
//        this.channelID = channelID;
//    }
//
//    public String getChannelID()
//    {
//        return channelID;
//    }

    public void setCanComment(int canComment)
    {
        this.canComment = canComment;
    }

    public int getCanComment()
    {
        return canComment;
    }

    public void setReachability(String reachability)
    {
        this.reachability = reachability;
    }

    public String getReachability()
    {
        return reachability;
    }

    public void setTotalRevenue(float totalRevenue)
    {
        this.totalRevenue = totalRevenue;
    }

    public float getTotalRevenue()
    {
        return totalRevenue;
    }

    public void setUserid(String userid)
    {
        this.userid = userid;
    }

    public String getUserid()
    {
        return userid;
    }

    public void setVideo(String video)
    {
        this.video = video;
    }

    public String getVideo()
    {
        return video;
    }

    public String toString()
    {
        return "postID : " + postID + "\n" +
                "caption : " + caption + "\n" +
                "picture : " + picture + "\n" +
                "likeCount : " + likeCount + "\n" +
                "viewCount : " + viewCount + "\n" +
//                "isLiked : " + isLiked + "\n" +
                "timestamp : " + timestamp + "\n" +
//                "user : " + user.toString() + "\n" +
//                "videoSegments : " + videoSegments + "\n" +
                "taggedUsers : " + taggedUsers + "\n" +
                "locationID : " + locationID + "\n" +
                "locationName : " + locationName + "\n" +
                "latitude : " + latitude + "\n" +
                "longitude : " + longitude + "\n" +
                "type : " + type + "\n" +
                "commentCount : " + commentCount + "\n" +
//                "repostCount : " + repostCount + "\n" +
//                "channelID : " + channelID + "\n" +
                "canComment : " + canComment + "\n" +
                "reachability : " + reachability + "\n" +
                "userid : " + userid + "\n" +
                "video : " + video + "\n" +
                "totalRevenue : " + totalRevenue ;
    }
}
