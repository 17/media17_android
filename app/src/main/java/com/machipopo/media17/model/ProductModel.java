package com.machipopo.media17.model;

/**
 * Created by POPO on 2015/11/23.
 */
public class ProductModel
{
    private String productID;
    private int price;
    private int point;
    private int bonusAmount;
    private int bonusPercentage;
    private String name;
    private String google_price = "";
    private String google_point = "";

    public void setProductID(String productID)
    {
        this.productID = productID;
    }

    public String getProductID()
    {
        return productID;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public int getPoint()
    {
        return point;
    }

    public void setBonusAmount(int bonusAmount)
    {
        this.bonusAmount = bonusAmount;
    }

    public int getBonusAmount()
    {
        return bonusAmount;
    }

    public void setBonusPercentage(int bonusPercentage)
    {
        this.bonusPercentage = bonusPercentage;
    }

    public int getBonusPercentage()
    {
        return bonusPercentage;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setGoogle_price(String google_price)
    {
        this.google_price = google_price;
    }

    public String getGoogle_price()
    {
        return google_price;
    }

    public void setGoogle_point(String google_point)
    {
        this.google_point = google_point;
    }

    public String getGoogle_point()
    {
        return google_point;
    }
}