package com.machipopo.media17.model;

/**
 * Created by POPO on 2015/11/27.
 */
public class LiveGiftsModel
{
    private GiftModel giftInfo;
    private UserModel userInfo;
    private int timestamp;

    public void setGiftInfo(GiftModel giftInfo)
    {
        this.giftInfo = giftInfo;
    }

    public GiftModel getGiftInfo()
    {
        return giftInfo;
    }

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }
}
