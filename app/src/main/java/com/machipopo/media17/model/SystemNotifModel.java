package com.machipopo.media17.model;

/**
 * Created by POPO on 5/15/15.
 */
public class SystemNotifModel
{
    private String message;
    private int timestamp;
    private int isRead;

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setIsRead(int isRead)
    {
        this.isRead = isRead;
    }

    public int getIsRead()
    {
        return isRead;
    }

    public String toString()
    {
        return "message : " + message + "\n" +
               "timestamp : " + timestamp + "\n" +
               "isRead : " + isRead ;
    }
}
