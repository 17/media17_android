package com.machipopo.media17.model;

/**
 * Created by POPO_INNOVATION_IMAC on 15/12/30.
 */
public class SubscriptionPurchaseModel {

    private UserModel userInfo;
    private int subscriptionTime;
    private String subscriptionProductID;
    private int point;

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setSubscriptionTime(int subscriptionTime)
    {
        this.subscriptionTime = subscriptionTime;
    }

    public int getSubscriptionTime()
    {
        return subscriptionTime;
    }

    public void setSubscriptionProductID(String subscriptionProductID)
    {
        this.subscriptionProductID = subscriptionProductID;
    }

    public String getSubscriptionProductID()
    {
        return subscriptionProductID;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public int getPoint()
    {
        return point;
    }


}
