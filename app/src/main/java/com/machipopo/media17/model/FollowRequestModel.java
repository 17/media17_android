package com.machipopo.media17.model;

/**
 * Created by POPO on 15/8/14.
 */
public class FollowRequestModel
{
    public UserModel user;
    public int timestamp;

    public void setUser(UserModel user)
    {
        this.user = user;
    }

    public UserModel getUser()
    {
        return user;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }
}
