package com.machipopo.media17.model;

/**
 * Created by POPO on 5/15/15.
 */
public class TagModel
{
    private String hashTag;
    private int postCount;

    public void setHashTag(String hashTag)
    {
        this.hashTag = hashTag;
    }

    public String getHashTag()
    {
        return hashTag;
    }

    public void setPostCount(int postCount)
    {
        this.postCount = postCount;
    }

    public int getPostCount()
    {
        return postCount;
    }

    public String toString()
    {
        return "hashTag : " + hashTag + "\n" +
               "postCount : " + postCount ;
    }
}
