package com.machipopo.media17.model;

/**
 * Created by POPO on 2015/11/19.
 */
public class GiftModel
{
    private String giftID;
    private int point;
    private String name;
    private String icon;
    private String picture;
    private String soundTrack;
    private int sequence;
    private String archiveFileName;
    private int width;
    private int height;
    private int frameDuration;
    private int numOfImages;

    public void setGiftID(String giftID)
    {
        this.giftID = giftID;
    }

    public String getGiftID()
    {
        return giftID;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public int getPoint()
    {
        return point;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        return picture;
    }

    public void setSoundTrack(String soundTrack)
    {
        this.soundTrack = soundTrack;
    }

    public String getSoundTrack()
    {
        return soundTrack;
    }

    public void setSequence(int sequence)
    {
        this.sequence = sequence;
    }

    public int getSequence()
    {
        return sequence;
    }

    public void setArchiveFileName(String archiveFileName)
    {
        this.archiveFileName = archiveFileName;
    }

    public String getArchiveFileName()
    {
        return archiveFileName;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public int getWidth()
    {
        return width;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public int getHeight()
    {
        return height;
    }

    public void setFrameDuration(int frameDuration)
    {
        this.frameDuration = frameDuration;
    }

    public int getFrameDuration()
    {
        return frameDuration;
    }

    public void setNumOfImages(int numOfImages)
    {
        this.numOfImages = numOfImages;
    }

    public int getNumOfImages()
    {
        return numOfImages;
    }
}
