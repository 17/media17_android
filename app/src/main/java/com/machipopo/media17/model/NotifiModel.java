package com.machipopo.media17.model;

/**
 * Created by POPO on 5/15/15.
 */
public class NotifiModel
{
    private String message;
    private String commentID;
    private String postID;
    private String type;
    private String friendUserID;
    private int timestamp;
    private int isRead;
    private PostModel post;
    private UserModel user;
    private int isFollowRequest;
    private int isFriendRequest;
    private UserModel targetUserInfo;

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setCommentID(String commentID)
    {
        this.commentID = commentID;
    }

    public String getCommentID()
    {
        return commentID;
    }

    public void setPostID(String postID)
    {
        this.postID = postID;
    }

    public String getPostID()
    {
        return postID;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setFriendUserID(String friendUserID)
    {
        this.friendUserID = friendUserID;
    }

    public String getFriendUserID()
    {
        return friendUserID;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setIsRead(int isRead)
    {
        this.isRead = isRead;
    }

    public int getIsRead()
    {
        return isRead;
    }

    public void setPost(PostModel post)
    {
        this.post = post;
    }

    public PostModel getPost()
    {
        return post;
    }

    public void setUser(UserModel user)
    {
        this.user = user;
    }

    public UserModel getUser()
    {
        return user;
    }

    public void setIsFollowRequest(int isFollowRequest)
    {
        this.isFollowRequest = isFollowRequest;
    }

    public int getIsFollowRequest()
    {
        return isFollowRequest;
    }

    public void setIsFriendRequest(int isFriendRequest)
    {
        this.isFriendRequest = isFriendRequest;
    }

    public int getIsFriendRequest()
    {
        return isFriendRequest;
    }

    public void setTargetUserInfo(UserModel targetUserInfo)
    {
        this.targetUserInfo = targetUserInfo;
    }

    public UserModel getTargetUserInfo()
    {
        return targetUserInfo;
    }

    public String toString()
    {
        return "message : " + message + "\n" +
               "commentID : " + commentID + "\n" +
               "postID : " + postID + "\n" +
               "type : " + type + "\n" +
               "friendUserID : " + friendUserID + "\n" +
               "timestamp : " + timestamp + "\n" +
               "isRead : " + isRead + "\n" +
               "post : " + post.toString() + "\n" +
               "user : " + user.toString() + "\n" +
               "isFollowRequest : " + isFollowRequest + "\n" +
               "isFriendRequest : " + isFriendRequest ;
    }
}
