package com.machipopo.media17.model;

/**
 * Created by POPO on 2015/11/23.
 */
public class PointUsageModel
{
    private String type;
    private int point;
    private UserModel userInfo;
    private GiftModel giftInfo;
    private int timestamp;

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public int getPoint()
    {
        return point;
    }

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setGiftInfo(GiftModel giftInfo)
    {
        this.giftInfo = giftInfo;
    }

    public GiftModel getGiftInfo()
    {
        return giftInfo;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }
}
