package com.machipopo.media17.model;

/**
 * Created by POPO on 5/15/15.
 */
public class LocationModel
{
    private String locationID;
    private String name;
    private float latitude;
    private float longitude;

    public void setLocationID(String locationID)
    {
        this.locationID = locationID;
    }

    public String getLocationID()
    {
        return locationID;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setLatitude(float latitude)
    {
        this.latitude = latitude;
    }

    public float getLatitude()
    {
        return latitude;
    }

    public void setLongitude(float longitude)
    {
        this.longitude = longitude;
    }

    public float getLongitude()
    {
        return longitude;
    }

    public String toString()
    {
        return "locationID : " + locationID + "\n" +
               "name : " + name + "\n" +
               "latitude : " + latitude + "\n" +
               "longitude : " + longitude ;
    }
}
