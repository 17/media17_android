package com.machipopo.media17.model;

/**
 * Created by POPO on 15/8/14.
 */
public class LeaderboardModel
{
    public UserModel user;
    public int likeCount;

    public void setUser(UserModel user)
    {
        this.user = user;
    }

    public UserModel getUser()
    {
        return user;
    }

    public void setLikeCount(int likeCount)
    {
        this.likeCount = likeCount;
    }

    public int getLikeCount()
    {
        return likeCount;
    }
}
