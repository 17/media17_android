package com.machipopo.media17.model;

/**
 * Created by POPO_INNOVATION_IMAC on 15/12/30.
 */
public class SubscribersPurchaseModel {

    private UserModel userInfo;
    private int subscriptionTime;
    private float revenue;

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setSubscriptionTime(int subscriptionTime)
    {
        this.subscriptionTime = subscriptionTime;
    }

    public int getSubscriptionTime()
    {
        return subscriptionTime;
    }

    public void setRevenue(float revenue)
    {
        this.revenue = revenue;
    }

    public float getRevenue()
    {
        return revenue;
    }
}
