package com.machipopo.media17.model;

/**
 * Created by POPO on 15/10/6.
 */
public class LiveRegionModel
{
    private String region;
    private int liveCount;

    public void setRegion(String region)
    {
        this.region = region;
    }

    public String getRegion()
    {
        return region;
    }

    public void setLiveCount(int liveCount)
    {
        this.liveCount = liveCount;
    }

    public int getLiveCount()
    {
        return liveCount;
    }
}
