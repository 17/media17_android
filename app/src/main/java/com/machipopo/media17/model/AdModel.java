package com.machipopo.media17.model;

/**
 * Created by POPO on 5/15/15.
 */
public class AdModel
{
    private String adID;
    private String caption;
    private String actionName;
    private String androidAppPackageName;
    private String iosAppID;
    private String adUrl;
    private String urlScheme;
    private String picture;
    private String video;
    private String description;
    private String headline;
    private String bidType;
    private int clickPrice;
    private int impressionPrice;
    private int installPrice;
    private String tyoe;

    public void setAdID(String adID)
    {
        this.adID = adID;
    }

    public String getAdID()
    {
        return adID;
    }

    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    public String getCaption()
    {
        return caption;
    }

    public void setActionName(String actionName)
    {
        this.actionName = actionName;
    }

    public String getActionName()
    {
        return actionName;
    }

    public void setAndroidAppPackageName(String androidAppPackageName)
    {
        this.androidAppPackageName = androidAppPackageName;
    }

    public String getAndroidAppPackageName()
    {
        return androidAppPackageName;
    }

    public void setIosAppID(String iosAppID)
    {
        this.iosAppID = iosAppID;
    }

    public String getIosAppID()
    {
        return iosAppID;
    }

    public void setAdUrl(String adUrl)
    {
        this.adUrl = adUrl;
    }

    public String getAdUrl()
    {
        return adUrl;
    }

    public void setUrlScheme(String urlScheme)
    {
        this.urlScheme = urlScheme;
    }

    public String getUrlScheme()
    {
        return urlScheme;
    }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        return picture;
    }

    public void setVideo(String video)
    {
        this.video = video;
    }

    public String getVideo()
    {
        return video;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public void setHeadline(String headline)
    {
        this.headline = headline;
    }

    public String getHeadline()
    {
        return headline;
    }

    public void setBidType(String bidType)
    {
        this.bidType = bidType;
    }

    public String getBidType()
    {
        return bidType;
    }

    public void setClickPrice(int clickPrice)
    {
        this.clickPrice = clickPrice;
    }

    public int getClickPrice()
    {
        return clickPrice;
    }

    public void setImpressionPrice(int impressionPrice)
    {
        this.impressionPrice = impressionPrice;
    }

    public int getImpressionPrice()
    {
        return impressionPrice;
    }

    public void setInstallPrice(int installPrice)
    {
        this.installPrice = installPrice;
    }

    public int getInstallPrice()
    {
        return installPrice;
    }

    public void setTyoe(String tyoe)
    {
        this.tyoe = tyoe;
    }

    public String getTyoe()
    {
        return tyoe;
    }

    public String toString()
    {
        return "adID : " + adID + "\n" +
                "caption : " + caption + "\n" +
                "actionName : " + actionName + "\n" +
                "androidAppPackageName : " + androidAppPackageName + "\n" +
                "iosAppID : " + iosAppID + "\n" +
                "adUrl : " + adUrl + "\n" +
                "urlScheme : " + urlScheme + "\n" +
                "picture : " + picture + "\n" +
                "video : " + video + "\n" +
                "description : " + description + "\n" +
                "headline : " + headline + "\n" +
                "bidType : " + bidType + "\n" +
                "clickPrice : " + clickPrice + "\n" +
                "impressionPrice : " + impressionPrice + "\n" +
                "installPrice : " + installPrice + "\n" +
                "tyoe : " + tyoe ;
    }
}
