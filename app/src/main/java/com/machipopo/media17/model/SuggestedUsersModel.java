package com.machipopo.media17.model;

import java.util.ArrayList;

/**
 * Created by POPO on 5/26/15.
 */
public class SuggestedUsersModel
{
    private String userID;
    private String openID;
    private String name;
    private String picture;

    private int isFollowing;
    private int postCount;
    private int followerCount;
    private int followingCount;
    private String bio;
    private String website;
    private int timestamp;
    private int lastLogin;
    private int isVerified;

    private int followRequestTime;
    private String privacyMode;

    private ArrayList<PostModel> postInfo;

    public void setPostInfo(ArrayList<PostModel> postInfo)
    {
        this.postInfo = postInfo;
    }

    public ArrayList<PostModel> getPostInfo()
    {
        return postInfo;
    }

    public void setUserID(String userID)
    {
        this.userID = userID;
    }

    public String getUserID()
    {
        return userID;
    }

    public void setOpenID(String openID)
    {
        this.openID = openID;
    }

    public String getOpenID()
    {
        return openID;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        return picture;
    }

    public void setIsFollowing(int isFollowing)
    {
        this.isFollowing = isFollowing;
    }

    public int getIsFollowing()
    {
        return isFollowing;
    }

    public void setPostCount(int postCount)
    {
        this.postCount = postCount;
    }

    public int getPostCount()
    {
        return postCount;
    }

    public void setFollowerCount(int followerCount)
    {
        this.followerCount = followerCount;
    }

    public int getFollowerCount()
    {
        return followerCount;
    }

    public void setFollowingCount(int followingCount)
    {
        this.followingCount = followingCount;
    }

    public int getFollowingCount()
    {
        return followingCount;
    }

    public void setBio(String bio)
    {
        this.bio = bio;
    }

    public String getBio()
    {
        return bio;
    }

    public void setWebsite(String website)
    {
        this.website = website;
    }

    public String getWebsite()
    {
        return website;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setLastLogin(int lastLogin)
    {
        this.lastLogin = lastLogin;
    }

    public void setIsVerified(int isVerified)
    {
        this.isVerified = isVerified;
    }

    public int getIsVerified()
    {
        return isVerified;
    }

    public void setFollowRequestTime(int followRequestTime)
    {
        this.followRequestTime = followRequestTime;
    }

    public int getFollowRequestTime()
    {
        return followRequestTime;
    }

    public void setPrivacyMode(String privacyMode)
    {
        this.privacyMode = privacyMode;
    }

    public String getPrivacyMode()
    {
        if(null == privacyMode){
            return "";
        }
        else{
            return privacyMode;
        }
    }

    public int getLastLogin()
    {
        return lastLogin;
    }

//    public String toString()
//    {
//        return "postInfo : " + postInfo.toString() + "\n" +
//                "userInfo : " + userInfo.toString() ;
//    }
}
