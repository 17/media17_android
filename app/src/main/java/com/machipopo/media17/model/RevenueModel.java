package com.machipopo.media17.model;

/**
 * Created by POPO on 6/10/15.
 */
public class RevenueModel
{
    private int year;
    private int month;
    private int day;
    private int totalViews;
    private int payed;
    private float revenue;
    private int timestamp;

    public void setYear(int year)
    {
        this.year = year;
    }

    public int getYear()
    {
        return year;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }

    public int getMonth()
    {
        return month;
    }

    public void setDay(int day)
    {
        this.day = day;
    }

    public int getDay()
    {
        return day;
    }

    public void setTotalViews(int totalViews)
    {
        this.totalViews = totalViews;
    }

    public int getTotalViews()
    {
        return totalViews;
    }

    public void setRevenue(float revenue)
    {
        this.revenue = revenue;
    }

    public float getRevenue()
    {
        return revenue;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setPayed(int payed)
    {
        this.payed = payed;
    }

    public int getPayed()
    {
        return payed;
    }
}
