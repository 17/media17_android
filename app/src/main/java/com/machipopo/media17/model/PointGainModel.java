package com.machipopo.media17.model;

/**
 * Created by POPO on 2015/11/23.
 */
public class PointGainModel
{
    private String type;
    private int point;
    private ProductModel productInfo;
    private int timestamp;

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setPoint(int point)
    {
        this.point = point;
    }

    public int getPoint()
    {
        return point;
    }

    public void setProductInfo(ProductModel productInfo)
    {
        this.productInfo = productInfo;
    }

    public ProductModel getProductInfo()
    {
        return productInfo;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }
}
