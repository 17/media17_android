package com.machipopo.media17.model;

import java.util.ArrayList;

/**
 * Created by POPO_INNOVATION_IMAC on 15/12/30.
 */
public class SubscriberUserModel {

    private UserModel userInfo;
    private int subscriptionTime;
    private int subscriptionEndTime;

    public void setUserInfo(UserModel userInfo)
    {
        this.userInfo = userInfo;
    }

    public UserModel getUserInfo()
    {
        return userInfo;
    }

    public void setSubscriptionTime(int subscriptionTime)
    {
        this.subscriptionTime = subscriptionTime;
    }

    public int getSubscriptionTime()
    {
        return subscriptionTime;
    }

    public void setSubscriptionEndTime(int subscriptionEndTime)
    {
        this.subscriptionEndTime = subscriptionEndTime;
    }

    public int getSubscriptionEndTime()
    {
        return subscriptionEndTime;
    }

}
