package com.machipopo.media17.model;

import com.facebook.ads.NativeAd;

/**
 * Created by POPO on 7/20/15.
 */
public class LiveFeedModel
{
    private LiveModel liveStreams;
    private FeedModel posts;
    private int type = 0;
    private NativeAd nativeAd;

    public void setLiveStreams(LiveModel liveStreams)
    {
        this.liveStreams = liveStreams;
    }

    public LiveModel getLiveStreams()
    {
        return liveStreams;
    }

    public void setPosts(FeedModel posts)
    {
        this.posts = posts;
    }

    public FeedModel getPosts()
    {
        return posts;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public int getType()
    {
        return type;
    }

    public void setNativeAd(NativeAd nativeAd)
    {
        this.nativeAd = nativeAd;
    }

    public NativeAd getNativeAd()
    {
        return nativeAd;
    }
}
