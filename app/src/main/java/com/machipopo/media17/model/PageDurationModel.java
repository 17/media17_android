package com.machipopo.media17.model;

/**
 * Created by POPO_INNOVATION_IMAC on 16/1/22.
 */
public class PageDurationModel {

    private int day;
    private int hour;
    private int minute;
    private int second;

    public void setDay (int day)
    {
        this.day = day;
    }

    public int getDay ()
    {
        return day;
    }

    public void setHour (int hour)
    {
        this.hour = hour;
    }

    public int getHour ()
    {
        return hour;
    }

    public void setMinute (int minute)
    {
        this.minute = minute;
    }

    public int getMinute ()
    {
        return minute;
    }

    public void setSecond (int second)
    {
        this.second = second;
    }

    public int getSecond ()
    {
        return second;
    }

}
