package com.machipopo.media17.model;

/**
 * Created by POPO on 5/13/15.
 */
public class UserModel
{
    private String userID;
    private String name;
    private String picture;
    private String gender;
    private String bio;
    private String website;
    private int age;
    private int isVerified;
    private int followerCount;
    private int followingCount;
    private int postCount;
    private int repostCount;
    private int likePostCount;
    private int isFollowing;
    private int isBlocked;
    private String privacyMode;
    private String openID;

    private String phoneNumber;
    private String coverPhoto;
    private int isChoice;
    private int lastLogin;
    private String email;
    private String countryCode;

    private String facebookID;
    private int receivedLikeCount;
    private int blockTime;
    private int followTime;
    private int likeCount;

    private int enterTime;

    private String pushLiveStream;
    private String pushLiveRestream;
    private String deviceModel;

    private int isAdmin;
    private int isFreezed;

    private String pushLike;
    private int pushFollow;
    private String pushComment;
    private String pushTag;
    private int pushFriendJoin;
    private String pushFriendFirstPost;
    private int pushFriendRequest;
    private int pushSystemNotif;
//    private int pushLiveStream;
//    private int pushLiveRestream;

    private int notifBadge;
    private int systemNotifBadge;
    private int pushFollowRequest;
    private int messageBadge;
    private int requestBadge;

    private String bankName;
    private String branchName;
    private String accountNumber;
    private String beneficiaryName;
    private String idCardName;
    private String idCardNumber;
    private String idCardFrontPicture;
    private String idCardBackPicture;
    private String passbookPicture;
    private int bankAccountVerified;

    private String bankAddress;
    private String swiftCode;
    private String rountingNumber;
    private String bankCountry;
    private String address;
    private String bankCode;
    private String bankBranchCode;

//    private double totalRevenueEarned;
    private String twitterID;
    private String instagramID;
    private int openIDModifyTime;
//    private int accountFacebookID;
//    private String deviceModel;
    private int followRequestTime;

    private String paypalEmail;
    private String alipayPhone;

    private int adsOn;
    private String verifiedPhoneNumber;

    private int unLockUser;
    private int followPrivacyMode;
    private int point;
    private float totalGiftRevenueEarned;

    private int giftModuleState;
    private int hasShownGiftModuleTutorial;
    private String appUpdateLink;
    private int forceUpdateApp;
    private String latestAppVersion;

    private String countryCallingCode;
    private String localPhoneNumber;
    private String phoneTwoDigitISO;

    public void setPoint(int point)
    {
        this.point = point;
    }

    public int getPoint()
    {
        return point;
    }

    public void setTotalGiftRevenueEarned(float totalGiftRevenueEarned)
    {
        this.totalGiftRevenueEarned = totalGiftRevenueEarned;
    }

    public float getTotalGiftRevenueEarned()
    {
        return totalGiftRevenueEarned;
    }

    public void setUserID(String userID)
    {
        this.userID = userID;
    }

    public String getUserID()
    {
        if(null == userID){
            return "";
        }
        else{
            return userID;
        }
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        if(null == name){
            return "";
        }
        else {
            return name;
        }
    }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        if(null == picture){
            return "";
        }
        else{
            return picture;
        }
    }

    public void setGender(String genber)
    {
        this.gender = gender;
    }

    public String getGender()
    {
        if(null == gender){
            return "";
        }
        else {
            return gender;
        }
    }

    public void setBio(String bio)
    {
        this.bio = bio;
    }

    public String getBio()
    {
        if(null == bio){
            return "";
        }
        else {
            return bio;
        }
    }

    public void setWebsite(String website)
    {
        this.website = website;
    }

    public String getWebsite()
    {
        if(null == website){
            return "";
        }
        else {
            return website;
        }
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public int getAge()
    {
        return age;
    }

    public void setIsVerified(int isVerified)
    {
        this.isVerified = isVerified;
    }

    public int getIsVerified()
    {
        return isVerified;
    }

    public void setFollowerCount(int followerCount)
    {
        this.followerCount = followerCount;
    }

    public int getFollowerCount()
    {
        return followerCount;
    }

    public void setFollowingCount(int followingCount)
    {
        this.followingCount = followingCount;
    }

    public int getFollowingCount()
    {
        return followingCount;
    }

    public void setPostCount(int postCount)
    {
        this.postCount = postCount;
    }

    public int getPostCount()
    {
        return postCount;
    }

    public void setRepostCount(int repostCount)
    {
        this.repostCount = repostCount;
    }

    public int getRepostCount()
    {
        return repostCount;
    }

    public void setLikePostCount(int likePostCount)
    {
        this.likePostCount = likePostCount;
    }

    public int getLikePostCount()
    {
        return likePostCount;
    }

    public void setIsFollowing(int isFollowing)
    {
        this.isFollowing = isFollowing;
    }

    public int getIsFollowing()
    {
        return isFollowing;
    }

    public void setIsBlocked(int isBlocked)
    {
        this.isBlocked = isBlocked;
    }

    public int getIsBlocked()
    {
        return isBlocked;
    }

    public void setPrivacyMode(String privacyMode)
    {
        this.privacyMode = privacyMode;
    }

    public String getPrivacyMode()
    {
        if(null == privacyMode){
            return "";
        }
        else{
            return privacyMode;
        }
    }

    public void setOpenID(String openID)
    {
        this.openID = openID;
    }

    public String getOpenID()
    {
        if(null == openID){
            return "";
        }
        else{
            return openID;
        }
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber()
    {
        if(null == phoneNumber){
            return "";
        }
        else {
            return phoneNumber;
        }
    }

    public void setCoverPhoto(String coverPhoto)
    {
        this.coverPhoto = coverPhoto;
    }

    public String getCoverPhoto()
    {
        if(null == coverPhoto){
            return "";
        }
        else {
            return coverPhoto;
        }
    }

    public void setIsChoice(int isChoice)
    {
        this.isChoice = isChoice;
    }

    public int getIsChoice()
    {
        return isChoice;
    }

    public void setLastLogin(int lastLogin)
    {
        this.lastLogin = lastLogin;
    }

    public int getLastLogin()
    {
        return lastLogin;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        if(null==email){
            return "";
        }
        else {
            return email;
        }
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getCountryCode()
    {
        if(null == countryCode){
            return "";
        }
        else {
            return countryCode;
        }
    }

    public void setFacebookID(String facebookID)
    {
        this.facebookID = facebookID;
    }

    public String getFacebookID()
    {
        if(null == facebookID){
            return "";
        }
        else {
            return facebookID;
        }
    }

    public void setReceivedLikeCount(int receivedLikeCount)
    {
        this.receivedLikeCount = receivedLikeCount;
    }

    public int getReceivedLikeCount()
    {
        return receivedLikeCount;
    }

    public void setBlockTime(int blockTime)
    {
        this.blockTime = blockTime;
    }

    public int getBlockTime()
    {
        return blockTime;
    }

    public void setFollowTime(int followTime)
    {
        this.followTime = followTime;
    }

    public int getFollowTime()
    {
        return followTime;
    }

    public void setLikeCount(int likeCount)
    {
        this.likeCount = likeCount;
    }

    public int getLikeCount()
    {
        return likeCount;
    }

    public void setEnterTime(int enterTime)
    {
        this.enterTime = enterTime;
    }

    public int getEnterTime()
    {
        return enterTime;
    }

    public void setPushLiveStream(String pushLiveStream)
    {
        this.pushLiveStream = pushLiveStream;
    }

    public String getPushLiveStream()
    {
        if(null == pushLiveStream){
            return "";
        }
        else {
            return pushLiveStream;
        }
    }

    public void setPushLiveRestream(String pushLiveRestream)
    {
        this.pushLiveRestream = pushLiveRestream;
    }

    public String getPushLiveRestream()
    {
        if(null == pushLiveRestream){
            return "";
        }
        else {
            return pushLiveRestream;
        }
    }

    public void setDeviceModel(String deviceModel)
    {
        this.deviceModel = deviceModel;
    }

    public String getDeviceModel()
    {
        if(null == deviceModel){
            return "";
        }
        else {
            return deviceModel;
        }
    }

    public void setIsAdmin(int isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    public int getIsAdmin()
    {
        return isAdmin;
    }

    public void setIsFreezed(int isFreezed)
    {
        this.isFreezed = isFreezed;
    }

    public int getIsFreezed()
    {
        return isFreezed;
    }

    public void setPushLike(String pushLike)
    {
        this.pushLike = pushLike;
    }

    public String getPushLike()
    {
        return pushLike;
    }

    public void setPushFollow(int pushFollow)
    {
        this.pushFollow = pushFollow;
    }

    public int getPushFollow()
    {
        return pushFollow;
    }

    public void setPushComment(String pushComment)
    {
        this.pushComment = pushComment;
    }

    public String getPushComment()
    {
        return pushComment;
    }

    public void setPushTag(String pushTag)
    {
        this.pushTag = pushTag;
    }

    public String getPushTag()
    {
        return pushTag;
    }

    public void setPushFriendJoin(int pushFriendJoin)
    {
        this.pushFriendJoin = pushFriendJoin;
    }

    public int getPushFriendJoin()
    {
        return pushFriendJoin;
    }

    public void setPushFriendFirstPost(String pushFriendFirstPost)
    {
        this.pushFriendFirstPost = pushFriendFirstPost;
    }

    public String getPushFriendFirstPost()
    {
        return pushFriendFirstPost;
    }

    public void setPushFriendRequest(int pushFriendRequest)
    {
        this.pushFriendRequest = pushFriendRequest;
    }

    public int getPushFriendRequest()
    {
        return pushFriendRequest;
    }

    public void setPushSystemNotif(int pushSystemNotif)
    {
        this.pushSystemNotif = pushSystemNotif;
    }

    public int getPushSystemNotif()
    {
        return pushSystemNotif;
    }

    public void setBankName(String bankName)
    {
        this.bankName = bankName;
    }

    public String getBankName()
    {
        return bankName;
    }

    public void setBranchName(String branchName)
    {
        this.branchName = branchName;
    }

    public String getBranchName()
    {
        return branchName;
    }

    public void setAccountNumber(String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setBeneficiaryName(String beneficiaryName)
    {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryName()
    {
        return beneficiaryName;
    }

    public void setIdCardName(String idCardName)
    {
        this.idCardName = idCardName;
    }

    public String getIdCardName()
    {
        return idCardName;
    }

    public void setIdCardNumber(String idCardNumber)
    {
        this.idCardNumber = idCardNumber;
    }

    public String getIdCardNumber()
    {
        return idCardNumber;
    }

    public void setIdCardFrontPicture(String idCardFrontPicture)
    {
        this.idCardFrontPicture = idCardFrontPicture;
    }

    public String getIdCardFrontPicture()
    {
        return idCardFrontPicture;
    }

    public void setIdCardBackPicture(String idCardBackPicture)
    {
        this.idCardBackPicture = idCardBackPicture;
    }

    public String getIdCardBackPicture()
    {
        return idCardBackPicture;
    }

    public void setPassbookPicture(String passbookPicture)
    {
        this.passbookPicture = passbookPicture;
    }

    public String getPassbookPicture()
    {
        return passbookPicture;
    }

    public void setBankAccountVerified(int bankAccountVerified)
    {
        this.bankAccountVerified = bankAccountVerified;
    }

    public int getBankAccountVerified()
    {
        return bankAccountVerified;
    }

    public void setFollowRequestTime(int followRequestTime)
    {
        this.followRequestTime = followRequestTime;
    }

    public int getFollowRequestTime()
    {
        return followRequestTime;
    }

    public void setPaypalEmail(String paypalEmail)
    {
        this.paypalEmail = paypalEmail;
    }

    public String getPaypalEmail()
    {
        return paypalEmail;
    }

    public void setAlipayPhone(String alipayPhone)
    {
        this.alipayPhone = alipayPhone;
    }

    public String getAlipayPhone()
    {
        return alipayPhone;
    }

    public void setAdsOn(int adsOn)
    {
        this.adsOn = adsOn;
    }

    public int getAdsOn()
    {
        return adsOn;
    }

    public void setNotifBadge(int notifBadge)
    {
        this.notifBadge = notifBadge;
    }

    public int getNotifBadge()
    {
        return notifBadge;
    }

    public void setSystemNotifBadge(int systemNotifBadge)
    {
        this.systemNotifBadge = systemNotifBadge;
    }

    public int getSystemNotifBadge()
    {
        return systemNotifBadge;
    }

    public void setMessageBadge(int messageBadge)
    {
        this.messageBadge = messageBadge;
    }

    public int getMessageBadge()
    {
        return messageBadge;
    }

    public void setRequestBadge(int requestBadge)
    {
        this.requestBadge = requestBadge;
    }

    public int getRequestBadge()
    {
        return requestBadge;
    }

    public void setVerifiedPhoneNumber(String verifiedPhoneNumber)
    {
        this.verifiedPhoneNumber = verifiedPhoneNumber;
    }

    public String getVerifiedPhoneNumber()
    {
        return verifiedPhoneNumber;
    }

    public void setUnLockUser(int unLockUser)
    {
        this.unLockUser = unLockUser;
    }

    public int getUnLockUser()
    {
        return unLockUser;
    }

    public void setFollowPrivacyMode(int followPrivacyMode)
    {
        this.followPrivacyMode = followPrivacyMode;
    }

    public int getFollowPrivacyMode()
    {
        return followPrivacyMode;
    }

    public void setGiftModuleState(int giftModuleState)
    {
        this.giftModuleState = giftModuleState;
    }

    public int getGiftModuleState()
    {
        return giftModuleState;
    }

    public void setHasShownGiftModuleTutorial(int hasShownGiftModuleTutorial)
    {
        this.hasShownGiftModuleTutorial = hasShownGiftModuleTutorial;
    }

    public int getHasShownGiftModuleTutorial()
    {
        return hasShownGiftModuleTutorial;
    }

    public void setAppUpdateLink(String appUpdateLink)
    {
        this.appUpdateLink = appUpdateLink;
    }

    public String getAppUpdateLink()
    {
        return appUpdateLink;
    }

    public void setForceUpdateApp(int forceUpdateApp)
    {
        this.forceUpdateApp = forceUpdateApp;
    }

    public int getForceUpdateApp()
    {
        return forceUpdateApp;
    }

    public void setLatestAppVersion(String latestAppVersion)
    {
        this.latestAppVersion = latestAppVersion;
    }

    public String getLatestAppVersion()
    {
        return latestAppVersion;
    }

    public void setCountryCallingCode(String countryCallingCode)
    {
        this.countryCallingCode = countryCallingCode;
    }

    public String getCountryCallingCode()
    {
        return countryCallingCode;
    }

    public void setLocalPhoneNumber(String localPhoneNumber)
    {
        this.localPhoneNumber = localPhoneNumber;
    }

    public String getLocalPhoneNumber()
    {
        return localPhoneNumber;
    }

    public void setPhoneTwoDigitISO(String phoneTwoDigitISO)
    {
        this.phoneTwoDigitISO = phoneTwoDigitISO;
    }

    public String getPhoneTwoDigitISO()
    {
        return phoneTwoDigitISO;
    }

    public String toString()
    {
        return "userID = " + userID + "\n" +
                "name = " + name + "\n" +
                "picture = " + picture + "\n" +
                "gender = " + gender + "\n" +
                "bio = " + bio + "\n" +
                "website = " + website + "\n" +
                "age = " + age + "\n" +
                "isVerified = " + isVerified + "\n" +
                "followerCount = " + followerCount + "\n" +
                "followingCount = " + followingCount + "\n" +
                "postCount = " + postCount + "\n" +
                "repostCount = " + repostCount + "\n" +
                "likePostCount = " + likePostCount + "\n" +
                "isFollowing = " + isFollowing + "\n" +
                "isBlocked = " + isBlocked + "\n" +
                "privacyMode = " + privacyMode + "\n" +
                "openID = " + openID + "\n" +
                "phoneNumber = " + phoneNumber + "\n" +
                "coverPhoto = " + coverPhoto + "\n" +
                "isChoice = " + isChoice + "\n" +
                "lastLogin = " + lastLogin ;
    }
}
