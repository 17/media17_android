package com.machipopo.media17.model;

/**
 * Created by POPO on 5/15/15.
 */
public class FriendNotifModel
{
    private UserModel friendUser;
    private UserModel targetUser;
    private String type;
    private int timestamp;
    private String postID;
    private PostModel post;

    public void setFriendUser(UserModel friendUser)
    {
        this.friendUser = friendUser;
    }

    public UserModel getFriendUser()
    {
        return friendUser;
    }

    public void setTargetUser(UserModel targetUser)
    {
        this.targetUser = targetUser;
    }

    public UserModel getTargetUser()
    {
        return targetUser;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setTimestamp(int timestamp)
    {
        this.timestamp = timestamp;
    }

    public int getTimestamp()
    {
        return timestamp;
    }

    public void setPostID(String postID)
    {
        this.postID = postID;
    }

    public String getPostID()
    {
        return postID;
    }

    public void setPost(PostModel post)
    {
        this.post = post;
    }

    public PostModel getPost()
    {
        return post;
    }

    public String toString()
    {
        return "friendUser : " + friendUser.toString() + "\n" +
               "targetUser : " + targetUser.toString() + "\n" +
               "type : " + type + "\n" +
               "timestamp : " + timestamp + "\n" +
               "postID : " + postID + "\n" +
               "post : " + post.toString() ;
    }
}
