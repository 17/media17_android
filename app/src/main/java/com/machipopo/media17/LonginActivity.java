package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by POPO on 5/21/15.
 */
public class LonginActivity extends BaseActivity
{
    private LonginActivity mCtx = this;
    private EditText mAccount, mPasswoird;
    private ConnectivityManager mConnectivityManager;
    private NetworkInfo mNetworkInfo;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.longin_activity);

        initTitleBar();

        mAccount = (EditText) findViewById(R.id.account);
        mPasswoird = (EditText) findViewById(R.id.password);

        showKeyboard();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.log_in_only));

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_white_back);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mCtx, LoginMenuActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.done));
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mAccount.getText().toString().length()!=0 && mPasswoird.getText().toString().length()!=0)
                {
                    mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

                    if (mNetworkInfo != null && mNetworkInfo.isConnected())
                    {
                        hideKeyboard();
                        showProgressDialog();
                        ApiManager.loginAction(mCtx,mAccount.getText().toString(), mPasswoird.getText().toString(), new ApiManager.LoginActionCallback()
                        {
                            @Override
                            public void onResult(boolean success, String message, UserModel user)
                            {
                                hideProgressDialog();

                                if(success)
                                {
                                    if (message.equals("ok"))
                                    {
                                        setConfig(Constants.ACCOUNT,mAccount.getText().toString());
                                        setConfig(Constants.PASSWORD,mPasswoird.getText().toString());

                                        Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                        mStory17Application.setUser(user);

                                        Intent intent = new Intent();
                                        intent.setClass(mCtx, MenuActivity.class);
                                        startActivity(intent);
                                        mCtx.finish();
                                    }
                                    else if (message.equals("freezed")) showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                    else showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                }
                                else showNetworkUnstableToast();
                            }
                        });
                    }
                    else
                    {
                        try{
//                              showToast(getString(R.string.login_internet));
                            Toast.makeText(mCtx, getString(R.string.login_internet), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
                else {
                    try{
//                              showToast(getString(R.string.login_enter));
                        Toast.makeText(mCtx, getString(R.string.login_enter), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intent = new Intent();
            intent.setClass(mCtx, LoginMenuActivity.class);
            startActivity(intent);
            mCtx.finish();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
