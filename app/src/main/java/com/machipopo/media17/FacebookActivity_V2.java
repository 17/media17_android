package com.machipopo.media17;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by POPO on 5/21/15.
 */
public class FacebookActivity_V2 extends BaseActivity
{
    private FacebookActivity_V2 mCtx = this;
    private CallbackManager callbackManager;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook_activity_v2);

        initTitleBar();

        Button mFb = (Button) findViewById(R.id.btn_fb);
        mFb.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showProgressDialog();

                FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>()
                        {
                            @Override
                            public void onSuccess(final LoginResult loginResult)
                            {
                                final String token = loginResult.getAccessToken().getToken();

                                GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback()
                                {
                                    @Override
                                    public void onCompleted(JSONObject user, GraphResponse response)
                                    {
                                        if (user != null)
                                        {
                                            try
                                            {
                                                JSONObject params = new JSONObject();
                                                params.put("facebookID", user.optString("id"));
                                                params.put("facebookAccessToken", token);

                                                final String NAME = user.optString("name");

                                                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                                                {
                                                    @Override
                                                    public void onResult(boolean success, String message)
                                                    {
                                                        if (success)
                                                        {
                                                            GraphRequest.newMyFriendsRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONArrayCallback()
                                                            {
                                                                @Override
                                                                public void onCompleted(JSONArray users, GraphResponse graphResponse)
                                                                {
                                                                    try
                                                                    {
                                                                        final JSONArray mJSONArray = new JSONArray();
                                                                        for (int i = 0; i < users.length(); i++)
                                                                        {
                                                                            JSONObject mObject = new JSONObject();
                                                                            mObject.put("id",users.getJSONObject(i).getString("id"));
                                                                            mObject.put("name",users.getJSONObject(i).getString("name"));
                                                                            mJSONArray.put(mObject);
                                                                        }

                                                                        ApiManager.uploadFacebookFriends(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), NAME, mJSONArray, new ApiManager.UploadFacebookFriendsCallback()
                                                                        {
                                                                            @Override
                                                                            public void onResult(boolean success, String message)
                                                                            {
                                                                                hideProgressDialog();
                                                                                if(success)
                                                                                {
                                                                                    Intent intent = new Intent();
                                                                                    intent.setClass(mCtx,FacebookFriendActivity.class);
                                                                                    intent.putExtra("facebook",mJSONArray.toString());
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                }
                                                                                else {
                                                                                    try{
//                    showToast(getString(R.string.failed));
                                                                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                                                    }
                                                                                    catch (Exception x){
                                                                                    }
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                    catch (JSONException e)
                                                                    {
                                                                        hideProgressDialog();
                                                                        try{
//                    showToast(getString(R.string.failed));
                                                                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                                        }
                                                                        catch (Exception x){
                                                                        }
                                                                    }
                                                                }
                                                            }).executeAsync();
                                                        }
                                                        else
                                                        {
                                                            hideProgressDialog();
                                                            try{
//                    showToast(getString(R.string.failed));
                                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                            }
                                                            catch (Exception x){
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                            catch (JSONException e)
                                            {
                                                hideProgressDialog();
                                                try{
//                    showToast(getString(R.string.failed));
                                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                            }
                                        }
                                    }
                                }).executeAsync();
                            }

                            @Override
                            public void onCancel()
                            {
                                hideProgressDialog();
                                try{
//                    showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }

                            @Override
                            public void onError(FacebookException exception)
                            {
                                hideProgressDialog();
                                try{
//                    showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        });
            }
        });
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.fb_friend));
        mTitle.setTextColor(Color.WHITE);

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.skip));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,FollowFriendActivityV2.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    private void showLoginFailDialog()
    {
        hideProgressDialog();
        showAlertDialog(getString(R.string.login_fail), getString(R.string.fb_login_reject_prompt));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
//            Intent intent = new Intent();
//            intent.setClass(mCtx,PhoneNumberActivity.class);
//            startActivity(intent);
//            mCtx.finish();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(callbackManager!=null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
