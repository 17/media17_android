package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.iapppay.interfaces.callback.IPayResultCallback;
import com.iapppay.pay.mobile.iapppaysecservice.utils.IAppPaySDKConfig;
import com.iapppay.sdk.main.IAppPay;
import com.iapppay.sdk.main.IAppPayOrderUtils;
import com.machipopo.media.OpusCodec;
import com.machipopo.media.PCMAudioPlayer;
import com.machipopo.media.VP8Codec;
import com.machipopo.media.VideoRenderer;
import com.machipopo.media17.View.BitmapBlurHelper;
import com.machipopo.media17.View.GiftView;
import com.machipopo.media17.View.RelativeSlideLayout;
import com.machipopo.media17.model.GiftLeaderboardModel;
import com.machipopo.media17.model.GiftLivestreamLeaderBoardModel;
import com.machipopo.media17.model.GiftModel;
import com.machipopo.media17.model.LiveComment;
import com.machipopo.media17.model.LiveGiftsModel;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.LiveStreamModel;
import com.machipopo.media17.model.MyEditText;
import com.machipopo.media17.model.PageDurationModel;
import com.machipopo.media17.model.ProductModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.util.IabHelper;
import com.machipopo.media17.util.IabResult;
import com.machipopo.media17.util.Inventory;
import com.machipopo.media17.util.Purchase;
import com.machipopo.media17.utils.LogEventUtil;
import com.machipopo.media17.utils.ShareUtil;
import com.machipopo.media17.utils.ZipUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.plattysoft.leonids.ParticleSystem;
import com.plattysoft.leonids.modifiers.ScaleModifier;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tencent.connect.UserInfo;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXImageObject;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import com.tencent.mm.sdk.platformtools.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class LiveStreamActivity extends BaseActivity implements PCMAudioPlayer.PCMAudioPlayerDelegate , MyEditText.EditKeyBackListener , InterstitialAdListener , GiftView.GiftEndListener {

    @Override
    public void OnEditKeyBack() {
        if(mEdit_layout.isShown())
        {
            editKeyState = true;
            mEdit_layout.setVisibility(View.INVISIBLE);
            hideKeyboard();

            mComment.setVisibility(View.VISIBLE);
            mLove.setVisibility(View.VISIBLE);
            mAllview.setVisibility(View.VISIBLE);
            mRestream.setVisibility(View.VISIBLE);
            mGift.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnGiftEnd()
    {
        if(mLiveGiftsModels.size()!=0)
        {
            mGiftShowState = false;
            mLiveGiftsModels.remove(0);

            if(mLiveGiftsModels.size()!=0)
            {
                mGiftShowState = true;

                File file = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
                if (file.exists())
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mGiftCommentBest.setVisibility(View.GONE);
                            mGiftComment.setVisibility(View.GONE);
                            mGiftAni.removeAllViews();
                            GiftView mGiftView = new GiftView(mCtx, mLiveGiftsModels.get(0).getGiftInfo().getFrameDuration(), mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels, mLiveGiftsModels.get(0).getGiftInfo().getGiftID(), mLiveGiftsModels.get(0).getGiftInfo().getNumOfImages(),mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
                            mGiftView.mGiftEndListener = mCtx;
                            mGiftAni.addView(mGiftView);
                            showGiftComment(mLiveGiftsModels.get(0).getGiftInfo(),mLiveGiftsModels.get(0).getUserInfo().getOpenID(),mLiveGiftsModels.get(0).getUserInfo().getPicture());

                            try {
                                if(mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack().length()!=0) {
                                    File sound = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                    if(sound.exists()){
                                        if(mMp3.isPlaying()){
                                            mMp3.stop();
                                        }

                                        mMp3 = null;
                                        mMp3 = new MediaPlayer();
                                        mMp3.setDataSource(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                        mMp3.prepare();
                                        mMp3.start();
                                    }
                                }
                            }
                            catch (IOException i) {
                            }
                            catch (Exception e){
                            }
                        }
                    });
                }
            }
            else
            {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mGiftCommentBest.setVisibility(View.GONE);
                        mGiftComment.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    private class VideoFrameObject extends Object {

        public VideoFrameObject() {
            yDataByteBuffer = ByteBuffer.allocateDirect(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT);
            uDataByteBuffer = ByteBuffer.allocateDirect(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT / 4);
            vDataByteBuffer = ByteBuffer.allocateDirect(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT / 4);
        }

        public long absTimestamp;
        public int videoFrameByteBufferLength;

        ByteBuffer videoFrameByteBuffer; // encoded data
        ByteBuffer yDataByteBuffer; // decoded data
        ByteBuffer uDataByteBuffer; // decoded data
        ByteBuffer vDataByteBuffer; // decoded data
    }

    private class AudioFrameObject extends Object {

        public AudioFrameObject() {
            audioDataByteBuffer = ByteBuffer.allocateDirect(1920 * 2);
        }

        long absTimestamp;
        public int audioFrameByteBufferLength;

        ByteBuffer audioFrameByteBuffer; // encoded data
        ByteBuffer audioDataByteBuffer; // decoded data
    }

    private class ChunkDataObject extends Object implements Comparable<ChunkDataObject> {
        public ChunkDataObject() {

        }

        public int compareTo(ChunkDataObject c)
        {
            return (chunkID - c.chunkID);
        }

        public int chunkID;
        byte[] chunkDataByteBuffer;
    }

    // Data Chunk Typesx
    public static final int CHUNK_DATA_TYPE_AUDIO = 0;
    public static final int CHUNK_DATA_TYPE_VIDEO = 1;

    // Chunking Params
    public static final int NUM_OF_CHUNKS_TO_BUFFER = 3;
    public static final int LATE_CHUNK_TOLERANCE_THRESHOLD = 7;
    public static final int MIN_AUDIO_BUFFER_COUNT_IN_QUEUE = 10;

    InetSocketAddress LIVE_STREAM_HOST_ADDRESS;

    final byte PACKET_TYPE_AUDIO = 0x00;
    final byte PACKET_TYPE_VIDEO = 0x01;
    final byte PACKET_TYPE_KEEP_ALIVE = 0x07;
    final byte PACKET_TYPE_NEW_LIVE_CHUNK_READY = 0x0B;
    final byte PACKET_TYPE_LIKE = 0x02;
    final byte PACKET_TYPE_COMMENT = 0x03;
    final byte PACKET_TYPE_BLOCK = 0x04;
    final byte PACKET_TYPE_STREAM_END = 0x05;
    final byte PACKET_TYPE_STREAM_INFO_CHANGE = 0x06;
    final byte PACKET_TYPE_KEEP_ALIVE_PUBLISHER = 0x08;
    final byte PACKET_TYPE_END_WATCH = 0x09;
    final byte PACKET_TYPE_PUBLISHER_DATA_ECHO_BACK = 0x0A;
    final byte PACKET_TYPE_GIFT = 0x0D;

    int liveStreamID = 0;
    int lastRequestedChunkID = 0;
    int lastParsedChunkID;
    long lastReceivedAudioDataTime;
    HashSet<Integer> retriedChunkIDSet = new HashSet<Integer>();

    Button switchButton;
    
    //leaderboard
    LinearLayout mLiveGiftLeaderboard;
    
    GLSurfaceView videoOutputView;
    VideoRenderer videoRenderer;

    Timer keepAliveTimer;

    boolean isUsingFrontCamera = false;

    PCMAudioPlayer pcmAudioPlayer;

    DatagramSocket datagramSocket;

    boolean isReceivingData = false;

    HandlerThread networkSendHandlerThread;
    Handler networkSendHandler;
    HandlerThread chunkParsingHandlerThread;
    Handler chunkParsingHandler;
    HandlerThread videoDecodeHandlerThread;
    Handler videoDecodeHandler;
    HandlerThread networkReceiveHandlerThread;
    Handler networkReceiveHandler;
    HandlerThread audioDecodeHandlerThread;
    Handler audioDecodeHandler;
    HandlerThread videoPlayerHandlerThread;
    Handler videoPlayHandler;
    HandlerThread audioPlayerHandlerThread;
    Handler audioPlayHandler;
    Handler handler;

    ArrayList<VideoFrameObject> queuedVideoFrameObjects = new ArrayList<VideoFrameObject>();
    ArrayList<ChunkDataObject> receivedChunkDataArray = new ArrayList<ChunkDataObject>();

    private String name="",caption="",picetur="",user="",myopen="";
    private ImageView mPicture;
    private TextView mName, mCaption;
    private DisplayImageOptions SelfOptions,PeopleOptions;
    private ImageView mLoadding;
    private AnimationDrawable mAnimaition;

    private ImageView mAllview;
    private LinearLayout mTopLayout;
    private ListView mListView;
    private ImageView mComment;
    private ImageView mLove;
    private Boolean mAllState = false;
    private ImageView mClose;
    private Gallery mGallery;
    private LinearLayout mPlayer;

    private Boolean mFirstView = true;

    private int mTimestamp = 0;
    private int startFullScreen, leaveFullScreen;
    private String mColorCode = "";

    private TextView mCount,mNow;

    private LiveStreamActivity mCtx = this;
    private ArrayList<UserModel> mUsers = new ArrayList<UserModel>();
    private PeopleAdapter mPeopleAdapter;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};

//    private int years[] = {R.drawable.money_1,R.drawable.money_2,R.drawable.money_3,R.drawable.money_4,R.drawable.money_5,R.drawable.money_6,R.drawable.money_7,R.drawable.money_8,R.drawable.money_9,R.drawable.money_10};
    private int xmass[] = {R.drawable.bear_2,R.drawable.bubble_9,R.drawable.cat_5,R.drawable.rabbit_7,R.drawable.xmas_1,R.drawable.logo_7};
//    private int xmass[] = {R.drawable.new_year_1,R.drawable.new_year_2,R.drawable.new_year_3,R.drawable.new_year_4,R.drawable.new_year_5,R.drawable.new_year_6,R.drawable.new_year_7,R.drawable.new_year_8};

    private int mColorPos = 0;

    private LayoutInflater inflater;
    private LinearLayout mEdit_layout;
    private MyEditText mEdit;
    private Button mSend;
    private Message mMessage;

    private int TenPos = 0;

    private View All;
    private ArrayList<LiveComment> mLiveComments = new ArrayList<LiveComment>();
    private CommentAdapter mCommentAdapter;

    private ImageView mRestream;
    private Boolean mReStreamState = false;

    private Story17Application mApplication;
    private int lag = 0;

    private String liveStreamVideoFrameCacheFolderPath = Singleton.getExternalPublicLiveStreamVideoFrameFolderPath();

    private int r = 0;
    private int g = 0;
    private int b = 0;

    private ParticleSystem ps;
    private UserModel mPlayerModel;

    private Dialog dialog;
    private Dialog UserDialog;
    private int nowLoadPeople = 0;
    private Dialog Enddialog;
    
    //leaderboard
    private Dialog LeaderboardLog;

    private Boolean editKeyState = false;

    private int mLikeCount = 0;
    private int lastCount = 0;
    private Boolean mGodHand = false;
    private int mCommentSec = 8;
    private Dialog mComSecDialog;
    private int[] mCommentColors = {R.color.live_comment_color2,R.color.live_comment_color3,R.color.live_comment_color4,R.color.live_comment_color5,R.color.live_comment_color6,R.color.live_comment_color7,R.color.live_comment_color1};

    private Boolean mGetCommentState = false;
    private int mCommentTime = 0;
//    private ArrayList<LiveComment> mLastComment = new ArrayList<LiveComment>();

    private String mUDP_IP = Constants.LIVE_STREAM_UDP_SERVER_IP;
    private int mLiveNowPeople = 0;

    private Timer mLoveTimer;
    private int lastFPS = 0;
    private int LagSix = 0;
    private Boolean mEnd = false;

    private InterstitialAd interstitialAd;
    private Boolean mAdReady = false;
    private int mAdShow = 0;

    private ParticleSystem particle;
    private ScaleModifier mScaleModifier;
    private ImageView mShowlove;
    private RelativeLayout mShare;

    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    private Boolean ShowDownTime = false;
    private TextView mDownTimeCdn;
    private long download_allTime_cdn = 0;
    private int pos_cdn = 0;
    private int faild_cdn = 0;

    private TextView mDownTimeOrigin;
    private long download_allTime_origin = 0;
    private int pos_origin = 0;
    private int faild_origin = 0;

    private ImageView mVerifie;
    private Dialog MyShareDialog;
    private Boolean mSharePicture = false;
    private ImageView mLiveReport;
    private Dialog mReportDialog;
    private ProgressBar mProgress;
    private ImageView mGift;
    private FrameLayout mGiftAni;

    private ArrayList<GiftModel> mGiftModels = new ArrayList<GiftModel>();
    private RelativeLayout mGiftLayout;
    private GridView mGiftList;
    private TextView mGiftCount;
    private GridAdapter mGridAdapter;
    private DisplayMetrics mDisplayMetrics;

    private ImageView mGiftImg;
    private int gift_loves[] = {R.drawable.giftpoint_1,R.drawable.giftpoint_2,R.drawable.giftpoint_3,R.drawable.giftpoint_4,R.drawable.giftpoint_5,R.drawable.giftpoint_6,R.drawable.giftpoint_7};
    private int gift_btn[] = {R.drawable.btn_gift_selector,R.drawable.btn_gift1_selector,R.drawable.btn_gift2_selector,R.drawable.btn_gift3_selector,R.drawable.btn_gift4_selector,R.drawable.btn_gift5_selector,R.drawable.btn_gift6_selector,R.drawable.btn_gift7_selector};

    private int love_pos = 0;
    IabHelper mHelper;
    private int mBuyPos = 0;
    private LinearLayout mBuyLayout;
    private Dialog mBuyDialog;
    private Boolean mDownloadState = true;
    private int mGiftToken = 0;
    private ArrayList<LiveGiftsModel> mLiveGiftsModels = new ArrayList<LiveGiftsModel>();
    private Boolean mGiftShowState = false;
    private Boolean mGiftLoad = false;
    private Boolean mChackCanGift = false;

    private LinearLayout mGiftComment;
    private ImageView mGiftIconPic;
    private TextView mGiftUserName;
    private TextView mGiftGiftName;
    private int gift_text_colors[] = {R.color.live_gift_color1,R.color.live_gift_color2,R.color.live_gift_color3,R.color.live_gift_color4,R.color.live_gift_color5,R.color.live_gift_color6,R.color.live_gift_color7};
    private Boolean mCanGiftState = true;

    private LinearLayout mBestGiftComment;
    private ImageView mBestGiftIconPicLeft;
    private TextView mBestGiftUserName;
    private TextView mBestGiftGiftName;
    private ImageView mBestGiftIconPicRight;

    private Dialog mGiftStateDialog;

    private LinearLayout mGiftCommentBest;
    private ImageView mGiftIconPicBest;
    private TextView mGiftUserNameBest;
    private TextView mGiftGiftNameBest;
    
    //leaderboard
    private ArrayList<GiftLeaderboardModel> mModelsLive = new ArrayList<GiftLeaderboardModel>();
    private ArrayList<LiveGiftsModel> mModels = new ArrayList<LiveGiftsModel>();
    private List<View> mLeaderBoardPage;
    private TextView mLiveTotalPoint;
    private TextView Leaderboard_streamer_id;
    private ImageView Leaderboard_viewer_pic;
    private int LiveLeaderboardtotal;

    private DisplayImageOptions GiftOptions;
    private MediaPlayer mMp3 = new MediaPlayer();
    private ImageView mLoadImg;
    private Boolean mLoadImgState = false;
    private TextView mLoadText;
    private ImageView mHideBtn;
    private RelativeLayout mHideLayout;
    private Dialog mCheckSendGiftDialog;

    private Boolean mGuest = false;
    private Button mBuyPointBtn;

    //for evnet tracking
    private String enterLiveFrom;
    private int commentCount =0;
    private int fullScreenCount=0;
    private int sendGiftPoint =0;
    private int buyPointCount =0;

    private CommentRetryThread mCommentRetryThread;

    private float percentage;
    private NumberFormat nt = NumberFormat.getPercentInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            setContentView(R.layout.live_stream);
        }
        catch (OutOfMemoryError o){
            Toast.makeText(mCtx, getString(R.string.live_watch_memory_error), Toast.LENGTH_SHORT).show();
            System.gc();
            if(mGuest)
            {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
            else mCtx.finish();
            return;
        }
        catch (Exception e){
            Toast.makeText(mCtx, getString(R.string.live_watch_memory_error), Toast.LENGTH_SHORT).show();
            System.gc();
            if(mGuest)
            {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
            else mCtx.finish();
            return;
        }

        regenerateLiveStreamVideoFrameCacheFolder();

        mApplication = (Story17Application) getApplication();

        //event tracking for how many times user bought giftpoint
        buyPointCount = Singleton.preferences.getInt(Constants.Buy_Point_Count, 0);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        liveStreamID = getIntent().getExtras().getInt("liveStreamID");

        try{
            enterLiveFrom = getIntent().getExtras().getString("enterLiveFrom");


        }catch (Exception x)
        {
            enterLiveFrom = "";
        }

        final Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("guest"))
            {
                mGuest = mBundle.getBoolean("guest");
            }
        }

        user = Singleton.preferences.getString(Constants.USER_ID, "");
        myopen = Singleton.preferences.getString(Constants.OPEN_ID, "");

        if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1) ShowDownTime = true;
        else ShowDownTime = false;

        // alloc native direct buffers
        AudioManager audioManager;
        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_NORMAL);
        audioManager.setSpeakerphoneOn(true);

        // setup views
        switchButton = (Button) findViewById(R.id.switchButton);
        videoOutputView = (GLSurfaceView) findViewById(R.id.videoOutputView);

        //leaderboard
        mLiveGiftLeaderboard = (LinearLayout) findViewById(R.id.live_leaderboard);

        mDisplayMetrics = new DisplayMetrics();
        mCtx.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        if(isTablet(mCtx))
        {
            int newH = (int)((float)mDisplayMetrics.widthPixels * (float) 1.7777f);
            int h =(int)((float)(newH - mDisplayMetrics.heightPixels)/(float)2);
            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
            mParams.bottomMargin = -h;
            mParams.topMargin = -h;
            videoOutputView.setLayoutParams(mParams);
        }

        try{
            // setup video renderer
            videoRenderer = new VideoRenderer();
            videoRenderer.setContext(mCtx);
            videoOutputView.setEGLContextClientVersion(2);
            videoOutputView.setRenderer(videoRenderer);
            videoOutputView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        }
        catch (OutOfMemoryError o){
            Toast.makeText(mCtx, getString(R.string.live_watch_memory_error), Toast.LENGTH_SHORT).show();
            System.gc();
            if(mGuest)
            {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
            else mCtx.finish();
            return;
        }
        catch (Exception e){
            Toast.makeText(mCtx, getString(R.string.live_watch_memory_error), Toast.LENGTH_SHORT).show();
            System.gc();
            if(mGuest)
            {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
            else mCtx.finish();
            return;
        }

        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        SelfOptions = new DisplayImageOptions.Builder()
        .displayer(new RoundedBitmapDisplayer(90))
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        mTopLayout = (LinearLayout) findViewById(R.id.top_layout);
        mName = (TextView) findViewById(R.id.name);
        //mCaption = (TextView) findViewById(R.id.caption);
        mPicture = (ImageView) findViewById(R.id.picture);
        mListView = (ListView) findViewById(R.id.list);
        mComment = (ImageView) findViewById(R.id.comment);
        mLove = (ImageView) findViewById(R.id.love);
        mClose = (ImageView) findViewById(R.id.close);

        mCount = (TextView) findViewById(R.id.count);

        mNow = (TextView) findViewById(R.id.now);
        mVerifie = (ImageView) findViewById(R.id.verifie);

        mLiveReport = (ImageView) findViewById(R.id.report);
        mShowlove = (ImageView) findViewById(R.id.showlove);
        mShare = (RelativeLayout) findViewById(R.id.share);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mGiftAni = (FrameLayout) findViewById(R.id.gift_ani);
        mGiftLayout = (RelativeLayout) findViewById(R.id.gift_layout);
        mGiftList = (GridView) findViewById(R.id.gift_list_layout);
        mGiftImg = (ImageView) findViewById(R.id.gift_img);
        mGiftCount = (TextView) findViewById(R.id.gift_count);
        mGiftCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));
        mGiftImg.setImageResource(gift_loves[(int) (Math.random() * gift_loves.length)]);

        mLoadImg = (ImageView) findViewById(R.id.load_img);
        mLoadText = (TextView) findViewById(R.id.loadding_text);
        mHideBtn = (ImageView) findViewById(R.id.hide_btn);
        mHideLayout = (RelativeLayout) findViewById(R.id.hide_layout);
        mBuyPointBtn = (Button) findViewById(R.id.buy_point_btn);

        mDownTimeCdn = (TextView) findViewById(R.id.down_time_cdn);
        mDownTimeOrigin = (TextView) findViewById(R.id.down_time_origin);
        if(ShowDownTime)
        {
            mDownTimeCdn.setVisibility(View.VISIBLE);
            mDownTimeOrigin.setVisibility(View.VISIBLE);
        }

        mGallery = (Gallery) findViewById(R.id.people);
        mGallery.setSpacing(10);
        mGallery.setUnselectedAlpha(255);
        mGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mGuest) {
                    showGuestLogin();

                    return;
                }

                try {
                    if (mUsers.size() > position) {
                        showUserDialog(position, 0, mUsers.get(position), null);
                    }
                } catch (Exception e) {

                }
            }
        });

        All = (View) findViewById(R.id.all);

        mBestGiftComment = (LinearLayout) findViewById(R.id.best_gift_comment);
        mBestGiftIconPicLeft = (ImageView) findViewById(R.id.best_gift_icon_pic_left);
        mBestGiftUserName = (TextView) findViewById(R.id.best_gift_user_name);
        mBestGiftGiftName = (TextView) findViewById(R.id.best_gift_gift_name);
        mBestGiftIconPicRight = (ImageView) findViewById(R.id.best_gift_icon_pic_right);

        mLoadding = (ImageView) findViewById(R.id.loadding);
        try
        {
            mLoadding.setBackgroundResource(R.drawable.live_loading);
            mAnimaition = (AnimationDrawable) mLoadding.getBackground();
            mAnimaition.start();
        }
        catch (OutOfMemoryError e)
        {
            System.gc();
            mLoadding.setImageResource(R.drawable.link_22);
        }
        mLoadText.setText(getString(R.string.live_loadding_first));

        RelativeSlideLayout mRelativeSlideLayout = (RelativeSlideLayout)findViewById(R.id.live_stream_RelativeSlideLayout);
        mRelativeSlideLayout.setSlideGestureListener(new RelativeSlideLayout.SlideGestureListener() {
            @Override
            public void onSwipeRight(View v, MotionEvent e1, MotionEvent e2) {
                try{
                    int[] location = new int[2];
                    mGallery.getLocationOnScreen(location);
                    if(e2.getRawY() >= (location[1]-50) && e2.getRawY() <= (location[1]+mGallery.getHeight()+50)){
                        return;
                    }
                    if (!mAllState) {
                        mAllview.performClick();
                    }
                }catch (Exception e){}
            }

            @Override
            public void onSwipeLeft(View v, MotionEvent e1, MotionEvent e2) {
                try{
                    int[] location = new int[2];
                    mGallery.getLocationOnScreen(location);
                    if(e2.getRawY() >= (location[1]-50) && e2.getRawY() <= (location[1]+mGallery.getHeight()+50)){
                        return;
                    }
                    if (mAllState) {
                        mAllview.performClick();
                    }
                }catch (Exception e){}
            }

            @Override
            public void onSwipeTop(View v, MotionEvent e1, MotionEvent e2) {
            }

            @Override
            public void onSwipeBottom(View v, MotionEvent e1, MotionEvent e2) {
            }

            @Override
            public void onDown(View v, MotionEvent e) {
            }
        });

        RelativeSlideLayout mListRelativeSlideLayout = (RelativeSlideLayout)findViewById(R.id.list_RelativeSlideLayout);
        mListRelativeSlideLayout.setSlideGestureListener(new RelativeSlideLayout.SlideGestureListener() {
            @Override
            public void onSwipeRight(View v, MotionEvent e1, MotionEvent e2) {}

            @Override
            public void onSwipeLeft(View v, MotionEvent e1, MotionEvent e2) {}

            @Override
            public void onSwipeTop(View v, MotionEvent e1, MotionEvent e2) {}

            @Override
            public void onSwipeBottom(View v, MotionEvent e1, MotionEvent e2) {}

            @Override
            public void onDown(View v, MotionEvent e) {
                if (mGuest) {
                    if (mApplication != null) {
//                        mApplication.showGuestLogin(mCtx,true);
                    }
                    return;
                }


                if (mAllState) {
                    return;
                }

                if (mEdit_layout.isShown()) {
                    mComment.setVisibility(View.VISIBLE);
                    mLove.setVisibility(View.VISIBLE);
                    mAllview.setVisibility(View.VISIBLE);
                    mRestream.setVisibility(View.VISIBLE);
                    mGift.setVisibility(View.VISIBLE);

                    mEdit_layout.setVisibility(View.INVISIBLE);
                    hideKeyboard();
                } else {
                    mLikeCount++;
//                    ApiManager.likeLiveStream(mCtx, user, liveStreamID, (int)lastAbsTime);
                    pressLove();
                }
            }
        });


        All.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mGuest) {
                    if (mApplication != null) {
//                        mApplication.showGuestLogin(mCtx,true);
                    }
                    return;
                }


                if (mAllState) {
                    return;
                }

                if (mEdit_layout.isShown()) {
                    mComment.setVisibility(View.VISIBLE);
                    mLove.setVisibility(View.VISIBLE);
                    mAllview.setVisibility(View.VISIBLE);
                    mRestream.setVisibility(View.VISIBLE);
                    mGift.setVisibility(View.VISIBLE);

                    mEdit_layout.setVisibility(View.INVISIBLE);
                    hideKeyboard();
                } else {
                    mLikeCount++;
//                    ApiManager.likeLiveStream(mCtx, user, liveStreamID, (int)lastAbsTime);
                    pressLove();
                }
            }
        });

        mAllview = (ImageView) findViewById(R.id.allview);
        mEdit_layout = (LinearLayout) findViewById(R.id.edit_layout);
        mEdit = (MyEditText) findViewById(R.id.edit);
        mEdit.mEditKeyBackListener = this;
        mSend = (Button) findViewById(R.id.send);
        mRestream = (ImageView) findViewById(R.id.restream);

        mGiftComment = (LinearLayout) findViewById(R.id.gift_comment);
        mGiftIconPic = (ImageView) findViewById(R.id.gift_icon_pic);
        mGiftUserName = (TextView) findViewById(R.id.gift_user_name);
        mGiftGiftName = (TextView) findViewById(R.id.gift_gift_name);

        mGiftCommentBest = (LinearLayout) findViewById(R.id.gift_comment_best);
        mGiftIconPicBest = (ImageView) findViewById(R.id.gift_icon_pic_best);
        mGiftUserNameBest = (TextView) findViewById(R.id.gift_user_name_best);
        mGiftGiftNameBest = (TextView) findViewById(R.id.gift_gift_name_best);

        mPlayer = (LinearLayout) findViewById(R.id.player);
        mGift = (ImageView) findViewById(R.id.gift);
        mGift.setImageResource(gift_btn[(int) (Math.random() * gift_btn.length)]);
        mBuyLayout = (LinearLayout) findViewById(R.id.buy_layout);
        mBuyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showGiftDialg();
            }
        });

        mBuyPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGiftDialg();
            }
        });

        mTopLayout.setVisibility(View.INVISIBLE);

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

        handler = new Handler(Looper.getMainLooper());

        networkSendHandlerThread = new HandlerThread("NETWORK_SEND");
        networkSendHandlerThread.start();
        networkSendHandler = new Handler(networkSendHandlerThread.getLooper());

        chunkParsingHandlerThread = new HandlerThread("CHUNK_PARSING");
        chunkParsingHandlerThread.start();
        chunkParsingHandler = new Handler(chunkParsingHandlerThread.getLooper());

        networkReceiveHandlerThread = new HandlerThread("NETWORK_RECEIVE");
        networkReceiveHandlerThread.start();
        networkReceiveHandler = new Handler(networkReceiveHandlerThread.getLooper());

        videoDecodeHandlerThread = new HandlerThread("VIDEO_DECODE");
        videoDecodeHandlerThread.start();
        videoDecodeHandler = new Handler(videoDecodeHandlerThread.getLooper());

        audioDecodeHandlerThread = new HandlerThread("AUDIO_DECODE");
        audioDecodeHandlerThread.start();
        audioDecodeHandler = new Handler(audioDecodeHandlerThread.getLooper());

        videoPlayerHandlerThread = new HandlerThread("VIDEO_PLAYER");
        videoPlayerHandlerThread.start();
        videoPlayHandler = new Handler(videoPlayerHandlerThread.getLooper());

        audioPlayerHandlerThread = new HandlerThread("AUDIO_PLAYER");
        audioPlayerHandlerThread.start();
        audioPlayHandler = new Handler(audioPlayerHandlerThread.getLooper());

        videoDecodeHandler.post(new Runnable() {
            @Override
            public void run() {
                VP8Codec.instance().setup(0);
            }
        });

        audioDecodeHandler.post(new Runnable() {
            @Override
            public void run() {
                OpusCodec.instance().setup(0);
            }
        });

        switchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (isUsingFrontCamera == false) {
                    isUsingFrontCamera = true;
                } else {
                    isUsingFrontCamera = false;
                }
            }
        });

        //leaderboard
        mLiveGiftLeaderboard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                LeaderboardLog = new Dialog(mCtx, R.style.LivePlayerDialog);
                LeaderboardLog.setContentView(R.layout.present_board_activity_live_viewer);

                mLiveTotalPoint =(TextView) LeaderboardLog.findViewById(R.id.total_gift_point);
                Leaderboard_viewer_pic = (ImageView) LeaderboardLog.findViewById(R.id.viewer_id);
                Leaderboard_streamer_id = (TextView) LeaderboardLog.findViewById(R.id.streamer_id);
                Leaderboard_streamer_id.setText(" " + name);

                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + Singleton.preferences.getString(Constants.PICTURE, "")), Leaderboard_viewer_pic, SelfOptions);

                final ListView mList = (ListView) LeaderboardLog.findViewById(R.id.list);
                final TextView nodata = (TextView) LeaderboardLog.findViewById(R.id.nodata);
                final ProgressBar progress = (ProgressBar) LeaderboardLog.findViewById(R.id.progress);

                progress.setVisibility(View.VISIBLE);

                ApiManager.getLiveStreamGiftLeaderboard(mCtx, user, liveStreamID, 0, 100, new ApiManager.GetLivestreamGiftLeaderboardCallback() {
                    @Override
                    public void onResult(boolean success, GiftLivestreamLeaderBoardModel liveGiftLeaderboard) {

                        progress.setVisibility(View.GONE);
                        if (success && liveGiftLeaderboard != null) {
                            LiveLeaderboardtotal = liveGiftLeaderboard.getTotalPoint();

                            if(liveGiftLeaderboard.getTotalPoint() !=0) {
                                //得知自己送的禮物，佔直播主收到禮物總點數的％
                                mLiveTotalPoint.setText(String.valueOf(nt.format(GetSentGiftPercentage( liveGiftLeaderboard.getMyPoint()))));
                                nodata.setVisibility(View.GONE);
                                mModelsLive.clear();
                                mModelsLive.addAll(liveGiftLeaderboard.getGiftLeaderboardInfo());
                                mList.setAdapter(mLeaderAdapter);
                            }
                            else {
                                mLiveTotalPoint.setText("0 %");
                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            mLiveTotalPoint.setText("0 %");
                            nodata.setVisibility(View.VISIBLE);
                        }
                    }
                });

                LeaderboardLog.show();
            }
        });


        mAllview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mGiftLayout.isShown()) {
                    mGiftLayout.setVisibility(View.GONE);
                    return;
                }

                if (!mAllState) {
                    //event tracking
                    fullScreenCount++;
                    startFullScreen = Singleton.getCurrentTimestamp();
                    try {
                        LogEventUtil.EnterFullScreen(mCtx, mApplication);
                    } catch (Exception x) {

                    }

                    mAllState = true;
                    mAllview.setImageResource(R.drawable.btn_live_fullscreen2_selector);
                    mTopLayout.setVisibility(View.GONE);
                    mListView.setVisibility(View.GONE);
                    mComment.setVisibility(View.INVISIBLE);
                    mLove.setVisibility(View.GONE);
                    mGallery.setVisibility(View.GONE);
                    mRestream.setVisibility(View.INVISIBLE);
                    mGift.setVisibility(View.INVISIBLE);
                    mHideBtn.setVisibility(View.INVISIBLE);
                    mLiveReport.setVisibility(View.GONE);
                    mHideLayout.setVisibility(View.GONE);
                } else {
                    //event tracking

                    leaveFullScreen = Singleton.getCurrentTimestamp();
                    try {
                        LogEventUtil.LeaveFullScreen(mCtx, mApplication, leaveFullScreen - startFullScreen);
                    } catch (Exception x) {

                    }

                    mAllState = false;
                    mAllview.setImageResource(R.drawable.btn_live_fullscreen_selector);
                    mTopLayout.setVisibility(View.VISIBLE);
                    mListView.setVisibility(View.VISIBLE);
                    mComment.setVisibility(View.VISIBLE);
                    mLove.setVisibility(View.VISIBLE);
                    mGallery.setVisibility(View.VISIBLE);
                    mRestream.setVisibility(View.VISIBLE);
                    mGift.setVisibility(View.VISIBLE);
                    mHideBtn.setVisibility(View.GONE);
                    mLiveReport.setVisibility(View.VISIBLE);
                    mHideLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        ApiManager.getLiveStreamInfo(mCtx, liveStreamID, new ApiManager.LiveStreamCallback() {
            @Override
            public void onResult(boolean success, LiveStreamModel liveStreamModel) {
                if (success && liveStreamModel != null) {
                    name = liveStreamModel.user.getOpenID();
                    caption = liveStreamModel.caption;
                    picetur = liveStreamModel.user.getPicture();

                    mName.setText(name);
                    //mCaption.setText(caption);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + picetur), mPicture, SelfOptions);

                    Picasso.with(mCtx).load(Singleton.getS3FileUrl(picetur)).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE).into(mTarget);

                    if (!mChackCanGift) {
                        if (liveStreamModel.canSendGift == 0) {
                            mCanGiftState = false;
                        }
                        mChackCanGift = true;
                    }

                    getPubishUserInfo();

                    new DownloadTask().execute(Singleton.getS3FileUrl(picetur));

                }
            }
        });

        ApiManager.enterLiveStream(mCtx, user, liveStreamID, new ApiManager.EnterLiveStreamCallback() {
            @Override
            public void onResult(boolean success, String message, int timestamp, String colorCode, int numberOfChunks, String ip) {
                if (success) {
                    mTimestamp = timestamp;
                    mColorCode = colorCode;

                    if (ip != null && ip.length() != 0) {
                        mUDP_IP = ip;
                    }

                    if (mColorCode.length() == 0) mColorCode = "#FFFFFF";

                    try {
                        mColorPos = (Integer.valueOf(mColorCode.substring(1, mColorCode.length()), 16).intValue() % 28);
                    } catch (Exception e) {
                        mColorPos = (int) (Math.random() * loves.length);
                    }

                    r = Integer.valueOf(mColorCode.substring(1, 3), 16);
                    g = Integer.valueOf(mColorCode.substring(3, 5), 16);
                    b = Integer.valueOf(mColorCode.substring(5, 7), 16);

                    //event tracking
                    try {
                        LogEventUtil.EnterLivePage(mCtx, mApplication, name, Integer.toString(liveStreamID), enterLiveFrom, caption);
                    } catch (Exception x) {

                    }

                    ApiManager.getLiveStreamInfo(mCtx, user, liveStreamID, new ApiManager.GetLiveStreamInfoCallback() {
                        @Override
                        public void onResult(boolean success, LiveModel mLiveModel) {
                            if (success) {
                                mCount.setText(mLiveModel.getViewerCount() + getString(R.string.live_people));
                                mNow.setText(String.format(getString(R.string.live_now), mLiveModel.getLiveViewerCount()));
                                mLiveNowPeople = mLiveModel.getLiveViewerCount();
                                mTopLayout.setVisibility(View.VISIBLE);

                                if (!mChackCanGift) {
                                    if (mLiveModel.getCanSendGift() == 0) {
                                        mCanGiftState = false;
                                    }
                                    mChackCanGift = true;
                                }

                                int fps = getLoveFPS(mLiveNowPeople);
                                if (mLoveTimer != null) {
                                    mLoveTimer.cancel();
                                    mLoveTimer.purge();
                                    mLoveTimer = null;
                                }
                                mLoveTimer = new Timer();
                                mLoveTimer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        showLove();
                                    }
                                }, 0, fps);

                                lastFPS = fps;
                            }
                        }
                    });

                    ApiManager.getLiveStreamViewers(mCtx, user, liveStreamID, 0, 100, new ApiManager.GetLiveStreamViewersCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<UserModel> model) {
                            if (success && model != null) {
                                if (model.size() != 0) {
                                    mUsers.clear();
                                    mUsers.addAll(model);

                                    try {
                                        if (mPeopleAdapter == null) {
                                            mPeopleAdapter = new PeopleAdapter();
                                            mGallery.setAdapter(mPeopleAdapter);
                                        } else mPeopleAdapter.notifyDataSetChanged();
                                        mGallery.setSelection(mUsers.size() / 2);
                                    } catch (Exception e) {

                                    }
                                }
                            }
                        }
                    });

                    ApiManager.getLiveStreamInfo(mCtx, liveStreamID, new ApiManager.LiveStreamCallback() {
                        @Override
                        public void onResult(boolean success, LiveStreamModel liveStreamModel) {
                            if (success && liveStreamModel != null) {
                                // setup datagram socket
                                if (networkSendHandler == null) {
                                    return;
                                }

                                networkSendHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        LIVE_STREAM_HOST_ADDRESS = new InetSocketAddress(mUDP_IP, Constants.LIVE_STREAM_UDP_SERVER_PORT);

                                        try {
                                            datagramSocket = new DatagramSocket(0);
                                        } catch (Exception e) {
                                            Singleton.log("UDP SOCKET ESTABLISH FAILED!");
                                        }

                                        Singleton.log("UDP SOCKET ESTABLISHED!");

                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                startReceivingData();

                                                sendKeepAliveData();
                                            }
                                        });
                                    }
                                });
                            } else {
                                finish();
                            }
                        }
                    });

                    switchButton.setVisibility(View.GONE);

                    audioPlayHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            pcmAudioPlayer = new PCMAudioPlayer();
                            pcmAudioPlayer.delegate = LiveStreamActivity.this;
                            pcmAudioPlayer.startPlaying();
                        }
                    });

                    videoOutputView.onResume();

                    keepAliveTimer = new Timer();
                    keepAliveTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            sendKeepAliveData();
                        }
                    }, 0, 1000L);
                } else {
                    isReceivingData = false;

                    if (mLoveTimer != null) {
                        mLoveTimer.cancel();
                        mLoveTimer.purge();
                        mLoveTimer = null;
                    }

                    try {
                        if (mCtx == null) return;

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    mLoadding.setVisibility(View.GONE);
                                    mLoadImg.setVisibility(View.GONE);
                                    mLoadText.setVisibility(View.GONE);
                                } catch (Exception e) {
                                }
                            }
                        });
                        if (Enddialog == null) {
                            Enddialog = new Dialog(mCtx, R.style.LivePlayerDialog);
                            Enddialog.setContentView(R.layout.live_end_dailog);
                            Window window = Enddialog.getWindow();
                            window.setGravity(Gravity.BOTTOM);
                            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            Enddialog.setCancelable(false);

                            TextView mTitle = (TextView) Enddialog.findViewById(R.id.title);
                            if (message.compareTo("blocked") == 0)
                                mTitle.setText(getString(R.string.live_stream_blocked));
                            else if (message.compareTo("ended") == 0)
                                mTitle.setText(getString(R.string.liveend));
                            else mTitle.setText(getString(R.string.live_broadcast_error));

                            Button mEnd = (Button) Enddialog.findViewById(R.id.end);
                            mEnd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    hideKeyboard();
//                                    mApplication.setReLoad(true);
                                    mApplication.isUserLive = false;
                                    if (Singleton.preferences.getInt(Constants.SHOW_AD, 0) == 1 && !mFirstView && mAdReady && interstitialAd != null) {
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                interstitialAd.show();
                                                mAdReady = false;
                                            }
                                        });
                                    } else {
                                        if (mGuest) {
//                                            Intent intent = new Intent();
//                                            intent.setClass(mCtx, GuestActivity.class);
//                                            startActivity(intent);
                                            mCtx.finish();
                                        } else mCtx.finish();
                                    }
                                }
                            });
                            Enddialog.show();
                        } else Enddialog.show();
                    } catch (Exception e) {

                    }
                }
            }
        });

        mClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                isReceivingData = false;
                mEnd = true;
                if(mLoveTimer!=null)
                {
                    mLoveTimer.cancel();
                    mLoveTimer.purge();
                    mLoveTimer = null;
                }

                if(!mGodHand && !mApplication.getIsSbtools() && mLikeCount!=0)
                {
                    networkSendHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            ApiManager.likeLivestreamBatchUpdate(mCtx, user, liveStreamID, mLikeCount);
                            mLikeCount = 0;
                        }
                    });
                }

                int quit = Singleton.getCurrentTimestamp() - mTimestamp;
                ApiManager.quitViewLiveStream(mCtx, user, liveStreamID, quit, new ApiManager.QuitViewLiveStreamCallback() {
                    @Override
                    public void onResult(boolean success, String message) {
                        hideKeyboard();
//                        mApplication.setReLoad(true);
                        if (Singleton.preferences.getInt(Constants.SHOW_AD, 0) == 1 && !mFirstView && mAdReady && interstitialAd != null) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    interstitialAd.show();
                                    mAdReady = false;
                                }
                            });
                        } else {
                            if (mGuest) {
//                                Intent intent = new Intent();
//                                intent.setClass(mCtx, GuestActivity.class);
//                                startActivity(intent);
                                mCtx.finish();
                            } else mCtx.finish();
                        }
                    }
                });
            }
        });

        mLove.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                try{
                    LogEventUtil.LikeLive(mCtx,mApplication,name,Integer.toString(liveStreamID));
                }catch (Exception x)
                {

                }

                mLikeCount++;
//                ApiManager.likeLiveStream(mCtx, user, liveStreamID, (int)lastAbsTime);

                pressLove();
            }
        });

        mComment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mGiftLayout.isShown())
                {
                    mGiftLayout.setVisibility(View.GONE);
                    return;
                }

                if(mEdit_layout.isShown())
                {
                    mComment.setVisibility(View.VISIBLE);
                    mLove.setVisibility(View.VISIBLE);
                    mAllview.setVisibility(View.VISIBLE);
                    mRestream.setVisibility(View.VISIBLE);
                    mGift.setVisibility(View.VISIBLE);

                    mEdit_layout.setVisibility(View.INVISIBLE);
                    hideKeyboard();
                }
                else
                {
                    if(mCommentSec < 8)
                    {
                        if(mComSecDialog!=null) mComSecDialog = null;
                        mComSecDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                        mComSecDialog.setContentView(R.layout.live_end_dailog);
                        Window window = mComSecDialog.getWindow();
                        window.setGravity(Gravity.BOTTOM);
                        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        mComSecDialog.setCancelable(false);

                        TextView mTitle = (TextView) mComSecDialog.findViewById(R.id.title);
                        mTitle.setText(String.format(getString(R.string.live_comment_sec), 8-mCommentSec));

                        Button mEnd = (Button)mComSecDialog.findViewById(R.id.end);
                        mEnd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mComSecDialog.dismiss();
                            }
                        });
                        mComSecDialog.show();
                    }
                    else
                    {
                        mComment.setVisibility(View.INVISIBLE);
                        mLove.setVisibility(View.INVISIBLE);
                        mAllview.setVisibility(View.INVISIBLE);
                        mRestream.setVisibility(View.INVISIBLE);
                        mGift.setVisibility(View.INVISIBLE);

                        mEdit.setText("");
                        mEdit_layout.setVisibility(View.VISIBLE);
                        mEdit.setEnabled(true);
                        mEdit.setFocusable(true);
                        mListView.clearFocus();
                        showKeyboard();
                    }
                }
            }
        });


        mGift.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

//                Dialog mBuyDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
//                mBuyDialog.setContentView(R.layout.buy_dialog);
//                mBuyDialog.show();

                //Singleton.preferenceEditor.putInt(Constants.GIFT_MODULE_STATE, 1).commit();

                if(Singleton.preferences.getInt(Constants.GIFT_MODULE_STATE, 1)==0)
                {
                    if(mGiftStateDialog!=null) mGiftStateDialog = null;
                    mGiftStateDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                    mGiftStateDialog.setContentView(R.layout.live_end_dailog);
                    Window window = mGiftStateDialog.getWindow();
                    window.setGravity(Gravity.BOTTOM);
                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                    TextView mTitle = (TextView) mGiftStateDialog.findViewById(R.id.title);
                    mTitle.setText(getString(R.string.gift_module_state));

                    Button mEnd = (Button)mGiftStateDialog.findViewById(R.id.end);
                    mEnd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mGiftStateDialog.dismiss();
                        }
                    });
                    mGiftStateDialog.show();

                    return;
                }
                else if(Singleton.preferences.getInt(Constants.GIFT_MODULE_STATE, 1)==2)
                {
                    if(mGiftStateDialog!=null) mGiftStateDialog = null;
                    mGiftStateDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                    mGiftStateDialog.setContentView(R.layout.live_end_dailog);
                    Window window = mGiftStateDialog.getWindow();
                    window.setGravity(Gravity.BOTTOM);
                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                    TextView mTitle = (TextView) mGiftStateDialog.findViewById(R.id.title);
                    mTitle.setText(getString(R.string.gift_module_update));

                    Button mEnd = (Button)mGiftStateDialog.findViewById(R.id.end);
                    mEnd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mGiftStateDialog.dismiss();

                            try
                            {
                                Uri uri = Uri.parse(Singleton.preferences.getString(Constants.APP_UPDATE_LINK, Constants.MEDIA17_WEBSITE));
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    });
                    mGiftStateDialog.show();

                    return;
                }

                if(!mCanGiftState)
                {
                    if(mGiftStateDialog!=null) mGiftStateDialog = null;
                    mGiftStateDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                    mGiftStateDialog.setContentView(R.layout.live_end_dailog);
                    Window window = mGiftStateDialog.getWindow();
                    window.setGravity(Gravity.BOTTOM);
                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                    TextView mTitle = (TextView) mGiftStateDialog.findViewById(R.id.title);
                    mTitle.setText(getString(R.string.gift_publish_update));

                    Button mEnd = (Button)mGiftStateDialog.findViewById(R.id.end);
                    mEnd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mGiftStateDialog.dismiss();
                        }
                    });
                    mGiftStateDialog.show();

                    return;
                }

                if(mGiftLayout.isShown())
                {
                    mGiftLayout.setVisibility(View.GONE);
                }
                else
                {
                    if(mGiftModels.size()==0)
                    {
                        mProgress.setVisibility(View.VISIBLE);
                        ApiManager.getGiftList(mCtx, new ApiManager.GetGiftListCallback()
                        {
                            @Override
                            public void onResult(boolean success, ArrayList<GiftModel> giftModel)
                            {
                                mProgress.setVisibility(View.GONE);
                                if(success && giftModel!=null)
                                {
                                    mGiftModels.addAll(giftModel);

                                    if(mGridAdapter!=null) mGridAdapter = null;
                                    mGridAdapter = new GridAdapter();
                                    mGiftList.setAdapter(mGridAdapter);
                                    mGiftLayout.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    mGiftLayout.setVisibility(View.GONE);
                                    try {
//                          showToast(getString(R.string.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mGridAdapter!=null)
                        {
//                            mGiftList.setSelection(0);
                            mGridAdapter.notifyDataSetChanged();
                        }
                        else
                        {
                            mGridAdapter = new GridAdapter();
                            mGiftList.setAdapter(mGridAdapter);
                        }
                        mGiftLayout.setVisibility(View.VISIBLE);
                    }
                }

//                ApiManager.getGiftList(mCtx, new ApiManager.GetGiftListCallback()
//                {
//                    @Override
//                    public void onResult(boolean success, ArrayList<GiftModel> giftModel)
//                    {
//                        if(giftModel!=null)
//                        {
//                            Log.d("123", "giftModel : " + giftModel.size());
//                            mGiftModels.addAll(giftModel);
////                            new DownloadFile().execute(Constants.NO_CHT_ALL_URL + giftModel.get(0).getZipFileName(),giftModel.get(0).getZipFileName());
//
//                            if(mGridAdapter!=null) mGridAdapter = null;
//                            mGridAdapter = new GridAdapter();
//                            mGiftList.setAdapter(mGridAdapter);
//                            mGiftLayout.setVisibility(View.VISIBLE);
//                        }
//                    }
//                });

//                try
//                {
//                    File file = new File(Singleton.getExternalMediaFolderPath() + "5000_C.zip");
//                    ZipUtils.upZipFile(file, Environment.getExternalStorageDirectory().getAbsolutePath() + "/." + Singleton.applicationContext.getPackageName());
//
//                    DisplayMetrics mDisplayMetrics = new DisplayMetrics();
//                    mCtx.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
//                    mGiftAni.removeAllViews();
//                    GiftView mGiftView = new GiftView(mCtx, 50, mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
//                    mGiftAni.addView(mGiftView,0);
//                }
//                catch (FileNotFoundException e)
//                {
//                }
//                catch (IOException e)
//                {
//                }
            }
        });

        mSend.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mEdit.getText().length()!=0)
                {
                    mCommentSec = 0;
                    final String commentMessage = mEdit.getText().toString();

                    //event tracking
                    commentCount++;
                    try{
                        LogEventUtil.CommentInLiveStream(mCtx,mApplication,user,Integer.toString(liveStreamID), commentMessage);
                    }
                    catch (Exception x)
                    {

                    }


                    if(mCommentRetryThread != null){
                        //to stop comment retrying
                        mCommentRetryThread.destroyProcess();
                        mCommentRetryThread = null;
                    }

                    ApiManager.commentLiveStream(mCtx, user, liveStreamID, commentMessage, mColorCode, (int) lastAbsTime, new ApiManager.CommentLiveStreamCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (!success) {
                                mCommentRetryThread = new CommentRetryThread(commentMessage, mColorCode, lastAbsTime);
                                mCommentRetryThread.start();
                            }
                        }
                    });

                    mEdit.setText("");

                    mComment.setVisibility(View.VISIBLE);
                    mLove.setVisibility(View.VISIBLE);
                    mAllview.setVisibility(View.VISIBLE);
                    mRestream.setVisibility(View.VISIBLE);
                    mGift.setVisibility(View.VISIBLE);

                    mEdit_layout.setVisibility(View.INVISIBLE);
                    hideKeyboard();
                }
            }
        });

        mPlayer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mPlayerModel!=null && dialog!=null)
                {
                    if(dialog.isShowing()) dialog.dismiss();
                    else dialog.show();
                }
            }
        });

        mShare.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

//                try
//                {
//                    URL url = new URL("http://17app.co/");
//                    TweetComposer.Builder builder = new TweetComposer.Builder(mCtx)
//                    .url(url)
//                    .text(myopen + getString(R.string.live_relive) + name + getString(R.string.live_relive2))
//                    .image(Uri.parse(Singleton.getS3FileUrl(picetur)));
//                    builder.show();
//                }
//                catch (MalformedURLException e)
//                {
//
//                }
                FacebookSdk.sdkInitialize(getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(mCtx);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>()
                {
                    @Override
                    public void onSuccess(Sharer.Result result)
                    {
                    }

                    @Override
                    public void onCancel()
                    {
                    }

                    @Override
                    public void onError(FacebookException error)
                    {
                    }
                });

                if(ShareDialog.canShow(ShareLinkContent.class))
                {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("17 App Live stream")
                    .setContentDescription(myopen + getString(R.string.live_relive) + String.format(getString(R.string.live_relive2), name))
                    .setContentUrl(Uri.parse("http://17app.co/"))
                    .setImageUrl(Uri.parse(Singleton.getS3FileUrl(picetur)))
                    .build();

                    shareDialog.show(linkContent);
                }
            }
        });

        ApiManager.hasRestreamedLiveStream(mCtx, user, liveStreamID, new ApiManager.HasRestreamedLiveStreamCallback()
        {
            @Override
            public void onResult(boolean success, String message, int has)
            {
                if (success)
                {
                    if (has == 0)
                    {
                        mReStreamState = true;
                    }
                }
            }
        });

        mRestream.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mPlayerModel==null)
                {
                    if(name.length()!=0)
                    {
                        getPubishUserInfo();
                    }
                    else
                    {
                        ApiManager.getLiveStreamInfo(mCtx, liveStreamID, new ApiManager.LiveStreamCallback()
                        {
                            @Override
                            public void onResult(boolean success, LiveStreamModel liveStreamModel)
                            {
                                if (success && liveStreamModel != null)
                                {
                                    name = liveStreamModel.user.getOpenID();
                                    caption = liveStreamModel.caption;
                                    picetur = liveStreamModel.user.getPicture();

                                    mName.setText(name);
                                   // mCaption.setText(caption);
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + picetur), mPicture, SelfOptions);

                                    if(!mChackCanGift){
                                        if(liveStreamModel.canSendGift==0)
                                        {
                                            mCanGiftState = false;
                                        }
                                        mChackCanGift = true;
                                    }

                                    getPubishUserInfo();

                                    if(!mSharePicture)
                                    {
                                        if(mDownloadState) new DownloadTask().execute(Singleton.getS3FileUrl(picetur));
                                    }
                                }
                            }
                        });
                    }
                    return;
                }

                if(!mSharePicture)
                {
                    if(mDownloadState) new DownloadTask().execute(Singleton.getS3FileUrl(picetur));
                    return;
                }

                if(Constants.INTERNATIONAL_VERSION)
                {
                    shareDailogInternational();
                }
                else
                {
                    shareDailogChina();
                }
            }
        });

        mLiveReport.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mReportDialog!=null) mReportDialog = null;
                mReportDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                mReportDialog.setContentView(R.layout.live_report_dailog);
                Window window = mReportDialog.getWindow();
                window.setGravity(Gravity.BOTTOM);
                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                Button mCancel = (Button) mReportDialog.findViewById(R.id.cancel);
                mCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mReportDialog.dismiss();
                    }
                });

                Button mReport1 = (Button) mReportDialog.findViewById(R.id.report_text1);
                mReport1.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mReportDialog.dismiss();
                        if(mPlayerModel!=null)
                        {
                            try {
                                LogEventUtil.ReportLive(mApplication, mApplication);
                            }
                            catch (Exception x)
                            {

                            }
                            ApiManager.reportLiveAction(mCtx, mPlayerModel.getUserID(), liveStreamID, getString(R.string.live_report_message1), new ApiManager.RequestCallback()
                            {
                                @Override
                                public void onResult(boolean success)
                                {
                                    if (success)
                                    {
                                        try {
//                          showToast(getString(R.string.done));
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                    else
                                    {
                                        try {
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            try {
//                          showToast(getString(R.string.error_failed));
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                    }
                });

                Button mReport2 = (Button) mReportDialog.findViewById(R.id.report_text2);
                mReport2.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mReportDialog.dismiss();
                        if(mPlayerModel!=null)
                        {
                            try {
                                LogEventUtil.ReportLive(mApplication, mApplication);
                            }
                            catch (Exception x)
                            {

                            }
                            ApiManager.reportLiveAction(mCtx, mPlayerModel.getUserID(), liveStreamID, getString(R.string.live_report_message2), new ApiManager.RequestCallback()
                            {
                                @Override
                                public void onResult(boolean success)
                                {
                                    if (success)
                                    {
                                        try {
//                          showToast(getString(R.string.done));
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                    else {
                                        try {
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            try {
//                          showToast(getString(R.string.error_failed));
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                    }
                });

                Button mReport3 = (Button) mReportDialog.findViewById(R.id.report_text3);
                mReport3.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mReportDialog.dismiss();
                        if(mPlayerModel!=null)
                        {
                            try {
                                LogEventUtil.ReportLive(mApplication, mApplication);
                            }
                            catch (Exception x)
                            {

                            }
                            ApiManager.reportLiveAction(mCtx, mPlayerModel.getUserID(), liveStreamID, getString(R.string.live_report_message3), new ApiManager.RequestCallback()
                            {
                                @Override
                                public void onResult(boolean success)
                                {
                                    if (success)
                                    {
                                        try {
//                          showToast(getString(R.string.done));
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                    else {
                                        try {
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            try {
//                          showToast(getString(R.string.error_failed));
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                    }
                });

                Button mReport4 = (Button) mReportDialog.findViewById(R.id.report_text4);
                mReport4.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mReportDialog.dismiss();
                        if(mPlayerModel!=null)
                        {
                            try {
                                LogEventUtil.ReportLive(mApplication, mApplication);
                            }
                            catch (Exception x)
                            {

                            }
                            ApiManager.reportLiveAction(mCtx, mPlayerModel.getUserID(), liveStreamID, getString(R.string.live_report_message4), new ApiManager.RequestCallback()
                            {
                                @Override
                                public void onResult(boolean success)
                                {
                                    if (success)
                                    {
                                        try {
//                          showToast(getString(R.string.done));
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                    else {
                                        try {
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            try {
//                          showToast(getString(R.string.error_failed));
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                    }
                });

                mReportDialog.show();
            }
        });

        PeopleOptions = new DisplayImageOptions.Builder()
        .displayer(new RoundedBitmapDisplayer(90))
        .showImageOnLoading(R.drawable.placehold_c)
        .showImageForEmptyUri(R.drawable.placehold_c)
        .showImageOnFail(R.drawable.placehold_c)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        GiftOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.gift_placehold)
        .showImageForEmptyUri(R.drawable.gift_placehold)
        .showImageOnFail(R.drawable.gift_placehold)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        MyPhoneStateListener myPhoneStateListener = new MyPhoneStateListener();
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

//        if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1) loadInterstitialAd();

        if(Constants.INTERNATIONAL_VERSION)
        {
        try
        {
            mHelper = new IabHelper(mCtx, Constants.GiftPublicKey);
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener()
            {
                public void onIabSetupFinished(IabResult result)
                {
                    if(result.isSuccess())
                    {
                        if(mApplication.getProductModels().size()!=0)
                        {
                            ArrayList<String> skuList = new ArrayList<String>();
                            for(int i = 0 ; i < mApplication.getProductModels().size() ; i++)
                            {
                                skuList.add(mApplication.getProductModels().get(i).getProductID());
                            }

                            mHelper.queryInventoryAsync(true, skuList, new IabHelper.QueryInventoryFinishedListener()
                            {
                                public void onQueryInventoryFinished(IabResult result, Inventory inventory)
                                {

                                    try{
                                        if (!result.isFailure()) {
                                            if(inventory != null){
                                                for (int i = 0; i < mApplication.getProductModels().size(); i++) {
//                                        Log.d("123","data : " + inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).toString());
                                                    mApplication.getProductModels().get(i).setGoogle_price(inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).getPrice());
                                                    mApplication.getProductModels().get(i).setGoogle_point(inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).getDescription());
                                                }
                                            }
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                        else
                        {
                            ApiManager.getPointProductList(mCtx, new ApiManager.ProductListCallback()
                            {
                                @Override
                                public void onResult(boolean success, ArrayList<ProductModel> productModels)
                                {
                                    if(success && productModels!=null)
                                    {
                                        if(productModels.size()!=0)
                                        {
                                            mApplication.setProductModels(productModels);

                                            ArrayList<String> skuList = new ArrayList<String>();
                                            for(int i = 0 ; i < productModels.size() ; i++)
                                            {
                                                if(productModels.get(i)!=null){
                                                    if(productModels.get(i).getProductID()!=null){
                                                        skuList.add(productModels.get(i).getProductID());
                                                    }
                                                }
                                            }

                                            if(mHelper!=null && skuList!=null && skuList.size()!=0){
                                                mHelper.queryInventoryAsync(true, skuList, new IabHelper.QueryInventoryFinishedListener()
                                                {
                                                    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                                                        try {
                                                            if (!result.isFailure()) {
                                                                if (inventory != null) {
                                                                    for (int i = 0; i < mApplication.getProductModels().size(); i++) {
                                                                        mApplication.getProductModels().get(i).setGoogle_price(inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).getPrice());
                                                                        mApplication.getProductModels().get(i).setGoogle_point(inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).getDescription());
                                                                    }
                                                                }
                                                            }
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }
        catch (Exception e)
        {

        }
        }
        else
        {
            IAppPay.init(mCtx, IAppPay.PORTRAIT, IAppPaySDKConfig.APP_ID);
        }
    }

    private void pressLove(){
        if (mLiveNowPeople < 11 && !mGodHand && !mApplication.getIsSbtools()) {
            final ByteBuffer keepAliveByteBuffer = ByteBuffer.allocate(17);
            keepAliveByteBuffer.order(ByteOrder.LITTLE_ENDIAN);

            keepAliveByteBuffer.put(PACKET_TYPE_LIKE);
            keepAliveByteBuffer.putInt(liveStreamID);
            keepAliveByteBuffer.putInt(r);
            keepAliveByteBuffer.putInt(g);
            keepAliveByteBuffer.putInt(b);

            // send encoded video data
            if (networkSendHandler == null) {
                return;
            }

            networkSendHandler.post(new Runnable() {
                public void run() {
                    try {
                        datagramSocket.send(new DatagramPacket(keepAliveByteBuffer.array(), keepAliveByteBuffer.position(), LIVE_STREAM_HOST_ADDRESS));
                    } catch (Exception e) {

                    }
                }
            });
        } else {
            if (!mAllState) {

                if (particle != null) particle = null;
                if (mScaleModifier != null) mScaleModifier = null;

                try {
                    mScaleModifier = new ScaleModifier((float) 0.8 + (float) (Math.random() * 0.2), (float) 1.0 + (float) (Math.random() * 0.2), 0, 1500);
                    particle = new ParticleSystem(mCtx, 1, loves[(int) (Math.random() * loves.length)], 3000);//mColorPos
                    particle.setSpeedByComponentsRange(-0.02f, 0.02f, -0.2f, -0.1f);
                    particle.setAcceleration(0.000003f, 90);
                    particle.setInitialRotationRange(-20, 20);
                    particle.setRotationSpeed(-20 + ((int) (Math.random() * 40)));
                    particle.setFadeOut(2000);
                    particle.addModifier(mScaleModifier);
                    particle.oneShot(mShowlove, 10);
                } catch (OutOfMemoryError o) {
                } catch (Exception e) {
                }

            }
        }
    }

    private void shareDailogInternational()
    {
        if(MyShareDialog!=null) MyShareDialog = null;
        MyShareDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        MyShareDialog.setContentView(R.layout.live_share_dailog_international);
        Window window = MyShareDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        MyShareDialog.setCancelable(true);

        final String mTitle = String.format(getString(R.string.live_relive2), mPlayerModel.getOpenID());
        final String mDescription = mTitle + "!" + caption;

        ImageView m17 = (ImageView) MyShareDialog.findViewById(R.id.layout_17);
        if(!mReStreamState) m17.setImageResource(R.drawable.share_17off);
        m17.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(!mReStreamState) {
                    Toast.makeText(mCtx,getString(R.string.shared),Toast.LENGTH_SHORT).show();
                    return;
                }

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"media17");
                }catch (Exception x)
                {

                }

                //Umeng monitor
                String Umeng_id="Share_Live_17";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                ApiManager.restreamLiveStream(mCtx, user, liveStreamID, myopen, name, new ApiManager.RestreamLiveStreamCallback()
                {
                    @Override
                    public void onResult(boolean success, String message)
                    {
                        if (success)
                        {
                            try {
//                          showToast(getString(R.string.done));
                                Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                            mReStreamState = false;
                            ApiManager.commentLiveStream(mCtx, user, liveStreamID, "* Share This Livestream *", mColorCode, (int) lastAbsTime, new ApiManager.CommentLiveStreamCallback()
                            {
                                @Override
                                public void onResult(boolean success, String message)
                                {
                                    if (success)
                                    {
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
        ImageView mFb = (ImageView) MyShareDialog.findViewById(R.id.layout_fb);
        mFb.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_FB";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"fb");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("facebook", BranchShareData(), new Branch.BranchLinkCreateListener() {
                    @Override
                    public void onLinkCreate(String url, BranchError error) {
                        FacebookSdk.sdkInitialize(getApplicationContext());
                        callbackManager = CallbackManager.Factory.create();
                        shareDialog = new ShareDialog(mCtx);
                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                            }

                            @Override
                            public void onCancel() {
                            }

                            @Override
                            public void onError(FacebookException error) {
                            }
                        });

                        if (ShareDialog.canShow(ShareLinkContent.class)) {
                            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                    .setContentTitle(mTitle)
                                    .setContentDescription(caption)
                                    .setContentUrl(Uri.parse(url))
                                    .setImageUrl(Uri.parse(Singleton.getS3FileUrl(mPlayerModel.getPicture())))
                                    .build();

                            shareDialog.show(linkContent);
                        }
                        mProgress.setVisibility(View.GONE);
                    }
                });
            }
        });
        ImageView mTwitter = (ImageView) MyShareDialog.findViewById(R.id.layout_twitter);
        mTwitter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Twitter";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"twitter");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("twitter", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "twitter","Twitter");
                    }
                });
            }
        });
        ImageView mIg = (ImageView) MyShareDialog.findViewById(R.id.layout_ig);
        mIg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_IG";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"ig");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("instagram", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "instagram","Instagram");
                    }
                });
            }
        });
        ImageView mLine = (ImageView) MyShareDialog.findViewById(R.id.layout_line);
        mLine.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Line";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"line");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("line", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "jp.naver.line.android","Line");
                    }
                });
            }
        });
        ImageView mWechat = (ImageView) MyShareDialog.findViewById(R.id.layout_wechat);
        mWechat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Wechat";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"wechat");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("wechat", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "com.tencent.mm","Wechat");
                    }
                });
            }
        });
        ImageView mWeibo = (ImageView) MyShareDialog.findViewById(R.id.layout_weibo);
        mWeibo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Weibo";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"weibo");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("weibo", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "weibo","Weibo");
                    }
                });
            }
        });
        ImageView mWhatapp = (ImageView) MyShareDialog.findViewById(R.id.layout_whatsapp);
        mWhatapp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Whatsapp";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"whatapp");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("Whatsapp", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "whatsapp","Whatsapp");
                    }
                });
            }
        });
        ImageView mMail = (ImageView) MyShareDialog.findViewById(R.id.layout_mail);
        mMail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Mail";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"mail");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("android.gm", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "android.gm","Gmail");
                    }
                });
            }
        });
        ImageView mMsg = (ImageView) MyShareDialog.findViewById(R.id.layout_msg);
        mMsg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_MSG";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                try{
                    LogEventUtil.ShareLive(mCtx,mApplication,mPlayerModel.getOpenID(),String.valueOf(liveStreamID),"messager");
                }catch (Exception x)
                {

                }

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("mms", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "mms","Msg");
                    }
                });
            }
        });
        ImageView mCopy = (ImageView) MyShareDialog.findViewById(R.id.layout_copy);
        mCopy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Link";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("sms", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        try
                        {
                            mProgress.setVisibility(View.GONE);
                            ClipboardManager myClipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                            myClipBoard.setText(mTitle + " " + url);
                            try {
//                          showToast(getString(R.string.complete));
                                Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                        catch (Exception e)
                        {
                            mProgress.setVisibility(View.GONE);
                            try {
//                          showToast(getString(R.string.error_failed));
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                    }
                });
            }
        });
        Button mBtn = (Button) MyShareDialog.findViewById(R.id.cancel);
        mBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                MyShareDialog.dismiss();
            }
        });

        MyShareDialog.show();
    }

    private void shareDailogChina()
    {
        if(MyShareDialog!=null) MyShareDialog = null;
        MyShareDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        MyShareDialog.setContentView(R.layout.live_share_dailog_china);
        Window window = MyShareDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        MyShareDialog.setCancelable(true);

        final String mTitle = String.format(getString(R.string.live_relive2), mPlayerModel.getOpenID());
        final String mDescription = mTitle + "!" + caption;

        ImageView m17 = (ImageView) MyShareDialog.findViewById(R.id.layout_17);
        if(!mReStreamState) m17.setImageResource(R.drawable.share_17off);
        m17.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(!mReStreamState) {
                    Toast.makeText(mCtx,getString(R.string.shared),Toast.LENGTH_SHORT).show();
                    return;
                }

                //Umeng monitor
                String Umeng_id="Share_Live_17";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                ApiManager.restreamLiveStream(mCtx, user, liveStreamID, myopen, name, new ApiManager.RestreamLiveStreamCallback()
                {
                    @Override
                    public void onResult(boolean success, String message)
                    {
                        if (success)
                        {
                            try {
//                          showToast(getString(R.string.done));
                                Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                            mReStreamState = false;
                            ApiManager.commentLiveStream(mCtx, user, liveStreamID, "* Share This Livestream *", mColorCode, (int) lastAbsTime, new ApiManager.CommentLiveStreamCallback()
                            {
                                @Override
                                public void onResult(boolean success, String message)
                                {
                                    if (success)
                                    {
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
        ImageView mQQ = (ImageView) MyShareDialog.findViewById(R.id.layout_qq);
        mQQ.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_QQ";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("qq", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        if (isInstalled(mCtx, "com.tencent.mobileqq","com.tencent.mobileqq.activity.JumpActivity"))
                        {
                            mProgress.setVisibility(View.GONE);

                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, mDescription + "  " + url);
                            sendIntent.setType("text/plain");
                            sendIntent.setClassName("com.tencent.mobileqq", "com.tencent.mobileqq.activity.JumpActivity");
                            mCtx.startActivity(sendIntent);

//                            String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
//                            File imageFileToShare = new File(imagePath);
//                            Uri uri = Uri.fromFile(imageFileToShare);
//
//                            ArrayList<Uri> list = new ArrayList<Uri>();
//                            list.add(uri);
//
//                            ShareUtil shareUtil = new ShareUtil(mCtx, list);
//                            Intent baseIntent = shareUtil.getBaseIntent("com.tencent.mobileqq.activity.JumpActivity");
//                            baseIntent.putExtra("summary", mDescription + "  " + url);
//                            baseIntent.setType("image/*");
////                            baseIntent.setType("text/*");
//                            baseIntent.putExtra(Intent.EXTRA_SUBJECT, mTitle);
//                            baseIntent.putExtra(Intent.EXTRA_TEXT, mDescription + "  " + url);
//                            mCtx.startActivity(baseIntent);
                        } else {
                            mProgress.setVisibility(View.GONE);
                            Toast.makeText(mCtx, "您还没有安装QQ！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        ImageView mQzone = (ImageView) MyShareDialog.findViewById(R.id.layout_qzone);
        mQzone.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Qzone";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("qzone", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "qzone", "Qzone");

//                        if (isInstalled(mCtx, "com.qzone", "com.qzone.ui.operation.QZonePublishMoodActivity")) {
//
//                            mProgress.setVisibility(View.GONE);
//                            String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
//                            File imageFileToShare = new File(imagePath);
//                            Uri uri = Uri.fromFile(imageFileToShare);
//
//                            ArrayList<Uri> list = new ArrayList<Uri>();
//                            list.add(uri);
//
//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_SEND_MULTIPLE);
//                            intent.putExtra(Intent.EXTRA_SUBJECT, mTitle);
//                            intent.putExtra(Intent.EXTRA_TEXT, mDescription + "  " + url);
//                            intent.setType("image/*");
//                            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, list);
//                            intent.setClassName("com.qzone", "com.qzone.ui.operation.QZonePublishMoodActivity");
//                            mCtx.startActivity(intent);
//                        } else {
//                            mProgress.setVisibility(View.GONE);
//                            Toast.makeText(mCtx, "您还没有安装QQ空间！", Toast.LENGTH_SHORT).show();
//                        }
                    }
                });
            }
        });
        ImageView mMoments = (ImageView) MyShareDialog.findViewById(R.id.layout_moments);
        mMoments.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_moments";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("moments", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        if (isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareToTimeLineUI")) {

                            mProgress.setVisibility(View.GONE);
                            String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
                            File imageFileToShare = new File(imagePath);
                            Uri uri = Uri.fromFile(imageFileToShare);

                            ArrayList<Uri> list = new ArrayList<Uri>();
                            list.add(uri);

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                            intent.putExtra("Kdescription", mDescription + "  " + url);
                            intent.putExtra(Intent.EXTRA_SUBJECT, mTitle);
                            intent.putExtra(Intent.EXTRA_TEXT, mDescription + "  " + url);
                            intent.setType("image/*");
                            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, list);
                            intent.setClassName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareToTimeLineUI");
                            mCtx.startActivity(intent);
                        } else {
                            mProgress.setVisibility(View.GONE);
                            Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        ImageView mLine = (ImageView) MyShareDialog.findViewById(R.id.layout_line);
        mLine.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Line";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("line", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "jp.naver.line.android","Line");
                    }
                });
            }
        });
        ImageView mWechat = (ImageView) MyShareDialog.findViewById(R.id.layout_wechat);
        mWechat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Wechat";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("wechat", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
//                        shareIntent(mTitle, mDescription + "  " + url, "com.tencent.mm","Wechat");

                        mProgress.setVisibility(View.GONE);
                        String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
                        File imageFileToShare = new File(imagePath);
                        Uri uri = Uri.fromFile(imageFileToShare);

                        ArrayList<Uri> list = new ArrayList<Uri>();
                        list.add(uri);

//                        if (isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI")) {
//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_SEND_MULTIPLE);
//                            intent.setType("image/*");
//                            intent.putExtra("Kdescription", mDescription + "  " + url);
//                            intent.putExtra(Intent.EXTRA_SUBJECT, mTitle);
//                            intent.putExtra(Intent.EXTRA_TEXT, mDescription + "  " + url);
//                            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, list);
//                            intent.setClassName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI");
//                            mCtx.startActivity(intent);
//                        } else {
//                            Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
//                        }

                        if (isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI")) {
//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_SEND);
//                            intent.putExtra("Kdescription", mDescription + "  " + url);
//                            intent.putExtra(Intent.EXTRA_SUBJECT, mTitle);
//                            intent.putExtra(Intent.EXTRA_TEXT, mDescription + "  " + url);
//                            intent.setType("text/*");
//                            intent.setClassName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI");
//                            mCtx.startActivity(intent);

                            try {
                                IWXAPI api;
                                api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
                                api.registerApp(Constants.WECHAT_APP_KEY);

                                WXWebpageObject webpage = new WXWebpageObject();
                                webpage.webpageUrl = url;

//                                Bitmap bmp = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "17app.jpg");
//                                WXImageObject imgObj = new WXImageObject(bmp);

                                WXMediaMessage msg = new WXMediaMessage(webpage);
                                msg.title = mTitle;
                                msg.description = mDescription + "  " + url;
//                                msg.mediaObject = imgObj;
//                                msg.thumbData = Util.bmpToByteArray(bmp, true);

                                SendMessageToWX.Req req = new SendMessageToWX.Req();
                                req.transaction = buildTransaction("link");
                                req.message = msg;
                                req.scene = SendMessageToWX.Req.WXSceneSession;
                                api.sendReq(req);
                            }
                            catch (OutOfMemoryError o){
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e){
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        ImageView mWeibo = (ImageView) MyShareDialog.findViewById(R.id.layout_weibo);
        mWeibo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Weibo";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("weibo", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "weibo","Weibo");
                    }
                });
            }
        });
        ImageView mWhatapp = (ImageView) MyShareDialog.findViewById(R.id.layout_whatsapp);
        mWhatapp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Whatsapp";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("Whatsapp", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "whatsapp","Whatsapp");
                    }
                });
            }
        });
        ImageView mMail = (ImageView) MyShareDialog.findViewById(R.id.layout_mail);
        mMail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Mail";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("android.gm", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "android.gm","Gmail");
                    }
                });
            }
        });
        ImageView mMsg = (ImageView) MyShareDialog.findViewById(R.id.layout_msg);
        mMsg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_MSG";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("mms", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        shareIntent(mTitle, mDescription + "  " + url, "mms","Msg");
                    }
                });
            }
        });
        ImageView mCopy = (ImageView) MyShareDialog.findViewById(R.id.layout_copy);
        mCopy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Umeng monitor
                String Umeng_id="Share_Live_Link";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                MyShareDialog.dismiss();
                mProgress.setVisibility(View.VISIBLE);
                Branch.getInstance(getApplicationContext()).getContentUrl("sms", BranchShareData(), new Branch.BranchLinkCreateListener()
                {
                    @Override
                    public void onLinkCreate(String url, BranchError error)
                    {
                        try
                        {
                            mProgress.setVisibility(View.GONE);
                            ClipboardManager myClipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                            myClipBoard.setText(mTitle + " " + url);
                            try {
//                          showToast(getString(R.string.complete));
                                Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                        catch (Exception e)
                        {
                            mProgress.setVisibility(View.GONE);
                            try {
//                          showToast(getString(R.string.error_failed));
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                    }
                });
            }
        });
        Button mBtn = (Button) MyShareDialog.findViewById(R.id.cancel);
        mBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                MyShareDialog.dismiss();
            }
        });

        MyShareDialog.show();
    }

    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis())
                :type + System.currentTimeMillis();
    }

    public static boolean isInstalled(Context context, String packageName, String activityName)
    {
        Intent intent = new Intent();
        intent.setClassName(packageName, activityName);
        List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, 0);
        if (list.size() > 0) {
            return true;
        }
        return false;
    }

    private Target mTarget = new Target()
    {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
        {
            try {
                mLoadImgState = true;
                mLoadImg.setVisibility(View.VISIBLE);
                mLoadImg.setImageBitmap(BitmapBlurHelper.doBlur(mCtx, bitmap, 2));
            }
            catch (OutOfMemoryError o){
                mLoadImgState = false;
                mLoadImg.setVisibility(View.GONE);
            }
            catch (Exception e){
                mLoadImgState = false;
                mLoadImg.setVisibility(View.GONE);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable)
        {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable)
        {

        }
    };

    private void showGiftDialg()
    {
        if(mApplication.getProductModels().size()!=0)
        {
            mBuyDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
            mBuyDialog.setContentView(R.layout.buy_dialog);

            //event tracking
            try{
                LogEventUtil.EnterBuyPointInLive(mCtx,mApplication);
            }catch (Exception x)
            {

            }

            TextView mCount = (TextView) mBuyDialog.findViewById(R.id.my_count);
            mCount.setText(String.format(getString(R.string.buy_my_p_money), Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));

            ((LinearLayout) mBuyDialog.findViewById(R.id.free)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Log.d("123", "free");
                }
            });

            LinearLayout mRow = (LinearLayout) mBuyDialog.findViewById(R.id.row);

            for(int i = 0 ; i < mApplication.getProductModels().size() ; i ++)
            {
                View mView = inflater.inflate(R.layout.buy_dialog_row, null);
                LinearLayout mLayout = (LinearLayout) mView.findViewById(R.id.buy_layout);
                ImageView mImg = (ImageView) mView.findViewById(R.id.img);
                TextView mPoint = (TextView) mView.findViewById(R.id.point);
                TextView mFreePoint = (TextView) mView.findViewById(R.id.free_point);
                final TextView mMoney = (TextView) mView.findViewById(R.id.money);

                final int pos = i;
                mLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mBuyDialog.dismiss();
                        mBuyPos = pos;

                        //event tracking
                        buyPointCount++;
                        try{
                            LogEventUtil.ClickBuyPointInLive(mCtx,mApplication,mApplication.getProductModels().get(pos).getPoint());
                        }catch (Exception x)
                        {

                        }

                        if(Constants.INTERNATIONAL_VERSION)
                        {
                        try {
                            if(mHelper!=null) mHelper.launchPurchaseFlow(mCtx, mApplication.getProductModels().get(pos).getProductID(), mApplication.getProductModels().get(pos).getPoint(), mPurchaseFinishedListener, mApplication.getProductModels().get(pos).getProductID());
                        }
                        catch (Exception e){
                            Toast.makeText(mCtx,getString(R.string.error_failed),Toast.LENGTH_SHORT).show();
                        }
                        }
                        else
                        {
                            Toast.makeText(mCtx,getString(R.string.processing),Toast.LENGTH_SHORT).show();
                            ApiManager.createOrder(mCtx, Singleton.preferences.getString(Constants.OPEN_ID, ""), mApplication.getProductModels().get(pos).getProductID(), new ApiManager.checkFacebookIDAvailableCallback() {
                                @Override
                                public void onResult(boolean success, String transid) {
                                    if(success && transid.length()!=0)
                                    {
                                        String pay = "transid=" + transid + "&appid=" + IAppPaySDKConfig.APP_ID;
                                        IAppPay.startPay(mCtx, pay, iPayResultCallback);

                                        //event tracking
                                        buyPointCount++;
                                        try{
                                            LogEventUtil.PurchasePoint(mCtx,mApplication,mApplication.getProductModels().get(pos).getPoint(),buyPointCount);
                                        }catch (Exception x)
                                        {

                                        }
                                    }
                                    else Toast.makeText(mCtx,getString(R.string.error_failed),Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });

                if(love_pos == gift_loves.length) love_pos = 0;
                mImg.setImageResource(gift_loves[love_pos]);
                love_pos++;

                if(Constants.INTERNATIONAL_VERSION)
                {
                    if(mApplication.getProductModels().get(pos).getGoogle_point().length()!=0) mPoint.setText(mApplication.getProductModels().get(i).getGoogle_point());
                    else mPoint.setText(mApplication.getProductModels().get(i).getPoint() + "P");

                    mMoney.setText(mApplication.getProductModels().get(i).getGoogle_price());
                }
                else
                {
                    mPoint.setText(mApplication.getProductModels().get(i).getPoint() + "P");

                    mMoney.setText("¥ " + String.valueOf(mApplication.getProductModels().get(i).getPrice()) + " ");
                }

                if(mApplication.getProductModels().get(i).getBonusPercentage()!=0)
                {
                    mFreePoint.setVisibility(View.VISIBLE);
                    mFreePoint.setText("(" + (mApplication.getProductModels().get(i).getPoint() - mApplication.getProductModels().get(i).getBonusAmount()) + "+" + mApplication.getProductModels().get(i).getBonusPercentage() + "%)");
                }
                else mFreePoint.setVisibility(View.GONE);

                mRow.addView(mView);
            }

            mBuyDialog.show();
        }
        else {
            try {
//                          showToast(getString(R.string.empty));
                Toast.makeText(mCtx, getString(R.string.empty), Toast.LENGTH_SHORT).show();
            } catch (Exception x) {
            }
        }
    }

    IPayResultCallback iPayResultCallback = new IPayResultCallback()
    {
        @Override
        public void onPayResult(int resultCode, String signvalue, String resultInfo)
        {
            switch (resultCode)
            {
                case IAppPay.PAY_SUCCESS:
                    boolean payState = IAppPayOrderUtils.checkPayResult(signvalue, IAppPaySDKConfig.PLATP_KEY);
                    if(payState){
                        Toast.makeText(mCtx, "支付成功", Toast.LENGTH_LONG).show();
                        ApiManager.getSelfInfo(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), new ApiManager.GetSelfInfoCallback() {
                            @Override
                            public void onResult(boolean success, String message, UserModel userModel) {
                                if (success && userModel != null) {
                                    try {
                                        Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, userModel.getPoint()).commit();
                                        mGiftCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));
                                    } catch (Exception e) {
                                        payPointError();
                                    }
                                }
                                else payPointError();
                            }
                        });

                        try{
                            String Umeng_id="Pay_Product_ipay";
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put("Point", String.valueOf(mApplication.getProductModels().get(mBuyPos).getPoint()));
                            mHashMap.put("userID", Singleton.preferences.getString(Constants.USER_ID, ""));
                            mHashMap.put("Price", "¥ " + String.valueOf(mApplication.getProductModels().get(mBuyPos).getPrice()));
                            MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                        }
                        catch (Exception e){
                        }
                    }
                    break;
                case IAppPay.PAY_ING:
                    Toast.makeText(mCtx, "成功下单", Toast.LENGTH_LONG).show();
                    break ;
                default:
                    Toast.makeText(mCtx, resultInfo, Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    private class DownloadFile extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Log.d("123","START");
        }

        @Override
        protected String doInBackground(String... download_url)
        {
            int count;

            try
            {
                Log.d("123","download_url[0] : " + download_url[0]);
                Log.d("123","download_url[1] : " + download_url[1]);

                URL url = new URL(download_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(),8192);

                OutputStream output = new FileOutputStream(Singleton.getExternalMediaFolderPath() + download_url[1]);
                byte data[] = new byte[1024];
                long total = 0;

                while ((count = input.read(data)) != -1)
                {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();

                File file = new File(Singleton.getExternalMediaFolderPath() + download_url[1]);
                ZipUtils.upZipFile(file, Environment.getExternalStorageDirectory().getAbsolutePath() + "/." + Singleton.applicationContext.getPackageName() + "/");
        }
            catch (Exception e)
            {
            }

            return null;
        }

        protected void onProgressUpdate(String... progress)
        {

        }

        @Override
        protected void onPostExecute(String file_url)
        {
            Log.d("123","END");

            DisplayMetrics mDisplayMetrics = new DisplayMetrics();
            mCtx.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
            mGiftAni.removeAllViews();
            GiftView mGiftView = new GiftView(mCtx, mGiftModels.get(0).getFrameDuration(), mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels,mGiftModels.get(0).getGiftID(),mGiftModels.get(0).getNumOfImages(),mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
            mGiftView.mGiftEndListener = mCtx;
            mGiftAni.addView(mGiftView);
        }
    }

    private JSONObject BranchShareData()
    {
        JSONObject mShareData = new JSONObject();
        try
        {
            mShareData.put("page", "live");
            mShareData.put("ID", liveStreamID);
            mShareData.put("$og_title", String.format(getString(R.string.live_relive2), mPlayerModel.getOpenID()));
            mShareData.put("$og_description", caption);
            mShareData.put("$og_image_url", Singleton.getS3FileUrl(mPlayerModel.getPicture()));
            mShareData.put("$og_type", "website");
            mShareData.put("$fallback_url", Constants.MEDIA17_WEBSITE_SHARE + "live/" + liveStreamID);
        }
        catch (JSONException ex)
        {
        }

        return mShareData;
    }

    private void shareIntent(String mTitle, String mDescription, String key, String app)
    {
        try
        {
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, mTitle);
            intent.putExtra(Intent.EXTRA_TEXT, mDescription);
            intent.setType("image/*");

            String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
            File imageFileToShare = new File(imagePath);
            Uri uri = Uri.fromFile(imageFileToShare);
            intent.putExtra(Intent.EXTRA_STREAM, uri);

            PackageManager mPackageManager = getPackageManager();
            List<ResolveInfo> mResolveInfos = mPackageManager.queryIntentActivities(intent, 0);

            ComponentName mComponentName = null;
            for (int i = 0; i < mResolveInfos.size(); i++)
            {
                ResolveInfo mResolveInfo = mResolveInfos.get(i);
                String packageName = mResolveInfo.activityInfo.packageName;

                if(packageName.contains(key))
                {
                    mComponentName = new ComponentName(packageName, mResolveInfo.activityInfo.name);
                    break;
                }
            }

            if(mComponentName!=null)
            {
                mProgress.setVisibility(View.GONE);
                intent.setComponent(mComponentName);
                startActivity(intent);
            }
            else
            {
                mProgress.setVisibility(View.GONE);
                try {
//                          showToast(getString(R.string.failed));
                    Toast.makeText(mCtx, String.format(getString(R.string.no_install),app), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }
            }
        }
        catch (Exception e)
        {
            mProgress.setVisibility(View.GONE);
        }
    }

    private class DownloadTask extends AsyncTask<String, Integer, Boolean>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            mDownloadState = false;
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            try
            {
                URL url = new URL(Singleton.getS3FileUrl(text[0]));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "17app.jpg";
                FileOutputStream outStream = new FileOutputStream(file);
                BitmapFactory.decodeStream(input).compress(Bitmap.CompressFormat.JPEG, 75, outStream);
                outStream.close();
                return true;
            }
            catch (OutOfMemoryError o)
            {
                return false;
            }
            catch (IOException e)
            {
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);

            mDownloadState = true;

            if(result) mSharePicture = true;
            else mSharePicture = false;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data)
    {
        try
        {
            if(callbackManager!=null) callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        catch (Exception e)
        {

        }

        try
        {
            if(mHelper!=null)
            {
            if (!mHelper.handleActivityResult(requestCode,resultCode, data))
            {
                super.onActivityResult(requestCode, resultCode, data);
            }
            }
        }
        catch (Exception e)
        {

        }

        if (requestCode == com.tencent.connect.common.Constants.REQUEST_LOGIN || requestCode == com.tencent.connect.common.Constants.REQUEST_APPBAR)
        {
            Tencent.onActivityResultData(requestCode, resultCode, data, loginListener);
        }
    }

    private void loadInterstitialAd()
    {
        interstitialAd = new InterstitialAd(this, Constants.AD_LIVE_ID);
        interstitialAd.setAdListener(this);
        interstitialAd.loadAd();
    }

    @Override
    public void onError(Ad ad, AdError error) {
    }

    @Override
    public void onAdLoaded(Ad ad) {
        mAdReady = true;
    }

    @Override
    public void onInterstitialDisplayed(Ad ad) {
    }

    @Override
    public void onInterstitialDismissed(Ad ad) {
        if(mGuest)
        {
//            Intent intent = new Intent();
//            intent.setClass(mCtx, GuestActivity.class);
//            startActivity(intent);
            mCtx.finish();
        }
        else mCtx.finish();;
    }

    @Override
    public void onAdClicked(Ad ad) {
    }

    private int getLoveFPS(int people)
    {
        if(people < 50)
        {
            return (int)(Math.random()* 50 + 300);
        }
        else if(people < 150)
        {
            return (int)(Math.random()* 50 + 200);
        }
        else if(people < 400)
        {
            return (int)(Math.random()* 50 + 150);
        }
        else if(people < 600)
        {
            return (int)(Math.random()* 25 + 100);
        }
        else if(people < 800)
        {
            return (int)(Math.random()* 25 + 80);
        }
        else if(people < 1000)
        {
            return (int)(Math.random()* 20 + 60);
        }
        else
        {
            return (int)(Math.random()* 5 + 40);
        }
    }

    private void showLove()
    {
        if(mLiveNowPeople > 10 )
        {
            if(!mAllState && !mFirstView)
            {
                if(LagSix < 7 && !mEnd)
                {
                    int miss = (int)(Math.random()* 100);
                    if(miss > 10)
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {

                                if(particle!=null) particle = null;
                                if(mScaleModifier!=null) mScaleModifier = null;

                                try {
                                    if(((int)(Math.random()*10)) < 2)
                                    {
                                        mScaleModifier = new ScaleModifier((float)0.8 + (float)(Math.random() * 0.2), (float)1.0 + (float)(Math.random() * 0.2), 0, 1500);
                                        particle = new ParticleSystem(mCtx, 1,xmass[(int) (Math.random() * xmass.length)], 3000);
                                        particle.setSpeedByComponentsRange(-0.02f, 0.02f, -0.2f, -0.1f);
                                        particle.setAcceleration(0.000003f, 90);
                                        particle.setInitialRotationRange(-20, 20);
                                        particle.setRotationSpeed(-20 + ((int) (Math.random() * 40)));
                                        particle.setFadeOut(2000);
                                        particle.addModifier(mScaleModifier);
                                        particle.oneShot(mShowlove, 10);
                                    }
                                    else
                                    {
                                        mScaleModifier = new ScaleModifier((float)0.8 + (float)(Math.random() * 0.2), (float)1.0 + (float)(Math.random() * 0.2), 0, 1500);
                                        particle = new ParticleSystem(mCtx, 1,loves[(int) (Math.random() * loves.length)], 3000);
                                        particle.setSpeedByComponentsRange(-0.02f, 0.02f, -0.2f, -0.1f);
                                        particle.setAcceleration(0.000003f, 90);
                                        particle.setInitialRotationRange(-20, 20);
                                        particle.setRotationSpeed(-20 + ((int) (Math.random() * 40)));
                                        particle.setFadeOut(2000);
                                        particle.addModifier(mScaleModifier);
                                        particle.oneShot(mShowlove, 10);
                                    }
                                }
                                catch (OutOfMemoryError o) {
                                }
                                catch (Exception e){
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    public float convertDpToPixel(float dp)
    {
        Resources resources = mCtx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    private void getPubishUserInfo()
    {
        ApiManager.getUserInfo(mCtx, name, new ApiManager.GetUserInfoCallback()
        {
            @Override
            public void onResult(boolean success, String message, UserModel user)
            {
                if (success && user != null)
                {
                    mPlayerModel = new UserModel();
                    mPlayerModel = user;

                    dialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                    dialog.setContentView(R.layout.live_player);
                    Window window = dialog.getWindow();
                    window.setGravity(Gravity.BOTTOM);
                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    dialog.setCancelable(true);

                    ImageView mPic = (ImageView)dialog.findViewById(R.id.pic);
                    Button mReport = (Button)dialog.findViewById(R.id.report);
                    TextView mName = (TextView)dialog.findViewById(R.id.name);
                    TextView mCaption = (TextView)dialog.findViewById(R.id.caption);
                    ImageView mMyVerifie = (ImageView)dialog.findViewById(R.id.verifie);
                    final Button mFollow = (Button)dialog.findViewById(R.id.follow);
                    mFollow.setVisibility(View.VISIBLE);
                    ImageView mClose = (ImageView)dialog.findViewById(R.id.close);

                    try {
                        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mPlayerModel.getPicture()), mPic, SelfOptions);
                    }
                    catch (OutOfMemoryError o){
                        mPic.setImageResource(R.drawable.placehold_profile_s);
                    }
                    catch (Exception e){
                        mPic.setImageResource(R.drawable.placehold_profile_s);
                    }
                    mName.setText(mPlayerModel.getOpenID());
                    //mCaption.setText(mPlayerModel.getBio());
                    if(mPlayerModel.getIsVerified()==1)
                    {
//                        mVerifie.setVisibility(View.VISIBLE);
                        mMyVerifie.setVisibility(View.VISIBLE);
                    }
                    else
                    {
//                        mVerifie.setVisibility(View.GONE);
                        mMyVerifie.setVisibility(View.GONE);
                    }

                    mClose.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            dialog.dismiss();
                        }
                    });

                    mReport.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {

                            try {
                                LogEventUtil.ReportUser(mCtx, mApplication);
                            }
                            catch (Exception X)
                            {

                            }

                            ApiManager.reportUserAction(mCtx, mPlayerModel.getUserID(), "Livestream User Report", "",new ApiManager.ReportUserActionCallback()
                            {
                                @Override
                                public void onResult(boolean success, String message)
                                {
                                    dialog.dismiss();
                                    if (success) {
                                        try {
//                          showToast(getString(R.string.complete));
                                            Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                    else {
                                        try {
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        }
                    });

                    if(mPlayerModel.getIsFollowing()==1)
                    {
                        mFollow.setText(getString(R.string.user_profile_following));
                        mFollow.setBackgroundResource(R.drawable.btn_green_selector);
                        mFollow.setTextColor(getResources().getColor(R.color.white));
                    }
                    else
                    {
                        if(mPlayerModel.getFollowRequestTime()!=0)
                        {
                            mFollow.setText(getString(R.string.private_mode_request_send));
                            mFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                            mFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                        }
                        else
                        {
                            mFollow.setText("+ " + getString(R.string.user_profile_follow));
                            mFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                            mFollow.setTextColor(getResources().getColor(R.color.black));
                        }
                    }

                    mFollow.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            if(mPlayerModel.getIsFollowing()==1)
                            {
                                mFollow.setText("+ " + getString(R.string.user_profile_follow));
                                mFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                mFollow.setTextColor(getResources().getColor(R.color.black));
                                mPlayerModel.setIsFollowing(0);

                                try {
                                    LogEventUtil.UnfollowUser(mCtx, mApplication, mPlayerModel.getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mPlayerModel.getUserID(), new ApiManager.UnfollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {
                                        }
                                    }
                                });
                            }
                            else
                            {
                                if(mPlayerModel.getFollowRequestTime()!=0)
                                {
                                    mFollow.setText("+ " + getString(R.string.user_profile_follow));
                                    mFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    mFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                                    mPlayerModel.setIsFollowing(0);
                                    mPlayerModel.setFollowRequestTime(0);
                                    ApiManager.cancelFollowRequests(mCtx, mPlayerModel.getUserID(), new ApiManager.RequestCallback() {
                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                                    {
                                        try {
//                          showToast(getString(R.string.follow_count_size));
                                            Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                        return ;
                                    }

                                    if(mPlayerModel.getPrivacyMode().compareTo("private")==0)
                                    {
                                        mFollow.setText(getString(R.string.private_mode_request_send));
                                        mFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                        mFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                                        mPlayerModel.setIsFollowing(0);
                                        mPlayerModel.setFollowRequestTime(Singleton.getCurrentTimestamp());
                                        ApiManager.sendFollowRequest(mCtx, mPlayerModel.getUserID(), new ApiManager.RequestCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success)
                                            {
                                                if (!success)
                                                {
                                                    mFollow.setText("+ " + getString(R.string.user_profile_follow));
                                                    mFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                                    mFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                                                    mPlayerModel.setIsFollowing(0);
                                                    mPlayerModel.setFollowRequestTime(0);
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        mFollow.setText(getString(R.string.user_profile_following));
                                        mFollow.setBackgroundResource(R.drawable.btn_green_selector);
                                        mFollow.setTextColor(getResources().getColor(R.color.white));
                                        mPlayerModel.setIsFollowing(1);

                                        try {
                                            LogEventUtil.UnfollowUser(mCtx, mApplication, mPlayerModel.getUserID());
                                        }
                                        catch (Exception x)
                                        {

                                        }

                                        ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mPlayerModel.getUserID(), new ApiManager.FollowUserActionCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if(mGiftLayout.isShown())
            {
                mGiftLayout.setVisibility(View.GONE);
                return true;
            }

            if(mEdit_layout.isShown())
            {
                mEdit.setText("");
                mEdit_layout.setVisibility(View.INVISIBLE);
                hideKeyboard();

                mComment.setVisibility(View.VISIBLE);
                mLove.setVisibility(View.VISIBLE);
                mAllview.setVisibility(View.VISIBLE);
                mRestream.setVisibility(View.VISIBLE);
                mGift.setVisibility(View.VISIBLE);
            }
            else
            {
                if(!editKeyState)
                {
                    mEnd = true;
                    isReceivingData = false;
                    if(mLoveTimer!=null)
                    {
                        mLoveTimer.cancel();
                        mLoveTimer.purge();
                        mLoveTimer = null;
                    }

                    if(!mGodHand && !mApplication.getIsSbtools() && mLikeCount!=0)
                    {
                        networkSendHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                ApiManager.likeLivestreamBatchUpdate(mCtx, user, liveStreamID, mLikeCount);
                                mLikeCount = 0;
                            }
                        });
                    }

                    int quit = Singleton.getCurrentTimestamp() - mTimestamp;
                    ApiManager.quitViewLiveStream(mCtx, user, liveStreamID, quit, new ApiManager.QuitViewLiveStreamCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            hideKeyboard();
//                            mApplication.setReLoad(true);
                            if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1 && !mFirstView && mAdReady && interstitialAd!=null)
                            {
                                handler.post(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        interstitialAd.show();
                                        mAdReady = false;
                                    }
                                });
                            }
                            else {
                                if(mGuest)
                                {
//                                    Intent intent = new Intent();
//                                    intent.setClass(mCtx, GuestActivity.class);
//                                    startActivity(intent);
                                    mCtx.finish();
                                }
                                else mCtx.finish();
                            }
                        }
                    });
                }
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private class PeopleAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mUsers.size();
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.live_people, null);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            try
            {
                if(mUsers.size() > i)
                {
                    if(mUsers.get(i).getPicture().length()!=0) ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUsers.get(i).getPicture()), holder.pic, PeopleOptions);
                    else holder.pic.setImageResource(R.drawable.placehold_c);
                }
                else holder.pic.setImageResource(R.drawable.placehold_c);
            }
            catch(Exception e)
            {
                holder.pic.setImageResource(R.drawable.placehold_c);
            }

            return convertView;
        }
    }

    private void showUserDialog(final int pos1,int type, UserModel userModel, LiveComment liveComment)
    {
        if(type==0)
        {
            if(Singleton.preferences.getString(Constants.USER_ID, "").compareTo(userModel.getUserID())==0) return ;
        }
        else
        {
            if(Singleton.preferences.getString(Constants.USER_ID, "").compareTo(liveComment.getUserInfo().getUserID())==0) return ;
        }

        if(UserDialog!=null) UserDialog = null;

        UserDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        UserDialog.setContentView(R.layout.live_player);
        Window window = UserDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        UserDialog.setCancelable(true);

        ImageView mPic = (ImageView)UserDialog.findViewById(R.id.pic);
        Button mReport = (Button)UserDialog.findViewById(R.id.report);
        TextView mName = (TextView)UserDialog.findViewById(R.id.name);
        TextView mCaption = (TextView)UserDialog.findViewById(R.id.caption);
        ImageView mUserVerifie = (ImageView)UserDialog.findViewById(R.id.verifie);
        LinearLayout mUserLay = (LinearLayout) UserDialog.findViewById(R.id.user_lay);
        mUserLay.setVisibility(View.VISIBLE);
        Button mUserCom = (Button)UserDialog.findViewById(R.id.user_com);
        final Button mUserFoll = (Button)UserDialog.findViewById(R.id.user_foll);
        ImageView mClose = (ImageView)UserDialog.findViewById(R.id.close);


        LinearLayout mAdmin_layout = (LinearLayout) UserDialog.findViewById(R.id.admin_layout);
        if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1)
        {
            mAdmin_layout.setVisibility(View.VISIBLE);

            TextView mAdmin_comment = (TextView)UserDialog.findViewById(R.id.admin_comment);
            Button mAdmin_block = (Button)UserDialog.findViewById(R.id.admin_block);
            Button mAdmin_ban = (Button)UserDialog.findViewById(R.id.admin_ban);
            Button mAdmin_freeze = (Button)UserDialog.findViewById(R.id.admin_freeze);

            if(type==0) mAdmin_comment.setText("無 - (頭像)");
            else mAdmin_comment.setText(liveComment.getMessage());

            String isID = "";
            if(type==0) isID = userModel.getUserID();
            else isID = liveComment.getUserInfo().getUserID();
            final String DeadID = isID;

            mAdmin_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                    builder.setMessage("封鎖？");
                    builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(mPlayerModel!=null)
                            {
                                try {
                                    LogEventUtil.BlockUser(mCtx, mApplication, mPlayerModel.getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.adminBlockUserAction(mCtx, mPlayerModel.getUserID(), DeadID, new ApiManager.RequestCallback() {
                                    @Override
                                    public void onResult(boolean success) {
                                        if (success) {
                                            try {
//                          showToast(getString(R.string.done));
                                                Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                            } catch (Exception x) {
                                            }
                                        } else {
                                            try {
//                          showToast(getString(R.string.error_failed));
                                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                            } catch (Exception x) {
                                            }
                                        }
                                    }
                                });
                            }
                            else {
                                try {
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        }
                    });
                    builder.setNeutralButton(R.string.cancel, null);
                    builder.create().show();
                }
            });

            mAdmin_ban.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                    builder.setMessage(getString(R.string.user_ban));
                    builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ApiManager.banUserAction(mCtx, DeadID, String.valueOf(liveStreamID), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {
                                        try {
//                          showToast(getString(R.string.done));
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    } else {
                                        try {
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        }
                    });
                    builder.setNeutralButton(R.string.cancel, null);
                    builder.create().show();
                }
            });

            mAdmin_freeze.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                    builder.setMessage(getString(R.string.user_freeze));
                    builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ApiManager.freezeUserAction(mCtx, DeadID, String.valueOf(liveStreamID), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {
                                        try {
//                          showToast(getString(R.string.done));
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    } else {
                                        try {
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        }
                    });
                    builder.setNeutralButton(R.string.cancel, null);
                    builder.create().show();
                }
            });
        }
        else mAdmin_layout.setVisibility(View.GONE);

        if(type==0)
        {
            final UserModel mUser = userModel;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUser.getPicture()), mPic, SelfOptions);
            mName.setText(mUser.getOpenID());
            mCaption.setText(mUser.getBio());
            if(mUser.getIsVerified()==1) mUserVerifie.setVisibility(View.VISIBLE);
            else mUserVerifie.setVisibility(View.GONE);
            mClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserDialog.dismiss();
                }
            });

            mReport.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    try {
                        LogEventUtil.ReportUser(mCtx, mApplication);
                    }
                    catch (Exception X)
                    {

                    }

                    ApiManager.reportUserAction(mCtx, mUser.getUserID(), "Livestream User Report", "",new ApiManager.ReportUserActionCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            UserDialog.dismiss();
                            if (success) {
                                try {
//                          showToast(getString(R.string.complete));
                                    Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                            else {
                                try {
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        }
                    });
                }
            });

            mUserCom.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    UserDialog.dismiss();
                    mEdit_layout.setVisibility(View.VISIBLE);
                    mEdit.setText("@" + mUser.getOpenID() + " ");
                    mEdit.setSelection(mEdit.getText().toString().length());

                    mComment.setVisibility(View.INVISIBLE);
                    mLove.setVisibility(View.INVISIBLE);
                    mAllview.setVisibility(View.INVISIBLE);
                    mRestream.setVisibility(View.INVISIBLE);
                    mGift.setVisibility(View.INVISIBLE);
                    mEdit.setEnabled(true);
                    mEdit.setFocusable(true);
                    mListView.clearFocus();
                    showKeyboard();
                }
            });

            if(mUser.getIsFollowing()==1)
            {
                mUserFoll.setText(getString(R.string.user_profile_following));
                mUserFoll.setBackgroundResource(R.drawable.btn_green_selector);
                mUserFoll.setTextColor(getResources().getColor(R.color.white));
            }
            else
            {
                if(mUser.getFollowRequestTime()!=0)
                {
                    mUserFoll.setText(getString(R.string.private_mode_request_send));
                    mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                    mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                    mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                    mUserFoll.setTextColor(getResources().getColor(R.color.black));
                }
            }

            mUserFoll.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mUser.getIsFollowing()==1)
                    {
                        mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                        mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                        mUserFoll.setTextColor(getResources().getColor(R.color.black));
                        mUser.setIsFollowing(0);

                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mUser.getUserID());
                        }
                        catch (Exception x)
                        {

                        }
                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mUser.getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mUser.getFollowRequestTime()!=0)
                        {
                            mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                            mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                            mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                            mUser.setIsFollowing(0);
                            mUser.setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mUser.getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try {
//                          showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                                return ;
                            }

                            if(mUser.getPrivacyMode().compareTo("private")==0)
                            {
                                mUserFoll.setText(getString(R.string.private_mode_request_send));
                                mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                                mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                                mUser.setIsFollowing(0);
                                mUser.setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mUser.getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                                            mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mUser.setIsFollowing(0);
                                            mUser.setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                mUserFoll.setText(getString(R.string.user_profile_following));
                                mUserFoll.setBackgroundResource(R.drawable.btn_green_selector);
                                mUserFoll.setTextColor(getResources().getColor(R.color.white));
                                mUser.setIsFollowing(1);

                                try{
                                LogEventUtil.FollowUser(mCtx, mApplication, mUser.getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mUser.getUserID(), new ApiManager.FollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
        else
        {
            final LiveComment mCommentDailog = liveComment;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mCommentDailog.getUserInfo().getPicture()), mPic, SelfOptions);
            mName.setText(mCommentDailog.getUserInfo().getOpenID());
            mCaption.setText(mCommentDailog.getUserInfo().getBio());
            if(mCommentDailog.getUserInfo().getIsVerified()==1) mUserVerifie.setVisibility(View.VISIBLE);
            else mUserVerifie.setVisibility(View.GONE);
            mClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserDialog.dismiss();
                }
            });

            mReport.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    try {
                        LogEventUtil.ReportUser(mCtx, mApplication);
                    }
                    catch (Exception X)
                    {

                    }

                    ApiManager.reportUserAction(mCtx, mCommentDailog.getUserInfo().getUserID(), "Livestream Comment Report", mCommentDailog.getMessage(),new ApiManager.ReportUserActionCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            UserDialog.dismiss();
                            if (success) {
                                try {
//                          showToast(getString(R.string.complete));
                                    Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                            else {
                                try {
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        }
                    });
                }
            });

            mUserCom.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    UserDialog.dismiss();
                    mEdit_layout.setVisibility(View.VISIBLE);
                    mEdit.setText("@" + mCommentDailog.getUserInfo().getOpenID() + " ");
                    mEdit.setSelection(mEdit.getText().toString().length());

                    mComment.setVisibility(View.INVISIBLE);
                    mLove.setVisibility(View.INVISIBLE);
                    mAllview.setVisibility(View.INVISIBLE);
                    mRestream.setVisibility(View.INVISIBLE);
                    mGift.setVisibility(View.INVISIBLE);
                    mEdit.setEnabled(true);
                    mEdit.setFocusable(true);
                    mListView.clearFocus();
                    showKeyboard();
                }
            });

            if(mCommentDailog.getUserInfo().getIsFollowing()==1)
            {
                mUserFoll.setText(getString(R.string.user_profile_following));
                mUserFoll.setBackgroundResource(R.drawable.btn_green_selector);
                mUserFoll.setTextColor(getResources().getColor(R.color.white));
            }
            else
            {
                if(mCommentDailog.getUserInfo().getFollowRequestTime()!=0)
                {
                    mUserFoll.setText(getString(R.string.private_mode_request_send));
                    mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                    mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                    mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                    mUserFoll.setTextColor(getResources().getColor(R.color.black));
                }
            }

            mUserFoll.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mCommentDailog.getUserInfo().getIsFollowing()==1)
                    {
                        mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                        mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                        mUserFoll.setTextColor(getResources().getColor(R.color.black));
                        mCommentDailog.getUserInfo().setIsFollowing(0);

                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mCommentDailog.getUserInfo().getUserID());
                        }
                        catch (Exception x)
                        {

                        }
                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mCommentDailog.getUserInfo().getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mCommentDailog.getUserInfo().getFollowRequestTime()!=0)
                        {
                            mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                            mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                            mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                            mCommentDailog.getUserInfo().setIsFollowing(0);
                            mCommentDailog.getUserInfo().setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mCommentDailog.getUserInfo().getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try {
//                          showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                                return ;
                            }

                            if(mCommentDailog.getUserInfo().getPrivacyMode().compareTo("private")==0)
                            {
                                mUserFoll.setText(getString(R.string.private_mode_request_send));
                                mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                                mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                                mCommentDailog.getUserInfo().setIsFollowing(0);
                                mCommentDailog.getUserInfo().setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mCommentDailog.getUserInfo().getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                                            mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mCommentDailog.getUserInfo().setIsFollowing(0);
                                            mCommentDailog.getUserInfo().setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                mUserFoll.setText(getString(R.string.user_profile_following));
                                mUserFoll.setBackgroundResource(R.drawable.btn_green_selector);
                                mUserFoll.setTextColor(getResources().getColor(R.color.white));
                                mCommentDailog.getUserInfo().setIsFollowing(1);
                                try{
                                LogEventUtil.FollowUser(mCtx,mApplication,mCommentDailog.getUserInfo().getUserID());}
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mCommentDailog.getUserInfo().getUserID(), new ApiManager.FollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }

        UserDialog.show();
    }

    private class ViewHolder
    {
        ImageView pic;
    }

    private void grabChunkData(final int chunkIDToDownload) {
        // handle initial case
        if(lastRequestedChunkID == 0) {
            lastRequestedChunkID = chunkIDToDownload-1;
        }

        int missingStartChunkID = lastRequestedChunkID+1;

        // update 'lastRequestedChunkID'
        lastRequestedChunkID = Math.max(chunkIDToDownload, lastRequestedChunkID);

        // try grab prev missing chunk data
        for(int i=missingStartChunkID;i<chunkIDToDownload;i++) {
            Singleton.log("RECOVER MISSING CHUNK ID: " + i);
            grabChunkData(i);
        }

//        Singleton.log("DOWNLOAD CHUNK: " + chunkIDToDownload);

        if(ShowDownTime) downloadChunk(chunkIDToDownload);
        downloadChunk(chunkIDToDownload, new ChunkDownloadCallback() {
            @Override
            public void onResult(boolean success, byte[] chunkDataBuffer) {
                if (success) {
//                    Singleton.log("CHUNK DOWNLOAD SUCCESS!");

                    ChunkDataObject chunkDataObj = new ChunkDataObject();
                    chunkDataObj.chunkDataByteBuffer = chunkDataBuffer;
                    chunkDataObj.chunkID = chunkIDToDownload;

                    receivedChunkDataArray.add(chunkDataObj);

                    // sort 'receivedChunkDataArray' => make it ASCENDING by chunkID

                    Collections.sort(receivedChunkDataArray);

                    // reset condition has been reached!
                    if (receivedChunkDataArray.size() >= LATE_CHUNK_TOLERANCE_THRESHOLD) {
                        resetAllDecoders();
                        return;
                    }

                    startParsingChunkData(NUM_OF_CHUNKS_TO_BUFFER);

                    if (retriedChunkIDSet.size() > 1000) {
                        retriedChunkIDSet = new HashSet<Integer>();
                    }
                } else {
//                    Singleton.log("CHUNK DOWNLOAD FAIL!");

                    if (!retriedChunkIDSet.contains(chunkIDToDownload)) {
                        retriedChunkIDSet.add(chunkIDToDownload);

                        grabChunkData(chunkIDToDownload);
                    } else {
                        resetAllDecoders();
                    }
                }
            }
        });
    }

    private void startParsingChunkData(int minChunkBufferCountThreshold) {
        while (receivedChunkDataArray.size() >= minChunkBufferCountThreshold) {
            // verify there exists a 'ChunkDataObject' so that lastReceivedChunkID->chunkID is increased by 1!
            ChunkDataObject candidate = receivedChunkDataArray.get(0);
            // handle initial case: set 'lastReceivedChunkID' as ([lowest chunkID]-1) in the 'receivedChunkDataArray'
            if(lastParsedChunkID == 0) {
                lastParsedChunkID = receivedChunkDataArray.get(0).chunkID - 1;
            }

            if(candidate.chunkID == lastParsedChunkID+1) {
                receivedChunkDataArray.remove(candidate);
            } else {
                break;
            }

            lastParsedChunkID = candidate.chunkID;

//            Singleton.log("lastParsedChunkID: "+lastParsedChunkID);

            final byte[] chunkDataToParse = candidate.chunkDataByteBuffer;

            chunkParsingHandler.post(new Runnable() {
                @Override
                public void run() {
                    // Parse AV Data
                    int dataPtr = 0;

                    while(dataPtr < chunkDataToParse.length) {
                        // get info
                        final int dataLength = ByteBuffer.wrap(chunkDataToParse, dataPtr, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
                        final int absTimestamp = ByteBuffer.wrap(chunkDataToParse, dataPtr+4, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
                        int headerLength = new Byte(chunkDataToParse[dataPtr+8]).intValue();
                        int dataType = new Byte(chunkDataToParse[dataPtr+9]).intValue();

                        final ByteBuffer packetData;
                        try {
                            packetData = ByteBuffer.allocateDirect(dataLength);
                        }
                        catch (OutOfMemoryError o) {
                            return;
                        }
                        catch (Exception e) {
                            return;
                        }

                        try{
                            System.arraycopy(chunkDataToParse, dataPtr + headerLength, packetData.array(), packetData.arrayOffset(), dataLength);
                        }
                        catch (Exception e){
                        }

                        if(dataType == CHUNK_DATA_TYPE_AUDIO) {
                            lastReceivedAudioDataTime = SystemClock.uptimeMillis();

                            // TRICKY: over 3 seconds not receive audio data => need reset audio player!
                            if(lastReceivedAudioDataTime>0 && SystemClock.uptimeMillis()-lastReceivedAudioDataTime>3000) {
                                audioPlayHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(pcmAudioPlayer!=null && isReceivingData) {
                                            pcmAudioPlayer.reset();
                                        }
                                    }
                                });
                            }

                            if(audioDecodeHandler!=null) {
                                audioDecodeHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(!isReceivingData) {
                                            return;
                                        }

                                        final AudioFrameObject afObj = new AudioFrameObject();
                                        afObj.audioFrameByteBuffer = packetData;
                                        afObj.absTimestamp = absTimestamp;
                                        afObj.audioFrameByteBufferLength = dataLength;

                                        int resultCode = OpusCodec.instance().decodeAudio(afObj.audioFrameByteBuffer, afObj.audioFrameByteBufferLength, afObj.audioDataByteBuffer);

                                        afObj.audioFrameByteBuffer = null;

                                        if(resultCode>0) {
                                            audioPlayHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if(!isReceivingData) {
                                                        return;
                                                    }

                                                    if (pcmAudioPlayer != null) {
                                                        pcmAudioPlayer.playAudio(afObj.audioDataByteBuffer.array(), afObj.audioDataByteBuffer.arrayOffset(), 1920 * 2, afObj.absTimestamp);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        } else if(dataType == CHUNK_DATA_TYPE_VIDEO) {
                            if(videoDecodeHandler!=null) {
                                videoDecodeHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(!isReceivingData) {
                                            return;
                                        }

                                        try {
                                            final VideoFrameObject vfObj = new VideoFrameObject();
                                            vfObj.videoFrameByteBuffer = packetData;
                                            vfObj.absTimestamp = absTimestamp;
                                            vfObj.videoFrameByteBufferLength = dataLength;

                                            int resultCode = VP8Codec.instance().decodeFrame(vfObj.videoFrameByteBuffer, vfObj.videoFrameByteBufferLength, 0, vfObj.yDataByteBuffer, vfObj.uDataByteBuffer, vfObj.vDataByteBuffer);
                                            if(resultCode==1) { // decode success
//                                    Singleton.log("DECODE SUCCESS");

                                                // Add to video frame queue for later display!

                                                // now decoded video frame data save to disk cache
                                                try {
                                                    ByteBuffer[] byteBuffers = new ByteBuffer[3];
                                                    byteBuffers[0] = vfObj.yDataByteBuffer;
                                                    byteBuffers[1] = vfObj.uDataByteBuffer;
                                                    byteBuffers[2] = vfObj.vDataByteBuffer;

                                                    FileChannel diskCacheFileChannle = new FileOutputStream(liveStreamVideoFrameCacheFolderPath+vfObj.absTimestamp).getChannel();

                                                    diskCacheFileChannle.write(byteBuffers);
                                                    diskCacheFileChannle.close();
                                                } catch (Exception e) {

                                                }

                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if(!isReceivingData) {
                                                            return;
                                                        }

                                                        queuedVideoFrameObjects.add(vfObj);
                                                    }
                                                });
                                            } else {
//                                    Singleton.log("DECODE FAIL");
                                            }

                                            vfObj.yDataByteBuffer = null;
                                            vfObj.uDataByteBuffer = null;
                                            vfObj.vDataByteBuffer = null;
                                            vfObj.videoFrameByteBuffer = null;
                                        }
                                        catch (OutOfMemoryError o) {
                                            return;
                                        }
                                        catch (Exception e) {
                                            return;
                                        }
                                    }
                                });
                            }
                        }

                        dataPtr += (headerLength + dataLength);
                    }
                }
            });
        }
    }

    long lastAbsTime = 0;
    int queuedRenderEventCount = 0;

    @Override
    public void didPlayAudio(final long absTime, int numOfAudioBufferInQueue) {
        if(absTime==0) {
            return;
        }

        if(numOfAudioBufferInQueue<=MIN_AUDIO_BUFFER_COUNT_IN_QUEUE) {
            startParsingChunkData(1);
        }

//        Singleton.log("absTime Diff: "+(absTime-lastAbsTime));

        lastAbsTime = absTime;

//        Singleton.log("queuedVideoFrameObjects.len: "+queuedVideoFrameObjects.size());

        // To Sync AV, play the latest video frame in queue, that has nearest absTimestamp to 'audioAbsTimestamp'
        VideoFrameObject videoFrameForDisplay = null;

        for(VideoFrameObject vfObj : queuedVideoFrameObjects) {
            if(vfObj.absTimestamp<=absTime) {
                videoFrameForDisplay = vfObj;
            }
        }

        if(videoFrameForDisplay==null) {
            return;
        }

        int numOfVideoFramesToDiscard = queuedVideoFrameObjects.indexOf(videoFrameForDisplay) + 1;
//        Singleton.log("numOfVideoFramesToDiscard.len: " + numOfVideoFramesToDiscard);

        for(int i=0;i<numOfVideoFramesToDiscard;i++) {
            try { // delete disk cache
                VideoFrameObject videoFrameToDiscard = queuedVideoFrameObjects.get(0);

                if(videoFrameForDisplay!=videoFrameToDiscard || isAppInForeground==false) {
                    new File(liveStreamVideoFrameCacheFolderPath+videoFrameToDiscard.absTimestamp).delete();
                }
            } catch (Exception e) {

            }

            try {
                queuedVideoFrameObjects.remove(0);
            } catch (Exception e) {

            }
        }

        //        Singleton.log("numOfAudioBufferInQueue: "+numOfAudioBufferInQueue+", queuedVideoFrameObjects.size: "+queuedVideoFrameObjects.size()+", numOfVideoFramesToDiscard: "+numOfVideoFramesToDiscard);
        //        Singleton.log("numOfVideoFramesToDiscard: "+numOfVideoFramesToDiscard);

        if(!isAppInForeground) {
            return;
        }

        final VideoFrameObject finalVideoFrameForDisplay = videoFrameForDisplay;

        if(queuedRenderEventCount>=1) {
            return;
        }

        queuedRenderEventCount++;

        videoOutputView.queueEvent(new Runnable() {
            @Override
            public void run() {
                videoRenderer.yuvDataBuffer.rewind();

                // load video frame data from disk cache
                try {
                    FileChannel diskCacheFileChannel = new FileInputStream(liveStreamVideoFrameCacheFolderPath + finalVideoFrameForDisplay.absTimestamp).getChannel();

                    int readBytes = 0;

                    while (true) {
                        readBytes = diskCacheFileChannel.read(videoRenderer.yuvDataBuffer);

                        if (readBytes <= 0) {
                            break;
                        }
                    }
                } catch (Exception e) {
//                    Singleton.log("LOAD VIDEO FILE FAIL: "+finalVideoFrameForDisplay.absTimestamp);
                }

                try {
                    new File(liveStreamVideoFrameCacheFolderPath + finalVideoFrameForDisplay.absTimestamp).delete(); // clear disk cache
                } catch (Exception e) {

                }

                videoRenderer.dataLoaded = true;
                videoOutputView.requestRender();

                queuedRenderEventCount--;

                lag++;

                if (mFirstView) {
                    mFirstView = false;

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(mAnimaition!=null)
                            {
                                mAnimaition.stop();
                                mLoadding.clearAnimation();
                            }
                            mLoadding.setVisibility(View.GONE);
                            mLoadImg.setVisibility(View.GONE);
                            mLoadText.setVisibility(View.GONE);
                        }
                    });
                }
            }
        });
    }

    private void resetAllDecoders() {
        Singleton.log("RESET ALL DECODERS!");

        lastParsedChunkID = 0;
        lastRequestedChunkID = 0;

        receivedChunkDataArray.clear();
        queuedVideoFrameObjects.clear();

        regenerateLiveStreamVideoFrameCacheFolder();

        if(!isReceivingData) {
            return;
        }

        try{
            audioPlayHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (pcmAudioPlayer != null) {
                        pcmAudioPlayer.reset();
                    }
                }
            });
        }catch (Exception e){

        }

        if(audioDecodeHandler!=null) {
            audioDecodeHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(!isReceivingData) {
                        return;
                    }

                    OpusCodec.instance().setup(0);
                }
            });
        }

        if(videoDecodeHandler!=null) {
            videoDecodeHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(!isReceivingData) {
                        return;
                    }

                    VP8Codec.instance().setup(0);
                }
            });
        }
    }

    public interface ChunkDownloadCallback
    {
        public void onResult(boolean success, byte[] chunkDataBuffer);
    }

    OkHttpClient mOkHttpClient;

    private void downloadChunk(final int chunkID, final ChunkDownloadCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
//                    URL url = new URL(Singleton.getLivestreamS3FileUrl("getLiveStreamChunk" + "/" + liveStreamID + "/" + chunkID));
//                    URLConnection conn = url.openConnection();
//                    conn.connect();

                    final long download_ready_cdn = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis();
//                    final long ready = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis();
                    if(mOkHttpClient==null) mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                    .url(Singleton.getLivestreamS3FileUrlCheoos("getLiveStreamChunk" + "/" + liveStreamID + "/" + chunkID))
                    .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if(!response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                faild_cdn++;
                                if(Constants.INTERNATIONAL_VERSION) mDownTimeCdn.setText("Cdn -  chunkID : " + chunkID + "  , failure : " + faild_cdn);
                                else mDownTimeCdn.setText("media17.cn -  chunkID : " + chunkID + "  , failure : " + faild_cdn);
                                callback.onResult(false, null);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                long time = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis()-download_ready_cdn;
                                download_allTime_cdn = download_allTime_cdn + time;
                                pos_cdn++;
                                if(Constants.INTERNATIONAL_VERSION) mDownTimeCdn.setText("Cdn - Download : " + time + "  , Mean ： " + (download_allTime_cdn/pos_cdn) + "(" + pos_cdn + ")" + " ,  chunkID : " + chunkID + "  , failure : " + faild_cdn);
                                else mDownTimeCdn.setText("media17.cn - Download : " + time + "  , Mean ： " + (download_allTime_cdn/pos_cdn) + "(" + pos_cdn + ")" + " ,  chunkID : " + chunkID + "  , failure : " + faild_cdn);
                            }
                        });
                    }

//                    int lengthOfFile = conn.getContentLength();
                    int lengthOfFile = (int)response.body().contentLength();

//                    Singleton.log("lengthOfFile: "+lengthOfFile);
                    try
                    {
                        final byte[] chunkDataBuffer = new byte[lengthOfFile];

//                        InputStream inputStream = new BufferedInputStream(url.openStream());
                        InputStream inputStream = new BufferedInputStream(response.body().byteStream());
                        int bytesRead;
                        int totalBytesRead = 0;

                        while ((bytesRead = inputStream.read(chunkDataBuffer, totalBytesRead, lengthOfFile - totalBytesRead)) != -1) {
                            totalBytesRead += bytesRead;

                            if(totalBytesRead==lengthOfFile || totalBytesRead==0) {
                                break;
                            }
                        }

                        inputStream.close();

                        if(totalBytesRead!=lengthOfFile) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
//                                    long downtime = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis() - ready;
//                                    Log.d("123", "error time : " + downtime);
//                                    Toast.makeText(mCtx,"error time : " + downtime,Toast.LENGTH_SHORT).show();
                                    callback.onResult(false, null);
                                }
                            });
                        } else {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
//                                    long downtime = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis() - ready;
//                                    Log.d("123","OK time : " + downtime);
//                                    Toast.makeText(mCtx,"OK time : " + downtime,Toast.LENGTH_SHORT).show();
//                                    all = all + downtime;
//                                    pos++;
                                    callback.onResult(true, chunkDataBuffer);
                                }
                            });
                        }
                    }
                    catch(Exception e)
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                    catch (OutOfMemoryError o)
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                } catch (Exception e) {
//                    Singleton.log("Download Chunk Exception: "+e.toString());

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            }
        }).start();
    }

    OkHttpClient mOkHttpClientOrigin;
    private void downloadChunk(final int chunkID) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String uri = "";
                    if(Constants.INTERNATIONAL_VERSION) uri = "http://17media-1752451560.us-west-2.elb.amazonaws.com/" + "getLiveStreamChunk" + "/" + liveStreamID + "/" + chunkID;
                    else uri = "http://cdn.17app.co/" + "getLiveStreamChunk" + "/" + liveStreamID + "/" + chunkID;
                    final long download_ready_origin = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis();
                    if(mOkHttpClientOrigin==null) mOkHttpClientOrigin = new OkHttpClient();
                    Request request = new Request.Builder()
                    .url(uri)
                    .build();

                    Response response = mOkHttpClientOrigin.newCall(request).execute();
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                long time = Calendar.getInstance(TimeZone.getDefault()).getTimeInMillis()-download_ready_origin;
                                download_allTime_origin = download_allTime_origin + time;
                                pos_origin++;
                                if(Constants.INTERNATIONAL_VERSION) mDownTimeOrigin.setText("Origin - Download : " + time + "  , Mean ： " + (download_allTime_origin / pos_origin) + "(" + pos_origin + ")" + " ,  chunkID : " + chunkID + "  , failure : " + faild_origin);
                                else mDownTimeOrigin.setText("17app.co - Download : " + time + "  , Mean ： " + (download_allTime_origin / pos_origin) + "(" + pos_origin + ")" + " ,  chunkID : " + chunkID + "  , failure : " + faild_origin);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                faild_origin++;
                                if(Constants.INTERNATIONAL_VERSION) mDownTimeOrigin.setText("Origin -  chunkID : " + chunkID + "  , failure : " + faild_origin);
                                else mDownTimeOrigin.setText("17app.co -  chunkID : " + chunkID + "  , failure : " + faild_origin);
                            }
                        });
                    }
                } catch (Exception e) {
                }
            }
        }).start();
    }

//    long all = 0;
//    int pos = 0;

    private boolean isAppInForeground = false;

    @Override
    public void onResume() {
        super.onResume();

        if(videoOutputView!=null) videoOutputView.onResume();

        isAppInForeground = true;

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    @Override
    public void onPause() {
        super.onPause();

        if(videoOutputView!=null) videoOutputView.onPause();

        isAppInForeground = false;
//        Log.d("123","平均 ： " + (all/pos));
//        Toast.makeText(mCtx,"平均 ： " + (all/pos),Toast.LENGTH_SHORT).show();

        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onDestroy() {
        isReceivingData = false;

        try
        {
            cleanUp();

            try {
                audioPlayHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(pcmAudioPlayer!=null) pcmAudioPlayer.stopPlaying();
                    }
                });
            } catch (Exception e){

            }

            if(keepAliveTimer!=null)
            {
                keepAliveTimer.cancel();
                keepAliveTimer.purge();
                keepAliveTimer = null;
            }

            if(audioDecodeHandler!=null) {
                audioDecodeHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        OpusCodec.instance().release();
                    }
                });
            }

            if(videoDecodeHandler!=null) {
                videoDecodeHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        VP8Codec.instance().release();
                    }
                });
            }

            // destroy all handler threads

            // TRICKY => this ensures that audio player is properly released!
            if(audioPlayHandler!=null) {
            audioPlayHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(handler!=null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(audioPlayerHandlerThread!=null) audioPlayerHandlerThread.getLooper().quit();
                        }
                    });
                    }
                }
            });
            }

            if(networkSendHandler!=null) {
            networkSendHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(handler!=null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(networkSendHandlerThread!=null) networkSendHandlerThread.getLooper().quit();
                        }
                    });
                    }
                }
            });
            }

            if(networkReceiveHandler!=null) {
            networkReceiveHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(handler!=null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(networkReceiveHandlerThread!=null) networkReceiveHandlerThread.getLooper().quit();
                        }
                    });
                    }
                }
            });
            }

            if(videoDecodeHandler!=null) {
            videoDecodeHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(handler!=null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(videoDecodeHandlerThread!=null) videoDecodeHandlerThread.getLooper().quit();
                        }
                    });
                    }
                }
            });
            }

            if(chunkParsingHandler!=null) {
            chunkParsingHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(handler!=null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(chunkParsingHandlerThread!=null) chunkParsingHandlerThread.getLooper().quit();
                        }
                    });
                    }
                }
            });
            }

            if(videoPlayHandler!=null) {
            videoPlayHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(handler!=null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(videoPlayerHandlerThread!=null) videoPlayerHandlerThread.getLooper().quit();
                        }
                    });
                    }
                }
            });
            }
        } catch (Exception e) {

        }

        if (interstitialAd != null) {
            interstitialAd.destroy();
        }

        queuedVideoFrameObjects.clear();
        receivedChunkDataArray.clear();

        regenerateLiveStreamVideoFrameCacheFolder();

        try
        {
            if (mHelper != null) mHelper.dispose();
            mHelper = null;
        }
        catch (Exception e)
        {

        }

        try{
            if(mMp3!=null) mMp3.release();
        }
        catch (Exception e){
        }

        try {
            if (mTarget != null) {
                mTarget.onBitmapFailed(null);
                Picasso.with(mCtx).cancelRequest(mTarget);
            }
        } catch (Exception e) {
        }

        try {
            if(mTencent!=null) mTencent.logout(mCtx);
        }
        catch (Exception e) {
        }

        super.onDestroy();
    }

    private void startReceivingData() {
        isReceivingData = true;

        new Thread(new Runnable() {
            public void run() {
                while(isReceivingData) {
                    try {
                        // start receiving response
                        final ByteBuffer receivedPacketBuffer = ByteBuffer.allocate(512);
                        receivedPacketBuffer.order(ByteOrder.LITTLE_ENDIAN);

                        final DatagramPacket responsePacket = new DatagramPacket(receivedPacketBuffer.array(), 512);
                        datagramSocket.receive(responsePacket);

                        networkReceiveHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                final int receivedDataLength = responsePacket.getLength();

                                final long currentTime = SystemClock.uptimeMillis();
                                final byte packetType = receivedPacketBuffer.get();
                                final int packetLiveStreamID = receivedPacketBuffer.getInt();

                                if(packetLiveStreamID!=liveStreamID) {
                                    return;
                                }

                                if (packetType == PACKET_TYPE_NEW_LIVE_CHUNK_READY) {
                                    int currentChunkID = receivedPacketBuffer.getInt();
                                    Singleton.log("CHUNK READY: " + currentChunkID);

                                    if (lastRequestedChunkID == 0) {
                                        grabChunkData(currentChunkID - 2);
                                        grabChunkData(currentChunkID - 1);
                                    }

                                    grabChunkData(currentChunkID);
                                }
                                else if (packetType == PACKET_TYPE_LIKE)
                                {
                                    if(mLiveNowPeople < 11)
                                    {
                                        String hex = String.format("#%02x%02x%02x", receivedPacketBuffer.getInt(5), receivedPacketBuffer.getInt(9), receivedPacketBuffer.getInt(13));

                                        try
                                        {
                                            mColorPos = (Integer.valueOf(hex.substring(1,hex.length()), 16).intValue()%28);
                                        }
                                        catch (Exception e)
                                        {
                                            mColorPos = (int) (Math.random() * loves.length);
                                        }

                                        if(mMessage!=null) mMessage = null;
                                        mMessage = new Message();
                                        mMessage.what = 1;

                                        VideoHandler.sendMessage(mMessage);
                                    }

                                }
                                else if (packetType == PACKET_TYPE_STREAM_INFO_CHANGE)
                                {
                                    if(mMessage!=null) mMessage = null;
                                    mMessage = new Message();
                                    mMessage.what = 2;
                                    VideoHandler.sendMessage(mMessage);
                                }
                                else if(packetType == PACKET_TYPE_STREAM_END)
                                {
                                    if(mMessage!=null) mMessage = null;
                                    mMessage = new Message();
                                    mMessage.what = 3;
                                    VideoHandler.sendMessage(mMessage);
                                }
                                else if(packetType == PACKET_TYPE_COMMENT)
                                {
                                    if(mMessage!=null) mMessage = null;
                                    mMessage = new Message();
                                    mMessage.what = 4;
                                    VideoHandler.sendMessage(mMessage);
                                }
                                else if(packetType == PACKET_TYPE_GIFT)
                                {
                                    mGiftToken = receivedPacketBuffer.getInt();

                                    if(mMessage!=null) mMessage = null;
                                    mMessage = new Message();
                                    mMessage.what = 5;
                                    VideoHandler.sendMessage(mMessage);
                                }
                            }
                        });
                    } catch (Exception e) {

                    }
                }
            }
        }).start();
    }

    private Handler VideoHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 1:
                    if(!mAllState)
                    {

                        if(particle!=null) particle = null;
                        if(mScaleModifier!=null) mScaleModifier = null;

                        try {
                            mScaleModifier = new ScaleModifier((float)0.8 + (float)(Math.random() * 0.2), (float)1.0 + (float)(Math.random() * 0.2), 0, 1500);
                            particle = new ParticleSystem(mCtx, 1,loves[(int) (Math.random() * loves.length)], 3000);//mColorPos
                            particle.setSpeedByComponentsRange(-0.02f, 0.02f, -0.2f, -0.1f);
                            particle.setAcceleration(0.000003f, 90);
                            particle.setInitialRotationRange(-20, 20);
                            particle.setRotationSpeed(-20 + ((int) (Math.random() * 40)));
                            particle.setFadeOut(2000);
                            particle.addModifier(mScaleModifier);
                            particle.oneShot(mShowlove, 10);
                        }
                        catch (OutOfMemoryError o) {
                        }
                        catch (Exception e){
                        }

                    }

                    break;
                case 2 :
                    if(!mAllState)
                    {
                        ApiManager.getLiveStreamInfo(mCtx, user, liveStreamID, new ApiManager.GetLiveStreamInfoCallback()
                        {
                            @Override
                            public void onResult(boolean success, LiveModel mLiveModel)
                            {
                                if (success)
                                {
                                    mCount.setText(mLiveModel.getViewerCount() + getString(R.string.live_people));
                                    mNow.setText(String.format(getString(R.string.live_now), mLiveModel.getLiveViewerCount()));
                                    mLiveNowPeople = mLiveModel.getLiveViewerCount();

                                    int fps = getLoveFPS(mLiveNowPeople);
                                    if(mLoveTimer!=null)
                                    {
                                        mLoveTimer.cancel();
                                        mLoveTimer.purge();
                                        mLoveTimer = null;
                                    }
                                    mLoveTimer = new Timer();
                                    mLoveTimer.schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            showLove();
                                        }
                                    }, 0, fps);

                                    lastFPS = fps;
                                }
                            }
                        });

                        if(nowLoadPeople != TenPos)
                        {
                            nowLoadPeople = TenPos;

                            ApiManager.getLiveStreamViewers(mCtx, user, liveStreamID, 0, 100, new ApiManager.GetLiveStreamViewersCallback()
                            {
                                @Override
                                public void onResult(boolean success, String message,ArrayList<UserModel> model)
                                {
                                    if (success && model!=null)
                                    {
                                        if(model.size()!=0)
                                        {
                                            mUsers.clear();
                                            mUsers.addAll(model);

                                            try
                                            {
                                                if(mPeopleAdapter==null)
                                                {
                                                    mPeopleAdapter = new PeopleAdapter();
                                                    mGallery.setAdapter(mPeopleAdapter);
                                                }
                                                else mPeopleAdapter.notifyDataSetChanged();
                                                mGallery.setSelection(mUsers.size()/2);
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                    break;
                case 3 :
                    isReceivingData = false;

                    if(mLoveTimer!=null)
                    {
                        mLoveTimer.cancel();
                        mLoveTimer.purge();
                        mLoveTimer = null;
                    }
                    mEnd = true;

                    if(!mGodHand && !mApplication.getIsSbtools() && mLikeCount!=0)
                    {
                        networkSendHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                ApiManager.likeLivestreamBatchUpdate(mCtx, user, liveStreamID, mLikeCount);
                                mLikeCount = 0;
                            }
                        });
                    }

                    int quit = Singleton.getCurrentTimestamp() - mTimestamp;
                    ApiManager.quitViewLiveStream(mCtx, user, liveStreamID, quit, new ApiManager.QuitViewLiveStreamCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        mLoadding.setVisibility(View.GONE);
                                        mLoadImg.setVisibility(View.GONE);
                                        mLoadText.setVisibility(View.GONE);
                                    } catch (Exception e) {
                                    }
                                }
                            });
                            if(Enddialog==null)
                            {
                                Enddialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                                Enddialog.setContentView(R.layout.live_end_dailog);
                                Window window = Enddialog.getWindow();
                                window.setGravity(Gravity.BOTTOM);
                                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                Enddialog.setCancelable(false);

                                Button mEnd = (Button)Enddialog.findViewById(R.id.end);
                                mEnd.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        hideKeyboard();
                                        mApplication.setReLoad(true);
                                        mApplication.isUserLive = false;
                                        if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1 && !mFirstView && mAdReady && interstitialAd!=null)
                                        {
                                            handler.post(new Runnable()
                                            {
                                                @Override
                                                public void run()
                                                {
                                                    interstitialAd.show();
                                                    mAdReady = false;
                                                }
                                            });
                                        }
                                        else {
                                            if(mGuest)
                                            {
//                                                Intent intent = new Intent();
//                                                intent.setClass(mCtx, GuestActivity.class);
//                                                startActivity(intent);
                                                mCtx.finish();
                                            }
                                            else mCtx.finish();
                                        }
                                    }
                                });

                                try{
                                    if(mCtx!=null) Enddialog.show();
                                }
                                catch (Exception e){
                                }
                            }
                            else
                            {
                                try{
                                    if(mCtx!=null) Enddialog.show();
                                }
                                catch (Exception e){
                                }
                            }
                        }
                    });

                    break;

                case 4 :
                    if(mGetCommentState) return;

                    int c = mTimestamp;
                    if(mLiveComments.size()!=0) c = mCommentTime - 1;

                    mGetCommentState = true;
                    ApiManager.getLiveStreamComments(mCtx, user, liveStreamID, c, 100, new ApiManager.GetLiveStreamCommentsCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<LiveComment> liveComments)
                        {
                            if(success && liveComments!=null)
                            {
                                if(liveComments.size()!=0)
                                {
                                    mCommentTime = liveComments.get(0).getTimestamp();

                                    if(mLiveComments.size()==0)
                                    {
                                        mLiveComments.addAll(liveComments);
//                                        mLastComment.clear();
//                                        mLastComment.addAll(liveComments);
                                    }
                                    else
                                    {
                                        for(int p = (liveComments.size()-1) ; p > -1 ; p--)
                                        {
                                            for(int k = 0 ; k < mLiveComments.size() ; k++)
                                            {
                                                if(liveComments.get(p).getMessage().compareTo(mLiveComments.get(k).getMessage())==0 && liveComments.get(p).getUserInfo().getOpenID().compareTo(mLiveComments.get(k).getUserInfo().getOpenID())==0 && liveComments.get(p).getLiveStreamChatID().compareTo(mLiveComments.get(k).getLiveStreamChatID())==0)
                                                {
                                                    liveComments.remove(p);
                                                    break;
                                                }
                                            }
                                        }
                                        Collections.reverse(liveComments);
//                                        mLastComment.clear();
//                                        mLastComment.addAll(liveComments);
                                        mLiveComments.addAll(liveComments);
                                    }

                                    if(mLiveComments.size() > 100)
                                    {
                                        int remove = mLiveComments.size() - 100;
                                        for(int k = 0 ; k < remove ; k++)
                                        {
                                            mLiveComments.remove(0);
                                        }
                                    }

                                    if(mCommentAdapter==null)
                                    {
                                        mCommentAdapter = new CommentAdapter();
                                        mListView.setAdapter(mCommentAdapter);
                                        mCommentAdapter.notifyDataSetChanged();
                                        mListView.smoothScrollToPosition(mLiveComments.size());
                                    }
                                    else
                                    {
                                        mCommentAdapter.notifyDataSetChanged();
                                        mListView.smoothScrollToPosition(mLiveComments.size());
                                    }

                                    mGetCommentState = false;
                                }
                                else mGetCommentState = false;
                            }
                            else mGetCommentState = false;
                        }
                    });

                    break;

                case 5 :

                    if(mGiftLoad) return;

                    mGiftLoad = true;
                    ApiManager.getLiveStreamGiftInfo(mCtx, liveStreamID, mGiftToken, new ApiManager.GetLiveStreamGiftInfoCallback() {
                        @Override
                        public void onResult(boolean success, LiveGiftsModel liveGiftsModel)
                        {
                            mGiftLoad = false;
                            if(success && liveGiftsModel!=null)
                            {
                                if(mLiveGiftsModels.size()==0 || mLiveGiftsModels.size()==1) mLiveGiftsModels.add(liveGiftsModel);
                                else
                                {
                                    int pos = mLiveGiftsModels.size()-1;
                                    for(int i = 1 ; i < mLiveGiftsModels.size() ; i++)
                                    {
                                        if(liveGiftsModel.getGiftInfo().getPoint() > mLiveGiftsModels.get(i).getGiftInfo().getPoint())
                                        {
                                            pos = i;
                                            break;
                                        }
                                    }
                                    mLiveGiftsModels.add(pos,liveGiftsModel);
                                }

                                if(!mGiftShowState)
                                {
                                    mGiftShowState = true;

                                    File file = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
                                    if (file.exists())
                                    {
                                        mGiftAni.removeAllViews();
                                        GiftView mGiftView = new GiftView(mCtx, mLiveGiftsModels.get(0).getGiftInfo().getFrameDuration(), mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels, mLiveGiftsModels.get(0).getGiftInfo().getGiftID(), mLiveGiftsModels.get(0).getGiftInfo().getNumOfImages(),mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
                                        mGiftView.mGiftEndListener = mCtx;
                                        mGiftAni.addView(mGiftView);
                                        showGiftComment(mLiveGiftsModels.get(0).getGiftInfo(),mLiveGiftsModels.get(0).getUserInfo().getOpenID(),mLiveGiftsModels.get(0).getUserInfo().getPicture());

                                        try {
                                            if(mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack().length()!=0) {
                                                File sound = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                                if(sound.exists()){
                                                    if(mMp3.isPlaying()){
                                                        mMp3.stop();
                                                    }

                                                    mMp3 = null;
                                                    mMp3 = new MediaPlayer();
                                                    mMp3.setDataSource(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                                    mMp3.prepare();
                                                    mMp3.start();
                                                }
                                            }
                                        }
                                        catch (IOException i) {
                                        }
                                        catch (Exception e){
                                        }
                                    }
                                }
                            }
                        }
                    });

//                    File file = new File(Singleton.getExternalMediaFolderPath() + "a_1000/a_1000_37.png");
//                    GPUImage mGPUImage = new GPUImage(mCtx);
//                    mGPUImage.setGLSurfaceView(mGl);
//                    mGPUImage.setImage(file);
//                    mGPUImage.setFilter(new GPUImageSepiaFilter());
//
//                    ApiManager.getLiveStreamReceivedGifts(mCtx, liveStreamID, mGiftTimestamp, 100, new ApiManager.GetLiveStreamReceivedGiftsCallback() {
//                        @Override
//                        public void onResult(boolean success, ArrayList<LiveGiftsModel> liveGiftsModels) {
//
//                            Log.d("123", "success : " + success);
//                            if (success && liveGiftsModels != null) {
//                                Log.d("123", "GIFT OKOK");
//                            }
//                        }
//                    });

                    break;
            }
            super.handleMessage(msg);
        }
    };

    LeaderAdapter mLeaderAdapter = new LeaderAdapter();
    private class LeaderAdapter extends BaseAdapter
    {
//        private ArrayList<GiftLeaderboardModel> mModels = new ArrayList<GiftLeaderboardModel>();
//
//        public LeaderAdapter(ArrayList<GiftLeaderboardModel> model)
//        {
//            mModels.addAll(model);
//        }

        @Override
        public int getCount()
        {
            return mModelsLive.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            LiveGiftViewHolder holder = new LiveGiftViewHolder();

            //得知每個送禮物的人，佔直播主收到禮物總點數的％
            percentage = GetSentGiftPercentage(( mModelsLive.get(position).getPoint()));
            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.present_board_row_live, null);
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.open = (TextView) convertView.findViewById(R.id.open);
                holder.like = (TextView) convertView.findViewById(R.id.like);
                holder.num = (TextView) convertView.findViewById(R.id.num);
                holder.number1_layout = (RelativeLayout) convertView.findViewById(R.id.number1_layout);
                holder.number1_pic = (ImageView) convertView.findViewById(R.id.number1_pic);
                holder.number1_like = (TextView) convertView.findViewById(R.id.number1_like);
                holder.number1_open = (TextView) convertView.findViewById(R.id.number1_open);
                holder.number2_layout = (RelativeLayout) convertView.findViewById(R.id.number2_layout);
                holder.number2_pic = (ImageView) convertView.findViewById(R.id.number2_pic);
                holder.number2_like = (TextView) convertView.findViewById(R.id.number2_like);
                holder.number2_open = (TextView) convertView.findViewById(R.id.number2_open);
                holder.number3_layout = (RelativeLayout) convertView.findViewById(R.id.number3_layout);
                holder.number3_pic = (ImageView) convertView.findViewById(R.id.number3_pic);
                holder.number3_like = (TextView) convertView.findViewById(R.id.number3_like);
                holder.number3_open = (TextView) convertView.findViewById(R.id.number3_open);

                convertView.setTag(holder);
            }
            else holder = (LiveGiftViewHolder) convertView.getTag();

            final int pos = position;


            if(pos==0)
            {
                holder.number1_layout.setVisibility(View.VISIBLE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModelsLive.get(position).getUserInfo().getPicture()), holder.number1_pic, SelfOptions);
                holder.number1_like.setText(String.valueOf(nt.format(percentage)));
                holder.number1_open.setText(mModelsLive.get(position).getUserInfo().getOpenID());
            }
            else if(pos==1)
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.VISIBLE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.GONE);
                holder.number2_like.setText(String.valueOf(nt.format(percentage)));
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModelsLive.get(position).getUserInfo().getPicture()), holder.number2_pic, SelfOptions);
                holder.number2_open.setText(mModelsLive.get(position).getUserInfo().getOpenID());
            }
            else if(pos==2)
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.VISIBLE);
                holder.layout.setVisibility(View.GONE);

                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModelsLive.get(position).getUserInfo().getPicture()), holder.number3_pic, SelfOptions);
                holder.number3_like.setText(String.valueOf(nt.format(percentage)));
                holder.number3_open.setText(mModelsLive.get(position).getUserInfo().getOpenID());
            }
            else
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.VISIBLE);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModelsLive.get(position).getUserInfo().getPicture()), holder.pic, SelfOptions);
                holder.open.setText(mModelsLive.get(position).getUserInfo().getOpenID());
                holder.like.setText(String.valueOf(nt.format(percentage)));
                holder.num.setText("NO."+(position+1));
            }

            return convertView;
        }
    }

    private class LiveGiftViewHolder {
        LinearLayout layout;
        ImageView pic;
        TextView open;
        TextView name;
        TextView like;
        ImageView verifie;
        TextView num;

        RelativeLayout number1_layout;
        ImageView number1_pic;
        TextView number1_like;
        TextView number1_open;

        RelativeLayout number2_layout;
        ImageView number2_pic;
        TextView number2_like;
        TextView number2_open;

        RelativeLayout number3_layout;
        ImageView number3_pic;
        TextView number3_like;
        TextView number3_open;
    }

    private class ViewHolderGrid
    {
        LinearLayout presend_layout;
        ImageView pic;
        TextView name;
        ImageView image;
    }

    public class CommentAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mLiveComments.size();
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup)
        {
            ViewHolderComment holder = new ViewHolderComment();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.live_comment_row, null);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.message = (TextView) convertView.findViewById(R.id.message);
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderComment) convertView.getTag();


            if(mLiveComments.get(i).getMessage().contains("* 分享了這則直播! *") || mLiveComments.get(i).getMessage().contains("* 分享了这则直播! *") || mLiveComments.get(i).getMessage().contains("* Shared This Livestream *") || mLiveComments.get(i).getMessage().contains("* Share This Livestream *"))
            {
                holder.layout.setBackgroundResource(R.drawable.btn_black_c2);
                holder.name.setText("  " + mLiveComments.get(i).getUserInfo().getOpenID() + " ");
                holder.message.setText(getString(R.string.feed_live_restream) + " ");
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getMessage().length() > 4)
            {
                if(mLiveComments.get(i).getMessage().substring(0,2).compareTo("* ")==0 && mLiveComments.get(i).getMessage().substring(mLiveComments.get(i).getMessage().length()-2,mLiveComments.get(i).getMessage().length()).compareTo(" *")==0)
                {
                    holder.layout.setBackgroundResource(R.drawable.btn_black_c2);
                    holder.name.setText("  " + mLiveComments.get(i).getUserInfo().getOpenID() + " ");
                    holder.message.setText(mLiveComments.get(i).getMessage().substring(2,mLiveComments.get(i).getMessage().length()-2) + " ");
                    holder.name.setTextColor(getResources().getColor(R.color.white));
                    holder.message.setTextColor(getResources().getColor(R.color.white));
                }
                else
                {
                    holder.layout.setBackgroundResource(R.drawable.btn_white_c2);
                    holder.name.setText("@" + mLiveComments.get(i).getUserInfo().getOpenID() + " ");
                    holder.message.setText(mLiveComments.get(i).getMessage());
                    holder.message.setTextColor(getResources().getColor(R.color.friend_txt_color));

                    try
                    {
                        if(mLiveComments.size() > i)
                        {
                            holder.name.setTextColor(getResources().getColor(mCommentColors[(Integer.valueOf(mLiveComments.get(i).getColorCode().substring(1, mLiveComments.get(i).getColorCode().length()), 16).intValue()%7)]));
                        }
                        else holder.name.setTextColor(getResources().getColor(mCommentColors[0]));
                    }
                    catch (Exception e)
                    {
                        holder.name.setTextColor(getResources().getColor(mCommentColors[0]));
                    }
                }
            }
            else
            {
                holder.layout.setBackgroundResource(R.drawable.btn_white_c2);
                holder.name.setText("@" + mLiveComments.get(i).getUserInfo().getOpenID() + " ");
                holder.message.setText(mLiveComments.get(i).getMessage());
                holder.message.setTextColor(getResources().getColor(R.color.friend_txt_color));

                try
                {
                    if(mLiveComments.size() > i)
                    {
                        holder.name.setTextColor(getResources().getColor(mCommentColors[(Integer.valueOf(mLiveComments.get(i).getColorCode().substring(1, mLiveComments.get(i).getColorCode().length()), 16).intValue()%7)]));
                    }
                    else holder.name.setTextColor(getResources().getColor(mCommentColors[0]));
                }
                catch (Exception e)
                {
                    holder.name.setTextColor(getResources().getColor(mCommentColors[0]));
                }
            }

            if(mLiveComments.get(i).getIsSystem()==1)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_red_c);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getIsSystem()==2)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_orange_c);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getIsSystem()==3)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_green_c);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getIsSystem()==4)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_blue_c);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getIsSystem()==5)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_black_c2);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    try
                    {
                        if(mLiveComments.size() > i)
                        {
                            showUserDialog(i,1,null,mLiveComments.get(i));
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            });

            return convertView;
        }
    }

    private class ViewHolderComment
    {
        TextView name;
        TextView message;
        LinearLayout layout;
    }

    private void cleanUp() {
        try {
            datagramSocket.close();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private void sendKeepAliveData() {
//        Singleton.log("SEND KEEP ALIVE!");
        final ByteBuffer keepAliveByteBuffer = ByteBuffer.allocate(5);
        keepAliveByteBuffer.order(ByteOrder.LITTLE_ENDIAN);

        keepAliveByteBuffer.put(PACKET_TYPE_KEEP_ALIVE);
        keepAliveByteBuffer.putInt(liveStreamID);

        // send encoded video data
        if(networkSendHandler==null) {
            return;
        }

        networkSendHandler.post(new Runnable() {
            public void run()
            {
                try
                {
                    datagramSocket.send(new DatagramPacket(keepAliveByteBuffer.array(), keepAliveByteBuffer.position(), LIVE_STREAM_HOST_ADDRESS));

                    ApiManager.keepViewLiveStream(mCtx, user, liveStreamID, 0, new ApiManager.KeepViewLiveStreamCallback()
                    {
                        @Override
                        public void onResult(boolean success, boolean state, String message,int blocked)
                        {
                            if(success)
                            {
                                LagSix = 0;

                                if(state)
                                {
                                    if(blocked==1)
                                    {
                                        isReceivingData = false;

                                        if(mLoveTimer!=null)
                                        {
                                            mLoveTimer.cancel();
                                            mLoveTimer.purge();
                                            mLoveTimer = null;
                                        }
                                        mEnd = true;

                                        if(!mGodHand && !mApplication.getIsSbtools() && mLikeCount!=0)
                                        {
                                            networkSendHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    ApiManager.likeLivestreamBatchUpdate(mCtx, user, liveStreamID, mLikeCount);
                                                    mLikeCount = 0;
                                                }
                                            });
                                        }

                                        int quit = Singleton.getCurrentTimestamp() - mTimestamp;
                                        ApiManager.quitViewLiveStream(mCtx, user, liveStreamID, quit, new ApiManager.QuitViewLiveStreamCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success, String message)
                                            {
                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            mLoadding.setVisibility(View.GONE);
                                                            mLoadImg.setVisibility(View.GONE);
                                                            mLoadText.setVisibility(View.GONE);
                                                        } catch (Exception e) {
                                                        }
                                                    }
                                                });
                                                if(Enddialog==null)
                                                {
                                                    Enddialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                                                    Enddialog.setContentView(R.layout.live_end_dailog);
                                                    Window window = Enddialog.getWindow();
                                                    window.setGravity(Gravity.BOTTOM);
                                                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                                    Enddialog.setCancelable(false);

                                                    TextView mTitle = (TextView) Enddialog.findViewById(R.id.title);
                                                    mTitle.setText(getString(R.string.live_stream_blocked));
                                                    Button mEnd = (Button)Enddialog.findViewById(R.id.end);
                                                    mEnd.setOnClickListener(new View.OnClickListener()
                                                    {
                                                        @Override
                                                        public void onClick(View v)
                                                        {
                                                            hideKeyboard();
                                                            mApplication.setReLoad(true);
                                                            mApplication.isUserLive = true;
                                                            if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1 && !mFirstView && mAdReady && interstitialAd!=null)
                                                            {
                                                                handler.post(new Runnable()
                                                                {
                                                                    @Override
                                                                    public void run()
                                                                    {
                                                                        interstitialAd.show();
                                                                        mAdReady = false;
                                                                    }
                                                                });
                                                            }
                                                            else {
                                                                if(mGuest)
                                                                {
//                                                                    Intent intent = new Intent();
//                                                                    intent.setClass(mCtx, GuestActivity.class);
//                                                                    startActivity(intent);
                                                                    mCtx.finish();
                                                                }
                                                                else mCtx.finish();
                                                            }
                                                        }
                                                    });
                                                    Enddialog.show();
                                                }
                                                else Enddialog.show();
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    if(message.compareTo("ended")==0)
                                    {
                                        isReceivingData = false;

                                        if(mLoveTimer!=null)
                                        {
                                            mLoveTimer.cancel();
                                            mLoveTimer.purge();
                                            mLoveTimer = null;
                                        }
                                        mEnd = true;

                                        if(!mGodHand && !mApplication.getIsSbtools() && mLikeCount!=0)
                                        {
                                            networkSendHandler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    ApiManager.likeLivestreamBatchUpdate(mCtx, user, liveStreamID, mLikeCount);
                                                    mLikeCount = 0;
                                                }
                                            });
                                        }

                                        int quit = Singleton.getCurrentTimestamp() - mTimestamp;
                                        ApiManager.quitViewLiveStream(mCtx, user, liveStreamID, quit, new ApiManager.QuitViewLiveStreamCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success, String message)
                                            {
                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            mLoadding.setVisibility(View.GONE);
                                                            mLoadImg.setVisibility(View.GONE);
                                                            mLoadText.setVisibility(View.GONE);
                                                        } catch (Exception e) {
                                                        }
                                                    }
                                                });
                                                if(Enddialog==null)
                                                {
                                                    Enddialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                                                    Enddialog.setContentView(R.layout.live_end_dailog);
                                                    Window window = Enddialog.getWindow();
                                                    window.setGravity(Gravity.BOTTOM);
                                                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                                    Enddialog.setCancelable(false);

                                                    Button mEnd = (Button)Enddialog.findViewById(R.id.end);
                                                    mEnd.setOnClickListener(new View.OnClickListener()
                                                    {
                                                        @Override
                                                        public void onClick(View v)
                                                        {
                                                            hideKeyboard();
                                                            mApplication.setReLoad(true);
                                                            mApplication.isUserLive = false;
                                                            if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1 && !mFirstView && mAdReady && interstitialAd!=null)
                                                            {
                                                                handler.post(new Runnable()
                                                                {
                                                                    @Override
                                                                    public void run()
                                                                    {
                                                                        interstitialAd.show();
                                                                        mAdReady = false;
                                                                    }
                                                                });
                                                            }
                                                            else {
                                                                if(mGuest)
                                                                {
//                                                                    Intent intent = new Intent();
//                                                                    intent.setClass(mCtx, GuestActivity.class);
//                                                                    startActivity(intent);
                                                                    mCtx.finish();
                                                                }
                                                                else mCtx.finish();
                                                            }
                                                        }
                                                    });

                                                    try{
                                                        if(mCtx!=null) Enddialog.show();
                                                    }
                                                    catch (Exception e){
                                                    }
                                                }
                                                else
                                                {
                                                    try{
                                                        if(mCtx!=null) Enddialog.show();
                                                    }
                                                    catch (Exception e){
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else if(message.compareTo("live_killed")==0)
                                    {
                                        if(mEnd) return ;

                                        isReceivingData = false;

                                        if(mLoveTimer!=null)
                                        {
                                            mLoveTimer.cancel();
                                            mLoveTimer.purge();
                                            mLoveTimer = null;
                                        }
                                        mEnd = true;

                                        if(Enddialog!=null) Enddialog = null;
                                        Enddialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                                        Enddialog.setContentView(R.layout.live_end_dailog);
                                        Window window = Enddialog.getWindow();
                                        window.setGravity(Gravity.BOTTOM);
                                        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                        Enddialog.setCancelable(false);

                                        TextView mTitle = (TextView) Enddialog.findViewById(R.id.title);
                                        mTitle.setText(getString(R.string.live_watch_kill));

                                        Button mEnd = (Button)Enddialog.findViewById(R.id.end);
                                        mEnd.setOnClickListener(new View.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(View v)
                                            {
                                                hideKeyboard();
                                                mApplication.setReLoad(true);
                                                mApplication.isUserLive = false;
                                                if(mGuest)
                                                {
//                                                    Intent intent = new Intent();
//                                                    intent.setClass(mCtx, GuestActivity.class);
//                                                    startActivity(intent);
                                                    mCtx.finish();
                                                }
                                                else mCtx.finish();
                                            }
                                        });

                                        Enddialog.show();
                                    }
                                }
                            }
                            else
                            {
                                LagSix++;
                            }
                        }
                    });

                    if(!mEnd)
                    {
                        if(lag==0)
                        {
                            if(!mLoadding.isShown())
                            {
                                handler.post(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        try
                                        {
                                            mLoadding.setBackgroundResource(R.drawable.live_lag);
                                            mAnimaition = (AnimationDrawable) mLoadding.getBackground();
                                            mAnimaition.start();
                                        }
                                        catch (OutOfMemoryError e)
                                        {
                                            System.gc();
//                                            mLoadding.setImageResource(R.drawable.lod_12);
                                        }
                                        mLoadding.setVisibility(View.VISIBLE);
                                        if(mLoadImgState) mLoadImg.setVisibility(View.VISIBLE);
                                        else mLoadImg.setVisibility(View.GONE);
                                        mLoadText.setText(getString(R.string.live_loadding_busy));
                                        mLoadText.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }
                        else
                        {
                            lag=0;
                            if(mLoadding.isShown())
                            {
                                handler.post(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if(mAnimaition!=null)
                                        {
                                            mLoadding.clearAnimation();
                                            mAnimaition.stop();
                                        }
                                        mLoadding.setVisibility(View.GONE);
                                        mLoadImg.setVisibility(View.GONE);
                                        mLoadText.setVisibility(View.GONE);
                                    }
                                });
                            }
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (mAnimaition != null) {
                                        mLoadding.clearAnimation();
                                        mAnimaition.stop();
                                    }
                                    mLoadding.setVisibility(View.GONE);
                                    mLoadImg.setVisibility(View.GONE);
                                    mLoadText.setVisibility(View.GONE);
                                } catch (Exception e) {
                                }
                            }
                        });
                    }
                }
                catch (Exception e)
                {

                }
            }
        });

        TenPos++;

//        if(TenPos%3==0)
//        {
//            int fps = getLoveFPS(mLiveNowPeople);
//            if(mLoveTimer!=null)
//            {
//                mLoveTimer.cancel();
//                mLoveTimer.purge();
//                mLoveTimer = null;
//            }
//            mLoveTimer = new Timer();
//            mLoveTimer.schedule(new TimerTask() {
//                @Override
//                public void run() {
//                    showLove();
//                }
//            }, 0, fps);
//
//            lastFPS = fps;
//        }

        if(TenPos > 30) TenPos = 0;

        if(editKeyState) editKeyState = false;

        if(mLikeCount-lastCount > 17)
        {
            mGodHand = true;
        }

        lastCount = mLikeCount;

        if(mCommentSec < 8) mCommentSec++;

//        if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1)
//        {
//            if(!mFirstView)
//            {
//                mAdShow++;
//
//                if(mAdShow > 10)
//                {
//                    if(mAdReady)
//                    {
//                        if(interstitialAd!=null)
//                        {
//                            handler.post(new Runnable()
//                            {
//                                @Override
//                                public void run()
//                                {
//                                    interstitialAd.show();
//                                    mAdReady = false;
//                                }
//                            });
//                        }
//                    }
//                }
//            }
//        }
    }

    private void regenerateLiveStreamVideoFrameCacheFolder() {
        try
        {
            File folder = new File(liveStreamVideoFrameCacheFolderPath);

            folder.mkdirs();

            // delete all cached files
            String[] children = folder.list();

            if(children!=null)
            {
                for (int i = 0; i < children.length; i++) {
                    new File(folder, children[i]).delete();
                }
            }
        }
        catch (Exception e)
        {

        }
    }

    private boolean isPad()
    {
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        float screenWidth = display.getWidth();
        float screenHeight = display.getHeight();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);
        if (screenInches >= 6.0) {
            return true;
        }
        return false;
    }

    public static boolean isTablet(Context context)
    {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    private class GridAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mGiftModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewGridHolder holder = new ViewGridHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.gift_grid_row, null);
                holder.gift_row_layout = (LinearLayout) convertView.findViewById(R.id.gift_row_layout);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.money = (TextView) convertView.findViewById(R.id.money);
                convertView.setTag(holder);
            }
            else holder = (ViewGridHolder) convertView.getTag();

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mGiftModels.get(position).getIcon()), holder.image, GiftOptions);
            holder.name.setText(mGiftModels.get(position).getName());
            holder.money.setText(String.valueOf(mGiftModels.get(position).getPoint()));

            holder.gift_row_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    mGiftLayout.setVisibility(View.GONE);

                    if(Singleton.preferences.getInt(Constants.GIFT_POINT, 0) >= mGiftModels.get(position).getPoint())
                    {
                        if(mCheckSendGiftDialog!=null) mCheckSendGiftDialog = null;
                        mCheckSendGiftDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                        mCheckSendGiftDialog.setContentView(R.layout.live_send_gift_dialog);
                        Window window = mCheckSendGiftDialog.getWindow();
                        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        mCheckSendGiftDialog.setCancelable(true);

                        TextView mTitle = (TextView) mCheckSendGiftDialog.findViewById(R.id.title);
                        ImageView mGiftIcon = (ImageView) mCheckSendGiftDialog.findViewById(R.id.gift_icon);
                        TextView mPoint = (TextView) mCheckSendGiftDialog.findViewById(R.id.point);
                        TextView mCancel = (TextView) mCheckSendGiftDialog.findViewById(R.id.cancel);
                        TextView mOk = (TextView) mCheckSendGiftDialog.findViewById(R.id.ok);

                        mTitle.setText(String.format(getString(R.string.live_check_send_gift_title), mGiftModels.get(position).getName()));
                        mGiftIcon.setImageResource(gift_loves[(int) (Math.random() * gift_loves.length)]);
                        mPoint.setText(String.valueOf(mGiftModels.get(position).getPoint()));
                        mCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mCheckSendGiftDialog.dismiss();
                            }
                        });

                        mOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mCheckSendGiftDialog.dismiss();

                                //event tracking
                                try {
                                    LogEventUtil.ClickSendGiftButton(mCtx, mApplication, Singleton.preferences.getInt(Constants.GIFT_POINT, 0));
                                    LogEventUtil.SendGiftLive(mCtx,mApplication,name,String.valueOf(liveStreamID),mGiftModels.get(position).getName(),mGiftModels.get(position).getPoint(),mGiftModels.get(position).getGiftID());
                                }
                                catch (Exception x)
                                {

                                }

                                File file = new File(Singleton.getExternalMediaFolderPath() + mGiftModels.get(position).getArchiveFileName().substring(0, mGiftModels.get(position).getArchiveFileName().length() - 4));
                                if (file.exists()) {

                                    ApiManager.sendGift(mCtx, liveStreamID, mGiftModels.get(position).getGiftID(), new ApiManager.RequestCallback() {

                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {
                                                int point = Singleton.preferences.getInt(Constants.GIFT_POINT, 0) - mGiftModels.get(position).getPoint();
                                                Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, point).commit();
                                                mGiftCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));

                                                sendGiftPoint+=mGiftModels.get(position).getPoint();

                                                LiveGiftsModel mLiveGiftsModel = new LiveGiftsModel();
                                                mLiveGiftsModel.setGiftInfo(mGiftModels.get(position));
                                                UserModel mUserModel = new UserModel();
                                                mUserModel.setOpenID(myopen);
                                                mLiveGiftsModel.setUserInfo(mUserModel);

                                                if (mLiveGiftsModels.size() == 0 || mLiveGiftsModels.size() == 1)
                                                    mLiveGiftsModels.add(mLiveGiftsModel);
                                                else {
                                                    int pos = mLiveGiftsModels.size() - 1;
                                                    for (int i = 1; i < mLiveGiftsModels.size(); i++) {
                                                        if (mLiveGiftsModel.getGiftInfo().getPoint() > mLiveGiftsModels.get(i).getGiftInfo().getPoint()) {
                                                            pos = i;
                                                            break;
                                                        }
                                                    }
                                                    mLiveGiftsModels.add(pos, mLiveGiftsModel);
                                                }

                                                if (!mGiftShowState) {
                                                    mGiftShowState = true;

                                                    File file = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0, mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length() - 4));
                                                    if (file.exists()) {
                                                        mGiftAni.removeAllViews();
                                                        GiftView mGiftView = new GiftView(mCtx, mLiveGiftsModels.get(0).getGiftInfo().getFrameDuration(), mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels, mLiveGiftsModels.get(0).getGiftInfo().getGiftID(), mLiveGiftsModels.get(0).getGiftInfo().getNumOfImages(), mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0, mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length() - 4));
                                                        mGiftView.mGiftEndListener = mCtx;
                                                        mGiftAni.addView(mGiftView);
                                                        showGiftComment(mLiveGiftsModels.get(0).getGiftInfo(), myopen, Singleton.preferences.getString(Constants.PICTURE, ""));

                                                        try {
                                                            if (mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack().length() != 0) {
                                                                File sound = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                                                if (sound.exists()) {
                                                                    if (mMp3.isPlaying()) {
                                                                        mMp3.stop();
                                                                    }

                                                                    mMp3 = null;
                                                                    mMp3 = new MediaPlayer();
                                                                    mMp3.setDataSource(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                                                    mMp3.prepare();
                                                                    mMp3.start();
                                                                }
                                                            }
                                                        } catch (IOException i) {
                                                        } catch (Exception e) {
                                                        }
                                                    } else {
                                                        try {
                                                            mGiftShowState = false;
                                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                        } catch (Exception x) {
                                                        }
                                                    }
                                                }
                                            } else {
                                                try {
                                                    mGiftLayout.setVisibility(View.GONE);
                                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                } catch (Exception x) {
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    try {
                                        mGiftLayout.setVisibility(View.GONE);
                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {
                                    }
                                }
                            }
                        });

                        mCheckSendGiftDialog.show();
                    }
                    else
                    {
                        showGiftDialg();
                    }
                }
            });

            return convertView;
        }
    }

    private class ViewGridHolder
    {
        LinearLayout gift_row_layout;
        ImageView image;
        TextView name;
        TextView money;
    }

    public class MyPhoneStateListener extends PhoneStateListener
    {
        @Override
        public void onCallStateChanged(int state, String phoneNumber)
        {
            switch (state)
            {
                case TelephonyManager.CALL_STATE_IDLE:
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    if(!mEnd) exitLive();
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    exitLive();
                    break;
                default:
                    break;
            }
        }
    }

    private void exitLive()
    {
        isReceivingData = false;
        mEnd = true;
        if(mLoveTimer!=null)
        {
            mLoveTimer.cancel();
            mLoveTimer.purge();
            mLoveTimer = null;
        }

        int quit = Singleton.getCurrentTimestamp() - mTimestamp;

        try{
            LogEventUtil.LeaveLivePage(mCtx,mApplication,user,Integer.toString(liveStreamID),quit,fullScreenCount,commentCount,mLikeCount,sendGiftPoint,enterLiveFrom);
        }catch (Exception x)
        {

        }
        commentCount = 0;
        fullScreenCount =0;
        sendGiftPoint = 0;
        Singleton.preferenceEditor.putInt(Constants.Buy_Point_Count,buyPointCount).commit();
        buyPointCount = 0;

        if(!mGodHand && !mApplication.getIsSbtools() && mLikeCount!=0)
        {
            networkSendHandler.post(new Runnable() {
                @Override
                public void run() {
                    ApiManager.likeLivestreamBatchUpdate(mCtx, user, liveStreamID, mLikeCount);
                    mLikeCount = 0;
                }
            });
        }

        ApiManager.quitViewLiveStream(mCtx, user, liveStreamID, quit, new ApiManager.QuitViewLiveStreamCallback()
        {
            @Override
            public void onResult(boolean success, String message)
            {
                hideKeyboard();
//                        mApplication.setReLoad(true);
                if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1 && !mFirstView && mAdReady && interstitialAd!=null)
                {
                    handler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            interstitialAd.show();
                            mAdReady = false;
                        }
                    });
                }
                else {
                    if(mGuest)
                    {
//                        Intent intent = new Intent();
//                        intent.setClass(mCtx, GuestActivity.class);
//                        startActivity(intent);
                        mCtx.finish();
                    }
                    else mCtx.finish();
                }
            }
        });
    }

    private IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener()
    {
        public void onIabPurchaseFinished(IabResult result,Purchase purchase)
        {
            if (result.isFailure())
            {
                try {
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }
                return;
            }
            else if (purchase.getSku().equals(mApplication.getProductModels().get(mBuyPos).getProductID()))
            {
//                Log.d("123", "111 OK");
                mHelper.queryInventoryAsync(mReceivedInventoryListener);

                //event tracking
                buyPointCount++;
                try{
                    LogEventUtil.PurchasePoint(mCtx,mApplication,mApplication.getProductModels().get(mBuyPos).getPoint(),buyPointCount);
                }catch (Exception x)
                {

                }

            }
        }
    };

    private IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener()
    {
        public void onQueryInventoryFinished(IabResult result,Inventory inventory)
        {
            if (result.isFailure())
            {
                try {
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }
//                Log.d("123", "222 error");
            }
            else
            {
//                Log.d("123", "222 OK");
                mHelper.consumeAsync(inventory.getPurchase(mApplication.getProductModels().get(mBuyPos).getProductID()),mConsumeFinishedListener);
            }
        }
    };

    private IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener()
    {
        public void onConsumeFinished(Purchase purchase,IabResult result)
        {
            if (result.isSuccess())
            {
                final String Sku = purchase.getSku();
                final String OrderId = purchase.getOrderId();
                final String Token = purchase.getToken();
                final String Payload = purchase.getDeveloperPayload();

                ApiManager.purchaseProduct(mCtx, purchase.getSku(), purchase.getOrderId(), purchase.getToken(), purchase.getDeveloperPayload(), new ApiManager.PurchaseProductCallback()
                {
                    @Override
                    public void onResult(boolean success, int point)
                    {
                        if(success)
                        {
                            try {
//                          showToast(getString(R.string.done));
                                Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                            int now = Singleton.preferences.getInt(Constants.GIFT_POINT, 0) + point;
                            Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, now).commit();
                            mGiftCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));
                        }
                        else
                        {
                            ApiManager.purchaseProduct(mCtx, Sku, OrderId, Token, Payload, new ApiManager.PurchaseProductCallback()
                            {
                                @Override
                                public void onResult(boolean success, int point)
                                {
                                    if(success)
                                    {
                                        try {
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                        int now = Singleton.preferences.getInt(Constants.GIFT_POINT, 0) + point;
                                        Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, now).commit();
                                        mGiftCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));
                                    }
                                    else
                                    {
                                        ApiManager.purchaseProduct(mCtx, Sku, OrderId, Token, Payload, new ApiManager.PurchaseProductCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success, int point)
                                            {
                                                if(success)
                                                {
                                                    try {
                                                        Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                    } catch (Exception x) {
                                                    }
                                                    int now = Singleton.preferences.getInt(Constants.GIFT_POINT, 0) + point;
                                                    Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, now).commit();
                                                    mGiftCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));
                                                }
                                                else
                                                {
                                                    payPointError();

                                                    Singleton.preferenceEditor.putString(Constants.IAP_USER, Singleton.preferences.getString(Constants.USER_ID, "")).commit();
                                                    Singleton.preferenceEditor.putString(Constants.IAP_SKU, Sku).commit();
                                                    Singleton.preferenceEditor.putString(Constants.IAP_ORDERID, OrderId).commit();
                                                    Singleton.preferenceEditor.putString(Constants.IAP_TOKEN, Token).commit();
                                                    Singleton.preferenceEditor.putString(Constants.IAP_PAYLOAD, Payload).commit();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });

//                Log.d("123", "purchase : " + purchase.toString());
//                Log.d("123", "333 ok");
            }
            else
            {
                try {
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }
//                Log.d("123", "333 error");
            }
        }
    };

    private void payPointError()
    {
        try{
            if(!isFinishing()){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.buy_point_error));
                builder.setTitle(getString(R.string.system_notif));
                builder.setPositiveButton("OK", null);
                builder.setCancelable(false);
                builder.create().show();
            }else{
                //Activity has finished and couldn't open any Dialogs
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showGiftComment(GiftModel gift, String open, String pic)
    {
//        Log.d("123","pic : " + Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + pic));
        if(gift.getPoint() >= 500)
        {
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + pic), mGiftIconPicBest, PeopleOptions);
            mGiftUserNameBest.setText(open);
            mGiftUserNameBest.setTextColor(getResources().getColor(gift_text_colors[(int) (Math.random() * gift_text_colors.length)]));
            mGiftGiftNameBest.setText(gift.getName());
            mGiftCommentBest.setVisibility(View.VISIBLE);
        }
        else
        {
            mGiftIconPic.setImageResource(gift_loves[(int) (Math.random() * gift_loves.length)]);
            mGiftUserName.setText(open);
            mGiftUserName.setTextColor(getResources().getColor(gift_text_colors[(int) (Math.random() * gift_text_colors.length)]));
            mGiftGiftName.setText(gift.getName());
            mGiftComment.setVisibility(View.VISIBLE);
        }
    }

    Dialog GuestDialog;
    private SharedPreferences sharedPreferences;
//    private CallbackManager callbackManager;
    private void showGuestLogin()
    {
        if(GuestDialog!=null) GuestDialog = null;

        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        GuestDialog.setContentView(R.layout.guest_login_dialog);
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        Button mSingup = (Button)GuestDialog.findViewById(R.id.singup);
        mSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();
                dialog_singup_view();
            }
        });

        Button mLogin = (Button)GuestDialog.findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(mCtx,LoginActivity_V3.class);
                startActivity(intent);
                mCtx.finish();
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.btnFBLogin);
        mFB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        GuestDialog.show();
    }

    private void dialog_singup_view()
    {
        if(GuestDialog!=null) GuestDialog = null;
        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        if(Constants.INTERNATIONAL_VERSION){
            GuestDialog.setContentView(R.layout.guest_singup_dialog_google);
        }
        else{
            GuestDialog.setContentView(R.layout.guest_singup_dialog_china);
        }
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout mWeibo = (LinearLayout)GuestDialog.findViewById(R.id.weibo_layout);
        mWeibo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupWeibo";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                showWebio();
            }
        });

        LinearLayout mQQ = (LinearLayout)GuestDialog.findViewById(R.id.qq_layout);
        mQQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupQQ";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                mTencent = Tencent.createInstance(Constants.QQAppid, mCtx);
                mTencent.login(mCtx, "all", loginListener);
            }
        });

        LinearLayout mWechat = (LinearLayout)GuestDialog.findViewById(R.id.wechat_layout);
        mWechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                if (ShareUtil.isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI"))
                {
                //Umeng monitor
                String Umeng_id="GuestModeSignupWechat";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                IWXAPI api;
                api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
                api.registerApp(Constants.WECHAT_APP_KEY);
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "wechat_sdk_demo";
                api.sendReq(req);
                }
                else
                {
                    Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                }
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.fb_layout);
        mFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                Intent intent;

                //Umeng monitor
                String Umeng_id_FB="FBlogin";
                HashMap<String,String> mHashMap_FB = new HashMap<String,String>();
                mHashMap_FB.put(Umeng_id_FB, Umeng_id_FB);
                MobclickAgent.onEventValue(mCtx, Umeng_id_FB, mHashMap_FB, 0);

                FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(final LoginResult loginResult) {
                                final String token = loginResult.getAccessToken().getToken();

                                GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject user, GraphResponse response) {
                                        if (user != null) {
                                            try {
                                                showProgressDialog();
                                                final String id = user.optString("id");
                                                final String name = user.optString("name");
//                                                    Log.e("DDD", "ID: " + id);

                                                // Download Profile photo
                                                DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, id);
                                                downloadFBProfileImage.execute();

                                                ApiManager.checkFacebookIDAvailable(mCtx, id, new ApiManager.checkFacebookIDAvailableCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String message) {
                                                        if (success) {

                                                            // Download user profile image
//                                                                DownloadFBProfileImage download = new DownloadFBProfileImage(mCtx, id);
//                                                                download.execute();

                                                            //Signup
                                                            GraphRequest request = GraphRequest.newMeRequest(
                                                                    loginResult.getAccessToken(),
                                                                    new GraphRequest.GraphJSONObjectCallback() {
                                                                        @Override
                                                                        public void onCompleted(
                                                                                JSONObject object,
                                                                                GraphResponse response) {
                                                                            // Application code
                                                                            String email = object.optString("email");
//                                                                                Log.e("DDD", "EMAIL: " + email);


                                                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, id).commit();
                                                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                            sharedPreferences.edit().putString("signup_type", "fb").commit();
                                                                            sharedPreferences.edit().putString("signup_token", token).commit();

                                                                            if (null != email) {
                                                                                if (email.contains("@")) {
                                                                                    String username = email.substring(0, email.indexOf("@"));
                                                                                    sharedPreferences.edit().putString(Constants.EMAIL, email).commit();
                                                                                    sharedPreferences.edit().putString(Constants.NAME, username).commit();
                                                                                }
                                                                            }
                                                                            hideProgressDialog();
                                                                            startActivity(new Intent(mCtx, SignupActivityV2.class));
                                                                            mCtx.finish();
                                                                        }
                                                                    });

                                                            Bundle parameters = new Bundle();
                                                            parameters.putString("fields", "email");
                                                            request.setParameters(parameters);
                                                            request.executeAsync();

                                                        } else {
                                                            //login directly
                                                            ApiManager.loginAction2(mCtx, "", "", id, new ApiManager.LoginAction2Callback() {
                                                                @Override
                                                                public void onResult(boolean success, String message, UserModel user) {
                                                                    hideProgressDialog();

                                                                    if (success) {
                                                                        if (message.equals("ok")) {

                                                                            Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                            mStory17Application.setUser(user);

                                                                            Intent intent = new Intent();
                                                                            intent.setClass(mCtx, MenuActivity.class);
                                                                            startActivity(intent);
                                                                            mCtx.finish();
                                                                        } else if (message.equals("freezed"))
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                        else
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                    } else {
                                                                        showNetworkUnstableToast();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });


                                            } catch (Exception e) {
                                                hideProgressDialog();
                                                try {
//                              showToast(getString(R.string.failed));
                                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                } catch (Exception x) {
                                                }
                                            }
                                        }
                                    }
                                }).executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        });
            }
        });

        LinearLayout mMessage = (LinearLayout)GuestDialog.findViewById(R.id.messag_layout);
        mMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupSMS";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                sharedPreferences.edit().putString("signup_type", "message").commit();
                sharedPreferences.edit().putString("signup_token", "").commit();
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(mCtx, SignupActivityV2.class);
                startActivity(intent);
                mCtx.finish();
            }
        });

        Button mEnd = (Button)GuestDialog.findViewById(R.id.end);
        mEnd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();

            }
        });

        GuestDialog.show();
    }

    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        private final String fbImageDownlaodUrl = "https://graph.facebook.com/";
        private final String attrs = "/picture?width=1080&height=1080";
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

            this.url = fbImageDownlaodUrl + userID + attrs;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {

        }

    }

    private Dialog mWeiboDialog;
    private void showWebio()
    {
        if(mWeiboDialog!=null) mWeiboDialog = null;
        mWeiboDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        mWeiboDialog.setContentView(R.layout.instagram_dialog);
        final WebView mWeb = (WebView) mWeiboDialog.findViewById(R.id.web);
        final ProgressBar mProgress = (ProgressBar) mWeiboDialog.findViewById(R.id.progress);
        mWeb.getSettings().setJavaScriptEnabled(true);
        mWeiboDialog.setCancelable(true);
        mWeb.loadUrl("https://api.weibo.com/oauth2/authorize?client_id=" + Constants.WEIBO_APP_KEY + "&response_type=code&redirect_uri=" + Constants.WEIBO_REDIRECT_URL);
        mWeb.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                super.onPageStarted(view, url, favicon);
                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);

                if(url.contains(Constants.IG_REDIRECT_URI) && url.contains("code="))
                {
                    mWeiboDialog.dismiss();
                    showProgressDialog();

                    int pos = url.indexOf("=") + 1;
                    String token = url.substring(pos,url.length());
                    try
                    {
                        OkHttpClient mClient = new OkHttpClient();

                        RequestBody body = new FormEncodingBuilder()
                                .add("client_id", Constants.WEIBO_APP_KEY)
                                .add("client_secret", Constants.WEIBO_SECRET)
                                .add("grant_type", "authorization_code")
                                .add("code", token)
                                .add("redirect_uri", Constants.WEIBO_REDIRECT_URL)
                                .build();

                        Request request = new Request.Builder().url("https://api.weibo.com/oauth2/access_token").post(body).build();
                        mClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful())
                                {
                                    try
                                    {
                                        JSONObject data = new JSONObject(response.body().string());
                                        final String token = data.getString("access_token");
                                        final String uid = data.getString("uid");

                                        OkHttpClient client = new OkHttpClient();
                                        Request request = new Request.Builder().url("https://api.weibo.com/2/users/show.json?access_token=" + token + "&uid=" + uid).build();
                                        client.newCall(request).enqueue(new Callback()
                                        {
                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                hideProgressDialog();
                                                showToast(getString(R.string.failed));
                                            }

                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if(response.isSuccessful())
                                                {
                                                    try
                                                    {
                                                        JSONObject info = new JSONObject(response.body().string());
                                                        final int id = info.getInt("id");
                                                        final String screen_name = info.getString("screen_name");
                                                        final String pic = info.getString("profile_image_url");

                                                        DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                                        downloadFBProfileImage.execute();

                                                        ApiManager.checkWeiboIDAvailable(mCtx, uid/*String.valueOf(id)*/, new ApiManager.checkFacebookIDAvailableCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                if (success) {
                                                                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, String.valueOf(id)).commit();
                                                                    sharedPreferences.edit().putString(Constants.FULL_NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putString(Constants.NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                    sharedPreferences.edit().putString("is_weibo", "weibo").commit();

                                                                    sharedPreferences.edit().putString("signup_type", "weibo").commit();
                                                                    sharedPreferences.edit().putString("signup_token", token).commit();

                                                                    hideProgressDialog();
                                                                    Intent intent = new Intent();
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                    intent.setClass(mCtx, SignupActivityV2.class);
                                                                    startActivity(intent);
                                                                    mCtx.finish();
                                                                } else {
                                                                    ApiManager.loginActionChina(mCtx, "", "", "weiboID", uid, new ApiManager.LoginAction2Callback() {
                                                                        @Override
                                                                        public void onResult(boolean success, String message, UserModel user) {
                                                                            hideProgressDialog();

                                                                            if (success) {
                                                                                if (message.equals("ok")) {

                                                                                    mApplication.setUser(user);

                                                                                    Intent intent = new Intent();
                                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                                    intent.setClass(mCtx, MenuActivity.class);
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                } else if (message.equals("freezed"))
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                                else
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                            } else {
                                                                                showNetworkUnstableToast();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                    catch (JSONException e)
                                                    {
                                                        hideProgressDialog();
                                                        showToast(getString(R.string.failed));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    catch (JSONException e)
                                    {
                                        hideProgressDialog();
                                        showToast(getString(R.string.failed));
                                    }
                                }
                                else
                                {
                                    hideProgressDialog();
                                    showToast(getString(R.string.failed));
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        hideProgressDialog();
                        showToast(getString(R.string.failed));
                    }
                }
                else
                {
//                            mWeiboDialog.dismiss();
//                            hideProgressDialog();
//                            showToast(getString(R.string.failed));
                }
            }
        });
        try{
            mWeb.clearCache(true);
            mWeb.getSettings().setCacheMode(WebView.PERSISTENT_NO_CACHE);
        }
        catch (Exception e){
        }
        mWeiboDialog.show();
    }

    //QQ-------

    public static Tencent mTencent;

    private class BaseUiListener implements IUiListener
    {

        @Override
        public void onComplete(Object response) {
            if (null == response) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            if (null != jsonResponse && jsonResponse.length() == 0) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }

            doComplete((JSONObject)response);
        }

        protected void doComplete(JSONObject values) {

        }

        @Override
        public void onError(UiError e) {
            hideProgressDialog();
            showToast(getString(R.string.failed));
        }

        @Override
        public void onCancel() {
            hideProgressDialog();
        }
    }

    private IUiListener loginListener = new BaseUiListener()
    {
        @Override
        protected void doComplete(JSONObject jsonObject)
        {
            try
            {
                final String token = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_ACCESS_TOKEN);
                String expires = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_EXPIRES_IN);
                final String openId = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_OPEN_ID);

                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);

                if (mTencent != null && mTencent.isSessionValid())
                {
                    IUiListener listener = new IUiListener()
                    {
                        @Override
                        public void onError(UiError e) {
                            hideProgressDialog();
                            showToast(getString(R.string.failed));
                        }

                        @Override
                        public void onComplete(final Object response)
                        {
                            try
                            {
                                JSONObject json = (JSONObject)response;

                                final String name = json.getString("nickname");
                                String pic = json.getString("figureurl_qq_1");

                                DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                downloadFBProfileImage.execute();

                                ApiManager.checkQQIDAvailable(mCtx, openId, new ApiManager.checkFacebookIDAvailableCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, openId).commit();
                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                            sharedPreferences.edit().putString(Constants.NAME, name).commit();
                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();
                                            sharedPreferences.edit().putString("signup_type", "qq").commit();
                                            sharedPreferences.edit().putString("signup_token", token).commit();
                                            hideProgressDialog();
                                            Intent intent = new Intent();
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setClass(mCtx, SignupActivityV2.class);
                                            startActivity(intent);
                                            mCtx.finish();
                                        }
                                        else
                                        {
                                            ApiManager.loginActionChina(mCtx, "", "", "qqID", openId, new ApiManager.LoginAction2Callback() {
                                                @Override
                                                public void onResult(boolean success, String message, UserModel user) {
                                                    hideProgressDialog();

                                                    if (success) {
                                                        if (message.equals("ok")) {

                                                            mApplication.setUser(user);

                                                            Intent intent = new Intent();
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            intent.setClass(mCtx, MenuActivity.class);
                                                            startActivity(intent);
                                                            mCtx.finish();
                                                        } else if (message.equals("freezed"))
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                        else
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                    } else {
                                                        showNetworkUnstableToast();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }
                        }

                        @Override
                        public void onCancel() {
                            hideProgressDialog();
                        }
                    };

                    UserInfo mInfo = new UserInfo(mCtx, mTencent.getQQToken());
                    mInfo.getUserInfo(listener);
                }
            }
            catch(Exception e) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
            }
        }
    };

    class DownloadProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        String url = "";

        public DownloadProfileImage(Context context, String userID) {
            this.context = context;

            this.url = userID.substring(0,userID.length()-2) + 640;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {
//                    java.net.URL url = new java.net.URL(this.url);
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setDoInput(true);
//                    connection.connect();

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }
    }

    float GetSentGiftPercentage (int share)
    {
        percentage = (float)share / (float)LiveLeaderboardtotal;
        
        //如果90.00%, 我們要把它轉換成90%
        if(((percentage*100) - (int)(percentage*100)) == 0 || ((percentage*100) - (int)(percentage*100)) < 0.0001) {
            //這裡設定要顯示小數點後幾位，如設成1就位顯示到小數點後一位 90.1%
            nt.setMinimumFractionDigits(0);
        } else {
            nt.setMinimumFractionDigits(2);
        }
        return percentage;
    }

    /**
     * @author Zack
     * @since 2016.02.19
     * To retry 3 times during 8 secs
     */
    private class CommentRetryThread extends Thread {

        private int retryMaxTimes = 3;
        private int retryMaxSec = 8 * 1000; //8sec

        private int retry = 1;
        private int nowRetry = 0;

        private String commentStr = "";
        private long lastAbsTime;
        private String mColorCode = "";

        private boolean isRunning = true;
        private boolean isSuccess;

        public CommentRetryThread(String commentStr, String mColorCode, long lastAbsTime) {
            this.commentStr = commentStr;
            this.lastAbsTime = lastAbsTime;
            this.mColorCode = mColorCode;
        }

        @Override
        public void run() {
            try {
                long nowTime = System.currentTimeMillis();

                while (isRunning && retry <= retryMaxTimes && (System.currentTimeMillis() - nowTime) < retryMaxSec) {
                    if (nowRetry == retry) {
                        sleep(500);
                    } else {
                        nowRetry = retry;
                        ApiManager.commentLiveStream(mCtx, user, liveStreamID, commentStr, mColorCode, (int) lastAbsTime, new ApiManager.CommentLiveStreamCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {
                                    isSuccess = success;
                                    isRunning = false;
                                } else {
                                    retry++;
                                }
                            }
                        });
                        sleep(2000);
                    }
                }

                //Log.d("CommentRetryThread", "nowRetry="+nowRetry+", isSuccess="+isSuccess);

                if (!isSuccess) {
                    mCtx.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mCtx, getString(R.string.upload_error), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void destroyProcess() {
            isRunning = false;
        }
    }
}
