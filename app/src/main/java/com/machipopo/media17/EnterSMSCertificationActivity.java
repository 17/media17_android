package com.machipopo.media17;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.telephony.gsm.SmsMessage;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.geetest.gt_sdk.GeetestLib;
import com.geetest.gt_sdk.GtDialog;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.DevUtils;
import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class  EnterSMSCertificationActivity extends BaseActivity {

    private EnterSMSCertificationActivity mCtx = this;
    private EditText mSMSCode;
    private Button mConfirm;
    private TextView mResendSMS;
    private TextView mBindingPhone;
    private CountDownTimer countDownTimer;
    private TextView mCountDownTimer;
    private LinearLayout mDialog;
    private TextView mConfirmNumber, mCancel,mDoubleConfirm;
    private View mBackground;
    private String SMSkey;
    private int SMS,Binding;
    private int isBroadcastRegister;  //record if SMS broadcast is registered
    private SharedPreferences sharedPreferences;
    private int isChangePhoneNumber;
    public final static String tag = "signup_setting";
    private GeetestLib gt = new GeetestLib();
    private String IPRequestCountRecord;
    private ProgressBar mProgress;
    private String type;
    private String signupType;
    private RelativeLayout mCountDownTimerLayout;
    private String timerText;

    // 设置获取id，challenge，success的URL，需替换成自己的服务器URL
    private String captchaURL = Constants.SERVER_IP + "getHumanTestParams";

    // 设置二次验证的URL，需替换成自己的服务器URL
    private String validateURL = Constants.SERVER_IP + "validateHumanTestResponse";

    //parse verification code from SMS
    private final static String MSG_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    static
    {
        System.loadLibrary("17media");
    }

    public class DataProvider
    {
        public native String sayHellolnC(String s);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_verification);
        mSMSCode = (EditText) findViewById(R.id.enterSMS);
        mConfirm = (Button) findViewById(R.id.confirm);
        mResendSMS = (TextView) findViewById(R.id.resendsms);
        mCountDownTimer =(TextView) findViewById(R.id.count_down_timer);
        mConfirmNumber= (TextView) findViewById(R.id.confirm_number);
        mDoubleConfirm= (TextView) findViewById(R.id.double_confirm);
        mCancel= (TextView) findViewById(R.id.cancel);
        mBackground = (View) findViewById(R.id.background);
        mDialog = (LinearLayout) findViewById(R.id.dialog);
        mBindingPhone = (TextView) findViewById(R.id.binding_phone);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mCountDownTimerLayout = (RelativeLayout) findViewById(R.id.count_down_timer_layout);

        mBindingPhone.setText("+ "+Singleton.preferences.getString(Constants.PROFILE_COUNTRY_CODE, "") + " " + Singleton.preferences.getString(Constants.PROFILE_PHONE, ""));
        sharedPreferences = getSharedPreferences(tag, 0);

        gt.setCaptchaURL(captchaURL);
        gt.setValidateURL(validateURL);

        final Intent intent = new Intent();
        final Bundle bundle = mCtx.getIntent().getExtras();
        SMS = bundle.getInt("SMS");
        Binding =bundle.getInt("Binding");

        if (SMS == 1)
        {
            type="resetPassword";
        }
        else if((SMS == 0) && Binding == 0)
        {
            type="register";

            //log last step that user start quiting registration
            if(SingupLastStepLog.getInstance().getmSignupLastStep().ordinal() < SingupLastStepLog.SignupLastStep.SMScode.ordinal()) {
                SingupLastStepLog.getInstance().setmSignupLastStep(SingupLastStepLog.SignupLastStep.SMScode);
            }

        }
        else
        {
            type="changePhoneNumber";
        }

        registerReceiver(mBroadcastReceiver, new IntentFilter(MSG_RECEIVED));
        isBroadcastRegister =1;

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCancel.getText().toString().length() > 0) {

                    //unregister the receiver to avoid leak
                    if (isBroadcastRegister != 0) {
                        unregisterReceiver(mBroadcastReceiver);
                        isBroadcastRegister = 0;
                    }

                    mProgress.setVisibility(View.VISIBLE);

                    hideKeyboard();
                    if (SMS == 0) {
                        if (Binding == 0) {
                            ApiManager.registerByPhoneAction(mCtx, sharedPreferences.getString(Constants.NAME, ""), sharedPreferences.getString(Constants.PASSWORD, ""), Singleton.preferences.getString(Constants.PROFILE_COUNTRY_ABBREVIATE, ""), Singleton.preferences.getString(Constants.PROFILE_COUNTRY_CODE, ""), Singleton.preferences.getString(Constants.PROFILE_PHONE, ""), mSMSCode.getText().toString(),
                                    sharedPreferences.getString("signup_type", ""),sharedPreferences.getString("signup_token", ""),sharedPreferences.getString(Constants.FACEBOOK_ID, ""),new ApiManager.RegisterByPhoneActionCallback() {
                                @Override
                                public void onResult(boolean success, String result, String message, UserModel user) {
                                    mProgress.setVisibility(View.GONE);
                                    if (success) {
                                        try {
                                            if (result.equals("success") || user != null) {

                                                try{
                                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                    mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                                                    mHashMap.put("version", Singleton.getVersion());
                                                    mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                                                    mHashMap.put("u", Singleton.getPhoneIMEI());
                                                    mHashMap.put("dn", DevUtils.getDevInfo());
                                                    LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_success", mHashMap);
                                                }
                                                catch (Exception e){
                                                }

                                                //event tracking
                                                String Umeng_id;
                                                signupType = sharedPreferences.getString("signup_type", "");

//                                                Log.d("17_gift","guest signup type"+signupType);

                                                try{
                                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                    mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                                                    mHashMap.put("version", Singleton.getVersion());
                                                    mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                                                    mHashMap.put("u", Singleton.getPhoneIMEI());
                                                    mHashMap.put("dn", DevUtils.getDevInfo());
                                                    LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_success_type_" + signupType, mHashMap);
                                                }
                                                catch (Exception e){
                                                }


                                                if(signupType.equals("message"))
                                                {
                                                    //Umeng monitor
                                                    Umeng_id="GuestModeSignupSMSSuccess";
                                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                    mHashMap.put(Umeng_id, Umeng_id);
                                                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                                                }
                                                else if (signupType.equals("qq"))
                                                {
                                                    //Umeng monitor
                                                    Umeng_id="GuestModeSignupQQSuccess";
                                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                    mHashMap.put(Umeng_id, Umeng_id);
                                                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                                                }
                                                else if (signupType.equals("weibo"))
                                                {
                                                    //Umeng monitor
                                                    Umeng_id="GuestModeSignupWeiboSuccess";
                                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                    mHashMap.put(Umeng_id, Umeng_id);
                                                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                                                }
                                                else if (signupType.equals("wechat"))
                                                {
                                                    //Umeng monitor
                                                    Umeng_id="GuestModeSignupWechatSuccess";
                                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                    mHashMap.put(Umeng_id, Umeng_id);
                                                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                                                }


                                                Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent();
                                                intent.setClass(mCtx, SignupFullnameActivityV2.class);
                                                Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                mStory17Application.setUser(user);
                                                startActivity(intent);
                                                mCtx.finish();

                                            } else {

                                                if (message.equals("verificationCode_timeout")) {
                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_timeout), Toast.LENGTH_SHORT).show();
                                                } else if (message.equals("verificationCode_used")) {
                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_used), Toast.LENGTH_SHORT).show();
                                                } else if (message.equals("verificationCode_error")) {
                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_error), Toast.LENGTH_SHORT).show();
                                                }
                                                else if(message.equals("openID_exists")){
                                                    Toast.makeText(mCtx,getString(R.string.account_same),Toast.LENGTH_SHORT).show();
                                                }
                                                else if(message.equals("maximum_count_exceeds"))
                                                {
                                                    Toast.makeText(mCtx,getString(R.string.maximum_count_exceeds),Toast.LENGTH_SHORT).show();
                                                }
                                                else {
                                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        } catch (Exception x) {
                                        }
                                    } else {
                                        try {
                                            //                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        } else {
                            ApiManager.changePhoneNumber(mCtx, Singleton.preferences.getString(Constants.OPEN_ID, ""), Singleton.preferences.getString(Constants.PROFILE_COUNTRY_ABBREVIATE, ""), Singleton.preferences.getString(Constants.PROFILE_COUNTRY_CODE, ""), Singleton.preferences.getString(Constants.PROFILE_PHONE, ""), mSMSCode.getText().toString(), new ApiManager.ChangePhoneNumberCallback() {
                                @Override
                                public void onResult(boolean success, String result, String message) {
                                    mProgress.setVisibility(View.GONE);
                                    if (success) {
                                        try {
                                            if (result.equals("success")) {
                                                hideKeyboard();

                                                try{
                                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                    mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                                                    mHashMap.put("version", Singleton.getVersion());
                                                    mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                                                    mHashMap.put("u", Singleton.getPhoneIMEI());
                                                    mHashMap.put("dn", DevUtils.getDevInfo());
                                                    LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_change_phone_number" + signupType, mHashMap);
                                                }
                                                catch (Exception e){
                                                }

                                                //Fix bug that rebinding phone page still show old phone number after rebinding. 
                                                Singleton.preferenceEditor.putString(Constants.LOCAL_PHONE_NUMBER,Singleton.preferences.getString(Constants.PROFILE_PHONE, "")).commit();
                                                Singleton.preferenceEditor.putString(Constants.COUNTRY_CALLING_CODE,Singleton.preferences.getString(Constants.PROFILE_COUNTRY_CODE, "")).commit();

                                                Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent();
                                                intent.setClass(mCtx, SettingActivity.class);
                                                startActivity(intent);
                                                mCtx.finish();

                                            } else {

//                                                Log.d("17_g", "registerbyphone result failure");

                                                if (message.equals("verificationCode_timeout")) {
                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_timeout), Toast.LENGTH_SHORT).show();
                                                } else if (message.equals("verificationCode_used")) {
                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_used), Toast.LENGTH_SHORT).show();
                                                } else if (message.equals("verificationCode_error")) {
                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_error), Toast.LENGTH_SHORT).show();
                                                } else if(message.equals("maximum_count_exceeds"))
                                                {
                                                    Toast.makeText(mCtx,getString(R.string.maximum_count_exceeds),Toast.LENGTH_SHORT).show();
                                                }
                                                else {
                                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        } catch (Exception x) {
                                        }
                                    } else {
                                        try {
                                            //                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                }
                            });
                        }
                    } else {
                        IPRequestCountRecord = "getResetPasswordToken";
                        ApiManager.checkNeedHumanTest(mCtx, IPRequestCountRecord, new ApiManager.CheckNeedHumanTestCallback() {
                            @Override
                            public void onResult(boolean success, String result, String isNeeded) {

                                if (success) {
                                    try {
                                        if (isNeeded.equals("no"))
                                        {
                                            ApiManager.getResetPasswordToken(mCtx, sharedPreferences.getString(Constants.NAME, ""), Singleton.preferences.getString(Constants.PROFILE_COUNTRY_CODE, ""), Singleton.preferences.getString(Constants.PROFILE_PHONE, ""), mSMSCode.getText().toString(), new ApiManager.GetResetPasswordTokenCallback() {
                                                @Override
                                                public void onResult(boolean success, String result, String message, String resetPasswordToken) {
                                                    mProgress.setVisibility(View.GONE);
                                                    if (success) {
                                                        try {

                                                            try{
                                                                HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                                mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                                                                mHashMap.put("version", Singleton.getVersion());
                                                                mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                                                                mHashMap.put("u", Singleton.getPhoneIMEI());
                                                                mHashMap.put("dn", DevUtils.getDevInfo());
                                                                LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_reset_password_token" + signupType, mHashMap);
                                                            }
                                                            catch (Exception e){
                                                            }

                                                            if (result.equals("success")) {
                                                                Intent intent = new Intent();
                                                                intent.setClass(mCtx, SmsSetupNewPasswordActivity.class);
                                                                intent.putExtra("resetPasswordToken", resetPasswordToken);
                                                                startActivity(intent);
                                                                mCtx.finish();
                                                            } else {
                                                                if (message.equals("verificationCode_timeout")) {
                                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_timeout), Toast.LENGTH_SHORT).show();
                                                                } else if (message.equals("verificationCode_used")) {
                                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_used), Toast.LENGTH_SHORT).show();
                                                                } else if (message.equals("verificationCode_error")) {
                                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_error), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else if((message.equals("phoneNumber_error")) || message.equals("openID_not_existed")||message.equals("openID_phoneNumber_not_paired"))
                                                                {
                                                                    Toast.makeText(mCtx, getString(R.string.phoneNumber_error), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else {
                                                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                                }
                                                            }

                                                        } catch (Exception x) {
                                                        }

                                                    } else {
                                                        try {
                                                            mProgress.setVisibility(View.GONE);
                                                            Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                        } catch (Exception x) {
                                                        }
                                                    }
                                                }
                                            });
                                        } else {
//                                            Log.d("17_g", "reset token and need test isNeeded" + isNeeded);
                                            //IPRequestCountRecord = "sendPhoneVerificationCode";
                                            mDialog.setVisibility(View.INVISIBLE);
                                            mProgress.setVisibility(View.GONE);
                                            GtAppDlgTask gtAppDlgTask = new GtAppDlgTask();
                                            gtAppDlgTask.execute();

                                        }
                                    } catch (Exception x) {
                                    }
                                } else {
                                    try {
                                        mProgress.setVisibility(View.GONE);
                                        //                          showToast(getString(R.string.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {

                                    }
                                }
                            }
                        });
                    }

                }
            }
        });

        //倒數60秒
        countDownTimer = new CountDownTimer(60000, 1000) {
            //每秒鐘執行一次 onTick
            public void onTick(long millisUntilFinished) {
                int min,second;
                min =(int) (millisUntilFinished / 1000)/60; //轉換成分鐘
                second = ((int) (millisUntilFinished / 1000))% 60; //轉換成秒
                if(second >=10)
                {
                    timerText = String.valueOf(min)+" : "+String.valueOf(second);
                }
                else
                {
                    timerText = String.valueOf(min)+" : 0"+String.valueOf(second);
                }
                mCountDownTimer.setText(String.format(getString(R.string.count_down_timer),timerText));
            }
            //60秒完成之後，執行onFinish
            public void onFinish() {

                mCountDownTimerLayout.setVisibility(View.GONE);
                mResendSMS.setVisibility(View.VISIBLE);

                //加字的底線
                mResendSMS.setPaintFlags(mResendSMS.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                mResendSMS.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DataProvider dp = new DataProvider();
                        SMSkey= dp.sayHellolnC("");

                        //unregister the receiver to avoid leak
                        if(isBroadcastRegister != 1) {
                            registerReceiver(mBroadcastReceiver, new IntentFilter(MSG_RECEIVED));
                            isBroadcastRegister =1;
                        }

                        IPRequestCountRecord = "sendPhoneVerificationCode";
                        mProgress.setVisibility(View.VISIBLE);

                        ApiManager.checkNeedHumanTest(mCtx,IPRequestCountRecord, new ApiManager.CheckNeedHumanTestCallback() {
                            @Override
                            public void onResult(boolean success, String result,String isNeeded) {

                                if (success) {
                                    try {
                                        if (isNeeded.equals("no")) {
                                            ApiManager.sendPhoneVerificationCode(mCtx, Singleton.preferences.getString(Constants.PROFILE_COUNTRY_CODE, ""), Singleton.preferences.getString(Constants.PROFILE_PHONE, ""), SMSkey,type,sharedPreferences.getString(Constants.NAME, ""), new ApiManager.SendPhoneVerificationInfoCallback() {
                                                @Override
                                                public void onResult(boolean success, String result, String message) {
                                                    if (success) {
                                                        try {
                                                            if (result.equals("success")) {

                                                                try{
                                                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                                    mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                                                                    mHashMap.put("version", Singleton.getVersion());
                                                                    mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                                                                    mHashMap.put("u", Singleton.getPhoneIMEI());
                                                                    mHashMap.put("dn", DevUtils.getDevInfo());
                                                                    LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_send_phone_verification_code" + signupType, mHashMap);
                                                                }
                                                                catch (Exception e){
                                                                }

                                                                countDownTimer.start();
                                                                mResendSMS.setVisibility(View.GONE);
                                                                mCountDownTimerLayout.setVisibility(View.VISIBLE);
                                                                mProgress.setVisibility(View.GONE);

                                                                Toast.makeText(mCtx, getString(R.string.sms_sending), Toast.LENGTH_SHORT).show();
                                                            } else {
                                                                mProgress.setVisibility(View.GONE);
                                                                if (message.equals("nexmo_error")) {
                                                                    Toast.makeText(mCtx, getString(R.string.nexmo_error), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else if (message.equals("phoneNumber_used")) {
                                                                    Toast.makeText(mCtx, getString(R.string.phone_is_registered), Toast.LENGTH_SHORT).show();
                                                                } else if (message.equals("phoneNumber_error") || message.equals("openID_phoneNumber_not_paired") ) {
                                                                    Toast.makeText(mCtx, getString(R.string.phoneNumber_error), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else {
                                                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        } catch (Exception x) {
                                                        }
                                                    } else {
                                                        try {
                                                            //                          showToast(getString(R.string.error_failed));
                                                            mProgress.setVisibility(View.GONE);
                                                            Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                        } catch (Exception x) {
                                                        }
                                                    }
                                                }
                                            });
                                        } else {
                                            //IPRequestCountRecord = "sendPhoneVerificationCode";
                                            mProgress.setVisibility(View.GONE);
                                            mDialog.setVisibility(View.INVISIBLE);
                                            GtAppDlgTask gtAppDlgTask = new GtAppDlgTask();
                                            gtAppDlgTask.execute();

                                        }
                                    } catch (Exception x) {
                                    }
                                } else {
                                    try {
                                        //                          showToast(getString(R.string.error_failed));
                                        mProgress.setVisibility(View.GONE);
                                        Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {

                                    }
                                }
                            }
                        });
                    }
                });
            }
        }.start();

        initTitleBar();
        showKeyboard();

        try{
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
            mHashMap.put("version", Singleton.getVersion());
            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
            mHashMap.put("u", Singleton.getPhoneIMEI());
            mHashMap.put("dn", DevUtils.getDevInfo());
            LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_enter_smsc_activity", mHashMap);
        }
        catch (Exception e){
        }
    }

    private void initTitleBar()
    {
        final Intent intent = new Intent();
        final Bundle bundle = mCtx.getIntent().getExtras();
        int Binding;
        Binding = bundle.getInt("Binding");

        //根據Binding判斷是有是由setting or login進入此activity，改變title bar的顏色
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.enter_sms_code));
        ImageView img = (ImageView) findViewById(R.id.img_left);
        if(Binding == 0)
        {
            img.setImageResource(R.drawable.nav_arrow_back_black);
            ((TextView) findViewById(R.id.title_name)).setTextColor(Color.BLACK);
            ((RelativeLayout)findViewById(R.id.title_bar_layout)).setBackgroundColor(Color.WHITE);
        }
        else
        {
            img.setImageResource(R.drawable.nav_arrow_white_back);
            ((RelativeLayout)findViewById(R.id.title_bar_layout)).setBackgroundColor(Color.BLACK);
            ((TextView) findViewById(R.id.title_name)).setTextColor(Color.WHITE);
            ((RelativeLayout) findViewById(R.id.background_color)).setBackgroundColor(Color.WHITE);
        }

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.INVISIBLE);

        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                mDialog.setVisibility(View.VISIBLE);
                mBackground.setVisibility(View.VISIBLE);
                ((View) findViewById(R.id.title_bar_background)).setVisibility(View.VISIBLE);

                mCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.setVisibility(View.INVISIBLE);
                        mBackground.setVisibility(View.INVISIBLE);
                        ((View) findViewById(R.id.title_bar_background)).setVisibility(View.INVISIBLE);
                    }

                });

                mDoubleConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mProgress.setVisibility(View.GONE);

                        //unregister the receiver to avoid leak
                        if(isBroadcastRegister != 0) {
                            unregisterReceiver(mBroadcastReceiver);
                            isBroadcastRegister =0;
                        }

                        hideKeyboard();
                        intent.putExtra("Binding", bundle.getInt("Binding")); //紀錄是從settingactivity or signupactivityV2 跳到bindingphonenumberactivity,避免crash
                        SMS = bundle.getInt("SMS");
                        if (SMS == 1) {
                            intent.setClass(mCtx, ForgotPasswordActivity_V2.class);
                        } else {
                            intent.setClass(mCtx, BindingPhoneNumberActivity.class);
                        }
                        startActivity(intent);
                        mCtx.finish();
                    }
                });
            }
        });
    }

    public String getConfig(String key, String def)
    {
        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
        return settings.getString(key, def);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context arg0, Intent intent) {
            if(intent.getAction().equals(MSG_RECEIVED)){
                Bundle msg = intent.getExtras();
                String code;
                Object[] messages = (Object[]) msg.get("pdus");
                SmsMessage sms = SmsMessage.createFromPdu((byte[])messages[0]);

                try{
                    //冒號下一位是空格，取空格後6位
                    int pos = sms.getMessageBody().lastIndexOf(":");
                    if(sms.getMessageBody().substring(pos+1,pos+2).compareTo(" ")==0) {
                        code = sms.getMessageBody().substring((pos + 2), (pos + 8));
                    }
                    else{
                        code = sms.getMessageBody().substring((pos + 1), (pos + 7));
                    }
                    mSMSCode.setText(code);
                }
                catch (Exception e){

                }
            }
        }
    };

    class GtAppDlgTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            return gt.startCaptcha();
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if (result) {

                openGtTest(mCtx, gt.getCaptcha(), gt.getChallenge(), true);

            } else {
                // 极验服务器暂时性宕机：

                Toast.makeText(
                        getBaseContext(),
                        getString(R.string.connet_erroe),
                        Toast.LENGTH_LONG).show();


//                Toast.makeText(
//                        getBaseContext(),
//                        R.string.hs__network_error_msg,
//                        Toast.LENGTH_LONG).show();

                // 1. 可以选择继续使用极验，去掉下行注释
                // openGtTest(context, gt.getCaptcha(), gt.getChallenge(), false);

                // 2. 使用自己的验证
            }
        }
    }

    public void openGtTest(Context ctx, String captcha, String challenge, boolean success) {

        GtDialog dialog = new GtDialog(ctx, captcha, challenge, success);

        // 启用debug可以在webview上看到验证过程的一些数据
        // dialog.setDebug(true);


        dialog.setGtListener(new GtDialog.GtListener() {

            @Override
            public void gtResult(boolean success, String result) {

                if (success) {

//                    toastMsg("client captcha succeed:" + result);

                    try {
                        JSONObject res_json = new JSONObject(result);

                        ApiManager.validateHumanTestResponse(mCtx ,res_json.getString("geetest_challenge"), res_json.getString("geetest_validate"), res_json.getString("geetest_seccode"),IPRequestCountRecord, new ApiManager.ValidateHumanTestResponseCallback() {
                            @Override
                            public void onResult(boolean success, String result)
                            {
                                if (success) {
                                    try {
                                        if (result.equals("success")) {

                                            //如果getresetpasswordtoken遇到ip rate limit, 做完geetest後就在搓一次getresetpasswordtoken
                                            if(IPRequestCountRecord.equals("getResetPasswordToken"))
                                            {
                                                ApiManager.getResetPasswordToken(mCtx, sharedPreferences.getString(Constants.NAME, ""), Singleton.preferences.getString(Constants.PROFILE_COUNTRY_CODE, ""), Singleton.preferences.getString(Constants.PROFILE_PHONE, ""), mSMSCode.getText().toString(), new ApiManager.GetResetPasswordTokenCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String result, String message, String resetPasswordToken) {
                                                    if (success) {
                                                        try {
                                                            if (result.equals("success"))
                                                            {
                                                                Intent intent = new Intent();
                                                                intent.setClass(mCtx, SmsSetupNewPasswordActivity.class);
                                                                intent.putExtra("resetPasswordToken", resetPasswordToken);
                                                                startActivity(intent);
                                                                mCtx.finish();
                                                            }
                                                            else
                                                            {
                                                                if (message.equals("verificationCode_timeout"))
                                                                {
                                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_timeout), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else if (message.equals("verificationCode_used"))
                                                                {
                                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_used), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else if (message.equals("verificationCode_error"))
                                                                {
                                                                    Toast.makeText(mCtx, getString(R.string.verificationCode_error), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else if((message.equals("phoneNumber_error")) || message.equals("openID_not_existed") ||message.equals("openID_phoneNumber_not_paired"))
                                                                {
                                                                    Toast.makeText(mCtx, getString(R.string.phoneNumber_error), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else
                                                                {
                                                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                                }
                                                            }

                                                        } catch (Exception x) {
                                                        }

                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                        }
                                                        catch (Exception x)
                                                        {
                                                        }
                                                    }
                                                    }
                                                });

                                            }
                                            else
                                            {
                                                ApiManager.sendPhoneVerificationCode(mCtx, Singleton.preferences.getString(Constants.PROFILE_COUNTRY_CODE, ""), Singleton.preferences.getString(Constants.PROFILE_PHONE, ""), SMSkey,type,sharedPreferences.getString(Constants.NAME, ""), new ApiManager.SendPhoneVerificationInfoCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String result, String message) {
                                                    if (success)
                                                    {
                                                        try
                                                        {
                                                            if (result.equals("success"))
                                                            {
                                                                countDownTimer.start();
                                                                mResendSMS.setVisibility(View.GONE);
                                                                mCountDownTimerLayout.setVisibility(View.VISIBLE);

                                                                Toast.makeText(mCtx, getString(R.string.sms_sending), Toast.LENGTH_SHORT).show();
                                                            }
                                                            else
                                                            {
                                                                if (message.equals("nexmo_error")) {
                                                                Toast.makeText(mCtx, getString(R.string.nexmo_error), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else if (message.equals("phoneNumber_used")) {
                                                                    Toast.makeText(mCtx, getString(R.string.phone_is_registered), Toast.LENGTH_SHORT).show();
                                                                } else if (message.equals("phoneNumber_error") || message.equals("openID_phoneNumber_not_paired") ) {
                                                                    Toast.makeText(mCtx, getString(R.string.phoneNumber_error), Toast.LENGTH_SHORT).show();
                                                                }
                                                                else {
                                                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        }
                                                        catch (Exception x)
                                                        {
                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            //                          showToast(getString(R.string.error_failed));
                                                            Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                        }
                                                        catch (Exception x) {
                                                        }
                                                    }
                                                    }
                                                });
                                            }

                                        }
                                        else
                                        {
                                            Toast.makeText(mCtx, getString(R.string.human_failure), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    catch (Exception x)
                                    {
                                }
                            }
                            else
                            {
                                try
                                {
                                    //                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x)
                                {
                                }
                            }
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();
                    }


                } else {

//                    toastMsg("client captcha failed:" + result);
                }
            }

            @Override
            public void closeGt() {

//                toastMsg("Close geetest windows");
            }
        });

        dialog.show();
    }

    private void toastMsg(String msg) {

        //Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();

    }

    private static String getDecodeText(Context context, String data)
    {
        try
        {
            JSONObject info = new JSONObject(data);
            String mKey = info.getString(Constants.KEY);
            String mData = info.getString(Constants.DATA);

            return mData;
        }
        catch (OutOfMemoryError o)
        {
            return "";
        }
        catch (JSONException e)
        {
            return "";
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            final Intent intent = new Intent();
            final Bundle bundle = mCtx.getIntent().getExtras();
            intent.putExtra("Binding", bundle.getInt("Binding")); //紀錄是從settingactivity or signupactivityV2 跳到bindingphonenumberactivity,避免crash

            hideKeyboard();
            mDialog.setVisibility(View.VISIBLE);
            mBackground.setVisibility(View.VISIBLE);
            ((View) findViewById(R.id.title_bar_background)).setVisibility(View.VISIBLE);

            mCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.setVisibility(View.INVISIBLE);
                    mBackground.setVisibility(View.INVISIBLE);
                    ((View) findViewById(R.id.title_bar_background)).setVisibility(View.INVISIBLE);
                }

            });

            mDoubleConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mProgress.setVisibility(View.GONE);

                    //unregister the receiver to avoid leak
                    if(isBroadcastRegister != 0) {
                        unregisterReceiver(mBroadcastReceiver);
                        isBroadcastRegister =0;
                    }

                    hideKeyboard();
                    intent.putExtra("Binding", bundle.getInt("Binding")); //紀錄是從settingactivity or signupactivityV2 跳到bindingphonenumberactivity,避免crash
                    SMS = bundle.getInt("SMS");
                    if (SMS == 1) {
                        intent.setClass(mCtx, ForgotPasswordActivity_V2.class);
                    } else {
                        intent.setClass(mCtx, BindingPhoneNumberActivity.class);
                    }
                    startActivity(intent);
                    mCtx.finish();
                }
            });

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }
}
