package com.machipopo.media17;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

/**
 * Created by POPO on 15/9/27.
 */
public class BaseFragmentActivity extends FragmentActivity
{
//    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if(Build.VERSION.SDK_INT >= 23)
        {
            ArrayList<String> mPermission = new ArrayList<String>();

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.CAMERA);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.RECORD_AUDIO);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.READ_CONTACTS);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.READ_SMS);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.READ_PHONE_STATE);
            }

            String[] permission = new String[mPermission.size()];
            for(int i = 0 ; i < mPermission.size() ; i++)
            {
                permission[i] = mPermission.get(i);
            }

            if(permission.length!=0)
            {
                ActivityCompat.requestPermissions(this, permission, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        doNext(requestCode, grantResults);
    }

    private final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 1;

    private void doNext(int requestCode, int[] grantResults)
    {
        if (requestCode == WRITE_EXTERNAL_STORAGE_REQUEST_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                // Permission Granted
            }
            else
            {
                // Permission Denied
            }
        }
    }

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onResume(this);
    }
    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPause(this);
    }
    public void onStart()
    {
        super.onStart();
        FlurryAgent.onStartSession(this,Constants.FLURRY_API_KEY);
    }
    public void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

//    public void showToast(String text)
//    {
//        try
//        {
//            int mPicId = R.drawable.sad;
//            if(text.compareTo(getString(R.string.done))==0 || text.compareTo(getString(R.string.complete))==0 || text.compareTo(getString(R.string.password_ok))==0) mPicId = R.drawable.happy;
//
//            showToast(text, mPicId);
//        }
//        catch (Exception e)
//        {
//        }
//    }
//
//    public void showToast(String text, int imageResourceID)
//    {
//        try
//        {
//            LayoutInflater inflater = getLayoutInflater();
//            RelativeLayout container = (RelativeLayout) inflater.inflate(R.layout.custom_toast, null);
//            TextView toastTextView = (TextView) container.findViewById(R.id.toastTextView);
//            ImageView toastImageView = (ImageView) container.findViewById(R.id.toastImageView);
//
//            toastTextView.setText(text);
//            toastImageView.setImageResource(imageResourceID);
//
//            final Toast toast = new Toast(this);
//            toast.setGravity(Gravity.CENTER, 0, dpToPixel(-60));
//            toast.setDuration(Toast.LENGTH_LONG);
//            toast.setView(container);
//            toast.show();
//
//            handler.postDelayed(new Runnable()
//            {
//                public void run()
//                {
//                    try
//                    {
//                        toast.cancel();
//                    }
//                    catch (Exception e)
//                    {
//                    }
//                }
//            }, 2000);
//        }
//        catch (Exception e)
//        {
//        }
//    }
//
//    public int dpToPixel(int dp)
//    {
//        return Singleton.dpToPixel(dp);
//    }
}
