package com.machipopo.media17;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.flurry.android.FlurryAgent;
import com.machipopo.media17.adapter.UserAdapter;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.notify.NotifyProvider;
import com.machipopo.media17.wxapi.WechatAPI;
import com.machipopo.media17.wxapi.WechatNotify;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.RequestListener;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.otto.Subscribe;
import com.tencent.mm.sdk.openapi.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;
import com.weibo.sdk.FriendshipsAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;

public class AddFriendActivity_EX extends BaseNewActivity implements View.OnClickListener {


    private InputMethodManager inputMethodManager = null;

    protected ListView mListView;
    protected ProgressBar mProgress;
    protected ImageView mNoData;
    protected Button mAgreeBtn;


    protected LinearLayout mFacebookTabLayout;
    protected LinearLayout mIGTabLayout;
    protected LinearLayout mTwitterTabLayout;
    protected LinearLayout mWechatTabLayout;
    protected LinearLayout mQQTabLayout;
    protected LinearLayout mWeiboTabLayout;

    protected ImageView mSearch;
    protected ImageView mContact;
    protected ImageView mFacebook;
    protected ImageView mIG;
    protected ImageView mTwitter;
    protected ImageView mWechat;
    protected ImageView mQQ;
    protected ImageView mWeibo;

    protected RelativeLayout mSearch_layout;
    protected EditText mSearchEditV;


    private enum IndexRule {UserModel, PhoneModel, FacebookModel, IGModel, TwitterModel, WechatModel, QQModel, WeiboModel};

    protected IndexRule mTabIndex = IndexRule.UserModel;
    protected int mUserModePosition = 0;

    private ArrayList<HashMap<String, String>> mContactData;
    private ArrayList<UserModel> mUserModel = new ArrayList<>();
    private ArrayList<UserModel> mPhoneModel = new ArrayList<>();
    private ArrayList<UserModel> mFacebookModel = new ArrayList<>();
    private ArrayList<UserModel> mIGModel = new ArrayList<>();
    private ArrayList<UserModel> mTwitterModel = new ArrayList<>();
    private ArrayList<UserModel> mWechatModel = new ArrayList<>();
    private ArrayList<UserModel> mQQModel = new ArrayList<>();
    private ArrayList<UserModel> mWeiboModel = new ArrayList<>();


    private ListAdapter mAdapter;

    private DisplayImageOptions mSelfOptions;

    private CallbackManager callbackManager;
    private Dialog mInstagramDialog;
    private SsoHandler mSsoHandler;
    private Tencent mTencent;


    protected TextWatcher mTextWatcher = new TextWatcher(){

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(s.toString().length()!= 0) {
                getSearchUsers(s.toString());
            }else{
                getHotUsers();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(getClass().getSimpleName());
    }

    public void onStart() {
        super.onStart();
        NotifyProvider.getInstance().register(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(getClass().getSimpleName());
        NotifyProvider.getInstance().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (mTencent != null) {
                mTencent.logout(this);
            }
        } catch (Exception e) {}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if(callbackManager != null) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }

            // Weibo SSO 授权回调 => 重要：发起 SSO 登陆的 Activity 必须重写 onActivityResults
            if (mSsoHandler != null) {
                mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
            }
        } catch (Exception e) {}


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        } catch (Exception e) {
        }

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        mSelfOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.placehold_profile_s)
                .showImageForEmptyUri(R.drawable.placehold_profile_s)
                .showImageOnFail(R.drawable.placehold_profile_s)
                .cacheInMemory(Constants.PHOTO_CACHE)
                .cacheOnDisk(Constants.PHOTO_CACHE)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        setContentView(R.layout.add_friend_activity_ex);

        initTitleBar();
        findChildView();

    }

    private void findChildView() {
        mListView = (ListView) findViewById(R.id.add_friends_listView);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);
        mAgreeBtn = (Button) findViewById(R.id.agree_load_friends_btn);


        mSearch = (ImageView) findViewById(R.id.search);
        mContact = (ImageView) findViewById(R.id.contact);

        mFacebookTabLayout = (LinearLayout) findViewById(R.id.facebook_tab_layout);
        mIGTabLayout = (LinearLayout) findViewById(R.id.ig_tab_layout);
        mTwitterTabLayout = (LinearLayout) findViewById(R.id.twitter_tab_layout);
        mWechatTabLayout = (LinearLayout) findViewById(R.id.wechat_tab_layout);
        mQQTabLayout = (LinearLayout) findViewById(R.id.qq_tab_layout);
        mWeiboTabLayout = (LinearLayout) findViewById(R.id.weibo_tab_layout);

        mFacebook = (ImageView) findViewById(R.id.facebook);
        mIG = (ImageView) findViewById(R.id.ig);
        mTwitter = (ImageView) findViewById(R.id.twitter);
        mWechat = (ImageView) findViewById(R.id.wechat);
        mQQ = (ImageView) findViewById(R.id.qq);
        mWeibo = (ImageView) findViewById(R.id.weibo);

        mSearch_layout = (RelativeLayout) findViewById(R.id.search_layout);
        mSearchEditV = (EditText) findViewById(R.id.add_friend_search_edit);


        mAgreeBtn.setOnClickListener(this);

        mSearch.setOnClickListener(this);
        mContact.setOnClickListener(this);
        mFacebookTabLayout.setOnClickListener(this);
        mIGTabLayout.setOnClickListener(this);
        mTwitterTabLayout.setOnClickListener(this);
        mWechatTabLayout.setOnClickListener(this);
        mQQTabLayout.setOnClickListener(this);
        mWeiboTabLayout.setOnClickListener(this);
        mAgreeBtn.setVisibility(View.GONE);

        mSearchEditV.addTextChangedListener(mTextWatcher);

        mListView.setEmptyView(mNoData);

        refreshTabStatus(mTabIndex);
        getHotUsers();

        /*if(Constants.INTERNATIONAL_VERSION){
            mFacebookTabLayout.setVisibility(View.VISIBLE);
            mIGTabLayout.setVisibility(View.VISIBLE);
            mTwitterTabLayout.setVisibility(View.VISIBLE);
            mWechatTabLayout.setVisibility(View.GONE);
            mQQTabLayout.setVisibility(View.GONE);
            mWeiboTabLayout.setVisibility(View.GONE);
        }else{
            mFacebookTabLayout.setVisibility(View.GONE);
            mIGTabLayout.setVisibility(View.GONE);
            mTwitterTabLayout.setVisibility(View.GONE);
            mWechatTabLayout.setVisibility(View.VISIBLE);
            mQQTabLayout.setVisibility(View.VISIBLE);
            mWeiboTabLayout.setVisibility(View.VISIBLE);
        }*/
    }

    private void initTitleBar() {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.find_friend));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.add_invite));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareTo(getString(R.string.share_subject), getString(R.string.share_body1) + Singleton.preferences.getString(Constants.OPEN_ID, "") + getString(R.string.share_body2) + " " + Constants.SHARE_LINK_APP, getString(R.string.share_title));
            }
        });
    }

    private void shareTo(String subject, String body, String chooserTitle) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    @Override
    public void onClick(View view) {
        if(mSearch == view){
            refreshTabStatus(IndexRule.UserModel);
            mAgreeBtn.setVisibility(View.GONE);
            if(mUserModel.size() != 0){
                setListAdapter(IndexRule.UserModel, mUserModel);
            }else{
                String searchStr = mSearchEditV.getText().toString();
                if(searchStr != null && searchStr.length()!= 0) {
                    getSearchUsers(searchStr);
                }else{
                    getHotUsers();
                }
            }
        } else if(mContact == view){
            refreshTabStatus(IndexRule.PhoneModel);
            mAgreeBtn.setVisibility(View.GONE);
            if(mPhoneModel.size() != 0){
                setListAdapter(IndexRule.PhoneModel, mPhoneModel);
            }else{
                getContacts();
            }
        }else if(mFacebookTabLayout == view){
            refreshTabStatus(IndexRule.FacebookModel);
            if (getConfig(Constants.SUMBIT_FACEBOOK, 0) == 0) {
                mAgreeBtn.setVisibility(View.VISIBLE);
                mNoData.setVisibility(View.GONE);
            }else{
                mAgreeBtn.setVisibility(View.GONE);
                if(mFacebookModel.size() != 0){
                    setListAdapter(IndexRule.FacebookModel, mFacebookModel);
                }else{
                    getFBFriend();
                }
            }
        }else if(mIGTabLayout == view){
            refreshTabStatus(IndexRule.IGModel);
            if (getConfig(Constants.SUMBIT_INSTAGRAM, 0) == 0) {
                mAgreeBtn.setVisibility(View.VISIBLE);
                mNoData.setVisibility(View.GONE);
            }else{
                mAgreeBtn.setVisibility(View.GONE);
                if(mIGModel.size() != 0){
                    setListAdapter(IndexRule.IGModel, mIGModel);
                }else{
                    getIGFriend();
                }
            }
        }else if(mTwitterTabLayout == view) {
            refreshTabStatus(IndexRule.TwitterModel);
            if (getConfig(Constants.SUMBIT_TWITTER, 0) == 0) {
                mAgreeBtn.setVisibility(View.VISIBLE);
                mNoData.setVisibility(View.GONE);
            }else{
                mAgreeBtn.setVisibility(View.GONE);
                if(mTwitterModel.size() != 0){
                    setListAdapter(IndexRule.TwitterModel, mTwitterModel);
                }else{
                    getTwitterFriend();
                }
            }
        }else if(mWechatTabLayout == view){
            refreshTabStatus(IndexRule.WechatModel);
            if (getConfig(Constants.SUMBIT_WECHAT, 0) == 0) {
                mAgreeBtn.setVisibility(View.VISIBLE);
                mNoData.setVisibility(View.GONE);
            }else{
                mAgreeBtn.setVisibility(View.GONE);
                if(mWechatModel.size() != 0){
                    setListAdapter(IndexRule.WechatModel, mWechatModel);
                }else{
                    wechatLogin();
                }
            }
        }else if(mQQTabLayout == view){
            refreshTabStatus(IndexRule.QQModel);
            if (getConfig(Constants.SUMBIT_QQ, 0) == 0) {
                mAgreeBtn.setVisibility(View.VISIBLE);
                mNoData.setVisibility(View.GONE);
            }else{
                mAgreeBtn.setVisibility(View.GONE);
                if(mQQModel.size() != 0){
                    setListAdapter(IndexRule.QQModel, mQQModel);
                }else{
                    getQQFriend();
                }
            }
        }else if(mWeiboTabLayout == view){
            refreshTabStatus(IndexRule.WeiboModel);
            if (getConfig(Constants.SUMBIT_WEIBO, 0) == 0) {
                mAgreeBtn.setVisibility(View.VISIBLE);
                mNoData.setVisibility(View.GONE);
            }else{
                mAgreeBtn.setVisibility(View.GONE);
                if(mWeiboModel.size() != 0){
                    setListAdapter(IndexRule.WeiboModel, mWeiboModel);
                }else{
                    getWeiboFriend();
                }
            }
        }else if(mAgreeBtn == view){
            mAgreeBtn.setVisibility(View.GONE);
            if(mTabIndex == IndexRule.FacebookModel){
                //setConfig(Constants.SUMBIT_FACEBOOK, 1);
                getFBFriend();
            }else if(mTabIndex == IndexRule.IGModel){
                //setConfig(Constants.SUMBIT_INSTAGRAM, 1);
                getIGFriend();
            }else if(mTabIndex == IndexRule.TwitterModel){
                //setConfig(Constants.SUMBIT_TWITTER, 1);
                getTwitterFriend();
            }else if(mTabIndex == IndexRule.WechatModel){
                //setConfig(Constants.SUMBIT_WECHAT, 1);
                getWechatFriend();
            }else if(mTabIndex == IndexRule.QQModel){
                //setConfig(Constants.SUMBIT_QQ, 1);
                getQQFriend();
            }else if(mTabIndex == IndexRule.WeiboModel){
                //setConfig(Constants.SUMBIT_WEIBO, 1);
                getWeiboFriend();
            }
        }
    }

    private void refreshTabStatus(IndexRule nowIndex){

        if(nowIndex == mTabIndex){
            return;
        }

        if(mTabIndex == IndexRule.UserModel){
            mSearch.setImageResource(R.drawable.search);
            mUserModePosition = mListView.getSelectedItemPosition();
        }else if(mTabIndex == IndexRule.PhoneModel){
            mContact.setImageResource(R.drawable.contact);
        }else if(mTabIndex == IndexRule.FacebookModel){
            mFacebook.setImageResource(R.drawable.facebook);
        }else if(mTabIndex == IndexRule.IGModel){
            mIG.setImageResource(R.drawable.ig);
        }else if(mTabIndex == IndexRule.TwitterModel){
            mTwitter.setImageResource(R.drawable.twitter);
        }else if(mTabIndex == IndexRule.WechatModel){
            mWechat.setImageResource(R.drawable.wechat);
        }else if(mTabIndex == IndexRule.QQModel){
            mQQ.setImageResource(R.drawable.qq);
        }else if(mTabIndex == IndexRule.WeiboModel){
            mWeibo.setImageResource(R.drawable.weibo);
        }

        if(nowIndex == IndexRule.UserModel){
            mSearch.setImageResource(R.drawable.search_active);
        }else if(nowIndex == IndexRule.PhoneModel){
            mContact.setImageResource(R.drawable.contact_active);
            mAgreeBtn.setText(getString(R.string.connect_contact));
        }else if(nowIndex == IndexRule.FacebookModel){
            mFacebook.setImageResource(R.drawable.facebook_active);
            mAgreeBtn.setText(getString(R.string.connect_facebook));
        }else if(nowIndex == IndexRule.IGModel){
            mIG.setImageResource(R.drawable.ig_active);
            mAgreeBtn.setText(getString(R.string.connect_instagram));
        }else if(nowIndex == IndexRule.TwitterModel){
            mTwitter.setImageResource(R.drawable.twitter_active);
            mAgreeBtn.setText(getString(R.string.connect_twitter));
        }else if(nowIndex == IndexRule.WechatModel){
            mWechat.setImageResource(R.drawable.wechat_active);
            mAgreeBtn.setText("");
        }else if(nowIndex == IndexRule.QQModel){
            mQQ.setImageResource(R.drawable.qq_active);
            mAgreeBtn.setText("");
        }else if(nowIndex == IndexRule.WeiboModel){
            mWeibo.setImageResource(R.drawable.weibo_active);
            mAgreeBtn.setText("");
        }

        if(nowIndex == IndexRule.UserModel){
            mSearch_layout.setVisibility(View.VISIBLE);
        }else{
            mSearch_layout.setVisibility(View.GONE);
        }

        hideKeyboard();
        mListView.setAdapter(null);
        setProgressView(false);

        mTabIndex = nowIndex;
    }

    protected void hideKeyboard() {
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void setConfig(String key, int value) {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putInt(key, value);
        PE.commit();
    }

    public int getConfig(String key, int def) {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        return settings.getInt(key, def);
    }

    public String getConfig(String key, String def) {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        return settings.getString(key, def);
    }

    private void setListAdapter(IndexRule index, ArrayList<UserModel> dataList){
        if(mTabIndex != index){
            return;
        }

        setProgressView(false);
        if(dataList != null){
            if(mTabIndex == IndexRule.UserModel && dataList.equals(mUserModel)){
                mAdapter = new UserAdapter(AddFriendActivity_EX.this, dataList, mSelfOptions, "SearchUserAndClick");
                mListView.setAdapter(mAdapter);
            }else {
                mAdapter = new UserAdapter(AddFriendActivity_EX.this, dataList, mSelfOptions);
                mListView.setAdapter(mAdapter);
            }
        }else{
            mAdapter = null;
            mListView.setAdapter(mAdapter);
        }
    }

    private void setProgressView(boolean isVisible){
        if(isVisible){
            mProgress.setVisibility(View.VISIBLE);
            mNoData.setVisibility(View.GONE);
        }else{
            mProgress.setVisibility(View.GONE);
            mNoData.setVisibility(View.VISIBLE);
        }
    }

    private void getHotUsers(){
        setProgressView(true);
        ApiManager.getHotUsers(this, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, new ApiManager.GetHotUsersCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<UserModel> userModel) {
                if (success && userModel != null) {
                    if (userModel.size() != 0) {
                        mUserModel.clear();
                        mUserModel.addAll(userModel);
                    }
                } else {
                    try {
                        Toast.makeText(AddFriendActivity_EX.this, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                    }
                }

                setListAdapter(IndexRule.UserModel, mUserModel);
            }
        });
    }

    private void getSearchUsers(String str){
        setProgressView(true);
        ApiManager.getSearchUsers(this, str, 0, 100, 0, new ApiManager.GetSearchUsersCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<UserModel> userModel) {
                if (success && userModel != null) {
                    if (userModel.size() != 0) {
                        mUserModel.clear();
                        mUserModel.addAll(userModel);
                    }
                } else {
                    try {
                        Toast.makeText(AddFriendActivity_EX.this, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                    }
                }

                setListAdapter(IndexRule.UserModel, mUserModel);
            }
        });
    }

    private void getContacts() {
        setProgressView(true);
        mContactData = new ArrayList<HashMap<String, String>>();
        Cursor c = null;
        try {
            c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            if(c != null){
                c.moveToFirst();
                do{
                    HashMap hm = new HashMap();
                    hm.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                    hm.put(ContactsContract.CommonDataKinds.Phone.NUMBER, c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                    hm.put(ContactsContract.CommonDataKinds.Phone.PHOTO_URI, c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
                    mContactData.add(hm);
                }while(c.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
            //No Data
            setListAdapter(IndexRule.PhoneModel, null);
            return;
        }finally {
            if(c != null){
                c.close();
            }
        }

        final JSONArray mJSONArray = new JSONArray();
        final JSONArray mFindJSONArray = new JSONArray();

        for (int i = 0; i < mContactData.size(); i++) {
            String num = "";

            if (mContactData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length() != 0) {
                if (mContactData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(0, 1).contains("0")) {
                    num = mContactData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(1, mContactData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length());
                } else {
                    num = mContactData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER);
                }

                try {
                    JSONObject mObject = new JSONObject();
                    mObject.put("phone", num.replaceAll("[^\\d]", ""));
                    mObject.put("name", mContactData.get(i).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    mJSONArray.put(mObject);

                    JSONObject mObject2 = new JSONObject();
                    mObject2.put("phone", getConfig("countryCode", "886") + num.replaceAll("[^\\d]", ""));
                    mObject2.put("name", mContactData.get(i).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    mJSONArray.put(mObject2);
                    mFindJSONArray.put(num.replaceAll("[^\\d]", ""));
                } catch (JSONException e) {
                }
            }
        }

        String phone = getConfig("phone", "");
        if (mJSONArray.length() != 0) {
            ApiManager.uploadPhoneNumbers(this, Singleton.preferences.getString(Constants.USER_ID, ""), phone, getConfig("countryCode", "886"), mJSONArray, new ApiManager.UploadPhoneNumbersCallback() {
                @Override
                public void onResult(boolean success, String message) {
                    if (success) {
                        ApiManager.findFriends(AddFriendActivity_EX.this, Singleton.preferences.getString(Constants.USER_ID, ""), mFindJSONArray.toString(), "contacts", new ApiManager.FindFriendsCallback() {
                            @Override
                            public void onResult(boolean success, ArrayList<UserModel> model) {
                                if (success && model != null) {
                                    if (model.size() != 0) {
                                        mPhoneModel.clear();
                                        mPhoneModel.addAll(model);
                                    }
                                }
                                setListAdapter(IndexRule.PhoneModel, mPhoneModel);
                            }
                        });
                    }else{
                        setListAdapter(IndexRule.PhoneModel, mPhoneModel);
                    }
                }
            });
        }
    }


    private void getFBFriend(){
        setProgressView(true);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        final String token = loginResult.getAccessToken().getToken();

                        GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject user, GraphResponse response) {
                                if (user != null) {
                                    try {
                                        JSONObject params = new JSONObject();
                                        params.put("facebookID", user.optString("id"));
                                        params.put("facebookAccessToken", token);

                                        final String NAME = user.optString("name");

                                        ApiManager.updateUserInfo(AddFriendActivity_EX.this, params, new ApiManager.UpdateUserInfoCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {
                                                    GraphRequest.newMyFriendsRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONArrayCallback() {
                                                        @Override
                                                        public void onCompleted(JSONArray users, GraphResponse graphResponse) {
                                                            try {
                                                                final JSONArray mJSONArray = new JSONArray();
                                                                final JSONArray mFindJSONArray = new JSONArray();
                                                                for (int i = 0; i < users.length(); i++) {
                                                                    JSONObject mObject = new JSONObject();
                                                                    mObject.put("id", users.getJSONObject(i).getString("id"));
                                                                    mObject.put("name", users.getJSONObject(i).getString("name"));
                                                                    mJSONArray.put(mObject);
                                                                    mFindJSONArray.put(users.getJSONObject(i).getString("id"));
                                                                }

                                                                ApiManager.uploadFacebookFriends(AddFriendActivity_EX.this, Singleton.preferences.getString(Constants.USER_ID, ""), NAME, mJSONArray, new ApiManager.UploadFacebookFriendsCallback() {
                                                                    @Override
                                                                    public void onResult(boolean success, String message) {
                                                                        if (success) {
                                                                            ApiManager.findFriends(AddFriendActivity_EX.this, Singleton.preferences.getString(Constants.USER_ID, ""), mFindJSONArray.toString(), "facebook", new ApiManager.FindFriendsCallback() {
                                                                                @Override
                                                                                public void onResult(boolean success, ArrayList<UserModel> model) {
                                                                                    if (success && model != null) {
                                                                                        if (model.size() != 0) {
                                                                                            mFacebookModel.clear();
                                                                                            mFacebookModel.addAll(model);
                                                                                        }
                                                                                    }
                                                                                    setListAdapter(IndexRule.FacebookModel, mFacebookModel);
                                                                                }
                                                                            });
                                                                        } else {
                                                                            setListAdapter(IndexRule.FacebookModel, mFacebookModel);
                                                                            // Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                });
                                                            } catch (JSONException e) {
                                                                setListAdapter(IndexRule.FacebookModel, mFacebookModel);
                                                                //  Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    }).executeAsync();
                                                } else {
                                                    setListAdapter(IndexRule.FacebookModel, mFacebookModel);
                                                    // Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                        setListAdapter(IndexRule.FacebookModel, mFacebookModel);
                                        // Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }).executeAsync();
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        setListAdapter(IndexRule.FacebookModel, mFacebookModel);
                    }
                });
    }

    private synchronized void getIGFriend(){

        if(mInstagramDialog != null && mInstagramDialog.isShowing()){
            mInstagramDialog.cancel();
        }

        mInstagramDialog = new Dialog(this, R.style.LivePlayerDialog);
        mInstagramDialog.setContentView(R.layout.instagram_dialog);
        WebView mWeb = (WebView) mInstagramDialog.findViewById(R.id.web);
        final ProgressBar mProgress = (ProgressBar) mInstagramDialog.findViewById(R.id.progress);
        mWeb.getSettings().setJavaScriptEnabled(true);
        mInstagramDialog.setCancelable(true);
        mWeb.loadUrl("https://instagram.com/oauth/authorize/?client_id=" + Constants.IG_CLIENT_ID + "&redirect_uri=" + Constants.IG_REDIRECT_URI + "&response_type=token");
        mWeb.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);

                if (url.contains(Constants.IG_REDIRECT_URI) && url.contains("#access_token=")) {
                    int pos = url.indexOf("=") + 1;
                    final String token = url.substring(pos, url.length());

                    try {
                        OkHttpClient mClient = new OkHttpClient();
                        Request request = new Request.Builder().url("https://api.instagram.com/v1/users/self?access_token=" + token).build();
                        mClient.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful()) {
                                    mInstagramDialog.dismiss();

                                    try {
                                        String mData = response.body().string();
                                        String id = new JSONObject(mData).getJSONObject("data").getString("id");
                                        final String NAME = new JSONObject(mData).getJSONObject("data").getString("full_name");
                                        OkHttpClient mClient = new OkHttpClient();
                                        Request request = new Request.Builder().url("https://api.instagram.com/v1/users/" + id + "/follows" + "/?access_token=" + token).build();
                                        mClient.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if (response.isSuccessful()) {
                                                    try {
                                                        JSONArray data = new JSONObject(response.body().string()).getJSONArray("data");
                                                        final JSONArray mJSONArray = new JSONArray();
                                                        final JSONArray mFindJSONArray = new JSONArray();
                                                        for (int i = 0; i < data.length(); i++) {
                                                            JSONObject mObject = new JSONObject();
                                                            mObject.put("id", data.getJSONObject(i).getString("id"));
                                                            mObject.put("name", data.getJSONObject(i).getString("username"));
                                                            mJSONArray.put(mObject);
                                                            mFindJSONArray.put(data.getJSONObject(i).getString("id"));
                                                        }

                                                        ApiManager.uploadInstagramFriends(AddFriendActivity_EX.this, NAME, mJSONArray, new ApiManager.UploadFacebookFriendsCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                Log.d("123", "success : " + success);
                                                                if (success) {
                                                                    ApiManager.findFriends(AddFriendActivity_EX.this, Singleton.preferences.getString(Constants.USER_ID, ""), mFindJSONArray.toString(), "instagram", new ApiManager.FindFriendsCallback() {
                                                                        @Override
                                                                        public void onResult(boolean success, ArrayList<UserModel> model) {
                                                                            if (success && model != null) {
                                                                                if (model.size() != 0) {
                                                                                    mIGModel.clear();
                                                                                    mIGModel.addAll(model);
                                                                                }
                                                                            }
                                                                            setListAdapter(IndexRule.IGModel, mIGModel);
                                                                        }
                                                                    });
                                                                } else {
                                                                    setListAdapter(IndexRule.IGModel, mIGModel);
                                                                    // Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        setListAdapter(IndexRule.IGModel, mIGModel);
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                setListAdapter(IndexRule.IGModel, mIGModel);
                                            }
                                        });
                                    } catch (Exception e) {
                                        setListAdapter(IndexRule.IGModel, mIGModel);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Request request, IOException e) {
                                setListAdapter(IndexRule.IGModel, mIGModel);
                            }
                        });
                    } catch (Exception e) {
                        setListAdapter(IndexRule.IGModel, mIGModel);
                    }
                }
            }
        });
        mInstagramDialog.show();
    }

    private void getTwitterFriend(){
    }

    private void wechatLogin() {
        IWXAPI api = WXAPIFactory.createWXAPI(this, Constants.WECHAT_APP_KEY, true);
        // 将该app注册到微信
        api.registerApp(Constants.WECHAT_APP_KEY);
        final SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "wechat_sdk_demo";
        //req.state = "carjob_wx_login";
        //req.state = "none";
        api.sendReq(req);
    }

    @Subscribe
    public void wechatNotify(WechatNotify notify){
        if(notify != null){
            switch(notify.errorCode) {
                case BaseResp.ErrCode.ERR_OK:
                    //"发送成功";
                    getWechatFriend();
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    //"发送取消";
                    break;
                case BaseResp.ErrCode.ERR_AUTH_DENIED:
                    //"发送被拒绝";
                    break;
                default:
                    //"发送返回";
                    break;
            }
        }
    }

    private void getWechatFriend(){
        final WechatAPI wechatAPI = new WechatAPI();
        new Thread(){
            @Override
            public void run() {
                String code = "";
                String resStr = wechatAPI.getAccessToken(Constants.WECHAT_APP_KEY, Constants.WECHAT_SECRET, code);
                try {
                    String accessToken = "";
                    String openId = "";
                    JSONObject jsonObj = new JSONObject(resStr);
                    accessToken = jsonObj.getString("access_token");
                    openId = jsonObj.getString("openid");
                    wechatAPI.getFriend(accessToken);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }

    private void getQQFriend(){
        mTencent = Tencent.createInstance(Constants.QQAppid, this);
        mTencent.login(this, "all", new IUiListener() {
            @Override
            public void onComplete(Object response) {
                try{
                    JSONObject jsonObject = (JSONObject)response;
                    final String token = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_ACCESS_TOKEN);
                    String expires = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_EXPIRES_IN);
                    final String openId = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_OPEN_ID);

                    mTencent.setAccessToken(token, expires);
                    mTencent.setOpenId(openId);
                    if (mTencent != null && mTencent.isSessionValid()) {
                        String url = "https://graph.qq.com/user/get_app_friends?access_token=%s&oauth_consumer_key=%s&openid=%s&format=json";
                        Formatter formatter = new Formatter();
                        formatter.format(url, token, mTencent.getAppId(), openId);
                        url = formatter.toString();
                        OkHttpClient mClient = new OkHttpClient();
                        Request request = new Request.Builder().url(url).build();
                        mClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                setListAdapter(IndexRule.QQModel, mQQModel);
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONArray data = new JSONObject(response.body().string()).getJSONArray("items");
                                        final JSONArray mJSONArray = new JSONArray();
                                        final JSONArray mFindJSONArray = new JSONArray();
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject mObject = new JSONObject();
                                            mObject.put("id", data.getJSONObject(i).getString("openid"));
                                            mObject.put("name", data.getJSONObject(i).getString("nickname"));
                                            mJSONArray.put(mObject);
                                            mFindJSONArray.put(data.getJSONObject(i).getString("openid"));
                                        }
                                        //TODO add friends to server
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        setListAdapter(IndexRule.QQModel, mQQModel);
                                    }
                                }else{
                                    setListAdapter(IndexRule.QQModel, mQQModel);
                                }
                            }
                        });

                    }
                }catch (Exception e){
                    setListAdapter(IndexRule.QQModel, mQQModel);
                }
            }

        @Override
            public void onError(UiError uiError) {
                setListAdapter(IndexRule.QQModel, mQQModel);
            }

            @Override
            public void onCancel() {
            }
        });


    }


    private void getWeiboFriend(){
        AuthInfo mAuthInfo = new AuthInfo(this, Constants.WEIBO_APP_KEY, Constants.WEIBO_REDIRECT_URL, Constants.WEIBO_SCOPE);
        mSsoHandler = new SsoHandler(this, mAuthInfo);
        mSsoHandler.authorize(new WeiboAuthListener() {
            @Override
            public void onComplete(Bundle bundle) {
                final Oauth2AccessToken mAccessToken = Oauth2AccessToken.parseAccessToken(bundle);
                if (mAccessToken.isSessionValid()) {
                    FriendshipsAPI mFriends = new FriendshipsAPI(AddFriendActivity_EX.this, Constants.WEIBO_APP_KEY, mAccessToken);

                    mFriends.friends(Long.valueOf(mAccessToken.getUid()), 200, 0, true, new RequestListener() {
                        @Override
                        public void onComplete(String s) {
                            try {
                                JSONObject info_obj = new JSONObject(s);
                                JSONArray info_array = info_obj.getJSONArray("users");
                                for(int i=0; i<info_array.length(); i++){
                                    JSONObject info = info_array.getJSONObject(i);
                                    int id = info.getInt("id");
                                    String screen_name = info.getString("screen_name");
                                    String pic = info.getString("profile_image_url");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onWeiboException(WeiboException e) {
                            e.printStackTrace();
                        }
                    });
                }
            }

            @Override
            public void onWeiboException(final WeiboException e) {
                e.printStackTrace();
            }

            @Override
            public void onCancel() {
            }

        });
    }


}