package com.machipopo.media17;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

/**
 * Created by POPO on 6/30/15.
 */
public class SettingNotifiActivity extends BaseNewActivity
{
    private SettingNotifiActivity mCtx = this;

    private LinearLayout like_close_layout,like_friend_layout,like_all_layout;
    private ImageView like_close,like_friend,like_all;

    private LinearLayout comment_close_layout,comment_friend_layout,comment_all_layout;
    private ImageView comment_close,comment_friend,comment_all;

    private LinearLayout fans_close_layout,fans_all_layout;
    private ImageView fans_close,fans_all;

    private LinearLayout tag_close_layout,tag_friend_layout,tag_all_layout;
    private ImageView tag_close,tag_friend,tag_all;

    private LinearLayout friend_close_layout,friend_all_layout;
    private ImageView friend_close,friend_all;

    private LinearLayout system_close_layout,system_all_layout;
    private ImageView system_close,system_all;

    private LinearLayout live_close_layout,live_all_layout;
    private ImageView live_close,live_all;

    private LinearLayout relive_close_layout,relive_all_layout;
    private ImageView relive_close,relive_all;

    private LinearLayout open_notifi_layout, open_sound_layout, open_vibrate_layout;
    private ImageView open_notifi, open_sound, open_vibrate;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_notifi_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        initTitleBar();

        open_notifi_layout = (LinearLayout) findViewById(R.id.open_notifi_layout);
        open_sound_layout = (LinearLayout) findViewById(R.id.open_sound_layout);
        open_vibrate_layout = (LinearLayout) findViewById(R.id.open_vibrate_layout);

        open_notifi = (ImageView) findViewById(R.id.open_notifi);
        open_sound = (ImageView) findViewById(R.id.open_sound);
        open_vibrate = (ImageView) findViewById(R.id.open_vibrate);

        if(getConfig(Constants.SETTING_NOTIFICATION_OPEN,1)==1) open_notifi.setImageResource(R.drawable.toggle_active);
        else open_notifi.setImageResource(R.drawable.toggle);

        if(getConfig(Constants.SETTING_NOTIFICATION_SOUND,1)==1) open_sound.setImageResource(R.drawable.toggle_active);
        else open_sound.setImageResource(R.drawable.toggle);

        if(getConfig(Constants.SETTING_NOTIFICATION_VIBRATE,1)==1) open_vibrate.setImageResource(R.drawable.toggle_active);
        else open_vibrate.setImageResource(R.drawable.toggle);

        open_notifi_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getConfig(Constants.SETTING_NOTIFICATION_OPEN,1)==1){
                    setConfig(Constants.SETTING_NOTIFICATION_OPEN,0);
                    open_notifi.setImageResource(R.drawable.toggle);

                    setConfig(Constants.SETTING_NOTIFICATION_SOUND,0);
                    open_sound.setImageResource(R.drawable.toggle);

                    setConfig(Constants.SETTING_NOTIFICATION_VIBRATE,0);
                    open_vibrate.setImageResource(R.drawable.toggle);
                }
                else{
                    setConfig(Constants.SETTING_NOTIFICATION_OPEN,1);
                    open_notifi.setImageResource(R.drawable.toggle_active);

                    setConfig(Constants.SETTING_NOTIFICATION_SOUND,1);
                    open_sound.setImageResource(R.drawable.toggle_active);

                    setConfig(Constants.SETTING_NOTIFICATION_VIBRATE,1);
                    open_vibrate.setImageResource(R.drawable.toggle_active);
                }
            }
        });

        open_sound_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getConfig(Constants.SETTING_NOTIFICATION_OPEN,1)==1)
                {
                    if(getConfig(Constants.SETTING_NOTIFICATION_SOUND,1)==1){
                        setConfig(Constants.SETTING_NOTIFICATION_SOUND,0);
                        open_sound.setImageResource(R.drawable.toggle);
                    }
                    else{
                        setConfig(Constants.SETTING_NOTIFICATION_SOUND,1);
                        open_sound.setImageResource(R.drawable.toggle_active);
                    }
                }
            }
        });

        open_vibrate_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getConfig(Constants.SETTING_NOTIFICATION_OPEN,1)==1)
                {
                    if(getConfig(Constants.SETTING_NOTIFICATION_VIBRATE,1)==1){
                        setConfig(Constants.SETTING_NOTIFICATION_VIBRATE,0);
                        open_vibrate.setImageResource(R.drawable.toggle);
                    }
                    else{
                        setConfig(Constants.SETTING_NOTIFICATION_VIBRATE,1);
                        open_vibrate.setImageResource(R.drawable.toggle_active);
                    }
                }
            }
        });

        like_close_layout = (LinearLayout) findViewById(R.id.like_close_layout);
        like_friend_layout = (LinearLayout) findViewById(R.id.like_friend_layout);
        like_all_layout = (LinearLayout) findViewById(R.id.like_all_layout);

        comment_close_layout = (LinearLayout) findViewById(R.id.comment_close_layout);
        comment_friend_layout = (LinearLayout) findViewById(R.id.comment_friend_layout);
        comment_all_layout = (LinearLayout) findViewById(R.id.comment_all_layout);

        fans_close_layout = (LinearLayout) findViewById(R.id.fans_close_layout);
        fans_all_layout = (LinearLayout) findViewById(R.id.fans_all_layout);

        tag_close_layout = (LinearLayout) findViewById(R.id.tag_close_layout);
        tag_friend_layout = (LinearLayout) findViewById(R.id.tag_friend_layout);
        tag_all_layout = (LinearLayout) findViewById(R.id.tag_all_layout);

        friend_close_layout = (LinearLayout) findViewById(R.id.friend_close_layout);
        friend_all_layout = (LinearLayout) findViewById(R.id.friend_all_layout);

        system_close_layout = (LinearLayout) findViewById(R.id.system_close_layout);
        system_all_layout = (LinearLayout) findViewById(R.id.system_all_layout);

        live_close_layout = (LinearLayout) findViewById(R.id.live_close_layout);
        live_all_layout = (LinearLayout) findViewById(R.id.live_all_layout);

        relive_close_layout = (LinearLayout) findViewById(R.id.relive_close_layout);
        relive_all_layout = (LinearLayout) findViewById(R.id.relive_all_layout);

        like_close = (ImageView) findViewById(R.id.like_close);
        like_friend = (ImageView) findViewById(R.id.like_friend);
        like_all = (ImageView) findViewById(R.id.like_all);

        comment_close = (ImageView) findViewById(R.id.comment_close);
        comment_friend = (ImageView) findViewById(R.id.comment_friend);
        comment_all = (ImageView) findViewById(R.id.comment_all);

        fans_close = (ImageView) findViewById(R.id.fans_close);
        fans_all = (ImageView) findViewById(R.id.fans_all);

        tag_close = (ImageView) findViewById(R.id.tag_close);
        tag_friend = (ImageView) findViewById(R.id.tag_friend);
        tag_all = (ImageView) findViewById(R.id.tag_all);

        friend_close = (ImageView) findViewById(R.id.friend_close);
        friend_all = (ImageView) findViewById(R.id.friend_all);

        system_close = (ImageView) findViewById(R.id.system_close);
        system_all = (ImageView) findViewById(R.id.system_all);

        live_close = (ImageView) findViewById(R.id.live_close);
        live_all = (ImageView) findViewById(R.id.live_all);

        relive_close = (ImageView) findViewById(R.id.relive_close);
        relive_all = (ImageView) findViewById(R.id.relive_all);

        if(getConfig(Constants.SETTING_NOTIFI_LIKE,2)==0) like_close.setVisibility(View.VISIBLE);
        else if(getConfig(Constants.SETTING_NOTIFI_LIKE,2)==1) like_friend.setVisibility(View.VISIBLE);
        else like_all.setVisibility(View.VISIBLE);

        if(getConfig(Constants.SETTING_NOTIFI_COMMENT,2)==0) comment_close.setVisibility(View.VISIBLE);
        else if(getConfig(Constants.SETTING_NOTIFI_COMMENT,2)==1) comment_friend.setVisibility(View.VISIBLE);
        else comment_all.setVisibility(View.VISIBLE);

        if(getConfig(Constants.SETTING_NOTIFI_FANS,1)==0) fans_close.setVisibility(View.VISIBLE);
        else fans_all.setVisibility(View.VISIBLE);

        if(getConfig(Constants.SETTING_NOTIFI_TAG,2)==0) tag_close.setVisibility(View.VISIBLE);
        else if(getConfig(Constants.SETTING_NOTIFI_TAG,2)==1) tag_friend.setVisibility(View.VISIBLE);
        else tag_all.setVisibility(View.VISIBLE);

        if(getConfig(Constants.SETTING_NOTIFI_FRIEND,1)==0) friend_close.setVisibility(View.VISIBLE);
        else friend_all.setVisibility(View.VISIBLE);

        if(getConfig(Constants.SETTING_NOTIFI_SYSTEM,1)==0) system_close.setVisibility(View.VISIBLE);
        else system_all.setVisibility(View.VISIBLE);

        if(getConfig(Constants.SETTING_NOTIFI_LIVE,1)==0) live_close.setVisibility(View.VISIBLE);
        else live_all.setVisibility(View.VISIBLE);

        if(getConfig(Constants.SETTING_NOTIFI_RELIVE,1)==0) relive_close.setVisibility(View.VISIBLE);
        else relive_all.setVisibility(View.VISIBLE);

        like_close_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_LIKE, 0);
                like_close.setVisibility(View.VISIBLE);
                like_friend.setVisibility(View.INVISIBLE);
                like_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushLike", "off");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        like_friend_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_LIKE,1);
                like_close.setVisibility(View.INVISIBLE);
                like_friend.setVisibility(View.VISIBLE);
                like_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushLike", "followerOnly");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        like_all_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_LIKE,2);
                like_close.setVisibility(View.INVISIBLE);
                like_friend.setVisibility(View.INVISIBLE);
                like_all.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushLike", "everyone");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        //---

        comment_close_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_COMMENT,0);
                comment_close.setVisibility(View.VISIBLE);
                comment_friend.setVisibility(View.INVISIBLE);
                comment_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushComment", "off");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        comment_friend_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_COMMENT,1);
                comment_close.setVisibility(View.INVISIBLE);
                comment_friend.setVisibility(View.VISIBLE);
                comment_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushComment", "followerOnly");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        comment_all_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_COMMENT,2);
                comment_close.setVisibility(View.INVISIBLE);
                comment_friend.setVisibility(View.INVISIBLE);
                comment_all.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushComment", "everyone");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        //---

        fans_close_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_FANS,0);
                fans_close.setVisibility(View.VISIBLE);
                fans_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushFollow", 0);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (success) {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        fans_all_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_FANS,1);
                fans_close.setVisibility(View.INVISIBLE);
                fans_all.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushFollow", 1);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        //---

        tag_close_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_TAG,0);
                tag_close.setVisibility(View.VISIBLE);
                tag_friend.setVisibility(View.INVISIBLE);
                tag_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushTag", "off");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        tag_friend_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_TAG,1);
                tag_close.setVisibility(View.INVISIBLE);
                tag_friend.setVisibility(View.VISIBLE);
                tag_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushTag", "followerOnly");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        tag_all_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_TAG,2);
                tag_close.setVisibility(View.INVISIBLE);
                tag_friend.setVisibility(View.INVISIBLE);
                tag_all.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushTag", "everyone");
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        //---

        friend_close_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_FRIEND,0);
                friend_close.setVisibility(View.VISIBLE);
                friend_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushFriendJoin", 0);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (success) {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        friend_all_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_FRIEND,1);
                friend_close.setVisibility(View.INVISIBLE);
                friend_all.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushFriendJoin", 1);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if(success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        //---

        system_close_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_SYSTEM,0);
                system_close.setVisibility(View.VISIBLE);
                system_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushSystemNotif", 0);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (success) {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        system_all_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_SYSTEM,1);
                system_close.setVisibility(View.INVISIBLE);
                system_all.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushSystemNotif", 1);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (success) {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        //---

        live_close_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_LIVE,0);
                live_close.setVisibility(View.VISIBLE);
                live_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushLiveStream", 0);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if (success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        live_all_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_LIVE,1);
                live_close.setVisibility(View.INVISIBLE);
                live_all.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushLiveStream", 1);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (success) {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        //---

        relive_close_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_RELIVE,0);
                relive_close.setVisibility(View.VISIBLE);
                relive_all.setVisibility(View.INVISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushLiveRestream", 0);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            if (success)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });

        relive_all_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig(Constants.SETTING_NOTIFI_RELIVE,1);
                relive_close.setVisibility(View.INVISIBLE);
                relive_all.setVisibility(View.VISIBLE);

                try
                {
                    JSONObject params = new JSONObject();
                    params.put("pushLiveRestream", 1);
                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (success) {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        });
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.notifi_setting));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mCtx.finish();
            }
        });
    }

    public void setConfig(String key, int value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putInt(key, value);
        PE.commit();
    }

    public int getConfig(String key,int def)
    {
        try
        {
            SharedPreferences settings = getSharedPreferences("settings",0);
            return settings.getInt(key, def);
        }
        catch (Exception e)
        {
            return 0;
        }
    }
}
