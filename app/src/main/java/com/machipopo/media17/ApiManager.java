package com.machipopo.media17;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.machipopo.media17.model.CommentModel;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.FollowRequestModel;
import com.machipopo.media17.model.GiftLeaderboardModel;
import com.machipopo.media17.model.GiftLivestreamLeaderBoardModel;
import com.machipopo.media17.model.GiftModel;
import com.machipopo.media17.model.LiveComment;
import com.machipopo.media17.model.LiveFeedModel;
import com.machipopo.media17.model.LiveGiftsModel;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.LiveRegionModel;
import com.machipopo.media17.model.LiveStreamModel;
import com.machipopo.media17.model.NotifiModel;
import com.machipopo.media17.model.PaymentModel;
import com.machipopo.media17.model.PointGainModel;
import com.machipopo.media17.model.PointUsageModel;
import com.machipopo.media17.model.PostModel;
import com.machipopo.media17.model.ProductModel;
import com.machipopo.media17.model.ReadGiftModel;
import com.machipopo.media17.model.RevenueModel;

/*Subscriber new class start*/
import com.machipopo.media17.model.SubscribeOptionModel;
import com.machipopo.media17.model.SubscriberUserModel;
import com.machipopo.media17.model.SubscribersPurchaseModel;
import com.machipopo.media17.model.SubscriptionPurchaseModel;
/*Subscriber new class end*/

import com.machipopo.media17.model.SuggestedUsersModel;
import com.machipopo.media17.model.SystemNotifModel;
import com.machipopo.media17.model.TagModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.Base64Utils;
import com.machipopo.media17.utils.LogEventUtil;
import com.machipopo.media17.utils.RSAUtils;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.umeng.analytics.MobclickAgent;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by POPO on 5/18/15.
 */
public class ApiManager
{
    private static OkHttpClient mClient;
    private static AsyncHttpClient client;

    private static ArrayList<UserModel> mModel = new ArrayList<UserModel>();
    private static ArrayList<SuggestedUsersModel> mSuggesteds = new ArrayList<SuggestedUsersModel>();
    private static ArrayList<PostModel> mPosts = new ArrayList<PostModel>();

    private static ArrayList<FeedModel> mFeedModel = new ArrayList<FeedModel>();
    private static ArrayList<CommentModel> mCommentModel = new ArrayList<CommentModel>();

    private static ArrayList<TagModel> mTagModel = new ArrayList<TagModel>();

    private static ArrayList<RevenueModel> mRevenueModel = new ArrayList<RevenueModel>();

    private static ArrayList<NotifiModel> mNotifiModel = new ArrayList<NotifiModel>();
    private static ArrayList<NotifiModel> mNotifiFriendModel = new ArrayList<NotifiModel>();

    private static ArrayList<LiveFeedModel> mFeedLiveModel = new ArrayList<LiveFeedModel>();

    private static ArrayList<LiveComment> mLiveComments = new ArrayList<LiveComment>();

    private static ArrayList<LiveModel> mLiveModels = new ArrayList<LiveModel>();

    private static ArrayList<GiftLeaderboardModel> mGiftLeaderboardModelsAll = new ArrayList<GiftLeaderboardModel>();
    private static ArrayList<GiftLeaderboardModel> mGiftLeaderboardModelsToday = new ArrayList<GiftLeaderboardModel>();

    private static ArrayList<GiftLivestreamLeaderBoardModel> mGiftLivestreamLeaderboardAll = new ArrayList<GiftLivestreamLeaderBoardModel>();

    /*Subscriber arraylist start*/
    private static ArrayList<SubscribeOptionModel> mSubscribeOptionModelAll = new ArrayList<SubscribeOptionModel>();
    private static ArrayList<SubscriberUserModel> SubscriberUserModelAll = new ArrayList<SubscriberUserModel>();
    private static ArrayList<SubscribersPurchaseModel> SubscribersPurchaseModelAll = new ArrayList<SubscribersPurchaseModel>();
    private static ArrayList<SubscriptionPurchaseModel> SubscriptionPurchaseModelAll = new ArrayList<SubscriptionPurchaseModel>();
    /*Subscriber arraylist end*/

    private static ArrayList<LiveModel> mHotLiveModels = new ArrayList<LiveModel>();
    private static ArrayList<LiveModel> mLatestLiveModels = new ArrayList<LiveModel>();
    private static ArrayList<LiveModel> mSuggestedLiveModels = new ArrayList<LiveModel>();
    private static ArrayList<GiftModel> mGiftModel = new ArrayList<GiftModel>();
    private static ArrayList<PointGainModel> mPointGainModels = new ArrayList<PointGainModel>();
    private static ArrayList<PointUsageModel> mPointUsageModels = new ArrayList<PointUsageModel>();
    private static ArrayList<ProductModel> mProductModels = new ArrayList<ProductModel>();
    private static ArrayList<ReadGiftModel> mReadGiftModels = new ArrayList<ReadGiftModel>();

    private static ArrayList<LiveGiftsModel> mLiveGiftsModels = new ArrayList<LiveGiftsModel>();
    private static ArrayList<UserModel> mModelHour = new ArrayList<UserModel>();
    private static ArrayList<UserModel> mModelDay = new ArrayList<UserModel>();
    private static ArrayList<UserModel> mModelWeek = new ArrayList<UserModel>();
    private static ArrayList<UserModel> mModelMonth = new ArrayList<UserModel>();
    private static ArrayList<UserModel> mModelYear = new ArrayList<UserModel>();

    private static ArrayList<LiveRegionModel> mLiveRegionModels = new ArrayList<LiveRegionModel>();

    private static ArrayList<SystemNotifModel> mSystemModels = new ArrayList<SystemNotifModel>();

    private static ArrayList<PaymentModel> mPaymentModels = new ArrayList<PaymentModel>();

    private static ArrayList<FollowRequestModel> mFollowRequestModel = new ArrayList<FollowRequestModel>();

    private static Handler handler;

    private static Boolean isLogEnabled = false;
    private SharedPreferences sharedPreferences;

    private static String getAbsoluteUrl(String relativeUrl)
    {
        return Constants.SERVER_IP + relativeUrl;
    }

    public interface LoginActionCallback
    {
        public void onResult(boolean success, String message, UserModel user);
    }

    public interface RegisterByPhoneActionCallback
    {
        public void onResult(boolean success, String result,String message ,UserModel user);
    }

    public interface ChangePhoneNumberCallback
    {
        public void onResult(boolean success, String result,String message);
    }

    public interface LoginAction2Callback
    {
        public void onResult(boolean success, String message, UserModel user);
    }

    public interface CheckOpenIDAvaliableCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface checkFacebookIDAvailableCallback
    {
        public void onResult(boolean success, String message);
    }


    public interface RegisterActionCallback
    {
        public void onResult(boolean success, String message, UserModel user);
    }

    public interface SendVerificationCodeCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetUserInfoCallback
    {
        public void onResult(boolean success, String message, UserModel user);
    }

    public interface GetSelfInfoCallback
    {
        public void onResult(boolean success, String message, UserModel userModel);
    }

    public interface BlockUserActionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface ReportUserActionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface ReportPostActionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface ReportCommentActionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface UpdateUserInfoCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface CheckLoginWithCompletionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetUserSuggestionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetFriendSuggestionCallback
    {
        public void onResult(boolean success, String message, ArrayList<UserModel> model);
    }

    public interface FindFriendsByPhoneCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface FreezeUserActionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetNotifCallback
    {
        public void onResult(boolean success, String message, ArrayList<NotifiModel> notifiModel);
    }

    public interface GetFriendNotifCallback
    {
        public void onResult(boolean success, String message, ArrayList<NotifiModel> notifiModel);
    }

    public interface GetSystemNotificationCallback
    {
        public void onResult(boolean success, String message, ArrayList<SystemNotifModel> systemModels);
    }

    public interface ReadSystemNotifWithCallbackCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface ReadNotifCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetPostFeedCallback
    {
        public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedLiveModel);
    }

    public interface GetUserPostCallback
    {
        public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel);
    }

    public interface GetPostInfoCallback
    {
        public void onResult(boolean success, FeedModel feedModel);
    }

    public interface PublishPostCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface LikePostCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface LikePostBatchUpdateCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface UnlikePostCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface CommentPostCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetCommentsCallback
    {
        public void onResult(boolean success, String message, ArrayList<CommentModel> commentModel);
    }

    public interface DeleteCommentCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface DeletePostCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface ClearPostCommentsCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface ViewPostCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetPostByHashTagCallback
    {
        public void onResult(boolean success, String message,ArrayList<FeedModel> feedModel);
    }

    public interface GetPostByLocationCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetHotPostsCallback
    {
        public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel);
    }

    public interface GetLikedPostsCallback
    {
        public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel);
    }

    public interface SearchHashTagCallback
    {
        public void onResult(boolean success, String message, ArrayList<TagModel> tagModel);
    }

    public interface LiveStreamCallback
    {
        public void onResult(boolean success, LiveStreamModel liveStreamModel);
    }

    public interface ReportCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface RemoveReportCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface SuccessCallback
    {
        public void onResult(boolean success);
        public void onProgress(int pos);
    }

    public interface UploadFacebookFriendsCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface UploadPhoneNumbersCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface FindFriendsCallback
    {
        public void onResult(boolean success, ArrayList<UserModel> model);
    }

    public interface FollowUserActionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface UnfollowUserActionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface FollowAllActionCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetSuggestedUsersCallback
    {
        public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested);
    }

    public interface  GetSearchUsersCallback
    {
        public void onResult(boolean success, String message, ArrayList<UserModel> userModel);
    }

    public interface GetCountryCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetRevenueReportCallback
    {
        public void onResult(boolean success, String message ,ArrayList<RevenueModel> revenueModel);
    }

    public interface GetHotUsersCallback
    {
        public void onResult(boolean success, String message ,ArrayList<UserModel> userModel);
    }

    public interface GetPostLikersCallback
    {
        public void onResult(boolean success, String message,ArrayList<UserModel> model);
    }

    public interface GetFollowerCallback
    {
        public void onResult(boolean success, String message,ArrayList<UserModel> model);
    }

    public interface ChangePasswordCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface GetUserLikersCallback
    {
        public void onResult(boolean success, String message,ArrayList<UserModel> model);
    }

    public interface EnterLiveStreamCallback
    {
        public void onResult(boolean success, String message,int timestamp,String colorCode,int numberOfChunks,String ip);
    }

    public interface QuitViewLiveStreamCallback
    {
        public void onResult(boolean success, String message);
    }

    public interface KeepViewLiveStreamCallback
    {
        public void onResult(boolean networkSuccess, boolean keepAliveSuccess, String message, int blocked);
    }

    public interface GetLiveStreamInfoCallback
    {
        public void onResult(boolean success, LiveModel mLiveModel);
    }

    public interface GetLiveStreamViewersCallback
    {
        public void onResult(boolean success ,String message, ArrayList<UserModel> model);
    }

    public interface GetLiveStreamCommentsCallback
    {
        public void onResult(boolean success ,String message, ArrayList<LiveComment> liveComment);
    }

    public interface CommentLiveStreamCallback
    {
        public void onResult(boolean success ,String message);
    }

    public interface HasRestreamedLiveStreamCallback
    {
        public void onResult(boolean success ,String message,int has);
    }

    public interface RestreamLiveStreamCallback
    {
        public void onResult(boolean success ,String message);
    }

    public interface GetHotLiveStreamsCallback
    {
        public void onResult(boolean success ,String message,ArrayList<LiveModel> liveModels);
    }

    public interface GetExploreSuggestedUsersCallback
    {
        public void onResult(boolean success ,ArrayList<SuggestedUsersModel> suggested);
    }

    public interface PublishLiveStreamCallback
    {
        public void onResult(boolean success ,int liveStreamID, int timestamp, String colorCode, String ip);
    }

    public interface EndLiveStreamCallback
    {
        public void onResult(boolean success ,String message);
    }

    public interface KeepLiveStreamCallback
    {
        public void onResult(boolean success ,String message);
    }

    public interface UpdateLiveStreamInfoCallback
    {
        public void onResult(boolean success ,String message);
    }
    
    public interface VerifyUserIdentityCallback
    {
        public void onResult(boolean networkSuccess, boolean success);
    }

    public interface GetLikeLeaderboardCallback
    {
        public void onResult(boolean success ,String message,ArrayList<UserModel> model);
    }

    public interface RecoverPasswordCallback
    {
        public void onResult(boolean networkSuccess, boolean success ,String message);
    }

    public interface IsUserOnLiveStreamCallback
    {
        public void onResult(String liveStreamID, String userIsOnLive);
    }

    public interface RequestCallback
    {
        public void onResult(boolean success);
    }

    public interface SendPhoneVerificationInfoCallback
    {
        public void onResult(boolean success,String result,String message);
    }

    public interface GetResetPasswordTokenCallback
    {
        public void onResult(boolean success,String result,String message, String resetPasswordToken);
    }

    public interface ResetPasswordCallback
    {
        public void onResult(boolean success,String result,String message);
    }

    public interface ValidateHumanTestResponseCallback
    {
        public void onResult(boolean success,String result);
    }

    public interface CheckNeedHumanTestCallback
    {
        public void onResult(boolean success,String result, String isNeeded);
    }

    public interface IsPhoneNumberUsedCallback
    {
        public void onResult(boolean success,String result, String isUsed, String message);
    }

    public interface IsPhoneNumberOpenIDPairedCallback
    {
        public void onResult(boolean success,String result, String isPaired, String message);
    }

    public interface GetFollowRequestsCallback
    {
        public void onResult(boolean success, ArrayList<UserModel> followRequestModel);
    }

    public interface GetPaymentsCallback
    {
        public void onResult(boolean success, ArrayList<PaymentModel> paymentModels);
    }

    public interface LiveRegionCallback
    {
        public void onResult(boolean success, ArrayList<LiveRegionModel> liveRegionModels);
    }

    public interface GetGiftListCallback
    {
        public void onResult(boolean success, ArrayList<GiftModel> giftModel);
    }

    public interface GetPointGainLogCallback
    {
        public void onResult(boolean success, ArrayList<PointGainModel> pointGainModels);
    }

    public interface GetPointUsageLogCallback
    {
        public void onResult(boolean success, ArrayList<PointUsageModel> pointUsageModels);
    }

    public interface GetGiftLeaderboardCallback
    {
        public void onResult(boolean success, ArrayList<GiftLeaderboardModel> giftLeaderboard);
    }
    
     /*Subscriber callback start*/

    public interface SubscriberUserInfoCallback
    {
        public void onResult(boolean success, ArrayList<SubscriberUserModel> SubscriberUserAll);
    }

    public interface SubscribersPurchaseslogCallback
    {
        public void onResult(boolean success, ArrayList<SubscribersPurchaseModel> SubscribersPurchaseAll);
    }

    public interface SubscriptionPurchaseslogCallback
    {
        public void onResult(boolean success, ArrayList<SubscriptionPurchaseModel> SubscriptionPurchaseAll);
    }

    public interface GetSubscriptionProductListCallback
    {
        public void onResult(boolean success, ArrayList<SubscribeOptionModel> subscriptionProductList);
    }

    /*Subscriber callback end*/

    public interface GetLivestreamGiftLeaderboardCallback
    {
        public void onResult(boolean success, GiftLivestreamLeaderBoardModel giftLivestreamLeaderboard);
    }

    public interface GetLiveStreamGiftInfoCallback
    {
        public void onResult(boolean success, LiveGiftsModel liveGiftsModel);
    }

    public interface ProductListCallback
    {
        public void onResult(boolean success, ArrayList<ProductModel> productModels);
    }

    public interface PurchaseProductCallback
    {
        public void onResult(boolean success, int point);
    }

    public interface GetLiveStreamReceivedGiftsCallback
    {
        public void onResult(boolean success, ArrayList<LiveGiftsModel> liveGiftsModels);
    }

    public interface GetUnreadReceivedGiftCallback
    {
        public void onResult(boolean success, ArrayList<ReadGiftModel> readGiftModels);
    }

    public interface GetNewestAppVersionCallback
    {
        public void onResult(boolean success, String newestVersion, String downloadURL);
    }


    public static void loginAction(final Context context, String openID, String password, final LoginActionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("password", Singleton.md5(password));

            post(context, "loginAction", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            final String message = data.getString("message");

                            if (message.equals("ok")) {
                                Singleton.preferenceEditor.putString(Constants.ACCESS_TOKEN, data.getString("accessToken"));
                                Singleton.preferenceEditor.commit();
                                saveUserInfo(context, data.getJSONObject("userInfo"));

                                Gson gson = new Gson();
                                final UserModel mUserModel = gson.fromJson(data.getJSONObject("userInfo").toString(), UserModel.class);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message, mUserModel);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message, null);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void loginAction2(final Context context, String openID, String password, String facebookID, final LoginAction2Callback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("password", Singleton.md5(password));
            params.put("facebookID", facebookID);

            post(context, "loginAction2", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            final String message = data.getString("message");

                            if (message.equals("ok"))
                            {
                                Singleton.preferenceEditor.putString(Constants.ACCESS_TOKEN, data.getString("accessToken"));
                                Singleton.preferenceEditor.commit();
                                saveUserInfo(context, data.getJSONObject("userInfo"));

                                Gson gson = new Gson();
                                final UserModel mUserModel = gson.fromJson(data.getJSONObject("userInfo").toString(), UserModel.class);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message, mUserModel);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message, null);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }


    public static void checkOpenIDAvailable(final Context context, String openID, final CheckOpenIDAvaliableCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);

            post(context, "checkOpenIDAvailable", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void checkFacebookIDAvailable(final Context context, String openID, final checkFacebookIDAvailableCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("facebookID", openID);

            post(context, "checkFacebookIDAvailable", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void registerAction(final Context context, String openID, String password, String name, String age, String gender, String picture/*, String coverPhoto*/, final RegisterActionCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="Register";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);

            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("password", Singleton.md5(password));
            params.put("name", name);
            params.put("age", age);
            params.put("gender", gender);
            params.put("picture", picture);
            params.put("deviceType", "ANDROID");
//            params.put("coverPhoto", coverPhoto);

            post(context, "registerAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");

                            if (message.equals("ok")) {
                                Singleton.preferenceEditor.putString(Constants.ACCESS_TOKEN, data.getString("accessToken"));
                                Singleton.preferenceEditor.commit();
                                saveUserInfo(context,data.getJSONObject("userInfo"));

                                Gson gson = new Gson();
                                final UserModel mUserModel = gson.fromJson(data.getJSONObject("userInfo").toString(), UserModel.class);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message,mUserModel);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message,null);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void registerAction_V2(final Context context, String openID, String password, String facebookID, final RegisterActionCallback callback)
    {
        /*
        var openID = req.body.openID;
		var password = req.body.password;
		var facebookID = req.body.facebookID;
		var deviceType = req.body.deviceType;
		var country = req.headers["cf-ipcountry"];
		var language = req.body.language;
         */
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("facebookID", facebookID);
            params.put("password", Singleton.md5(password));
            params.put("deviceType", "ANDROID");
            params.put("country", "");
            params.put("language", "");

//            params.put("coverPhoto", coverPhoto);

            post(context, "registerAction2", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");

                            if (message.equals("ok")) {
                                Singleton.preferenceEditor.putString(Constants.ACCESS_TOKEN, data.getString("accessToken"));
                                Singleton.preferenceEditor.commit();
                                saveUserInfo(context,data.getJSONObject("userInfo"));

                                Gson gson = new Gson();
                                final UserModel mUserModel = gson.fromJson(data.getJSONObject("userInfo").toString(), UserModel.class);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message,mUserModel);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message,null);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void sendVerificationCode(final Context context, String countryCode, String phoneNumber, final SendVerificationCodeCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("countryCode", countryCode);
            params.put("phoneNumber", phoneNumber);

            post(context, "sendVerificationCode", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getUserInfo(final Context context, String targetOpenID, final GetUserInfoCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetOpenID", targetOpenID);

            post(context, "getUserInfo", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            Gson gson = new Gson();
                            final UserModel mUserModel = gson.fromJson(data.toString(), UserModel.class);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mUserModel);
                                }
                            });
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getSelfInfo(final Context context, String userID, final GetSelfInfoCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);

            post(context, "getSelfInfo", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            final UserModel mUserModel = gson.fromJson(jsonE.getAsJsonObject(), UserModel.class);

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mUserModel);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void unblockUserAction(final Context context, String unblockUserID, final BlockUserActionCallback callback)
    {
        try
        {

            JSONObject params = new JSONObject();
            params.put("unblockUserID", unblockUserID);

            post(context, "unblockUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void blockUserAction(final Context context, String blockUserID, final BlockUserActionCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="BlockUser";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);

            JSONObject params = new JSONObject();
            params.put("blockedUserID", blockUserID);

            post(context, "blockUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getBlockUserList(final Context context, int beforeTime, int count, final GetFriendSuggestionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getBlockUserList", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void reportUserAction(final Context context, String reportedUserID, String reason, String message, final ReportUserActionCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="ReporUser";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context,Umeng_id,mHashMap,0);

            JSONObject params = new JSONObject();
            params.put("reportedUserID", reportedUserID);
            params.put("reason", reason);
            params.put("message", message);

            post(context, "reportUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "");
                                }
                            });
                        } catch (JSONException e) {
                            Singleton.log("exception:");
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {
        }
    }

    public static void reportPostAction(final Context context, String reportedUserID, String reason, String postID, final ReportPostActionCallback callback)
    {

        try
        {
            //Umeng monitor
            String Umeng_id="ReportPost";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context,Umeng_id,mHashMap,0);

            JSONObject params = new JSONObject();
            params.put("reportedUserID", reportedUserID);
            params.put("reason", reason);
            params.put("postID", postID);
            params.put("message", "");

            post(context, "reportPostAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void reportCommentAction(final Context context, String reportedUserID, String reason, String commentID, final ReportCommentActionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("reportedUserID", reportedUserID);
            params.put("reason", reason);
            params.put("commentID", commentID);

            post(context, "reportCommentAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void updateUserInfo(final Context context, JSONObject params, final UpdateUserInfoCallback callback)
    {
        try
        {
            post(context, "updateUserInfo", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void checkLoginWithCompletion(final Context context, final CheckLoginWithCompletionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();

            post(context, "checkLoginWithCompletion", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getUserSuggestion(final Context context, int offset, int count, final GetUserSuggestionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("offset", offset);
            params.put("count", count);

            post(context, "getUserSuggestion", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getFriendSuggestion(final Context context, String friendUserID, int offset, int count, final GetFriendSuggestionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("friendUserID", friendUserID);
            params.put("offset", offset);
            params.put("count", count);

            post(context, "getFriendSuggestion", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void findFriendsByPhone(final Context context, String phoneNumbers, final FindFriendsByPhoneCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("phoneNumbers", phoneNumbers);

            post(context, "findFriendsByPhone", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void freezeUserAction(final Context context, String freezedUserID, final FreezeUserActionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("freezedUserID", freezedUserID);

            post(context, "freezeUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getNotif(final Context context, int beforeTime, int count, final GetNotifCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getNotif", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mNotifiModel.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                NotifiModel notifiModel = new NotifiModel();
                                notifiModel.setUser(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("friendUserInfo"), UserModel.class));

                                JsonObject mPosObject = mJSONArray.get(k).getAsJsonObject().getAsJsonObject("postInfo");

                                if(mPosObject!=null)
                                {
                                    PostModel post = new PostModel();

                                    post.setUser(gson.fromJson(mPosObject.getAsJsonObject("userInfo"), UserModel.class));

                                    post.setPostID(mPosObject.get("postID").getAsString());
                                    post.setUserid(mPosObject.get("userID").getAsString());
                                    post.setCaption(mPosObject.get("caption").getAsString());
                                    post.setTimestamp(mPosObject.get("timestamp").getAsInt());
                                    post.setVideo(mPosObject.get("video").getAsString());
//                                post.setTaggedUsers(mPosObject.get("taggedUsers").getAsInt());
                                    post.setType(mPosObject.get("type").getAsString());
                                    post.setCanComment(mPosObject.get("canComment").getAsInt());
                                    post.setReachability(mPosObject.get("reachability").getAsString());
                                    post.setTotalRevenue(mPosObject.get("totalRevenue").getAsFloat());
                                    post.setLocationName(mPosObject.get("locationName").getAsString());
                                    post.setLocationID(mPosObject.get("locationID").getAsString());
                                    post.setLatitude(mPosObject.get("latitude").getAsFloat());
                                    post.setLongitude(mPosObject.get("longitude").getAsFloat());
                                    post.setLikeCount(mPosObject.get("likeCount").getAsInt());
                                    post.setCommentCount(mPosObject.get("commentCount").getAsInt());
                                    post.setViewCount(mPosObject.get("viewCount").getAsInt());
                                    post.setPicture(mPosObject.get("picture").getAsString());
                                    post.setIsLiked(mPosObject.get("liked").getAsInt());

                                    notifiModel.setPost(post);
                                }

                                notifiModel.setPostID(mJSONArray.get(k).getAsJsonObject().get("postID").getAsString());
                                notifiModel.setFriendUserID(mJSONArray.get(k).getAsJsonObject().get("friendUserID").getAsString());
                                notifiModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                notifiModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                notifiModel.setIsRead(mJSONArray.get(k).getAsJsonObject().get("isRead").getAsInt());
                                notifiModel.setCommentID(mJSONArray.get(k).getAsJsonObject().get("commentID").getAsString());

                                mNotifiModel.add(notifiModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mNotifiModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getFriendNotif(final Context context, int beforeTime, int count, final GetFriendNotifCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getFriendNotif", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mNotifiFriendModel.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                NotifiModel notifiModel = new NotifiModel();
                                notifiModel.setUser(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("friendUserInfo"), UserModel.class));

                                JsonObject mPosObject = mJSONArray.get(k).getAsJsonObject().getAsJsonObject("postInfo");

                                if(mPosObject!=null && mPosObject.toString().length()>2)
                                {
                                    PostModel post = new PostModel();
                                    post.setUser(gson.fromJson(mPosObject.getAsJsonObject("userInfo"), UserModel.class));

                                    post.setPostID(mPosObject.get("postID").getAsString());
                                    post.setUserid(mPosObject.get("userID").getAsString());
                                    post.setCaption(mPosObject.get("caption").getAsString());
                                    post.setTimestamp(mPosObject.get("timestamp").getAsInt());
                                    post.setVideo(mPosObject.get("video").getAsString());
//                                post.setTaggedUsers(mPosObject.get("taggedUsers").getAsInt());
                                    post.setType(mPosObject.get("type").getAsString());
                                    post.setCanComment(mPosObject.get("canComment").getAsInt());
                                    post.setReachability(mPosObject.get("reachability").getAsString());
                                    post.setTotalRevenue(mPosObject.get("totalRevenue").getAsFloat());
                                    post.setLocationName(mPosObject.get("locationName").getAsString());
                                    post.setLocationID(mPosObject.get("locationID").getAsString());
                                    post.setLatitude(mPosObject.get("latitude").getAsFloat());
                                    post.setLongitude(mPosObject.get("longitude").getAsFloat());
                                    post.setLikeCount(mPosObject.get("likeCount").getAsInt());
                                    post.setCommentCount(mPosObject.get("commentCount").getAsInt());
                                    post.setViewCount(mPosObject.get("viewCount").getAsInt());
                                    post.setPicture(mPosObject.get("picture").getAsString());
                                    post.setIsLiked(mPosObject.get("liked").getAsInt());

                                    notifiModel.setPost(post);
                                }

                                notifiModel.setPostID(mJSONArray.get(k).getAsJsonObject().get("postID").getAsString());
                                notifiModel.setFriendUserID(mJSONArray.get(k).getAsJsonObject().get("friendUserID").getAsString());
                                notifiModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                notifiModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());

                                JsonObject mTargetObject = mJSONArray.get(k).getAsJsonObject().getAsJsonObject("targetUserInfo");
                                if(mTargetObject!=null && mTargetObject.toString().length()>2)
                                {
                                    notifiModel.setTargetUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("targetUserInfo"), UserModel.class));
                                }

//                            notifiModel.setIsRead(mJSONArray.get(k).getAsJsonObject().get("isRead").getAsInt());
//                            notifiModel.setCommentID(mJSONArray.get(k).getAsJsonObject().get("commentID").getAsString());

                                mNotifiFriendModel.add(notifiModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mNotifiFriendModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getSystemNotif(final Context context, int beforeTime, int count, final GetSystemNotificationCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getSystemNotif", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mSystemModels.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                SystemNotifModel mSystemNotifModel = new SystemNotifModel();
                                mSystemNotifModel.setMessage(mJSONArray.get(k).getAsJsonObject().get("message").getAsString());
                                mSystemNotifModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                mSystemNotifModel.setIsRead(mJSONArray.get(k).getAsJsonObject().get("isRead").getAsInt());
                                mSystemModels.add(mSystemNotifModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mSystemModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void readSystemNotifWithCallback(final Context context, final ReadSystemNotifWithCallbackCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();

            post(context, "readSystemNotifWithCallback", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void readNotif(final Context context, final ReadNotifCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();

            post(context, "readNotif", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getPostFeed(final Context context, int beforeTime, int count, int fetchLiveStream, final GetPostFeedCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);
            params.put("apiVersion","v2");
            params.put("fetchLiveStream", fetchLiveStream);

            post(context, "getPostFeed", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));

                            mFeedLiveModel.clear();

                            JsonArray mLiveArray = jsonE.getAsJsonObject().getAsJsonArray("liveStreams");

                            for(int k = 0 ; k < mLiveArray.size() ; k++)
                            {
                                LiveFeedModel LivesFeedModel = new LiveFeedModel();

                                LiveModel mLiveModel = new LiveModel();
                                mLiveModel.setUserID(mLiveArray.get(k).getAsJsonObject().get("userID").getAsString());
                                mLiveModel.setUserInfo(gson.fromJson(mLiveArray.get(k).getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));
                                mLiveModel.setCoverPhoto(mLiveArray.get(k).getAsJsonObject().get("coverPhoto").getAsString());
                                mLiveModel.setLiveStreamID(mLiveArray.get(k).getAsJsonObject().get("liveStreamID").getAsInt());
                                mLiveModel.setCaption(mLiveArray.get(k).getAsJsonObject().get("caption").getAsString());
                                mLiveModel.setLocationName(mLiveArray.get(k).getAsJsonObject().get("locationName").getAsString());
                                mLiveModel.setLatitude(mLiveArray.get(k).getAsJsonObject().get("latitude").getAsDouble());
                                mLiveModel.setLongitude(mLiveArray.get(k).getAsJsonObject().get("longitude").getAsDouble());
                                mLiveModel.setShareLocation(mLiveArray.get(k).getAsJsonObject().get("shareLocation").getAsDouble());
                                mLiveModel.setFollowerOnlyChat(mLiveArray.get(k).getAsJsonObject().get("followerOnlyChat").getAsInt());
                                mLiveModel.setChatAvailable(mLiveArray.get(k).getAsJsonObject().get("chatAvailable").getAsInt());
                                mLiveModel.setBeginTime(mLiveArray.get(k).getAsJsonObject().get("beginTime").getAsInt());
                                mLiveModel.setEndTime(mLiveArray.get(k).getAsJsonObject().get("endTime").getAsInt());
                                mLiveModel.setDuration(mLiveArray.get(k).getAsJsonObject().get("duration").getAsInt());
                                mLiveModel.setViewerCount(mLiveArray.get(k).getAsJsonObject().get("viewerCount").getAsInt());
                                mLiveModel.setLiveViewerCount(mLiveArray.get(k).getAsJsonObject().get("liveViewerCount").getAsInt());
                                mLiveModel.setReceivedLikeCount(mLiveArray.get(k).getAsJsonObject().get("receivedLikeCount").getAsInt());
                                mLiveModel.setReplayCount(mLiveArray.get(k).getAsJsonObject().get("replayCount").getAsInt());
                                mLiveModel.setReplayAvailable(mLiveArray.get(k).getAsJsonObject().get("replayAvailable").getAsInt());
                                mLiveModel.setTotalViewTime(mLiveArray.get(k).getAsJsonObject().get("totalViewTime").getAsInt());
                                mLiveModel.setRevenue(mLiveArray.get(k).getAsJsonObject().get("revenue").getAsInt());
                                mLiveModel.setAudioOnly(mLiveArray.get(k).getAsJsonObject().get("audioOnly").getAsInt());
                                mLiveModel.setRestreamerOpenID(mLiveArray.get(k).getAsJsonObject().get("restreamerOpenID").getAsString());

                                LivesFeedModel.setLiveStreams(mLiveModel);
                                mFeedLiveModel.add(LivesFeedModel);
                            }


                            JsonArray mJSONArray = jsonE.getAsJsonObject().getAsJsonArray("posts");

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                LiveFeedModel LivesFeedModel = new LiveFeedModel();

                                FeedModel feedModel = new FeedModel();
                                feedModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));

                                feedModel.setPostID(mJSONArray.get(k).getAsJsonObject().get("postID").getAsString());
                                feedModel.setUserID(mJSONArray.get(k).getAsJsonObject().get("userID").getAsString());
                                feedModel.setCaption(mJSONArray.get(k).getAsJsonObject().get("caption").getAsString());
                                feedModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                feedModel.setPicture(mJSONArray.get(k).getAsJsonObject().get("picture").getAsString());

//                            JsonArray mTagged = mJSONArray.get(k).getAsJsonObject().get("taggedUsers").getAsJsonArray();
//                            int[] t = new int[mTagged.size()];
//                            for(int p = 0 ; p < mTagged.size() ; p++)
//                            {
//                                t[p] = mTagged.get(p).getAsInt();
//                            }
//                            feedModel.setTaggedUsers(t);

                                feedModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                feedModel.setCanComment(mJSONArray.get(k).getAsJsonObject().get("canComment").getAsInt());
                                feedModel.setVideo(mJSONArray.get(k).getAsJsonObject().get("video").getAsString());
                                feedModel.setReachability(mJSONArray.get(k).getAsJsonObject().get("reachability").getAsString());
                                feedModel.setTotalRevenue(mJSONArray.get(k).getAsJsonObject().get("totalRevenue").getAsFloat());
                                feedModel.setLocationName(mJSONArray.get(k).getAsJsonObject().get("locationName").getAsString());
                                feedModel.setLocationID(mJSONArray.get(k).getAsJsonObject().get("locationID").getAsString());
                                feedModel.setLatitude(mJSONArray.get(k).getAsJsonObject().get("latitude").getAsFloat());
                                feedModel.setLongitude(mJSONArray.get(k).getAsJsonObject().get("longitude").getAsFloat());
                                feedModel.setLikeCount(mJSONArray.get(k).getAsJsonObject().get("likeCount").getAsInt());
                                feedModel.setCommentCount(mJSONArray.get(k).getAsJsonObject().get("commentCount").getAsInt());
                                feedModel.setViewCount(mJSONArray.get(k).getAsJsonObject().get("viewCount").getAsInt());
                                feedModel.setLiked(mJSONArray.get(k).getAsJsonObject().get("liked").getAsInt());

                                LivesFeedModel.setPosts(feedModel);
                                mFeedLiveModel.add(LivesFeedModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mFeedLiveModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getUserPost(final Context context,String targetUserID,  int beforeTime, int count, final GetUserPostCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getUserPost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mFeedModel.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                FeedModel feedModel = new FeedModel();
                                feedModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));

                                feedModel.setPostID(mJSONArray.get(k).getAsJsonObject().get("postID").getAsString());
                                feedModel.setUserID(mJSONArray.get(k).getAsJsonObject().get("userID").getAsString());
                                feedModel.setCaption(mJSONArray.get(k).getAsJsonObject().get("caption").getAsString());
                                feedModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                feedModel.setPicture(mJSONArray.get(k).getAsJsonObject().get("picture").getAsString());

//                            JsonArray mTagged = mJSONArray.get(k).getAsJsonObject().get("taggedUsers").getAsJsonArray();
//                            int[] t = new int[mTagged.size()];
//                            for(int p = 0 ; p < mTagged.size() ; p++)
//                            {
//                                t[p] = mTagged.get(p).getAsInt();
//                            }
//                            feedModel.setTaggedUsers(t);

                                feedModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                feedModel.setCanComment(mJSONArray.get(k).getAsJsonObject().get("canComment").getAsInt());
                                feedModel.setVideo(mJSONArray.get(k).getAsJsonObject().get("video").getAsString());
                                feedModel.setReachability(mJSONArray.get(k).getAsJsonObject().get("reachability").getAsString());
                                feedModel.setTotalRevenue(mJSONArray.get(k).getAsJsonObject().get("totalRevenue").getAsFloat());
                                feedModel.setLocationName(mJSONArray.get(k).getAsJsonObject().get("locationName").getAsString());
                                feedModel.setLocationID(mJSONArray.get(k).getAsJsonObject().get("locationID").getAsString());
                                feedModel.setLatitude(mJSONArray.get(k).getAsJsonObject().get("latitude").getAsFloat());
                                feedModel.setLongitude(mJSONArray.get(k).getAsJsonObject().get("longitude").getAsFloat());
                                feedModel.setLikeCount(mJSONArray.get(k).getAsJsonObject().get("likeCount").getAsInt());
                                feedModel.setCommentCount(mJSONArray.get(k).getAsJsonObject().get("commentCount").getAsInt());
                                feedModel.setViewCount(mJSONArray.get(k).getAsJsonObject().get("viewCount").getAsInt());
                                feedModel.setLiked(mJSONArray.get(k).getAsJsonObject().get("liked").getAsInt());

                                mFeedModel.add(feedModel);
                            }



                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mFeedModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getPostInfo(final Context context, String postID, final GetPostInfoCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "getPostInfo", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));

                            final FeedModel feedModel = new FeedModel();
                            feedModel.setUserInfo(gson.fromJson(jsonE.getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));

                            feedModel.setPostID(jsonE.getAsJsonObject().get("postID").getAsString());
                            feedModel.setUserID(jsonE.getAsJsonObject().get("userID").getAsString());
                            feedModel.setCaption(jsonE.getAsJsonObject().get("caption").getAsString());
                            feedModel.setTimestamp(jsonE.getAsJsonObject().get("timestamp").getAsInt());
                            feedModel.setPicture(jsonE.getAsJsonObject().get("picture").getAsString());
                            feedModel.setType(jsonE.getAsJsonObject().get("type").getAsString());
                            feedModel.setCanComment(jsonE.getAsJsonObject().get("canComment").getAsInt());
                            feedModel.setVideo(jsonE.getAsJsonObject().get("video").getAsString());
                            feedModel.setReachability(jsonE.getAsJsonObject().get("reachability").getAsString());
                            feedModel.setTotalRevenue(jsonE.getAsJsonObject().get("totalRevenue").getAsFloat());
                            feedModel.setLocationName(jsonE.getAsJsonObject().get("locationName").getAsString());
                            feedModel.setLocationID(jsonE.getAsJsonObject().get("locationID").getAsString());
                            feedModel.setLatitude(jsonE.getAsJsonObject().get("latitude").getAsFloat());
                            feedModel.setLongitude(jsonE.getAsJsonObject().get("longitude").getAsFloat());
                            feedModel.setLikeCount(jsonE.getAsJsonObject().get("likeCount").getAsInt());
                            feedModel.setCommentCount(jsonE.getAsJsonObject().get("commentCount").getAsInt());
                            feedModel.setViewCount(jsonE.getAsJsonObject().get("viewCount").getAsInt());
                            feedModel.setLiked(jsonE.getAsJsonObject().get("liked").getAsInt());

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, feedModel);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

//    public static void publishPost(final Context context, String userID, String caption, String taggedUsers, String video, String picture, String locationData, String locationID, String type, String canComment, String reachability, final PublishPostCallback callback)
//    {
//        try
//        {
//
//            JSONObject params = new JSONObject();
//            params.put("userID",userID);
//            params.put("caption",caption);
//            params.put("taggedUsers",taggedUsers);
//            params.put("video",video);
//            params.put("picture",picture);
//            params.put("locationData",locationData);
//            params.put("locationID",locationID);
//            params.put("type",type);
//            params.put("canComment", canComment);
//            params.put("reachability", reachability);
//
//            post(context, "publishPost", params, new TextHttpResponseHandler() {
//                @Override
//                public void onSuccess(int i, Header[] headers, String s) {
//                    try {
//                        JSONObject data = new JSONObject(getDecodeText(context, s));
//                        String result = data.getString("result");
//                        String message = data.getString("message");
//                        if (result.equals("success")) {
//                            callback.onResult(true, message);
//                        } else callback.onResult(false, message);
//                    } catch (JSONException e) {
//                        callback.onResult(false, "");
//                    }
//                }
//
//                @Override
//                public void onFailure(int i, Header[] headers, String s, Throwable throwable) {
//                    callback.onResult(false, "");
//                }
//            });
//
//        }
//        catch(Exception e)
//        {
//
//        }
//    }

    public static void likePost(final Context context, String postID, final LikePostCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="LikePost";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);


            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "likePost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void likePostBatchUpdate(final Context context, String postID, int likeCount, final LikePostBatchUpdateCallback callback)
    {
        try
        {

            JSONObject params = new JSONObject();
            params.put("postID", postID);
            params.put("likeCount", likeCount);

            post(context, "likePostBatchUpdate", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void unlikePost(final Context context, String postID, final UnlikePostCallback callback)
    {
        try
        {

            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "unlikePost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void commentPost(final Context context, String postID, String comment, String userTags, final CommentPostCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="CommentPost";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context,Umeng_id,mHashMap,0);

            JSONObject params = new JSONObject();
            params.put("postID", postID);
            params.put("comment", comment);
            params.put("userTags", userTags);

            post(context, "commentPost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {

                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getComments(final Context context, String postID, int beforeTime, int count, final GetCommentsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getComments", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mCommentModel.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                CommentModel commentModel = new CommentModel();
                                commentModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));

                                commentModel.setCommentID(mJSONArray.get(k).getAsJsonObject().get("commentID").getAsString());
                                commentModel.setComment(mJSONArray.get(k).getAsJsonObject().get("comment").getAsString());
                                commentModel.setUserID(mJSONArray.get(k).getAsJsonObject().get("userID").getAsString());
                                commentModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());

                                mCommentModel.add(commentModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mCommentModel);
                                }
                            });
                        }
                        catch (OutOfMemoryError o)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void deleteComment(final Context context, String postID, String commentID, final DeleteCommentCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);
            params.put("commentID", commentID);

            post(context, "deleteComment", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void deletePost(final Context context, String postID, final DeletePostCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "deletePost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void clearPostComments(final Context context, String postID, final ClearPostCommentsCallback callback)
    {
        try
        {

            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "clearPostComments", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void viewPost(final Context context, String postsID, final ViewPostCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postsID", postsID);

            post(context, "viewPost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getPostByHashTag(final Context context, String userID, String hashTag, int beforeTime, int count, final GetPostByHashTagCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID",userID);
            params.put("hashTag",hashTag);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getPostByHashTag", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mFeedModel.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                FeedModel feedModel = new FeedModel();
                                feedModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));

                                feedModel.setPostID(mJSONArray.get(k).getAsJsonObject().get("postID").getAsString());
                                feedModel.setUserID(mJSONArray.get(k).getAsJsonObject().get("userID").getAsString());
                                feedModel.setCaption(mJSONArray.get(k).getAsJsonObject().get("caption").getAsString());
                                feedModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                feedModel.setPicture(mJSONArray.get(k).getAsJsonObject().get("picture").getAsString());

//                            JsonArray mTagged = mJSONArray.get(k).getAsJsonObject().get("taggedUsers").getAsJsonArray();
//                            int[] t = new int[mTagged.size()];
//                            for(int p = 0 ; p < mTagged.size() ; p++)
//                            {
//                                t[p] = mTagged.get(p).getAsInt();
//                            }
//                            feedModel.setTaggedUsers(t);

                                feedModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                feedModel.setCanComment(mJSONArray.get(k).getAsJsonObject().get("canComment").getAsInt());
                                feedModel.setVideo(mJSONArray.get(k).getAsJsonObject().get("video").getAsString());
                                feedModel.setReachability(mJSONArray.get(k).getAsJsonObject().get("reachability").getAsString());
                                feedModel.setTotalRevenue(mJSONArray.get(k).getAsJsonObject().get("totalRevenue").getAsFloat());
                                feedModel.setLocationName(mJSONArray.get(k).getAsJsonObject().get("locationName").getAsString());
                                feedModel.setLocationID(mJSONArray.get(k).getAsJsonObject().get("locationID").getAsString());
                                feedModel.setLatitude(mJSONArray.get(k).getAsJsonObject().get("latitude").getAsFloat());
                                feedModel.setLongitude(mJSONArray.get(k).getAsJsonObject().get("longitude").getAsFloat());
                                feedModel.setLikeCount(mJSONArray.get(k).getAsJsonObject().get("likeCount").getAsInt());
                                feedModel.setCommentCount(mJSONArray.get(k).getAsJsonObject().get("commentCount").getAsInt());
                                feedModel.setViewCount(mJSONArray.get(k).getAsJsonObject().get("viewCount").getAsInt());
                                feedModel.setLiked(mJSONArray.get(k).getAsJsonObject().get("liked").getAsInt());

                                mFeedModel.add(feedModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mFeedModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getPostByLocation(final Context context, String locationID, int beforeTime, int count, final GetPostByLocationCallback callback)
    {
        try
        {

            JSONObject params = new JSONObject();
            params.put("locationID",locationID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getPostByLocation", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getHotPost(final Context context, int beforeTime, int count, final GetHotPostsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getHotPost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            //Log.d("17_g","[getHotPost] : "+ jsonE.toString());
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mFeedModel.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                FeedModel feedModel = new FeedModel();
                                feedModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));

                                feedModel.setPostID(mJSONArray.get(k).getAsJsonObject().get("postID").getAsString());
                                feedModel.setUserID(mJSONArray.get(k).getAsJsonObject().get("userID").getAsString());
                                feedModel.setCaption(mJSONArray.get(k).getAsJsonObject().get("caption").getAsString());
                                feedModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                feedModel.setPicture(mJSONArray.get(k).getAsJsonObject().get("picture").getAsString());

//                            JsonArray mTagged = mJSONArray.get(k).getAsJsonObject().get("taggedUsers").getAsJsonArray();
//                            int[] t = new int[mTagged.size()];
//                            for(int p = 0 ; p < mTagged.size() ; p++)
//                            {
//                                t[p] = mTagged.get(p).getAsInt();
//                            }
//                            feedModel.setTaggedUsers(t);

                                feedModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                feedModel.setCanComment(mJSONArray.get(k).getAsJsonObject().get("canComment").getAsInt());
                                feedModel.setVideo(mJSONArray.get(k).getAsJsonObject().get("video").getAsString());
                                feedModel.setReachability(mJSONArray.get(k).getAsJsonObject().get("reachability").getAsString());
                                feedModel.setTotalRevenue(mJSONArray.get(k).getAsJsonObject().get("totalRevenue").getAsFloat());
                                feedModel.setLocationName(mJSONArray.get(k).getAsJsonObject().get("locationName").getAsString());
                                feedModel.setLocationID(mJSONArray.get(k).getAsJsonObject().get("locationID").getAsString());
                                feedModel.setLatitude(mJSONArray.get(k).getAsJsonObject().get("latitude").getAsFloat());
                                feedModel.setLongitude(mJSONArray.get(k).getAsJsonObject().get("longitude").getAsFloat());
                                feedModel.setLikeCount(mJSONArray.get(k).getAsJsonObject().get("likeCount").getAsInt());
                                feedModel.setCommentCount(mJSONArray.get(k).getAsJsonObject().get("commentCount").getAsInt());
                                feedModel.setViewCount(mJSONArray.get(k).getAsJsonObject().get("viewCount").getAsInt());
                                feedModel.setLiked(mJSONArray.get(k).getAsJsonObject().get("liked").getAsInt());

                                mFeedModel.add(feedModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mFeedModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getLikedPost(final Context context, String targetUserID, int beforeTime, int count, final GetLikedPostsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getLikedPost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mFeedModel.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                try {
                                    FeedModel feedModel = new FeedModel();
                                    feedModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));

                                    feedModel.setPostID(mJSONArray.get(k).getAsJsonObject().get("postID").getAsString());
                                    feedModel.setUserID(mJSONArray.get(k).getAsJsonObject().get("userID").getAsString());
                                    feedModel.setCaption(mJSONArray.get(k).getAsJsonObject().get("caption").getAsString());
                                    feedModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                    feedModel.setPicture(mJSONArray.get(k).getAsJsonObject().get("picture").getAsString());

//                            JsonArray mTagged = mJSONArray.get(k).getAsJsonObject().get("taggedUsers").getAsJsonArray();
//                            int[] t = new int[mTagged.size()];
//                            for(int p = 0 ; p < mTagged.size() ; p++)
//                            {
//                                t[p] = mTagged.get(p).getAsInt();
//                            }
//                            feedModel.setTaggedUsers(t);

                                    feedModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                    feedModel.setCanComment(mJSONArray.get(k).getAsJsonObject().get("canComment").getAsInt());
                                    feedModel.setVideo(mJSONArray.get(k).getAsJsonObject().get("video").getAsString());
                                    feedModel.setReachability(mJSONArray.get(k).getAsJsonObject().get("reachability").getAsString());
                                    feedModel.setTotalRevenue(mJSONArray.get(k).getAsJsonObject().get("totalRevenue").getAsFloat());
                                    feedModel.setLocationName(mJSONArray.get(k).getAsJsonObject().get("locationName").getAsString());
                                    feedModel.setLocationID(mJSONArray.get(k).getAsJsonObject().get("locationID").getAsString());
                                    feedModel.setLatitude(mJSONArray.get(k).getAsJsonObject().get("latitude").getAsFloat());
                                    feedModel.setLongitude(mJSONArray.get(k).getAsJsonObject().get("longitude").getAsFloat());
                                    feedModel.setLikeCount(mJSONArray.get(k).getAsJsonObject().get("likeCount").getAsInt());
                                    feedModel.setCommentCount(mJSONArray.get(k).getAsJsonObject().get("commentCount").getAsInt());
                                    feedModel.setViewCount(mJSONArray.get(k).getAsJsonObject().get("viewCount").getAsInt());
                                    feedModel.setLiked(mJSONArray.get(k).getAsJsonObject().get("liked").getAsInt());

                                    mFeedModel.add(feedModel);
                                } catch (Exception e) {

                                }
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mFeedModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void searchHashTag(final Context context, String query, int offset, int count, final SearchHashTagCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="SearchHashTag";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context,Umeng_id,mHashMap,0);

            JSONObject params = new JSONObject();
            params.put("query",query);
            params.put("offset", offset);
            params.put("count", count);

            post(context, "searchHashTag", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mTagModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mTagModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), TagModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mTagModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void report(final Context context, String postID, final ReportCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "report", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void removeReport(final Context context, String postID, final RemoveReportCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "removeReport", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void uploadFacebookFriends(final Context context, String userID, String myFacebookName, JSONArray array, final UploadFacebookFriendsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("myFacebookName",myFacebookName);
            params.put("facebookData", array.toString());

            post(context, "uploadFacebookFriends", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void uploadInstagramFriends(final Context context, String myInstagramName, JSONArray array, final UploadFacebookFriendsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("myInstagramName",myInstagramName);
            params.put("instagramData", array.toString());

            post(context, "uploadInstagramFriends", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void uploadPhoneNumbers(final Context context, String userID, String myPhoneNumber, String countryCode, JSONArray array, final UploadPhoneNumbersCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("myPhoneNumber",myPhoneNumber);
            params.put("countryCode",countryCode);
            params.put("phoneNumberData", array.toString());

            post(context, "uploadPhoneNumbers", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void findFriends(final Context context, String userID, String array, String method, final FindFriendsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("jsonData", array);
            params.put("method", method);

            post(context, "findFriends", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++) {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            if (mModel.size() != 0) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, mModel);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, mModel);
                                    }
                                });
                            }
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void followUserAction(final Context context, String userID, String follow_userID, final FollowUserActionCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="FollowUser";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);


            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", follow_userID);

            post(context, "followUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void unfollowUserAction(final Context context, String userID, String follow_userID, final UnfollowUserActionCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="UnFollowUser";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);


            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", follow_userID);

            post(context, "unfollowUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void followAllAction(final Context context, String userID, String array, final FollowAllActionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("followedUserIDs", array);
//            params.put("sendPushNotif",0);

            post(context, "followAllAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getSuggestedUsers(final Context context, String userID, int offset, int count, final GetSuggestedUsersCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("offset", offset);
            params.put("count", count);

            post(context, "getSuggestedUsers", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mArray = jsonE.getAsJsonArray();

                            mSuggesteds.clear();

                            for(int k = 0 ; k < mArray.size() ; k++)
                            {
                                SuggestedUsersModel mSuggested = new SuggestedUsersModel();
                                mSuggested.setUserID(mArray.get(k).getAsJsonObject().get("userID").getAsString());
                                mSuggested.setOpenID(mArray.get(k).getAsJsonObject().get("openID").getAsString());
                                mSuggested.setName(mArray.get(k).getAsJsonObject().get("name").getAsString());
                                mSuggested.setPicture(mArray.get(k).getAsJsonObject().get("picture").getAsString());
                                mSuggested.setFollowRequestTime(0);
                                mSuggested.setPrivacyMode(mArray.get(k).getAsJsonObject().get("privacyMode").getAsString());
                                mSuggested.setIsVerified(mArray.get(k).getAsJsonObject().get("isVerified").getAsInt());
                                JsonArray mPostsArray = mArray.get(k).getAsJsonObject().getAsJsonArray("pictures");

                                ArrayList<PostModel> mPosts = new ArrayList<PostModel>();

                                for(int j = 0 ; j < mPostsArray.size() ; j++)
                                {
                                    PostModel mPostModel = new PostModel();
                                    mPostModel.setPicture(mPostsArray.get(j).getAsJsonObject().get("picture").getAsString());
                                    mPosts.add(mPostModel);
                                }

                                mSuggested.setPostInfo(mPosts);
                                mSuggesteds.add(mSuggested);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, mSuggesteds);
                                }
                            });
                        }
                        catch(Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getSearchUsers(final Context context, String query, int offset, int count, int followingOnly, final GetSearchUsersCallback callback)
    {
        try
        {
            //Umeng Monitor
            String Umeng_id="SearchUser";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);

            JSONObject params = new JSONObject();
            params.put("query", query);
            params.put("count", count);
            params.put("followingOnly", followingOnly);
            params.put("offset", offset);

            post(context, "getSearchUsers", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++) {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModel);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void publishPost(final Context context, String userID, String caption, String userTags , String hashTags , String picture, String video, String type, final PublishPostCallback callback)
    {
        try
        {
            //Umeng Monitor
            String Umeng_id="PublishPost";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context,Umeng_id,mHashMap,0);

            JSONObject params = new JSONObject();
            params.put("userID",userID);
            params.put("caption",caption);
            params.put("userTags",userTags);
            params.put("hashTags",hashTags);
            params.put("taggedUsers","");
            params.put("video",video);
            params.put("picture",picture);
            params.put("locationID","");
            params.put("locationName","");
            params.put("latitude", 0);
            params.put("longitude",0);
            params.put("type",type);
            params.put("canComment",1);
            params.put("reachability","");

            post(context, "publishPost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, "");
                                    }
                                });
                            }
                            else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, "");
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getCountry(final Context context, final GetCountryCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();

            post(context, "getCountry", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {


                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            final String country = data.getString("country");

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, country);
                                }
                            });
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getRevenueReport(final Context context, String userID, int minTimestamp , int maxTimestamp ,final GetRevenueReportCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID",userID);
            params.put("minTimestamp",minTimestamp);
            params.put("maxTimestamp", maxTimestamp);

            post(context, "getRevenueReport", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mRevenueModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++) {
                                mRevenueModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), RevenueModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mRevenueModel);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getHotUsers(final Context context, String userID, int offset, int count, final GetHotUsersCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("offset", offset);
            params.put("count", count);

            post(context, "getHotUsers", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++) {
                                UserModel mUserModel = new UserModel();
                                mUserModel.setUserID(mJSONArray.get(k).getAsJsonObject().get("userID").getAsString());
                                mUserModel.setOpenID(mJSONArray.get(k).getAsJsonObject().get("openID").getAsString());
                                mUserModel.setPrivacyMode(mJSONArray.get(k).getAsJsonObject().get("privacyMode").getAsString());
                                mUserModel.setPhoneNumber(mJSONArray.get(k).getAsJsonObject().get("phoneNumber").getAsString());
                                mUserModel.setName(mJSONArray.get(k).getAsJsonObject().get("name").getAsString());
                                mUserModel.setBio(mJSONArray.get(k).getAsJsonObject().get("bio").getAsString());
                                mUserModel.setPicture(mJSONArray.get(k).getAsJsonObject().get("picture").getAsString());
                                mUserModel.setCoverPhoto(mJSONArray.get(k).getAsJsonObject().get("coverPhoto").getAsString());
                                mUserModel.setWebsite(mJSONArray.get(k).getAsJsonObject().get("website").getAsString());
                                mUserModel.setAge(mJSONArray.get(k).getAsJsonObject().get("age").getAsInt());
                                mUserModel.setGender(mJSONArray.get(k).getAsJsonObject().get("gender").getAsString());
                                mUserModel.setIsVerified(mJSONArray.get(k).getAsJsonObject().get("isVerified").getAsInt());
                                mUserModel.setFollowerCount(mJSONArray.get(k).getAsJsonObject().get("followerCount").getAsInt());
                                mUserModel.setFollowingCount(mJSONArray.get(k).getAsJsonObject().get("followingCount").getAsInt());
                                mUserModel.setPostCount(mJSONArray.get(k).getAsJsonObject().get("postCount").getAsInt());
                                mUserModel.setRepostCount(mJSONArray.get(k).getAsJsonObject().get("repostCount").getAsInt());
                                mUserModel.setLikePostCount(mJSONArray.get(k).getAsJsonObject().get("likePostCount").getAsInt());
                                mUserModel.setIsChoice(mJSONArray.get(k).getAsJsonObject().get("isChoice").getAsInt());
                                mUserModel.setLastLogin(mJSONArray.get(k).getAsJsonObject().get("lastLogin").getAsInt());
                                mUserModel.setIsBlocked(mJSONArray.get(k).getAsJsonObject().get("isBlocked").getAsInt());
                                mUserModel.setIsFollowing(mJSONArray.get(k).getAsJsonObject().get("isFollowing").getAsInt());

                                mModel.add(mUserModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModel);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getPostLikers(final Context context, String userID, String postID, int offset, int count, final GetPostLikersCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("postID", postID);
            params.put("offset", offset);
            params.put("count", count);

            post(context, "getPostLikers", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            String Umeng_id="LikePost";
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put(Umeng_id, Umeng_id);
                            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getFollower(final Context context, String userID, String targetUserID, int beforeTime, int count, final GetFollowerCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", targetUserID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getFollower", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void verifyUserIdentity(final Context context, String userID, String accessToken, final VerifyUserIdentityCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("accessToken", accessToken);

            post(context, "verifyUserIdentity", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");

                            if(result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, true);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, false);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, false);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getFollowing(final Context context, String userID, String targetUserID, int beforeTime, int count, final GetFollowerCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", targetUserID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getFollowing", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void changePassword(final Context context, String userID, String oldPassword, String newPassword, final ChangePasswordCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("oldPassword", Singleton.md5(oldPassword));
            params.put("newPassword", Singleton.md5(newPassword));

            post(context, "changePassword", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getUserLikers(final Context context, String userID, String targetUserID, int offset, int count, final GetUserLikersCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", targetUserID);
            params.put("offset", offset);
            params.put("count", count);

            post(context, "getUserLikers", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLiveStreamInfo(final Context context, int liveStreamID, final LiveStreamCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("liveStreamID", liveStreamID);

            post(context, "getLiveStreamInfo", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            String json = getDecodeText(context, response.body().string());

                            Gson gson = new Gson();
                            JSONObject jsonObject = new JSONObject(json);

                            final LiveStreamModel liveStreamModel = new LiveStreamModel();
                            liveStreamModel.liveStreamID = jsonObject.getInt("liveStreamID");
                            liveStreamModel.numberOfChunks = jsonObject.getInt("numberOfChunks");
                            liveStreamModel.canSendGift = jsonObject.getInt("canSendGift");
                            liveStreamModel.canSendGiftWithTextAndVoice = jsonObject.getInt("canSendGiftWithTextAndVoice");

                            UserModel userModel = new UserModel();
                            userModel.setOpenID(jsonObject.getJSONObject("userInfo").getString("openID"));
                            userModel.setPicture(jsonObject.getJSONObject("userInfo").getString("picture"));
                            liveStreamModel.user = userModel;
                            liveStreamModel.caption = jsonObject.getString("caption");

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, liveStreamModel);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void enterLiveStream(final Context context, String userID, int liveStreamID, final EnterLiveStreamCallback callback)
    {
        try
        {
            //Umeng monitors
            String Umeng_id="WatchLiveCount";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);

            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);

            post(context, "enterLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success"))
                            {
                                final int timestamp = data.getInt("timestamp");
                                final String colorCode = data.getString("colorCode");
                                final int numberOfChunks = data.getInt("numberOfChunks");

                                if(data.has("publicIP"))
                                {
                                    final String ip = data.getString("publicIP");
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            callback.onResult(true, message,timestamp,colorCode,numberOfChunks,ip);
                                        }
                                    });
                                }
                                else
                                {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            callback.onResult(true, message,timestamp,colorCode,numberOfChunks,"");
                                        }
                                    });
                                }
                            }
                            else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message,0,"",0,"");
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",0,"",0,"");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",0,"",0,"");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",0,"",0,"");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {
        }
    }

    public static void quitViewLiveStream(final Context context, String userID, int liveStreamID, int duration, final QuitViewLiveStreamCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("duration", duration);

            post(context, "quitViewLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true, "");
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {
        }
    }

    public static void keepViewLiveStream(final Context context, String userID, int liveStreamID, int isPreview, final KeepViewLiveStreamCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("isPreview", isPreview);

            post(context, "keepViewLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");

                            if (result.equals("success"))
                            {
                                final int blocked = data.getInt("blocked");
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, true, message,blocked);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, false, message,0);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, false, "",0);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, false, "",0);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, false, "",0);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLiveStreamInfo(final Context context, String userID, int liveStreamID, final GetLiveStreamInfoCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);

            post(context, "getLiveStreamInfo", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));

                            final LiveModel mLiveModel = new LiveModel();
                            mLiveModel.setUserID(data.getString("userID"));
//                        mLiveModel.setUserInfo(gson.fromJson(, UserModel.class));
                            mLiveModel.setCoverPhoto(data.getString("coverPhoto"));
                            mLiveModel.setLiveStreamID(data.getInt("liveStreamID"));
                            mLiveModel.setCaption(data.getString("caption"));
                            mLiveModel.setLocationName(data.getString("locationName"));
                            mLiveModel.setLatitude(data.getDouble("latitude"));
                            mLiveModel.setLongitude(data.getDouble("longitude"));
                            mLiveModel.setShareLocation(data.getDouble("shareLocation"));
                            mLiveModel.setFollowerOnlyChat(data.getInt("followerOnlyChat"));
                            mLiveModel.setChatAvailable(data.getInt("chatAvailable"));
                            mLiveModel.setBeginTime(data.getInt("beginTime"));
                            mLiveModel.setEndTime(data.getInt("endTime"));
                            mLiveModel.setDuration(data.getInt("duration"));
                            mLiveModel.setViewerCount(data.getInt("viewerCount"));
                            mLiveModel.setLiveViewerCount(data.getInt("liveViewerCount"));
                            mLiveModel.setReceivedLikeCount(data.getInt("receivedLikeCount"));
                            mLiveModel.setReplayCount(data.getInt("replayCount"));
                            mLiveModel.setReplayAvailable(data.getInt("replayAvailable"));
                            mLiveModel.setTotalViewTime(data.getInt("totalViewTime"));
                            mLiveModel.setRevenue((float)data.getDouble("revenue"));
                            mLiveModel.setAudioOnly(data.getInt("audioOnly"));
                            mLiveModel.setCanSendGift(data.getInt("canSendGift"));
                            mLiveModel.setCanSendGiftWithTextAndVoice(data.getInt("canSendGiftWithTextAndVoice"));

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, mLiveModel);
                                }
                            });
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {
        }
    }

    public static void getLiveStreamViewers(final Context context, String userID, int liveStreamID, int afterTime, int count, final GetLiveStreamViewersCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("afterTime", afterTime);
            params.put("count", count);

            post(context, "getLiveStreamViewers", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {
        }
    }

    public static void likeLiveStream(final Context context, String userID, int liveStreamID, int absTimestamp)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="LikeLive";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);

            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("absTimestamp", absTimestamp);

            post(context, "likeLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {

                    }
                    else
                    {

                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {

                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void likeLivestreamBatchUpdate(final Context context, String userID, int liveStreamID, int likeCount)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("likeCount", likeCount);

            post(context, "likeLivestreamBatchUpdate", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {

                    }
                    else
                    {

                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {

                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLiveStreamComments(final Context context, String userID, int liveStreamID, int afterTime, int count,final GetLiveStreamCommentsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("afterTime", afterTime);
            params.put("count", count);

            post(context, "getLiveStreamComments", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mLiveComments.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mLiveComments.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), LiveComment.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mLiveComments);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void commentLiveStream(final Context context, String userID, int liveStreamID, String comment, String  colorCode, int absTimestamp, final CommentLiveStreamCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="CommentInLiveStream";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context,Umeng_id,mHashMap,0);

            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("comment", comment);
            params.put("colorCode", colorCode);
            params.put("absTimestamp", absTimestamp);

            post(context, "commentLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {

                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void hasRestreamedLiveStream(final Context context, String userID, int liveStreamID, final HasRestreamedLiveStreamCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);

            post(context, "hasRestreamedLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            final int result = data.getInt("hasRestreamed");
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",result);
                                }
                            });
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",1);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",1);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",1);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {
        }
    }

    public static void restreamLiveStream(final Context context, String userID, int liveStreamID, String myOpenID, String liveStreamerOpenID, final RestreamLiveStreamCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("myOpenID", myOpenID);
            params.put("liveStreamerOpenID", liveStreamerOpenID);

            post(context, "restreamLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "");
                                }
                            });
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {
        }
    }

    public static void getHotLiveStreams(final Context context, String userID, final GetHotLiveStreamsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);

            post(context, "getHotLiveStreams", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mLiveModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mLiveModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), LiveModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mLiveModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getExploreSuggestedUsers(final Context context, String userID, int beforeTime, int count, final GetExploreSuggestedUsersCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getExploreSuggestedUsers", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mArray = jsonE.getAsJsonArray();

                            mSuggesteds.clear();

                            for(int k = 0 ; k < mArray.size() ; k++)
                            {
                                SuggestedUsersModel mSuggested = new SuggestedUsersModel();
                                mSuggested.setUserID(mArray.get(k).getAsJsonObject().get("userID").getAsString());
                                mSuggested.setOpenID(mArray.get(k).getAsJsonObject().get("openID").getAsString());
                                mSuggested.setName(mArray.get(k).getAsJsonObject().get("name").getAsString());
                                mSuggested.setPicture(mArray.get(k).getAsJsonObject().get("picture").getAsString());

                                mSuggested.setIsFollowing(mArray.get(k).getAsJsonObject().get("isFollowing").getAsInt());
                                mSuggested.setPostCount(mArray.get(k).getAsJsonObject().get("postCount").getAsInt());
                                mSuggested.setFollowerCount(mArray.get(k).getAsJsonObject().get("followerCount").getAsInt());
                                mSuggested.setFollowingCount(mArray.get(k).getAsJsonObject().get("followingCount").getAsInt());
                                mSuggested.setBio(mArray.get(k).getAsJsonObject().get("bio").getAsString());
                                mSuggested.setWebsite(mArray.get(k).getAsJsonObject().get("website").getAsString());
                                mSuggested.setLastLogin(mArray.get(k).getAsJsonObject().get("lastLogin").getAsInt());
                                mSuggested.setIsVerified(mArray.get(k).getAsJsonObject().get("isVerified").getAsInt());

                                JsonArray mPostsArray = mArray.get(k).getAsJsonObject().getAsJsonArray("pictures");

                                ArrayList<PostModel> mPosts = new ArrayList<PostModel>();

                                for(int j = 0 ; j < mPostsArray.size() ; j++)
                                {
                                    PostModel mPostModel = new PostModel();
                                    mPostModel.setPicture(mPostsArray.get(j).getAsJsonObject().get("picture").getAsString());
                                    mPostModel.setTimestamp(mPostsArray.get(j).getAsJsonObject().get("timestamp").getAsInt());
                                    mPosts.add(mPostModel);
                                }

                                mSuggested.setPostInfo(mPosts);
                                mSuggesteds.add(mSuggested);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, mSuggesteds);
                                }
                            });
                        }
                        catch(Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void publishLiveStream(final Context context, String userID, String caption, final PublishLiveStreamCallback callback)
    {
        try
        {
            String Umeng_id="PublishLiveStream";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context,Umeng_id,mHashMap,0);

            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("caption", caption);

            params.put("locationName", Singleton.preferences.getString(Constants.LIVE_MY_CITY, "Taiwan"));
            params.put("coverPhoto", "");
            params.put("latitude", Singleton.preferences.getFloat(Constants.LIVE_MY_LAT, 25));
            params.put("longitude", Singleton.preferences.getFloat(Constants.LIVE_MY_LON, 125));
            params.put("shareLocation", 0);
            params.put("followerOnlyChat", 0);
            params.put("videoCodec", "vp8");
            params.put("canSendGift", 1);
            params.put("hashtags", "");
            params.put("canSendGiftWithTextAndVoice", 0);

            post(context, "publishLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {

                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));

                            final int liveStreamID = data.getInt("liveStreamID");

                            if (liveStreamID!=0)
                            {
                                final int timestamp = data.getInt("timestamp");
                                final String colorCode = data.getString("colorCode");

                                if(data.has("publicIP"))
                                {
                                    final String ip = data.getString("publicIP");
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            callback.onResult(true,liveStreamID,timestamp,colorCode,ip);
                                        }
                                    });
                                }
                                else
                                {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            callback.onResult(true,liveStreamID,timestamp,colorCode,"");
                                        }
                                    });
                                }
                            }
                            else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false,0,0,"","");
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,0,0,"","");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, 0,0,"","");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, 0,0,"","");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void endLiveStream(final Context context, String userID, int liveStreamID, int duration, final EndLiveStreamCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("duration", duration);

            post(context, "endLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "");
                                }
                            });
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,"");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,"");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,"");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void keepLiveStream(final Context context, String userID, int liveStreamID, final KeepLiveStreamCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);

            post(context, "keepLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));

                            String result = data.getString("result");
                            final String message = data.getString("message");

                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else {
                                if(message.compareTo("live_killed")==0) {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            callback.onResult(true, message);
                                        }
                                    });
                                }
                                else {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            callback.onResult(false, message);
                                        }
                                    });
                                }
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,"");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,"");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLikeLeaderboardHour(final Context context, String userID, int offset, int count, String mode, final GetLikeLeaderboardCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("offset", offset);
            params.put("count", count);
            params.put("mode", mode);

            post(context, "getLikeLeaderboard", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModelHour.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModelHour.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModelHour);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLikeLeaderboardDay(final Context context, String userID, int offset, int count, String mode, final GetLikeLeaderboardCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("offset", offset);
            params.put("count", count);
            params.put("mode", mode);

            post(context, "getLikeLeaderboard", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModelDay.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModelDay.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModelDay);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLikeLeaderboardWeek(final Context context, String userID, int offset, int count, String mode, final GetLikeLeaderboardCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("offset", offset);
            params.put("count", count);
            params.put("mode", mode);

            post(context, "getLikeLeaderboard", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModelWeek.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModelWeek.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModelWeek);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLikeLeaderboardMonth(final Context context, String userID, int offset, int count, String mode, final GetLikeLeaderboardCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("offset", offset);
            params.put("count", count);
            params.put("mode", mode);

            post(context, "getLikeLeaderboard", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModelMonth.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModelMonth.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModelMonth);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLikeLeaderboardYear(final Context context, String userID, int offset, int count, String mode, final GetLikeLeaderboardCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("offset", offset);
            params.put("count", count);
            params.put("mode", mode);

            post(context, "getLikeLeaderboard", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModelYear.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModelYear.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mModelYear);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void updateLiveStreamInfo(final Context context, String userID, int liveStreamID, int audioOnly, String coverPhoto, final UpdateLiveStreamInfoCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("liveStreamID", liveStreamID);
            params.put("audioOnly", audioOnly);
            params.put("coverPhoto", coverPhoto);

            post(context, "updateLiveStreamInfo", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));

                            String result = data.getString("result");
                            final String message = data.getString("message");

                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,"");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,"");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void recoverPassword(final Context context, String openID, String email, final RecoverPasswordCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("email", email);

            post(context, "recoverPassword", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));

                            String result = data.getString("result");
                            final String message = data.getString("message");

                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, true, message);
                                    }
                                });
                            }
                            else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true,false, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,false,"");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,false,"");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getUserLiveStreams(final Context context, String userID, int beforeTime, int count, final GetHotLiveStreamsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getUserLiveStreams", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mLiveModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mLiveModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), LiveModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mLiveModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void isUserOnLiveStream(final Context context, String targetUserID, final IsUserOnLiveStreamCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);

            post(context, "isUserOnLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));

                            final String liveStreamID = data.getString("liveStreamID");
                            final String userIsOnLive = data.getString("userIsOnLive");

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(liveStreamID, userIsOnLive);
                                }
                            });
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult("", "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult("", "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult("", "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getFollowRequests(final Context context, int beforeTime, int count, final GetFollowRequestsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getFollowRequests", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mModel.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), UserModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, mModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void sendFollowRequest(final Context context, String targetUserID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);

            post(context, "sendFollowRequest", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void cancelFollowRequests(final Context context, String targetUserID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);

            post(context, "cancelFollowRequests", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void acceptFollowRequest(final Context context, String targetUserID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);

            post(context, "acceptFollowRequest", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void rejectFollowRequest(final Context context, String targetUserID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);

            post(context, "rejectFollowRequest", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void hidePostAction(final Context context, String postID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "hidePostAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void banUserAction(final Context context, String bannedUserID, String inLivestreamID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("bannedUserID", bannedUserID);
            params.put("inLivestreamID", inLivestreamID);

            post(context, "banUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void freezeUserAction(final Context context, String freezedUserID, String inLivestreamID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("freezedUserID", freezedUserID);
            params.put("inLivestreamID", inLivestreamID);

            post(context, "freezeUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void updateOpenID(final Context context, String openID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);

            post(context, "updateOpenID", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void adminBlockUserAction(final Context context, String publisherUserID, String blockedUserID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("publisherUserID", publisherUserID);
            params.put("blockedUserID", blockedUserID);

            post(context, "adminBlockUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getPayments(final Context context, int offset, int count, final GetPaymentsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("offset", offset);
            params.put("count", count);

            post(context, "getPayments", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mPaymentModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mPaymentModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), PaymentModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, mPaymentModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLiveStreamsForReview(final Context context, String region, int beforeTime, int count, int modulo, int devisor, final GetHotLiveStreamsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("region", region);
            params.put("beforeTime", beforeTime);
            params.put("count", count);
            params.put("modulo", modulo);
            params.put("devisor", devisor);

            post(context, "getLiveStreamsForReview", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));

                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mLiveModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mLiveModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), LiveModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mLiveModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void passLiveStream(final Context context, int liveStreamID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("liveStreamID", liveStreamID);

            post(context, "passLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void hideLiveStream(final Context context, int liveStreamID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("liveStreamID", liveStreamID);

            post(context, "hideLiveStream", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void hideUserFromLiveAction(final Context context, String targetUserID, int hideUserValue, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);
            params.put("hideUserValue", hideUserValue);

            post(context, "hideUserFromLiveAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void verifyUserAction(final Context context, String targetUserID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);

            post(context, "verifyUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void removeVerifiedUserAction(final Context context, String targetUserID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);

            post(context, "removeVerifiedUserAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void adminPromotePost(final Context context, String postID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "adminPromotePost", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getExplorePicturesForReview(final Context context, String region, int beforeTime, int count, int modulo, int devisor, final GetLikedPostsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("region", region);
            params.put("beforeTime", beforeTime);
            params.put("count", count);
            params.put("modulo", modulo);
            params.put("devisor", devisor);

            post(context, "getExplorePicturesForReview", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            mFeedModel.clear();

                            for(int k = 0 ; k < mJSONArray.size() ; k++)
                            {
                                try {
                                    FeedModel feedModel = new FeedModel();
                                    feedModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().getAsJsonObject("userInfo"), UserModel.class));

                                    feedModel.setPostID(mJSONArray.get(k).getAsJsonObject().get("postID").getAsString());
                                    feedModel.setUserID(mJSONArray.get(k).getAsJsonObject().get("userID").getAsString());
                                    feedModel.setCaption(mJSONArray.get(k).getAsJsonObject().get("caption").getAsString());
                                    feedModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                    feedModel.setPicture(mJSONArray.get(k).getAsJsonObject().get("picture").getAsString());

//                            JsonArray mTagged = mJSONArray.get(k).getAsJsonObject().get("taggedUsers").getAsJsonArray();
//                            int[] t = new int[mTagged.size()];
//                            for(int p = 0 ; p < mTagged.size() ; p++)
//                            {
//                                t[p] = mTagged.get(p).getAsInt();
//                            }
//                            feedModel.setTaggedUsers(t);

                                    feedModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                    feedModel.setCanComment(mJSONArray.get(k).getAsJsonObject().get("canComment").getAsInt());
                                    feedModel.setVideo(mJSONArray.get(k).getAsJsonObject().get("video").getAsString());
                                    feedModel.setReachability(mJSONArray.get(k).getAsJsonObject().get("reachability").getAsString());
                                    feedModel.setTotalRevenue(mJSONArray.get(k).getAsJsonObject().get("totalRevenue").getAsFloat());
                                    feedModel.setLocationName(mJSONArray.get(k).getAsJsonObject().get("locationName").getAsString());
                                    feedModel.setLocationID(mJSONArray.get(k).getAsJsonObject().get("locationID").getAsString());
                                    feedModel.setLatitude(mJSONArray.get(k).getAsJsonObject().get("latitude").getAsFloat());
                                    feedModel.setLongitude(mJSONArray.get(k).getAsJsonObject().get("longitude").getAsFloat());
                                    feedModel.setLikeCount(mJSONArray.get(k).getAsJsonObject().get("likeCount").getAsInt());
                                    feedModel.setCommentCount(mJSONArray.get(k).getAsJsonObject().get("commentCount").getAsInt());
                                    feedModel.setViewCount(mJSONArray.get(k).getAsJsonObject().get("viewCount").getAsInt());
                                    feedModel.setLiked(mJSONArray.get(k).getAsJsonObject().get("liked").getAsInt());

                                    mFeedModel.add(feedModel);
                                } catch (Exception e) {

                                }
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", mFeedModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void passExplorePicture(final Context context, String postID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "passExplorePicture", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void hideExplorePicture(final Context context, String postID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("postID", postID);

            post(context, "hideExplorePicture", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getLiveStreamHotCountryList(final Context context, final LiveRegionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            post(context, "getLiveStreamHotCountryList", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mLiveRegionModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++) {
                                mLiveRegionModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), LiveRegionModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, mLiveRegionModels);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getHotLiveStreams2(final Context context, String region, int beforeTime, int count, final GetHotLiveStreamsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("region", region);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            if(isLogEnabled) {
                Log.d("17_g", "[S][getHotLiveStreams2] region : " + region + " count : " + count);
            }

            post(context, "getSuggestedLiveStreams", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            if(isLogEnabled) {
                                Log.d("17_g", "[R][getHotLiveStreams2] jsonE : " + jsonE.toString());
                            }
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mHotLiveModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mHotLiveModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), LiveModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mHotLiveModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLatestLiveStreams(final Context context, String region, int beforeTime, int count, final GetHotLiveStreamsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("region", region);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getLatestLiveStreams", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mLatestLiveModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mLatestLiveModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), LiveModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mLatestLiveModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getSuggestedLiveStreams(final Context context, String region, int beforeTime, int count, final GetHotLiveStreamsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("region", region);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getSuggestedLiveStreams", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mSuggestedLiveModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mSuggestedLiveModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), LiveModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, "",mSuggestedLiveModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void reportLiveAction(final Context context, String reportedUserID, int livestreamID, String reason, final RequestCallback callback)
    {
        try
        {
            //Umeng monitor
            String Umeng_id="ReportLive";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context,Umeng_id,mHashMap,0);

            JSONObject params = new JSONObject();
            params.put("reportedUserID", reportedUserID);
            params.put("livestreamID", livestreamID);
            params.put("reason", reason);

            post(context, "reportLiveAction", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(true);
                            }
                        });
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getGiftList(final Context context, final GetGiftListCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();

            post(context, "getGiftList", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mGiftModel.clear();
//
                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mGiftModel.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), GiftModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mGiftModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getPointGainLog(final Context context, int beforeTime, int count, final GetPointGainLogCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);
            if(Constants.INTERNATIONAL_VERSION) params.put("platform", "ANDROID");
            else params.put("platform", "ANDROID_CN");

            post(context, "getPointGainLog", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mPointGainModels.clear();
//
                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                PointGainModel mPointGainModel = new PointGainModel();
                                mPointGainModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                mPointGainModel.setPoint(mJSONArray.get(k).getAsJsonObject().get("point").getAsInt());
                                mPointGainModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                mPointGainModel.setProductInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("productInfo"), ProductModel.class));

                                mPointGainModels.add(mPointGainModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mPointGainModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getPointUsageLog(final Context context, int beforeTime, int count, final GetPointUsageLogCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);
            if(Constants.INTERNATIONAL_VERSION) params.put("platform", "ANDROID");
            else params.put("platform", "ANDROID_CN");

            post(context, "getPointUsageLog", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mPointUsageModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                PointUsageModel mPointUsageModel = new PointUsageModel();
                                mPointUsageModel.setType(mJSONArray.get(k).getAsJsonObject().get("type").getAsString());
                                mPointUsageModel.setPoint(mJSONArray.get(k).getAsJsonObject().get("point").getAsInt());
                                mPointUsageModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                mPointUsageModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));
                                mPointUsageModel.setGiftInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("giftInfo"), GiftModel.class));

                                mPointUsageModels.add(mPointUsageModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mPointUsageModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void sendGift(final Context context, int liveStreamID, String giftID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("liveStreamID", liveStreamID);
            params.put("giftID", giftID);

            post(context, "sendGift", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true);
                                    }
                                });
                            }
                            else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    public static void getGiftLeaderboardAll(final Context context, String targetUserID, int offset, int count, String type, final GetGiftLeaderboardCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);
            params.put("offset", offset);
            params.put("count", count);
            params.put("type", type);

            post(context, "getGiftLeaderboard", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mGiftLeaderboardModelsAll.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                GiftLeaderboardModel mGiftLeaderboardModel = new GiftLeaderboardModel();
                                mGiftLeaderboardModel.setPoint(mJSONArray.get(k).getAsJsonObject().get("point").getAsInt());
                                mGiftLeaderboardModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));

                                mGiftLeaderboardModelsAll.add(mGiftLeaderboardModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mGiftLeaderboardModelsAll);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }



    public static void getLiveStreamGiftInfo(final Context context, int liveStreamID, int giftToken, final GetLiveStreamGiftInfoCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("liveStreamID", liveStreamID);
            params.put("giftToken", giftToken);

            post(context, "getLiveStreamGiftInfo", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            final LiveGiftsModel mLiveGiftsModel = new LiveGiftsModel();
                            mLiveGiftsModel.setTimestamp(jsonE.getAsJsonObject().get("timestamp").getAsInt());
                            mLiveGiftsModel.setUserInfo(gson.fromJson(jsonE.getAsJsonObject().get("userInfo"), UserModel.class));
                            mLiveGiftsModel.setGiftInfo(gson.fromJson(jsonE.getAsJsonObject().get("giftInfo"), GiftModel.class));

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mLiveGiftsModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getGiftLeaderboardToday(final Context context, String targetUserID, int offset, int count, String type, final GetGiftLeaderboardCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);
            params.put("offset", offset);
            params.put("count", count);
            params.put("type", type);

            post(context, "getGiftLeaderboard", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mGiftLeaderboardModelsToday.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                GiftLeaderboardModel mGiftLeaderboardModel = new GiftLeaderboardModel();
                                mGiftLeaderboardModel.setPoint(mJSONArray.get(k).getAsJsonObject().get("point").getAsInt());
                                mGiftLeaderboardModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));

                                mGiftLeaderboardModelsToday.add(mGiftLeaderboardModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mGiftLeaderboardModelsToday);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

   //SMS verification
    public static void sendPhoneVerificationCode(final Context context,String countryCallingCode,String myPhoneNumber,String key,String type,String openID, final SendPhoneVerificationInfoCallback callback)
    {
        try
        {

            JSONObject params = new JSONObject();
            //setConfig(context, Constants.PHONE_VERIFICATION_UUID, UUID.randomUUID().toString());
            Singleton.preferenceEditor.putString(Constants.PHONE_VERIFICATION_UUID, UUID.randomUUID().toString()).commit();
            params.put("requestID", Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, ""));
            params.put("type", type);
            params.put("language", Singleton.applicationContext.getString(R.string.current_language));
            params.put("countryCallingCode", countryCallingCode);
            params.put("phoneNumber", myPhoneNumber);
            params.put("openID",openID);
            params.put("signature", Singleton.md5(Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, "") + Singleton.applicationContext.getString(R.string.current_language) + countryCallingCode + myPhoneNumber + key));

            if(isLogEnabled == true)
            {
                Log.d("17_gift", "[sendphoneverification] UUID : " + Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, "") + ", language : " + Singleton.applicationContext.getString(R.string.current_language) + ", country code : " + countryCallingCode + ", phonenumber : " + myPhoneNumber + ", type : "+type + ", openID : "+openID);
            }
            post(context, "sendPhoneVerificationCode", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
//                    Log.d("17_g","code="+response.code());
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject jsonE = new JSONObject(getDecodeText(context, response.body().string()));
                            if(isLogEnabled == true)
                            {
                                Log.d("17_g", "[R][sendphoneverification] jsonE :" + jsonE.toString());
                            }
                            final String result = jsonE.getString("result");
                            final String message = jsonE.getString("message");
//                                final String publicUserIP;

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,result,message);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,"fail","exceptionerror");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,"fail","responseerror");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
//                    Log.d("17_gift","phone verify : onfailure");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,"fail","failure");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void isPhoneNumberUsed(final Context context,String countryCallingCode,String localPhoneNumber,final IsPhoneNumberUsedCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("countryCallingCode", countryCallingCode);
            params.put("localPhoneNumber", localPhoneNumber);
            if(isLogEnabled == true)
            {
                Log.d("17_g", "[isPhoneNumberUsed] countryCallingCode : " + countryCallingCode + ", localPhoneNumber : "+localPhoneNumber);
            }
            post(context, "isPhoneNumberUsed", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {

                    if (response.isSuccessful()) {
//                        final String result = "no";
                        try {
                            Gson gson = new Gson();
                            //JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JSONObject jsonE = new JSONObject(getDecodeText(context, response.body().string()));

                            final String result = jsonE.getString("result");
                            final String isUsed;
                            final String message;

                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][isPhoneNumberUsed] jsonE :" + jsonE.toString());
                            }

                            if(result.equals("success"))
                            {
                                isUsed = jsonE.getString("isUsed");

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, result, isUsed,"");
                                    }
                                });
                            }
                            else
                            {
                                message = jsonE.getString("message");

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, result,"",message);
                                    }
                                });
                            }
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "exception","","");
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "fail","","");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "fail","","");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void isPhoneNumberOpenIDPaired(final Context context,String countryCallingCode,String localPhoneNumber,String openID,final IsPhoneNumberOpenIDPairedCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("countryCallingCode", countryCallingCode);
            params.put("localPhoneNumber", localPhoneNumber);
            params.put("openID",openID);
            if(isLogEnabled == true)
            {
                Log.d("17_g", "[isPhoneNumberOpenIDPaired] countryCallingCode : " + countryCallingCode + ", localPhoneNumber : "+localPhoneNumber+ ", openID : "+openID);
            }
            post(context, "isPhoneNumberOpenIDPaired", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {

                    if (response.isSuccessful()) {
//                        final String result = "no";
                        try {
                            Gson gson = new Gson();
                            //JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JSONObject jsonE = new JSONObject(getDecodeText(context, response.body().string()));

                            final String result = jsonE.getString("result");
                            final String isPaired;
                            final String message;

                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][isPhoneNumberOpenIDPaired] jsonE :" + jsonE.toString());
                            }

                            if(result.equals("success"))
                            {
                                isPaired = jsonE.getString("isPaired");

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, result, isPaired,"");
                                    }
                                });
                            }
                            else
                            {
                                message = jsonE.getString("message");

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, result,"",message);
                                    }
                                });
                            }
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "exception","","");
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "fail","","");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "fail","","");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }


    public static void checkNeedHumanTest(final Context context,String IPRequestCountRecord,final CheckNeedHumanTestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("type", IPRequestCountRecord);
            if(isLogEnabled == true)
            {
                Log.d("17_g", "[checkNeedHumanTest] IPRequestCountRecord : " + IPRequestCountRecord);
            }
            post(context, "checkNeedHumanTest", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {

                    if (response.isSuccessful()) {
//                        final String result = "no";
                        try {
                            Gson gson = new Gson();
                            //JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JSONObject jsonE = new JSONObject(getDecodeText(context, response.body().string()));
                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][checkhumantest] jsonE :" + jsonE.toString());
                            }
                            final String result = jsonE.getString("result");
                            final String isNeeded = jsonE.getString("isNeeded");

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, result,isNeeded);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "exception","");
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "fail","");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "fail","");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void registerByPhoneAction(final Context context, String openID, String password,String phone2DigitISO ,String countryCallingCode,String localPhoneNumber,String verificationCode,String type,String token,String id,final RegisterByPhoneActionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("password", Singleton.md5(password));
            params.put("requestID", Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, ""));
            params.put("countryCallingCode", countryCallingCode);
            params.put("localPhoneNumber", localPhoneNumber);
            params.put("verificationCode", verificationCode);
            params.put("phoneTwoDigitISO",phone2DigitISO);

//            Log.d("17_g", "type : " + type);

            if(type.equals("fb"))
            {
                params.put("facebookID", id);
                params.put("facebookAccessToken",token);
            }
            else if (type.equals("qq"))
            {
                params.put("qqID", id);
                params.put("qqAccessToken",token);
            }
            else if (type.equals("weibo"))
            {
                params.put("weiboID", id);
                params.put("weiboAccessToken",token);
            }
            else if (type.equals("wechat"))
            {
                params.put("wechatID", id);
                params.put("wechatAccessToken",token);
            }


            if(isLogEnabled == true) {
                Log.d("17_gift", "[registerByPhoneAction] UUID :" + Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, "") + ", openID :" + openID + ", country code :" + countryCallingCode + ", localPhoneNumber :" + localPhoneNumber + ", verificationCode :" + verificationCode + ", phone2DigitISO :" + phone2DigitISO);
                Log.d("17_gift", "[registerByPhoneAction] id : "+id+ ", token : "+ token);
            }

            post(context, "registerByPhoneAction", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][registerByPhoneAction] Json " + data.toString());
                            }
                            final String result = data.getString("result");
                            final String message = data.getString("message");

                            if (result.equals("success")) {

                                Singleton.preferenceEditor.putString(Constants.ACCESS_TOKEN, data.getString("accessToken"));
                                Singleton.preferenceEditor.commit();
                                saveUserInfo(context, data.getJSONObject("userInfo"));

                                Gson gson = new Gson();
                                final UserModel mUserModel = gson.fromJson(data.getJSONObject("userInfo").toString(), UserModel.class);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, result, message, mUserModel);
                                    }

                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, result, message, null);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", "", null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }
    
    /*Subscriber API Start*/

    public static void getSubscribers(final Context context,String userID,String targetUserID,int beforeTime, int count, final SubscriberUserInfoCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", targetUserID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            Log.d("17_gift", "targetUserID :" + targetUserID);

            post(context, "getSubscribers", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));

                            Log.d("17_gift","json :"+jsonE.toString());
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            SubscriberUserModelAll.clear();
                            Log.d("17_gift", "json size :" + mJSONArray.size());
                            for (int k = 0; k < mJSONArray.size(); k++) {
                                SubscriberUserModel mSubscriberUserModel = new SubscriberUserModel();
                                mSubscriberUserModel.setSubscriptionTime(mJSONArray.get(k).getAsJsonObject().get("subscriptionTime").getAsInt());
                                mSubscriberUserModel.setSubscriptionTime(mJSONArray.get(k).getAsJsonObject().get("subscriptionEndTime").getAsInt());
                                mSubscriberUserModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));

                                SubscriberUserModelAll.add(mSubscriberUserModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("17_gift","pass");
                                    callback.onResult(true, SubscriberUserModelAll);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getSubscribing(final Context context,String userID,String targetUserID,int beforeTime, int count, final SubscriberUserInfoCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", targetUserID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getSubscribing", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            SubscriberUserModelAll.clear();

                            for (int k = 0; k < mJSONArray.size(); k++) {
                                SubscriberUserModel mSubscriberUserModel = new SubscriberUserModel();
                                mSubscriberUserModel.setSubscriptionTime(mJSONArray.get(k).getAsJsonObject().get("subscriptionTime").getAsInt());
                                mSubscriberUserModel.setSubscriptionEndTime(mJSONArray.get(k).getAsJsonObject().get("subscriptionEndTime").getAsInt());
                                mSubscriberUserModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));

                                SubscriberUserModelAll.add(mSubscriberUserModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, SubscriberUserModelAll);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getSubscribersPurchaseLog(final Context context,String userID,int beforeTime, int count, final SubscribersPurchaseslogCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getSubscribersPurchaseLog", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            SubscribersPurchaseModelAll.clear();

                            for (int k = 0; k < mJSONArray.size(); k++) {
                                SubscribersPurchaseModel mSubscribersPurchaseModel = new SubscribersPurchaseModel();
                                mSubscribersPurchaseModel.setSubscriptionTime(mJSONArray.get(k).getAsJsonObject().get("subscriptionTime").getAsInt());
                                mSubscribersPurchaseModel.setRevenue(mJSONArray.get(k).getAsJsonObject().get("revenue").getAsInt());
                                mSubscribersPurchaseModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));

                                SubscribersPurchaseModelAll.add(mSubscribersPurchaseModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, SubscribersPurchaseModelAll);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getSubscriptionPurchaseLog(final Context context,String userID,int beforeTime, int count, final SubscriptionPurchaseslogCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getSubscriptionPurchaseLog", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            SubscriptionPurchaseModelAll.clear();

                            for (int k = 0; k < mJSONArray.size(); k++) {
                                SubscriptionPurchaseModel mSubscriptionPurchaseModel = new SubscriptionPurchaseModel();
                                mSubscriptionPurchaseModel.setSubscriptionTime(mJSONArray.get(k).getAsJsonObject().get("subscriptionTime").getAsInt());
                                mSubscriptionPurchaseModel.setSubscriptionProductID(mJSONArray.get(k).getAsJsonObject().get("subscriptionProductID").getAsString());
                                mSubscriptionPurchaseModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));
                                mSubscriptionPurchaseModel.setPoint(mJSONArray.get(k).getAsJsonObject().get("point").getAsInt());

                                SubscriptionPurchaseModelAll.add(mSubscriptionPurchaseModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, SubscriptionPurchaseModelAll);
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, null);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    //subscriber API for get subscription options
    public static void getSubscriptionProductList(final Context context, final GetSubscriptionProductListCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("platform", "ANDROID");
            params.put("language", Singleton.applicationContext.getString(R.string.current_language));

            post(context, "getSubscriptionProductList", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));

                            mSubscribeOptionModelAll.clear();

                            Log.d("17_gifts", "json :  " + jsonE.toString());

                            JsonArray mJSONArray = jsonE.getAsJsonArray();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                SubscribeOptionModel mSubscribeOptionModel = new SubscribeOptionModel();
                                mSubscribeOptionModel.setSubscriptionProductID(mJSONArray.get(k).getAsJsonObject().get("subscriptionProductID").getAsString());
                                mSubscribeOptionModel.setPoint(mJSONArray.get(k).getAsJsonObject().get("point").getAsInt());
                                mSubscribeOptionModel.setDiscountPoint(mJSONArray.get(k).getAsJsonObject().get("discountPoint").getAsInt());
                                mSubscribeOptionModel.setDiscountPercentage(mJSONArray.get(k).getAsJsonObject().get("discountPercentage").getAsFloat());
                                mSubscribeOptionModel.setIcon(mJSONArray.get(k).getAsJsonObject().get("icon").getAsString());
                                mSubscribeOptionModel.setName(mJSONArray.get(k).getAsJsonObject().get("name").getAsString());

                                mSubscribeOptionModelAll.add(mSubscribeOptionModel);
                            }
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mSubscribeOptionModelAll);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void subScribeUser(final Context context, String userID, String targetUserID,String subscriptionProductID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", targetUserID);
            params.put("platform", "ANDROID");
            params.put("subscriptionProductID", subscriptionProductID);

            Log.d("17_Gift", " user : " + userID);
            Log.d("17_Gift", " targetUserID : " + targetUserID);
            Log.d("17_Gift", " SubscriptionID : " + subscriptionProductID);
            post(context, "subscribeUser", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            Log.d("17_gift", "data : " + data.toString());
                            String result = data.getString("result");
                            if (result.equals("success")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }


    public static void isSubscribing(final Context context, String userID, String targetUserID, final RequestCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("userID", userID);
            params.put("targetUserID", targetUserID);

            Log.d("17_Gift", " user : " + userID);
            Log.d("17_Gift", " targetUserID : " + targetUserID);
            post(context, "isSubscribing", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            Log.d("17_gift", "data : " + data.toString());
                            String result = data.getString("result");
                            if (result.equals("no")) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false);
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false);
                        }
                    });
                }
            });

        }
        catch(Exception e)
        {

        }
    }

    /*Subscriber API end*/


    public static void changePhoneNumber (final Context context, String openID,String phone2DigitISO ,String countryCallingCode,String localPhoneNumber,String verificationCode, final ChangePhoneNumberCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("requestID", Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, ""));
            params.put("countryCallingCode", countryCallingCode);
            params.put("localPhoneNumber", localPhoneNumber);
            params.put("phoneTwoDigitISO", phone2DigitISO);
            params.put("verificationCode", verificationCode);
            if(isLogEnabled == true) {
                Log.d("17_gift", "[changePhoneNumber] UUID :" + Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, "") + ", openID :" + openID + ", countryCallingCode :" + countryCallingCode + ", localPhoneNumber :" + localPhoneNumber + ", verificationCode :" + verificationCode+", phone2DigitISO : " + phone2DigitISO);
            }

            //Umeng monitor
            String Umeng_id="ChangePhoneNumber";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);

            post(context, "ChangePhoneNumber", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if (response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][changePhoneNumber] Json " + data.toString());
                            }
                            final String result = data.getString("result");
                            final String message = data.getString("message");

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, result,message);
                                }
                            });
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", "");
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }
    
    public static void getLiveStreamGiftLeaderboard(final Context context, String targetUserID, int liveStreamID, int offset, int count, final GetLivestreamGiftLeaderboardCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("targetUserID", targetUserID);
            params.put("liveStreamID", liveStreamID);
            params.put("offset", offset);
            params.put("count", count);

            if(isLogEnabled == true) {
                Log.d("17_g", "[getLiveStreamGiftLeaderboard]");
            }

            post(context, "getLiveStreamGiftLeaderboard", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][getLiveStreamGiftLeaderboard] json : " + jsonE.toString());
                            }
                            mGiftLeaderboardModelsAll.clear();

                            final GiftLivestreamLeaderBoardModel mGiftLivestreamLeaderBoardModel = new GiftLivestreamLeaderBoardModel();
                            mGiftLivestreamLeaderBoardModel.setMyPoint(jsonE.getAsJsonObject().get("myPoint").getAsInt());
                            mGiftLivestreamLeaderBoardModel.setTotalPoint(jsonE.getAsJsonObject().get("totalPoint").getAsInt());

                            JsonArray mJSONArray = jsonE.getAsJsonObject().get("rank").getAsJsonArray();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                GiftLeaderboardModel mGiftLeaderboardModel = new GiftLeaderboardModel();
                                mGiftLeaderboardModel.setPoint(mJSONArray.get(k).getAsJsonObject().get("point").getAsInt());
                                mGiftLeaderboardModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));

                                mGiftLeaderboardModelsAll.add(mGiftLeaderboardModel);
                            }

                            mGiftLivestreamLeaderBoardModel.setGiftLeaderboardInfo(mGiftLeaderboardModelsAll);

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mGiftLivestreamLeaderBoardModel);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getResetPasswordToken(final Context context, String openID,String countryCallingCode,String localPhoneNumber,String verificationCode,final GetResetPasswordTokenCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("requestID", Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, ""));
            params.put("countryCallingCode", countryCallingCode);
            params.put("localPhoneNumber", localPhoneNumber);
            params.put("verificationCode", verificationCode);
            if(isLogEnabled == true) {
                Log.d("17_gift", "[getResetPasswordToken] UUID :" + Singleton.preferences.getString(Constants.PHONE_VERIFICATION_UUID, "") + " openID :" + openID + ", countryCallingCode :" + countryCallingCode + ", localPhoneNumber :" + localPhoneNumber + ", verificationCode :" + verificationCode);
            }
            post(context, "getResetPasswordToken", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][getResetPasswordToken] Json " + data.toString());
                            }
                            final String result = data.getString("result");
                            final String message = data.getString("message");
                            final String resetPasswordToken;

                            if(result.equals("success"))
                            {
                                resetPasswordToken= data.getString("resetPasswordToken");

                                handler.post(new Runnable()
                                {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, result, message, resetPasswordToken);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, result, message, "");
                                    }
                                });
                            }

                        }
                        catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", "", "");
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", "", "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", "", "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void resetPassword(final Context context, String resetPasswordToken ,String newPassword,String confirmNewPassword,final ResetPasswordCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("resetPasswordToken", resetPasswordToken);
            params.put("newPassword", Singleton.md5(newPassword));
            params.put("confirmNewPassword", Singleton.md5(confirmNewPassword));

            if(isLogEnabled == true) {
                Log.d("17_gift", "[resetPassword] resetPasswordToken :" + resetPasswordToken + " newPassword :" + newPassword + " confirmNewPassword :" + confirmNewPassword);
            }

            //Umeng monitor
            String Umeng_id="ResetPasswordByPhone";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);

            post(context, "resetPassword", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][resetPassword] Json " + data.toString());
                            }
                            final String result = data.getString("result");
                            final String message = data.getString("message");

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, result,message);
                                }
                            });
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "","");
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "","");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "","");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void validateHumanTestResponse(final Context context,String geetest_challenge ,String geetest_validate,String geetest_seccode,String IPRequestCountRecord,final ValidateHumanTestResponseCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("geetest_challenge", geetest_challenge);
            params.put("geetest_validate", geetest_validate);
            params.put("geetest_seccode", geetest_seccode);
            params.put("type", IPRequestCountRecord);

            if(isLogEnabled == true) {
                Log.d("17_gift", "[validateHumanTestResponse] challenge :" + geetest_challenge + " validate :" + geetest_validate + " seccode :" + geetest_seccode + " IPRequestCountRecord" + IPRequestCountRecord);
            }
            post(context, "validateHumanTestResponse", params, new Callback() {
                @Override
                public void onResponse(Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][validateHumanTestResponse] Json " + data.toString());
                            }
                            final String result = data.getString("result");

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true, result);
                                }
                            });
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "fail");
                                }
                            });
                        }
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "fail");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "fail");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getPointProductList(final Context context, final ProductListCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            if(Constants.INTERNATIONAL_VERSION) params.put("platform", "ANDROID");
            else params.put("platform", "ANDROID_CN");

            post(context, "getPointProductList", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mProductModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                mProductModels.add(gson.fromJson(mJSONArray.get(k).getAsJsonObject(), ProductModel.class));
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mProductModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void purchaseProduct(final Context context, String productID, String androidOrderID, String androidPurchaseToken, String androidDeveloperPayload, final PurchaseProductCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("productID", productID);
            if(Constants.INTERNATIONAL_VERSION) params.put("platform", "ANDROID");
            else params.put("platform", "ANDROID_CN");
            params.put("androidOrderID", androidOrderID);
            params.put("androidPurchaseToken", androidPurchaseToken);
            params.put("androidDeveloperPayload", androidDeveloperPayload);

            post(context, "purchaseProduct", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            String message = data.getString("message");

                            if(result.equals("success") && message.equals("ok"))
                            {
                                final int point = data.getInt("point");
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, point);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, 0);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, 0);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, 0);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, 0);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getReceivedGiftLog(final Context context, int beforeTime, int count, final GetLiveStreamReceivedGiftsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getReceivedGiftLog", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mLiveGiftsModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                LiveGiftsModel mLiveGiftsModel = new LiveGiftsModel();
                                mLiveGiftsModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                mLiveGiftsModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));
                                mLiveGiftsModel.setGiftInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("giftInfo"), GiftModel.class));

                                mLiveGiftsModels.add(mLiveGiftsModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mLiveGiftsModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getLiveStreamReceivedGifts(final Context context, int liveStreamID, int afterTime, int count, final GetLiveStreamReceivedGiftsCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("liveStreamID", liveStreamID);
            params.put("afterTime", afterTime);
            params.put("count", count);

            if(isLogEnabled) {
                Log.d("17_g", "[getLiveStreamReceivedGifts]");
            }
            post(context, "getLiveStreamReceivedGifts", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            if(isLogEnabled) {
                                Log.d("17_g", "[R][getLiveStreamReceivedGifts] json : " + jsonE.toString());
                            }

                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mLiveGiftsModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                LiveGiftsModel mLiveGiftsModel = new LiveGiftsModel();
                                mLiveGiftsModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                mLiveGiftsModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));
                                mLiveGiftsModel.setGiftInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("giftInfo"), GiftModel.class));

                                mLiveGiftsModels.add(mLiveGiftsModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mLiveGiftsModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void readGift(final Context context, int giftToken)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("giftToken", giftToken);

            post(context, "readGift", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {

                    }
                    else
                    {

                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {

                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getUnreadReceivedGift(final Context context, int liveStreamID, int beforeTime, int count, final GetUnreadReceivedGiftCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("liveStreamID", liveStreamID);
            params.put("beforeTime", beforeTime);
            params.put("count", count);

            post(context, "getUnreadReceivedGift", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            Gson gson = new Gson();
                            JsonElement jsonE = new JsonParser().parse(getDecodeText(context, response.body().string()));
                            JsonArray mJSONArray = jsonE.getAsJsonArray();
                            mReadGiftModels.clear();

                            for (int k = 0; k < mJSONArray.size(); k++)
                            {
                                ReadGiftModel mReadGiftModel = new ReadGiftModel();
                                mReadGiftModel.setGiftToken(mJSONArray.get(k).getAsJsonObject().get("giftToken").getAsInt());
                                mReadGiftModel.setTimestamp(mJSONArray.get(k).getAsJsonObject().get("timestamp").getAsInt());
                                mReadGiftModel.setUserInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("userInfo"), UserModel.class));
                                mReadGiftModel.setGiftInfo(gson.fromJson(mJSONArray.get(k).getAsJsonObject().get("giftInfo"), GiftModel.class));

                                mReadGiftModels.add(mReadGiftModel);
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(true,mReadGiftModels);
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false,null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false,null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void createOrder(final Context context,String openID, String productID, final checkFacebookIDAvailableCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("productID", productID);
            params.put("openID", openID);

            post(context, "createOrder", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            if (result.equals("success"))
                            {
                                final String transid = data.getString("transid");

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, transid);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, "");
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false,null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void loginActionwechatID(final Context context, String openID, String password, String wechatID, final LoginAction2Callback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("password", Singleton.md5(password));
            params.put("wechatID", wechatID);

            post(context, "loginAction2", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            final String message = data.getString("message");

                            if (message.equals("ok"))
                            {
                                Singleton.preferenceEditor.putString(Constants.ACCESS_TOKEN, data.getString("accessToken"));
                                Singleton.preferenceEditor.commit();
                                saveUserInfo(context, data.getJSONObject("userInfo"));

                                Gson gson = new Gson();
                                final UserModel mUserModel = gson.fromJson(data.getJSONObject("userInfo").toString(), UserModel.class);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message, mUserModel);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message, null);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void registerAction_V2_wechatID(final Context context, String openID, String password, String wechatID, final RegisterActionCallback callback)
    {
        /*
        var openID = req.body.openID;
		var password = req.body.password;
		var facebookID = req.body.facebookID;
		var deviceType = req.body.deviceType;
		var country = req.headers["cf-ipcountry"];
		var language = req.body.language;
         */
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("wechatID", wechatID);
            params.put("password", Singleton.md5(password));
            params.put("deviceType", "ANDROID");
            params.put("country", "");
            params.put("language", "");

//            params.put("coverPhoto", coverPhoto);

            post(context, "registerAction2", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");

                            if (message.equals("ok")) {
                                Singleton.preferenceEditor.putString(Constants.ACCESS_TOKEN, data.getString("accessToken"));
                                Singleton.preferenceEditor.commit();
                                saveUserInfo(context,data.getJSONObject("userInfo"));

                                Gson gson = new Gson();
                                final UserModel mUserModel = gson.fromJson(data.getJSONObject("userInfo").toString(), UserModel.class);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message,mUserModel);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message,null);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "",null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "",null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "",null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void checkWechatIDAvailable(final Context context, String wechatID, final checkFacebookIDAvailableCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("wechatID", wechatID);

            post(context, "checkWechatIDAvailable", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void checkQQIDAvailable(final Context context, String qqID, final checkFacebookIDAvailableCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("qqID", qqID);

            post(context, "checkQQIDAvailable", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void checkWeiboIDAvailable(final Context context, String weiboID, final checkFacebookIDAvailableCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("weiboID", weiboID);

            post(context, "checkWeiboIDAvailable", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));

                            if(isLogEnabled == true) {
                                Log.d("17_g", "[R][checkWeiboIDAvailable] data :  " + data.toString());
                            }
                            String result = data.getString("result");
                            final String message = data.getString("message");
                            if (result.equals("success"))
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, message);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void loginActionChina(final Context context, String openID, String password, String type, String id, final LoginAction2Callback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("openID", openID);
            params.put("password", Singleton.md5(password));
            params.put(type, id);

            post(context, "loginAction2", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            if(isLogEnabled) {
                                Log.d("17_g", "[R][loginAction2] data = " + data);
                            }
                            final String message = data.getString("message");

                            if (message.equals("ok"))
                            {
                                Singleton.preferenceEditor.putString(Constants.ACCESS_TOKEN, data.getString("accessToken"));
                                Singleton.preferenceEditor.commit();
                                saveUserInfo(context, data.getJSONObject("userInfo"));

                                Gson gson = new Gson();
                                final UserModel mUserModel = gson.fromJson(data.getJSONObject("userInfo").toString(), UserModel.class);
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message, mUserModel);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, message, null);
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", null);
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", null);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", null);
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void eventTracking(final Context context, String array, final checkFacebookIDAvailableCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("eventArray", array);

            post(context, "eventTracking", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
//                    if(response.isSuccessful())
//                    {
//                        try
//                        {
//                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
//                            String result = data.getString("result");
//                            final String message = data.getString("message");
//                            if (result.equals("success"))
//                            {
//                                handler.post(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        callback.onResult(true, message);
//                                    }
//                                });
//                            }
//                            else
//                            {
//                                handler.post(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        callback.onResult(false, message);
//                                    }
//                                });
//                            }
//                        }
//                        catch (JSONException e)
//                        {
//                            handler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    callback.onResult(false, "");
//                                }
//                            });
//                        }
//                    }
//                    else
//                    {
//                        handler.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                callback.onResult(false, "");
//                            }
//                        });
//                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
//                    handler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            callback.onResult(false, "");
//                        }
//                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void getNewestAppVersion(final Context context, final GetNewestAppVersionCallback callback)
    {
        try
        {
            JSONObject params = new JSONObject();
            post(context, "getNewestAppVersion", params, new Callback()
            {
                @Override
                public void onResponse(Response response) throws IOException
                {
                    if(response.isSuccessful())
                    {
                        try
                        {
                            JSONObject data = new JSONObject(getDecodeText(context, response.body().string()));
                            String result = data.getString("result");

                            if (result.equals("success"))
                            {
                                final String newestVersion = data.getString("newestVersion");
                                final String downloadURL = data.getString("downloadURL");

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(true, newestVersion, downloadURL);
                                    }
                                });
                            }
                            else
                            {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.onResult(false, "", "");
                                    }
                                });
                            }
                        }
                        catch (JSONException e)
                        {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(false, "", "");
                                }
                            });
                        }
                    }
                    else
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onResult(false, "", "");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Request request, IOException e)
                {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onResult(false, "", "");
                        }
                    });
                }
            });
        }
        catch(Exception e)
        {

        }
    }

    public static void uploadFile(final Context context, final String filePath, String name, final SuccessCallback callback)
    {
        try
        {
            File myFile = new File(filePath);
            RequestParams mRequest = new RequestParams();

            mRequest.put("myfile", myFile);

            postPhoto(context, "uploadFile", mRequest, new AsyncHttpResponseHandler()
            {
                @Override
                public void onProgress(long bytesWritten, long totalSize)
                {
                    int count = (int) ((bytesWritten * 1.0 / totalSize) * 100);
                    callback.onProgress(count);
                }

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                    callback.onResult(true);
                }

                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                    callback.onResult(false);
                }
            });
        }
        catch(Exception e)
        {

        }
    }

//    public static void uploadFile(final Context context, final String filePath, String name, final SuccessCallback callback)
//    {
//        try
//        {
//            File myFile = new File(filePath);
//            RequestParams mRequest = new RequestParams();
//
//            mRequest.put("myfile", myFile);
//
//            postPhoto(context, "uploadFile", mRequest, new JsonHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                    callback.onResult(true);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    callback.onResult(false);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    this.onFailure(statusCode, headers, "", throwable);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                    this.onFailure(statusCode, headers, "", throwable);
//                }
//            });
//        }
//        catch(Exception e)
//        {
//
//        }
//    }

    private static void saveUserInfo(Context context, JSONObject data)
    {
        try
        {
            Singleton.preferenceEditor.putString(Constants.USER_ID, getStringJSONObject(data, "userID"));
            Singleton.preferenceEditor.putString(Constants.OPEN_ID, getStringJSONObject(data, "openID"));
            Singleton.preferenceEditor.putString(Constants.PRIVACY_MODE, getStringJSONObject(data, "privacyMode"));
            Singleton.preferenceEditor.putString(Constants.PHONE_NUMBER, getStringJSONObject(data, "phoneNumber"));
            Singleton.preferenceEditor.putString(Constants.NAME, getStringJSONObject(data, "name"));
            Singleton.preferenceEditor.putString(Constants.BIO, getStringJSONObject(data, "bio"));
            Singleton.preferenceEditor.putString(Constants.PICTURE, getStringJSONObject(data, "picture"));
            Singleton.preferenceEditor.putString(Constants.COVER_PHOTO, getStringJSONObject(data, "coverPhoto"));
            Singleton.preferenceEditor.putString(Constants.WEBSITE, getStringJSONObject(data, "website"));
            Singleton.preferenceEditor.putString(Constants.GENDER, getStringJSONObject(data,"gender"));
            Singleton.preferenceEditor.putString(Constants.EMAIL, getStringJSONObject(data, "email"));
            Singleton.preferenceEditor.putString(Constants.COUNTRY_CODE, getStringJSONObject(data, "countryCode"));
            Singleton.preferenceEditor.putInt(Constants.IS_ADMIN_V2, getIntJSONObject(data, "isAdmin"));
            Singleton.preferenceEditor.putString(Constants.IS_FREEZED, getStringJSONObject(data, "isFreezed"));
            Singleton.preferenceEditor.putString(Constants.PUSH_LIKE, getStringJSONObject(data, "pushLike"));
            Singleton.preferenceEditor.putString(Constants.PUSH_FOLLOW, getStringJSONObject(data, "pushFollow"));
            Singleton.preferenceEditor.putString(Constants.PUSH_COMMENT, getStringJSONObject(data, "pushComment"));
            Singleton.preferenceEditor.putString(Constants.PUSH_TAG, getStringJSONObject(data, "pushTag"));
            Singleton.preferenceEditor.putString(Constants.PUSH_FRIEND_JOIN, getStringJSONObject(data, "pushFriendJoin"));
            Singleton.preferenceEditor.putString(Constants.PUSH_FRIEND_FIRST_POST, getStringJSONObject(data, "pushFriendFirstPost"));
            Singleton.preferenceEditor.putString(Constants.PUSH_FOLLOW_REQUEST, getStringJSONObject(data, "pushFriendRequest"));
            Singleton.preferenceEditor.putString(Constants.PUSH_SYSTEM_NOTIF, getStringJSONObject(data, "pushSystemNotif"));
            Singleton.preferenceEditor.putString(Constants.NOTIF_BADGE, getStringJSONObject(data, "notifBadge"));
            Singleton.preferenceEditor.putString(Constants.SYSTEM_NOTIF_BADGE, getStringJSONObject(data, "systemNotifBadge"));
            Singleton.preferenceEditor.putString(Constants.PUSH_FOLLOW_REQUEST, getStringJSONObject(data, "pushFollowRequest"));
            Singleton.preferenceEditor.putString(Constants.MESSAGE_BADGE, getStringJSONObject(data, "messageBadge"));
            Singleton.preferenceEditor.putString(Constants.BANK_NAME, getStringJSONObject(data, "bankName"));
            Singleton.preferenceEditor.putString(Constants.BANK_ADDRESS, getStringJSONObject(data, "bankAddress"));
            Singleton.preferenceEditor.putString(Constants.ACCOUNT_NUMBER, getStringJSONObject(data, "accountNumber"));
            Singleton.preferenceEditor.putString(Constants.BENEFICIARY_NAME, getStringJSONObject(data, "beneficiaryName"));
            Singleton.preferenceEditor.putString(Constants.SWIFT_CODE, getStringJSONObject(data, "swiftCode"));
            Singleton.preferenceEditor.putString(Constants.ROUNTING_NUMBER, getStringJSONObject(data, "rountingNumber"));
            Singleton.preferenceEditor.putString(Constants.BANK_COUNTRY, getStringJSONObject(data, "bankCountry"));
            Singleton.preferenceEditor.putString(Constants.ID_CARD_NUMBER, getStringJSONObject(data, "idCardNumber"));
            Singleton.preferenceEditor.putString(Constants.ADDRESS, getStringJSONObject(data, "address"));
            Singleton.preferenceEditor.putString(Constants.BANK_CODE, getStringJSONObject(data, "bankCode"));
            Singleton.preferenceEditor.putString(Constants.BANK_BRANCH_CODE, getStringJSONObject(data, "bankBranchCode"));
            Singleton.preferenceEditor.putString(Constants.ID_CARD_FRONT_PICTURE, getStringJSONObject(data, "idCardFrontPicture"));
            Singleton.preferenceEditor.putString(Constants.ID_CARD_BACK_PICTURE, getStringJSONObject(data, "idCardBackPicture"));
            Singleton.preferenceEditor.putString(Constants.PASSBOOK_PICTURE, getStringJSONObject(data, "passbookPicture"));
            Singleton.preferenceEditor.putString(Constants.BANK_ACCOUNT_VERIFIED, getStringJSONObject(data, "bankAccountVerified"));
            Singleton.preferenceEditor.putString(Constants.TOTAL_REVENUE_EARNED, getStringJSONObject(data, "totalRevenueEarned"));
            Singleton.preferenceEditor.putString(Constants.TWITTER_ID, getStringJSONObject(data, "twitterID"));
            Singleton.preferenceEditor.putString(Constants.FACEBOOK_ID, getStringJSONObject(data, "facebookID"));
            Singleton.preferenceEditor.putString(Constants.INSTAGRAM_ID, getStringJSONObject(data, "instagramID"));

            Singleton.preferenceEditor.putInt(Constants.AGE, getIntJSONObject(data, "age"));
            Singleton.preferenceEditor.putInt(Constants.IS_VERIFIED, getIntJSONObject(data, "isVerified"));
            Singleton.preferenceEditor.putInt(Constants.FOLLOWER_COUNT, getIntJSONObject(data, "followerCount"));
            Singleton.preferenceEditor.putInt(Constants.FOLLOWING_COUNT, getIntJSONObject(data, "followingCount"));
            Singleton.preferenceEditor.putInt(Constants.POST_COUNT, getIntJSONObject(data, "postCount"));
            Singleton.preferenceEditor.putInt(Constants.REPOST_COUNT, getIntJSONObject(data, "repostCount"));
            Singleton.preferenceEditor.putInt(Constants.LIKE_POST_COUNT, getIntJSONObject(data, "likePostCount"));
            Singleton.preferenceEditor.putInt(Constants.IS_CHOICE, getIntJSONObject(data, "isChoice"));
            Singleton.preferenceEditor.putInt(Constants.LAST_LOGIN, getIntJSONObject(data, "lastLogin"));
            Singleton.preferenceEditor.putInt(Constants.ECEIVEDLIKECOUNT, getIntJSONObject(data,"receivedLikeCount"));
            Singleton.preferenceEditor.putInt(Constants.FOLLOWING_COUNT_V2, getIntJSONObject(data, "followingCount"));
            Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, getIntJSONObject(data, "point"));
            Singleton.preferenceEditor.putString(Constants.GIFT_TOTAL_REVENUE, getStringJSONObject(data, "totalGiftRevenueEarned"));

            Singleton.preferenceEditor.putInt(Constants.SHOW_AD, getIntJSONObject(data, "adsOn"));
            Singleton.preferenceEditor.putString(Constants.VERIFIED_PHONE_NUMBER, getStringJSONObject(data, "verifiedPhoneNumber"));

            Singleton.preferenceEditor.putInt(Constants.GIFT_MODULE_STATE, getIntJSONObject(data, "giftModuleState"));
            Singleton.preferenceEditor.putInt(Constants.HAS_SHOWN_GIFT_MODULE_TUTORIAL, getIntJSONObject(data, "hasShownGiftModuleTutorial"));
            Singleton.preferenceEditor.putString(Constants.APP_UPDATE_LINK, getStringJSONObject(data, "appUpdateLink"));
            Singleton.preferenceEditor.putInt(Constants.FORCE_UPDATE_APP, getIntJSONObject(data, "forceUpdateApp"));
            Singleton.preferenceEditor.putString(Constants.LATEST_APP_VERSION, getStringJSONObject(data, "latestAppVersion"));

            Singleton.preferenceEditor.putString(Constants.COUNTRY_CALLING_CODE, getStringJSONObject(data, "countryCallingCode"));
            Singleton.preferenceEditor.putString(Constants.LOCAL_PHONE_NUMBER, getStringJSONObject(data, "localPhoneNumber"));
            Singleton.preferenceEditor.putString(Constants.PHONE_TWO_DIGIT_ISO, getStringJSONObject(data, "phoneTwoDigitISO"));

            Singleton.preferenceEditor.commit();

            if(getStringJSONObject(data, "pushLike").compareTo("off")==0) setConfig(context,Constants.SETTING_NOTIFI_LIKE, 0);
            else if(getStringJSONObject(data, "pushLike").compareTo("followerOnly")==0) setConfig(context,Constants.SETTING_NOTIFI_LIKE, 1);
            else setConfig(context,Constants.SETTING_NOTIFI_LIKE, 2);

            if(getStringJSONObject(data, "pushComment").compareTo("off")==0) setConfig(context,Constants.SETTING_NOTIFI_COMMENT, 0);
            else if(getStringJSONObject(data, "pushComment").compareTo("followerOnly")==0) setConfig(context,Constants.SETTING_NOTIFI_COMMENT, 1);
            else setConfig(context,Constants.SETTING_NOTIFI_COMMENT, 2);

            if(getStringJSONObject(data, "pushTag").compareTo("off")==0) setConfig(context,Constants.SETTING_NOTIFI_TAG, 0);
            else if(getStringJSONObject(data, "pushTag").compareTo("followerOnly")==0) setConfig(context,Constants.SETTING_NOTIFI_TAG, 1);
            else setConfig(context,Constants.SETTING_NOTIFI_TAG, 2);

            setConfig(context,Constants.SETTING_NOTIFI_FANS,getIntJSONObject(data, "pushFollow"));
            setConfig(context,Constants.SETTING_NOTIFI_FRIEND,getIntJSONObject(data, "pushFriendJoin"));
            setConfig(context,Constants.SETTING_NOTIFI_SYSTEM,getIntJSONObject(data, "pushSystemNotif"));
            setConfig(context,Constants.SETTING_NOTIFI_LIVE,getIntJSONObject(data, "pushLiveStream"));
            setConfig(context,Constants.SETTING_NOTIFI_RELIVE,getIntJSONObject(data, "pushLiveRestream"));

            setConfig(context,Constants.REVENUE_NAME,getStringJSONObject(data, "idCardName"));
            setConfig(context,Constants.REVENUE_ID,getStringJSONObject(data, "idCardNumber"));
            setConfig(context,Constants.REVENUE_BANK_NAME,getStringJSONObject(data, "bankName"));
            setConfig(context,Constants.REVENUE_BANK_BRANCH,getStringJSONObject(data, "branchName"));
            setConfig(context,Constants.REVENUE_BANK_NUMBER,getStringJSONObject(data, "accountNumber"));
            setConfig(context,Constants.REVENUE_ACCOUNT_NAME,getStringJSONObject(data, "beneficiaryName"));

            setConfig(context,Constants.REVENUE_ID_FRONT,getStringJSONObject(data, "idCardFrontPicture"));
            setConfig(context,Constants.REVENUE_ID_BACK,getStringJSONObject(data, "idCardBackPicture"));
            setConfig(context,Constants.REVENUE_ACCOUNT_PICTURE,getStringJSONObject(data, "passbookPicture"));

            setConfig(context,Constants.REVENUE_PAYPAL, getStringJSONObject(data, "paypalEmail"));
            setConfig(context,Constants.REVENUE_ALIPAY, getStringJSONObject(data, "alipayPhone"));

            if (getIntJSONObject(data,"isFreezed")==1)
            {
                Singleton.generateIsFreezedFile();
            }
        }
        catch (Exception e)
        {
            Singleton.log(e.toString());
        }
    }

    public static void setConfig(Context context, String key, int value)
    {
        SharedPreferences settings = context.getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putInt(key, value);
        PE.commit();
    }

    public static void setConfig(Context context, String key, String value)
    {
        SharedPreferences settings = context.getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    /*
     * Solve ProfileFragment saveUserInfo after getUserPost have a null data problem.
     * if JSONObject data get a null value, it'll return a default a empty string or 0 integer.
     */
    private static String getStringJSONObject(JSONObject data, String key){
        try{
            return data.getString(key);
        }
        catch(Exception e){
            return "";
        }
    }

    private static int getIntJSONObject(JSONObject data, String key){
        try {
            return data.getInt(key);
        }
        catch(Exception e){
            return 0;
        }
    }

    private static void addCommonFields(JSONObject params)
    {
        try
        {
            if(mClient==null)
            {
                mClient = new OkHttpClient();

                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager()
                {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain,String authType) throws CertificateException
                    {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain,String authType) throws CertificateException
                    {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers()
                    {
                        return new X509Certificate[0];
                    }
                } };

                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
                mClient.setSslSocketFactory(sslSocketFactory);

                mClient.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });
            }

            if(handler==null) handler = new Handler(Looper.getMainLooper());

            if(!params.has("userID")) params.put("userID", Singleton.preferences.getString(Constants.USER_ID, ""));
            if(!params.has("deviceID")) params.put("deviceID", Singleton.preferences.getString(Constants.DEVICE_ID, ""));
            if(!params.has("accessToken")) params.put("accessToken", Singleton.preferences.getString(Constants.ACCESS_TOKEN, ""));
            if(!params.has("nonce")) params.put("nonce", UUID.randomUUID().toString());
            if(!params.has("ipCountry")) params.put("ipCountry", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));

            params.put("packageName", Constants.PackgeName);
            params.put("version", Singleton.getVersion());
            params.put("language", Singleton.applicationContext.getString(R.string.current_language));
            params.put("deviceType", "ANDROID");
            if(!Constants.INTERNATIONAL_VERSION) params.put("chinaVersion", "1");
        }
        catch (Exception e)
        {

        }
    }

    private static void post(Context context, String url, JSONObject params, Callback callback)
    {
        try
        {
            params.put("action", url);
            addCommonFields(params);

//            String[] data = RSAUtils.getRsaEncodeData(context, params.toString());
//            RequestBody body = new FormEncodingBuilder()
//            .add("data", data[0])
//            .add("key", data[1])
//            .build();

            RequestBody body = new FormEncodingBuilder()
            .add("data", params.toString())
            .add("key", "")
            .add("cypher", "0_v2")
            .build();

            Request request = new Request.Builder().url(getAbsoluteUrl("apiGateWay")).post(body).build();
            mClient.newCall(request).enqueue(callback);

//            mClient.newCall(request).enqueue(new Callback()
//            {
//                @Override
//                public void onResponse(Response response) throws IOException
//                {
//                    Log.d("123", "status code : " + response.code());
//                    Log.d("123", "protocol: " + response.protocol().toString());
//                    if(response.isSuccessful())
//                    {
//                        String body = response.body().string();
//                    }
//                }
//
//                @Override
//                public void onFailure(Request request, IOException e)
//                {
//                }
//            });
        }
        catch (Exception e)
        {

        }
    }

//    private static void postlogin(Context context, String url, JSONObject params, AsyncHttpResponseHandler responseHandler)
//    {
//        try
//        {
//            params.put("action", url);
//
//            if(clientLogin==null)
//            {
//                clientLogin = new AsyncHttpClient(true, 8080, 8443);//true, 8080, 8443
//                clientLogin.setAuthenticationPreemptive(false);
//                clientLogin.setMaxConnections(10);
//                clientLogin.setTimeout(10000);
//                clientLogin.setResponseTimeout(10000);
//            }
//
//            if(!params.has("userID")) params.put("userID", Singleton.preferences.getString(Constants.USER_ID, ""));
//            if(!params.has("deviceID")) params.put("deviceID", Singleton.preferences.getString(Constants.DEVICE_ID, ""));
//            if(!params.has("accessToken")) params.put("accessToken", Singleton.preferences.getString(Constants.ACCESS_TOKEN, ""));
//            if(!params.has("nonce")) params.put("nonce", UUID.randomUUID().toString());
//
//            params.put("version", Singleton.getVersion());
//            params.put("language", Singleton.applicationContext.getString(R.string.current_language));
//            params.put("deviceType", "ANDROID");
//
//            String[] data = RSAUtils.getRsaEncodeData(context, params.toString());
//
//            RequestParams rsa = new RequestParams();
//            rsa.put("data", data[0]);
//            rsa.put("key", data[1]);
//
//            clientLogin.post(getAbsoluteUrl("apiGateWay"), rsa, responseHandler);
//        }
//        catch (Exception e)
//        {
//
//        }
//    }

    private static void LocalCheckCode(String key, String data)
    {
        try
        {
//        String Local = "";
//        String Server = "";
//        String Key = "";

            byte[] mDataByte = RSAUtils.decryptAES(key.getBytes("UTF-8"), Base64Utils.decode(data));
            String code = new String(mDataByte,"UTF-8");
            Log.d("123", "code = " + code);
        }
        catch (UnsupportedEncodingException e)
        {

        }
    }

    private static String getDecodeText(Context context, String data)
    {
        try
        {
            JSONObject info = new JSONObject(data);
            String mKey = info.getString(Constants.KEY);
            String mData = info.getString(Constants.DATA);

//            String text = RSAUtils.getRsaDecodeData(context, mData,mKey);
//            if(ShowLog()) Log.d("123","text : " + text);
//            return text;
            return mData;
        }
        catch (OutOfMemoryError o)
        {
            return "";
        }
        catch (JSONException e)
        {
            return "";
        }
    }

//    private static void postPhoto(Context context, String url, JSONObject params, AsyncHttpResponseHandler responseHandler)
//    {
//        try
//        {
//            params.put("action", url);
//
//            if(client==null)
//            {
//                client = new AsyncHttpClient(true, 8080, 8443);
//                client.setAuthenticationPreemptive(false);
//                client.setMaxConnections(30);
//                client.setTimeout(50000);
//                client.setResponseTimeout(50000);
//            }
//
//            if(!params.has("userID")) params.put("userID", Singleton.preferences.getString(Constants.USER_ID, ""));
//            if(!params.has("deviceID")) params.put("deviceID", Singleton.preferences.getString(Constants.DEVICE_ID, ""));
//            if(!params.has("accessToken")) params.put("accessToken", Singleton.preferences.getString(Constants.ACCESS_TOKEN, ""));
//            params.put("version", Singleton.getVersion());
//            params.put("language", Singleton.applicationContext.getString(R.string.current_language));
//            params.put("deviceType", "ANDROID");
//            if(!params.has("nonce")) params.put("nonce", UUID.randomUUID().toString());
//
//            String[] data = RSAUtils.getRsaEncodeData(context, params.toString());
//
//            RequestParams rsa = new RequestParams();
//            rsa.put("data", data[0]);
//            rsa.put("key", data[1]);
//
////            LocalCheckCode(data[2], data[0]);
//
//            if(ShowLog())
//            {
//                Log.d("123","params : " + params);
//                Log.d("123","data : " + data[0]);
//                Log.d("123","AES : " + data[2]);
//            }
//
//            client.post(getAbsoluteUrl("apiGateWay"), rsa, responseHandler);
//        }
//        catch (Exception e)
//        {
//
//        }
//    }

    public static void postPhoto(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
    {
        try
        {
            if(client==null)
            {
                client = new AsyncHttpClient(80);
                client.setAuthenticationPreemptive(false);
                client.setMaxConnections(30);
                client.setTimeout(50000);
                client.setResponseTimeout(50000);
            }

            if(!params.has("userID")) params.put("userID", Singleton.preferences.getString(Constants.USER_ID, ""));
            if(!params.has("deviceID")) params.put("deviceID", Singleton.preferences.getString(Constants.DEVICE_ID, ""));
            if(!params.has("accessToken")) params.put("accessToken", Singleton.preferences.getString(Constants.ACCESS_TOKEN, ""));
            params.put("packageName", Constants.PackgeName);
            params.put("version", Singleton.getVersion());
            params.put("language", Singleton.applicationContext.getString(R.string.current_language));
            params.put("deviceType", "ANDROID");
            if(!Constants.INTERNATIONAL_VERSION) params.put("chinaVersion", "1");
            if(!params.has("nonce")) params.put("nonce", UUID.randomUUID().toString());

            client.post(Constants.SERVER_IP + url, params,responseHandler);
        }
        catch (Exception e)
        {

        }
    }

//    public static void postPhoto(Context context, String url, RequestParams params, JsonHttpResponseHandler responseHandler)
//    {
//        try
//        {
//            if(client==null)
//            {
//                client = new AsyncHttpClient(true, 8080, 8443);
//                client.setAuthenticationPreemptive(false);
//                client.setMaxConnections(30);
//                client.setTimeout(50000);
//                client.setResponseTimeout(50000);
//            }
//
//            if(!params.has("userID")) params.put("userID", Singleton.preferences.getString(Constants.USER_ID, ""));
//            if(!params.has("deviceID")) params.put("deviceID", Singleton.preferences.getString(Constants.DEVICE_ID, ""));
//            if(!params.has("accessToken")) params.put("accessToken", Singleton.preferences.getString(Constants.ACCESS_TOKEN, ""));
//            params.put("version", Singleton.getVersion());
//            params.put("language", Singleton.applicationContext.getString(R.string.current_language));
//            params.put("deviceType", "ANDROID");
//            if(!params.has("nonce")) params.put("nonce", UUID.randomUUID().toString());
//
//            client.post(getAbsoluteUrl(url), params,responseHandler);
//        }
//        catch (Exception e)
//        {
//
//        }
//    }

    private static Boolean ShowLog()
    {
        return false;
    }
}
