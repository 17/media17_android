package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.model.LiveGiftsModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by POPO on 2015/11/17.
 */
public class MyPresendActivity extends BaseNewActivity
{
    private MyPresendActivity mCtx = this;
    private LayoutInflater inflater;

    private PullToRefreshListView mList;
    private ProgressBar mProgress;
    private ImageView mNoData;

    private ArrayList<LiveGiftsModel> mModels = new ArrayList<LiveGiftsModel>();
    private MyAdapter mMyAdapter;

    private DisplayImageOptions SelfOptions;
    private Boolean isFetchingData = false;
    private Boolean noMoreData = false;

    private SimpleDateFormat mDateFormat;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_presend_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        mList = (PullToRefreshListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                ApiManager.getReceivedGiftLog(mCtx, Integer.MAX_VALUE, 15, new ApiManager.GetLiveStreamReceivedGiftsCallback() {
                    @Override
                    public void onResult(boolean success, ArrayList<LiveGiftsModel> liveGiftsModels) {
                        mList.onRefreshComplete();

                        if (success && liveGiftsModels != null) {
                            if (liveGiftsModels.size() != 0) {
                                mNoData.setVisibility(View.GONE);
                                mModels.clear();
                                mModels.addAll(liveGiftsModels);

                                if (mMyAdapter != null) mMyAdapter = null;
                                mMyAdapter = new MyAdapter();
                                mList.setAdapter(mMyAdapter);

                                isFetchingData = false;
                                noMoreData = false;

                                if (liveGiftsModels.size() < 15) {
                                    noMoreData = true;
                                }
                            } else {
                                mNoData.setVisibility(View.VISIBLE);
                            }
                        } else {
                            mNoData.setVisibility(View.VISIBLE);
                            try{
//                              showToast(getString(R.string.failed));
                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                        }
                    }
                });
            }
        });

        mList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_FLING) {
                    ImageLoader.getInstance().pause();
                }

                if (scrollState == SCROLL_STATE_IDLE) {
                    ImageLoader.getInstance().resume();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        mProgress.setVisibility(View.VISIBLE);

        ApiManager.getReceivedGiftLog(mCtx, Integer.MAX_VALUE, 15, new ApiManager.GetLiveStreamReceivedGiftsCallback() {
            @Override
            public void onResult(boolean success, ArrayList<LiveGiftsModel> liveGiftsModels) {
                mProgress.setVisibility(View.GONE);

                if (success && liveGiftsModels != null) {
                    if (liveGiftsModels.size() != 0) {
                        mNoData.setVisibility(View.GONE);
                        mModels.clear();
                        mModels.addAll(liveGiftsModels);
                        mMyAdapter = new MyAdapter();
                        mList.setAdapter(mMyAdapter);

                        if (liveGiftsModels.size() < 15) {
                            noMoreData = true;
                        }
                    } else {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    try {
                        mNoData.setVisibility(View.VISIBLE);
                        try{
//                              showToast(getString(R.string.failed));
                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    } catch (Exception e) {

                    }
                }
            }
        });

        mDateFormat = new SimpleDateFormat("yyyyMM - dd");

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.give_me_presend));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class MyAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderGrid holder = new ViewHolderGrid();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.my_presend_row, null);
                holder.presend_layout = (LinearLayout) convertView.findViewById(R.id.presend_layout);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.presend = (TextView) convertView.findViewById(R.id.presend);

                convertView.setTag(holder);
            }
            else holder = (ViewHolderGrid) convertView.getTag();

            final int pos = position;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getUserInfo().getPicture()), holder.pic, SelfOptions);
            holder.name.setText(mModels.get(position).getUserInfo().getOpenID());

            if(mModels.get(pos).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            Date date = new Date(mModels.get(position).getTimestamp()*1000L);
            holder.date.setText(mDateFormat.format(date).substring(0,4) + "\n" + mDateFormat.format(date).substring(4,mDateFormat.format(date).length()));
            holder.presend.setText(mModels.get(position).getGiftInfo().getName());

            holder.presend_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if (mModels.get(pos).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mModels.get(pos).getUserInfo().getName());
                        intent.putExtra("picture", mModels.get(pos).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mModels.get(pos).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mModels.get(pos).getUserInfo().getPostCount());
                        intent.putExtra("follow", mModels.get(pos).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mModels.get(pos).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mModels.get(pos).getUserInfo().getOpenID());
                        intent.putExtra("bio", mModels.get(pos).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mModels.get(pos).getUserInfo().getUserID());
                        intent.putExtra("web", mModels.get(pos).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

//            if(position>=getCount()-5)
//            {
//                LoadDataFollow(false);
//            }

            return convertView;
        }
    }

    private class ViewHolderGrid
    {
        LinearLayout presend_layout;
        TextView date;
        ImageView pic;
        TextView name;
        ImageView verifie;
        TextView presend;
    }
}
