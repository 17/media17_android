package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.helpshift.Helpshift;
import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by POPO on 6/10/15.
 */
public class SettingActivity extends BaseActivity
{
    private SettingActivity mCtx = this;
    private Story17Application mApplication;
    private Button mLogout;
    private TextView mCurrencyTextView;

    private LinearLayout mEdit, mPassword, mNotifi, mSave, mFriend, mCall , mCurrency,mSetPrivateLayout,mFaqLayout;
    private LinearLayout mBlockLayout, mFollowLayout,mLoveCountLayout;
    private ImageView mCheckBox, mSetPrivate;
    private ImageView mSetFollow;
    private LinearLayout mRebindingPhoneLayout;
    ArrayList<String> currencyList = new ArrayList<String>();
    private TextView mVersionCode;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();

        initTitleBar();

        mEdit = (LinearLayout) findViewById(R.id.edit_layout);
        mPassword = (LinearLayout) findViewById(R.id.password_layout);
        mNotifi = (LinearLayout) findViewById(R.id.notifi_layout);
        mSave = (LinearLayout) findViewById(R.id.save_layout);
        mFriend = (LinearLayout) findViewById(R.id.friend_layout);
        mCall = (LinearLayout) findViewById(R.id.call_layout);
        mCurrency = (LinearLayout) findViewById(R.id.currecncy_layout);
        mCurrencyTextView = (TextView) findViewById(R.id.currency_type_text);
        mBlockLayout = (LinearLayout) findViewById(R.id.block_layout);
        mFollowLayout = (LinearLayout) findViewById(R.id.set_follow_layout);
        mSetFollow = (ImageView) findViewById(R.id.set_follow);

        mCheckBox = (ImageView) findViewById(R.id.save);
        mLogout = (Button) findViewById(R.id.logout);

        mSetPrivateLayout = (LinearLayout) findViewById(R.id.set_private_layout);
        mSetPrivate = (ImageView) findViewById(R.id.set_private);
        mFaqLayout = (LinearLayout) findViewById(R.id.faq_layout);
        mLoveCountLayout = (LinearLayout) findViewById(R.id.love_count_layout);
        mRebindingPhoneLayout = (LinearLayout) findViewById(R.id.rebinding_phone_layout);

        mVersionCode = (TextView) findViewById(R.id.version_code);
        mVersionCode.setText(Singleton.getVersion());

        Set<String> keys = Singleton.mCurrencyRate.keySet();
        for(String key: keys){
            currencyList.add(key);
        }

        mCurrencyTextView.setText(Singleton.getCurrencyType());

        mCurrency.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int index = currencyList.indexOf(Singleton.getCurrencyType());

                showSingleItemPicker(getString(R.string.select_currency), currencyList, index, new SingleItemPickerCallback() {
                    @Override
                    public void onResult(boolean done, int selectedItem) {

                        if (done) {

                            Singleton.preferenceEditor.putInt(Constants.CURRENCY_RATE_SETTED, 1);
                            Singleton.preferenceEditor.putString(Constants.CURRENCY_RATE, currencyList.get(selectedItem));
                            Singleton.preferenceEditor.commit();
                            mCurrencyTextView.setText(Singleton.getCurrencyType());

                        }
                    }
                });
            }
        });


        mEdit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, ProfileEditActivity.class);
                startActivity(intent);
            }
        });

        mRebindingPhoneLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, BindingPhoneNumberActivity.class);
                intent.putExtra("Binding", 1);
                startActivity(intent);
                mCtx.finish();
            }
        });

        mPassword.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, SettingPasswordActivity.class);
                startActivity(intent);
            }
        });

        mNotifi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, SettingNotifiActivity.class);
                startActivity(intent);
            }
        });

        if(getConfig(Constants.SAVE_PICTURE,"1").compareTo("1")==0) mCheckBox.setImageResource(R.drawable.toggle_active);
        else mCheckBox.setImageResource(R.drawable.toggle);

        mSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(getConfig(Constants.SAVE_PICTURE,"1").compareTo("1")==0)
                {
                    mCheckBox.setImageResource(R.drawable.toggle);
                    setConfig(Constants.SAVE_PICTURE,"0");
                }
                else
                {
                    mCheckBox.setImageResource(R.drawable.toggle_active);
                    setConfig(Constants.SAVE_PICTURE, "1");
                }
            }
        });

        mCheckBox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(getConfig(Constants.SAVE_PICTURE,"1").compareTo("1")==0)
                {
                    mCheckBox.setImageResource(R.drawable.toggle);
                    setConfig(Constants.SAVE_PICTURE,"0");
                }
                else
                {
                    mCheckBox.setImageResource(R.drawable.toggle_active);
                    setConfig(Constants.SAVE_PICTURE, "1");
                }
            }
        });

        if(getConfig(Constants.SETTING_PRIVATE_MODE,"0").compareTo("1")==0) mSetPrivate.setImageResource(R.drawable.toggle_active);
        else mSetPrivate.setImageResource(R.drawable.toggle);

        mSetPrivateLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(getConfig(Constants.SETTING_PRIVATE_MODE,"1").compareTo("1")==0)
                {
                    mSetPrivate.setImageResource(R.drawable.toggle);
                    setConfig(Constants.SETTING_PRIVATE_MODE,"0");
                    setPrivateMode("open");
                }
                else
                {
                    mSetPrivate.setImageResource(R.drawable.toggle_active);
                    setConfig(Constants.SETTING_PRIVATE_MODE, "1");
                    setPrivateMode("private");
                }
            }
        });

        mSetPrivate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(getConfig(Constants.SETTING_PRIVATE_MODE,"1").compareTo("1")==0)
                {
                    mSetPrivate.setImageResource(R.drawable.toggle);
                    setConfig(Constants.SETTING_PRIVATE_MODE,"0");
                    setPrivateMode("open");
                }
                else
                {
                    mSetPrivate.setImageResource(R.drawable.toggle_active);
                    setConfig(Constants.SETTING_PRIVATE_MODE, "1");
                    setPrivateMode("private");
                }
            }
        });

        mBlockLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, SettingBlockActivity.class);
                startActivity(intent);
            }
        });

        if(getConfig(Constants.SETTING_FOLLOW_MODE,"0").compareTo("1")==0) mSetFollow.setImageResource(R.drawable.toggle_active);
        else mSetFollow.setImageResource(R.drawable.toggle);

        mFollowLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(getConfig(Constants.SETTING_FOLLOW_MODE,"1").compareTo("1")==0)
                {
                    mSetFollow.setImageResource(R.drawable.toggle);
                    setConfig(Constants.SETTING_FOLLOW_MODE,"0");
                    setFollowMode("0");
                }
                else
                {
                    mSetFollow.setImageResource(R.drawable.toggle_active);
                    setConfig(Constants.SETTING_FOLLOW_MODE, "1");
                    setFollowMode("1");
                }
            }
        });

        mSetFollow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(getConfig(Constants.SETTING_FOLLOW_MODE,"1").compareTo("1")==0)
                {
                    mSetFollow.setImageResource(R.drawable.toggle);
                    setConfig(Constants.SETTING_FOLLOW_MODE,"0");
                    setFollowMode("0");
                }
                else
                {
                    mSetFollow.setImageResource(R.drawable.toggle_active);
                    setConfig(Constants.SETTING_FOLLOW_MODE, "1");
                    setFollowMode("1");
                }
            }
        });

        mFriend.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                //event tracking [FIX-ME] channel is hard to parse currently. 
                try{
                    LogEventUtil.ShareApp(mCtx,mApplication,"none");
                }catch (Exception X)
                {

                }
                shareTo(getString(R.string.share_subject), getString(R.string.share_body1) + Singleton.preferences.getString(Constants.OPEN_ID, "") + getString(R.string.share_body2) + " " + Constants.SHARE_LINK_APP, getString(R.string.share_title));
            }
        });

        mFaqLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                HashMap config = new HashMap();
                config.put("notificationIcon", R.mipmap.notifibage);
                config.put("enableInAppNotification", true);
                Helpshift.install(mApplication, "754b7c20139a1dae4b6c15ebfeb2b851", "machipopo.helpshift.com", "machipopo_platform_20150907061055486-02d7e391a75b284", config);
                Helpshift.setUserIdentifier(Singleton.preferences.getString(Constants.OPEN_ID, ""));
                Helpshift.showFAQs(mCtx);
            }
        });

        mCall.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                HashMap config = new HashMap();
                config.put("notificationIcon", R.mipmap.notifibage);
                config.put("enableInAppNotification", true);
                Helpshift.install(mApplication, "754b7c20139a1dae4b6c15ebfeb2b851", "machipopo.helpshift.com", "machipopo_platform_20150907061055486-02d7e391a75b284", config);
                Helpshift.setUserIdentifier(Singleton.preferences.getString(Constants.OPEN_ID, ""));
                HashMap ConversationConfig = new HashMap ();
                ConversationConfig.put("hideNameAndEmail", true);
                Helpshift.showConversation(mCtx,ConversationConfig);

//                Intent email = new Intent(Intent.ACTION_SEND);
//                email.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@machipopo.com"});
//                email.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.report_bug));
//                email.putExtra(Intent.EXTRA_TEXT, "");
//                email.setType("message/rfc822");
//                startActivity(Intent.createChooser(email, getString(R.string.report_title)));
            }
        });

        mLogout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mApplication.setLogout(true);
                mCtx.finish();
            }
        });

        mLoveCountLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, SettingMyCountActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.setting));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });

    }

    private void setPrivateMode(String mode)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("privacyMode", mode);

            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
            {
                @Override
                public void onResult(boolean success, String message)
                {
                    if (success)
                    {

                    }
                }
            });
        }
        catch (JSONException e)
        {

        }
    }

    private void setFollowMode(String mode)
    {
        try
        {
            JSONObject params = new JSONObject();
            params.put("followPrivacyMode", mode);

            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
            {
                @Override
                public void onResult(boolean success, String message)
                {
                    if (success)
                    {

                    }
                }
            });
        }
        catch (JSONException e)
        {

        }
    }

    private void shareTo(String subject, String body, String chooserTitle) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    public String getConfig(String key,String def)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, def);
    }

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }
}
