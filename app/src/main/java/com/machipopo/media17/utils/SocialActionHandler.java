package com.machipopo.media17.utils;

public interface SocialActionHandler {
    public void handleHashtag(String hashtag);
    public void handleMention(String mention);
}
