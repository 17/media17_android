package com.machipopo.media17.utils;

import android.content.Context;
import android.util.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class RSAUtils
{
    private static String RSA = "RSA";

    public static String[] getRsaEncodeData(Context context, String data)
    {
        try
        {
            String[] mEncodeText = new String[3];

            UUID mUUID = UUID.randomUUID();
            String mNewUUID = mUUID.toString().replaceAll("-", "");

//            byte[] mDataByte = encryptAES(mNewUUID.getBytes("UTF-8"), data.getBytes("UTF-8"));
//            mEncodeText[0] = Base64Utils.encode(mDataByte);

            mEncodeText[0] = AESUtils.encrypt(mNewUUID,data);

            InputStream mInPublic = context.getResources().getAssets().open("public.pem");
            PublicKey mPublicKey = loadPublicKey(mInPublic);
            byte[] mEncryptByte = encryptData(mNewUUID.getBytes(), mPublicKey);
            mEncodeText[1] = Base64Utils.encode(mEncryptByte);

            mEncodeText[2] = mNewUUID;

//            String[] mEncodeText = new String[3];
//            UUID mUUID = UUID.randomUUID();
//            String mNewUUID = mUUID.toString().replaceAll("-", "");
//            byte[] mDataByte = encryptAES(mNewUUID.getBytes("UTF-8"), data.getBytes("UTF-8"));
//            mEncodeText[0] = Base64.encodeToString(mDataByte, Base64.DEFAULT);
//            InputStream mInPublic = context.getResources().getAssets().open("public.pem");
//            PublicKey mPublicKey = loadPublicKey(mInPublic);
//            byte[] mEncryptByte = encryptData(mNewUUID.getBytes(), mPublicKey);
//            mEncodeText[1] = Base64Utils.encode(mEncryptByte);
//            mEncodeText[2] = mNewUUID;

            return mEncodeText;
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public static String getRsaDecodeData(Context context, String data, String rsa)
    {
        try
        {
            InputStream mInPrivate = context.getResources().getAssets().open("private.pem");
            PrivateKey mPrivateKey = loadPrivateKey(mInPrivate);
            byte[] mDecryptByte = decryptData(Base64.decode(rsa.getBytes(),Base64.DEFAULT), mPrivateKey);
            String mDecodeText = new String(mDecryptByte);
            String mAes = mDecodeText.substring(mDecodeText.length()-32,mDecodeText.length());

//            byte[] mDataByte = decryptAES(mAes.getBytes("UTF-8"), Base64.decode(data.getBytes("UTF-8"), Base64.DEFAULT));
//            return new String(mDataByte,"UTF-8");

            return AESUtils.decrypt(mAes, data);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static KeyPair generateRSAKeyPair()
    {
        return generateRSAKeyPair(1024);
    }

    public static KeyPair generateRSAKeyPair(int keyLength)
    {
        try
        {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(RSA);
            kpg.initialize(keyLength);

            return kpg.genKeyPair();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] encryptData(byte[] data, PublicKey publicKey)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return cipher.doFinal(data);

//            Cipher cipher = Cipher.getInstance(RSA);
//            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
//            return cipher.doFinal(data);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] decryptData(byte[] encryptedData, PrivateKey privateKey)
    {
        try
        {
            Cipher cipher = Cipher.getInstance(RSA);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            return cipher.doFinal(encryptedData);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static PublicKey getPublicKey(byte[] keyBytes) throws NoSuchAlgorithmException,InvalidKeySpecException
    {
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(RSA);
        PublicKey publicKey = keyFactory.generatePublic(keySpec);

        return publicKey;
    }

    public static PrivateKey getPrivateKey(byte[] keyBytes) throws NoSuchAlgorithmException,InvalidKeySpecException
    {
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(RSA);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);

        return privateKey;
    }

    public static PublicKey getPublicKey(String modulus, String publicExponent)throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        BigInteger bigIntModulus = new BigInteger(modulus);
        BigInteger bigIntPrivateExponent = new BigInteger(publicExponent);
        RSAPublicKeySpec keySpec = new RSAPublicKeySpec(bigIntModulus, bigIntPrivateExponent);
        KeyFactory keyFactory = KeyFactory.getInstance(RSA);
        PublicKey publicKey = keyFactory.generatePublic(keySpec);

        return publicKey;
    }

    public static PrivateKey getPrivateKey(String modulus, String privateExponent)throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        BigInteger bigIntModulus = new BigInteger(modulus);
        BigInteger bigIntPrivateExponent = new BigInteger(privateExponent);
        RSAPublicKeySpec keySpec = new RSAPublicKeySpec(bigIntModulus, bigIntPrivateExponent);
        KeyFactory keyFactory = KeyFactory.getInstance(RSA);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);

        return privateKey;
    }

    public static PublicKey loadPublicKey(String publicKeyStr) throws Exception
    {
        try
        {
            byte[] buffer = Base64Utils.decode(publicKeyStr);
            KeyFactory keyFactory = KeyFactory.getInstance(RSA);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            return (RSAPublicKey) keyFactory.generatePublic(keySpec);

//            byte[] buffer = Base64.decode(publicKeyStr,Base64.DEFAULT);
//            KeyFactory keyFactory = KeyFactory.getInstance(RSA);
//            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
//            return (RSAPublicKey) keyFactory.generatePublic(keySpec);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new Exception("無此解析算法");
        }
        catch (InvalidKeySpecException e)
        {
            throw new Exception("公鑰規則格式錯誤");
        }
        catch (NullPointerException e)
        {
            throw new Exception("公鑰數據為空");
        }
    }

    public static PrivateKey loadPrivateKey(String privateKeyStr) throws Exception
    {
        try
        {
            byte[] buffer = Base64Utils.decode(privateKeyStr);
//            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
            KeyFactory keyFactory = KeyFactory.getInstance(RSA);
            return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new Exception("無此解析算法");
        }
        catch (InvalidKeySpecException e)
        {
            throw new Exception("私鑰規則格式錯誤");
        }
        catch (NullPointerException e)
        {
            throw new Exception("私鑰數據為空");
        }
    }

    public static PublicKey loadPublicKey(InputStream in) throws Exception
    {
        try
        {
            return loadPublicKey(readPublicKey(in));
        }
        catch (IOException e)
        {
            throw new Exception("公鑰數據讀取錯誤");
        }
        catch (NullPointerException e)
        {
            throw new Exception("公鑰輸入為空");
        }
    }

    public static PrivateKey loadPrivateKey(InputStream in) throws Exception
    {
        try
        {
            return loadPrivateKey(readPrivateKey(in));
        }
        catch (IOException e)
        {
            throw new Exception("私鑰數據讀取錯誤");
        }
        catch (NullPointerException e)
        {
            throw new Exception("私鑰輸入為空");
        }
    }

    private static String readPrivateKey(InputStream in) throws IOException
    {
        InputStreamReader reader = new InputStreamReader(in);
        BufferedReader br = new BufferedReader(reader);
        StringBuffer sb = new StringBuffer();
        String line;

        while((line = br.readLine()) != null)
        {
            sb.append(line);
        }

        String mNew = sb.toString().replace("-----BEGIN PRIVATE KEY-----", "");
        mNew = mNew.replace("-----END PRIVATE KEY-----", "");

//        Log.d("123","Private sb :" + sb.toString());
//        Log.d("123","Private mNew :" + mNew);

        return mNew;
    }

    private static String readPublicKey(InputStream in) throws IOException
    {
        InputStreamReader reader = new InputStreamReader(in);
        BufferedReader br = new BufferedReader(reader);
        StringBuffer sb = new StringBuffer();
        String line;

        while((line = br.readLine()) != null)
        {
            sb.append(line);
        }

        String mNew = sb.toString().replace("-----BEGIN PUBLIC KEY-----", "");
        mNew = mNew.replace("-----END PUBLIC KEY-----", "");

//        Log.d("123","sb : " + sb.toString());
//        Log.d("123","mNew : " + mNew);

        return mNew;
    }

    public static void printPublicKeyInfo(PublicKey publicKey)
    {
        RSAPublicKey rsaPublicKey = (RSAPublicKey) publicKey;
        System.out.println("----------RSAPublicKey----------");
        System.out.println("Modulus.length=" + rsaPublicKey.getModulus().bitLength());
        System.out.println("Modulus=" + rsaPublicKey.getModulus().toString());
        System.out.println("PublicExponent.length=" + rsaPublicKey.getPublicExponent().bitLength());
        System.out.println("PublicExponent=" + rsaPublicKey.getPublicExponent().toString());
    }

    public static void printPrivateKeyInfo(PrivateKey privateKey)
    {
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;
        System.out.println("----------RSAPrivateKey ----------");
        System.out.println("Modulus.length=" + rsaPrivateKey.getModulus().bitLength());
        System.out.println("Modulus=" + rsaPrivateKey.getModulus().toString());
        System.out.println("PrivateExponent.length=" + rsaPrivateKey.getPrivateExponent().bitLength());
        System.out.println("PrivatecExponent=" + rsaPrivateKey.getPrivateExponent().toString());
    }

    public static byte[] encryptAES(byte[] key,byte[] text)
    {
        try
        {
            AlgorithmParameterSpec mAlgorithmParameterSpec = new IvParameterSpec("0000000000000000".getBytes("UTF-8"));
            SecretKeySpec mSecretKeySpec = new SecretKeySpec(key, "AES");
            Cipher mCipher = null;
            mCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            mCipher.init(Cipher.ENCRYPT_MODE,mSecretKeySpec,mAlgorithmParameterSpec);
            return mCipher.doFinal(text);
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public static byte[] decryptAES(byte[] key,byte[] text)
    {
        try
        {
            AlgorithmParameterSpec mAlgorithmParameterSpec = new IvParameterSpec("0000000000000000".getBytes("UTF-8"));
            SecretKeySpec mSecretKeySpec = new SecretKeySpec(key, "AES");
            Cipher mCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            mCipher.init(Cipher.DECRYPT_MODE,mSecretKeySpec,mAlgorithmParameterSpec);
            return mCipher.doFinal(text);
        }
        catch(Exception ex)
        {
            return null;
        }
    }
}