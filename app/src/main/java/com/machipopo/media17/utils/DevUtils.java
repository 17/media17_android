package com.machipopo.media17.utils;

import android.os.Build;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by POPO on 15/7/22.
 */
public class DevUtils {

    private static Map<String, String> BLOCK_DEVICES = new HashMap<>();

    public static boolean isAllowToFilter(){
        if(BLOCK_DEVICES.size() == 0){
            build();
        }

        String model = getModel().replaceAll("-","").replaceAll("_", "").replaceAll(" ", "").trim().toUpperCase();
        if(BLOCK_DEVICES.containsKey(model)){
            return false;
        }
        return true;
    }

    /**
     * Return SDK Version. ex : 21
     * @return ex : 21
     */
    public static int getAndroidSdkVersion(){
        return Build.VERSION.SDK_INT;
    }

    /**
     * Return device Info
     * @return
     */
    public static String getDevInfo(){
        String manufacturer = getManufacturer();
        if(manufacturer == null || manufacturer.equals("")){
            manufacturer = getBrand();
        }

        return manufacturer + "_" + getModel() + "_" + getAndroidVersion();
    }

    /**
     * ex: samsung
     * @return
     */
    private static String getBrand(){
        return android.os.Build.BRAND;
    }

    /**
     * ex: samsung
     * @return
     */
    private static String getManufacturer(){
        return android.os.Build.MANUFACTURER;
    }

    /**
     * ex: GT-I9300
     * @return
     */
    private static String getModel(){
        return android.os.Build.MODEL;
    }

    /**
     * ex: 5.0.1
     * @return ex: Android-5.0.1
     */
    private static String getAndroidVersion(){
        return "Android-" + Build.VERSION.RELEASE;
    }

    private static void build(){
        BLOCK_DEVICES.put("GTI9500", "samsung");
        BLOCK_DEVICES.put("SGHN075T", "samsung");
        BLOCK_DEVICES.put("SMN9005", "samsung");
        BLOCK_DEVICES.put("SMN900U", "samsung");
        BLOCK_DEVICES.put("HTCBUTTERFLY", "htc");
        BLOCK_DEVICES.put("HTCD816X", "htc");
        BLOCK_DEVICES.put("HTCD610X", "htc");
        BLOCK_DEVICES.put("D2533", "sony");
        BLOCK_DEVICES.put("C6902", "sony");
        BLOCK_DEVICES.put("C6903", "sony");
        BLOCK_DEVICES.put("MEITU2", "meitu");

    }

}
