package com.machipopo.media17.utils;

public interface FeedTagActionHandler {
    public void handleHashtag(String hashtag);
    public void handleMention(String mention);
    public void handleEmail(String email);
    public void handleUrl(String url);
}
