package com.machipopo.media17.utils;

import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by POPO on 5/20/15.
 */
public enum AESUtils
{
    ;
//    private static final String ENCRYPTION_IV = "0000000000000000";

    public static String encrypt(String AESkey, String src)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, makeKey(AESkey), makeIv());
            return AESBase.encodeBytes(cipher.doFinal(src.getBytes()));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static String decrypt(String AESkey, String src)
    {
        String decrypted = "";
        try
        {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, makeKey(AESkey), makeIv());
            decrypted = new String(cipher.doFinal(AESBase.decode(src)));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return decrypted;
    }

    static AlgorithmParameterSpec makeIv()
    {
        byte[] bytes = new byte[16];
        Arrays.fill(bytes, (byte) 0);
//        String c =  Arrays.toString(bytes);
//        Log.d("123", "c : " + c);

        return new IvParameterSpec(bytes);
    }

    static Key makeKey(String AESkey)
    {
        return new SecretKeySpec(AESkey.getBytes(), "AES");
    }
}