package com.machipopo.media17.utils;

import android.content.Context;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.machipopo.media17.Constants;
import com.machipopo.media17.Singleton;
import com.machipopo.media17.Story17Application;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dean on 16/1/11.
 */
public class LogEventUtil
{
    private static HashMap<String,String> mSDKHashMap;

    private static void addDefaultEvent(HashMap<String,String> mHashMap)
    {
        if(!mHashMap.containsKey("platform")) mHashMap.put("platform", "Android");
        if(!mHashMap.containsKey("version")) mHashMap.put("version", Singleton.getVersion());
        if(!mHashMap.containsKey("u")) mHashMap.put("u", Singleton.getPhoneIMEI());
        if(!mHashMap.containsKey("dn")) mHashMap.put("dn", DevUtils.getDevInfo());
        if(!mHashMap.containsKey("t")) mHashMap.put("t", String.valueOf(Singleton.getCurrentTimestamp_min()));
        if(!mHashMap.containsKey("ip")) mHashMap.put("ip", Singleton.preferences.getString(Constants.IP, ""));
        if(!mHashMap.containsKey("region")) mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
        if(!mHashMap.containsKey("longitude")) mHashMap.put("longitude", String.valueOf(Singleton.preferences.getFloat(Constants.LIVE_MY_LON, 0)));
        if(!mHashMap.containsKey("latitude")) mHashMap.put("latitude", String.valueOf(Singleton.preferences.getFloat(Constants.LIVE_MY_LAT, 0)));
        if(!mHashMap.containsKey("sessionID")) mHashMap.put("sessionID", Singleton.preferences.getString(Constants.EVENT_SESSION_ID, ""));
        if(!mHashMap.containsKey("app_open_times")) mHashMap.put("app_open_times", String.valueOf(Singleton.preferences.getInt(Constants.OPEN_APP_COUNT, 0)));
        if(!mHashMap.containsKey("isGuest")) mHashMap.put("isGuest","");

        if(!mHashMap.containsKey("from_event")) mHashMap.put("from_event",Singleton.preferences.getString(Constants.LOG_EVENT_NAME_FROM, ""));
        if(!mHashMap.containsKey("from_event_id")) mHashMap.put("from_event_id",Singleton.preferences.getString(Constants.LOG_EVENT_ID_FROM, ""));
        if(!mHashMap.containsKey("event")) mHashMap.put("event",Singleton.preferences.getString(Constants.LOG_EVENT_NAME, ""));
        if(!mHashMap.containsKey("event_id")) mHashMap.put("event_id",Singleton.preferences.getString(Constants.LOG_EVENT_ID, ""));
    }

    private static void sendServer(Context mCtx, Story17Application application, HashMap<String,String> hashMap)
    {
        JSONObject obj = new JSONObject(hashMap);
        application.mEventArray.put(obj);

//        Log.d("123","application.mEventArray : " + application.mEventArray.toString());

        if(application.mEventArray.length() > 9)
        {
            application.mEventArray = null;
            application.mEventArray = new JSONArray();
        }
    }

    private static void sendUmeng(Context mCtx, String eventName, HashMap<String,String> hashMap)
    {
        try{
            MobclickAgent.onEventValue(mCtx, eventName, hashMap, 0);
        }
        catch (Exception e){
        }
    }

    private static void sendFlurry(Context mCtx, String eventName, HashMap<String,String> hashMap)
    {
        try{
            FlurryAgent.logEvent(eventName, hashMap);
        }
        catch (Exception e){
        }
    }

    private static void sendSDKLog(Context mCtx, String eventName, HashMap<String,String> hashMap)
    {
        sendUmeng(mCtx, hashMap.get("event").toString(), hashMap);
        sendFlurry(mCtx, hashMap.get("event").toString(), hashMap);
    }

    public static void sendSDKLogEvent(Context mCtx, String eventName, HashMap<String,String> hashMap)
    {
        sendUmeng(mCtx, eventName, hashMap);
        sendFlurry(mCtx, eventName, hashMap);
    }

    private static void resetMap()
    {
        try{
            if(mSDKHashMap==null) mSDKHashMap = new HashMap<String,String>();
            else mSDKHashMap.clear();

            String user = "NULL";
            if(Singleton.preferences.getString(Constants.USER_ID, "").length()!=0) user = Singleton.preferences.getString(Constants.USER_ID, "");
            else user = "guest";

            mSDKHashMap.put("userID", user);
            addDefaultEvent(mSDKHashMap);
        }
        catch (Exception e){
        }
    }

    private static void resetName(String name, String id)
    {
        Singleton.preferenceEditor.putString(Constants.LOG_EVENT_ID_FROM, Singleton.preferences.getString(Constants.LOG_EVENT_ID, "")).commit();
        Singleton.preferenceEditor.putString(Constants.LOG_EVENT_NAME_FROM, Singleton.preferences.getString(Constants.LOG_EVENT_NAME, "")).commit();

        Singleton.preferenceEditor.putString(Constants.LOG_EVENT_ID, id).commit();
        Singleton.preferenceEditor.putString(Constants.LOG_EVENT_NAME, name).commit();
    }

    //### Session Related
    public static void OpenApp(Context mCtx, Story17Application application){
        Singleton.preferenceEditor.putString(Constants.EVENT_SESSION_ID, Singleton.getUUID()).commit();

        Singleton.preferenceEditor.putString(Constants.LOG_EVENT_ID_FROM, Singleton.getUUID()).commit();
        Singleton.preferenceEditor.putString(Constants.LOG_EVENT_NAME_FROM, "LaunchAPP").commit();

        Singleton.preferenceEditor.putString(Constants.LOG_EVENT_ID, Singleton.getUUID()).commit();
        Singleton.preferenceEditor.putString(Constants.LOG_EVENT_NAME, "OpenApp").commit();

        resetMap();
        application.mHashMap = mSDKHashMap;
        sendSDKLog(mCtx, "OpenApp", mSDKHashMap);
    }

    public static void CloseApp(Context mCtx, Story17Application application, int duration, int networkUnstableCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "CloseApp");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "CloseApp", application.mHashMap);

        resetName("CloseApp", id);
        resetMap();
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("networkUnstableCount", String.valueOf(networkUnstableCount));
        application.mHashMap = mSDKHashMap;
    }

    public static void NetWorkUnstable(Context mCtx)
    {

    }

    //### Share Related
    public static void ShareApp(Context mCtx, Story17Application application, String channel){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ShareApp");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ShareApp", application.mHashMap);

        resetName("ShareApp", id);
        resetMap();
        mSDKHashMap.put("channel", channel);
        application.mHashMap = mSDKHashMap;
    }

    public static void ShareLive(Context mCtx, Story17Application application, String targetUserID, String liveStreamID, String channel){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ShareLive");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ShareLive", application.mHashMap);

        resetName("ShareLive", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("liveStreamID", liveStreamID);
        mSDKHashMap.put("channel", channel);
        application.mHashMap = mSDKHashMap;
    }

    public static void SharePost(Context mCtx, Story17Application application, String targetUserID, String postID, String channel){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SharePost");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SharePost", application.mHashMap);

        resetName("SharePost", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("postID", postID);
        mSDKHashMap.put("channel", channel);
        application.mHashMap = mSDKHashMap;
    }

    public static void ShareUser(Context mCtx, Story17Application application, String targetUserID, String channel){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ShareUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ShareUser", application.mHashMap);

        resetName("ShareUser", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("channel", channel);
        application.mHashMap = mSDKHashMap;
    }

    //### Gift Related
    public static void SendGiftUser(Context mCtx, Story17Application application, String targetUserID, String giftName, int giftPoint, String giftID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SendGiftUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SendGiftUser", application.mHashMap);

        resetName("SendGiftUser", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("giftName", giftName);
        mSDKHashMap.put("giftPoint", String.valueOf(giftPoint));
        mSDKHashMap.put("giftID", giftID);
        application.mHashMap = mSDKHashMap;
    }

    public static void SendGiftLive(Context mCtx, Story17Application application, String targetUserID, String liveStreamID, String giftName, int giftPoint, String giftID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SendGiftLive");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SendGiftLive", application.mHashMap);

        resetName("SendGiftLive", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("liveStreamID", liveStreamID);
        mSDKHashMap.put("giftName", giftName);
        mSDKHashMap.put("giftPoint", String.valueOf(giftPoint));
        mSDKHashMap.put("giftID", giftID);
        application.mHashMap = mSDKHashMap;
    }

    public static void SendGiftPost(Context mCtx, Story17Application application, String targetUserID, String postID, String giftName, int giftPoint, String giftID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SendGiftLive");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SendGiftLive", application.mHashMap);

        resetName("SendGiftLive", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("postID", postID);
        mSDKHashMap.put("giftName", giftName);
        mSDKHashMap.put("giftPoint", String.valueOf(giftPoint));
        mSDKHashMap.put("giftID", giftID);
        application.mHashMap = mSDKHashMap;
    }

    public static void PurchasePoint(Context mCtx, Story17Application application, int point, int BuyTimes){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SendGiftLive");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SendGiftLive", application.mHashMap);

        resetName("SendGiftLive", id);
        resetMap();
        mSDKHashMap.put("point", String.valueOf(point));
        mSDKHashMap.put("BuyTimes", String.valueOf(BuyTimes));
        application.mHashMap = mSDKHashMap;
    }

    //** Register Flow **
    public static void StartRegister(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "StartRegister");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "StartRegister", application.mHashMap);

        resetName("StartRegister", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void SuccessfulRegister(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SuccessfulRegister");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SuccessfulRegister", application.mHashMap);

        resetName("SuccessfulRegister", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void QuitRegister(Context mCtx, Story17Application application, String lastStep){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "QuitRegister");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "QuitRegister", application.mHashMap);

        resetName("QuitRegister", id);
        resetMap();
        mSDKHashMap.put("lastStep", lastStep);
        application.mHashMap = mSDKHashMap;
    }

    public static void BindEmail(Context mCtx, Story17Application application, String email){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "BindEmail");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "BindEmail", application.mHashMap);

        resetName("BindEmail", id);
        resetMap();
        mSDKHashMap.put("email", email);
        application.mHashMap = mSDKHashMap;
    }

    public static void BindPhone(Context mCtx, Story17Application application, String phoneNumber){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "BindPhone");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "BindPhone", application.mHashMap);

        resetName("BindPhone", id);
        resetMap();
        mSDKHashMap.put("phoneNumber", phoneNumber);
        application.mHashMap = mSDKHashMap;
    }

    //** Feed Page **
    public static void EnterFeedPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterFeedPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterFeedPage", application.mHashMap);

        resetName("EnterFeedPage", id);
        resetMap();
        //TODO
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveFeedPage(Context mCtx, Story17Application application, int duration, int watchCount, int refreshCount, int gridModeCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveFeedPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveFeedPage", application.mHashMap);

        resetName("EnterFeedPage", id);
        resetMap();
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("watchCount", String.valueOf(watchCount));
        mSDKHashMap.put("refreshCount", String.valueOf(refreshCount));
        mSDKHashMap.put("gridModeCount", String.valueOf(gridModeCount));
        application.mHashMap = mSDKHashMap;
    }

    public static void ClickInFeedPage(Context mCtx, Story17Application application, String targetUserID, String type, String postID, String liveStreamID, int duration){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ClickInFeedPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ClickInFeedPage", application.mHashMap);

        resetName("EnterFeedPage", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("type", type);
        mSDKHashMap.put("postID", postID);
        mSDKHashMap.put("liveStreamID", liveStreamID);
        mSDKHashMap.put("duration", String.valueOf(duration));
        application.mHashMap = mSDKHashMap;
    }

    //** Discover Page **
    public static void EnterDiscoverPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterDiscoverPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterDiscoverPage", application.mHashMap);

        resetName("EnterDiscoverPage", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveDiscoverPage(Context mCtx, Story17Application application, int duration, int refreshCount, int cellCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveDiscoverPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveDiscoverPage", application.mHashMap);

        resetName("LeaveDiscoverPage", id);
        resetMap();
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("refreshCount", String.valueOf(refreshCount));
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));
        application.mHashMap = mSDKHashMap;
    }

    public static void EnterHotLiveTrendingPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterHotLiveTrendingPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterHotLiveTrendingPage", application.mHashMap);

        resetName("EnterHotLiveTrendingPage", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveHotLiveTrendingPage(Context mCtx, Story17Application application, int duration, int cellCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveHotLiveTrendingPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveHotLiveTrendingPage", application.mHashMap);

        resetName("LeaveHotLiveTrendingPage", id);
        resetMap();
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));
        application.mHashMap = mSDKHashMap;
    }

    public static void EnterHotLiveLatestPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterHotLiveLatestPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterHotLiveLatestPage", application.mHashMap);

        resetName("EnterHotLiveLatestPage", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveHotLiveLatestPage(Context mCtx, Story17Application application, int duration, int cellCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveHotLiveLatestPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveHotLiveLatestPage", application.mHashMap);

        resetName("LeaveHotLiveLatestPage", id);
        resetMap();
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));
        application.mHashMap = mSDKHashMap;
    }

    public static void EnterDiscoverUser(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterDiscoverUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterDiscoverUser", application.mHashMap);

        resetName("EnterDiscoverUser", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveDiscoverUser(Context mCtx, Story17Application application, int cellCount, int duration, int refreshCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveDiscoverUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveDiscoverUser", application.mHashMap);

        resetName("LeaveDiscoverUser", id);
        resetMap();
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("refreshCount", String.valueOf(refreshCount));
        application.mHashMap = mSDKHashMap;
    }

    public static void ClickDiscoverUser(Context mCtx, Story17Application application, String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ClickDiscoverUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ClickDiscoverUser", application.mHashMap);

        resetName("ClickDiscoverUser", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;
    }

    public static void FollowChoicedUser(Context mCtx, Story17Application application, String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "FollowChoicedUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "FollowChoicedUser", application.mHashMap);

        resetName("FollowChoicedUser", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;
    }

    public static void FollowDiscoverUser(Context mCtx, Story17Application application, String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "FollowDiscoverUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "FollowDiscoverUser", application.mHashMap);

        resetName("FollowDiscoverUser", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;
    }

    public static void ClickDiscoverPictures(Context mCtx, Story17Application application, int position, int number){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ClickDiscoverPictures");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ClickDiscoverPictures", application.mHashMap);

        resetName("ClickDiscoverPictures", id);
        resetMap();
        mSDKHashMap.put("position", String.valueOf(position));
        mSDKHashMap.put("number", String.valueOf(number));
        application.mHashMap = mSDKHashMap;
    }

    public static void FollowAllSuggestUser(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "FollowAllSuggestUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "FollowAllSuggestUser", application.mHashMap);

        resetName("FollowAllSuggestUser", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void FollowSuggestUser(Context mCtx, Story17Application application, String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "FollowSuggestUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "FollowSuggestUser", application.mHashMap);

        resetName("FollowSuggestUser", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;
    }

    //** Notification Page **
    public static void EnterFollowPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterFollowPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterFollowPage", application.mHashMap);

        resetName("EnterFollowPage", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveFollowPage(Context mCtx, Story17Application application, int duration, int cellCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveFollowPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveFollowPage", application.mHashMap);

        resetName("LeaveFollowPage", id);
        resetMap();
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));
        application.mHashMap = mSDKHashMap;
    }

    public static void EnterSelfFollowPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterSelfFollowPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterSelfFollowPage", application.mHashMap);

        resetName("EnterSelfFollowPage", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    //[FIX-ME] followbackcount is not implement yet
    public static void LeaveSelfFollowPage(Context mCtx, Story17Application application, int duration, int followBackCount, int cellCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveSelfFollowPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveSelfFollowPage", application.mHashMap);

        resetName("LeaveSelfFollowPage", id);
        resetMap();
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("followBackCount", String.valueOf(followBackCount));
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));
        application.mHashMap = mSDKHashMap;
    }

    public static void EnterSysPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterSysPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterSysPage", application.mHashMap);

        resetName("EnterSysPage", id);
        resetMap();
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveSysPage(Context mCtx, Story17Application application, int duration, int count){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveSysPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveSysPage", application.mHashMap);

        resetName("LeaveSysPage", id);
        resetMap();
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("count", String.valueOf(count));
        application.mHashMap = mSDKHashMap;
    }

    //** Post Page **
    public static void EnterPostPage(Context mCtx, Story17Application application, String targetUserID, String postID, String from){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterPostPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterPostPage", application.mHashMap);

        resetName("EnterPostPage", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("postID", postID);
        mSDKHashMap.put("from", from);
        application.mHashMap = mSDKHashMap;
    }

    public static void LeavePostPage(Context mCtx, Story17Application application, String targetUserID, int duration, String postID, int likeCount, String from){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeavePostPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeavePostPage", application.mHashMap);

        resetName("LeavePostPage", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("postID", postID);
        mSDKHashMap.put("likeCount", String.valueOf(likeCount));
        mSDKHashMap.put("from", from);
        application.mHashMap = mSDKHashMap;
    }

    public static void CommentPost(Context mCtx, Story17Application application, String targetUserID, String content){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "CommentPost");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "CommentPost", application.mHashMap);

        resetName("CommentPost", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("content", content);
        application.mHashMap = mSDKHashMap;
    }

    public static void LikePost(Context mCtx, Story17Application application, String targetUserID, String postID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LikePost");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LikePost", application.mHashMap);

        resetName("LikePost", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("postID", postID);
        application.mHashMap = mSDKHashMap;
    }

    public static void LikePostSinglePost(Context mCtx, Story17Application application, String targetUserID, String postID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LikePostSinglePost");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LikePostSinglePost", application.mHashMap);

        resetName("LikePostSinglePost", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("postID", postID);
        application.mHashMap = mSDKHashMap;
    }

    //** Live Page **
    public static void EnterLivePage(Context mCtx, Story17Application application,String targetUserID, String liveStreamID, String from, String caption){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterLivePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterLivePage", application.mHashMap);

        resetName("EnterLivePage", id);
        resetMap();

        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("liveStreamID", liveStreamID);
        mSDKHashMap.put("from", from);
        mSDKHashMap.put("caption",caption);
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveLivePage(Context mCtx, Story17Application application, String targetUserID, String liveStreamID, int duration, int fullScreenCount, int commentCount, int likeCount, int cosumePoint,String from){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterLivePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterLivePage", application.mHashMap);

        resetName("EnterLivePage", id);
        resetMap();

        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("liveStreamID",liveStreamID);
        mSDKHashMap.put("duration",String.valueOf(duration));
        mSDKHashMap.put("fullScreenCount",String.valueOf(fullScreenCount));
        mSDKHashMap.put("commentCount",String.valueOf(commentCount));
        mSDKHashMap.put("likeCount",String.valueOf(likeCount));
        mSDKHashMap.put("cosumePoint",String.valueOf(cosumePoint));
        mSDKHashMap.put("from",from);
        application.mHashMap = mSDKHashMap;

    }

    public static void EnterFullScreen(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterFullScreen");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterFullScreen", application.mHashMap);

        resetName("EnterFullScreen", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveFullScreen(Context mCtx, Story17Application application, int duration){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveFullScreen");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveFullScreen", application.mHashMap);

        resetName("LeaveFullScreen", id);
        resetMap();

        mSDKHashMap.put("duration",String.valueOf(duration));
        application.mHashMap = mSDKHashMap;
    }

    public static void CommentInLiveStream(Context mCtx, Story17Application application,String targetUserID, String liveStreamID, String comment){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "CommentInLiveStream");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "CommentInLiveStream", application.mHashMap);

        resetName("CommentInLiveStream", id);
        resetMap();

        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("liveStreamID", liveStreamID);
        mSDKHashMap.put("comment",comment);
        application.mHashMap = mSDKHashMap;
    }

    public static void LikeLive(Context mCtx, Story17Application application, String targetUserID, String liveStreamID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LikeLive");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LikeLive", application.mHashMap);

        resetName("LikeLive", id);
        resetMap();

        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("liveStreamID", liveStreamID);
        application.mHashMap = mSDKHashMap;
    }

    public static void ClickSendGiftButton(Context mCtx, Story17Application application, int hasPoint){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ClickSendGiftButton");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ClickSendGiftButton", application.mHashMap);

        resetName("ClickSendGiftButton", id);
        resetMap();

        mSDKHashMap.put("hasPoint",String.valueOf(hasPoint));
        application.mHashMap = mSDKHashMap;
    }

    public static void EnterBuyPointInLive(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterBuyPointInLive");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterBuyPointInLive", application.mHashMap);

        resetName("EnterBuyPointInLive", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void ClickBuyPointInLive(Context mCtx, Story17Application application,int point){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterLivePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterLivePage", application.mHashMap);

        resetName("EnterLivePage", id);
        resetMap();

        mSDKHashMap.put("point", String.valueOf(point));
        application.mHashMap = mSDKHashMap;
    }

    public static void GiveupBuyPointInLive(Context mCtx, Story17Application application,int point){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterLivePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterLivePage", application.mHashMap);

        resetName("EnterLivePage", id);
        resetMap();

        mSDKHashMap.put("point",String.valueOf(point));
        application.mHashMap = mSDKHashMap;
    }

    public static void DidBuyPointInLive(Context mCtx, Story17Application application, int point){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "DidBuyPointInLive");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "DidBuyPointInLive", application.mHashMap);

        resetName("DidBuyPointInLive", id);
        resetMap();

        mSDKHashMap.put("point",String.valueOf(point));
        application.mHashMap = mSDKHashMap;
    }

    //** Publish Page **
    public static void EnterPublishPostPage(Context mCtx, Story17Application application, String type){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterPublishPostPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterPublishPostPage", application.mHashMap);

        resetName("EnterPublishPostPage", id);
        resetMap();

        mSDKHashMap.put("type",type);
        application.mHashMap = mSDKHashMap;

    }

    public static void EnterFilterPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterFilterPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterFilterPage", application.mHashMap);

        resetName("EnterFilterPage", id);
        resetMap();

        application.mHashMap = mSDKHashMap;

    }

    public static void LeaveFilterPage(Context mCtx, Story17Application application, int duration, boolean anyChange){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveFilterPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveFilterPage", application.mHashMap);

        resetName("LeaveFilterPage", id);
        resetMap();

        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("anyChange",String.valueOf(anyChange));
        application.mHashMap = mSDKHashMap;

    }

    public static void EnterTextTypePage(Context mCtx, Story17Application application,String type){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterTextTypePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterTextTypePage", application.mHashMap);

        resetName("EnterTextTypePage", id);
        resetMap();

        mSDKHashMap.put("type",type);
        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveTextTypePage(Context mCtx, Story17Application application,String type, boolean post, String caption, int duration){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveTextTypePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveTextTypePage", application.mHashMap);

        resetName("LeaveTextTypePage", id);
        resetMap();

        mSDKHashMap.put("type", type);
        mSDKHashMap.put("post", String.valueOf(post));
        mSDKHashMap.put("caption",caption);
        mSDKHashMap.put("duration",String.valueOf(duration));
        application.mHashMap = mSDKHashMap;
    }

    public static void EnterPublishLivePage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterPublishLivePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterPublishLivePage", application.mHashMap);

        resetName("EnterPublishLivePage", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void StartLiveStreaming(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "StartLiveStreaming");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "StartLiveStreaming", application.mHashMap);

        resetName("StartLiveStreaming", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void EndLiveStreaming(Context mCtx, Story17Application application, String liveStreamID, int viewerCount,int duration){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EndLiveStreaming");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EndLiveStreaming", application.mHashMap);

        resetName("EndLiveStreaming", id);
        resetMap();

        mSDKHashMap.put("liveStreamID", liveStreamID);
        mSDKHashMap.put("viewerCount",String.valueOf(viewerCount));
        mSDKHashMap.put("duration",String.valueOf(duration));
        application.mHashMap = mSDKHashMap;
    }

    public static void PublishPost(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "PublishPost");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "PublishPost", application.mHashMap);

        resetName("PublishPost", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void LeavePublishPageWithoutPub(Context mCtx, Story17Application application, int duration, String type){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeavePublishPageWithoutPub");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeavePublishPageWithoutPub", application.mHashMap);

        resetName("LeavePublishPageWithoutPub", id);
        resetMap();

        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("type",type);
        application.mHashMap = mSDKHashMap;
    }

    //** Self Profile Page **
    public static void EnterSelfPage(Context mCtx, Story17Application application){

        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterSelfPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterSelfPage", application.mHashMap);

        resetName("EnterSelfPage", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveSelfPage(Context mCtx, Story17Application application, int duration){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveSelfPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveSelfPage", application.mHashMap);

        resetName("LeaveSelfPage", id);
        resetMap();

        mSDKHashMap.put("duration", String.valueOf(duration));
        application.mHashMap = mSDKHashMap;

    }

    //** ProfilePage **
    public static void EnterProfilePage(Context mCtx, Story17Application application,String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterProfilePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterProfilePage", application.mHashMap);

        resetName("EnterProfilePage", id);
        resetMap();
        mSDKHashMap.put("userID", targetUserID);
        application.mHashMap = mSDKHashMap;

    }

    public static void LeaveProfilePage(Context mCtx, Story17Application application,String targetUserID,Boolean followAction, int gridCount, int favorCount, int listCount ){

        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveProfilePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveProfilePage", application.mHashMap);

        resetName("LeaveProfilePage", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        mSDKHashMap.put("followAction", String.valueOf(followAction));
        mSDKHashMap.put("gridCount", String.valueOf(gridCount));
        mSDKHashMap.put("favorCount", String.valueOf(favorCount));
        mSDKHashMap.put("listCount", String.valueOf(listCount));
        application.mHashMap = mSDKHashMap;


    }

    public static void EnterRevenuePage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterRevenuePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterRevenuePage", application.mHashMap);

        resetName("EnterRevenuePage", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveRevenuePage(Context mCtx, Story17Application application, int duration) {
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveRevenuePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveRevenuePage", application.mHashMap);

        resetName("LeaveRevenuePage", id);
        resetMap();

        mSDKHashMap.put("duration", String.valueOf(duration));
        application.mHashMap = mSDKHashMap;
    }

    public static void EnterReceivedLikePage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveRevenuePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveRevenuePage", application.mHashMap);

        resetName("LeaveRevenuePage", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveReceivedLikePage(Context mCtx,Story17Application application, int duration, int cellCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveReceivedLikePage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveReceivedLikePage", application.mHashMap);

        resetName("LeaveReceivedLikePage", id);
        resetMap();

        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));

        application.mHashMap = mSDKHashMap;
    }

    public static void EnterProfileLikerPage(Context mCtx,Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterProfileLikerPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterProfileLikerPage", application.mHashMap);

        resetName("EnterProfileLikerPage", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void EnterFansPage(Context mCtx,Story17Application application, int duration){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterFansPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterFansPage", application.mHashMap);

        resetName("EnterFansPage", id);
        resetMap();

        mSDKHashMap.put("duration", String.valueOf(duration));

        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveFansPage(Context mCtx, Story17Application application, int duration, int cellCount){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveFansPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveFansPage", application.mHashMap);

        resetName("LeaveFansPage", id);
        resetMap();

        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));

        application.mHashMap = mSDKHashMap;

    }

    public static void EnterFollowingPage(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterFollowingPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterFollowingPage", application.mHashMap);

        resetName("EnterFollowingPage", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    public static void LeaveFollowingPage(Context mCtx, Story17Application application, int duration, int followerCount, int cellCount ){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveFollowingPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveFollowingPage", application.mHashMap);

        resetName("LeaveFollowingPage", id);
        resetMap();

        mSDKHashMap.put("duration", String.valueOf(duration));
        mSDKHashMap.put("followerCount", String.valueOf(followerCount));
        mSDKHashMap.put("cellCount", String.valueOf(cellCount));

        application.mHashMap = mSDKHashMap;
    }

    //** Find Friend Page **
    public static void EnterFindFriendPage(Context mCtx, Story17Application application,String userID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "EnterFindFriendPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "EnterFindFriendPage", application.mHashMap);

        resetName("EnterFindFriendPage", id);
        resetMap();
        mSDKHashMap.put("userID", userID);
        application.mHashMap = mSDKHashMap;

    }

    public static void LeaveFindFriendPage(Context mCtx, Story17Application application,String userID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "LeaveFindFriendPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "LeaveFindFriendPage", application.mHashMap);

        resetName("LeaveFindFriendPage", id);
        resetMap();
        mSDKHashMap.put("userID", userID);
        application.mHashMap = mSDKHashMap;

    }

    public static void SearchUserAndClick_FriendPage(Context mCtx, Story17Application application,String userID,String query,String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SearchUserAndClick_FriendPage");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SearchUserAndClick_FriendPage", application.mHashMap);

        resetName("SearchUserAndClick_FriendPage", id);
        resetMap();
        mSDKHashMap.put("userID", userID);
        mSDKHashMap.put("query", query);
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;

    }

    public static void SearchContactUserAndClick(Context mCtx, Story17Application application,String userID,String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SearchContactUserAndClick");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SearchContactUserAndClick", application.mHashMap);

        resetName("SearchContactUserAndClick", id);
        resetMap();
        mSDKHashMap.put("userID", userID);
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;


    }

    public static void SearchFBUserAndClick(Context mCtx, Story17Application application,String userID,String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SearchContactUserAndClick");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SearchContactUserAndClick", application.mHashMap);

        resetName("SearchContactUserAndClick", id);
        resetMap();
        mSDKHashMap.put("userID", userID);
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;

    }

    public static void FollowContactFriend(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SearchContactUserAndClick");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SearchContactUserAndClick", application.mHashMap);

        resetName("SearchContactUserAndClick", id);
        resetMap();
        application.mHashMap = mSDKHashMap;

    }

    public static void FollowFBFriend(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "FollowFBFriend");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "FollowFBFriend", application.mHashMap);

        resetName("FollowFBFriend", id);
        resetMap();

        application.mHashMap = mSDKHashMap;
    }

    //** Search User Page ** //depreciate;
    public static void SearchUserAndClick(Context mCtx, Story17Application application,String query,String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SearchUserAndClick");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SearchUserAndClick", application.mHashMap);

        resetName("SearchUserAndClick", id);
        resetMap();
        mSDKHashMap.put("query", query);
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;
    }

    public static void SearchUserAndFollow(Context mCtx, Story17Application application,String query,String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SearchUserAndFollow");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SearchUserAndFollow", application.mHashMap);

        resetName("SearchUserAndFollow", id);
        resetMap();
        mSDKHashMap.put("query", query);
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;

    }

    public static void SearchHashTagAndClick(Context mCtx, Story17Application application,String query,String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SearchHashTagAndClick");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SearchHashTagAndClick", application.mHashMap);

        resetName("SearchHashTagAndClick", id);
        resetMap();
        mSDKHashMap.put("query", query);
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;

    }

    public static void SearchPageDuration(Context mCtx, Story17Application application,String userID,int duration){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "SearchPageDuration");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "SearchPageDuration", application.mHashMap);

        resetName("SearchPageDuration", id);
        resetMap();
        mSDKHashMap.put("userID", userID);
        mSDKHashMap.put("duration", String.valueOf(duration));
        application.mHashMap = mSDKHashMap;

    }

    //** Follow **
    public static void FollowUser(Context mCtx, Story17Application application,String targetUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "FollowUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "FollowUser", application.mHashMap);

        resetName("FollowUser", id);
        resetMap();
        mSDKHashMap.put("targetUserID", targetUserID);
        application.mHashMap = mSDKHashMap;
    }

    public static void UnfollowUser(Context mCtx, Story17Application application,String followedUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "UnfollowUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "UnfollowUser", application.mHashMap);

        resetName("UnfollowUser", id);
        resetMap();
        mSDKHashMap.put("followedUserID", followedUserID);
        application.mHashMap = mSDKHashMap;
    }

    //** Report Related **
    public static void ReportLive(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ReportLive");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ReportLive", application.mHashMap);

        resetName("ReportLive", id);
        resetMap();
        //TODO
        application.mHashMap = mSDKHashMap;

    }

    public static void ReportPost(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ReportPost");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ReportPost", application.mHashMap);

        resetName("ReportPost", id);
        resetMap();
        //TODO
        application.mHashMap = mSDKHashMap;
    }

    public static void ReportUser(Context mCtx, Story17Application application){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "ReportUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "ReportUser", application.mHashMap);

        resetName("ReportUser", id);
        resetMap();
        //TODO
        application.mHashMap = mSDKHashMap;
    }

    public static void BlockUser(Context mCtx, Story17Application application,String blockedUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "BlockUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "BlockUser", application.mHashMap);

        resetName("BlockUser", id);
        resetMap();
        mSDKHashMap.put("blockedUserID", blockedUserID);
        application.mHashMap = mSDKHashMap;
    }

    public static void UnblockUser(Context mCtx, Story17Application application,String blockedUserID){
        String id = Singleton.getUUID();
        application.mHashMap.put("to_event", "UnblockUser");
        application.mHashMap.put("to_event_id", id);

        sendServer(mCtx, application, application.mHashMap);
        sendSDKLog(mCtx, "UnblockUser", application.mHashMap);

        resetName("UnblockUser", id);
        resetMap();
        mSDKHashMap.put("blockedUserID", blockedUserID);
        application.mHashMap = mSDKHashMap;
    }
}
