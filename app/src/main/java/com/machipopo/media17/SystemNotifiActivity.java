package com.machipopo.media17;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.model.SystemNotifModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

/**
 * Created by POPO on 2015/11/4.
 */
public class SystemNotifiActivity extends BaseNewActivity {
    private SystemNotifiActivity mCtx = this;
    private LayoutInflater inflater;

    private PullToRefreshListView mListViewSystem;
    private SystemAdapter mListAdapterSystem;
    private ProgressBar mProgressSystem;
    private ImageView mNoDataSystem;
    private ArrayList<SystemNotifModel> mNotifiModelSystem = new ArrayList<SystemNotifModel>();
    private Story17Application mApplication;
    private int startSysPage = 0;

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_liker_activity);

        mApplication = (Story17Application) mCtx.getApplication();

        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        } catch (Exception e) {
        }

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        //event tracking
        try {
            LogEventUtil.EnterSysPage(mCtx, mApplication);
        } catch (Exception X) {

        }

        mListViewSystem = (PullToRefreshListView) findViewById(R.id.list);
        mProgressSystem = (ProgressBar) findViewById(R.id.progress);
        mNoDataSystem = (ImageView) findViewById(R.id.nodata);

        mListViewSystem.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                ApiManager.getSystemNotif(mCtx, Integer.MAX_VALUE, 100, new ApiManager.GetSystemNotificationCallback() {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<SystemNotifModel> notifiModelSystem) {
                        mListViewSystem.onRefreshComplete();

                        if (success && notifiModelSystem != null) {
                            if (notifiModelSystem.size() != 0) {
                                mNoDataSystem.setVisibility(View.GONE);
                                mNotifiModelSystem.clear();
                                mNotifiModelSystem.addAll(notifiModelSystem);
                                mListAdapterSystem = new SystemAdapter();
                                mListViewSystem.setAdapter(mListAdapterSystem);
                            } else {
                                mNoDataSystem.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
            }
        });

        mProgressSystem.setVisibility(View.VISIBLE);
        ApiManager.getSystemNotif(mCtx, Integer.MAX_VALUE, 100, new ApiManager.GetSystemNotificationCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<SystemNotifModel> notifiModelSystem) {
                mProgressSystem.setVisibility(View.GONE);

                if (success && notifiModelSystem != null) {
                    if (notifiModelSystem.size() != 0) {
                        mNoDataSystem.setVisibility(View.GONE);
                        mNotifiModelSystem.clear();
                        mNotifiModelSystem.addAll(notifiModelSystem);
                        mListAdapterSystem = new SystemAdapter();
                        mListViewSystem.setAdapter(mListAdapterSystem);
                    } else {
                        mNoDataSystem.setVisibility(View.VISIBLE);
                    }
                } else {
                    try {
                        mNoDataSystem.setVisibility(View.VISIBLE);
                        try {
//                          showToast(getString(R.string.failed));
                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                        } catch (Exception x) {
                        }
                    } catch (Exception e) {

                    }
                }
            }
        });

        //test
    }

    private void initTitleBar() {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.setting_notifi_system));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class SystemAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return mNotifiModelSystem.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolderSystem holder = new ViewHolderSystem();

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.notifi_system_row, null);
                holder.message = (TextView) convertView.findViewById(R.id.message);
                holder.date = (TextView) convertView.findViewById(R.id.date);

                convertView.setTag(holder);
            } else holder = (ViewHolderSystem) convertView.getTag();

            holder.message.setText(mNotifiModelSystem.get(position).getMessage());
            holder.date.setText(Singleton.getElapsedTimeString(mNotifiModelSystem.get(position).getTimestamp()));

            return convertView;
        }
    }

    private class ViewHolderSystem {
        TextView message;
        TextView date;
    }

    @Override
    public void onDestroy()
    {
        //event tracking
        try{
            LogEventUtil.LeaveSysPage(mCtx,mApplication,Singleton.getCurrentTimestamp()-startSysPage,mNotifiModelSystem.size());
        }catch (Exception x)
        {

        }

        super.onDestroy();
    }
}
