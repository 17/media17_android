package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.fragment.BaseFragment;
import com.machipopo.media17.model.NotifiModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by POPO on 6/16/15.
 */
public class NotifiSelfFragment extends BaseFragment
{
    private LayoutInflater inflater;

    private PullToRefreshListView mListView;
    private listAdapter mListAdapter;

    private ProgressBar mProgress;
    private ImageView mNoData;

    private Boolean isFetchingData = false;
    private Boolean noMoreData = false;

    private DisplayImageOptions SelfOptions;
    private Story17Application mApplication;

    public ArrayList<NotifiModel> mNotifiModel = new ArrayList<NotifiModel>();


    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.notifi_self, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mNoData = (ImageView) getView().findViewById(R.id.nodata);
        mProgress = (ProgressBar) getView().findViewById(R.id.progress);
        mListView = (PullToRefreshListView) getView().findViewById(R.id.list);

        mApplication = (Story17Application) getActivity().getApplication();

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
        {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView)
            {
                ApiManager.getNotif(getActivity(), Integer.MAX_VALUE, 15, new ApiManager.GetNotifCallback()
                {
                    @Override
                    public void onResult(boolean success, String message,ArrayList<NotifiModel> notifiModel)
                    {
                        mListView.onRefreshComplete();

                        if (success && notifiModel != null)
                        {
                            if(notifiModel.size()!=0)
                            {
                                mNoData.setVisibility(View.GONE);
                                mNotifiModel.clear();
                                mNotifiModel.addAll(notifiModel);

                                if(mListAdapter!=null) mListAdapter = null;
                                mListAdapter = new listAdapter();
                                mListView.setAdapter(mListAdapter);

                                isFetchingData = false;
                                noMoreData = false;

                                if (mNotifiModel.size() < 15) {
                                    noMoreData = true;
                                }
                            }
                            else
                            {
                                mNoData.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            mNoData.setVisibility(View.VISIBLE);
                            try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                        }
                    }
                });
            }
        });

        mListView.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {
                if (scrollState == SCROLL_STATE_FLING) {
                    ImageLoader.getInstance().pause();
                }

                if (scrollState == SCROLL_STATE_IDLE) {
                    ImageLoader.getInstance().resume();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        mProgress.setVisibility(View.VISIBLE);
        ApiManager.getNotif(getActivity(), Integer.MAX_VALUE, 15, new ApiManager.GetNotifCallback()
        {
            @Override
            public void onResult(boolean success, String message,ArrayList<NotifiModel> notifiModel)
            {
                mProgress.setVisibility(View.GONE);

                if (success && notifiModel != null)
                {
                    if(notifiModel.size()!=0)
                    {
                        mNoData.setVisibility(View.GONE);
                        mNotifiModel.clear();
                        mNotifiModel.addAll(notifiModel);
                        mListAdapter = new listAdapter();
                        mListView.setAdapter(mListAdapter);

                        if (mNotifiModel.size() < 15) {
                            noMoreData = true;
                        }
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    try
                    {
                        mNoData.setVisibility(View.VISIBLE);
                        try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                            Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                    catch(Exception e)
                    {

                    }
                }
            }
        });

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private class listAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mNotifiModel.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.notifi_row, null);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
//                holder.name = (TextView) convertView.findViewById(R.id.name);
//                holder.type = (TextView) convertView.findViewById(R.id.type);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.follow = (ImageView) convertView.findViewById(R.id.follow);
//                holder.doing = (TextView) convertView.findViewById(R.id.doing);

                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModel.get(position).getUser().getPicture()), holder.pic, SelfOptions);
            holder.pic.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    if (mNotifiModel.get(position).getUser().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeUserActivity.class);
                        intent.putExtra("title", mNotifiModel.get(position).getUser().getName());
                        intent.putExtra("picture", mNotifiModel.get(position).getUser().getPicture());
                        intent.putExtra("isfollowing", mNotifiModel.get(position).getUser().getIsFollowing());
                        intent.putExtra("post", mNotifiModel.get(position).getUser().getPostCount());
                        intent.putExtra("follow", mNotifiModel.get(position).getUser().getFollowerCount());
                        intent.putExtra("following", mNotifiModel.get(position).getUser().getFollowingCount());
                        intent.putExtra("open", mNotifiModel.get(position).getUser().getOpenID());
                        intent.putExtra("bio", mNotifiModel.get(position).getUser().getBio());
                        intent.putExtra("targetUserID", mNotifiModel.get(position).getUser().getUserID());
                        intent.putExtra("web", mNotifiModel.get(position).getUser().getWebsite());
                        startActivity(intent);
                    }
                }
            });
//            holder.name.setText("@" + mNotifiModel.get(position).getUser().getOpenID());
//            holder.name.setOnClickListener(new View.OnClickListener()
//            {
//                @Override
//                public void onClick(View v)
//                {
//                    if(mNotifiModel.get(position).getUser().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
//                    {
//                        Intent intent = new Intent();
//                        intent.setClass(getActivity(), HomeUserActivity.class);
//                        intent.putExtra("title", mNotifiModel.get(position).getUser().getName());
//                        intent.putExtra("picture", mNotifiModel.get(position).getUser().getPicture());
//                        intent.putExtra("isfollowing", mNotifiModel.get(position).getUser().getIsFollowing());
//                        intent.putExtra("post", mNotifiModel.get(position).getUser().getPostCount());
//                        intent.putExtra("follow", mNotifiModel.get(position).getUser().getFollowerCount());
//                        intent.putExtra("following", mNotifiModel.get(position).getUser().getFollowingCount());
//                        intent.putExtra("open", mNotifiModel.get(position).getUser().getOpenID());
//                        intent.putExtra("bio", mNotifiModel.get(position).getUser().getBio());
//                        intent.putExtra("targetUserID", mNotifiModel.get(position).getUser().getUserID());
//                        intent.putExtra("web", mNotifiModel.get(position).getUser().getWebsite());
//                        startActivity(intent);
//                    }
//                }
//            });
            holder.day.setText(Singleton.getElapsedTimeString(mNotifiModel.get(position).getTimestamp()));
            holder.img.setVisibility(View.GONE);
            holder.follow.setVisibility(View.GONE);

            if(mNotifiModel.get(position).getType().compareTo("follow")==0)
            {
//                holder.type.setText("開始追蹤你");
            }
            else if(mNotifiModel.get(position).getType().compareTo("postLike")==0)
            {
//                holder.type.setText("說你的相片讚");

                if(mNotifiModel.get(position).getPost()!=null)
                {
                    holder.img.setVisibility(View.VISIBLE);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModel.get(position).getPost().getPicture()), holder.img, SelfOptions);
                    holder.img.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), PhotoActivity.class);
                            intent.putExtra("photo", mNotifiModel.get(position).getPost().getPicture());
                            intent.putExtra("open", mNotifiModel.get(position).getPost().getUser().getOpenID());
                            intent.putExtra("picture", mNotifiModel.get(position).getPost().getUser().getPicture());
                            intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModel.get(position).getPost().getTimestamp()));
                            intent.putExtra("view", mNotifiModel.get(position).getPost().getViewCount());
                            intent.putExtra("money", mNotifiModel.get(position).getPost().getTotalRevenue());
                            intent.putExtra("dio", mNotifiModel.get(position).getPost().getCaption());
                            intent.putExtra("likecount", mNotifiModel.get(position).getPost().getLikeCount());
                            intent.putExtra("commentcount", mNotifiModel.get(position).getPost().getCommentCount());
                            intent.putExtra("likeed", mNotifiModel.get(position).getPost().getIsLiked());

                            intent.putExtra("postid", mNotifiModel.get(position).getPostID());
                            intent.putExtra("userid", mNotifiModel.get(position).getPost().getUser().getUserID());

                            intent.putExtra("name", mNotifiModel.get(position).getPost().getUser().getName());
                            intent.putExtra("isFollowing", mNotifiModel.get(position).getPost().getUser().getIsFollowing());
                            intent.putExtra("postCount", mNotifiModel.get(position).getPost().getUser().getPostCount());
                            intent.putExtra("followerCount", mNotifiModel.get(position).getPost().getUser().getFollowerCount());
                            intent.putExtra("followingCount", mNotifiModel.get(position).getPost().getUser().getFollowingCount());
                            intent.putExtra("goto", false);

                            startActivity(intent);
                        }
                    });
                }
            }
            else if(mNotifiModel.get(position).getType().compareTo("newContactsFriendJoin")==0)
            {
//                holder.doing.setVisibility(View.VISIBLE);
//                holder.doing.setText("你的通訊錄好友 以名稱");
//                holder.type.setText("加入了17");

                holder.follow.setVisibility(View.VISIBLE);
                if(mNotifiModel.get(position).getUser().getIsFollowing()==0)
                {
                    holder.follow.setImageResource(R.drawable.follow);

                    final ImageView img = holder.follow;

                    holder.follow.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            img.setImageResource(R.drawable.follow_down);
                            mNotifiModel.get(position).getUser().setIsFollowing(1);

//                            ApiManager.followUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModel.get(position).getUser().getUserID(), new ApiManager.FollowUserActionCallback() {
//                                @Override
//                                public void onResult(boolean success, String message) {
//                                    if (success) {
//
//                                    }
//                                }
//                            });
                        }
                    });
                }
                else
                {
                    holder.follow.setImageResource(R.drawable.follow_down);

                    final ImageView img = holder.follow;

                    holder.follow.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            img.setImageResource(R.drawable.follow);
                            mNotifiModel.get(position).getUser().setIsFollowing(0);
                            try {
                                LogEventUtil.UnfollowUser(getActivity(), mApplication, mNotifiModel.get(position).getUser().getUserID());
                            }
                            catch (Exception x)
                            {

                            }

                            ApiManager.unfollowUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModel.get(position).getUser().getUserID(), new ApiManager.UnfollowUserActionCallback() {
                                @Override
                                public void onResult(boolean success, String message) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                    });
                }
            }
            else if(mNotifiModel.get(position).getType().compareTo("newFBFriendJoin")==0)
            {
//                holder.doing.setVisibility(View.VISIBLE);
//                holder.doing.setText("你的Facebook好友");
//                holder.type.setText("加入了17");

                holder.follow.setVisibility(View.VISIBLE);
                if(mNotifiModel.get(position).getUser().getIsFollowing()==0)
                {
                    holder.follow.setImageResource(R.drawable.follow);

                    final ImageView img = holder.follow;

                    holder.follow.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            img.setImageResource(R.drawable.follow_down);
                            mNotifiModel.get(position).getUser().setIsFollowing(1);

//                            ApiManager.followUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModel.get(position).getUser().getUserID(), new ApiManager.FollowUserActionCallback() {
//                                @Override
//                                public void onResult(boolean success, String message) {
//                                    if (success) {
//
//                                    }
//                                }
//                            });
                        }
                    });
                }
                else
                {
                    holder.follow.setImageResource(R.drawable.follow_down);

                    final ImageView img = holder.follow;

                    holder.follow.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            img.setImageResource(R.drawable.follow);
                            mNotifiModel.get(position).getUser().setIsFollowing(0);

                            try {
                                LogEventUtil.UnfollowUser(getActivity(), mApplication, mNotifiModel.get(position).getUser().getUserID());
                            }
                            catch (Exception x)
                            {

                            }

                            ApiManager.unfollowUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModel.get(position).getUser().getUserID(), new ApiManager.UnfollowUserActionCallback() {
                                @Override
                                public void onResult(boolean success, String message) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                    });
                }
            }
            else if(mNotifiModel.get(position).getType().compareTo("postComment")==0)
            {
//                holder.type.setText("回應了一篇相片");

                if(mNotifiModel.get(position).getPost()!=null)
                {
                    holder.img.setVisibility(View.VISIBLE);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModel.get(position).getPost().getPicture()), holder.img, SelfOptions);
                    holder.img.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), PhotoActivity.class);
                            intent.putExtra("photo", mNotifiModel.get(position).getPost().getPicture());
                            intent.putExtra("open", mNotifiModel.get(position).getPost().getUser().getOpenID());
                            intent.putExtra("picture", mNotifiModel.get(position).getPost().getUser().getPicture());
                            intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModel.get(position).getPost().getTimestamp()));
                            intent.putExtra("view", mNotifiModel.get(position).getPost().getViewCount());
                            intent.putExtra("money", mNotifiModel.get(position).getPost().getTotalRevenue());
                            intent.putExtra("dio", mNotifiModel.get(position).getPost().getCaption());
                            intent.putExtra("likecount", mNotifiModel.get(position).getPost().getLikeCount());
                            intent.putExtra("commentcount", mNotifiModel.get(position).getPost().getCommentCount());
                            intent.putExtra("likeed", mNotifiModel.get(position).getPost().getIsLiked());

                            intent.putExtra("postid", mNotifiModel.get(position).getPostID());
                            intent.putExtra("userid", mNotifiModel.get(position).getPost().getUser().getUserID());

                            intent.putExtra("name", mNotifiModel.get(position).getPost().getUser().getName());
                            intent.putExtra("isFollowing", mNotifiModel.get(position).getPost().getUser().getIsFollowing());
                            intent.putExtra("postCount", mNotifiModel.get(position).getPost().getUser().getPostCount());
                            intent.putExtra("followerCount", mNotifiModel.get(position).getPost().getUser().getFollowerCount());
                            intent.putExtra("followingCount", mNotifiModel.get(position).getPost().getUser().getFollowingCount());
                            intent.putExtra("goto", false);

                            startActivity(intent);
                        }
                    });
                }

            }
            else if(mNotifiModel.get(position).getType().compareTo("commentTag")==0)
            {
//                holder.doing.setVisibility(View.VISIBLE);
//                holder.doing.setText("你的好友");
//                holder.type.setText("在留言中標籤了你");

                if(mNotifiModel.get(position).getPost()!=null)
                {
                    holder.img.setVisibility(View.VISIBLE);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModel.get(position).getPost().getPicture()), holder.img, SelfOptions);
                    holder.img.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), PhotoActivity.class);
                            intent.putExtra("photo", mNotifiModel.get(position).getPost().getPicture());
                            intent.putExtra("open", mNotifiModel.get(position).getPost().getUser().getOpenID());
                            intent.putExtra("picture", mNotifiModel.get(position).getPost().getUser().getPicture());
                            intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModel.get(position).getPost().getTimestamp()));
                            intent.putExtra("view", mNotifiModel.get(position).getPost().getViewCount());
                            intent.putExtra("money", mNotifiModel.get(position).getPost().getTotalRevenue());
                            intent.putExtra("dio", mNotifiModel.get(position).getPost().getCaption());
                            intent.putExtra("likecount", mNotifiModel.get(position).getPost().getLikeCount());
                            intent.putExtra("commentcount", mNotifiModel.get(position).getPost().getCommentCount());
                            intent.putExtra("likeed", mNotifiModel.get(position).getPost().getIsLiked());

                            intent.putExtra("postid", mNotifiModel.get(position).getPostID());
                            intent.putExtra("userid", mNotifiModel.get(position).getPost().getUser().getUserID());

                            intent.putExtra("name", mNotifiModel.get(position).getPost().getUser().getName());
                            intent.putExtra("isFollowing", mNotifiModel.get(position).getPost().getUser().getIsFollowing());
                            intent.putExtra("postCount", mNotifiModel.get(position).getPost().getUser().getPostCount());
                            intent.putExtra("followerCount", mNotifiModel.get(position).getPost().getUser().getFollowerCount());
                            intent.putExtra("followingCount", mNotifiModel.get(position).getPost().getUser().getFollowingCount());
                            intent.putExtra("goto", false);

                            startActivity(intent);
                        }
                    });
                }
            }
            else
            {
//                holder.type.setText("追蹤你的近況");
            }

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView pic;
//        TextView name;
//        TextView type;
        TextView day;
        ImageView img;
        ImageView follow;
//        TextView doing;
    }

    public void LoadData(final boolean refresh)
    {
        if(isFetchingData)
        {
            return;
        }

        if(refresh)
        {
            noMoreData = false;
        }

        if(noMoreData)
        {
            return;
        }

        isFetchingData = true;

        ApiManager.getNotif(getActivity(), mNotifiModel.get(mNotifiModel.size() - 1).getTimestamp(), 15, new ApiManager.GetNotifCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<NotifiModel> notifiModel) {
                isFetchingData = false;

                if (success && notifiModel != null) {
                    if (notifiModel.size() != 0) {
                        mNotifiModel.addAll(notifiModel);

                        if (mNotifiModel.size() < 15) {
                            noMoreData = true;
                        }

                        if (mListAdapter != null) {
                            mListAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }
}
