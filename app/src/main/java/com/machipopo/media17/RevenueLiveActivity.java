package com.machipopo.media17;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.LiveModel;
import com.umeng.analytics.MobclickAgent;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by POPO on 15/8/21.
 */
public class RevenueLiveActivity extends BaseNewActivity
{
    private RevenueLiveActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;

    private ArrayList<LiveModel> mLiveModels = new ArrayList<LiveModel>();

    private ListView mList;
    private ProgressBar mProgress;
    private ImageView mNoData;

    private SimpleDateFormat birthday;
    private String c = "USD";
    private DecimalFormat df;
    private SimpleDateFormat mDateFormatTo;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_live_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        mList = (ListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mDateFormatTo = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        birthday = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        c = Singleton.getCurrencyType();
        df = new DecimalFormat("#.####");

        mProgress.setVisibility(View.VISIBLE);
        mNoData.setVisibility(View.GONE);

        ApiManager.getUserLiveStreams(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), Integer.MAX_VALUE, 100, new ApiManager.GetHotLiveStreamsCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<LiveModel> liveModel)
            {
                mProgress.setVisibility(View.GONE);

                if (success && liveModel != null)
                {
                    if (liveModel.size() != 0)
                    {
                        mNoData.setVisibility(View.GONE);

                        mLiveModels.clear();
                        mLiveModels.addAll(liveModel);
                        mList.setAdapter(new MyAdapter());
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    try
                    {
                        if (mCtx != null)
                        {
                            try{
//                                             showToast(getString(R.string.failed));
                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
        });
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_live));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class MyAdapter extends BaseAdapter
    {

        @Override
        public int getCount()
        {
            return mLiveModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.revenue_live_row, null);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.likes = (TextView) convertView.findViewById(R.id.likes);
                holder.time = (TextView) convertView.findViewById(R.id.time);
                holder.viewrs = (TextView) convertView.findViewById(R.id.viewrs);
                holder.timevount = (TextView) convertView.findViewById(R.id.timevount);
                holder.revenue = (TextView) convertView.findViewById(R.id.revenue);
                holder.gift_revenue = (TextView) convertView.findViewById(R.id.gift_revenue);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            Date date = new Date(mLiveModels.get(position).getBeginTime()*1000L);
            holder.day.setText(birthday.format(date));

            if(mLiveModels.get(position).getCaption().length()==0) holder.title.setText(getString(R.string.noset));
            else holder.title.setText(mLiveModels.get(position).getCaption());

            holder.likes.setText(String.valueOf(mLiveModels.get(position).getReceivedLikeCount()));
            holder.viewrs.setText(String.valueOf(mLiveModels.get(position).getViewerCount()));

            float all = 0f;
            all = mLiveModels.get(position).getRevenue()*Singleton.getCurrencyRate();
            holder.revenue.setText(df.format((double) all) + " " + c);

            float gift = 0f;
            gift = mLiveModels.get(position).getGiftRevenue()*Singleton.getCurrencyRate();
            holder.gift_revenue.setText(df.format((double) gift) + " " + c);

            holder.time.setText(mDateFormatTo.format(new Date((mLiveModels.get(position).getDuration() * 1000L) - TimeZone.getDefault().getRawOffset())));
            holder.timevount.setText(mDateFormatTo.format(new Date((mLiveModels.get(position).getTotalViewTime() * 1000L) - TimeZone.getDefault().getRawOffset())));

            return convertView;
        }
    }

    private class ViewHolder
    {
        TextView day;
        TextView title;
        TextView likes;
        TextView time;
        TextView viewrs;
        TextView timevount;
        TextView revenue;
        TextView gift_revenue;
    }
}
