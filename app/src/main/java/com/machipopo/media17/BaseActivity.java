package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by POPO on 5/20/15.
 */
public class BaseActivity extends BaseFragmentActivity
{
    ProgressDialog progressDialog = null;
    Handler handler = new Handler();
    InputMethodManager inputMethodManager = null;
    ContentResolver contentResolver = null;

    static ImagePickerDialogCallback imagePickerDialogCallback = null;
//    boolean needUploadPicture = true;
//    boolean needCreateThumbnail = true;
//    boolean needCrop = true;
    boolean needUploadPicture = false;
    boolean needCreateThumbnail = false;
    boolean needCrop = false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        contentResolver = (ContentResolver) getContentResolver();

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = BaseActivity.this.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }
    }

    public boolean isShowingProgressDialog()
    {
        return !(progressDialog==null);
    }

    public void showProgressDialog()
    {
        try
        {
            if(progressDialog==null)
            {
                progressDialog = ProgressDialog.show(this, "", getString(R.string.processing));
            }

            progressDialog.setCancelable(true);
        }
        catch (Exception e)
        {
        }
    }

    public void showProgressDialog(DialogInterface.OnCancelListener cancelListener)
    {
        try
        {
            if(progressDialog==null)
            {
                progressDialog = ProgressDialog.show(this, "",  getString(R.string.processing));
            }

            progressDialog.setCancelable(true);
            progressDialog.setOnCancelListener(cancelListener);
        }
        catch(Exception e)
        {
        }
    }

    public void hideProgressDialog()
    {
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    protected void onDestroy()
    {
        if(progressDialog!=null)
        {
            try
            {
                progressDialog.dismiss();
                progressDialog = null;
            }
            catch (Exception e)
            {
            }
        }

        super.onDestroy();
    }

    public void showToast(String text) {
        try
        {
            int mPicId = R.drawable.sad;
            if(text.compareTo(getString(R.string.done))==0 || text.compareTo(getString(R.string.complete))==0 || text.compareTo(getString(R.string.password_ok))==0) mPicId = R.drawable.happy;

            showToast(text, mPicId);
        }
        catch (Exception e)
        {
        }
    }

    public void showNetworkUnstableToast()
    {
        showToast(getString(R.string.connet_erroe), R.drawable.sad);
    }

    public void showToast(String text, int imageResourceID)
    {
        try
        {
            LayoutInflater inflater = getLayoutInflater();
            RelativeLayout container = (RelativeLayout) inflater.inflate(R.layout.custom_toast, null);
            TextView toastTextView = (TextView) container.findViewById(R.id.toastTextView);
            ImageView toastImageView = (ImageView) container.findViewById(R.id.toastImageView);

            toastTextView.setText(text);
            toastImageView.setImageResource(imageResourceID);

            final Toast toast = new Toast(this);
            toast.setGravity(Gravity.CENTER, 0, dpToPixel(-60));
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(container);
            toast.show();

            handler.postDelayed(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        toast.cancel();
                    }
                    catch (Exception e)
                    {
                    }
                }
            }, 2000);
        }
        catch (Exception e)
        {
        }
    }

    public int dpToPixel(int dp)
    {
        return Singleton.dpToPixel(dp);
    }

    public void hideKeyboard()
    {
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void hideKeyboard(long delayMillis)
    {
        handler.postDelayed(new Runnable() {
            public void run() {
                hideKeyboard();
            }
        }, delayMillis);
    }

    public void showKeyboard()
    {
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void showKeyboard(long delayMillis)
    {
        handler.postDelayed(new Runnable() {
            public void run() {
                showKeyboard();
            }
        }, delayMillis);
    }

    public void showAlertDialog(String title, String message)
    {
        showOKDialog(title, message, null);
    }

    public void showOKDialog(String title, String message, final TestActivity.DialogCallback callback) {
        try {
            final Dialog dialog = new Dialog(this, R.style.MyDialog);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.my_dialog);

            RelativeLayout rootView = (RelativeLayout) dialog.findViewById(R.id.rootView);
            Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
            View marginView = (View) dialog.findViewById(R.id.marginView);
            Button okButton = (Button) dialog.findViewById(R.id.okButton);
            TextView titleTextView = (TextView) dialog.findViewById(R.id.titleTextView);
            TextView messageTextView = (TextView) dialog.findViewById(R.id.messageTextView);

            rootView.getLayoutParams().width = Singleton.SCREEN_WIDTH * 8 / 10;

            cancelButton.setVisibility(View.GONE);
            marginView.setVisibility(View.GONE);

            okButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();

                    if(callback!=null) {
                        callback.onResult(true);
                    }
                }
            });

            titleTextView.setText(title);
            messageTextView.setText(message);

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    dialog.dismiss();

                    callback.onResult(false);
                }
            });

            dialog.show();
        } catch(Exception e) {

        }
    }

    public interface DialogCallback
    {
        public void onResult(boolean didClickOk);
    }

    public void showYesNoDialog(String title, String message, final DialogCallback callback) {
        try
        {
            final Dialog dialog = new Dialog(this, R.style.MyDialog);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.my_dialog);

            RelativeLayout rootView = (RelativeLayout) dialog.findViewById(R.id.rootView);
            Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
            Button okButton = (Button) dialog.findViewById(R.id.okButton);
            TextView titleTextView = (TextView) dialog.findViewById(R.id.titleTextView);
            TextView messageTextView = (TextView) dialog.findViewById(R.id.messageTextView);

            rootView.getLayoutParams().width = Singleton.SCREEN_WIDTH * 8 / 10;

            cancelButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            okButton.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    dialog.dismiss();

                    if(callback!=null)
                    {
                        callback.onResult(true);
                    }
                }
            });

            titleTextView.setText(title);
            messageTextView.setText(message);

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener()
            {
                public void onCancel(DialogInterface dialog)
                {
                    dialog.dismiss();

                    callback.onResult(false);
                }
            });

            dialog.show();
        }
        catch (Exception e)
        {

        }
    }

    public interface SingleItemPickerCallback
    {
        public void onResult(boolean done, int selectedItem);
    }

    public void showSingleItemPicker(String title, final ArrayList<String> items, int initialSelectedItemIndex, final SingleItemPickerCallback callback)
    {
        try
        {
            final Dialog dialog = new Dialog(this, R.style.MyDialog);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.item_picker_dialog);

            RelativeLayout rootView = (RelativeLayout) dialog.findViewById(R.id.rootView);
            TextView titleTextView = (TextView) dialog.findViewById(R.id.titleTextView);
            Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
            Button okButton = (Button) dialog.findViewById(R.id.okButton);
            final ListView itemListView = (ListView) dialog.findViewById(R.id.itemListView);

            final HashSet<Integer> selectedIndexSet = new HashSet<Integer>();
            selectedIndexSet.add(new Integer(initialSelectedItemIndex));

            itemListView.setAdapter(new BaseAdapter() {
                @Override
                public View getView(final int position, View rowView, ViewGroup parent) {
                    if(rowView==null||rowView.getClass().equals(ItemRow.class)==false) {
                        rowView = new ItemRow(BaseActivity.this);
                    }

                    ItemRow itemRow = (ItemRow) rowView;

                    itemRow.nameTextView.setText(items.get(position));

                    if(selectedIndexSet.contains(new Integer(position))) {
                        itemRow.selectionIndicatorImageView.setImageResource(R.drawable.select_on);
                    } else {
                        itemRow.selectionIndicatorImageView.setImageResource(R.drawable.select_off);
                    }

                    itemRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedIndexSet.clear();
                            selectedIndexSet.add(new Integer(position));

                            notifyDataSetChanged();
                        }
                    });

                    return itemRow;
                }

                @Override
                public long getItemId(int position) {
                    return 0;
                }

                @Override
                public Object getItem(int position) {
                    return null;
                }

                @Override
                public int getCount() {
                    return items.size();
                }
            });

            titleTextView.setText(title);

            rootView.getLayoutParams().width = Singleton.SCREEN_WIDTH * 8 / 10;

            cancelButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        callback.onResult(false, selectedIndexSet.iterator().next());
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    dialog.dismiss();
                }
            });

            okButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        callback.onResult(true, selectedIndexSet.iterator().next());
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    dialog.dismiss();
                }
            });

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    callback.onResult(false, selectedIndexSet.iterator().next());

                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {
            Singleton.log(e.toString());
        }
    }

    public interface ImagePickerDialogCallback
    {
        public void onResult(boolean imageOk, boolean uploadComplete, String imageFileName);
    }

    public void showImagePickerOptionDialog(boolean needUploadPicture, boolean needCreateThumbnail, boolean needCrop, ImagePickerDialogCallback callback)
    {
        imagePickerDialogCallback = callback;
        this.needUploadPicture = needUploadPicture;
        this.needCreateThumbnail = needCreateThumbnail;
        this.needCrop = needCrop;

        showOptionPickerDialog(new ArrayList<String>(Arrays.asList(getString(R.string.take_pic), getString(R.string.pick_album))), new OptionPickerCallback() {
            @Override
            public void onResult(boolean done, int selectedIndex) {
                if(!done) {
                    return;
                }

                if(selectedIndex==0) {
                    startTakePictureIntent();
                } else if(selectedIndex==1) {
                    startPickImageIntent();
                }
            }
        });
    }

    public void startTakePictureIntent()
    {
        new File(Singleton.getExternalTempImagePath()).delete();

        try{
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Singleton.getExternalTempImagePath())));
            startActivityForResult(intent, Constants.TAKE_PICTURE_REQUEST);
        }
        catch (Exception e){
        }
    }

    public void startPickImageIntent()
    {
        new File(Singleton.getExternalTempImagePath()).delete();

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Constants.PICK_IMAGE_REQUEST);
    }

    public interface OptionPickerCallback
    {
        public void onResult(boolean done, int selectedIndex);
    }

    public void showOptionPickerDialog(final ArrayList<String> items, final OptionPickerCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        CharSequence[] itemTitles = items.toArray(new CharSequence[items.size()]);

        builder.setItems(itemTitles, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callback.onResult(true, which);
            }
        });

        AlertDialog dialog = builder.create();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();

                callback.onResult(false, 0);
            }
        });

        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

//        try{
//            if(!((Story17Application) getApplication()).getNoShowToast())
//            {
//                return ;
//            }
//        }
//        catch (Exception e){
//        }

        try
        {
            if(resultCode!=RESULT_OK)
            {
                return;
            }

            boolean hasGotPicture = false;

            if(requestCode==Constants.TAKE_PICTURE_REQUEST)
            {
                hasGotPicture = true;
                needCrop = true;
                needCreateThumbnail = true;
            }

            if(requestCode==Constants.PICK_IMAGE_REQUEST)
            {
                if(data!=null && data.getData()!=null)
                {
                    String selectedImageFilePath = Singleton.getRealPathFromURI(data.getData(), this);
                    String tmpImageFilePath = Singleton.getExternalTempImagePath();

                    if(selectedImageFilePath==null)
                    {
                        // copy image to sdcard
                        try {
                            Uri selectedImageURI = data.getData();
                            InputStream input = contentResolver.openInputStream(selectedImageURI);
                            BitmapHelper.saveBitmap(BitmapFactory.decodeStream(input, null, BitmapHelper.getBitmapOptions(2)), tmpImageFilePath, Bitmap.CompressFormat.JPEG);
                            hasGotPicture = true;
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    else
                    {
                        Singleton.copyFile(new File(selectedImageFilePath), new File(tmpImageFilePath));
                        hasGotPicture = true;
                    }
                }
            }

            if(requestCode==Constants.CROP_IMAGE_REQUEST)
            {
                hasGotPicture = true;
                needUploadPicture = true;
            }

            if(hasGotPicture==false)
            {
                try
                {
                    imagePickerDialogCallback.onResult(false, false, "");
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }

                return;
            }

            // crop
            if(needCrop)
            {
                needCrop = false;

                startActivityForResult(new Intent().setClass(this, CropImageActivity.class), Constants.CROP_IMAGE_REQUEST);
                return;
            }

            int largeSize = Constants.PROFILE_PICTURE_PIXELS;
            int thumbnailSize = Constants.PROFILE_PICTURE_THUMBNAIL_PIXELS;
            if(getConfig(Constants.PHONE_WIDTH_HEIGHT_SIZE,Constants.DEFAULT_PIXELS) < Constants.CHECK_PIXELS)
            {
                largeSize = Constants.LOW_PROFILE_PICTURE_PIXELS;
                thumbnailSize = Constants.LOW_PROFILE_PICTURE_THUMBNAIL_PIXELS;
            }

            // generate thumbnail
            Bitmap large = BitmapHelper.getBitmap(Singleton.getExternalTempImagePath(), largeSize);
            Bitmap thumbnail = BitmapHelper.getBitmap(Singleton.getExternalTempImagePath(), thumbnailSize);

            if(large==null || thumbnail==null)
            {
                imagePickerDialogCallback.onResult(false, false, "");
                showAlertDialog(getString(R.string.prompt), getString(R.string.unknown_error_occur));
                return;
            }

            final String pictureFileName = Singleton.getUUIDFileName("jpg");
            final String thumbnailPictureFileName = "THUMBNAIL_" + pictureFileName;

//            Singleton.log("pictureFileName:" + pictureFileName);
//            Singleton.log("thumbnailPictureFileName:" + thumbnailPictureFileName);

//            Log.d("123", "pictureFileName : " + pictureFileName);
//            Log.d("123", "thumbnailPictureFileName : " + thumbnailPictureFileName);

            new File(Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName).delete();

            BitmapHelper.saveBitmap(large, Singleton.getExternalMediaFolderPath() + pictureFileName, Bitmap.CompressFormat.JPEG);
            BitmapHelper.saveBitmap(thumbnail, Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName, Bitmap.CompressFormat.JPEG);

            if(!needUploadPicture)
            {
                imagePickerDialogCallback.onResult(true, false, pictureFileName);
                return;
            }

            needCreateThumbnail = true;
            showProgressDialog();

            ArrayList<String> filesToUpload = new ArrayList<String>();
            filesToUpload.add(pictureFileName);

            if(needCreateThumbnail)
            {
                filesToUpload.add(thumbnailPictureFileName);
            }

            MultiFileUploader uploader = new MultiFileUploader(filesToUpload);

            uploader.startUpload(BaseActivity.this, new MultiFileUploader.MultiFileUploaderCallback()
            {
                @Override
                public void didComplete()
                {
                    hideProgressDialog();
                    showCompleteToast();
                    imagePickerDialogCallback.onResult(true, true, pictureFileName);
                }

                @Override
                public void didFail()
                {
                    hideProgressDialog();
                    showNetworkUnstableToast();
                    imagePickerDialogCallback.onResult(true, false, pictureFileName);
                }
            });
        }
        catch (Exception e)
        {

        }
    }

    public int getConfig(String key, int def)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getInt(key, def);
    }

    public void showCompleteToast() {
        showToast(getString(R.string.done), R.drawable.happy);
    }
}
