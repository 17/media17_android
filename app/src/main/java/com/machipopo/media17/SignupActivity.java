package com.machipopo.media17;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by POPO on 5/21/15.
 */
public class SignupActivity extends BaseActivity
{
    private SignupActivity mCtx = this;
    private EditText mAccount, mPasswoird, mCheck;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        initTitleBar();

        mAccount = (EditText) findViewById(R.id.account);
        mPasswoird = (EditText) findViewById(R.id.password);
        mCheck = (EditText) findViewById(R.id.check);

        mAccount.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                if(charSequence.toString().length()!=0)
                {
                    String text = charSequence.toString().substring(charSequence.toString().length()-1,charSequence.toString().length());
                    if(text.compareTo("A")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "a");
                    else if(text.compareTo("B")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "b");
                    else if(text.compareTo("C")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "c");
                    else if(text.compareTo("D")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "d");
                    else if(text.compareTo("E")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "e");
                    else if(text.compareTo("F")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "f");
                    else if(text.compareTo("G")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "g");
                    else if(text.compareTo("H")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "h");
                    else if(text.compareTo("I")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "i");
                    else if(text.compareTo("J")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "j");
                    else if(text.compareTo("K")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "k");
                    else if(text.compareTo("L")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "l");
                    else if(text.compareTo("M")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "m");
                    else if(text.compareTo("N")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "n");
                    else if(text.compareTo("O")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "o");
                    else if(text.compareTo("P")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "p");
                    else if(text.compareTo("Q")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "q");
                    else if(text.compareTo("R")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "r");
                    else if(text.compareTo("S")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "s");
                    else if(text.compareTo("T")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "t");
                    else if(text.compareTo("U")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "u");
                    else if(text.compareTo("V")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "v");
                    else if(text.compareTo("W")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "w");
                    else if(text.compareTo("X")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "x");
                    else if(text.compareTo("Y")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "y");
                    else if(text.compareTo("Z")==0) mAccount.setText(mAccount.getText().toString().substring(0,mAccount.getText().toString().length()-1) + "z");

                    mAccount.setSelection(mAccount.getText().toString().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable)
            {
            }
        });

        showKeyboard();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.singup));

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_white_back);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mCtx, LoginMenuActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.next));
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
                //account 2-20
                //passwoud 5-20
                if(mAccount.getText().toString().length()!=0 && mPasswoird.getText().toString().length()!=0 /*&& mCheck.getText().toString().length()!=0*/)
                {
                    if(mAccount.getText().toString().length()<2)
                    {
                        try{
//                          showToast(getString(R.string.account_size));
                            Toast.makeText(mCtx, getString(R.string.account_size), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                        return;
                    }

                    if(mPasswoird.getText().toString().length()<5)
                    {
                        try{
//                          showToast(getString(R.string.password_size));
                            Toast.makeText(mCtx, getString(R.string.password_size), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                        return;
                    }

//                    if(mPasswoird.getText().toString().compareTo(mCheck.getText().toString())==0)
//                    {
                        showProgressDialog();
                        ApiManager.checkOpenIDAvailable(mCtx, mAccount.getText().toString(), new ApiManager.CheckOpenIDAvaliableCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                hideProgressDialog();
//                                Log.d("123", "success : " + success);

                                if (success) {
                                    Intent intent = new Intent();
                                    intent.setClass(mCtx, SignupSettingActivity.class);
                                    intent.putExtra("account", mAccount.getText().toString());
                                    intent.putExtra("password", mPasswoird.getText().toString());
                                    startActivity(intent);
                                    mCtx.finish();
                                } else {
                                    try{
//                          showToast(getString(R.string.account_same));
                                        Toast.makeText(mCtx, getString(R.string.account_same), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
//                    }
//                    else Toast.makeText(mCtx, getString(R.string.password_same), Toast.LENGTH_SHORT).show();
                }
                else {
                    try{
//                          showToast(getString(R.string.entet_onfo));
                        Toast.makeText(mCtx, getString(R.string.entet_onfo), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intent = new Intent();
            intent.setClass(mCtx, LoginMenuActivity.class);
            startActivity(intent);
            mCtx.finish();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
