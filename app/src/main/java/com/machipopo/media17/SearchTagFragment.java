package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.fragment.BaseFragment;
import com.machipopo.media17.model.TagModel;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by POPO on 6/1/15.
 */
public class SearchTagFragment extends BaseFragment
{
    private PullToRefreshListView mList;
    private LayoutInflater inflater;
    private String mText = "";

    private InputMethodManager inputMethodManager;
    private ArrayList<TagModel> mTagsModel = new ArrayList<TagModel>();
    private TagAdapter mTagAdapter;

    public SearchTagFragment()
    {

    }

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart("SearchTagFragment");
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd("SearchTagFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.search_tag, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        mList = (PullToRefreshListView) getView().findViewById(R.id.list);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View views, int i, long l)
            {
                if(mTagsModel!=null && mTagsModel.size()!=0)
                {
                    ViewGroup view = (ViewGroup) getActivity().getWindow().getDecorView();
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    Intent intent = new Intent();
                    intent.setClass(getActivity(),TagPostActivity.class);
                    intent.putExtra("tag",mTagsModel.get(i-1).getHashTag());
                    startActivity(intent);
                }
            }
        });
    }

    public void setSearchText(String text)
    {
        if(text.length()==0)
        {
            mText = "";
            mTagsModel.clear();

            mTagAdapter = new TagAdapter();
            mList.setAdapter(mTagAdapter);

            return;
        }

        if(mText.compareTo(text)==0)
            return;

        mText = text;
        ApiManager.searchHashTag(getActivity(), text, 0, 100, new ApiManager.SearchHashTagCallback()
        {
            @Override
            public void onResult(boolean success, String message,ArrayList<TagModel> tahmodel)
            {
                if (success && tahmodel!=null)
                {
                    mTagsModel.clear();
                    mTagsModel.addAll(tahmodel);

                    mTagAdapter = new TagAdapter();
                    mList.setAdapter(mTagAdapter);

//                    ViewGroup view = (ViewGroup) getActivity().getWindow().getDecorView();
//                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                else {
                    if(!isAdded()) {
                        return;
                    }

                    try{
//                      ((SearchInfoActivity)getActivity()).showToast(getString(R.string.search_failed));
                        Toast.makeText(getActivity(), getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                };
            }
        });
    }

    private class TagAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mTagsModel.size();
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup)
        {
            ViewHolder holder = new ViewHolder();

            //Umeng Monitor
            String Umeng_id="SearchUserAndClick";
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put(Umeng_id, Umeng_id);
            MobclickAgent.onEventValue(getActivity(),Umeng_id,mHashMap,0);

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.search_tag_row, null);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.count = (TextView) convertView.findViewById(R.id.count);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.name.setText(mTagsModel.get(i).getHashTag());
            holder.count.setText(String.valueOf(mTagsModel.get(i).getPostCount()));

            return convertView;
        }
    }

    private class ViewHolder
    {
        TextView name;
        TextView count;
    }
}
