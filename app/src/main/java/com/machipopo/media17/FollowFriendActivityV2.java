package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.SuggestedUsersModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by POPO on 5/26/15.
 */
public class FollowFriendActivityV2 extends BaseActivity
{
    private FollowFriendActivityV2 mCtx = this;
    private LayoutInflater inflater;
    private ArrayList<SuggestedUsersModel> mSuggesteds = new ArrayList<SuggestedUsersModel>();
    private Button mBtn, mNext;
    private TextView mText;
    private ListView mList;
    private FriendAdapter mFriendAdapter;
    private ArrayList<Boolean> mState = new ArrayList<Boolean>();
    private Story17Application mApplication;

    private DisplayMetrics mDisplayMetrics;

    private DisplayImageOptions BigOptions, SelfOptions;

    private ImageView mNoData;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.follow_friend_activity_v2);

        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        initTitleBar();

        mText = (TextView) findViewById(R.id.have);
        mBtn = (Button) findViewById(R.id.all);
        mList = (ListView) findViewById(R.id.list);
        mNext = (Button) findViewById(R.id.btn_next);

        mNoData = (ImageView) findViewById(R.id.nodata);
        mApplication = (Story17Application) getApplication();

        showProgressDialog();
        ApiManager.getSuggestedUsers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, new ApiManager.GetSuggestedUsersCallback() {
            @Override
            public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested) {
                hideProgressDialog();
                if (success)//&& suggested!=null
                {
                    if (suggested.size() != 0) {
                        mSuggesteds.addAll(suggested);
                        mText.setText(getString(R.string.have) + mSuggesteds.size() + getString(R.string.have_super));

                        mBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                JSONArray mJSONArray = new JSONArray();
                                for (int i = 0; i < mSuggesteds.size(); i++) {
                                    if(mSuggesteds.get(i).getPrivacyMode().compareTo("private")!=0)  mJSONArray.put(mSuggesteds.get(i).getUserID());
                                }

                                for (int i = 0; i < mSuggesteds.size(); i++) {
                                    mState.set(i, true);
                                }
//                                mBtn.setText("全部追蹤");
                                mFriendAdapter.notifyDataSetChanged();

                                //event tracking
                                try{
                                    LogEventUtil.FollowAllSuggestUser(mCtx,mApplication);
                                }catch (Exception X)
                                {

                                }

                                ApiManager.followAllAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mJSONArray.toString(), new ApiManager.FollowAllActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {

                                        }
                                    }
                                });
                            }
                        });

                        for (int i = 0; i < mSuggesteds.size(); i++) {
                            mState.add(false);
                        }

                        mFriendAdapter = new FriendAdapter();
                        mList.setAdapter(mFriendAdapter);
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                        mBtn.setVisibility(View.GONE);
//                        Toast.makeText(mCtx, "無朋友", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    mNoData.setVisibility(View.VISIBLE);
                    mBtn.setVisibility(View.GONE);
//                    Toast.makeText(mCtx, "搜尋失敗", Toast.LENGTH_SHORT).show();
                }
            }
        });

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.super_people));
        mTitle.setTextColor(Color.WHITE);

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.done));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, MenuActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    private class FriendAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mSuggesteds.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.follow_suggeste_row, null);
                holder.img = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.follow = (Button) convertView.findViewById(R.id.follow);

                holder.pic_layout = (LinearLayout) convertView.findViewById(R.id.pic_layout);
                holder.pic1 = (ImageView) convertView.findViewById(R.id.pic1);
                holder.pic2 = (ImageView) convertView.findViewById(R.id.pic2);
                holder.pic3 = (ImageView) convertView.findViewById(R.id.pic3);

                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            //http://story17.machipopo.com/THUMBNAIL_744FE2D7-9317-41D2-AC2C-5D276E075F8F.jpg
            //Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModel.get(position).getPicture())
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(position).getPicture()), holder.img,SelfOptions);

            holder.name.setText(mSuggesteds.get(position).getOpenID());
            holder.dio.setText(mSuggesteds.get(position).getName());
            final int pos = position;

            if(mState.get(position))
            {
                holder.follow.setText(getString(R.string.notifi_follow));
                holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
                holder.follow.setTextColor(Color.WHITE);
            }
            else
            {
                if(mSuggesteds.get(pos).getFollowRequestTime()!=0)
                {
                    holder.follow.setText(getString(R.string.private_mode_request_send));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    holder.follow.setText("+ " + getString(R.string.user_profile_follow));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
            }

            final Button btn = holder.follow;
            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mState.get(pos))
                    {
                        mState.set(pos, false);
                        btn.setText("+ " + getString(R.string.user_profile_follow));
                        btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                        btn.setTextColor(getResources().getColor(R.color.content_text_color));
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mSuggesteds.get(pos).getUserID());
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mSuggesteds.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if (success)
                                {

                                }
                            }
                        });
                    }
                    else
                    {
                        if(mSuggesteds.get(pos).getFollowRequestTime()!=0)
                        {
                            btn.setText("+ " + getString(R.string.user_profile_follow));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                            mState.set(pos, false);
                            mSuggesteds.get(pos).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mSuggesteds.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                                                showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }
                                return ;
                            }

                            if(mSuggesteds.get(pos).getPrivacyMode().compareTo("private")==0)
                            {
                                btn.setText(getString(R.string.private_mode_request_send));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                mState.set(pos, false);
                                mSuggesteds.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mSuggesteds.get(pos).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mState.set(pos, false);
                                            mSuggesteds.get(pos).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                mState.set(pos,true);
                                btn.setText(getString(R.string.user_profile_following));
                                btn.setBackgroundResource(R.drawable.btn_green_selector);
                                btn.setTextColor(Color.WHITE);

                                String Umeng_id="FollowUser";
                                HashMap<String,String> mHashMap = new HashMap<String,String>();
                                mHashMap.put(Umeng_id, Umeng_id);
                                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                                try{
                                LogEventUtil.FollowUser(mCtx,mApplication,mSuggesteds.get(pos).getUserID());
                                LogEventUtil.FollowSuggestUser(mCtx, mApplication, mSuggesteds.get(pos).getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mSuggesteds.get(pos).getUserID(), new ApiManager.FollowUserActionCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {

                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            int height = (int) mDisplayMetrics.widthPixels/3;
            holder.pic_layout.getLayoutParams().height = height;
            int size = mSuggesteds.get(position).getPostInfo().size();

            if(size>=3)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(0).getPicture()), holder.pic1,BigOptions);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(1).getPicture()), holder.pic2,BigOptions);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(2).getPicture()), holder.pic3,BigOptions);
            }
            else if(size==2)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(0).getPicture()), holder.pic1,BigOptions);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(1).getPicture()), holder.pic2,BigOptions);
            }
            else if(size==1)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(0).getPicture()), holder.pic1,BigOptions);
            }

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView img;
        TextView name;
        TextView dio;
        Button follow;

        LinearLayout pic_layout;
        ImageView pic1;
        ImageView pic2;
        ImageView pic3;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {


            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}