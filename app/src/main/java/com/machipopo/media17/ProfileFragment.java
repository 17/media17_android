package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshHeaderGridView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.View.TouchImage;
import com.machipopo.media17.fragment.BaseFragment;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;
import com.warnyul.android.widget.FastVideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by POPO on 5/27/15.
 */
public class ProfileFragment extends BaseFragment
{
    private Story17Application mApplication;
    private LayoutInflater inflater;

    private ImageView mImg;
    private TextView mFollowers, mFollowing,mFollowersText,mFollowingText;
    private Button mMoney;
    private TextView mOpen, mDio, mWeb,mLikecount;
    private ImageView mGrid, mList, mLike,mEdit;
    private ImageView mVerifie;
    private ImageView likecountImg;

    private PullToRefreshHeaderGridView mScroll;
    private ProgressBar mProgress;
    private RelativeLayout mBear;
    private ImageView mNoData;

    private String name = "", picture = "", open = "", bio = "";
    private String post = "", follow = "", following = "",web="";
    private String targetUserID = "";
    private String likecount = "0";

//    private Boolean isfollowing;
    private int mGridHeight;

    private DisplayMetrics mDisplayMetrics;
    private DisplayImageOptions BigOptions, SelfOptions,imgOptions,GorpOptions;

    private GridAdapter mGridAdapter;
    private MyListAdapter mListAdapter;
    private LikeListAdapter mLikeListAdapter;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};
    private int bears[] = {R.drawable.bear_1,R.drawable.bear_2,R.drawable.bear_3,R.drawable.bear_4,R.drawable.bear_5,R.drawable.bear_6,R.drawable.bear_7,R.drawable.bear_8};
    private int bubbles[] = {R.drawable.bubble_1,R.drawable.bubble_2,R.drawable.bubble_3,R.drawable.bubble_4,R.drawable.bubble_5,R.drawable.bubble_6,R.drawable.bubble_7,R.drawable.bubble_8,R.drawable.bubble_9,R.drawable.bubble_10};
    private int cats[] = {R.drawable.cat_1,R.drawable.cat_2,R.drawable.cat_3,R.drawable.cat_4,R.drawable.cat_5,R.drawable.cat_6,R.drawable.cat_7,R.drawable.cat_8};
    private int rabbits[] = {R.drawable.rabbit_1,R.drawable.rabbit_2,R.drawable.rabbit_3,R.drawable.rabbit_4,R.drawable.rabbit_5,R.drawable.rabbit_6,R.drawable.rabbit_7,R.drawable.rabbit_8};
    private int logos[] = {R.drawable.logo_1,R.drawable.logo_2,R.drawable.logo_3,R.drawable.logo_4,R.drawable.logo_5,R.drawable.logo_6,R.drawable.logo_7,R.drawable.logo_8};

    private float mY = 0 ,mX = 0;

    private ArrayList<FeedModel> mFeedModels = new ArrayList<FeedModel>();
    private ArrayList<FeedModel> mLikMoedels = new ArrayList<FeedModel>();

    private View mHeader;

    private String[] mListMoreText= {"",""};
    private String[] mLikeMoreText;

    private Boolean MyisFetchingData = false;
    private Boolean MynoMoreData = false;
    private int MyNowPos = 0;
    private ArrayList<FastVideoView> mMyVideoLists = new ArrayList<FastVideoView>();

    private Boolean LikeisFetchingData = false;
    private Boolean LikenoMoreData = false;
    private int LikeNowPos = 0;
    private ArrayList<FastVideoView> mLikeVideoLists = new ArrayList<FastVideoView>();

    private Boolean mVideoPlayState = true;
    private int mMyPagte = 0;

    private Boolean mSetHeaderHeight = true;
    private LinearLayout mFollowerLayout,mFollowingLayout,mLikeLayout;

    private int mLikeCount = 0;
    private String mLikePos = "";
    private String mLikePosUser = "";
    private Timer mTimer;
    private int lastCount = 0;
    private Boolean mGodHand = false;
    private RelativeLayout mLoadLayout;
    private RelativeLayout mSystem_bage;
    private ProgressBar mBigProgress;
    private ImageView mPresent;

    //event tracking
    private int startSelfPage = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.profile_fragment, container, false);
    }

    @Override
    public void onHiddenChanged(boolean hidden)
    {
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        mApplication = (Story17Application) getActivity().getApplication();
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDisplayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        contentResolver = (ContentResolver) getActivity().getContentResolver();
        mGridHeight = (int)((float)mDisplayMetrics.widthPixels/(float)3);

        try{
            picture = Singleton.preferences.getString(Constants.PICTURE, "");
        }catch (Exception e){
            picture = "";
        }

        try {
            post = getConfig(Constants.PROFILE_POST_COUNT, "0");
        }catch (Exception e){
            post = "0";
        }

        try {
            follow = getConfig(Constants.PROFILE_FOLLOWER_COUNT, "0");
        }catch (Exception e){
            follow = "0";
        }

        try {
            following = getConfig(Constants.PROFILE_FOLLOWING_COUNT, "0");
        }catch (Exception e){
            following = "0";
        }

        try {
            open = Singleton.preferences.getString(Constants.OPEN_ID, "");
        }catch (Exception e){
            open = "";
        }

        try {
            bio = getConfig(Constants.PROFILE_BIO, "");
        }catch (Exception e){
            bio = "";
        }

        try {
            targetUserID = Singleton.preferences.getString(Constants.USER_ID, "");
        }catch (Exception e){
            targetUserID = "";
        }

        try {
            name = getConfig(Constants.PROFILE_NAME, "");
        }catch (Exception e){
            name = "";
        }

        try {
            web = getConfig(Constants.PROFILE_WEB, "");
        }catch (Exception e){
            web = "";
        }

        try {
            likecount = getConfig(Constants.PROFILE_LIKE_COUNT, "0");
        }catch (Exception e){
            likecount = "0";
        }

        initTitleBar();

        //event tracking
        startSelfPage = Singleton.getCurrentTimestamp();
        try{
            LogEventUtil.EnterSelfPage(getActivity(),mApplication);
        }catch (Exception x)
        {

        }

        mScroll = (PullToRefreshHeaderGridView) getView().findViewById(R.id.scroll);
        mHeader = inflater.inflate(R.layout.profile_header, null);
        mScroll.getRefreshableView().addHeaderView(mHeader);
        mScroll.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshBase refreshView) {
                ApiManager.getUserPost(getActivity(), targetUserID, Integer.MAX_VALUE, 18, new ApiManager.GetUserPostCallback() {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                        mScroll.onRefreshComplete();

                        if (!isAdded()) {
                            return;
                        }

                        if (success && feedModel != null) {
                            if (feedModel.size() != 0) {
                                mNoData.setVisibility(View.GONE);

                                MyisFetchingData = false;
                                MynoMoreData = false;
                                mFeedModels.clear();
                                mFeedModels.addAll(feedModel);
                                mLikMoedels.clear();
                                mGrid.setImageResource(R.drawable.profile_grid_active);
                                mList.setImageResource(R.drawable.profile_list);
                                mLike.setImageResource(R.drawable.profile_like);

                                if (mGridAdapter != null) mGridAdapter = null;
                                mGridAdapter = new GridAdapter();
                                mScroll.setAdapter(mGridAdapter);

                                mMyPagte = 0;

                                mScroll.getRefreshableView().setNumColumns(3);

                                if (feedModel.size() < 18) {
                                    MynoMoreData = true;
                                }
                            }
                            reHeaderHeight();
                        }
                    }
                });
            }
        });

        mScroll.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {
                if (scrollState == SCROLL_STATE_FLING)
                {
                    ImageLoader.getInstance().pause();
                    mVideoPlayState = false;
                }

                if (scrollState == SCROLL_STATE_IDLE)
                {
                    ImageLoader.getInstance().resume();
                    mVideoPlayState = true;
                    if(mMyPagte==1 || mMyPagte==2)
                    {
//                        if(mListAdapter!=null) mListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        imgOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        mImg = (ImageView) getView().findViewById(R.id.img);
        mFollowers = (TextView) getView().findViewById(R.id.followers);
        mFollowing = (TextView) getView().findViewById(R.id.following);
        mFollowersText = (TextView) getView().findViewById(R.id.followers_text);
        mFollowingText = (TextView) getView().findViewById(R.id.following_text);
        mEdit = (ImageView) getView().findViewById(R.id.edit);
        mOpen = (TextView) getView().findViewById(R.id.open);
        mDio = (TextView) getView().findViewById(R.id.dio);
        mMoney = (Button) getView().findViewById(R.id.money);
        mWeb = (TextView) getView().findViewById(R.id.web);
        mLikecount = (TextView) getView().findViewById(R.id.likecount);
        likecountImg = (ImageView) getView().findViewById(R.id.likecount_icon);
        mGrid = (ImageView) getView().findViewById(R.id.grid);
        mList = (ImageView) getView().findViewById(R.id.list);
        mLike = (ImageView) getView().findViewById(R.id.like);
        mVerifie = (ImageView) getView().findViewById(R.id.verifie);

        mFollowerLayout = (LinearLayout) getView().findViewById(R.id.follower_layout);
        mFollowingLayout = (LinearLayout) getView().findViewById(R.id.following_layout);
        mLikeLayout = (LinearLayout) getView().findViewById(R.id.like_layout);
        mLoadLayout = (RelativeLayout) getView().findViewById(R.id.load_layout);
        mSystem_bage = (RelativeLayout) getView().findViewById(R.id.system_bage);
        mBigProgress = (ProgressBar) getView().findViewById(R.id.big_progress);
        mPresent = (ImageView) getView().findViewById(R.id.present);

        if(mApplication.isVerifie) mVerifie.setVisibility(View.VISIBLE);
        else mVerifie.setVisibility(View.GONE);

        mProgress = (ProgressBar) getView().findViewById(R.id.progress);
        mBear = (RelativeLayout) getView().findViewById(R.id.bear);
        mNoData = (ImageView) getView().findViewById(R.id.nodata);

        mFollowers.setText(follow);
        mFollowing.setText(following);
        mLikecount.setText(likecount);
        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + picture), mImg, imgOptions);
        mOpen.setText(name);

        if(web.length()!=0)
        {
            mWeb.setText(web);
            mWeb.setVisibility(View.VISIBLE);
        }
        else mWeb.setVisibility(View.GONE);

        if(bio.length()!=0)
        {
            mDio.setText(bio);
            mDio.setVisibility(View.VISIBLE);
        }
        else mDio.setVisibility(View.GONE);

        String c = Singleton.getCurrencyType();
        double m = (double) getMoneyConfig(Constants.REVENUE_TODAY, 0f) *(double)Singleton.getCurrencyRate();
        DecimalFormat df=new DecimalFormat("#.####");
        if(Constants.INTERNATIONAL_VERSION){
            mMoney.setText(getString(R.string.today_revenue) + " " + df.format(m) + " " + c);
        }
        else{
            if(c.compareTo("CNY")==0){
                mMoney.setText(getString(R.string.today_revenue) + " " + "¥" + " " + df.format(m));
            }
            else{
                mMoney.setText(getString(R.string.today_revenue) + " " + df.format(m) + " " + c);
            }
        }

        mImg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback()
                {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, final String imageFileName)
                    {
                        if (uploadComplete)
                        {
                            Singleton.preferenceEditor.putString(Constants.PICTURE, imageFileName).commit();
//                            Story17Application mApplication = (Story17Application) getActivity().getApplication();
//                            mApplication.getUser().setPicture(imageFileName);

                            mImg.setImageBitmap(BitmapHelper.getBitmap(Singleton.getExternalMediaFolderPath() + imageFileName, 1));

                            try
                            {
                                JSONObject params = new JSONObject();
                                params.put("picture", imageFileName);

                                ApiManager.updateUserInfo(getActivity(), params, new ApiManager.UpdateUserInfoCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            mImg.setImageBitmap(BitmapHelper.getBitmap(Singleton.getExternalMediaFolderPath() + imageFileName, 1));
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {

                            }
                        }
                        else
                        {
                            showNetworkUnstableToast();
                        }
                    }
                });
            }
        });

        mLikecount.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(), PostLikerActivity.class);
                intent.putExtra("target_id", Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("user_id", Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        likecountImg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(), PostLikerActivity.class);
                intent.putExtra("target_id", Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("user_id", Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        mFollowers.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),ProfileFollowActivity.class);
                intent.putExtra("follower",true);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        mFollowersText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),ProfileFollowActivity.class);
                intent.putExtra("follower",true);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        mFollowing.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),ProfileFollowActivity.class);
                intent.putExtra("follower",false);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        mFollowingText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),ProfileFollowActivity.class);
                intent.putExtra("follower",false);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), ProfileEditActivity.class);
                startActivity(intent);
            }
        });

        mMoney.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),RevenueActivity.class);
                startActivity(intent);
            }
        });

        mGrid.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mNoData.setVisibility(View.GONE);
                mGrid.setImageResource(R.drawable.profile_grid_active);
                mList.setImageResource(R.drawable.profile_list);
                mLike.setImageResource(R.drawable.profile_like);

                if(mGridAdapter!=null) mGridAdapter = null;
                mGridAdapter = new GridAdapter();
                mScroll.setAdapter(mGridAdapter);

                mScroll.getRefreshableView().setNumColumns(3);

                if(mFeedModels.size()==0)
                {
                    mNoData.setVisibility(View.VISIBLE);
                }

                if(mBear!=null) mBear.removeAllViews();

                mMyPagte = 0;
                reHeaderHeight();
            }
        });

        mList.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mNoData.setVisibility(View.GONE);
                mGrid.setImageResource(R.drawable.profile_grid);
                mList.setImageResource(R.drawable.profile_list_active);
                mLike.setImageResource(R.drawable.profile_like);
                mScroll.setPullToRefreshEnabled(true);

                if(mBear!=null) mBear.removeAllViews();

                if(mFeedModels.size()==0)
                {
                    mNoData.setVisibility(View.VISIBLE);
                }

                if(mListAdapter!=null) mListAdapter = null;
                mListAdapter = new MyListAdapter(mFeedModels,true);
                mScroll.setAdapter(mListAdapter);

                mScroll.getRefreshableView().setNumColumns(1);

                mMyPagte = 1;
                reHeaderHeight();
            }
        });

        mLike.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mNoData.setVisibility(View.GONE);
                mGrid.setImageResource(R.drawable.profile_grid);
                mList.setImageResource(R.drawable.profile_list);
                mLike.setImageResource(R.drawable.profile_like_active);
                mScroll.setPullToRefreshEnabled(true);

                if(mBear!=null) mBear.removeAllViews();

                mMyPagte = 2;

                //event tracking
                try{
                    LogEventUtil.EnterProfileLikerPage(getActivity(),mApplication);
                }catch (Exception x)
                {

                }

                if(mLikMoedels.size()==0)
                {
                    mBigProgress.setVisibility(View.VISIBLE);
                    ApiManager.getLikedPost(getActivity(), targetUserID, Integer.MAX_VALUE, 18, new ApiManager.GetLikedPostsCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
                        {
                            mBigProgress.setVisibility(View.GONE);
                            if (success && feedModel!=null)
                            {
                                mLikMoedels.clear();
                                mLikMoedels.addAll(feedModel);

                                if(mLikMoedels.size()==0)
                                {
                                    mNoData.setVisibility(View.VISIBLE);
                                }

                                if(mLikeListAdapter!=null) mLikeListAdapter = null;
                                mLikeListAdapter = new LikeListAdapter(mLikMoedels,false);
                                mScroll.setAdapter(mLikeListAdapter);

                                mScroll.getRefreshableView().setNumColumns(1);

                                LikeisFetchingData = false;
                                LikenoMoreData = false;

                                if (feedModel.size() < 18)
                                {
                                    LikenoMoreData = true;
                                }
                            }
                            else {
                                try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                    Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                            reHeaderHeight();
                        }
                    });
                }
                else
                {
                    if(mLikeListAdapter!=null) mLikeListAdapter = null;
                    mLikeListAdapter = new LikeListAdapter(mLikMoedels,false);
                    mScroll.setAdapter(mLikeListAdapter);

                    mScroll.getRefreshableView().setNumColumns(1);

                    if (mLikMoedels.size() < 18)
                    {
                        LikenoMoreData = true;
                    }
                }
            }
        });

        if(mFeedModels.size()==0)
        {
            mProgress.setVisibility(View.VISIBLE);
            ApiManager.getUserPost(getActivity(), targetUserID, Integer.MAX_VALUE, 18, new ApiManager.GetUserPostCallback()
            {
                @Override
                public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
                {
                    mProgress.setVisibility(View.GONE);
                    if (success && feedModel!=null)
                    {
                        if(feedModel.size()!=0)
                        {
                            mFeedModels.clear();
                            mFeedModels.addAll(feedModel);

                            mGridAdapter = new GridAdapter();
                            mScroll.setAdapter(mGridAdapter);

                            if (feedModel.size() < 18)
                            {
                                MynoMoreData = true;
                            }
                        }
                        else
                        {
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        try
                        {
                            if(getActivity()!=null)
                            {
                                mNoData.setVisibility(View.VISIBLE);
                                try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                    Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                        catch(Exception e)
                        {

                        }
                    }
                    reHeaderHeight();
                }
            });
        }
        else
        {
            mProgress.setVisibility(View.GONE);

            if(mFeedModels!=null && mFeedModels.size()!=0)
            {
                mNoData.setVisibility(View.GONE);
                mGridAdapter = new GridAdapter();
                mScroll.setAdapter(mGridAdapter);

                if (mFeedModels.size() < 18)
                {
                    MynoMoreData = true;
                }
            }
            else
            {
                mNoData.setVisibility(View.VISIBLE);
            }
            reHeaderHeight();
        }

        reHeaderHeight();

        mFollowerLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),ProfileFollowActivity.class);
                intent.putExtra("follower", true);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        mFollowingLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),ProfileFollowActivity.class);
                intent.putExtra("follower",false);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        mLikeLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(), PostLikerActivity.class);
                intent.putExtra("target_id", Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("user_id", Singleton.preferences.getString(Constants.USER_ID, ""));
                startActivity(intent);
            }
        });

        mPresent.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(), PresentBoardActivity.class);
                intent.putExtra("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("openId", Singleton.preferences.getString(Constants.OPEN_ID, ""));
                startActivity(intent);
            }
        });

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        GorpOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

//    private void downloadAsyn(final String url, final String destFileDir)
//    {
//        downloadAsyn("http://cdn.17app.co/5c678bb8-5e90-4157-a218-a0d9b24a562a.jpg",Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/");
//
//        OkHttpClient mOkHttpClient = new OkHttpClient();
//        Request request = new Request.Builder()
//        .url(url)
//        .build();
//
//        Call call = mOkHttpClient.newCall(request);
//        call.enqueue(new Callback()
//        {
//            @Override
//            public void onFailure(final Request request, final IOException e)
//            {
//                Log.d("123", "error");
//            }
//
//            @Override
//            public void onResponse(Response response)
//            {
//                InputStream is = null;
//                byte[] buf = new byte[2048];
//                int len = 0;
//                FileOutputStream fos = null;
//                try
//                {
//                    is = response.body().byteStream();
//                    File file = new File(destFileDir, getFileName(url));
//                    fos = new FileOutputStream(file);
//                    while ((len = is.read(buf)) != -1)
//                    {
//                        fos.write(buf, 0, len);
//                    }
//                    fos.flush();
//                    Log.d("123","file : " + file.getAbsolutePath());
//                }
//                catch (IOException e)
//                {
//                    Log.d("123","error e ");
//                }
//                finally
//                {
//                    try
//                    {
//                        if (is != null) is.close();
//                    }
//                    catch (IOException e)
//                    {
//                    }
//
//                    try
//                    {
//                        if (fos != null) fos.close();
//                    }
//                    catch (IOException e)
//                    {
//                    }
//                }
//            }
//        });
//    }
//
//    private String getFileName(String path)
//    {
//        int separatorIndex = path.lastIndexOf("/");
//        return (separatorIndex < 0) ? path : path.substring(separatorIndex + 1, path.length());
//    }

    private void reHeaderHeight()
    {
        mLoadLayout.setVisibility(View.VISIBLE);
        if(mProgress.isShown() || mNoData.isShown()) mLoadLayout.setVisibility(View.VISIBLE);
        else mLoadLayout.setVisibility(View.GONE);

        mSetHeaderHeight = true;
        mHeader.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (mSetHeaderHeight) {
                    mScroll.getRefreshableView().setHeaderViewHeight(mHeader.getHeight());
                    mSetHeaderHeight = false;
                }
            }
        });
    }

    private void initTitleBar()
    {
        ((RelativeLayout) getView().findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) getView().findViewById(R.id.title_name);
        mTitle.setText(open);
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) getView().findViewById(R.id.img_left);
        img.setVisibility(View.VISIBLE);
        img.setImageResource(R.drawable.btn_profile_news_selector);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mApplication.updateUserBadge(getActivity());
                mSystem_bage.setVisibility(View.GONE);
                Intent intent = new Intent();
                intent.setClass(getActivity(),SystemNotifiActivity.class);
                startActivity(intent);
            }
        });

        ImageView btn = (ImageView) getView().findViewById(R.id.img_right);
        btn.setImageResource(R.drawable.btn_setting_selector);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),SettingActivity.class);
                startActivity(intent);
            }
        });
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getActivity().getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    public Float getMoneyConfig(String key, Float def)
    {
        SharedPreferences settings = getActivity().getSharedPreferences("settings", 0);
        return settings.getFloat(key, def);
    }

    public String getConfig(String key, String def)
    {
        SharedPreferences settings = getActivity().getSharedPreferences("settings", 0);
        return settings.getString(key, def);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        MobclickAgent.onPageStart("ProfileFragment");

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mLikeCount - lastCount > 17) {
                    mGodHand = true;
                }

                lastCount = mLikeCount;
            }
        }, 0, 1000);

        String c = Singleton.getCurrencyType();
        double m = (double) getMoneyConfig(Constants.REVENUE_TODAY, 0f) *(double)Singleton.getCurrencyRate();
        DecimalFormat df=new DecimalFormat("#.####");
        if(Constants.INTERNATIONAL_VERSION){
            mMoney.setText(getString(R.string.today_revenue) + " " + df.format(m) + " " + c);
        }
        else{
            if(c.compareTo("CNY")==0){
                mMoney.setText(getString(R.string.today_revenue) + " " + "¥" + " " + df.format(m));
            }
            else{
                mMoney.setText(getString(R.string.today_revenue) + " " + df.format(m) + " " + c);
            }
        }

        if(mApplication!=null)
        {
            if(mApplication.getLogout())
            {
                mApplication.setLogout(false);

                SharedPreferences Mysettings = getActivity().getSharedPreferences("settings", 0);
                Mysettings.edit().clear().commit();

                Singleton.preferences.edit().clear().commit();

                Singleton.preferenceEditor.putInt(Constants.LOGOUT, 1).commit();

                mApplication.setSignOutState(true);
                Intent intent = new Intent();
                intent.setClass(getActivity(),GuestActivity.class);
                intent.putExtra("sign",true);
                startActivity(intent);

                getActivity().finish();
            }
        }

        if(mFollowers!=null && mFollowing!=null && mLikecount!=null && mOpen != null && mWeb!= null && mDio != null && mVerifie!=null)
        {
            ApiManager.getSelfInfo(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), new ApiManager.GetSelfInfoCallback()
            {
                @Override
                public void onResult(boolean success, String message, UserModel userModel)
                {
                    if(success && userModel!=null)
                    {
                        try
                        {
                            mFollowers.setText(String.valueOf(userModel.getFollowerCount()));
                            mFollowing.setText(String.valueOf(userModel.getFollowingCount()));
                            mLikecount.setText(String.valueOf(userModel.getReceivedLikeCount()));

                            mOpen.setText(String.valueOf(userModel.getName()));

                            if(String.valueOf(userModel.getWebsite()).length()!=0)
                            {
                                mWeb.setText(String.valueOf(userModel.getWebsite()));
                                mWeb.setVisibility(View.VISIBLE);
                            }
                            else mWeb.setVisibility(View.GONE);

                            if(String.valueOf(userModel.getBio()).length()!=0)
                            {
                                mDio.setText(String.valueOf(userModel.getBio()));
                                mDio.setVisibility(View.VISIBLE);
                            }
                            else mDio.setVisibility(View.GONE);

                            if(userModel.getIsVerified()==1)
                            {
                                mVerifie.setVisibility(View.VISIBLE);
                                mApplication.isVerifie = true;
                            }
                            else
                            {
                                mVerifie.setVisibility(View.GONE);
                                mApplication.isVerifie = false;
                            }

                            reHeaderHeight();

                            ((TextView) getView().findViewById(R.id.title_name)).setText(Singleton.preferences.getString(Constants.OPEN_ID, ""));

                            if(userModel.getNotifBadge()!=0 || userModel.getSystemNotifBadge()!=0 || userModel.getMessageBadge()!=0)
                            {
//                                ((MenuActivity)getActivity()).mBadge.setVisibility(View.VISIBLE);
                            }

                            if(userModel.getPrivacyMode().compareTo("private")==0) setConfig(Constants.SETTING_PRIVATE_MODE,"1");
                            else setConfig(Constants.SETTING_PRIVATE_MODE,"0");

                            if(userModel.getFollowPrivacyMode()==1) setConfig(Constants.SETTING_FOLLOW_MODE, "1");
                            else setConfig(Constants.SETTING_FOLLOW_MODE, "0");

                            Singleton.preferenceEditor.putInt(Constants.IS_ADMIN_V2, userModel.getIsAdmin()).commit();
                            Singleton.preferenceEditor.putInt(Constants.FOLLOWING_COUNT_V2, userModel.getFollowingCount()).commit();
                            Singleton.preferenceEditor.putInt(Constants.SHOW_AD, userModel.getAdsOn()).commit();

                            Singleton.preferenceEditor.putString(Constants.COUNTRY_CALLING_CODE, userModel.getCountryCallingCode()).commit();
                            Singleton.preferenceEditor.putString(Constants.LOCAL_PHONE_NUMBER, userModel.getLocalPhoneNumber()).commit();
                            Singleton.preferenceEditor.putString(Constants.PHONE_TWO_DIGIT_ISO, userModel.getPhoneTwoDigitISO()).commit();

                            if(userModel.getVerifiedPhoneNumber().length()!=0) Singleton.preferenceEditor.putString(Constants.VERIFIED_PHONE_NUMBER, userModel.getVerifiedPhoneNumber()).commit();
                            else
                            {
                                if(Singleton.preferences.getString(Constants.VERIFIED_PHONE_NUMBER, "").length()!=0)
                                {
                                    mApplication.updateVerifiedPhoneNumber(getActivity(),Singleton.preferences.getString(Constants.VERIFIED_PHONE_NUMBER, ""));
                                }
                            }

                            if(userModel.getSystemNotifBadge()!=0) mSystem_bage.setVisibility(View.VISIBLE);
                            else mSystem_bage.setVisibility(View.GONE);

                            try {
                                Singleton.preferenceEditor.putInt(Constants.GIFT_MODULE_STATE, userModel.getGiftModuleState()).commit();
                                Singleton.preferenceEditor.putInt(Constants.HAS_SHOWN_GIFT_MODULE_TUTORIAL, userModel.getHasShownGiftModuleTutorial()).commit();
                                Singleton.preferenceEditor.putString(Constants.APP_UPDATE_LINK, userModel.getAppUpdateLink()).commit();
                                Singleton.preferenceEditor.putInt(Constants.FORCE_UPDATE_APP, userModel.getForceUpdateApp()).commit();
                                Singleton.preferenceEditor.putString(Constants.LATEST_APP_VERSION, userModel.getLatestAppVersion()).commit();
                            }
                            catch(Exception e){
                            }

                            Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, userModel.getPoint()).commit();

                            setConfig(Constants.REVENUE_PAYPAL, userModel.getPaypalEmail());
                            setConfig(Constants.REVENUE_ALIPAY,userModel.getAlipayPhone());

                            setConfig(Constants.PROFILE_POST_COUNT, String.valueOf(userModel.getPostCount()));
                            setConfig(Constants.PROFILE_FOLLOWER_COUNT,String.valueOf(userModel.getFollowerCount()));
                            setConfig(Constants.PROFILE_FOLLOWING_COUNT,String.valueOf(userModel.getFollowingCount()));
                            setConfig(Constants.PROFILE_LIKE_COUNT,String.valueOf(userModel.getReceivedLikeCount()));
                            setConfig(Constants.PROFILE_NAME,String.valueOf(userModel.getName()));
                            setConfig(Constants.PROFILE_WEB,String.valueOf(userModel.getWebsite()));
                            setConfig(Constants.PROFILE_BIO,String.valueOf(userModel.getBio()));

                            setConfig(Constants.PROFILE_SEX,String.valueOf(userModel.getGender()));
                            setConfig(Constants.PROFILE_AGE,String.valueOf(userModel.getAge()));
                            setConfig(Constants.PROFILE_EMAIL,String.valueOf(userModel.getEmail()));
                            setConfig(Constants.PROFILE_COUNTRY_CODE,String.valueOf(userModel.getCountryCode()));
                            setConfig(Constants.PROFILE_PHONE,String.valueOf(userModel.getPhoneNumber()));

                            if(userModel.getIsFreezed()==1)
                            {
                                File mFileSetting = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/");
                                mFileSetting.mkdir();

                                File mFileUser = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/");
                                mFileUser.mkdir();

                                File mFileFreezed = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/");
                                mFileFreezed.mkdir();

                                File mFileIs = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/is/");
                                mFileIs.mkdir();

                                File mFileLogin = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.setting/user/.freezed/is/17.login");
                                mFileLogin.mkdir();

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(getString(R.string.your_account_freezed));
                                builder.setTitle(getString(R.string.system_notif));
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        getActivity().finish();
                                    }
                                }).setCancelable(false);

                                builder.create().show();
                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            });
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if(mBear!=null) mBear.removeAllViews();

        if(mMyVideoLists!=null && mMyVideoLists.size()!=0)
        {
            for(int i = 0 ; i < mMyVideoLists.size() ; i++)
            {
                if(mMyVideoLists.get(i).isPlaying())
                {
                    mMyVideoLists.get(i).stopPlayback();
                }
            }
        }

        if(mLikeVideoLists!=null && mLikeVideoLists.size()!=0)
        {
            for(int i = 0 ; i < mLikeVideoLists.size() ; i++)
            {
                if(mLikeVideoLists.get(i).isPlaying())
                {
                    mLikeVideoLists.get(i).stopPlayback();
                }
            }
        }

        if(mLikeCount!=0)
        {
            if(mApplication!=null)
            {
                if(mLikePosUser.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                {
                    mApplication.sendLikeCountV2(getActivity(),mLikePos,mLikeCount,mGodHand);

                    mLikePos = "";
                    mLikeCount = 0;
                    mLikePosUser = "";
                }
            }
        }

        if(mTimer!=null)
        {
            mLikePosUser = "";
            mLikeCount = 0;
            mLikePos = "";
            lastCount = 0;
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }

        MobclickAgent.onPageEnd("ProfileFragment");
    }

    private class GridAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.grid_row, null);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.video = (ImageView) convertView.findViewById(R.id.video);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.image.getLayoutParams().width = mGridHeight;
            holder.image.getLayoutParams().height = mGridHeight;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX +mFeedModels.get(position).getPicture()), holder.image,GorpOptions);
            holder.image.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), PhotoActivity.class);
                    intent.putExtra("photo", mFeedModels.get(position).getPicture());
                    intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                    intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                    intent.putExtra("day", Singleton.getElapsedTimeString(mFeedModels.get(position).getTimestamp()));
                    intent.putExtra("view", mFeedModels.get(position).getViewCount());
                    intent.putExtra("money", mFeedModels.get(position).getTotalRevenue());
                    intent.putExtra("dio", mFeedModels.get(position).getCaption());
                    intent.putExtra("likecount", mFeedModels.get(position).getLikeCount());
                    intent.putExtra("commentcount", mFeedModels.get(position).getCommentCount());
                    intent.putExtra("likeed", mFeedModels.get(position).getLiked());

                    intent.putExtra("postid", mFeedModels.get(position).getPostID());
                    intent.putExtra("userid", mFeedModels.get(position).getUserID());

                    intent.putExtra("name", mFeedModels.get(position).getUserInfo().getName());
                    intent.putExtra("isFollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                    intent.putExtra("postCount", mFeedModels.get(position).getUserInfo().getPostCount());
                    intent.putExtra("followerCount", mFeedModels.get(position).getUserInfo().getFollowerCount());
                    intent.putExtra("followingCount", mFeedModels.get(position).getUserInfo().getFollowingCount());
                    intent.putExtra("goto", false);
                    intent.putExtra("type", mFeedModels.get(position).getType());
                    intent.putExtra("video", mFeedModels.get(position).getVideo());
                    startActivity(intent);
                }
            });

            if(mFeedModels.get(position).getType().compareTo("image")==0) holder.video.setVisibility(View.GONE);
            else holder.video.setVisibility(View.VISIBLE);

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView image;
        ImageView video;
    }

    private class MyListAdapter extends BaseAdapter
    {
        private ArrayList<FeedModel> mFeedAdapterArray = new ArrayList<FeedModel>();
        private Boolean mSelf;

        public MyListAdapter(ArrayList<FeedModel> models,Boolean self)
        {
            mFeedAdapterArray.clear();
            mFeedAdapterArray.addAll(models);

            mSelf = self;
        }

        public void addModel(ArrayList<FeedModel> models)
        {
            mFeedAdapterArray.clear();
            mFeedAdapterArray.addAll(models);
        }

        @Override
        public int getCount()
        {
            return mFeedAdapterArray.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ListViewHolder holder = new ListViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ListViewHolder) convertView.getTag();
            }

            if(mFeedAdapterArray.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.down_layout.setVisibility(View.VISIBLE);
            holder.live.setVisibility(View.GONE);

            holder.name.setText(mFeedAdapterArray.get(position).getUserInfo().getOpenID());
            holder.day.setText(Singleton.getElapsedTimeString(mFeedAdapterArray.get(position).getTimestamp()));

            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.money_text.setVisibility(View.VISIBLE);

                String c = Singleton.getCurrencyType();
                double m = (double) mFeedAdapterArray.get(position).getTotalRevenue();
                DecimalFormat df=new DecimalFormat("#.####");
                m = m * Singleton.getCurrencyRate();
                holder.money_text.setText(df.format(m) + " " + c);
                holder.view_text.setText(String.format(getString(R.string.home_views), String.valueOf(mFeedAdapterArray.get(position).getViewCount())));
                holder.view_text.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.money_text.setVisibility(View.GONE);
                holder.view_text.setVisibility(View.GONE);
            }

            if(mFeedAdapterArray.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mFeedAdapterArray.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.GONE);
            holder.like_text.setText(String.format(getString(R.string.home_like),String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
            holder.comment_text.setText(String.format(getString(R.string.home_comment),String.valueOf(mFeedAdapterArray.get(position).getCommentCount())));

            final String thisUserId = mFeedAdapterArray.get(position).getUserID();

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.like_text.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                        intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                        startActivity(intent);
                    }
                });
            }
            else
            {
                holder.like_text.setOnClickListener(null);
            }

            holder.comment_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    mCimmentPos = position;
//                    mCommentCount = true;
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                    intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(mFeedAdapterArray.get(position).getLiked()==0)
            {
                holder.btn_like.setImageResource(R.drawable.btn_like_selector);
            }
            else holder.btn_like.setImageResource(R.drawable.like_down);

            final TextView likeText = holder.like_text;
            final ImageView like = holder.btn_like;
            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                        intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                        startActivity(intent);
                    }
                });
                holder.btn_like.setOnTouchListener(null);
            }
            else
            {
                holder.btn_like.setOnClickListener(null);
                holder.btn_like.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            mY = event.getY();
                            mX = event.getX();
                        }

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            double num = Math.sqrt(Math.pow(((double) mX - (double) event.getX()), 2) + Math.pow(((double) mY - (double) event.getY()), 2));

                            if (num < 30) {
                                int run = (int) (Math.random() * 10000);
                                int img;
                                int mW, mH;

                                if (run < 500) {
                                    img = logos[(int) (Math.random() * logos.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 1000) {
                                    img = rabbits[(int) (Math.random() * rabbits.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 2000) {
                                    img = cats[(int) (Math.random() * cats.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 3000) {
                                    img = bears[(int) (Math.random() * bears.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 4000) {
                                    img = bubbles[(int) (Math.random() * bubbles.length)];
                                    mW = 120;
                                    mH = 120;
                                } else {
                                    img = loves[(int) (Math.random() * loves.length)];
                                    mW = 60;
                                    mH = 60;
                                }

                                int mS = -30 + ((int) (Math.random() * 60));
                                int mTx = -100 + ((int) (Math.random() * 200));
                                int mTy = 500 + ((int) (Math.random() * 1200));

                                ImageView cat = new ImageView(getActivity());
                                cat.setImageResource(img);
                                cat.setVisibility(View.INVISIBLE);

                                RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                cat.setLayoutParams(mParams);

                                AnimationSet mAnimationSet = new AnimationSet(true);

                                ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                scale.setDuration(500);

                                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                alpha.setDuration(3000);

                                RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                rotate.setDuration(750);

                                TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                translate.setDuration(2000);

                                mAnimationSet.addAnimation(scale);
                                mAnimationSet.addAnimation(rotate);
                                mAnimationSet.addAnimation(translate);
                                mAnimationSet.addAnimation(alpha);
                                cat.startAnimation(mAnimationSet);

                                mBear.addView(cat);

                                mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                                if (mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                    {
                                        mLikeCount++;
                                    }
                                    else
                                    {
                                        if(mLikeCount!=0)
                                        {
                                            mApplication.sendLikeCountV2(getActivity(),mLikePos,mLikeCount,mGodHand);
                                        }

                                        mLikePos = mFeedAdapterArray.get(position).getPostID();
                                        mLikeCount = 1;
                                    }
//                                    ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                        @Override
//                                        public void onResult(boolean success, String message) {
//                                            if (success) {
//
//                                            }
//                                        }
//                                    });
                                } else {
                                    if (mFeedAdapterArray.get(position).getLiked() != 1) {
                                        mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                        likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                        mFeedAdapterArray.get(position).setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        if(!mApplication.getIsSbtools())
                                        {
                                            ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }

                        return true;
                    }
                });
            }

            holder.btn_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mCimmentPos = position;
//                    mCommentCount = true;
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                    intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_more.setVisibility(View.VISIBLE);
                holder.btn_more.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) mListMoreText[0] = getString(R.string.post_delete);
                        else mListMoreText[0] = getString(R.string.post_block);

                        mListMoreText[1] = getString(R.string.post_share);

                        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(which == 0)
                                {
                                    if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
                                    {
                                        ApiManager.deletePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.DeletePostCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success, String message)
                                            {
                                                if (success)
                                                {
                                                    mBigProgress.setVisibility(View.VISIBLE);

                                                    ApiManager.getUserPost(getActivity(), targetUserID, Integer.MAX_VALUE, 18, new ApiManager.GetUserPostCallback()
                                                    {
                                                        @Override
                                                        public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
                                                        {
                                                            mBigProgress.setVisibility(View.GONE);

                                                            if (!isAdded()) {
                                                                return;
                                                            }

                                                            if (success && feedModel != null)
                                                            {
                                                                if (feedModel.size() != 0)
                                                                {
                                                                    mNoData.setVisibility(View.GONE);

                                                                    MyisFetchingData = false;
                                                                    MynoMoreData = false;

                                                                    mFeedModels.clear();
                                                                    mFeedModels.addAll(feedModel);

                                                                    mGrid.setImageResource(R.drawable.profile_grid);
                                                                    mList.setImageResource(R.drawable.profile_list_active);
                                                                    mLike.setImageResource(R.drawable.profile_like);

                                                                    if (mListAdapter != null) mListAdapter = null;
                                                                    mListAdapter = new MyListAdapter(mFeedModels,true);
                                                                    mScroll.setAdapter(mListAdapter);

                                                                    mMyPagte = 1;
                                                                    mScroll.getRefreshableView().setNumColumns(1);

                                                                    if (feedModel.size() < 18) {
                                                                        MynoMoreData = true;
                                                                    }
                                                                }
                                                                reHeaderHeight();
                                                            }
                                                        }
                                                    });
                                                }
                                                else {
                                                    try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        //檢舉
                                    }
                                }
                                else
                                {
                                    if(mFeedAdapterArray.get(position).getType().compareTo("image")==0)
                                    {
                                        mBigProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(getActivity()).getContentUrl("facebook", BranchSharePostData(mFeedAdapterArray.get(position).getPostID(), R.string.share_post_image, mFeedAdapterArray.get(position).getUserInfo().getOpenID(), mFeedAdapterArray.get(position).getCaption(), mFeedAdapterArray.get(position).getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mBigProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedAdapterArray.get(position).getCaption() + "  " + url;

                                                try {
                                                    LogEventUtil.SharePost(getActivity(), mApplication, mFeedAdapterArray.get(position).getUserID(), mFeedAdapterArray.get(position).getPostID(), "none");
                                                }catch (Exception x)
                                                {

                                                }

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedAdapterArray.get(position).getPicture());
                                            }
                                        });
                                    }
                                    else
                                    {
                                        mBigProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(getActivity()).getContentUrl("facebook", BranchSharePostData(mFeedAdapterArray.get(position).getPostID(), R.string.share_post_video, mFeedAdapterArray.get(position).getUserInfo().getOpenID(), mFeedAdapterArray.get(position).getCaption(), mFeedAdapterArray.get(position).getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mBigProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedAdapterArray.get(position).getCaption() + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedAdapterArray.get(position).getPicture());
                                            }
                                        });
                                    }
                                }
                            }
                        }).show();
                    }
                });
            }
            else
            {
                holder.btn_more.setVisibility(View.GONE);
            }

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedAdapterArray.get(position).getUserInfo().getPicture()), holder.self, SelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeUserActivity.class);
                        intent.putExtra("title", mFeedAdapterArray.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedAdapterArray.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedAdapterArray.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedAdapterArray.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedAdapterArray.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedAdapterArray.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedAdapterArray.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedAdapterArray.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedAdapterArray.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeUserActivity.class);
                        intent.putExtra("title", mFeedAdapterArray.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedAdapterArray.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedAdapterArray.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedAdapterArray.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedAdapterArray.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedAdapterArray.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedAdapterArray.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedAdapterArray.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedAdapterArray.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            if(mSelf)
            {
                if(MyNowPos >= (position+2) || MyNowPos <= (position-2))
                {
                    for(int p = 0 ; p < mMyVideoLists.size() ; p++)
                    {
                        if(mMyVideoLists.get(p)!=null)
                        {
                            if(mMyVideoLists.get(p).isPlaying())
                            {
                                mMyVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mMyVideoLists.clear();
                }
            }
            else
            {
                if(LikeNowPos >= (position+2) || LikeNowPos <= (position-2))
                {
                    for(int p = 0 ; p < mLikeVideoLists.size() ; p++)
                    {
                        if(mLikeVideoLists.get(p)!=null)
                        {
                            if(mLikeVideoLists.get(p).isPlaying())
                            {
                                mLikeVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mLikeVideoLists.clear();
                }
            }

            holder.photo.setOnClickListener(null);
            if(mFeedAdapterArray.get(position).getType().compareTo("image")==0)
            {
                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.GONE);
                holder.vcon.setVisibility(View.GONE);

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }

                holder.photo.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(getActivity());
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                            {

                                mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                mFeedAdapterArray.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                {
                                    mLikeCount++;
                                }
                                else
                                {
                                    if(mLikeCount!=0)
                                    {
                                        mApplication.sendLikeCountV2(getActivity(),mLikePos,mLikeCount,mGodHand);
                                    }

                                    mLikePos = mFeedAdapterArray.get(position).getPostID();
                                    mLikeCount = 1;
                                }
//                                ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            }
                            else
                            {
                                if(mFeedAdapterArray.get(position).getLiked()!=1)
                                {
                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }
            else
            {
                if(mSelf)
                {
                    MyNowPos = position;
                    mMyVideoLists.add(holder.video);
                }
                else
                {
                    LikeNowPos = position;
                    mLikeVideoLists.add(holder.video);
                }

                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.VISIBLE);
                holder.vcon.setVisibility(View.VISIBLE);

                holder.video.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.video.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }

                final ImageView mPhotoV = holder.photo;
                holder.video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mPhotoV.setVisibility(View.GONE);
                    }
                });

                final FastVideoView mVideoV = holder.video;
                holder.video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mVideoV.start();
                    }
                });

                if(mVideoPlayState)
                {
                    holder.video.setVideoPath(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getVideo()));
                    holder.video.start();
                }

                holder.video.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(getActivity());
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                            {

                                mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                mFeedAdapterArray.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                {
                                    mLikeCount++;
                                }
                                else
                                {
                                    if(mLikeCount!=0)
                                    {
                                        mApplication.sendLikeCountV2(getActivity(),mLikePos,mLikeCount,mGodHand);
                                    }

                                    mLikePos = mFeedAdapterArray.get(position).getPostID();
                                    mLikeCount = 1;
                                }
//                                ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            }
                            else
                            {
                                if(mFeedAdapterArray.get(position).getLiked()!=1)
                                {
                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }

            holder.dio.linkify(new FeedTagActionHandler()
            {
                @Override
                public void handleHashtag(String hashtag)
                {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention)
                {
                    mBigProgress.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(getActivity(), mention, new ApiManager.GetUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, UserModel user)
                        {
                            mBigProgress.setVisibility(View.GONE);
                            if (success && user != null)
                            {
                                Intent intent = new Intent();
                                intent.setClass(getActivity(), HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                startActivity(intent);
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email)
                {

                }

                @Override
                public void handleUrl(String url)
                {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.open_uri_error));
                            Toast.makeText(getActivity(), getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            if(mSelf)
            {
                if(position>=getCount()-5)
                {
                    LoadData(false);
                }
            }
            else
            {
                if(position>=getCount()-5)
                {
                    LoadDataLike(false);
                }
            }


            return convertView;
        }
    }

    private void shareTo(String subject, String body, String chooserTitle) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    private void shareTo(String subject, String body, String chooserTitle, String pic)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
//        String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
//        File imageFileToShare = new File(imagePath);
//        Uri uri = Uri.fromFile(imageFileToShare);
//        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    private class ListViewHolder
    {
        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
    }

    private class LikeListAdapter extends BaseAdapter
    {
        private ArrayList<FeedModel> mFeedAdapterArray = new ArrayList<FeedModel>();
        private Boolean mSelf;

        public LikeListAdapter(ArrayList<FeedModel> models,Boolean self)
        {
            mFeedAdapterArray.clear();
            mFeedAdapterArray.addAll(models);

            mSelf = self;
        }

        @Override
        public int getCount()
        {
            return mFeedAdapterArray.size();
        }

        public void addModel(ArrayList<FeedModel> models)
        {
            mFeedAdapterArray.clear();
            mFeedAdapterArray.addAll(models);
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ListViewHolderLike holder = new ListViewHolderLike();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ListViewHolderLike) convertView.getTag();
            }

            if(mFeedAdapterArray.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.down_layout.setVisibility(View.VISIBLE);
            holder.live.setVisibility(View.GONE);

            holder.name.setText(mFeedAdapterArray.get(position).getUserInfo().getOpenID());
            holder.day.setText(Singleton.getElapsedTimeString(mFeedAdapterArray.get(position).getTimestamp()));

            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.money_text.setVisibility(View.VISIBLE);

                String c = Singleton.getCurrencyType();
                double m = (double) mFeedAdapterArray.get(position).getTotalRevenue()*(double)Singleton.getCurrencyRate();
                DecimalFormat df=new DecimalFormat("#.####");
                holder.money_text.setText(df.format(m) + " " + c);
                holder.view_text.setText(String.format(getString(R.string.home_views), String.valueOf(mFeedAdapterArray.get(position).getViewCount())));
                holder.view_text.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.money_text.setVisibility(View.GONE);
                holder.view_text.setVisibility(View.GONE);
            }

            if(mFeedAdapterArray.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mFeedAdapterArray.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.GONE);
            holder.like_text.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
            holder.comment_text.setText(String.format(getString(R.string.home_comment), String.valueOf(mFeedAdapterArray.get(position).getCommentCount())));

            final String thisUserId = mFeedAdapterArray.get(position).getUserID();

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.like_text.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                        intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                        startActivity(intent);
                    }
                });
            }
            else
            {
                holder.like_text.setOnClickListener(null);
            }

            holder.comment_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    mCimmentPos = position;
//                    mCommentCount = true;
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                    intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(mFeedAdapterArray.get(position).getLiked()==0)
            {
                holder.btn_like.setImageResource(R.drawable.btn_like_selector);
            }
            else holder.btn_like.setImageResource(R.drawable.like_down);

            final TextView likeText = holder.like_text;
            final ImageView like = holder.btn_like;
            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                        intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                        startActivity(intent);
                    }
                });
                holder.btn_like.setOnTouchListener(null);
            }
            else
            {
                holder.btn_like.setOnClickListener(null);
                holder.btn_like.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            mY = event.getY();
                            mX = event.getX();
                        }

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            double num = Math.sqrt(Math.pow(((double) mX - (double) event.getX()), 2) + Math.pow(((double) mY - (double) event.getY()), 2));

                            if (num < 30) {
                                int run = (int) (Math.random() * 10000);
                                int img;
                                int mW, mH;

                                if (run < 500) {
                                    img = logos[(int) (Math.random() * logos.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 1000) {
                                    img = rabbits[(int) (Math.random() * rabbits.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 2000) {
                                    img = cats[(int) (Math.random() * cats.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 3000) {
                                    img = bears[(int) (Math.random() * bears.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 4000) {
                                    img = bubbles[(int) (Math.random() * bubbles.length)];
                                    mW = 120;
                                    mH = 120;
                                } else {
                                    img = loves[(int) (Math.random() * loves.length)];
                                    mW = 60;
                                    mH = 60;
                                }

                                int mS = -30 + ((int) (Math.random() * 60));
                                int mTx = -100 + ((int) (Math.random() * 200));
                                int mTy = 500 + ((int) (Math.random() * 1200));

                                ImageView cat = new ImageView(getActivity());
                                cat.setImageResource(img);
                                cat.setVisibility(View.INVISIBLE);

                                RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                cat.setLayoutParams(mParams);

                                AnimationSet mAnimationSet = new AnimationSet(true);

                                ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                scale.setDuration(500);

                                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                alpha.setDuration(3000);

                                RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                rotate.setDuration(750);

                                TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                translate.setDuration(2000);

                                mAnimationSet.addAnimation(scale);
                                mAnimationSet.addAnimation(rotate);
                                mAnimationSet.addAnimation(translate);
                                mAnimationSet.addAnimation(alpha);
                                cat.startAnimation(mAnimationSet);

                                mBear.addView(cat);

                                mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                                if (mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                    {
                                        mLikeCount++;
                                    }
                                    else
                                    {
                                        if(mLikeCount!=0)
                                        {
                                            mApplication.sendLikeCountV2(getActivity(),mLikePos,mLikeCount,mGodHand);
                                        }

                                        mLikePos = mFeedAdapterArray.get(position).getPostID();
                                        mLikeCount = 1;
                                    }
//                                    ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                        @Override
//                                        public void onResult(boolean success, String message) {
//                                            if (success) {
//
//                                            }
//                                        }
//                                    });
                                } else {
                                    if (mFeedAdapterArray.get(position).getLiked() != 1) {
                                        mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                        likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                        mFeedAdapterArray.get(position).setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        if(!mApplication.getIsSbtools())
                                        {
                                            ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }

                        return true;
                    }
                });
            }

            holder.btn_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mCimmentPos = position;
//                    mCommentCount = true;
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                    intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_more.setVisibility(View.VISIBLE);
                holder.btn_more.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) mListMoreText[0] = getString(R.string.post_delete);
                        else mListMoreText[0] = getString(R.string.post_block);

                        mListMoreText[1] = getString(R.string.post_share);

                        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(which == 0)
                                {
                                    if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
                                    {
                                        ApiManager.deletePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.DeletePostCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success, String message)
                                            {
                                                if (success)
                                                {
                                                    mBigProgress.setVisibility(View.VISIBLE);

                                                    ApiManager.getUserPost(getActivity(), targetUserID, Integer.MAX_VALUE, 18, new ApiManager.GetUserPostCallback()
                                                    {
                                                        @Override
                                                        public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
                                                        {
                                                            mBigProgress.setVisibility(View.GONE);

                                                            if (!isAdded()) {
                                                                return;
                                                            }

                                                            if (success && feedModel != null)
                                                            {
                                                                if (feedModel.size() != 0)
                                                                {
                                                                    mNoData.setVisibility(View.GONE);

                                                                    MyisFetchingData = false;
                                                                    MynoMoreData = false;

                                                                    mFeedModels.clear();
                                                                    mFeedModels.addAll(feedModel);

                                                                    mGrid.setImageResource(R.drawable.profile_grid);
                                                                    mList.setImageResource(R.drawable.profile_list_active);
                                                                    mLike.setImageResource(R.drawable.profile_like);

                                                                    if (mListAdapter != null) mListAdapter = null;
                                                                    mListAdapter = new MyListAdapter(mFeedModels,true);
                                                                    mScroll.setAdapter(mListAdapter);

                                                                    mMyPagte = 1;
                                                                    mScroll.getRefreshableView().setNumColumns(1);

                                                                    if (feedModel.size() < 18) {
                                                                        MynoMoreData = true;
                                                                    }
                                                                }
                                                                reHeaderHeight();
                                                            }
                                                        }
                                                    });
                                                }
                                                else {
                                                    try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        //檢舉
                                    }
                                }
                                else
                                {
                                    shareTo(getString(R.string.share_subject), getString(R.string.share_body1) + mFeedAdapterArray.get(position).getUserInfo().getOpenID() + getString(R.string.share_body2) + " " + Constants.SHARE_LINK_APP, getString(R.string.share_title));
                                }
                            }
                        }).show();
                    }
                });
            }
            else
            {
                holder.btn_more.setVisibility(View.VISIBLE);
                holder.btn_more.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(mLikeMoreText!=null) mLikeMoreText = null;

                        if(mFeedAdapterArray.get(position).getLiked()==0) mLikeMoreText = new String[2];
                        else
                        {
                            mLikeMoreText = new String[3];
                            mLikeMoreText[2] = getString(R.string.back_like);
                        }

                        if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) mLikeMoreText[0] = getString(R.string.post_delete);
                        else mLikeMoreText[0] = getString(R.string.post_block);

                        mLikeMoreText[1] = getString(R.string.post_share);

                        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.more)).setItems(mLikeMoreText, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(which == 0)
                                {
                                    if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
                                    {

                                    }
                                    else
                                    {
                                        //檢舉
                                        ArrayList<String> strArray  = new ArrayList<String>();
                                        strArray =  Singleton.getLocalizeStringOfArray(Constants.REPORT_POST_OPTION);
                                        String[] reportString = strArray.toArray(new String[strArray.size()]);

                                        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.report_reason)).setItems(reportString, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                String reason = Singleton.getLocalizedString(Constants.REPORT_POST_OPTION.get(which));
                                                try {
                                                    LogEventUtil.ReportPost(getActivity(), mApplication);
                                                }
                                                catch (Exception x)
                                                {

                                                }

                                                ApiManager.reportPostAction(getActivity(),  mFeedAdapterArray.get(position).getUserID(), reason, mFeedAdapterArray.get(position).getPostID(), new ApiManager.ReportPostActionCallback() {

                                                    @Override
                                                    public void onResult(boolean success, String message) {
                                                        if(success){
                                                            try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.complete));
                                                                Toast.makeText(getActivity(), getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                                            }
                                                            catch (Exception x){
                                                            }
                                                        }else{
                                                            try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                                                Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                            }
                                                            catch (Exception x){
                                                            }
                                                        }
                                                    }
                                                });


                                            }
                                        }).show();
                                    }
                                }
                                else if(which == 1)
                                {
                                    if(mFeedAdapterArray.get(position).getType().compareTo("image")==0)
                                    {
                                        mBigProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(getActivity()).getContentUrl("facebook", BranchSharePostData(mFeedAdapterArray.get(position).getPostID(), R.string.share_post_image, mFeedAdapterArray.get(position).getUserInfo().getOpenID(), mFeedAdapterArray.get(position).getCaption(), mFeedAdapterArray.get(position).getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mBigProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedAdapterArray.get(position).getCaption() + "  " + url;

                                                try {
                                                    LogEventUtil.SharePost(getActivity(), mApplication, mFeedAdapterArray.get(position).getUserID(), mFeedAdapterArray.get(position).getPostID(), "none");
                                                }catch (Exception x)
                                                {

                                                }

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedAdapterArray.get(position).getPicture());
                                            }
                                        });
                                    }
                                    else
                                    {
                                        mBigProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(getActivity()).getContentUrl("facebook", BranchSharePostData(mFeedAdapterArray.get(position).getPostID(), R.string.share_post_video, mFeedAdapterArray.get(position).getUserInfo().getOpenID(), mFeedAdapterArray.get(position).getCaption(), mFeedAdapterArray.get(position).getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mBigProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedAdapterArray.get(position).getCaption() + "  " + url;

                                                try {
                                                    LogEventUtil.SharePost(getActivity(), mApplication, mFeedAdapterArray.get(position).getUserID(), mFeedAdapterArray.get(position).getPostID(), "none");
                                                }catch (Exception x)
                                                {

                                                }

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedAdapterArray.get(position).getPicture());
                                            }
                                        });
                                    }
                                }
                                else if(which == 2)
                                {
                                    mFeedAdapterArray.get(position).setLiked(0);
                                    like.setImageResource(R.drawable.like);
                                    ApiManager.unlikePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.UnlikePostCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message)
                                        {
                                            if (success)
                                            {
                                                if (mFeedAdapterArray != null)
                                                {
                                                    if(mFeedAdapterArray.size()>position)
                                                    {
                                                        mFeedAdapterArray.remove(position);
                                                    }
                                                }

                                                if (mLikeListAdapter != null) mLikeListAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    });
                                }
                            }
                        }).show();
                    }
                });
            }

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedAdapterArray.get(position).getUserInfo().getPicture()), holder.self, SelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeUserActivity.class);
                        intent.putExtra("title", mFeedAdapterArray.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedAdapterArray.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedAdapterArray.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedAdapterArray.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedAdapterArray.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedAdapterArray.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedAdapterArray.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedAdapterArray.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedAdapterArray.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeUserActivity.class);
                        intent.putExtra("title", mFeedAdapterArray.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedAdapterArray.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedAdapterArray.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedAdapterArray.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedAdapterArray.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedAdapterArray.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedAdapterArray.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedAdapterArray.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedAdapterArray.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            if(mSelf)
            {
                if(MyNowPos >= (position+2) || MyNowPos <= (position-2))
                {
                    for(int p = 0 ; p < mMyVideoLists.size() ; p++)
                    {
                        if(mMyVideoLists.get(p)!=null)
                        {
                            if(mMyVideoLists.get(p).isPlaying())
                            {
                                mMyVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mMyVideoLists.clear();
                }
            }
            else
            {
                if(LikeNowPos >= (position+2) || LikeNowPos <= (position-2))
                {
                    for(int p = 0 ; p < mLikeVideoLists.size() ; p++)
                    {
                        if(mLikeVideoLists.get(p)!=null)
                        {
                            if(mLikeVideoLists.get(p).isPlaying())
                            {
                                mLikeVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mLikeVideoLists.clear();
                }
            }

            holder.photo.setOnClickListener(null);
            if(mFeedAdapterArray.get(position).getType().compareTo("image")==0)
            {
                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.GONE);
                holder.vcon.setVisibility(View.GONE);

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }

                holder.photo.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(getActivity());
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                            {

                                mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                mFeedAdapterArray.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                {
                                    mLikeCount++;
                                }
                                else
                                {
                                    if(mLikeCount!=0)
                                    {
                                        mApplication.sendLikeCountV2(getActivity(),mLikePos,mLikeCount,mGodHand);
                                    }

                                    mLikePos = mFeedAdapterArray.get(position).getPostID();
                                    mLikeCount = 1;
                                }
//                                ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            }
                            else
                            {
                                if(mFeedAdapterArray.get(position).getLiked()!=1)
                                {
                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }
            else
            {
                if(mSelf)
                {
                    MyNowPos = position;
                    mMyVideoLists.add(holder.video);
                }
                else
                {
                    LikeNowPos = position;
                    mLikeVideoLists.add(holder.video);
                }

                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.VISIBLE);
                holder.vcon.setVisibility(View.VISIBLE);

                holder.video.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.video.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }

                final ImageView mPhotoV = holder.photo;
                holder.video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mPhotoV.setVisibility(View.GONE);
                    }
                });

                final FastVideoView mVideoV = holder.video;
                holder.video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mVideoV.start();
                    }
                });

                if(mVideoPlayState)
                {
                    holder.video.setVideoPath(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getVideo()));
                    holder.video.start();
                }

                holder.video.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(getActivity());
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                            {

                                mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                mFeedAdapterArray.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                {
                                    mLikeCount++;
                                }
                                else
                                {
                                    if(mLikeCount!=0)
                                    {
                                        mApplication.sendLikeCountV2(getActivity(),mLikePos,mLikeCount,mGodHand);
                                    }

                                    mLikePos = mFeedAdapterArray.get(position).getPostID();
                                    mLikeCount = 1;
                                }
//                                ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            }
                            else
                            {
                                if(mFeedAdapterArray.get(position).getLiked()!=1)
                                {
                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(getActivity(), mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }

            holder.dio.linkify(new FeedTagActionHandler()
            {
                @Override
                public void handleHashtag(String hashtag)
                {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention)
                {
                    mBigProgress.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(getActivity(), mention, new ApiManager.GetUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, UserModel user)
                        {
                            mBigProgress.setVisibility(View.GONE);
                            if (success && user != null)
                            {
                                Intent intent = new Intent();
                                intent.setClass(getActivity(), HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                startActivity(intent);
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email)
                {

                }

                @Override
                public void handleUrl(String url)
                {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.open_uri_error));
                            Toast.makeText(getActivity(), getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            if(mSelf)
            {
                if(position>=getCount()-5)
                {
                    LoadData(false);
                }
            }
            else
            {
                if(position>=getCount()-5)
                {
                    LoadDataLike(false);
                }
            }


            return convertView;
        }
    }

    private class ListViewHolderLike
    {
        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
    }

    private class DownloadTask extends AsyncTask<String, Integer, Boolean>
    {
        private String subject = "";
        private String body = "";
        private String chooserTitle = "";
        private String uri = "";

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            mBigProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            subject = text[0];
            body = text[1];
            chooserTitle = text[2];
            uri = text[3];

            try
            {
                URL url = new URL(Singleton.getS3FileUrl(uri));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "17app.jpg";
                FileOutputStream outStream = new FileOutputStream(file);
                BitmapFactory.decodeStream(input).compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.close();
                return true;
            }
            catch (IOException e)
            {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);
            mBigProgress.setVisibility(View.GONE);

            if(result) shareTo(subject,body,chooserTitle,uri);
            else {
                try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.error_failed));
                    Toast.makeText(getActivity(), getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
            }
        }
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    public void LoadData(final boolean refresh)
    {
        if(MyisFetchingData)
        {
            return;
        }

        if(refresh)
        {
            MynoMoreData = false;
        }

        if(MynoMoreData)
        {
            return;
        }

        MyisFetchingData = true;
        ApiManager.getUserPost(getActivity(), targetUserID, mFeedModels.get(mFeedModels.size()-1).getTimestamp(), 18, new ApiManager.GetUserPostCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
            {
                MyisFetchingData = false;

                if (success && feedModel!=null)
                {
                    if (feedModel.size() != 0)
                    {
                        mFeedModels.addAll(feedModel);

                        if (feedModel.size() < 18)
                        {
                            MynoMoreData = true;
                        }
                        if(mMyPagte==0)
                        {
                            if(mGridAdapter!=null)
                            {
                                mGridAdapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            if(mListAdapter!=null)
                            {
                                mListAdapter.addModel(mFeedModels);
                                mListAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                } else {
                    try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    public void LoadDataLike(final boolean refresh)
    {
        if(LikeisFetchingData)
        {
            return;
        }

        if(refresh)
        {
            LikenoMoreData = false;
        }

        if(LikenoMoreData)
        {
            return;
        }

        LikeisFetchingData = true;

        ApiManager.getLikedPost(getActivity(), targetUserID, mLikMoedels.get(mLikMoedels.size()-1).getTimestamp(), 18, new ApiManager.GetLikedPostsCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
            {
                LikeisFetchingData = false;

                if (success && feedModel!=null)
                {
                    if (feedModel.size() != 0)
                    {
                        mLikMoedels.addAll(feedModel);

                        if (feedModel.size() < 18)
                        {
                            LikenoMoreData = true;
                        }

                        if (mLikeListAdapter != null) {
                            //[Zack]resolved IndexOutOfBoundsException
                            mLikeListAdapter = new LikeListAdapter(mLikMoedels, false);
                            mScroll.setAdapter(mLikeListAdapter);
                        }
                    }
                }
                else {
                    try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    static ImagePickerDialogCallback imagePickerDialogCallback = null;
    boolean needUploadPicture = false;
    boolean needCreateThumbnail = false;
    boolean needCrop = false;
    Handler handler = new Handler();
    ContentResolver contentResolver = null;
    ProgressDialog progressDialog = null;

    public void showImagePickerOptionDialog(boolean needUploadPicture, boolean needCreateThumbnail, boolean needCrop, ImagePickerDialogCallback callback)
    {
        imagePickerDialogCallback = callback;
        this.needUploadPicture = needUploadPicture;
        this.needCreateThumbnail = needCreateThumbnail;
        this.needCrop = needCrop;

        showOptionPickerDialog(new ArrayList<String>(Arrays.asList(getString(R.string.take_pic), getString(R.string.pick_album))), new OptionPickerCallback() {
            @Override
            public void onResult(boolean done, int selectedIndex) {
                if (!done) {
                    return;
                }

                if (selectedIndex == 0) {
                    startTakePictureIntent();
                } else if (selectedIndex == 1) {
                    startPickImageIntent();
                }
            }
        });
    }

    public void showOptionPickerDialog(final ArrayList<String> items, final OptionPickerCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        CharSequence[] itemTitles = items.toArray(new CharSequence[items.size()]);

        builder.setItems(itemTitles, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callback.onResult(true, which);
            }
        });

        AlertDialog dialog = builder.create();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();

                callback.onResult(false, 0);
            }
        });

        dialog.show();
    }

    public interface ImagePickerDialogCallback
    {
        public void onResult(boolean imageOk, boolean uploadComplete, String imageFileName);
    }

    public interface OptionPickerCallback
    {
        public void onResult(boolean done, int selectedIndex);
    }

    public void startTakePictureIntent()
    {
        new File(Singleton.getExternalTempImagePath()).delete();

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Singleton.getExternalTempImagePath())));
        startActivityForResult(intent, Constants.TAKE_PICTURE_REQUEST);
    }

    public void startPickImageIntent()
    {
        new File(Singleton.getExternalTempImagePath()).delete();

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Constants.PICK_IMAGE_REQUEST);
    }

    public void showNetworkUnstableToast()
    {
        showToast(getString(R.string.connet_erroe), R.drawable.sad);
    }

    public void showToast(String text, int imageResourceID)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        RelativeLayout container = (RelativeLayout) inflater.inflate(R.layout.custom_toast, null);
        TextView toastTextView = (TextView) container.findViewById(R.id.toastTextView);
        ImageView toastImageView = (ImageView) container.findViewById(R.id.toastImageView);

        toastTextView.setText(text);
        toastImageView.setImageResource(imageResourceID);

        final Toast toast = new Toast(getActivity());
        toast.setGravity(Gravity.CENTER, 0, dpToPixel(-60));
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(container);
        toast.show();

        handler.postDelayed(new Runnable()
        {
            public void run()
            {
                toast.cancel();
            }
        }, 2000);
    }

    public int dpToPixel(int dp)
    {
        return Singleton.dpToPixel(dp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        try
        {
            if(resultCode!=getActivity().RESULT_OK)
            {
                return;
            }

            boolean hasGotPicture = false;

            if(requestCode==Constants.TAKE_PICTURE_REQUEST)
            {
                hasGotPicture = true;
                needCrop = true;
                needCreateThumbnail = true;
            }

            if(requestCode==Constants.PICK_IMAGE_REQUEST)
            {
                if(data!=null && data.getData()!=null)
                {
                    String selectedImageFilePath = Singleton.getRealPathFromURI(data.getData(), getActivity());
                    String tmpImageFilePath = Singleton.getExternalTempImagePath();

                    if(selectedImageFilePath==null)
                    {
                        // copy image to sdcard
                        try {
                            Uri selectedImageURI = data.getData();
                            InputStream input = contentResolver.openInputStream(selectedImageURI);
                            BitmapHelper.saveBitmap(BitmapFactory.decodeStream(input, null, BitmapHelper.getBitmapOptions(2)), tmpImageFilePath, Bitmap.CompressFormat.JPEG);
                            hasGotPicture = true;
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    else
                    {
                        Singleton.copyFile(new File(selectedImageFilePath), new File(tmpImageFilePath));
                        hasGotPicture = true;
                    }
                }
            }

            if(requestCode==Constants.CROP_IMAGE_REQUEST)
            {
                hasGotPicture = true;
                needUploadPicture = true;
            }

            if(hasGotPicture==false)
            {
                try
                {
                    imagePickerDialogCallback.onResult(false, false, "");
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }

                return;
            }

            // crop
            if(needCrop)
            {
                needCrop = false;

                startActivityForResult(new Intent().setClass(getActivity(), CropImageActivity.class), Constants.CROP_IMAGE_REQUEST);
                return;
            }

            int largeSize = Constants.PROFILE_PICTURE_PIXELS;
            int thumbnailSize = Constants.PROFILE_PICTURE_THUMBNAIL_PIXELS;
            if(getConfig(Constants.PHONE_WIDTH_HEIGHT_SIZE,Constants.DEFAULT_PIXELS) < Constants.CHECK_PIXELS)
            {
                largeSize = Constants.LOW_PROFILE_PICTURE_PIXELS;
                thumbnailSize = Constants.LOW_PROFILE_PICTURE_THUMBNAIL_PIXELS;
            }

            // generate thumbnail
            Bitmap large = BitmapHelper.getBitmap(Singleton.getExternalTempImagePath(), largeSize);
            Bitmap thumbnail = BitmapHelper.getBitmap(Singleton.getExternalTempImagePath(), thumbnailSize);

            if(large==null || thumbnail==null)
            {
                imagePickerDialogCallback.onResult(false, false, "");
                showAlertDialog(getString(R.string.prompt), getString(R.string.unknown_error_occur));
                return;
            }

            final String pictureFileName = Singleton.getUUIDFileName("jpg");
            final String thumbnailPictureFileName = "THUMBNAIL_" + pictureFileName;

            Singleton.log("pictureFileName:" + pictureFileName);
            Singleton.log("thumbnailPictureFileName:" + thumbnailPictureFileName);

            new File(Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName).delete();

            BitmapHelper.saveBitmap(large, Singleton.getExternalMediaFolderPath() + pictureFileName, Bitmap.CompressFormat.JPEG);
            BitmapHelper.saveBitmap(thumbnail, Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName, Bitmap.CompressFormat.JPEG);

            if(!needUploadPicture)
            {
                imagePickerDialogCallback.onResult(true, false, pictureFileName);
                return;
            }

            needCreateThumbnail = true;
            showProgressDialog();

            ArrayList<String> filesToUpload = new ArrayList<String>();
            filesToUpload.add(pictureFileName);

            if(needCreateThumbnail)
            {
                filesToUpload.add(thumbnailPictureFileName);
            }

            MultiFileUploader uploader = new MultiFileUploader(filesToUpload);

            uploader.startUpload(getActivity(), new MultiFileUploader.MultiFileUploaderCallback()
            {
                @Override
                public void didComplete()
                {
                    hideProgressDialog();
                    showCompleteToast();
                    imagePickerDialogCallback.onResult(true, true, pictureFileName);
                }

                @Override
                public void didFail()
                {
                    hideProgressDialog();
                    showNetworkUnstableToast();
                    imagePickerDialogCallback.onResult(true, false, pictureFileName);
                }
            });
        }
        catch (Exception e)
        {

        }
    }

    public int getConfig(String key, int def)
    {
        SharedPreferences settings = getActivity().getSharedPreferences("settings", 0);
        return settings.getInt(key, def);
    }

    public void showAlertDialog(String title, String message)
    {
        showOKDialog(title, message, null);
    }

    public void showOKDialog(String title, String message, final TestActivity.DialogCallback callback) {
        try {
            final Dialog dialog = new Dialog(getActivity(), R.style.MyDialog);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.my_dialog);

            RelativeLayout rootView = (RelativeLayout) dialog.findViewById(R.id.rootView);
            Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
            View marginView = (View) dialog.findViewById(R.id.marginView);
            Button okButton = (Button) dialog.findViewById(R.id.okButton);
            TextView titleTextView = (TextView) dialog.findViewById(R.id.titleTextView);
            TextView messageTextView = (TextView) dialog.findViewById(R.id.messageTextView);

            rootView.getLayoutParams().width = Singleton.SCREEN_WIDTH * 8 / 10;

            cancelButton.setVisibility(View.GONE);
            marginView.setVisibility(View.GONE);

            okButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();

                    if(callback!=null) {
                        callback.onResult(true);
                    }
                }
            });

            titleTextView.setText(title);
            messageTextView.setText(message);

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    dialog.dismiss();

                    callback.onResult(false);
                }
            });

            dialog.show();
        } catch(Exception e) {

        }
    }

    public void showProgressDialog()
    {
        try
        {
            if(progressDialog==null)
            {
                progressDialog = ProgressDialog.show(getActivity(), "", getString(R.string.processing));
            }

            progressDialog.setCancelable(true);
        }
        catch (Exception e)
        {
        }
    }

    public void hideProgressDialog()
    {
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showCompleteToast() {
        showToast(getString(R.string.done), R.drawable.happy);
    }

    private JSONObject BranchSharePostData(String PostID, int PorV, String OpenID, String caption, String Picture)
    {
        JSONObject mShareData = new JSONObject();

        //Umeng Monitor
        String Umeng_id="SharePost";
        HashMap<String,String> mHashMap = new HashMap<String,String>();
        mHashMap.put(Umeng_id, Umeng_id);
        MobclickAgent.onEventValue(getActivity(),Umeng_id,mHashMap,0);

        try
        {
            mShareData.put("page", "p");
            mShareData.put("ID", PostID);
            mShareData.put("$og_title", String.format(getString(PorV), OpenID));
            mShareData.put("$og_description", caption);
            mShareData.put("$og_image_url", Singleton.getS3FileUrl(Picture));
            mShareData.put("$og_type", "website");
            mShareData.put("$fallback_url", Constants.MEDIA17_WEBSITE_SHARE + "p/" + PostID);
        }
        catch (JSONException ex)
        {
        }

        return mShareData;
    }

    @Override
    public void onDestroy()
    {
        //event tracking
        try{
            LogEventUtil.LeaveSelfPage(getActivity(),mApplication,(Singleton.getCurrentTimestamp()-startSelfPage));
        }catch (Exception x)
        {

        }
        super.onDestroy();
    }
}
