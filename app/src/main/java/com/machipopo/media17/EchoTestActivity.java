package com.machipopo.media17;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.machipopo.media.PCMAudioPlayer;
import com.machipopo.media.PCMAudioRecorder;
import com.umeng.analytics.MobclickAgent;

public class EchoTestActivity extends BaseActivity implements PCMAudioRecorder.PCMAudioRecorderDelegate
{
    Button startBtn;
    Button stopBtn;
    boolean isPlaying;

    PCMAudioPlayer audioPlayer;
    PCMAudioRecorder audioRecorder;

    private MusicIntentReceiver myReceiver;
    AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.echo_test_activity);

        myReceiver = new MusicIntentReceiver();

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);

        if(audioManager.isWiredHeadsetOn()) {
            audioManager.setSpeakerphoneOn(false);
        } else {
            audioManager.setSpeakerphoneOn(true);
        }

        startBtn = (Button) findViewById(R.id.startBtn);
        stopBtn = (Button) findViewById(R.id.stopBtn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPlaying = true;
            }
        });

        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPlaying = false;
            }
        });

        audioPlayer = new PCMAudioPlayer();
        audioPlayer.startPlaying();

        audioRecorder = new PCMAudioRecorder();
        audioRecorder.delegate = this;
        audioRecorder.startRecording();
    }

    @Override public void onResume() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(myReceiver, filter);
        super.onResume();

        MobclickAgent.onPageStart(EchoTestActivity.this.getClass().getSimpleName());
    }

    private class MusicIntentReceiver extends BroadcastReceiver {
        @Override public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        audioManager.setSpeakerphoneOn(true); // no head set
                        break;
                    case 1:
                        audioManager.setSpeakerphoneOn(false); // has head set
                        break;
                    default:
                }
            }
        }
    }

    @Override public void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();

        MobclickAgent.onPageEnd(EchoTestActivity.this.getClass().getSimpleName());
    }

    /* PCMAudioRecorderDelegate */
    @Override
    public void didGetAudioData(byte[] buffer, int readBytes) {
        if(!isPlaying) {
            return;
        }

        audioPlayer.playAudio(buffer, 0, readBytes, 0);
    }
}
