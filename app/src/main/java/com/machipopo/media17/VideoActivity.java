package com.machipopo.media17;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media.FloatPCMAudioRecorder;
import com.machipopo.media.Mp4Recorder;
import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by POPO on 6/2/15.
 */
public class VideoActivity extends BaseFragmentActivity implements SurfaceHolder.Callback, Camera.PreviewCallback, FloatPCMAudioRecorder.FloatPCMAudioRecorderDelegate
{
    private VideoActivity mCtx = this;
    private Story17Application mApplication;
    private Camera camera;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private Camera.PictureCallback jpegCallback;

    private View mView;
    private ImageView mTake;
    private DisplayMetrics mDisplayMetrics;

    private RelativeLayout mTitleBar;

    private ImageView mGallery,mCancel;
    private ContentResolver contentResolver = null;

    private ProgressBar mProgressBar;
    private Boolean mTaking = false;
    private int captureMode = Constants.CAPTURE_MODE_MOVIE;

    private ImageView mLight, mLine, mCame, mLineView;
    private Boolean isCame = false;
    private int mFlashState = 1;
    private int cameraPosition = 1;

    private ViewPager mPager;
    private ArrayList<View> mViewList;
//    private Gallery mGry;
    private ImageView mVideoDelete,mVideoGO;
    private LinearLayout mVideoBar;
    private TextView mMin;
    private RelativeLayout mBarLayout;
    private Boolean mAutoFocusState = true;

    ByteBuffer yuvDataBuffer;
    byte[] yuvData;
    ByteBuffer audioDataBuffer;
    FloatPCMAudioRecorder pcmAudioRecorder;
    boolean isRecording = false;
    HandlerThread videoEncodeHandlerThread;
    Handler videoEncodeHandler;
    Handler handler;
    String movieFilePath;
    String movieName;

    private int mVideoCount = 0;
    private String mVideoPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/"+"video_frame/";
    private Boolean isRecorder = false;
    private String mVideoName = "video_frame_";

    private double oneW = 0;
    private ArrayList<TextView> mTextsBar = new ArrayList<TextView>();
    private int mVideoOne = 0;
    private Message mMessage;
    private TextView mTime;
    private Handler mTimeHandler = new Handler();
    private LinearLayout.LayoutParams mLayoutParams;
    private LinearLayout.LayoutParams mLayoutParamsBar;

    private ArrayList<Integer> mVideoPos = new ArrayList<Integer>();
    private ArrayList<Integer> mAudiosPos = new ArrayList<Integer>();
    private Boolean mDeleteState = false;
    private Boolean mAudioDelete = true;

    private ArrayList<float[]> mAudioList = new ArrayList<float[]>();
    private int audioSize = 0;

    private int deW = 0;
    private int deH = 0;

    private ChangePreviewTask mChangePreviewTask;
    private Handler mHandler;
    private int POS = 0;
//    private GryAdapter mGryAdapter;
    private Boolean mGalleryState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }


        mApplication = (Story17Application) getApplication();

        // Create Pictures Folder if it's not exist
        File PictureFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath());
        if(!PictureFolder.exists()){
            PictureFolder.mkdir();
        }

//---

//        movieFilePath = Singleton.getExternalMediaFolderPath() + Constants.MOVIE_FILE_NAME;
        movieName = Singleton.getUUIDFileName("mp4");
        movieFilePath = Singleton.getExternalMediaFolderPath() + movieName;

        // alloc native direct buffers
        yuvDataBuffer = ByteBuffer.allocateDirect(Constants.MOVIE_WIDTH * Constants.MOVIE_WIDTH * 3 / 2);
        yuvDataBuffer.order(ByteOrder.nativeOrder());
        audioDataBuffer = ByteBuffer.allocateDirect(1024 * 4); // 1024 PCM data buffer
        audioDataBuffer.order(ByteOrder.nativeOrder());

        // alloc JVM byte buffers
        yuvData = new byte[Constants.MOVIE_WIDTH * Constants.MOVIE_WIDTH *3/2];
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

        handler = new Handler();

        videoEncodeHandlerThread = new HandlerThread("VIDEO_ENCODE");
        videoEncodeHandlerThread.start();
        videoEncodeHandler = new Handler(videoEncodeHandlerThread.getLooper());

//---

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        contentResolver = (ContentResolver) getContentResolver();

        initTitleBar();

        //event tracking
        try{
            LogEventUtil.EnterPublishPostPage(mCtx, mApplication, "video");
        }catch (Exception x)
        {

        }

        mView = (View) findViewById(R.id.myview);
        mView.getLayoutParams().height = mDisplayMetrics.widthPixels;
        mView.getLayoutParams().width = mDisplayMetrics.widthPixels;

//        mView.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                if(camera!=null && mAutoFocusState)
//                {
//                    mAutoFocusState = false;
//
//                    try {
//                        camera.autoFocus(new Camera.AutoFocusCallback()
//                        {
//                            @Override
//                            public void onAutoFocus(boolean success, Camera cam)
//                            {
//                                mAutoFocusState = true;
//                            }
//                        });
//                    } catch (Exception e){
//
//                    }
//                }
//            }
//        });

        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        jpegCallback = new Camera.PictureCallback()
        {
            public void onPictureTaken(byte[] data, Camera camera)
            {
                takePic(data,camera);
            }
        };

        mTake = (ImageView) findViewById(R.id.take);
        mGallery = (ImageView) findViewById(R.id.gallery);
        mCancel = (ImageView) findViewById(R.id.cancel);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);

        mLight = (ImageView) findViewById(R.id.light);
        mLine = (ImageView) findViewById(R.id.line);
        mCame = (ImageView) findViewById(R.id.came);
        mLineView = (ImageView) findViewById(R.id.line_layout);

        TakeState(captureMode);

        mGallery.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, Constants.PICK_IMAGE_REQUEST);

                if(camera!=null) camera.stopPreview();
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mCtx.finish();
            }
        });

        mLight.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(camera!=null)
                {
                    try
                    {
                        if(mFlashState==1)
                        {
                            mLight.setImageResource(R.mipmap.camera_flash_on_active);
                            mFlashState = 2;
                            Camera.Parameters p = camera.getParameters();
                            p.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                            camera.setParameters(p);
                        }
                        else if(mFlashState==2)
                        {
                            mLight.setImageResource(R.mipmap.camera_flash_auto_active);
                            mFlashState = 3;
                            Camera.Parameters p = camera.getParameters();
                            p.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                            camera.setParameters(p);
                        }
                        else
                        {
                            mLight.setImageResource(R.mipmap.camera_flash_off);
                            mFlashState = 1;
                            Camera.Parameters p = camera.getParameters();
                            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                            camera.setParameters(p);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
        });

        mLine.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mLineView.isShown())
                {
                    mLine.setImageResource(R.mipmap.camera_gridtoggle);
                    mLineView.setVisibility(View.GONE);
                }
                else
                {
                    mLine.setImageResource(R.mipmap.camera_gridtoggle_down);
                    mLineView.setVisibility(View.VISIBLE);
                }
            }
        });

        mCame.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(camera!=null)
                {
                    try
                    {
                        int cameraCount = 0;
                        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                        cameraCount = Camera.getNumberOfCameras();

                        for (int i = 0; i < cameraCount; i++)
                        {
                            Camera.getCameraInfo(i, cameraInfo);
                            if (cameraPosition == 1)
                            {
                                //前鏡頭
                                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                                {
                                    mCame.setImageResource(R.mipmap.camera_cameratoggle_down);
                                    isCame = true;

                                    if (camera != null)
                                    {
                                        camera.setPreviewCallback(null);
                                        camera.stopPreview();
                                        camera.release();
                                        camera = null;
                                    }

                                    camera = Camera.open(i);
                                    camera.setDisplayOrientation(90);
                                    camera.setPreviewDisplay(surfaceHolder);

                                    if(captureMode==Constants.CAPTURE_MODE_MOVIE)
                                    {
                                        Camera.Parameters parameters = camera.getParameters();
                                        parameters.setPreviewSize(640,480);
                                        parameters.setPictureSize(640, 480);
                                        camera.setParameters(parameters);
                                    }
                                    else
                                    {
                                        String manufacturer = Build.MANUFACTURER;
                                        if(manufacturer.compareTo("HTC")==0)
                                        {
                                            Camera.Parameters parameters = camera.getParameters();
                                            parameters.setPreviewSize(1280,720);
                                            parameters.setPictureSize(1280,720);
                                            parameters.setFocusMode("auto");
                                            camera.setParameters(parameters);
                                        }
                                        else
                                        {
                                            Camera.Parameters parameters = camera.getParameters();
                                            parameters.setPreviewSize(deW,deH);
                                            parameters.setPictureSize(deW,deH);
                                            camera.setParameters(parameters);
                                        }
                                    }

                                    camera.startPreview();
                                    camera.setPreviewCallback(VideoActivity.this);
                                    cameraPosition = 0;
                                    break;
                                }
                            }
                            else
                            {
                                //後鏡頭
                                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                                {
                                    mCame.setImageResource(R.mipmap.camera_cameratoggle);
                                    isCame = false;

                                    if (camera != null)
                                    {
                                        camera.setPreviewCallback(null);
                                        camera.stopPreview();
                                        camera.release();
                                        camera = null;
                                    }

                                    camera = Camera.open(i);
                                    camera.setDisplayOrientation(90);
                                    camera.setPreviewDisplay(surfaceHolder);

                                    if(captureMode==Constants.CAPTURE_MODE_MOVIE)
                                    {
                                        Camera.Parameters parameters = camera.getParameters();
                                        parameters.setPreviewSize(640,480);
                                        parameters.setPictureSize(640,480);
                                        camera.setParameters(parameters);
                                    }
                                    else
                                    {
                                        String manufacturer = Build.MANUFACTURER;
                                        if(manufacturer.compareTo("HTC")==0)
                                        {
                                            Camera.Parameters parameters = camera.getParameters();
                                            parameters.setPreviewSize(1280,720);
                                            parameters.setPictureSize(1280,720);
                                            parameters.setFocusMode("auto");
                                            camera.setParameters(parameters);
                                        }
                                        else
                                        {
                                            Camera.Parameters parameters = camera.getParameters();
                                            parameters.setPreviewSize(deW,deH);
                                            parameters.setPictureSize(deW,deH);
                                            camera.setParameters(parameters);
                                        }
                                    }

                                    camera.startPreview();
                                    camera.setPreviewCallback(VideoActivity.this);
                                    cameraPosition = 1;
                                    break;
                                }
                            }

                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        });

        mPager = (ViewPager) findViewById(R.id.pager);
        LayoutInflater lf = mCtx.getLayoutInflater().from(mCtx);

        mViewList = new ArrayList<View>();
        mViewList.add(lf.inflate(R.layout.null_pager, null));
//        mViewList.add(lf.inflate(R.layout.null_pager, null));
//        mViewList.add(lf.inflate(R.layout.null_pager, null));

        mPager.setAdapter(new MyPagerAdapter());

//        mGry = (Gallery) findViewById(R.id.gry);
//        mGryAdapter = new GryAdapter();
//        mGry.setAdapter(mGryAdapter);
//        mGry.setSpacing(50);

        POS = 0;

//        mGry.setOnItemClickListener(new AdapterView.OnItemClickListener()
//        {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
//            {
//                mGalleryState = true;
//                POS = position;
//
//                if(adapterView==null) {
//                    return;
//                }
//
//                ((TextView)adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.white));
//                ((TextView)adapterView.getChildAt(1)).setTextColor(getResources().getColor(R.color.white));
//                ((TextView)adapterView.getChildAt(2)).setTextColor(getResources().getColor(R.color.white));
//
//                ((TextView)view).setTextColor(getResources().getColor(R.color.white));
//
//                if(camera!=null)
//                {
//                    if(position==0)
//                    {
//                        mTake.setImageResource(R.drawable.btn_dock_camera_selector);
//                        mBarLayout.setVisibility(View.GONE);
//                        mGallery.setVisibility(View.VISIBLE);
//                        mCancel.setVisibility(View.INVISIBLE);
//                        mVideoDelete.setVisibility(View.GONE);
//                        mVideoGO.setVisibility(View.GONE);
//
//                        if(mChangePreviewTask!=null) mChangePreviewTask = null;
//                        mChangePreviewTask = new ChangePreviewTask();
//                        mChangePreviewTask.execute(0);
//                    }
//                    else if(position==1)
//                    {
//                        captureMode = Constants.CAPTURE_MODE_MOVIE;
//
//                        mTake.setImageResource(R.drawable.btn_dock_video_selector);
//                        mBarLayout.setVisibility(View.VISIBLE);
//                        mGallery.setVisibility(View.GONE);
//                        mCancel.setVisibility(View.GONE);
//                        if(mVideoCount!=0)mVideoDelete.setVisibility(View.VISIBLE);
//                        else mVideoDelete.setVisibility(View.INVISIBLE);
//                        if(mVideoCount>=90)mVideoGO.setVisibility(View.VISIBLE);
//                        else mVideoGO.setVisibility(View.INVISIBLE);
//
//                        if(mChangePreviewTask!=null) mChangePreviewTask = null;
//                        mChangePreviewTask = new ChangePreviewTask();
//                        mChangePreviewTask.execute(1);
//                    }
//                    else
//                    {
//                        mProgressBar.setVisibility(View.VISIBLE);
//                        mView.setBackgroundColor(Color.BLACK);
//
//                        try
//                        {
//                            if(mHandler!=null) mHandler = null;
//                            mHandler = new Handler();
//                            mHandler.postDelayed(new Runnable()
//                            {
//                                @Override
//                                public void run() {
//                                    if (camera != null)
//                                    {
//                                        camera.setPreviewCallback(null);
//                                        camera.stopPreview();
//                                        camera.release();
//                                        camera = null;
//                                    }
//                                    mApplication.setOpenLive(true);
//                                    mCtx.finish();
//                                }
//                            },200);
//                        }
//                        catch (Exception e)
//                        {
//                            if (camera != null)
//                            {
//                                camera.setPreviewCallback(null);
//                                camera.stopPreview();
//                                camera.release();
//                                camera = null;
//                            }
//                            mApplication.setOpenLive(true);
//                            mCtx.finish();
//                        }
//                    }
//                }
//            }
//        });

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position)
            {
                if(!mGalleryState)
                {
                    POS = position;

                    if(camera!=null)
                    {
                        if(position==0)
                        {
                            mTake.setImageResource(R.drawable.btn_dock_camera_selector);
                            mBarLayout.setVisibility(View.GONE);
                            mGallery.setVisibility(View.VISIBLE);
                            mCancel.setVisibility(View.INVISIBLE);
                            mVideoDelete.setVisibility(View.GONE);
                            mVideoGO.setVisibility(View.GONE);

//                            mGry.setSelection(0);
//                            mGryAdapter.notifyDataSetChanged();

                            if(mChangePreviewTask!=null) mChangePreviewTask = null;
                            mChangePreviewTask = new ChangePreviewTask();
                            mChangePreviewTask.execute(0);
                        }
                        else if(position==1)
                        {
                            captureMode = Constants.CAPTURE_MODE_MOVIE;

                            mTake.setImageResource(R.drawable.btn_dock_video_selector);
                            mBarLayout.setVisibility(View.VISIBLE);
                            mGallery.setVisibility(View.GONE);
                            mCancel.setVisibility(View.GONE);
                            if(mVideoCount!=0)mVideoDelete.setVisibility(View.VISIBLE);
                            else mVideoDelete.setVisibility(View.INVISIBLE);
                            if(mVideoCount>=90)mVideoGO.setVisibility(View.VISIBLE);
                            else mVideoGO.setVisibility(View.INVISIBLE);

//                            mGry.setSelection(1);
//                            mGryAdapter.notifyDataSetChanged();

                            if(mChangePreviewTask!=null) mChangePreviewTask = null;
                            mChangePreviewTask = new ChangePreviewTask();
                            mChangePreviewTask.execute(1);
                        }
                        else
                        {
                            mProgressBar.setVisibility(View.VISIBLE);
                            mView.setBackgroundColor(Color.BLACK);

                            try
                            {
                                if(mHandler!=null) mHandler = null;
                                mHandler = new Handler();
                                mHandler.postDelayed(new Runnable()
                                {
                                    @Override
                                    public void run() {
                                        if (camera != null)
                                        {
                                            camera.setPreviewCallback(null);
                                            camera.stopPreview();
                                            camera.release();
                                            camera = null;
                                        }
                                        mApplication.setOpenLive(true);
                                        mCtx.finish();
                                    }
                                },200);
                            }
                            catch (Exception e)
                            {
                                if (camera != null)
                                {
                                    camera.setPreviewCallback(null);
                                    camera.stopPreview();
                                    camera.release();
                                    camera = null;
                                }
                                mApplication.setOpenLive(true);
                                mCtx.finish();
                            }
                        }
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

        mBarLayout = (RelativeLayout) findViewById(R.id.bar_layout);
        mVideoDelete = (ImageView) findViewById(R.id.video_delete);
        mVideoGO = (ImageView) findViewById(R.id.video_go);
        mVideoBar = (LinearLayout) findViewById(R.id.video_bar);
        mMin = (TextView) findViewById(R.id.min);
        mTime = (TextView) findViewById(R.id.time);

        RelativeLayout.LayoutParams mMinLayout = new RelativeLayout.LayoutParams((int)convertDpToPixel(3),(int)convertDpToPixel(5));
        mMinLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        mMinLayout.leftMargin = (int)(((double)mDisplayMetrics.widthPixels / (double) 100) * (double) 20);
        mMin.setLayoutParams(mMinLayout);

        mVideoDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(!mDeleteState)
                {
                    mTextsBar.get(mTextsBar.size()-1).setAlpha(0.5f);
                    mTextsBar.get(mTextsBar.size()-2).setAlpha(0.5f);
                    mDeleteState = true;
                }
                else
                {
                    mVideoBar.removeViewAt(mVideoBar.getChildCount()-1);
                    mVideoBar.removeViewAt(mVideoBar.getChildCount()-1);
                    mTextsBar.remove(mTextsBar.size()-1);
                    mTextsBar.remove(mTextsBar.size()-1);
                    mDeleteState = false;

                    mVideoPos.remove(mVideoPos.size()-1);

                    if(mVideoPos.size()!=0) mVideoCount = mVideoPos.get(mVideoPos.size()-1);
                    else mVideoCount = 0;

                    mProgressBar.setVisibility(View.VISIBLE);
                    new Thread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            mAudioDelete = false;
                            if(mAudiosPos.size()>1)
                            {
                                for(int i = mAudioList.size()-1 ; i > mAudiosPos.get(mAudiosPos.size()-2) ; i--)
                                {
                                    mAudioList.remove(i);
                                }
                            }
                            else
                            {
                                mAudioList.clear();
                                mAudiosPos.add(0);
                            }

                            if(mMessage!=null) mMessage = null;
                            mMessage = new Message();
                            mMessage.what = 3;
                            VideoHandler.sendMessage(mMessage);
                        }
                    }).start();

                    if(mVideoBar.getChildCount()==0 || mTextsBar.size()==0) mVideoDelete.setVisibility(View.INVISIBLE);
                    if(mVideoCount>=90)mVideoGO.setVisibility(View.VISIBLE);
                    else mVideoGO.setVisibility(View.INVISIBLE);
                }
            }
        });

        mVideoGO.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mDeleteState)
                {
                    mTextsBar.get(mTextsBar.size()-1).setAlpha(1.0f);
                    mTextsBar.get(mTextsBar.size()-2).setAlpha(1.0f);
                    mDeleteState = false;
                }

                mApplication.setLoader(0);
                mApplication.setUpLoaderState(true);

                mApplication.uploadVideo(mCtx,mVideoCount,mVideoPath,mVideoName,yuvDataBuffer,audioDataBuffer,mAudioList,movieFilePath,handler,movieName,audioSize);

                if(camera!=null) camera.stopPreview();

                Intent intent = new Intent();
                intent.setClass(mCtx, CameraVideoPostActivity.class);
                intent.putExtra("file", Singleton.getExternalMediaFolderPath() + movieName.substring(0,movieName.length()-3) + "jpg");
                intent.putExtra("file2", Singleton.getExternalMediaFolderPath() + "THUMBNAIL_" + movieName.substring(0,movieName.length()-3) + "jpg");
                intent.putExtra("name", movieName.substring(0,movieName.length()-3) + "jpg");
                intent.putExtra("name2", "THUMBNAIL_" + movieName.substring(0,movieName.length()-3) + "jpg");
                intent.putExtra("video", movieName);
                startActivity(intent);
                mCtx.finish();
            }
        });

        oneW = (double)mDisplayMetrics.widthPixels / (double) 450;

        mLayoutParams = new LinearLayout.LayoutParams((int) convertDpToPixel(1), (int) convertDpToPixel(5));
        mTimeHandler.postDelayed(TimeRun,500);

//        mLayoutParamsBar = new LinearLayout.LayoutParams(0,(int)convertDpToPixel(5));

//        TextView mBar = new TextView(mCtx);
//        mBar.setLayoutParams(new LinearLayout.LayoutParams(0,(int)convertDpToPixel(5)));
//        mBar.setBackgroundColor(getResources().getColor(R.color.main_color));
//        mBar.getLayoutParams().width = (int)(oneW * (double)225);
//        mVideoBar.addView(mBar);

        // audio recorder
        pcmAudioRecorder = new FloatPCMAudioRecorder();
        pcmAudioRecorder.delegate = this;
        pcmAudioRecorder.startRecording();

        File mFile = new File(mVideoPath);
        if(!mFile.exists()) mFile.mkdir();

        captureMode = Constants.CAPTURE_MODE_MOVIE;

        mTake.setImageResource(R.drawable.btn_dock_video_selector);
        mBarLayout.setVisibility(View.VISIBLE);
        mGallery.setVisibility(View.GONE);
        mCancel.setVisibility(View.GONE);
        if(mVideoCount!=0)mVideoDelete.setVisibility(View.VISIBLE);
        else mVideoDelete.setVisibility(View.INVISIBLE);
        if(mVideoCount>=90)mVideoGO.setVisibility(View.VISIBLE);
        else mVideoGO.setVisibility(View.INVISIBLE);
    }

    private class ChangePreviewTask extends AsyncTask<Integer, Integer, Integer>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
            mView.setBackgroundColor(Color.BLACK);
        }

        @Override
        protected Integer doInBackground(Integer... pos)
        {
            if(pos[0]==0)
            {
                captureMode = Constants.CAPTURE_MODE_PHOTO;

                TakeState(captureMode);

                if(deW!=0 && deH!=0)
                {
                    if(camera!=null)
                    {
                        Camera.Parameters parameters = camera.getParameters();
                        parameters.setPreviewSize(deW,deH);
                        parameters.setPictureSize(deW,deH);
                        camera.setParameters(parameters);
                    }
                }

                return  pos[0];
            }
            else
            {
                TakeState(captureMode);

                if(camera!=null)
                {
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setPreviewSize(640,480);
                    parameters.setPictureSize(640,480);
                    camera.setParameters(parameters);
                }

                return  pos[0];
            }
        }

        @Override
        protected void onPostExecute(Integer pos)
        {
            super.onPostExecute(pos);

//            if(pos==0)
//            {
//                if(((deH/3)*4)==deW)
//                {
//                    RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.widthPixels/3*4);
//                    mSurParams.topMargin = (int)(((mDisplayMetrics.widthPixels/3*4)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1 ;
//                    surfaceView.setLayoutParams(mSurParams);
//                }
//                else
//                {
//                    RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.heightPixels);
//                    mSurParams.topMargin = (int)(((mDisplayMetrics.heightPixels)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1;
//                    surfaceView.setLayoutParams(mSurParams);
//                }
//                mPager.setCurrentItem(0);
//            }
//            else
//            {
//                RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.widthPixels/3*4);
//                mSurParams.topMargin = (int)(((mDisplayMetrics.widthPixels/3*4)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1 ;
//                surfaceView.setLayoutParams(mSurParams);
//                mPager.setCurrentItem(1);
//            }

            RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.widthPixels/3*4);
            mSurParams.topMargin = (int)(((mDisplayMetrics.widthPixels/3*4)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1 ;
            surfaceView.setLayoutParams(mSurParams);

            try
            {
                if(mHandler!=null) mHandler = null;
                mHandler = new Handler();
                mHandler.postDelayed(new Runnable()
                {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.GONE);
                        mView.setBackgroundColor(Color.parseColor("#00000000"));
                    }
                },500);
            }
            catch (Exception e)
            {
                mProgressBar.setVisibility(View.GONE);
                mView.setBackgroundColor(Color.parseColor("#00000000"));
            }

            mGalleryState = false;
        }
    }

    private class ChangeTask extends AsyncTask<Integer, Integer, Integer>
    {
        private Camera camera;

        public ChangeTask(Camera camera)
        {
            this.camera = camera;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
            surfaceView.setVisibility(View.GONE);
        }

        @Override
        protected Integer doInBackground(Integer... pos)
        {
            if(pos[0]==0)
            {
                if(deW!=0 && deH!=0)
                {
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setPreviewSize(deW,deH);
                    camera.setParameters(parameters);
                }

                return pos[0];
            }
            else
            {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setPreviewSize(640, 480);
                camera.setParameters(parameters);

                return pos[0];
            }
        }

        @Override
        protected void onPostExecute(Integer pos)
        {
            super.onPostExecute(pos);

            if(pos==0)
            {
                if(((deH/3)*4)==deW)
                {
                    RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.widthPixels/3*4);
                    mSurParams.topMargin = (int)(((mDisplayMetrics.widthPixels/3*4)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1 ;
                    surfaceView.setLayoutParams(mSurParams);
                }
                else
                {
                    RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.heightPixels);
                    mSurParams.topMargin = (int)(((mDisplayMetrics.heightPixels)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1;
                    surfaceView.setLayoutParams(mSurParams);
                }
            }
            else
            {
                RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.widthPixels/3*4);
                mSurParams.topMargin = (int)(((mDisplayMetrics.widthPixels/3*4)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1 ;
                surfaceView.setLayoutParams(mSurParams);
            }

            mProgressBar.setVisibility(View.GONE);
            surfaceView.setVisibility(View.VISIBLE);
        }
    }

    private void TakeState(int captureMode)
    {
        if(captureMode==Constants.CAPTURE_MODE_MOVIE)
        {
            mTake.setOnClickListener(null);
            mTake.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    TakeTouch(event);

                    return false;
                }
            });
        }
        else if(captureMode==Constants.CAPTURE_MODE_PHOTO)
        {
            mTake.setOnTouchListener(null);
            mTake.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    TakeClick();
                }
            });
        }
    }

    private void TakeClick()
    {
        try
        {
            if(camera!=null)
            {
                if(jpegCallback!=null)
                {
                    camera.takePicture(null, null, jpegCallback);
                }
                else
                {
                    jpegCallback = new Camera.PictureCallback()
                    {
                        public void onPictureTaken(byte[] data, Camera camera)
                        {
                            takePic(data,camera);
                        }
                    };

                    camera.takePicture(null, null, jpegCallback);
                }
            }
            else {
                try {
//                          showToast(getString(R.string.failed));
                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }
            }
        }
        catch(Exception e)
        {
            try {
//                          showToast(getString(R.string.failed));
                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
            } catch (Exception x) {
            }
        }
    }

    private void TakeTouch(MotionEvent event)
    {
        if (event.getAction()==MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL || event.getAction()==MotionEvent.ACTION_OUTSIDE)
        {
            if(!isRecording) return;

            isRecording = false;
            int OneCount = mVideoOne;
            mVideoOne = 0;

            int pos = mVideoCount;
            mVideoPos.add(pos);

            int Apos = mAudioList.size();
            mAudiosPos.add(Apos);

            TextView mBar = new TextView(mCtx);
            mBar.setLayoutParams(mLayoutParams);
            mBar.setBackgroundColor(Color.WHITE);
            mVideoBar.addView(mBar);
            mTextsBar.add(mBar);

            if(OneCount<30)
            {
                try {
//                          showToast(getString(R.string.video_size));
                    Toast.makeText(mCtx, getString(R.string.video_size), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }

                mVideoBar.removeViewAt(mVideoBar.getChildCount()-1);
                mVideoBar.removeViewAt(mVideoBar.getChildCount()-1);
                mTextsBar.remove(mTextsBar.size()-1);
                mTextsBar.remove(mTextsBar.size()-1);

                mVideoPos.remove(mVideoPos.size()-1);

                if(mVideoPos.size()!=0) mVideoCount = mVideoPos.get(mVideoPos.size()-1);
                else mVideoCount = 0;

                mProgressBar.setVisibility(View.VISIBLE);
                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mAudioDelete = false;
                        if(mAudiosPos.size()>1)
                        {
                            for(int i = mAudioList.size()-1 ; i > mAudiosPos.get(mAudiosPos.size()-2) ; i--)
                            {
                                mAudioList.remove(i);
                            }
                        }
                        else
                        {
                            mAudioList.clear();
                            mAudiosPos.add(0);
                        }

                        if(mMessage!=null) mMessage = null;
                        mMessage = new Message();
                        mMessage.what = 3;
                        VideoHandler.sendMessage(mMessage);
                    }
                }).start();
            }

            if(mVideoCount!=0)mVideoDelete.setVisibility(View.VISIBLE);
            else mVideoDelete.setVisibility(View.INVISIBLE);
            if(mVideoCount>=90)mVideoGO.setVisibility(View.VISIBLE);
            else mVideoGO.setVisibility(View.INVISIBLE);
        }
        else if(event.getAction()==MotionEvent.ACTION_DOWN)
        {
//            new File(movieFilePath).delete();

            if(!mAudioDelete) return;

            if(mDeleteState)
            {
                mTextsBar.get(mTextsBar.size()-1).setAlpha(1.0f);
                mTextsBar.get(mTextsBar.size()-2).setAlpha(1.0f);
                mDeleteState = false;
            }

            if(!isRecorder)
            {
                TextView mBar = new TextView(mCtx);
                mBar.setLayoutParams(new LinearLayout.LayoutParams(0,(int)convertDpToPixel(5)));
                mBar.setBackgroundColor(getResources().getColor(R.color.light_gray));
//                mBar.getLayoutParams().width = (int)(oneW * (double)225);
                mVideoBar.addView(mBar);
                mTextsBar.add(mBar);

                mAudiosPos.add(0);

                isRecorder = true;
                Mp4Recorder.instance().init(movieFilePath, Mp4Recorder.MOVIE_MODE);

//                handler.postDelayed(new Runnable()
//                {
//                    @Override
//                    public void run()
//                    {
                        isRecording = true;
//                    }
//                }, 300);
            }
            else
            {
                TextView mBar = new TextView(mCtx);
                mBar.setLayoutParams(new LinearLayout.LayoutParams(0,(int)convertDpToPixel(5)));
                mBar.setBackgroundColor(getResources().getColor(R.color.light_gray));
//                mBar.getLayoutParams().width = (int)(oneW * (double)225);
                mVideoBar.addView(mBar);
                mTextsBar.add(mBar);

                isRecording = true;
            }
        }
    }

//    private class GryAdapter extends BaseAdapter
//    {
//        @Override
//        public int getCount()
//        {
//            return 3;
//        }
//
//        @Override
//        public Object getItem(int i)
//        {
//            return null;
//        }
//
//        @Override
//        public long getItemId(int i)
//        {
//            return 0;
//        }
//
//        @Override
//        public View getView(int pos, View view, ViewGroup viewGroup)
//        {
//            TextView mText = new TextView(mCtx);
//            mText.setTextSize(16.0f);
//
//            if(pos==0) mText.setText(getString(R.string.camera_photo));
//            else if(pos==1) mText.setText(getString(R.string.camera_video));
//            else mText.setText(getString(R.string.camera_live));
//
//            mText.setTextColor(getResources().getColor(R.color.white));
//
//            if(POS == pos) mText.setTextColor(getResources().getColor(R.color.white));
//
//            return mText;
//        }
//    }

    private class MyPagerAdapter extends PagerAdapter
    {

        @Override
        public int getCount()
        {
            return mViewList.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mViewList.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            container.addView(mViewList.get(position));
            View view = mViewList.get(position);

            ImageView mBar = (ImageView) view.findViewById(R.id.bar);
            mBar.getLayoutParams().height = (int)convertDpToPixel(50);

            ImageView mSee = (ImageView) view.findViewById(R.id.see);
            mSee.getLayoutParams().height = mDisplayMetrics.widthPixels;
            mSee.getLayoutParams().width = mDisplayMetrics.widthPixels;

            mSee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (camera != null && mAutoFocusState) {
                        mAutoFocusState = false;

                        try {
                            camera.autoFocus(new Camera.AutoFocusCallback() {
                                @Override
                                public void onAutoFocus(boolean success, Camera cam) {
                                    mAutoFocusState = true;
                                }
                            });
                        } catch (Exception e) {

                        }
                    }
                }
            });

            return mViewList.get(position);
        }
    }

    private void initTitleBar()
    {
        mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.camera_video));
        mTitle.setTextColor(getResources().getColor(R.color.white));

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_white_back);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });

    }

    public float convertDpToPixel(float dp)
    {
        Resources resources = mCtx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public float pxFromDp(float dp)
    {
        return dp * mCtx.getResources().getDisplayMetrics().density;
    }

    private void takePic(byte[] data, Camera camera)
    {
        if(!mTaking)
        {
            mTaking = true;
            try
            {
                camera.stopPreview();

                // Get original bitmap
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;

                Bitmap originalBitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);

                // Get large: 1080*1080
                Bitmap largeBitmap = Bitmap.createBitmap(1080, 1080, Bitmap.Config.ARGB_8888);
                Canvas largeCanvas = new Canvas(largeBitmap);

                int cropOffset = (originalBitmap.getWidth() - originalBitmap.getHeight()) / 2;
                largeCanvas.drawBitmap(originalBitmap, new Rect(cropOffset, 0, originalBitmap.getWidth()-cropOffset, originalBitmap.getHeight()), new Rect(0, 0, 1080, 1080), new Paint(Paint.FILTER_BITMAP_FLAG));
                originalBitmap.recycle();

                String name = Singleton.getUUIDFileName("jpg");
                FileOutputStream outStream = null;
                String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + name;
                outStream = new FileOutputStream(file);
                largeBitmap.compress(Bitmap.CompressFormat.JPEG, 95, outStream);
                outStream.close();

                // Get small bitmap: 640*640
                Bitmap smallBitmap = Bitmap.createBitmap(640, 640, Bitmap.Config.ARGB_8888);
                Canvas smallCanvas = new Canvas(smallBitmap);
                smallCanvas.drawBitmap(largeBitmap, new Rect(0, 0, 1080, 1080), new Rect(0, 0, 640, 640), new Paint(Paint.FILTER_BITMAP_FLAG));
                largeBitmap.recycle();

                FileOutputStream outStream2 = null;
                String file2 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "THUMBNAIL_" + name;
                outStream2 = new FileOutputStream(file2);
                smallBitmap.compress(Bitmap.CompressFormat.JPEG, 95, outStream2);
                outStream2.close();
                smallBitmap.recycle();

                // Enter Camera Filter
                Intent intent = new Intent();
                intent.setClass(mCtx, CameraFilterViewController.class);
                intent.putExtra("file", file);
                intent.putExtra("file2", file2);
                intent.putExtra("name", name);
                intent.putExtra("name2", "THUMBNAIL_" + name);
                if(isCame) intent.putExtra("isCame", 1);
                else intent.putExtra("isCame", 0);
                startActivity(intent);

                /// Finish Camera Activity
                mCtx.finish();
            } catch (FileNotFoundException e) {

                try {
//                          showToast(getString(R.string.failed));
                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }
            } catch (IOException e) {
                try {
//                          showToast(getString(R.string.failed));
                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }
            }
        }
    }

    private class OpenAsyncTask extends AsyncTask<String, Integer, Boolean>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            try
            {
                camera = Camera.open();
                camera.setDisplayOrientation(90);
                camera.setPreviewDisplay(surfaceHolder);

                mFlashState = 1;

                Camera.Parameters parameters = camera.getParameters();
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);

                if(captureMode==Constants.CAPTURE_MODE_MOVIE)
                {
                    parameters.setPreviewSize(640, 480); // force onPreviewFrame callback to get 640*480 video frame buffer data
                    camera.setParameters(parameters);
                } else {
                    // choose optimal picture size : smallest one with height > 1080
                    int targetWidth = 0;
                    int targetHeight = 0;

                    List<Camera.Size> sizes = parameters.getSupportedPictureSizes();

                    for (int i = 0; i < sizes.size(); i++) {
                        int w = sizes.get(i).width;
                        int h = sizes.get(i).height;

                        // check it is 4:3 resolution
                        if(Math.abs((float)w/(float)h-4.0/3.0)>0.01) {
                            continue;
                        }

                        if(targetHeight==0 || (h>1080 && targetHeight>h)) {
                            targetHeight = h;
                            targetWidth = w;
                        }

//                        Singleton.log("w: "+sizes.get(i).width+", h: "+sizes.get(i).height);
                    }

//                    Singleton.log("target w: "+targetWidth+", target h: "+targetHeight);

                    parameters.setPictureSize(targetWidth, targetHeight);

                    // choose a preview size that is 16:9
                    int targetPreviewWidth = 0;
                    int targetPreviewHeight = 0;

                    List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();

                    for (int i = 0; i < previewSizes.size(); i++) {
                        int w = previewSizes.get(i).width;
                        int h = previewSizes.get(i).height;

                        // check it is 4:3 resolution
                        if(Math.abs((float)w/(float)h-4.0/3.0)>0.01) {
                            continue;
                        }

                        if(h>targetPreviewHeight) {
                            targetPreviewHeight = h;
                            targetPreviewWidth = w;
                        }

//                        Singleton.log("w: "+sizes.get(i).width+", h: "+sizes.get(i).height);
                    }

                    if(targetPreviewWidth>0 && targetHeight>0) {
                        parameters.setPreviewSize(targetPreviewWidth, targetPreviewHeight);
                    }

//                    Singleton.log("targetPreviewWidth w: " + targetPreviewWidth + ", targetPreviewHeight h: " + targetPreviewHeight);

                    parameters.setFocusMode("auto");
                    camera.setParameters(parameters);
                }

                camera.startPreview();
                camera.setPreviewCallback(VideoActivity.this);

                return true;
            }
            catch (RuntimeException e)
            {
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);

            mProgressBar.setVisibility(View.GONE);

            if(((deH/3)*4)==deW) {
                RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.widthPixels/3*4);
                mSurParams.topMargin = (int)(((mDisplayMetrics.widthPixels/3*4)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1 ;
                surfaceView.setLayoutParams(mSurParams);
            }
            else
            {
                RelativeLayout.LayoutParams mSurParams = new RelativeLayout.LayoutParams(mDisplayMetrics.widthPixels,mDisplayMetrics.heightPixels);
                mSurParams.topMargin = (int)(((mDisplayMetrics.heightPixels)/2) - ((mDisplayMetrics.widthPixels/2) + convertDpToPixel(50))) * -1;
                surfaceView.setLayoutParams(mSurParams);
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        new OpenAsyncTask().execute(null, null, null);

//        captureMode = Constants.CAPTURE_MODE_MOVIE;
//        if(mChangePreviewTask!=null) mChangePreviewTask = null;
//        mChangePreviewTask = new ChangePreviewTask();
//        mChangePreviewTask.execute(1);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        refreshCamera();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        if(pcmAudioRecorder!=null) pcmAudioRecorder.stopRecording();

        if(camera!=null)
        {
            try
            {
                camera.stopPreview();
                camera.setPreviewCallback(null);
                camera.release();
                camera = null;
            }
            catch (Exception e)
            {

            }

        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if(camera!=null)
        {
            camera.stopPreview();
            camera.setPreviewCallback(null);
        }

        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    public void onResume()
    {
        super.onResume();

        refreshCamera();

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void refreshCamera()
    {
        try
        {
            if(camera!=null)
            {
                camera.startPreview();
                camera.setPreviewCallback(VideoActivity.this);
            }
        }
        catch (Exception e)
        {
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        try
        {
            if(resultCode!=RESULT_OK)
            {
                return;
            }

            if(requestCode==Constants.PICK_IMAGE_REQUEST)
            {
                boolean hasGotPicture = false;

                if(data!=null && data.getData()!=null)
                {
                    String selectedImageFilePath = Singleton.getRealPathFromURI(data.getData(), this);
                    String tmpImageFilePath = Singleton.getExternalTempImagePath();

                    if(selectedImageFilePath==null)
                    {
                        try
                        {
                            Uri selectedImageURI = data.getData();
                            InputStream input = contentResolver.openInputStream(selectedImageURI);
                            BitmapHelper.saveBitmap(BitmapFactory.decodeStream(input, null, BitmapHelper.getBitmapOptions(2)), tmpImageFilePath, Bitmap.CompressFormat.JPEG);
                            hasGotPicture = true;
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    else
                    {
                        Singleton.copyFile(new File(selectedImageFilePath), new File(tmpImageFilePath));
                        hasGotPicture = true;
                    }
                }

                startActivityForResult(new Intent().setClass(this, CropImageActivity.class), Constants.CROP_IMAGE_REQUEST);

            }

            if(requestCode==Constants.CROP_IMAGE_REQUEST)
            {
                mTaking = true;

                final String pictureFileName = Singleton.getUUIDFileName("jpg");
                final String thumbnailPictureFileName = "THUMBNAIL_" + pictureFileName;

                int largeSize = Constants.PROFILE_PICTURE_PIXELS;
                int thumbnailSize = Constants.PROFILE_PICTURE_THUMBNAIL_PIXELS;
                if(getConfig(Constants.PHONE_WIDTH_HEIGHT_SIZE,Constants.DEFAULT_PIXELS) < Constants.CHECK_PIXELS)
                {
                    largeSize = Constants.LOW_PROFILE_PICTURE_PIXELS;
                    thumbnailSize = Constants.LOW_PROFILE_PICTURE_THUMBNAIL_PIXELS;
                }

                Bitmap thumbnail = BitmapHelper.getBitmap(Singleton.getExternalTempImagePath(), thumbnailSize);
                BitmapHelper.saveBitmap(thumbnail, Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName, Bitmap.CompressFormat.JPEG);
                thumbnail.recycle();
                thumbnail = null;

                Bitmap large = BitmapHelper.getBitmap(Singleton.getExternalTempImagePath(), largeSize);
                BitmapHelper.saveBitmap(large, Singleton.getExternalMediaFolderPath() + pictureFileName, Bitmap.CompressFormat.JPEG);
                mView.setBackgroundDrawable(new BitmapDrawable(large));
                large.recycle();
                large = null;

                new File(Singleton.getExternalTempImagePath()).delete();

                Intent intent = new Intent();
                intent.setClass(mCtx, CameraFilterViewController.class);
                intent.putExtra("file", Singleton.getExternalMediaFolderPath() + pictureFileName);
                intent.putExtra("file2", Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName);
                intent.putExtra("name", pictureFileName);
                intent.putExtra("name2", thumbnailPictureFileName);
                intent.putExtra("isCame", 2);
                startActivity(intent);

                mCtx.finish();
            }
        }
        catch (Exception e)
        {
            try {
//                          showToast(getString(R.string.failed));
                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
            } catch (Exception x) {
            }
        }
    }

    @Override
    public void onPreviewFrame(final byte[] sourceYUVData, Camera camera)
    {
        if(!isRecording)
        {
            return;
        }

        mVideoOne++;
        mVideoCount++;

        if(mVideoOne==1)
        {
            LinearLayout.LayoutParams mBar = new LinearLayout.LayoutParams(0,(int)convertDpToPixel(5));
            mLayoutParamsBar = mBar;
        }

        mLayoutParamsBar.width = (int) (oneW * (double) mVideoOne);
        mTextsBar.get(mTextsBar.size()-1).setLayoutParams(mLayoutParamsBar);

        if(mVideoCount==450)
        {
            isRecording = false;

            isRecording = false;
            mVideoOne = 0;

            int pos = mVideoCount;
            mVideoPos.add(pos);

            int Apos = mAudioList.size();
            mAudiosPos.add(Apos);

            TextView mBarEnd = new TextView(mCtx);
            mBarEnd.setLayoutParams(mLayoutParams);
            mBarEnd.setBackgroundColor(Color.WHITE);
            mVideoBar.addView(mBarEnd);
            mTextsBar.add(mBarEnd);

            if(mVideoCount!=0)mVideoDelete.setVisibility(View.VISIBLE);
            else mVideoDelete.setVisibility(View.INVISIBLE);
            if(mVideoCount>=90)mVideoGO.setVisibility(View.VISIBLE);
            else mVideoGO.setVisibility(View.INVISIBLE);
        }

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    fillVideoFrame(sourceYUVData);
                    FileOutputStream out = new FileOutputStream(mVideoPath + mVideoName + mVideoCount);
                    out.write(yuvData);
                    out.close();

                    if(mVideoCount==1)
                    {
                        if(mMessage!=null) mMessage = null;
                        mMessage = new Message();
                        mMessage.what = 4;
                        VideoHandler.sendMessage(mMessage);
                    }
                }
                catch (Exception e)
                {
                }
            }
        }).start();
    }

    private void fillVideoFrame(byte[] sourceYUVData)
    {
        int yDestOffset = 0;
        int ySrcOffset = 80;

        if(cameraPosition==0) {
            yDestOffset += (480 -1);
        }

        for(int y=0;y<480;y++) {
            for(int x=0;x<480;x++) {
                yuvData[yDestOffset] = sourceYUVData[ySrcOffset];

                if(cameraPosition==0) {
                    yDestOffset -= 1;
                } else {
                    yDestOffset += 1;
                }

                ySrcOffset += 1;
            }

            if(cameraPosition==0) {
                yDestOffset += 480 *2;
            }

            ySrcOffset += 160;
        }


        int uDestOffset = 480*480;
        int vDestOffset = 480*480 * 5/4;
        int uvSrcOffset = 640*480 + 80;

        if(cameraPosition==0){
            uDestOffset += (240 - 1);
            vDestOffset += (240 - 1);
        }

        for(int y=0;y<240;y++) {
            for (int x = 0; x < 240; x++) {
                yuvData[uDestOffset] = sourceYUVData[uvSrcOffset + 1];
                yuvData[vDestOffset] = sourceYUVData[uvSrcOffset];

                if (cameraPosition == 0) {
                    uDestOffset -= 1;
                    vDestOffset -= 1;
                } else {
                    uDestOffset += 1;
                    vDestOffset += 1;
                }

                uvSrcOffset += 2;
            }

            if (cameraPosition == 0) {
                uDestOffset += 240 *2;
                vDestOffset += 240 *2;
            }

            uvSrcOffset += 160;
        }
    }

    private Runnable TimeRun = new Runnable()
    {
        @Override
        public void run()
        {
            if(mTime.isShown()) mTime.setVisibility(View.GONE);
            else mTime.setVisibility(View.VISIBLE);

            mTimeHandler.postDelayed(TimeRun,500);
        }
    };

    private Handler VideoHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 1:
                    mTextsBar.get(mTextsBar.size()-1).setLayoutParams(new LinearLayout.LayoutParams((int)(oneW * (double)mVideoOne),(int)convertDpToPixel(5)));
                    break;
                case 2:
                    TextView mBar = new TextView(mCtx);
                    mBar.setLayoutParams(mLayoutParams);
                    mBar.setBackgroundColor(Color.WHITE);
                    mVideoBar.addView(mBar);
                    mTextsBar.add(mBar);
                    break;
                case 3:
                    mAudioDelete = true;
                    mProgressBar.setVisibility(View.GONE);
                    break;
                case 4:
                    camera.takePicture(null, null, new Camera.PictureCallback()
                    {
                        public void onPictureTaken(final byte[] data, Camera take_camera)
                        {
                            take_camera.getParameters().setPreviewSize(640, 480);
                            take_camera.getParameters().setPictureSize(640,480);
                            take_camera.startPreview();

                            new Thread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    try
                                    {
                                        Bitmap b = BitmapFactory.decodeByteArray(data, 0, data.length);

                                        Matrix m = new Matrix();
                                        if(isCame) m.postRotate(-90f);
                                        else m.postRotate(90f);

                                        Bitmap bit = Bitmap.createBitmap(b,(b.getWidth() - b.getHeight())/2,0,b.getHeight(),b.getHeight(),m,true);

                                        b.recycle();
                                        b=null;

                                        Bitmap bigBitmap = BitmapHelper.getResizedBitmap(bit,1080,1080);

                                        bit.recycle();
                                        bit=null;

                                        String name = movieName.substring(0,movieName.length()-3) + "jpg";
                                        FileOutputStream outStream = null;
                                        String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + name;
                                        outStream = new FileOutputStream(file);
                                        bigBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                                        outStream.close();

                                        Bitmap smailBitmap = BitmapHelper.getResizedBitmap(bigBitmap,640,640);

                                        bigBitmap.recycle();
                                        bigBitmap=null;

                                        FileOutputStream outStream2 = null;
                                        String file2 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "THUMBNAIL_" + name;
                                        outStream2 = new FileOutputStream(file2);
                                        smailBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream2);
                                        outStream2.close();

                                        smailBitmap.recycle();
                                        smailBitmap=null;
                                    }
                                    catch (FileNotFoundException e)
                                    {
                                    }
                                    catch (IOException e)
                                    {
                                    }
                                }
                            }).start();
                        }
                    });
                    break;
                case 5:
                    isRecording = false;
                    mVideoOne = 0;

                    int pos = mVideoCount;
                    mVideoPos.add(pos);

                    int Apos = mAudioList.size();
                    mAudiosPos.add(Apos);

                    TextView mBarEnd = new TextView(mCtx);
                    mBarEnd.setLayoutParams(mLayoutParams);
                    mBarEnd.setBackgroundColor(Color.WHITE);
                    mVideoBar.addView(mBarEnd);
                    mTextsBar.add(mBarEnd);

                    if(mVideoCount!=0)mVideoDelete.setVisibility(View.VISIBLE);
                    else mVideoDelete.setVisibility(View.INVISIBLE);
                    if(mVideoCount>=90)mVideoGO.setVisibility(View.VISIBLE);
                    else mVideoGO.setVisibility(View.INVISIBLE);
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Override
    public void didGetAudioData(final float[] pcmData, final int numOfSamples)
    {
        if(!isRecording)
        {
            return;
        }

        if(audioSize==0) audioSize = numOfSamples;
        mAudioList.add(pcmData);
    }

    public int getConfig(String key, int def)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getInt(key, def);
    }
}
