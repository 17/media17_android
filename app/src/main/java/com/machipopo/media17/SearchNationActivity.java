package com.machipopo.media17;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;


public class SearchNationActivity extends BaseActivity
{
    private SearchNationActivity mCtx = this;
    //private SearchView searchView;
    private EditText searchView;
    private ListView listView;
    private String[] mListCountry ;
    private String[] mListCode ;
    private String[] mListCountryAbbrv;
    private HashMap<String,String> CODE = new HashMap<String,String>();
    private HashMap<String,String> NAME = new HashMap<String,String>();
    private LayoutInflater inflater;
    private NationAdapter mNationAdapter;

    //private String[] nation;
    private ArrayAdapter<String> nationList;

    ArrayList<String> mListNation = new ArrayList<String>();
    ArrayList<String> mListAbbreviation = new ArrayList<String>();
    ArrayList<String> mListCountryCode = new ArrayList<String>();
    private InputMethodManager inputMethodManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_nation);

        initTitleBar();
        searchView = (EditText) findViewById(R.id.search_view);
        listView = (ListView) findViewById(R.id.list);

        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        try {
            String code = ReadFromfile("country_code.txt", mCtx);
            JSONArray mCodeArray = new JSONArray(code);
            mListCountry = new String[mCodeArray.length()];
            mListCountryAbbrv = new String[mCodeArray.length()];
            mListCode = new String[mCodeArray.length()];
            //nation = new String[mCodeArray.length()];
            for (int i = 0; i < mCodeArray.length(); i++) {
                CODE.put(mCodeArray.getJSONObject(i).getString("country"), mCodeArray.getJSONObject(i).getString("countryCode"));
                NAME.put(mCodeArray.getJSONObject(i).getString("countryCode"), mCodeArray.getJSONObject(i).getString("country"));
                mListCode[i] = mCodeArray.getJSONObject(i).getString("countryCode");
                mListCountry[i] = getResources().getString(getResources().getIdentifier(mCodeArray.getJSONObject(i).getString("country"), "string", getPackageName()));
                mListCountryAbbrv[i] = mCodeArray.getJSONObject(i).getString("country");
                //nation[i] = mListCountry[i]+ " ( "+ mListCountryAbbrv[i]+ ", +"+ mListCode[i]+ " )";
            }
        } catch (JSONException e) {
        }

        mListNation.clear();
        mListAbbreviation.clear();
        mListCountryCode.clear();
        for (int i = 0; i < mListCode.length; i++) {
            //mList.add(nation[i]);
            mListNation.add(mListCountry[i]);
            mListAbbreviation.add(mListCountryAbbrv[i]);
            mListCountryCode.add(mListCode[i]);
        }

        mNationAdapter = new NationAdapter();
        listView.setAdapter(mNationAdapter);

//

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                
                mListNation.clear();
                mListAbbreviation.clear();
                mListCountryCode.clear();

                //支援選單搜尋功能
                if (s.toString().length() != 0) {
                    for(int i = 0 ; i < mListCode.length ; i++)
                    {
                        if((mListCountry[i].toLowerCase().contains(s.toString().toLowerCase())) ||(mListCountryAbbrv[i].toLowerCase().contains(s.toString().toLowerCase()))||(mListCode[i].toLowerCase().contains(s.toString().toLowerCase())) )
                        {
                            mListNation.add(mListCountry[i]);
                            mListAbbreviation.add(mListCountryAbbrv[i]);
                            mListCountryCode.add(mListCode[i]);
                        }
                    }
                }
                else
                {
                    for(int i = 0 ; i < mListCode.length ; i++)
                    {
                        mListNation.add(mListCountry[i]);
                        mListAbbreviation.add(mListCountryAbbrv[i]);
                        mListCountryCode.add(mListCode[i]);
                    }
                }
                mNationAdapter.notifyDataSetChanged();

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }, 200);
    }


    private void initTitleBar()
    {
        final Intent intent = new Intent();
        RelativeLayout mTitleBarLayout = (RelativeLayout) findViewById(R.id.title_bar_layout);
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.nation));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        ImageView img = (ImageView) findViewById(R.id.img_left);

        img.setImageResource(R.drawable.nav_arrow_white_back);
        mTitleBarLayout.setBackgroundColor(Color.BLACK);
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.nation));
        ((TextView) findViewById(R.id.title_name)).setTextColor(Color.WHITE);

        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });

    }



    public String ReadFromfile(String fileName, Context context)
    {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try
        {
            fIn = context.getResources().getAssets().open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null)
            {
                returnString.append(line);
            }
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally
        {
            try
            {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            }
            catch (Exception e2)
            {
                e2.getMessage();
            }
        }

        return returnString.toString();
    }


    public class NationAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mListNation.size();
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.nation_listview, null);
                holder.nation = (TextView) convertView.findViewById(R.id.nation);
                holder.abbreviation = (TextView) convertView.findViewById(R.id.abbreviation);
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.layout.setBackgroundResource(R.drawable.btn_white_c2);
            holder.nation.setText(mListNation.get(i));
            holder.abbreviation.setText(mListAbbreviation.get(i));
            holder.code.setText(mListCountryCode.get(i));

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Singleton.preferenceEditor.putString(Constants.COUNTRY_CALLING_CODE, mListCountryCode.get(i)).commit();
                    Singleton.preferenceEditor.putString(Constants.PROFILE_COUNTRY, mListNation.get(i)).commit();
                    Singleton.preferenceEditor.putString(Constants.PHONE_TWO_DIGIT_ISO, mListAbbreviation.get(i)).commit();

                    mCtx.finish();
                }
            });

            return convertView;
        }
    }

    private class ViewHolder
    {
        LinearLayout layout;
        TextView nation;
        TextView abbreviation;
        TextView code;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            mCtx.finish();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
