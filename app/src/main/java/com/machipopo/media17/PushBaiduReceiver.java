package com.machipopo.media17;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;

import com.baidu.android.pushservice.PushMessageReceiver;
import com.machipopo.media17.utils.DevUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by POPO on 15/9/29.
 */
public class PushBaiduReceiver extends PushMessageReceiver
{
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;

    @Override
    public void onBind(Context context, int errorCode, String appid,String userId, String channelId, String requestId)
    {
//        String responseString = "onBind errorCode=" + errorCode + " appid="+ appid + " userId=" + userId + " channelId=" + channelId+ " requestId=" + requestId;
//        Log.d("123", responseString);

        if (errorCode == 0)
        {
            try
            {
                JSONObject params = new JSONObject();
                params.put("pushToken", channelId);
                params.put("deviceModel", DevUtils.getDevInfo());
                params.put("deviceType", "ANDROID");
                params.put("packageName", Constants.PackgeName);
                Singleton.preferenceEditor.putString("baidu", channelId).commit();

                ApiManager.updateUserInfo(context, params, new ApiManager.UpdateUserInfoCallback()
                {
                    @Override
                    public void onResult(boolean success, String message)
                    {
                        if(success)
                        {
                        }
                    }
                });
            }
            catch (JSONException e)
            {

            }
        }
    }

    @Override
    public void onMessage(Context context, String message, String customContentString)
    {
//        String messageString = "透传消息 message=\"" + message+ "\" customContentString=" + customContentString;
//        Log.d("123", messageString);

        if(message!=null && message.length()!=0)
        {
            try
            {
                String type = "";
                String openid = "";

                JSONObject json = new JSONObject(message);

                if(json.has("message")) type = json.getString("message");
                if(json.has("senderOpenID")) openid = json.getString("senderOpenID");

                if(type.compareTo("NEW_SYSTEM_NOTIF")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_SYSTEM,1)==1)
                    {
                        setConfig(context,"page","");
                        sendNotification(context,context.getString(R.string.gcm_system),1);
                    }
                }
                else if(type.compareTo("NEW_FRIEND_JOIN_FROM_FB")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_FRIEND,1)==1)
                    {
                        setConfig(context,"page","");
                        sendNotification(context,context.getString(R.string.notifi_facebook_me)+ " " + openid + " " + context.getString(R.string.notifi_17),2);
                    }
                }
                else if(type.compareTo("NEW_FRIEND_JOIN_FROM_CONTACTS")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_FRIEND,1)==1)
                    {
                        setConfig(context,"page","");
                        sendNotification(context,context.getString(R.string.notifi_contast_me) + " " + openid + " " + context.getString(R.string.notifi_17),3);
                    }
                }
                else if(type.compareTo("NEW_COMMENT_TAG")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_TAG,2)!=0)
                    {
                        setConfig(context,"page","p");
                        if(json.has("postID")) setConfig(context,"ID",json.getString("postID"));
                        sendNotification(context,openid + " " + context.getString(R.string.notifi_tag_me),4);
                    }
                }
                else if(type.compareTo("NEW_POST_TAG")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_TAG,2)!=0)
                    {
                        setConfig(context,"page","p");
                        if(json.has("postID")) setConfig(context,"ID",json.getString("postID"));
                        sendNotification(context,openid + " " + context.getString(R.string.gcm_photo_tag),5);
                    }
                }
                else if(type.compareTo("NEW_COMMENT")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_COMMENT,2)!=0)
                    {
                        setConfig(context,"page","p");
                        if(json.has("postID")) setConfig(context,"ID",json.getString("postID"));
                        sendNotification(context,openid + " " + context.getString(R.string.notifi_comment_me),6);
                    }
                }
                else if(type.compareTo("NEW_FOLLOW")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_FANS,1)==1)
                    {
                        setConfig(context,"page","");
                        sendNotification(context,openid + " " + context.getString(R.string.notifi_start_me),7);
                    }
                }
                else if(type.compareTo("NEW_LIKE")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_LIKE,2)!=0)
                    {
                        setConfig(context,"page","p");
                        if(json.has("postID")) setConfig(context,"ID",json.getString("postID"));
                        sendNotification(context,openid + " " + context.getString(R.string.notifi_photo_like_me),8);
                    }
                }
                else if(type.compareTo("NEW_LIVE_STREAM")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_LIVE,1)==1)
                    {
                        setConfig(context,"page","live");
                        if(json.has("liveStreamID")) setConfig(context,"ID",String.valueOf(json.getInt("liveStreamID")));
                        sendNotification(context,openid + " " + context.getString(R.string.live_new),9);
                    }
                }
                else if(type.compareTo("NEW_LIVE_RESTREAM")==0)
                {
                    if(getConfig(context,Constants.SETTING_NOTIFI_RELIVE,1)==1)
                    {
                        String liveID = "";
                        if(json.has("liveStreamerOpenID")) liveID = json.getString("liveStreamerOpenID");

                        setConfig(context,"page","live");
                        if(json.has("liveStreamID")) setConfig(context,"ID",String.valueOf(json.getInt("liveStreamID")));
                        sendNotification(context,openid + " " + context.getString(R.string.live_relive) + " " + String.format(context.getString(R.string.live_relive2),liveID), 10);
                    }
                }
            }
            catch (JSONException e)
            {

            }

        }
    }

    @Override
    public void onNotificationClicked(Context context, String title,String description, String customContentString)
    {
//        String notifyString = "通知点击 title=\"" + title + "\" description=\""+ description + "\" customContent=" + customContentString;
//        Log.d("123", notifyString);
//
//        if (!TextUtils.isEmpty(customContentString))
//        {
//            JSONObject customJson = null;
//            try
//            {
//                customJson = new JSONObject(customContentString);
//                String myvalue = null;
//                if (!customJson.isNull("mykey"))
//                {
//                    myvalue = customJson.getString("mykey");
//                }
//            }
//            catch (JSONException e)
//            {
//                e.printStackTrace();
//            }
//        }
    }

    @Override
    public void onNotificationArrived(Context context, String title,String description, String customContentString)
    {
//        String notifyString = "onNotificationArrived  title=\"" + title+ "\" description=\"" + description + "\" customContent="+ customContentString;
//        Log.d("123", notifyString);
//
//        if (!TextUtils.isEmpty(customContentString))
//        {
//            JSONObject customJson = null;
//            try
//            {
//                customJson = new JSONObject(customContentString);
//                String myvalue = null;
//                if (!customJson.isNull("mykey"))
//                {
//                    myvalue = customJson.getString("mykey");
//                }
//            }
//            catch (JSONException e)
//            {
//                e.printStackTrace();
//            }
//        }
    }

    @Override
    public void onSetTags(Context context, int errorCode,List<String> sucessTags, List<String> failTags, String requestId)
    {
//        String responseString = "onSetTags errorCode=" + errorCode+ " sucessTags=" + sucessTags + " failTags=" + failTags+ " requestId=" + requestId;
//        Log.d("123", responseString);
    }

    @Override
    public void onDelTags(Context context, int errorCode,List<String> sucessTags, List<String> failTags, String requestId)
    {
//        String responseString = "onDelTags errorCode=" + errorCode+ " sucessTags=" + sucessTags + " failTags=" + failTags+ " requestId=" + requestId;
//        Log.d("123", responseString);
    }

    @Override
    public void onListTags(Context context, int errorCode, List<String> tags,String requestId)
    {
//        String responseString = "onListTags errorCode=" + errorCode + " tags="+ tags;
//        Log.d("123", responseString);
    }

    @Override
    public void onUnbind(Context context, int errorCode, String requestId)
    {
//        String responseString = "onUnbind errorCode=" + errorCode+ " requestId = " + requestId;
//        Log.d("123", responseString);
//
//        if (errorCode == 0)
//        {
//            // 解绑定成功
//        }
    }

    private void sendNotification(Context context, String msg,int type)
    {
        try{
            if(Singleton.preferences.getInt(Constants.LOGOUT, 1)==1) return;
        }
        catch (Exception e){
        }

        try{
            if(getConfig(context,Constants.SETTING_NOTIFICATION_OPEN,1)==0) return;
        }
        catch (Exception e){
        }

        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(context, MyNotificationActivity.class), 0);

        if(getConfig(context,Constants.SETTING_NOTIFICATION_SOUND,1)==1 && getConfig(context,Constants.SETTING_NOTIFICATION_VIBRATE,1)==0)
        {
            NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(context)
            .setSmallIcon(R.mipmap.notifibage)
            .setContentTitle("17")
            .setStyle(new NotificationCompat.BigTextStyle()
            .bigText(msg))
            .setAutoCancel(true)
            .setDefaults(Notification.DEFAULT_SOUND)
            .setContentText(msg);

            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
        else if(getConfig(context,Constants.SETTING_NOTIFICATION_SOUND,1)==0 && getConfig(context,Constants.SETTING_NOTIFICATION_VIBRATE,1)==1)
        {
            NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(context)
            .setSmallIcon(R.mipmap.notifibage)
            .setContentTitle("17")
            .setStyle(new NotificationCompat.BigTextStyle()
            .bigText(msg))
            .setAutoCancel(true)
            .setDefaults(Notification.DEFAULT_VIBRATE)
            .setContentText(msg);

            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
        else
        {
            NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(context)
            .setSmallIcon(R.mipmap.notifibage)
            .setContentTitle("17")
            .setStyle(new NotificationCompat.BigTextStyle()
            .bigText(msg))
            .setAutoCancel(true)
            .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
            .setContentText(msg);

            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }

//        NotificationCompat.Builder mBuilder =
//        new NotificationCompat.Builder(context)
//        .setSmallIcon(R.mipmap.notifibage)
//        .setContentTitle("17")
//        .setStyle(new NotificationCompat.BigTextStyle()
//        .bigText(msg))
//        .setAutoCancel(true)
//        .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
//        .setContentText(msg);
//
//        mBuilder.setContentIntent(contentIntent);
//        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public int getConfig(Context context,String key,int def)
    {
        try
        {
            SharedPreferences settings = context.getSharedPreferences("settings",0);
            return settings.getInt(key, def);
        }
        catch (Exception e)
        {
            return 0;
        }
    }

    public void setConfig(Context context,String key, String value)
    {
        try
        {
            SharedPreferences settings = context.getSharedPreferences("settings", 0);
            SharedPreferences.Editor PE = settings.edit();
            PE.putString(key, value);
            PE.commit();
        }
        catch (Exception e)
        {
        }
    }
}
