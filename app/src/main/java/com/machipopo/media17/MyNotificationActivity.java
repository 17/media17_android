package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by POPO on 2015/11/16.
 */
public class MyNotificationActivity extends BaseNewActivity
{
    private MyNotificationActivity mCtx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);

        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.main_color));
            }
        } catch (Exception e) {
        }

        if(getConfig("page","").length()!=0) setConfig(this,"notifi","1");

        Intent intent = new Intent();
        intent.setClass(mCtx, StartActivity.class);
        startActivity(intent);
        mCtx.finish();
    }

    public void setConfig(Context context,String key, String value)
    {
        try
        {
            SharedPreferences settings = context.getSharedPreferences("settings", 0);
            SharedPreferences.Editor PE = settings.edit();
            PE.putString(key, value);
            PE.commit();
        }
        catch (Exception e)
        {
        }
    }

    public String getConfig(String key , String def)
    {
        try
        {
            SharedPreferences settings = getSharedPreferences("settings",0);
            return settings.getString(key, def);
        }
        catch (Exception e)
        {
            return "";
        }
    }
}
