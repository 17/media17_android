package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

/**
 * Created by POPO on 6/29/15.
 */
public class PostLikerActivity extends BaseNewActivity
{
    private PostLikerActivity mCtx = this;
    private LayoutInflater inflater;

    private String mUserID = "", mPostID = "", mTarget = "";
    private Boolean mAllUser = false;

    private PullToRefreshListView mList;
    private ProgressBar mProgress;
    private ImageView mNoData;

    private ArrayList<UserModel> mModels = new ArrayList<UserModel>();
    private MyAdapter mMyAdapter;

    private DisplayImageOptions SelfOptions;
    private Story17Application mApplication;

    //event tracking
    private int startSelfLikePage = 0;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_liker_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Bundle mBundle = getIntent().getExtras();
        if (mBundle != null)
        {
            if(mBundle.containsKey("post_id")) mPostID = mBundle.getString("post_id");
            if(mBundle.containsKey("user_id")) mUserID = mBundle.getString("user_id");
            if(mBundle.containsKey("target_id")) mTarget = mBundle.getString("target_id");
        }

        if(mTarget.length()!=0) mAllUser = true;

        initTitleBar();

        //event tracking
        startSelfLikePage = Singleton.getCurrentTimestamp();
        try{
            LogEventUtil.EnterReceivedLikePage(mCtx,mApplication);
        }catch (Exception x)
        {

        }


        mList = (PullToRefreshListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mProgress.setVisibility(View.VISIBLE);
        mApplication = (Story17Application) getApplication();

        if(mAllUser)
        {
            ApiManager.getUserLikers(mCtx, mUserID, mTarget, 0, 30, new ApiManager.GetUserLikersCallback()
            {
                @Override
                public void onResult(boolean success, String message, ArrayList<UserModel> models)
                {
                    mProgress.setVisibility(View.GONE);

                    if (success && models != null)
                    {
                        if (models.size() != 0)
                        {
                            mNoData.setVisibility(View.GONE);
                            mModels.clear();
                            mModels.addAll(models);

                            mMyAdapter = new MyAdapter();
                            mList.setAdapter(mMyAdapter);
                        }
                        else
                        {
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
        else
        {
            ApiManager.getPostLikers(mCtx, mUserID, mPostID, 0, 30, new ApiManager.GetPostLikersCallback()
            {
                @Override
                public void onResult(boolean success, String message, ArrayList<UserModel> models)
                {
                    mProgress.setVisibility(View.GONE);

                    if (success && models != null)
                    {
                        if (models.size() != 0)
                        {
                            mNoData.setVisibility(View.GONE);
                            mModels.clear();
                            mModels.addAll(models);

                            mMyAdapter = new MyAdapter();
                            mList.setAdapter(mMyAdapter);
                        }
                        else
                        {
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        if(mAllUser) mTitle.setText(getString(R.string.like_user));
        else mTitle.setText(getString(R.string.like_photo));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class MyAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderGrid holder = new ViewHolderGrid();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.post_liker_row, null);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.like = (TextView) convertView.findViewById(R.id.like);
                holder.love = (ImageView) convertView.findViewById(R.id.love);
                holder.follow = (Button) convertView.findViewById(R.id.follow);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderGrid) convertView.getTag();

            final int pos = position;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getPicture()), holder.pic,SelfOptions);
            holder.pic.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (mModels.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mModels.get(pos).getName());
                        intent.putExtra("picture", mModels.get(pos).getPicture());
                        intent.putExtra("isfollowing", mModels.get(pos).getIsFollowing());
                        intent.putExtra("post", mModels.get(pos).getPostCount());
                        intent.putExtra("follow", mModels.get(pos).getFollowerCount());
                        intent.putExtra("following", mModels.get(pos).getFollowingCount());
                        intent.putExtra("open", mModels.get(pos).getOpenID());
                        intent.putExtra("bio", mModels.get(pos).getBio());
                        intent.putExtra("targetUserID", mModels.get(pos).getUserID());
                        intent.putExtra("web", mModels.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setText(mModels.get(position).getOpenID());
            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (mModels.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mModels.get(pos).getName());
                        intent.putExtra("picture", mModels.get(pos).getPicture());
                        intent.putExtra("isfollowing", mModels.get(pos).getIsFollowing());
                        intent.putExtra("post", mModels.get(pos).getPostCount());
                        intent.putExtra("follow", mModels.get(pos).getFollowerCount());
                        intent.putExtra("following", mModels.get(pos).getFollowingCount());
                        intent.putExtra("open", mModels.get(pos).getOpenID());
                        intent.putExtra("bio", mModels.get(pos).getBio());
                        intent.putExtra("targetUserID", mModels.get(pos).getUserID());
                        intent.putExtra("web", mModels.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.like.setText(String.valueOf(mModels.get(pos).getLikeCount()));
            holder.love.setImageResource(loves[(int) (Math.random() * loves.length)]);

            if (mModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
            {
                holder.follow.setVisibility(View.GONE);
            }
            else holder.follow.setVisibility(View.VISIBLE);

            if(mModels.get(position).getIsFollowing()==1)
            {
                holder.follow.setText(getString(R.string.user_profile_following));
                holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
                holder.follow.setTextColor(Color.WHITE);
            }
            else
            {
                if(mModels.get(position).getFollowRequestTime()!=0)
                {
                    holder.follow.setText(getString(R.string.private_mode_request_send));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    holder.follow.setText("+ " + getString(R.string.user_profile_follow));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
            }

            final Button btn = holder.follow;
            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mModels.get(pos).getIsFollowing()==1)
                    {
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mModels.get(pos).getUserID());
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mModels.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if (success)
                                {
                                    mModels.get(pos).setIsFollowing(0);
                                    btn.setText("+ " + getString(R.string.user_profile_follow));
                                    btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                }
                                else {
                                    try{
//                                      showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mModels.get(pos).getFollowRequestTime()!=0)
                        {
                            btn.setText("+ " + getString(R.string.user_profile_follow));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                            mModels.get(pos).setIsFollowing(0);
                            mModels.get(pos).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mModels.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                                      showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                                return ;
                            }

                            if(mModels.get(pos).getPrivacyMode().compareTo("private")==0)
                            {
                                btn.setText(getString(R.string.private_mode_request_send));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                mModels.get(pos).setIsFollowing(0);
                                mModels.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mModels.get(pos).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mModels.get(pos).setIsFollowing(0);
                                            mModels.get(pos).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                try{
                                    LogEventUtil.FollowUser(mCtx, mApplication, mModels.get(pos).getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mModels.get(pos).getUserID(), new ApiManager.FollowUserActionCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            mModels.get(pos).setIsFollowing(1);
                                            btn.setText(getString(R.string.user_profile_following));
                                            btn.setBackgroundResource(R.drawable.btn_green_selector);
                                            btn.setTextColor(Color.WHITE);
                                        }
                                        else {
                                            try{
//                                      showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            if(mModels.get(pos).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            return convertView;
        }
    }

    private class ViewHolderGrid
    {
        ImageView pic;
        TextView name;
        TextView like;
        ImageView love;
        Button follow;
        ImageView verifie;
    }

    @Override
    public void onDestroy()
    {
        //event tracking
        try{
            LogEventUtil.LeaveReceivedLikePage(mCtx,mApplication,(Singleton.getCurrentTimestamp()-startSelfLikePage),mModels.size());
        }catch (Exception x)
        {

        }

        super.onDestroy();
    }
}