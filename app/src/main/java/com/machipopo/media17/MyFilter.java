package com.machipopo.media17;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;

import com.machipopo.media.ShaderLoader;
import com.machipopo.media17.View.TiltEffectDrawingView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.WeakHashMap;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by POPO on 15/6/18.
 */
public class MyFilter implements GLSurfaceView.Renderer{
    //===========================================
    // Softreference
    //===========================================
    private static WeakHashMap<String, SoftReference<Bitmap>> textureCache;

    //===========================================
    // Filter constant
    //===========================================
    private static final String COLOR_MAP1 = "COLOR_MAP1"; // color map 1 pic name
    private static final String COLOR_MAP2 = "COLOR_MAP2"; // color map 2 pic name
    private static final String OVERLAY1 = "OVERLAY1"; // overlay 1 pic name
    private static final String OVERLAY2 = "OVERLAY2"; // overlay 2 pic name
    //===========================================
    // Texture constant
    //===========================================
    private static final String COLOR_MAP_TEXTURE1 = "colorMapTexture1";
    private static final String COLOR_MAP_TEXTURE2 = "colorMapTexture2";
    private static final String OVERLAY_TEXTURE1 = "overlayTexture1";
    private static final String OVERLAY_TEXTURE2 = "overlayTexture2";
    private static final String SHARED_COLOR_MAP = "shared_color_map";
    private static final String SHARED_COLOR_MAP_TEXTURE = "sharedColorMapTexture";
    //===========================================
    // Texture Bitmap
    //===========================================
    private Bitmap colorMapImage1, colorMapImage2, overlayImage1, overlayImage2, sharedColorMapImage;
    //===========================================
    // gaussian
    //===========================================—
    private static final float[] gaussianOffsetTap9 = new float[]{0.0f, 1.3846153846f, 3.2307692308f};
    private static final float[] gaussianWeightTap9 = new float[]{0.2270270270f, 0.3162162162f/2.0f, 0.0702702703f/2.0f};

    private static final float[] gaussianOffsetTap25 = new float[]{0.0f, 1.44827586f, 3.37931034f, 5.31034483f, 7.24137931f, 9.17241379f, 11.10344828f};
    private static final float[] gaussianWeightTap25 = new float[]{0.14944601f, 0.25281284f/2.0f, 0.12888498f/2.0f, 0.03730881f/2.0f, 0.00581436f/2.0f, 0.00044240f/2.0f, 0.00001361f/2.0f};

    private static final float[] gaussianOffsetTap49 = new float[]{0.0f, 1.47169811f, 3.43396226f, 5.39622642f, 7.35849057f, 9.32075472f, 11.28301887f, 13.24528302f, 15.20754717f, 17.16981132f, 19.13207547f, 21.09433962f, 23.05660377f};
    private static final float[] gaussianWeightTap49 = new float[]{0.11011603f, 0.20071415f/2.0f, 0.13842355f/2.0f, 0.07060717f/2.0f, 0.02643049f/2.0f, 0.00717399f/2.0f, 0.00138786f/2.0f, 0.00018683f/2.0f, 0.00001693f/2.0f, 0.00000098f/2.0f, 0.00000003f/2.0f, 0.0f, 0.0f};

    private static final float[] gaussianOffsetTap81 = new float[]{0.0f, 1.48235294f, 3.45882353f, 5.43529412f, 7.41176471f, 9.38823529f, 11.36470588f, 13.34117647f, 15.31764706f, 17.29411765f, 19.27058824f, 21.24705882f, 23.22352941f, 25.20000000f, 27.17647059f, 29.15294118f, 31.12941176f, 33.10588235f, 35.08235294f, 37.05882353f, 39.03529412f};
    private static final float[] gaussianWeightTap81 = new float[]{0.08679764f, 0.16377778f/2.0f, 0.12975631f/2.0f, 0.08523885f/2.0f, 0.04634210f/2.0f, 0.02079453f/2.0f, 0.00767262f/2.0f, 0.00231673f/2.0f, 0.00056902f/2.0f, 0.00011284f/2.0f, 0.00001790f/2.0f, 0.00000225f/2.0f, 0.00000022f/2.0f, 0.00000002f/2.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};

    //===========================================

    private int glProgram;
    private int vertexShader;
    private int fragmentShader;
    private int positionAttribute;
    private int inputTextureCoordinateAttribute;
    private int displayWidth;
    private int displayHeight;
    private int imageWidth;
    private int imageHeight;

    public static int TARGET_IMAGE_SIZE = 1080;
    private Context context;
    private String currentFilter = "NORMAL_FILTER";
    private float currentFilterPercentage = 1.0f;
    private String currentParameter = "";
    private String currentColorMapParameterName = "";
    private String currentColorMapIndexName = "";
    private float currentColorMapParameterValue = 0.0f;
    public float currentColorMapIndexValue = 0.0f;
    private float currentParameterValue = 0.0f;

    // rotation matrix
    int rotationMatrixHandle;

    private int[] outputTexture = new int[1];
    private int[] framebuffer = new int[1];
    FloatBuffer vertexBuffer;
    FloatBuffer textureCoordBuffer;

    private Map<String, Integer> textureMapping;
    private Map<String, String> filterParams;
    private Map<String, Integer> resFilterParams;
    private boolean isCreateOutputImg = false;
    private boolean isCreateFinalImg = false;
    private Map<String, Float> lastColorMapRecord = new HashMap<>();
    private Map<String, Float> currentColorMapRecord = new HashMap<>();
    private final Queue<Runnable> mRunOnDraw = new LinkedList<>();
    private CameraFilterViewController.RenderEventListener renderCallBack;
    //=====================================
    // Tilt
    //=====================================
    private float radiaX = 0.0f;
    private float radiaY = 0.0f;
    private float radiaRadius = 0.0f;
    private float linearX = 0.0f;
    private float linearY = 0.0f;
    private float linearRadius = 0.0f;
    private float linearAngle = 0.0f;
    //=====================================

    private int isCame = 0;

    private static final float[] squareVertices = {
            -1.0f, -1.0f,
            1.0f, -1.0f,
            -1.0f,  1.0f,
            1.0f,  1.0f,
    };

    private static final float[] squareTextureCoordinates = {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
    };
    private int flip;

    public MyFilter(Context context, int dWidth, int dHeight, int cam){
        this.context = context;

//        imageWidth = w;
//        imageHeight = h;
        displayWidth = dWidth;
        displayHeight = dHeight;
        isCame = cam;

        textureMapping = new HashMap<String, Integer>();
        textureCache = new WeakHashMap<String, SoftReference<Bitmap>>();
        filterParams = new HashMap<String, String>();
        resFilterParams = new HashMap<String, Integer>();

        // make vertex buffer and draw list buffer
        ByteBuffer vbf = ByteBuffer.allocateDirect(squareVertices.length * 4);
        vbf.order(ByteOrder.nativeOrder());
        vertexBuffer = vbf.asFloatBuffer();
        vertexBuffer.put(squareVertices);
        vertexBuffer.position(0);

        ByteBuffer tbf = ByteBuffer.allocateDirect(squareTextureCoordinates.length * 4);
        tbf.order(ByteOrder.nativeOrder());
        textureCoordBuffer = tbf.asFloatBuffer();
        textureCoordBuffer.put(squareTextureCoordinates);
        textureCoordBuffer.position(0);

        outputTexture = new int[1];
        framebuffer = new int[1];

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//        checkGlError("glActiveTexture");

        // generate output texture
        GLES20.glGenTextures(1, outputTexture, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, outputTexture[0]);
//        checkGlError("glBindTexture");
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        // create output frame buffer
//        GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
//        checkGlError("glActiveTexture");


        // Associate a two-dimensional texture image with the byte buffer

        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, TARGET_IMAGE_SIZE, TARGET_IMAGE_SIZE, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);//textureCoordBuffer //outPutBuffer
//        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D,0);

        GLES20.glGenFramebuffers(1, framebuffer, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, framebuffer[0]);

        // create frame buffer
//        GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, GLES20.GL_RGBA, imageSize.getWidth(), imageSize.getHeight());
//        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, outputTexture[0]);
//        try {
//            checkGlError("glBindTexture");
//        }catch(Exception e){
//            Log.e(CameraFilterViewController.dTag, e.getMessage());
//        }

        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, outputTexture[0], 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
    }

    public void setRenderListener(CameraFilterViewController.RenderEventListener onRenderEventListener){
        renderCallBack = onRenderEventListener;
    }

    protected void runOnDraw(final Runnable runnable) {
        synchronized (mRunOnDraw) {
            mRunOnDraw.add(runnable);
        }
    }

    public void loadInputImage(final Bitmap bmp){
        runOnDraw(new Runnable() {
            @Override
            public void run() {
                imageWidth = bmp.getWidth();
                loadInputImage(bmp, "inputImageTexture");
            }
        });
    }

    public void loadInputImage(Bitmap image, String textureName){
        if(image == null){
            return;
        }

        int texture = 0;

        if(!textureMapping.containsKey(textureName)){
            int[] textures = new int[1];
            textures[0] = texture;

            // generate new texture
            GLES20.glGenTextures(1, textures, 0);
            texture = textures[0];
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
//            checkGlError("glBindTexture");
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        } else {
            texture = textureMapping.get(textureName);
        }

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
//        checkGlError("glBindTexture");
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, image, 0);

        textureMapping.put(textureName, texture);
    }

    public void setParameter(String parameterName, float floatValue){
        int uniformLocation = GLES20.glGetUniformLocation(glProgram, parameterName);
        GLES20.glUniform1f(uniformLocation, floatValue);
    }

    public void setParameter(String parameterName, int intValue){
        int uniformLocation = GLES20.glGetUniformLocation(glProgram, parameterName);
        GLES20.glUniform1i(uniformLocation, intValue);
    }

    public void loadFragmentShader(){
        try {
            clearProgram();

//            String generalVertexShaderString = AESUtils.decrypt("9e28169d69284e95ace68d86725e3bcd", readRawTextFile(context, R.raw.vertex__shader));
//            String generalFragmentShaderString = AESUtils.decrypt("db1588fcf9f1f90fcc6146f2e057e65b", readRawTextFile(context, R.raw.fragment__shader));

            glProgram = GLES20.glCreateProgram();
//            vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, generalVertexShaderString);//ShaderLoader.instance().getVertexShader());
//            fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, generalFragmentShaderString);// ShaderLoader.instance().getFragmentShader());
            vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, ShaderLoader.instance().getVertexShader());
            fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, ShaderLoader.instance().getFragmentShader());

//        try{
//            checkGlError("glUseProgram");
//        }catch(Exception ee){
//            Singleton.log("glUseProgram Error : " + ee.getMessage());
//        }


            GLES20.glAttachShader(glProgram, vertexShader);
//            checkGlError("glAttachShader");
            GLES20.glAttachShader(glProgram, fragmentShader);
//        try {
//            checkGlError("glAttachShader");
//        }catch(Exception ee){
//            Singleton.log("glAttachShader Error : " + ee.getMessage());
//        }

            GLES20.glLinkProgram(glProgram);
            GLES20.glUseProgram(glProgram);
            GLES20.glValidateProgram(glProgram);

            // enable vertex attribute array
            positionAttribute = GLES20.glGetAttribLocation(glProgram, "position");
            inputTextureCoordinateAttribute = GLES20.glGetAttribLocation(glProgram, "inputTextureCoordinate");

            // bind vertex buffer and texture buffer
            GLES20.glEnableVertexAttribArray(positionAttribute);
            GLES20.glEnableVertexAttribArray(inputTextureCoordinateAttribute);
        }
        catch(Exception e){
            Singleton.log("loadFragmentShader Error : " + e.getMessage());
            renderCallBack.onErrorOccurred();
        }


        if(glProgram < 0 || positionAttribute < 0 || inputTextureCoordinateAttribute < 0){
            Singleton.log("loadFragmentShader Error : ");
            Singleton.log("program : " + glProgram);
            Singleton.log("positionAttribute : " + positionAttribute);
            Singleton.log("inputTextureCoordinateAttribute : " + inputTextureCoordinateAttribute);
            renderCallBack.onErrorOccurred();
        }
    }

    public static String readRawTextFile(Context ctx, int resId)
    {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }
        return text.toString();
    }

    public void outputImage() {
        // bind input textures
        int textureIndex = 2;
        int activeTexture = GLES20.GL_TEXTURE2;

        Singleton.log("textureMapping size : " + textureMapping.size());

        for(Object key : textureMapping.keySet()){
            int textureLocation = GLES20.glGetUniformLocation(glProgram, (String) key);
            int texture = textureMapping.get(key);

//            try {
                GLES20.glActiveTexture(activeTexture);
//                checkGlError("glActiveTexture");
//            }catch(Exception e){
//                Singleton.log("glActiveTexture Error : " + e.getMessage());
//            }

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
//            checkGlError("glBindTexture");
            GLES20.glUniform1i(textureLocation, textureIndex);

            textureIndex++;

            if(textureIndex==2) {
                activeTexture = GLES20.GL_TEXTURE2;
            } else if(textureIndex==3) {
                activeTexture = GLES20.GL_TEXTURE3;
            } else if(textureIndex==4) {
                activeTexture = GLES20.GL_TEXTURE4;
            } else if(textureIndex==5) {
                activeTexture = GLES20.GL_TEXTURE5;
            } else if(textureIndex==6) {
                activeTexture = GLES20.GL_TEXTURE6;
            } else if(textureIndex==7) {
                activeTexture = GLES20.GL_TEXTURE7;
            }
        }

        // render image
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0); // bind to output texture
//        checkGlError("glActiveTexture");

//        GLES20.glViewport(0, 0, imageSize.getWidth(), imageSize.getHeight());

//        if(isCreateOutputImg || isCreateFinalImg) {
//            GLES20.glViewport(0, 0, imageWidth, imageHeight);
//        }else{
//            if(displayWidth < TARGET_IMAGE_SIZE) {
//                GLES20.glViewport(0, TARGET_IMAGE_SIZE - displayWidth, displayWidth, displayHeight);
//            }
//            else{
//                GLES20.glViewport(0, 0, displayWidth, displayHeight);
//            }
//        }

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        GLES20.glVertexAttribPointer(positionAttribute, 2, GLES20.GL_FLOAT, false, 0, vertexBuffer);
        GLES20.glVertexAttribPointer(inputTextureCoordinateAttribute, 2, GLES20.GL_FLOAT, false, 0, textureCoordBuffer);
//        checkGlError("vertex attribute setup");

//        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
//        checkGlError("glDrawArrays");

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
    }

    int _w, _h, _yOffset;
    public void setViewPort(int w, int h, int yOffset) {
        _w = w;
        _h = h;
        _yOffset = yOffset;
        runOnDraw(new Runnable() {
            @Override
            public void run() {
                GLES20.glViewport(0, _yOffset, _w, _h);
            }
        });

    }

    public Bitmap getOutputBitmap() {
        int size = 0;
        if(_yOffset > 0){
            size = 1080;
        }else{
            size = displayWidth;
        }


        IntBuffer intBuffer = IntBuffer.allocate(size * size);
        GLES20.glReadPixels(0, 0, size, size, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, intBuffer);

        Bitmap outputBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        outputBitmap.copyPixelsFromBuffer(intBuffer);
        intBuffer.clear();


        return outputBitmap;
    }

    public void loadFilter(String filterName) {
        //filterParams.clear();
        filterParams.put(COLOR_MAP1, ""); // color map 1 pic name
        filterParams.put(COLOR_MAP2, ""); // color map 2 pic name
        filterParams.put(OVERLAY1, ""); // overlay 1 pic name
        filterParams.put(OVERLAY2, ""); // overlay 2 pic name

        //======== NORMAL_FILTER ========//
        if (filterName.equalsIgnoreCase(FilterName.NORMAL_FILTER)) {
            filterParams.put(COLOR_MAP1, ""); // color map 1 pic name
            filterParams.put(OVERLAY1, ""); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name

            //======== RICH_FILTER ========//
        } else if(filterName.equalsIgnoreCase(FilterName.RICH_FILTER)) {
            filterParams.put(COLOR_MAP1, "rich_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight6.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("rich_map.png", R.drawable.rich_map);
            resFilterParams.put("overlay_softlight6.png", R.drawable.overlay_softlight6);

            //======== WARM_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.WARM_FILTER)) {
            filterParams.put(COLOR_MAP1, "warm_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight2.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("warm_map.png", R.drawable.warm_map);
            resFilterParams.put("overlay_softlight2.png", R.drawable.overlay_softlight2);

            //======== SOFT_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.SOFT_FILTER)) {
            filterParams.put(COLOR_MAP1, "soft_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, ""); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("soft_map.png", R.drawable.soft_map);

            //======== ROSE_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.ROSE_FILTER)) {
            filterParams.put(COLOR_MAP1, "rose_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, ""); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("rose_map.png", R.drawable.rose_map);

            //======== MORNING_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.MORNING_FILTER)) {
            filterParams.put(COLOR_MAP1, "morning_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, ""); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("morning_map.png", R.drawable.morning_map);

            //======== SUNSHINE_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.SUNSHINE_FILTER)) {
            filterParams.put(COLOR_MAP1, "sunshine_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight2.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("sunshine_map.png", R.drawable.sunshine_map);
            resFilterParams.put("overlay_softlight2.png", R.drawable.overlay_softlight2);

            //======== SUNSET_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.SUNSET_FILTER)) {
            filterParams.put(COLOR_MAP1, "sunset_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("sunset_map.png", R.drawable.sunset_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);

            //======== COOL_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.COOL_FILTER)) {
            filterParams.put(COLOR_MAP1, "cool_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight2.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("cool_map.png", R.drawable.cool_map);
            resFilterParams.put("overlay_softlight2.png", R.drawable.overlay_softlight2);

            //======== FREEZE_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.FREEZE_FILTER)) {
            filterParams.put(COLOR_MAP1, "freeze_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("freeze_map.png", R.drawable.freeze_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);

            //======== OCEAN_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.OCEAN_FILTER)) {
            filterParams.put(COLOR_MAP1, "ocean_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("ocean_map.png", R.drawable.ocean_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);

            //======== DREAM_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.DREAM_FILTER)) {
            filterParams.put(COLOR_MAP1, "dream_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight3.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("dream_map.png", R.drawable.dream_map);
            resFilterParams.put("overlay_softlight3.png", R.drawable.overlay_softlight3);

            //======== VIOLET_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.VIOLET_FILTER)) {
            filterParams.put(COLOR_MAP1, "violet_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("violet_map.png", R.drawable.violet_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);

            //======== MELLOW_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.MELLOW_FILTER)) {
            filterParams.put(COLOR_MAP1, "mellow_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight6.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("mellow_map.png", R.drawable.mellow_map);
            resFilterParams.put("overlay_softlight6.png", R.drawable.overlay_softlight6);

            //======== BLEAK_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.BLEAK_FILTER)) {
            filterParams.put(COLOR_MAP1, "bleak_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, "overlay_softlight2.png"); // overlay 2 pic name
            resFilterParams.put("bleak_map.png", R.drawable.bleak_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);
            resFilterParams.put("overlay_softlight2.png", R.drawable.overlay_softlight2);

            //======== MEMORY_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.MEMORY_FILTER)) {
            filterParams.put(COLOR_MAP1, "memory_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, "overlay_softlight3.png"); // overlay 2 pic name
            resFilterParams.put("memory_map.png", R.drawable.memory_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);
            resFilterParams.put("overlay_softlight3.png", R.drawable.overlay_softlight3);

            //======== PURE_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.PURE_FILTER)) {
            filterParams.put(COLOR_MAP1, "pure_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, ""); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("pure_map.png", R.drawable.pure_map);

            //======== CALM_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.CALM_FILTER)) {
            filterParams.put(COLOR_MAP1, "calm_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight2.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("calm_map.png", R.drawable.calm_map);
            resFilterParams.put("overlay_softlight2.png", R.drawable.overlay_softlight2);

            //======== AUTUMN_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.AUTUMN_FILTER)) {
            filterParams.put(COLOR_MAP1, "autumn_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, "overlay_softlight3.png"); // overlay 2 pic name
            resFilterParams.put("autumn_map.png", R.drawable.autumn_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);
            resFilterParams.put("overlay_softlight3.png", R.drawable.overlay_softlight3);

            //======== FANTASY_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.FANTASY_FILTER)) {
            filterParams.put(COLOR_MAP1, "fantasy_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight4.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("fantasy_map.png", R.drawable.fantasy_map);
            resFilterParams.put("overlay_softlight4.png", R.drawable.overlay_softlight4);

            //======== FREEDOM_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.FREEDOM_FILTER)) {
            filterParams.put(COLOR_MAP1, "freedom_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight2.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("freedom_map.png", R.drawable.freedom_map);
            resFilterParams.put("overlay_softlight2.png", R.drawable.overlay_softlight2);

            //======== MILD_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.MILD_FILTER)) {
            filterParams.put(COLOR_MAP1, "mild_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight5.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("mild_map.png", R.drawable.mild_map);
            resFilterParams.put("overlay_softlight5.png", R.drawable.overlay_softlight5);

            //======== PLAIRIE_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.PRAIRIE_FILTER)) {
            filterParams.put(COLOR_MAP1, "plairie_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight5.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("plairie_map.png", R.drawable.plairie_map);
            resFilterParams.put("overlay_softlight5.png", R.drawable.overlay_softlight5);

            //======== DEEP_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.DEEP_FILTER)) {
            filterParams.put(COLOR_MAP1, "deep_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight2.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("deep_map.png", R.drawable.deep_map);
            resFilterParams.put("overlay_softlight2.png", R.drawable.overlay_softlight2);

            //======== GLOW_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.GLOW_FILTER)) {
            filterParams.put(COLOR_MAP1, "glow_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight5.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("glow_map.png", R.drawable.glow_map);
            resFilterParams.put("overlay_softlight5.png", R.drawable.overlay_softlight5);

            //======== MEMOIR_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.MEMOIR_FILTER)) {
            filterParams.put(COLOR_MAP1, "memoir_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight6.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("memoir_map.png", R.drawable.memoir_map);
            resFilterParams.put("overlay_softlight6.png", R.drawable.overlay_softlight6);

            //======== MIST_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.MIST_FILTER)) {
            filterParams.put(COLOR_MAP1, "mist_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight5.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("mist_map.png", R.drawable.mist_map);
            resFilterParams.put("overlay_softlight5.png", R.drawable.overlay_softlight5);

            //======== VIVID_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.VIVID_FILTER)) {
            filterParams.put(COLOR_MAP1, "vivid_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("vivid_map.png", R.drawable.vivid_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);

            //======== CHILL_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.CHILL_FILTER)) {
            filterParams.put(COLOR_MAP1, "chill_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight1.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, "overlay_softlight5.png"); // overlay 2 pic name
            resFilterParams.put("chill_map.png", R.drawable.chill_map);
            resFilterParams.put("overlay_softlight1.png", R.drawable.overlay_softlight1);
            resFilterParams.put("overlay_softlight5.png", R.drawable.overlay_softlight5);

            //======== PINKY_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.PINKY_FILTER)) {
            filterParams.put(COLOR_MAP1, "pinky_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight5.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, ""); // overlay 2 pic name
            resFilterParams.put("pinky_map.png", R.drawable.pinky_map);
            resFilterParams.put("overlay_softlight5.png", R.drawable.overlay_softlight5);

            //======== ADVENTURE_FILTER ========//
        } else if (filterName.equalsIgnoreCase(FilterName.ADVENTURE_FILTER)) {
            filterParams.put(COLOR_MAP1, "adventure_map.png"); // color map 1 pic name
            filterParams.put(OVERLAY1, "overlay_softlight2.png"); // overlay 1 pic name
            filterParams.put(OVERLAY2, "overlay_softlight3.png"); // overlay 2 pic name
            resFilterParams.put("adventure_map.png", R.drawable.adventure_map);
            resFilterParams.put("overlay_softlight2.png", R.drawable.overlay_softlight2);
            resFilterParams.put("overlay_softlight3.png", R.drawable.overlay_softlight3);
        }

//        String fragmentShaderString = readRawTextFile(context, R.raw.fragment_shader);

        SoftReference<Bitmap> reference;

        // load color map 1
        if(!filterParams.get(COLOR_MAP1).equals("")){
            setParameter(ParameterName.COLOR_MAP1_ENABLED, 1);

            if(textureCache.containsKey(filterParams.get(COLOR_MAP1))){
                reference = textureCache.get(filterParams.get(COLOR_MAP1));
                colorMapImage1 = reference.get();
                if (colorMapImage1 == null) {
                    colorMapImage1 = BitmapFactory.decodeResource(context.getResources(), (int) resFilterParams.get(filterParams.get(COLOR_MAP1)));
                    textureCache.put(filterParams.get(COLOR_MAP1), new SoftReference<Bitmap>(colorMapImage1));
                }
            }else{
                colorMapImage1 = BitmapFactory.decodeResource(context.getResources(), (int) resFilterParams.get(filterParams.get(COLOR_MAP1)));
                textureCache.put(filterParams.get(COLOR_MAP1), new SoftReference<Bitmap>(colorMapImage1));
            }
            loadInputImage(colorMapImage1, COLOR_MAP_TEXTURE1);

        }else{
            setParameter(ParameterName.COLOR_MAP1_ENABLED, 0);
        }

        // load overlay 1
        if(!filterParams.get(OVERLAY1).equals("")){
            setParameter(ParameterName.OVERLAY1_ENABLED, 1);

            if(textureCache.containsKey(filterParams.get(OVERLAY1))){
                reference = textureCache.get(filterParams.get(OVERLAY1));
                overlayImage1 = reference.get();
                if (overlayImage1 == null) {
                    overlayImage1 = BitmapFactory.decodeResource(context.getResources(), (int) resFilterParams.get(filterParams.get(OVERLAY1)));
                    textureCache.put(filterParams.get(OVERLAY1), new SoftReference<Bitmap>(overlayImage1));
                }
            }else{
                overlayImage1 = BitmapFactory.decodeResource(context.getResources(), (int) resFilterParams.get(filterParams.get(OVERLAY1)));
                textureCache.put(filterParams.get(OVERLAY1), new SoftReference<Bitmap>(overlayImage1));
            }
            loadInputImage(overlayImage1, OVERLAY_TEXTURE1);

        }else{
            setParameter(ParameterName.OVERLAY1_ENABLED, 0);
        }

        // load color map 2
        if(!filterParams.get(COLOR_MAP2).equals("")){
            setParameter(ParameterName.COLOR_MAP2_ENABLED, 1);

            if(textureCache.containsKey(filterParams.get(COLOR_MAP2))){
                reference = textureCache.get(filterParams.get(COLOR_MAP2));
                colorMapImage2 = reference.get();
                if (colorMapImage2 == null) {
                    colorMapImage2 = BitmapFactory.decodeResource(context.getResources(), (int) resFilterParams.get(filterParams.get(COLOR_MAP2)));
                    textureCache.put(filterParams.get(COLOR_MAP2), new SoftReference<Bitmap>(colorMapImage2));
                }
            }else{
                colorMapImage2 = BitmapFactory.decodeResource(context.getResources(), (int) resFilterParams.get(filterParams.get(COLOR_MAP2)));
                textureCache.put(filterParams.get(COLOR_MAP2), new SoftReference<Bitmap>(colorMapImage2));
            }
            loadInputImage(colorMapImage2, COLOR_MAP_TEXTURE2);

        }else{
            setParameter(ParameterName.COLOR_MAP2_ENABLED, 0);
        }


        // load overlay 2
        if(!filterParams.get(OVERLAY2).equals("")){
            setParameter(ParameterName.OVERLAY2_ENABLED, 1);

            if(textureCache.containsKey(filterParams.get(OVERLAY2))){
                reference = textureCache.get(filterParams.get(OVERLAY2));
                overlayImage2 = reference.get();
                if (overlayImage2 == null) {
                    overlayImage2 = BitmapFactory.decodeResource(context.getResources(), (int) resFilterParams.get(filterParams.get(OVERLAY2)));
                    textureCache.put(filterParams.get(OVERLAY2), new SoftReference<Bitmap>(overlayImage2));
                }
            }else{
                overlayImage2 = BitmapFactory.decodeResource(context.getResources(), (int) resFilterParams.get(filterParams.get(OVERLAY2)));
                textureCache.put(filterParams.get(OVERLAY2), new SoftReference<Bitmap>(overlayImage2));
            }
            loadInputImage(overlayImage2, OVERLAY_TEXTURE2);

        }else{
            setParameter(ParameterName.OVERLAY2_ENABLED, 0);
        }


//        loadFragmentShader(fragmentShaderString);
    }

    public void resetToolParams()
    {
        setParameter(ParameterName.FILTER_MIX_PERCENTAGE_PARAMETER, 1.0f);
        setParameter(ParameterName.BRIGHTNESS_PARAMETER, 0.0f); // brightness
        setParameter(ParameterName.CONTRAST_PARAMETER, 0.0f); // contrast
        setParameter(ParameterName.UNSHARPMASK_PARAMETER, 0.0f); // unsharpMask
        setParameter(ParameterName.SKINSMOOTH_PARAMETER, 0.0f); // skinSmooth
        setParameter(ParameterName.AUTO_ENHANCE_PARAMETER, 0.0f); // auto enhance
        setParameter(ParameterName.WARMTH_PARAMETER, 0.0f); // warmth
        setParameter(ParameterName.SATURATION_PARAMETER, 0.0f); // saturation
        setParameter(ParameterName.FADE_PARAMETER, 0.0f); // fade
        setParameter(ParameterName.HIGHLIGHTS_PARAMETER, 0.0f); // highlights
        setParameter(ParameterName.SHADOWS_PARAMETER, 0.0f); // shadows
        setParameter(ParameterName.VIGNETTE_PARAMETER, 0.0f); // vignette
        setParameter(ParameterName.SHARPEN_PARAMETER, 0.0f); // sharpen
        setParameter(ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 0.0f); // shadowsColorMapIndex
        setParameter(ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 0.0f); // highlightsColorMapIndex
        setParameter(ParameterName.COLOR_SHADOWS_PARAMETER, 0.0f); // colorShadows
        setParameter(ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 0.0f); // colorHighlights

        // tilt shift
        setParameter(ParameterName.TILT_SHIFT_MODE_PARAMETER, 0.0f);
        setParameter(ParameterName.TILT_SHIFT_RADIAL_CENTER_X_PARAMETER, 0.0f);
        setParameter(ParameterName.TILT_SHIFT_RADIAL_CENTER_Y_PARAMETER, 0.0f);
        setParameter(ParameterName.TILT_SHIFT_RADIAL_RADIUS_PARAMETER, 0.0f);
        setParameter(ParameterName.TILT_SHIFT_LINEAR_CENTER_X_PARAMETER, 0.0f);
        setParameter(ParameterName.TILT_SHIFT_LINEAR_CENTER_Y_PARAMETER, 0.0f);
        setParameter(ParameterName.TILT_SHIFT_LINEAR_RADIUS_PARAMETER, 0.0f);
        setParameter(ParameterName.TILT_SHIFT_LINEAR_ANGLE_PARAMETER, 0.0f);
    }

    public void enqueueResetToolParams(){
        runOnDraw(new Runnable() {
            @Override
            public void run() {
                resetToolParams();
            }
        });
    }

    public void clearProgram()
    {
        try{
            GLES20.glDeleteProgram(glProgram);
        }catch(Exception ee){
            Singleton.log("clearProgram() glProgram Error : " + ee.getMessage());
        }

        try{
            GLES20.glDeleteShader(vertexShader);
        }catch(Exception ee){
            Singleton.log("clearProgram() vertex__shader Error : " + ee.getMessage());
        }

        try{
            GLES20.glDeleteShader(fragmentShader);
        }catch(Exception ee){
            Singleton.log("clearProgram() fragment__shader Error : " + ee.getMessage());
        }
    }

    public void dealloc() {
        for(Object key : textureMapping.keySet()) {
            int texture = textureMapping.get(key);

            int[] textureArray = new int[1];
            textureArray[0] = texture;

            GLES20.glDeleteTextures(1, textureArray, 0);
        }

        GLES20.glDeleteTextures(1, outputTexture, 0);
        GLES20.glDeleteFramebuffers(1, framebuffer, 0);

        clearProgram();

        try{
            vertexBuffer.clear();
            textureCoordBuffer.clear();
        }catch(Exception e){}

        try{
            vertexBuffer.clear();
        }catch(Exception e){}

        try{
            textureCoordBuffer.clear();
        }catch(Exception e){}

        for(String key : textureCache.keySet()){
            try {
                textureCache.get(key).get().recycle();
            }catch(Exception e){}
        }

        try {
            colorMapImage1.recycle();
            colorMapImage2.recycle();
            overlayImage1.recycle();
            overlayImage2.recycle();
            sharedColorMapImage.recycle();
        }
        catch(Exception e){}
    }

    // helper method to compile openGL shader
    public static int loadShader(int type, String shaderCode){
        int shader = GLES20.glCreateShader(type);
        try {
            GLES20.glShaderSource(shader, shaderCode);
            GLES20.glCompileShader(shader);
        }
        catch(Exception e){
            Singleton.log("glCompileShader ERROR : " + e.getMessage());
        }
        return shader;
    }

    public static String glGetAttribLocation(Context ctx, int resId)
    {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }

        try{
            buffreader.close();
            inputreader.close();
            inputStream.close();
        }catch(Exception e){
        }
        return text.toString();//substring(0, text.length()-1);
    }


    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
//        int[] maxVertexAttribs = new int[1];
//        int[] maxVertexUniforms = new int[1];
//        int[] maxVaryings = new int[1];
//        int[] maxVertexTextureUnits = new int[1];
//        int[] maxCombinedTextureUnits = new int[1];
//        GLES20.glGetIntegerv(GLES20.GL_MAX_VERTEX_ATTRIBS, maxVertexAttribs, 0);
//        GLES20.glGetIntegerv(GLES20.GL_MAX_VERTEX_UNIFORM_VECTORS, maxVertexUniforms, 0);
//        GLES20.glGetIntegerv(GLES20.GL_MAX_VARYING_VECTORS, maxVaryings, 0);
//        GLES20.glGetIntegerv(GLES20.GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,maxVertexTextureUnits, 0);
//        GLES20.glGetIntegerv(GLES20.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,maxCombinedTextureUnits, 0);
//        Singleton.log("maxVertexAttribs: " + maxVertexAttribs[0]);
//        Singleton.log("maxVertexUniforms: " + maxVertexUniforms[0]);
//        Singleton.log("maxVaryings: " + maxVaryings[0]);
//        Singleton.log("maxVertexTextureUnits: " + maxVertexTextureUnits[0]);
//        Singleton.log("maxCombinedTextureUnits: " + maxCombinedTextureUnits[0]);
//
//        int[] maxViewportSize = new int[2];
//        GLES20.glGetIntegerv(GLES20.GL_MAX_VIEWPORT_DIMS, maxViewportSize, 0);

        GLES20.glClearColor(0, 0, 0, 1);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        // Enable Smooth Shading, default not really needed.
//        gl.glShadeModel(GL10.GL_SMOOTH);

        loadFragmentShader();

        // load gaussian coefficients
//        GLES20.glUniform1fv(GLES20.glGetUniformLocation(glProgram, "gaussianOffsetTap9"), 3, FloatBuffer.wrap(gaussianOffsetTap9));
//        GLES20.glUniform1fv(GLES20.glGetUniformLocation(glProgram, "gaussianWeightTap9"), 3, FloatBuffer.wrap(gaussianWeightTap9));
//        GLES20.glUniform1fv(GLES20.glGetUniformLocation(glProgram, "gaussianOffsetTap25"), 7, FloatBuffer.wrap(gaussianOffsetTap25));
//        GLES20.glUniform1fv(GLES20.glGetUniformLocation(glProgram, "gaussianWeightTap25"), 7, FloatBuffer.wrap(gaussianWeightTap25));
//        GLES20.glUniform1fv(GLES20.glGetUniformLocation(glProgram, "gaussianOffsetTap49"), 13, FloatBuffer.wrap(gaussianOffsetTap49));
//        GLES20.glUniform1fv(GLES20.glGetUniformLocation(glProgram, "gaussianWeightTap49"), 13, FloatBuffer.wrap(gaussianWeightTap49));
//        GLES20.glUniform1fv(GLES20.glGetUniformLocation(glProgram, "gaussianOffsetTap81"), 21, FloatBuffer.wrap(gaussianOffsetTap81));
//        GLES20.glUniform1fv(GLES20.glGetUniformLocation(glProgram, "gaussianWeightTap81"), 21, FloatBuffer.wrap(gaussianWeightTap81));
        rotationMatrixHandle = GLES20.glGetUniformLocation(glProgram, "rotationMatrix");

        // load shared color map
        SoftReference<Bitmap> reference;

        if (textureCache.containsKey("shared_color_map")) {
            reference = textureCache.get("shared_color_map");
            sharedColorMapImage = reference.get();
            if (sharedColorMapImage == null) {
                sharedColorMapImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.shared_color_map);
                textureCache.put("shared_color_map", new SoftReference<Bitmap>(sharedColorMapImage));
            }
        } else {
            sharedColorMapImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.shared_color_map);
            textureCache.put("shared_color_map", new SoftReference<Bitmap>(sharedColorMapImage));
        }

        loadInputImage(sharedColorMapImage, SHARED_COLOR_MAP_TEXTURE);

        // setup initial params
        setParameter(ParameterName.FILTER_MIX_PERCENTAGE_PARAMETER, 1.0f);
        float pixelSize = 1.0f / 1080f;
        setParameter(ParameterName.PIXEL_STEP_SIZE, pixelSize);

        loadFilter("NORMAL_FILTER");
        resetToolParams();

        lastColorMapRecord.put(ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 0.0f);
        lastColorMapRecord.put(ParameterName.COLOR_SHADOWS_PARAMETER, 0.0f);
        lastColorMapRecord.put(ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 0.0f);
        lastColorMapRecord.put(ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 0.0f);

        currentColorMapRecord.put(ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, 0.0f);
        currentColorMapRecord.put(ParameterName.COLOR_SHADOWS_PARAMETER, 0.0f);
        currentColorMapRecord.put(ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, 0.0f);
        currentColorMapRecord.put(ParameterName.COLOR_HIGHLIGHTS_PARAMETER, 0.0f);

        // set output type
        setParameter("renderOutputType", 0);

    }

    public void setRotation(float degrees, int flip) {
        // rotation
        float[] rotationMatrix = new float[16];
        float[] flipMatrix = new float[16];
        float[] finalResult = new float[16];

        android.opengl.Matrix.setRotateM(rotationMatrix, 0, (float) degrees, 0.0f, 0.0f, 1.0f);

        if(flip==1) {
            android.opengl.Matrix.setRotateM(flipMatrix, 0, 180, 0.0f, 1.0f, 0.0f);
        } else {
            android.opengl.Matrix.setIdentityM(flipMatrix, 0);
        }

        Matrix.multiplyMM(finalResult, 0, rotationMatrix, 0, flipMatrix, 0);
        GLES20.glUniformMatrix4fv(rotationMatrixHandle, 1, false, finalResult, 0);
    }

    float degrees;
    int fflip;
    public void enqueueSetRotation(float _degreess, int _flip){
        this.degrees = _degreess;
        this.fflip = _flip;

        runOnDraw(new Runnable() {
            @Override
            public void run() {
                setRotation(degrees, fflip);
            }
        });
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        //updateViewSize(width, height);
        if(isCame == 1) setRotation(0, 1);
        else if(isCame == 0) setRotation(180f, 1);
        else setRotation(-90f, 1);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        synchronized (mRunOnDraw) {
            while (!mRunOnDraw.isEmpty()) {
                mRunOnDraw.poll().run();
            }
        }

        outputImage();

        // Call Back
        renderCallBack.onDrawFrameFinish();

        vertexBuffer.position(0);
        textureCoordBuffer.position(0);
//        GLES20.glDisableVertexAttribArray(positionAttribute);
//        GLES20.glDisableVertexAttribArray(inputTextureCoordinateAttribute);
//        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

//        if(isCreateOutputImg) renderNotify.onDrawFrameFinish();
//        if(isCreateFinalImg) renderNotify.onOutputFinalImageFinish(outputBitmap);
    }

    public void setCurrentFilter(String filter,float percentage){
        this.currentFilter = filter;
        this.currentFilterPercentage = percentage;
        runOnDraw(new Runnable() {
            @Override
            public void run() {
                loadFilter(currentFilter);
                setParameter("filterMixPercentage", currentFilterPercentage);
            }
        });
    }

    public void setCurrentParameter(String parameter, float value){
        this.currentParameter = parameter;
        this.currentParameterValue = value;

        runOnDraw(new Runnable() {
            @Override
            public void run() {
                setParameter(currentParameter, currentParameterValue);
            }
        });
    }

    public void setColorMapParameter(String indexName, float indexValue, String colorName, float colorValue) {
        currentColorMapIndexName = indexName;
        currentColorMapIndexValue = indexValue;
        currentColorMapRecord.put(indexName, indexValue);

        currentColorMapParameterName = colorName;
        currentColorMapParameterValue = colorValue;
        currentColorMapRecord.put(colorName, colorValue);
        runOnDraw(new Runnable() {
            @Override
            public void run() {
                setParameter(currentColorMapIndexName, currentColorMapIndexValue);
                setParameter(currentColorMapParameterName, currentColorMapParameterValue);
            }
        });
    }

    public void resetColorMap() {
        runOnDraw(new Runnable() {
            @Override
            public void run() {
                setParameter(ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, lastColorMapRecord.get(ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER));
                setParameter(ParameterName.COLOR_SHADOWS_PARAMETER, lastColorMapRecord.get(ParameterName.COLOR_SHADOWS_PARAMETER));
                setParameter(ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, lastColorMapRecord.get(ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER));
                setParameter(ParameterName.COLOR_HIGHLIGHTS_PARAMETER, lastColorMapRecord.get(ParameterName.COLOR_HIGHLIGHTS_PARAMETER));
            }
        });
    }

    public void updateColopMapRecord(){
        lastColorMapRecord.put(ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER, currentColorMapRecord.get(ParameterName.SHADOWS_COLOR_MAP_INDEX_PARAMETER));
        lastColorMapRecord.put(ParameterName.COLOR_SHADOWS_PARAMETER, currentColorMapRecord.get(ParameterName.COLOR_SHADOWS_PARAMETER));
        lastColorMapRecord.put(ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER, currentColorMapRecord.get(ParameterName.HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER));
        lastColorMapRecord.put(ParameterName.COLOR_HIGHLIGHTS_PARAMETER, currentColorMapRecord.get(ParameterName.COLOR_HIGHLIGHTS_PARAMETER));
    }

    public void setTiltParameter(int type, float centerX, float centerY, float radius, float angle) {
        if (type == TiltEffectDrawingView.DrawType.Circle) {
            radiaX = centerX;
            radiaY = centerY;
            radiaRadius = radius;

            runOnDraw(new Runnable() {
                @Override
                public void run() {
                    setParameter(MyFilter.ParameterName.TILT_SHIFT_MODE_PARAMETER, 1.0f);
                    setParameter(MyFilter.ParameterName.TILT_SHIFT_RADIAL_CENTER_X_PARAMETER, radiaX);
                    setParameter(MyFilter.ParameterName.TILT_SHIFT_RADIAL_CENTER_Y_PARAMETER, radiaY);
                    setParameter(MyFilter.ParameterName.TILT_SHIFT_RADIAL_RADIUS_PARAMETER, radiaRadius);
                }
            });
        } else {
            linearX = centerX;
            linearY = centerY;
            linearRadius = radius;
            linearAngle = angle;

            runOnDraw(new Runnable() {
                @Override
                public void run() {
                    setParameter(MyFilter.ParameterName.TILT_SHIFT_MODE_PARAMETER, 2.0f);
                    setParameter(MyFilter.ParameterName.TILT_SHIFT_LINEAR_CENTER_X_PARAMETER, linearX);
                    setParameter(MyFilter.ParameterName.TILT_SHIFT_LINEAR_CENTER_Y_PARAMETER, linearY);
                    setParameter(MyFilter.ParameterName.TILT_SHIFT_LINEAR_RADIUS_PARAMETER, radiaRadius);
                }
            });
        }
    }

    public void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
//            throw new RuntimeException(op + ": glError " + error);
            renderCallBack.onErrorOccurred();
        }
    }

    // generate filter preview thumbnails
//    public void doCreatePreviewImage(){
//
//        new Runnable() {
//            @Override
//            public void run() {
//                loadInputImage(imageSize, "inputImageTexture");
//
//                String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/." + Singleton.applicationContext.getPackageName() + "/FILTER_PREVIEW_";
//                Bitmap bmp = null;
//                FileOutputStream outStream2 = null;
//
//                for(String fName : FilterName.allNames)
////                for(int i= FilterName.allNames.size()-1 ; i>=0 ; i--)
//                {
//                    loadFilter(fName);
////                    loadFilter(FilterName.allNames.get(i));
//                    setParameter("filterMixPercentage", 1.0f);
//                    outputImage();
//
//                    try {
//                        outStream2 = new FileOutputStream(file + fName + ".jpg");
////                        outStream2 = new FileOutputStream(file + FilterName.allNames.get(i) + ".jpg");
//                        outputBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outStream2);
//                        outStream2.close();
//                    } catch (FileNotFoundException fnf) {
//                    } catch (IOException ioe) {
//                    }
//
//                    vertexBuffer.clear();
//                    textureCoordBuffer.clear();
//                }
//
//                renderNotify.onCreatePreviewImageFinished();
////        try {
////            bmp.recycle();
////        }catch(Exception e){}
//            }
//
//        }.run();
//    }

//    public void setPreviewImgFlag(boolean b){
//        isCreateOutputImg = b;
//    }

//    public boolean getPreviewImgflag(){
//        return isCreateOutputImg;
//    }

//    public void setCreateFinalImgFalg(boolean isCreate) {
//        isCreateFinalImg = isCreate;
//        runOnDraw(new Runnable() {
//            @Override
//            public void run() {
//                loadFilter(currentFilter);
//                setParameter("filterMixPercentage", currentFilterPercentage);
//                setParameter(currentParameter, currentParameterValue);
//            }
//        });
//    }

//    public void setPreviewImg(Bitmap bmp){
//        this.previewBmp = bmp;
//    }

    static public class FilterName{
        // 30 filters, 2015/07/02 update
        static final String NORMAL_FILTER = "NORMAL_FILTER";
        static final String RICH_FILTER = "RICH_FILTER";
        static final String WARM_FILTER = "WARM_FILTER";
        static final String SOFT_FILTER = "SOFT_FILTER";
        static final String ROSE_FILTER = "ROSE_FILTER";
        static final String MORNING_FILTER = "MORNING_FILTER";
        static final String SUNSHINE_FILTER = "SUNSHINE_FILTER";
        static final String SUNSET_FILTER = "SUNSET_FILTER";
        static final String COOL_FILTER = "COOL_FILTER";
        static final String FREEZE_FILTER = "FREEZE_FILTER";
        static final String OCEAN_FILTER = "OCEAN_FILTER";
        static final String DREAM_FILTER = "DREAM_FILTER";
        static final String VIOLET_FILTER = "VIOLET_FILTER";
        static final String MELLOW_FILTER = "MELLOW_FILTER";
        static final String BLEAK_FILTER = "BLEAK_FILTER";
        static final String MEMORY_FILTER = "MEMORY_FILTER";
        static final String PURE_FILTER = "PURE_FILTER";
        static final String CALM_FILTER = "CALM_FILTER";
        static final String AUTUMN_FILTER = "AUTUMN_FILTER";
        static final String FANTASY_FILTER = "FANTASY_FILTER";
        static final String FREEDOM_FILTER = "FREEDOM_FILTER";
        static final String MILD_FILTER = "MILD_FILTER";
        static final String PRAIRIE_FILTER = "PRAIRIE_FILTER";
        static final String DEEP_FILTER = "DEEP_FILTER";
        static final String GLOW_FILTER = "GLOW_FILTER";
        static final String MEMOIR_FILTER = "MEMOIR_FILTER";
        static final String MIST_FILTER = "MIST_FILTER";
        static final String VIVID_FILTER = "VIVID_FILTER";
        static final String CHILL_FILTER = "CHILL_FILTER";
        static final String PINKY_FILTER = "PINKY_FILTER";
        static final String ADVENTURE_FILTER = "ADVENTURE_FILTER";

        // useless
//        static String PIECE_FILTER = "PIECE_FILTER";
//        static String MY_1911_FILTER = "MY_1911_FILTER";
//        static String SWEET_FILTER = "SWEET_FILTER";
//        static String TOUCH_FILTER = "TOUCH_FILTER";
//        static String PURITY_FILTER = "PURITY_FILTER";
        static ArrayList<String> allNames() {
            return new ArrayList<String>(Arrays.asList(NORMAL_FILTER, RICH_FILTER, WARM_FILTER, SOFT_FILTER, ROSE_FILTER, MORNING_FILTER, SUNSHINE_FILTER, SUNSET_FILTER, COOL_FILTER, FREEZE_FILTER,
                    OCEAN_FILTER, DREAM_FILTER, VIOLET_FILTER, MELLOW_FILTER, BLEAK_FILTER, MEMORY_FILTER, PURE_FILTER, CALM_FILTER, AUTUMN_FILTER, FANTASY_FILTER,
                    FREEDOM_FILTER, MILD_FILTER, PRAIRIE_FILTER, DEEP_FILTER, GLOW_FILTER, MEMOIR_FILTER, MIST_FILTER, VIVID_FILTER, CHILL_FILTER, PINKY_FILTER,
                    ADVENTURE_FILTER));
        }
    }

    public class ParameterName{
        static final String FILTER_MIX_PERCENTAGE_PARAMETER = "filterMixPercentage";
        static final String BRIGHTNESS_PARAMETER = "brightness"; // brightness
        static final String CONTRAST_PARAMETER = "contrast"; // contrast
        static final String UNSHARPMASK_PARAMETER = "unsharpMask"; //
        static final String SKINSMOOTH_PARAMETER = "skinSmooth"; // skinSmooth
        static final String AUTO_ENHANCE_PARAMETER = "autoEnhance"; // auto enhance
        static final String WARMTH_PARAMETER = "warmth"; // warmth
        static final String SATURATION_PARAMETER = "saturation"; // saturation
        static final String FADE_PARAMETER = "fade"; //fade
        static final String HIGHLIGHTS_PARAMETER = "highlights"; // highlights
        static final String SHADOWS_PARAMETER = "shadows"; // shadows
        static final String VIGNETTE_PARAMETER = "vignette"; // vignette
        static final String SHARPEN_PARAMETER = "sharpen"; // sharpen
        static final String SHADOWS_COLOR_MAP_INDEX_PARAMETER = "shadowsColorMapIndex"; // shadowsColorMapIndex
        static final String HIGHLIGHTS_COLOR_MAP_INDEX_PARAMETER = "highlightsColorMapIndex"; // highlightsColorMapIndex
        static final String COLOR_SHADOWS_PARAMETER = "colorShadows"; // colorShadows
        static final String COLOR_HIGHLIGHTS_PARAMETER = "colorHighlights"; // colorHighlights

        // tilt shift
        static final String TILT_SHIFT_MODE_PARAMETER = "tiltShiftMode";
        static final String TILT_SHIFT_RADIAL_CENTER_X_PARAMETER = "tiltShiftRadialCenterX";
        static final String TILT_SHIFT_RADIAL_CENTER_Y_PARAMETER = "tiltShiftRadialCenterY";
        static final String TILT_SHIFT_RADIAL_RADIUS_PARAMETER = "tiltShiftRadialRadius";
        static final String TILT_SHIFT_LINEAR_CENTER_X_PARAMETER = "tiltShiftLinearCenterX";
        static final String TILT_SHIFT_LINEAR_CENTER_Y_PARAMETER = "tiltShiftLinearCenterY";
        static final String TILT_SHIFT_LINEAR_RADIUS_PARAMETER = "tiltShiftLinearRadius";
        static final String TILT_SHIFT_LINEAR_ANGLE_PARAMETER = "tiltShiftLinearAngle";

        // color map
        static final String COLOR_MAP1_ENABLED = "colorMap1Enabled";
        static final String COLOR_MAP2_ENABLED = "colorMap2Enabled";
        static final String OVERLAY1_ENABLED = "overlay1Enabled";
        static final String OVERLAY2_ENABLED = "overlay2Enabled";

        // others
        static final String PIXEL_STEP_SIZE = "pixelStepSize";
    }
}