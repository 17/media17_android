package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by POPO on 15/7/24.
 */
public class LoginActivity_V2 extends BaseActivity {
    private LoginActivity_V2 mCtx = this;
    private EditText mAccount, mPasswoird;
    private ConnectivityManager mConnectivityManager;
    private NetworkInfo mNetworkInfo;
    private RelativeLayout btnLogin;
    private boolean focus = false;
    private TextView mForgot;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.longin_activity_v2);

        initTitleBar();

        mAccount = (EditText) findViewById(R.id.account);
        mPasswoird = (EditText) findViewById(R.id.password);
        btnLogin = (RelativeLayout) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(ButtonClickEventListener);

        mPasswoird.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                focus = hasFocus;
            }
        });

        mForgot = (TextView) findViewById(R.id.forgot);
        mForgot.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        showKeyboard();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.log_in_only));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_back);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mCtx, LoginMenuActivity_V2.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intent = new Intent();
//            intent.setClass(mCtx, LoginMenuActivity_V2.class);
            intent.setClass(mCtx, LoginMenuActivity_V3.class);
            startActivity(intent);
            mCtx.finish();

            return true;
        }

        if(keyCode == KeyEvent.KEYCODE_ENTER && focus){
            processLogin();
        }

        return super.onKeyUp(keyCode, event);
    }

    private View.OnClickListener ButtonClickEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            processLogin();
        }
    };

    private void processLogin(){
        if(mAccount.getText().toString().length()!=0 && mPasswoird.getText().toString().length()!=0)
        {
            mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

            if (mNetworkInfo != null && mNetworkInfo.isConnected())
            {
                hideKeyboard();
                showProgressDialog();
                ApiManager.loginAction(mCtx,mAccount.getText().toString(), mPasswoird.getText().toString(), new ApiManager.LoginActionCallback()
                {
                    @Override
                    public void onResult(boolean success, String message, UserModel user)
                    {
                        hideProgressDialog();

                        if(success)
                        {
                            if (message.equals("ok"))
                            {
                                setConfig(Constants.ACCOUNT,mAccount.getText().toString());
                                setConfig(Constants.PASSWORD,mPasswoird.getText().toString());

                                Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                mStory17Application.setUser(user);

                                Intent intent = new Intent();
                                intent.setClass(mCtx, MenuActivity.class);
                                startActivity(intent);
                                mCtx.finish();
                            }
                            else if (message.equals("freezed")) showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                            else showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                        }
                        else{
                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_fail));
//                            showNetworkUnstableToast();
                        }
                    }
                });
            }
            else
            {
                try{
//                              showToast(getString(R.string.login_internet));
                    Toast.makeText(mCtx, getString(R.string.login_internet), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
            }
        }
        else{
            try{
//                              showToast(getString(R.string.login_enter));
                Toast.makeText(mCtx, getString(R.string.login_enter), Toast.LENGTH_SHORT).show();
            }
            catch (Exception x){
            }
        }
    }
}
