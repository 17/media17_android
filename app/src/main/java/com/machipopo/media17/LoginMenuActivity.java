package com.machipopo.media17;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by POPO on 6/4/15.
 */
public class LoginMenuActivity extends BaseActivity
{
    private LoginMenuActivity mCtx = this;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(com.machipopo.media17.R.layout.login_menu_activity);

        Button mSingUp = (Button) findViewById(R.id.sign_up);
        TextView mLongIn = (TextView) findViewById(R.id.long_in);
        mLongIn.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        mSingUp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                Intent intent = new Intent();
//                intent.setClass(LoginMenuActivity.this, LiveStreamActivity.class);
//                intent.putExtra("mode", LiveStreamActivity.LIVE_STREAM_MODE_BROADCAST);
//                startActivity(intent);
//                finish();

                Intent intent = new Intent();
                intent.setClass(mCtx,SignupActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });

        mLongIn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                Intent intent = new Intent();
//                intent.setClass(LoginMenuActivity.this, LiveStreamActivity.class);
//                intent.putExtra("mode", LiveStreamActivity.LIVE_STREAM_MODE_WATCH);
//                startActivity(intent);
//                finish();

                Intent intent = new Intent();
                intent.setClass(mCtx,LonginActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }
}
