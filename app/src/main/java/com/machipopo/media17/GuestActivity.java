package com.machipopo.media17;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.flurry.android.FlurryAgent;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.NotifiModel;
import com.machipopo.media17.model.SuggestedUsersModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.DevUtils;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.machipopo.media17.utils.LogEventUtil;
import com.machipopo.media17.utils.ShareUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tencent.connect.UserInfo;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dean on 16/1/4.
 */
public class GuestActivity extends BaseActivity
{
    private GuestActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;
    private DisplayMetrics mDisplayMetrics;

    private ViewPager mViewPager;
    private View mViewLive, mViewExplore;
    private List<View> mViewList;

    private TextView mTab1,mTab2;
    private Button mSingup, mLogin;

    private DisplayImageOptions SelfOptions;

    private PullToRefreshGridView mListViewLive;
    private LiveLstAdapter mListAdapterLive;
    private ProgressBar mProgressLive;
    private ImageView mNoDataLive;
    public ArrayList<LiveModel> mLiveModels = new ArrayList<LiveModel>();
    private int mGridHeight;

    private PullToRefreshGridView mListViewExplore;
    private ExploreAdapter mListAdapterExplore;
    private ProgressBar mProgressExplore;
    private ImageView mNoDataExplore;
    private Boolean isFetchingDataExplore = false;
    private Boolean noMoreDataExplore = false;
    private ArrayList<FeedModel> mFeedModels = new ArrayList<FeedModel>();
    private int mGridHeightExpore;

    private ArrayList<SuggestedUsersModel> mSuggested = new ArrayList<SuggestedUsersModel>();

    private Dialog GuestDialog;
    private SharedPreferences sharedPreferences;
    private CallbackManager callbackManager;

    private Boolean mSignOut = false;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onResume(this);
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());

        if(!mSignOut){
            if(mApplication!=null){
                if(mApplication.getSignOutState()) mCtx.finish();
                else if(mApplication.getMenuFinish()) mCtx.finish();
            }
        }

        if(mApplication!=null){
            if(mApplication.getMenuFinish()) mCtx.finish();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPause(this);
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    public void onStart()
    {
        super.onStart();
        FlurryAgent.onStartSession(this, Constants.FLURRY_API_KEY);
    }
    public void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guest_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        mGridHeight = (int)((float)mDisplayMetrics.widthPixels/(float)2);
        mGridHeightExpore = (int)((float)mDisplayMetrics.widthPixels/(float)3);

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null){
            if(mBundle.containsKey("sign")){
                mSignOut = mBundle.getBoolean("sign");
            }
        }

        initTitleBar();

        LayoutInflater lf = getLayoutInflater().from(mCtx);
        mViewLive = lf.inflate(R.layout.guest_live, null);
        mViewExplore = lf.inflate(R.layout.guest_explore, null);

        mViewList = new ArrayList<View>();
        mViewList.add(mViewLive);
        mViewList.add(mViewExplore);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mPagerAdapter);

        SmartTabLayout mPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        mPagerTab.setViewPager(mViewPager);
        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mTab1.setTextColor(getResources().getColor(R.color.white));
                        mTab2.setTextColor(getResources().getColor(R.color.primary_content_color));
                        break;
                    case 1:
                        mTab1.setTextColor(getResources().getColor(R.color.primary_content_color));
                        mTab2.setTextColor(getResources().getColor(R.color.white));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mTab1 = (TextView) findViewById(R.id.tab1);
        mTab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0);
            }
        });
        mTab2 = (TextView) findViewById(R.id.tab2);
        mTab2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mViewPager.setCurrentItem(1);
            }
        });

        mSingup = (Button) findViewById(R.id.singup);
        mSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(GuestDialog!=null) GuestDialog = null;
                GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                if(Constants.INTERNATIONAL_VERSION){
                    GuestDialog.setContentView(R.layout.guest_singup_dialog_google);
                }
                else{
                    GuestDialog.setContentView(R.layout.guest_singup_dialog_china);
                }
                Window window = GuestDialog.getWindow();
                window.setGravity(Gravity.BOTTOM);
                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                LinearLayout mWeibo = (LinearLayout)GuestDialog.findViewById(R.id.weibo_layout);
                mWeibo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try{
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                            mHashMap.put("version", Singleton.getVersion());
                            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                            mHashMap.put("u", Singleton.getPhoneIMEI());
                            mHashMap.put("dn", DevUtils.getDevInfo());
                            LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_signup_webio", mHashMap);
                        }
                        catch (Exception e){
                        }

                        //Umeng monitor
                        String Umeng_id="GuestModeSignupWeibo";
                        HashMap<String,String> mHashMap = new HashMap<String,String>();
                        mHashMap.put(Umeng_id, Umeng_id);
                        MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                        GuestDialog.dismiss();
                        showWebio();
                    }
                });

                LinearLayout mQQ = (LinearLayout)GuestDialog.findViewById(R.id.qq_layout);
                mQQ.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try{
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                            mHashMap.put("version", Singleton.getVersion());
                            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                            mHashMap.put("u", Singleton.getPhoneIMEI());
                            mHashMap.put("dn", DevUtils.getDevInfo());
                            LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_signup_qq", mHashMap);
                        }
                        catch (Exception e){
                        }

                        //Umeng monitor
                        String Umeng_id="GuestModeSignupQQ";
                        HashMap<String,String> mHashMap = new HashMap<String,String>();
                        mHashMap.put(Umeng_id, Umeng_id);
                        MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                        GuestDialog.dismiss();
                        mTencent = Tencent.createInstance(Constants.QQAppid, mCtx);
                        mTencent.login(mCtx, "all", loginListener);
                    }
                });

                LinearLayout mWechat = (LinearLayout)GuestDialog.findViewById(R.id.wechat_layout);
                mWechat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GuestDialog.dismiss();

                        try{
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                            mHashMap.put("version", Singleton.getVersion());
                            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                            mHashMap.put("u", Singleton.getPhoneIMEI());
                            mHashMap.put("dn", DevUtils.getDevInfo());
                            LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_signup_wechat", mHashMap);
                        }
                        catch (Exception e){
                        }

                        if (ShareUtil.isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI"))
                        {
                        //Umeng monitor
                        String Umeng_id="GuestModeSignupWechat";
                        HashMap<String,String> mHashMap = new HashMap<String,String>();
                        mHashMap.put(Umeng_id, Umeng_id);
                        MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                        IWXAPI api;
                        api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
                        api.registerApp(Constants.WECHAT_APP_KEY);
                        SendAuth.Req req = new SendAuth.Req();
                        req.scope = "snsapi_userinfo";
                        req.state = "wechat_sdk_demo";
                        api.sendReq(req);
                        }
                        else {
                            Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.fb_layout);
                mFB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GuestDialog.dismiss();

                        try{
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                            mHashMap.put("version", Singleton.getVersion());
                            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                            mHashMap.put("u", Singleton.getPhoneIMEI());
                            mHashMap.put("dn", DevUtils.getDevInfo());
                            LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_signup_fb", mHashMap);
                        }
                        catch (Exception e){
                        }

                        Intent intent;

                        //Umeng monitor
                        String Umeng_id_FB="FBlogin";
                        HashMap<String,String> mHashMap_FB = new HashMap<String,String>();
                        mHashMap_FB.put(Umeng_id_FB, Umeng_id_FB);
                        MobclickAgent.onEventValue(mCtx,Umeng_id_FB,mHashMap_FB,0);

                        FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
                        callbackManager = CallbackManager.Factory.create();
                        LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

                        LoginManager.getInstance().registerCallback(callbackManager,
                                new FacebookCallback<LoginResult>() {
                                    @Override
                                    public void onSuccess(final LoginResult loginResult) {
                                        final String token = loginResult.getAccessToken().getToken();

                                        GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                            @Override
                                            public void onCompleted(JSONObject user, GraphResponse response) {
                                                if (user != null) {
                                                    try {
                                                        showProgressDialog();
                                                        final String id = user.optString("id");
                                                        final String name = user.optString("name");
//                                                    Log.e("DDD", "ID: " + id);

                                                        // Download Profile photo
                                                        DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, id);
                                                        downloadFBProfileImage.execute();

                                                        ApiManager.checkFacebookIDAvailable(mCtx, id, new ApiManager.checkFacebookIDAvailableCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                if (success) {

                                                                    // Download user profile image
//                                                                DownloadFBProfileImage download = new DownloadFBProfileImage(mCtx, id);
//                                                                download.execute();

                                                                    //Signup
                                                                    GraphRequest request = GraphRequest.newMeRequest(
                                                                            loginResult.getAccessToken(),
                                                                            new GraphRequest.GraphJSONObjectCallback() {
                                                                                @Override
                                                                                public void onCompleted(
                                                                                        JSONObject object,
                                                                                        GraphResponse response) {
                                                                                    // Application code
                                                                                    String email = object.optString("email");
//                                                                                Log.e("DDD", "EMAIL: " + email);


                                                                                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, id).commit();
                                                                                    sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                                                                    sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                                    sharedPreferences.edit().putString("signup_type", "fb").commit();
                                                                                    sharedPreferences.edit().putString("signup_token", token).commit();

                                                                                    if (null != email) {
                                                                                        if (email.contains("@")) {
                                                                                            String username = email.substring(0, email.indexOf("@"));
                                                                                            sharedPreferences.edit().putString(Constants.EMAIL, email).commit();
                                                                                            sharedPreferences.edit().putString(Constants.NAME, username).commit();
                                                                                        }
                                                                                    }
                                                                                    hideProgressDialog();
                                                                                    startActivity(new Intent(mCtx, SignupActivityV2.class));
//                                                                                    mCtx.finish();
                                                                                }
                                                                            });

                                                                    Bundle parameters = new Bundle();
                                                                    parameters.putString("fields", "email");
                                                                    request.setParameters(parameters);
                                                                    request.executeAsync();

                                                                } else {
                                                                    //login directly
                                                                    ApiManager.loginAction2(mCtx, "", "", id, new ApiManager.LoginAction2Callback() {
                                                                        @Override
                                                                        public void onResult(boolean success, String message, UserModel user) {
                                                                            hideProgressDialog();

                                                                            if (success) {
                                                                                if (message.equals("ok")) {

                                                                                    Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                                    mStory17Application.setUser(user);

                                                                                    Intent intent = new Intent();
                                                                                    intent.setClass(mCtx, MenuActivity.class);
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                } else if (message.equals("freezed"))
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                                else
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                            } else {
                                                                                showNetworkUnstableToast();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });


                                                    } catch (Exception e) {
                                                        hideProgressDialog();
                                                        try {
//                              showToast(getString(R.string.failed));
                                                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                        } catch (Exception x) {
                                                        }
                                                    }
                                                }
                                            }
                                        }).executeAsync();
                                    }

                                    @Override
                                    public void onCancel() {
                                        hideProgressDialog();
                                        try {
//                              showToast(getString(R.string.failed));
                                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }

                                    @Override
                                    public void onError(FacebookException exception) {
                                        hideProgressDialog();
                                        try {
//                              showToast(getString(R.string.failed));
                                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception x) {
                                        }
                                    }
                                });
                    }
                });

                LinearLayout mMessage = (LinearLayout)GuestDialog.findViewById(R.id.messag_layout);
                mMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GuestDialog.dismiss();

                        try{
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                            mHashMap.put("version", Singleton.getVersion());
                            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                            mHashMap.put("u", Singleton.getPhoneIMEI());
                            mHashMap.put("dn", DevUtils.getDevInfo());
                            LogEventUtil.sendSDKLogEvent(mCtx, "v26_click_login", mHashMap);
                        }
                        catch (Exception e){
                        }

                        //Umeng monitor
                        String Umeng_id="GuestModeSignupSMS";
                        HashMap<String,String> mHashMap = new HashMap<String,String>();
                        mHashMap.put(Umeng_id, Umeng_id);
                        MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);


                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString("signup_type", "message").commit();
                        sharedPreferences.edit().putString("signup_token", "").commit();
                        Intent intent = new Intent();
                        intent.setClass(mCtx,SignupActivityV2.class);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                });

                Button mEnd = (Button)GuestDialog.findViewById(R.id.end);
                mEnd.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        GuestDialog.dismiss();

                    }
                });

                GuestDialog.show();
//                mCtx.finish();

//                IWXAPI api;
//                api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
//                api.registerApp(Constants.WECHAT_APP_KEY);
//                SendAuth.Req req = new SendAuth.Req();
//                req.scope = "snsapi_userinfo";
//                req.state = "wechat_sdk_demo";
//                api.sendReq(req);

//                mTencent = Tencent.createInstance(Constants.QQAppid, mCtx);
//                mTencent.login(mCtx, "all", loginListener);


            }
        });

        mLogin = (Button) findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mCtx, LoginActivity_V3.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        ApiManager.getExploreSuggestedUsers(mCtx, "", Integer.MAX_VALUE,50,new ApiManager.GetExploreSuggestedUsersCallback()
        {
            @Override
            public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested)
            {
                if (success && suggested != null)
                {
                    if(suggested.size()!=0)
                    {
                        mSuggested.clear();
                        mSuggested.addAll(suggested);
                    }
                }
            }
        });

        if(Build.VERSION.SDK_INT >= 23)
        {
            ArrayList<String> mPermission = new ArrayList<String>();

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.CAMERA);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.RECORD_AUDIO);
            }

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                mPermission.add(Manifest.permission.READ_CONTACTS);
            }

            final String[] permission = new String[mPermission.size()];
            for(int i = 0 ; i < mPermission.size() ; i++)
            {
                permission[i] = mPermission.get(i);
            }

            if(permission.length!=0)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                builder.setMessage(getString(R.string.check_permission_title));
                builder.setTitle(getString(R.string.system_notif));
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(mCtx, permission, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
                    }
                }).setCancelable(false);
                builder.create().show();
            }
        }

        if(mApplication.showUpdateDialog) {
            mApplication.showUpdateDialog = false;
            ApiManager.getNewestAppVersion(mCtx, new ApiManager.GetNewestAppVersionCallback() {
                @Override
                public void onResult(boolean success, String newestVersion, final String downloadURL) {
                    if(success && newestVersion.length()!=0 && downloadURL.length()!=0)
                    {
                        if(newestVersion.compareTo(Singleton.getVersion())!=0)
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                            if(Constants.INTERNATIONAL_VERSION) builder.setMessage("17 App Have New Version, Update？");
                            else builder.setMessage("17有新版本，是否进行更新？");
                            builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        Uri uri = Uri.parse(downloadURL);
                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                    catch (Exception e) {
                                    }
                                }
                            });
                            builder.setNegativeButton(R.string.cancel,null);
                            builder.create().show();
                        }
                    }
                }
            });
        }
    }

    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        private final String fbImageDownlaodUrl = "https://graph.facebook.com/";
        private final String attrs = "/picture?width=1080&height=1080";
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

            this.url = fbImageDownlaodUrl + userID + attrs;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        doNext(requestCode, grantResults);
    }

    private final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 1;

    private void doNext(int requestCode, int[] grantResults)
    {
        if (requestCode == WRITE_EXTERNAL_STORAGE_REQUEST_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                // Permission Granted
            }
            else
            {
                // Permission Denied
            }
        }
    }

    private void initTitleBar()
    {
        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSuggested.size()!=0)
                {
                    mApplication.setSuggested(mSuggested);
                    Intent intent = new Intent();
                    intent.setClass(mCtx, FollowSuggestedActivity.class);
                    intent.putExtra("guest", true);
                    startActivity(intent);
//                    mCtx.finish();
                }
                else
                {
                    ApiManager.getExploreSuggestedUsers(mCtx, "", Integer.MAX_VALUE,50,new ApiManager.GetExploreSuggestedUsersCallback()
                    {
                        @Override
                        public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested)
                        {
                            if (success && suggested != null)
                            {
                                if(suggested.size()!=0)
                                {
                                    mSuggested.clear();
                                    mSuggested.addAll(suggested);

                                    mApplication.setSuggested(mSuggested);
                                    Intent intent = new Intent();
                                    intent.setClass(mCtx, FollowSuggestedActivity.class);
                                    intent.putExtra("guest", true);
                                    startActivity(intent);
//                                    mCtx.finish();
                                }
                                else
                                {
                                    try{
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                            else
                            {
                                try{
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    //webio----
    private Dialog mWeiboDialog;
    private void showWebio()
    {
        if(mWeiboDialog!=null) mWeiboDialog = null;
        mWeiboDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        mWeiboDialog.setContentView(R.layout.instagram_dialog);
        final WebView mWeb = (WebView) mWeiboDialog.findViewById(R.id.web);
        final ProgressBar mProgress = (ProgressBar) mWeiboDialog.findViewById(R.id.progress);
        mWeb.getSettings().setJavaScriptEnabled(true);
        mWeiboDialog.setCancelable(true);
        mWeb.loadUrl("https://api.weibo.com/oauth2/authorize?client_id=" + Constants.WEIBO_APP_KEY + "&response_type=code&redirect_uri=" + Constants.WEIBO_REDIRECT_URL);
        mWeb.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                super.onPageStarted(view, url, favicon);
                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);

                if(url.contains(Constants.IG_REDIRECT_URI) && url.contains("code="))
                {
                    mWeiboDialog.dismiss();
                    showProgressDialog();

                    int pos = url.indexOf("=") + 1;
                    String token = url.substring(pos,url.length());
                    try
                    {
                        OkHttpClient mClient = new OkHttpClient();

                        RequestBody body = new FormEncodingBuilder()
                                .add("client_id", Constants.WEIBO_APP_KEY)
                                .add("client_secret", Constants.WEIBO_SECRET)
                                .add("grant_type", "authorization_code")
                                .add("code", token)
                                .add("redirect_uri", Constants.WEIBO_REDIRECT_URL)
                                .build();

                        Request request = new Request.Builder().url("https://api.weibo.com/oauth2/access_token").post(body).build();
                        mClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful())
                                {
                                    try
                                    {
                                        JSONObject data = new JSONObject(response.body().string());
                                        final String token = data.getString("access_token");
                                        final String uid = data.getString("uid");

                                        OkHttpClient client = new OkHttpClient();
                                        Request request = new Request.Builder().url("https://api.weibo.com/2/users/show.json?access_token=" + token + "&uid=" + uid).build();
                                        client.newCall(request).enqueue(new Callback()
                                        {
                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                hideProgressDialog();
                                                showToast(getString(R.string.failed));
                                            }

                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if(response.isSuccessful())
                                                {
                                                    try
                                                    {

                                                        try{
                                                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                                                            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                                                            mHashMap.put("version", Singleton.getVersion());
                                                            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                                                            mHashMap.put("u", Singleton.getPhoneIMEI());
                                                            mHashMap.put("dn", DevUtils.getDevInfo());
                                                            LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_get_webio_token", mHashMap);
                                                        }
                                                        catch (Exception e){
                                                        }

                                                        JSONObject info = new JSONObject(response.body().string());
                                                        final int id = info.getInt("id");
                                                        final String screen_name = info.getString("screen_name");
                                                        final String pic = info.getString("profile_image_url");

                                                        DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                                        downloadFBProfileImage.execute();

                                                        ApiManager.checkWeiboIDAvailable(mCtx, uid /*String.valueOf(id)*/, new ApiManager.checkFacebookIDAvailableCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                if (success) {
                                                                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, uid).commit();
                                                                    sharedPreferences.edit().putString(Constants.FULL_NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putString(Constants.NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                    sharedPreferences.edit().putString("is_weibo", "weibo").commit();

                                                                    sharedPreferences.edit().putString("signup_type", "weibo").commit();
                                                                    sharedPreferences.edit().putString("signup_token", token).commit();

                                                                    hideProgressDialog();
                                                                    startActivity(new Intent(mCtx, SignupActivityV2.class));
//                                                                    mCtx.finish();
                                                                } else {
                                                                    ApiManager.loginActionChina(mCtx, "", "", "weiboID", uid, new ApiManager.LoginAction2Callback() {
                                                                        @Override
                                                                        public void onResult(boolean success, String message, UserModel user) {
                                                                            hideProgressDialog();

                                                                            if (success) {
                                                                                if (message.equals("ok")) {

                                                                                    Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                                    mStory17Application.setUser(user);

                                                                                    Intent intent = new Intent();
                                                                                    intent.setClass(mCtx, MenuActivity.class);
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                } else if (message.equals("freezed"))
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                                else
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                            } else {
                                                                                showNetworkUnstableToast();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                    catch (JSONException e)
                                                    {
                                                        hideProgressDialog();
                                                        showToast(getString(R.string.failed));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    catch (JSONException e)
                                    {
                                        hideProgressDialog();
                                        showToast(getString(R.string.failed));
                                    }
                                }
                                else
                                {
                                    hideProgressDialog();
                                    showToast(getString(R.string.failed));
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        hideProgressDialog();
                        showToast(getString(R.string.failed));
                    }
                }
                else
                {
//                            mWeiboDialog.dismiss();
//                            hideProgressDialog();
//                            showToast(getString(R.string.failed));
                }
            }
        });

        try{
            mWeb.clearCache(true);
            mWeb.getSettings().setCacheMode(WebView.PERSISTENT_NO_CACHE);
        }
        catch (Exception e){
        }

        mWeiboDialog.show();
    }

    //QQ-------

    public static Tencent mTencent;

    private class BaseUiListener implements IUiListener
    {

        @Override
        public void onComplete(Object response) {
            if (null == response) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            if (null != jsonResponse && jsonResponse.length() == 0) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }

            doComplete((JSONObject)response);
        }

        protected void doComplete(JSONObject values) {

        }

        @Override
        public void onError(UiError e) {
            hideProgressDialog();
            showToast(getString(R.string.failed));
        }

        @Override
        public void onCancel() {
            hideProgressDialog();
        }
    }

    private IUiListener loginListener = new BaseUiListener()
    {
        @Override
        protected void doComplete(JSONObject jsonObject)
        {
            try
            {
                final String token = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_ACCESS_TOKEN);
                String expires = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_EXPIRES_IN);
                final String openId = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_OPEN_ID);

                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);

                if (mTencent != null && mTencent.isSessionValid())
                {
                    IUiListener listener = new IUiListener()
                    {
                        @Override
                        public void onError(UiError e) {
                            hideProgressDialog();
                            showToast(getString(R.string.failed));
                        }

                        @Override
                        public void onComplete(final Object response)
                        {
                            try
                            {

                                try{
                                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                                    mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                                    mHashMap.put("version", Singleton.getVersion());
                                    mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                                    mHashMap.put("u", Singleton.getPhoneIMEI());
                                    mHashMap.put("dn", DevUtils.getDevInfo());
                                    LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_get_qq_token", mHashMap);
                                }
                                catch (Exception e){
                                }

                                JSONObject json = (JSONObject)response;

                                final String name = json.getString("nickname");
                                String pic = json.getString("figureurl_qq_1");

                                DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                downloadFBProfileImage.execute();

                                ApiManager.checkQQIDAvailable(mCtx, openId, new ApiManager.checkFacebookIDAvailableCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, openId).commit();
                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                            sharedPreferences.edit().putString(Constants.NAME, name).commit();
                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();
                                            sharedPreferences.edit().putString("signup_type", "qq").commit();
                                            sharedPreferences.edit().putString("signup_token", token).commit();
                                            hideProgressDialog();
                                            startActivity(new Intent(mCtx, SignupActivityV2.class));
//                                            mCtx.finish();
                                        }
                                        else
                                        {
                                            ApiManager.loginActionChina(mCtx, "", "", "qqID", openId, new ApiManager.LoginAction2Callback() {
                                                @Override
                                                public void onResult(boolean success, String message, UserModel user) {
                                                    hideProgressDialog();

                                                    if (success) {
                                                        if (message.equals("ok")) {

                                                            Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                            mStory17Application.setUser(user);

                                                            Intent intent = new Intent();
                                                            intent.setClass(mCtx, MenuActivity.class);
                                                            startActivity(intent);
                                                            mCtx.finish();
                                                        } else if (message.equals("freezed"))
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                        else
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                    } else {
                                                        showNetworkUnstableToast();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }
                        }

                        @Override
                        public void onCancel() {
                            hideProgressDialog();
                        }
                    };

                    UserInfo mInfo = new UserInfo(mCtx, mTencent.getQQToken());
                    mInfo.getUserInfo(listener);
                }
            }
            catch(Exception e) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
            }
        }
    };

    class DownloadProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        String url = "";

        public DownloadProfileImage(Context context, String userID) {
            this.context = context;

            this.url = userID.substring(0,userID.length()-2) + 640;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {
//                    java.net.URL url = new java.net.URL(this.url);
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setDoInput(true);
//                    connection.connect();

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if(mTencent!=null) mTencent.logout(mCtx);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == com.tencent.connect.common.Constants.REQUEST_LOGIN || requestCode == com.tencent.connect.common.Constants.REQUEST_APPBAR)
        {
            Tencent.onActivityResultData(requestCode,resultCode,data,loginListener);
        }

        try {
            if(callbackManager!=null) callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        catch (Exception e) {
        }
    }

    //---------

    private PagerAdapter mPagerAdapter = new PagerAdapter()
    {
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            return mViewList.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mViewList.get(position));
        }

        @Override
        public int getItemPosition(Object object)
        {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            if(position==0)
            {
                container.addView(mViewList.get(position));

                View view = mViewList.get(position);

                mNoDataLive = (ImageView) view.findViewById(R.id.nodata);
                mProgressLive = (ProgressBar) view.findViewById(R.id.progress);
                mListViewLive = (PullToRefreshGridView) view.findViewById(R.id.feed_grid);

                mListViewLive.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase refreshView)
                    {
                        ApiManager.getHotLiveStreams2(mCtx, "local",Integer.MAX_VALUE, 15, new ApiManager.GetHotLiveStreamsCallback()
                        {
                            @Override
                            public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels)
                            {
                                mListViewLive.onRefreshComplete();

                                if (success && liveModels != null)
                                {
                                    if(liveModels.size()!=0)
                                    {
                                        mNoDataLive.setVisibility(View.GONE);
                                        mLiveModels.clear();
                                        mLiveModels.addAll(liveModels);

                                        if(mListAdapterLive!=null) mListAdapterLive = null;
                                        mListAdapterLive = new LiveLstAdapter();
                                        mListViewLive.setAdapter(mListAdapterLive);
                                    }
                                    else
                                    {
                                        mNoDataLive.setVisibility(View.VISIBLE);
                                    }
                                }
                                else
                                {
                                    mNoDataLive.setVisibility(View.VISIBLE);
                                    try{
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                });

                mListViewLive.setOnScrollListener(new AbsListView.OnScrollListener()
                {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState)
                    {
                        if (scrollState == SCROLL_STATE_FLING) {
                            ImageLoader.getInstance().pause();
                        }

                        if (scrollState == SCROLL_STATE_IDLE) {
                            ImageLoader.getInstance().resume();
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });

                if(mLiveModels.size()==0)
                {
                    mProgressLive.setVisibility(View.VISIBLE);

                    ApiManager.getHotLiveStreams2(mCtx, "local", Integer.MAX_VALUE, 15, new ApiManager.GetHotLiveStreamsCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels)
                        {
                            mProgressLive.setVisibility(View.GONE);

                            if (success && liveModels != null)
                            {
                                if(liveModels.size()!=0)
                                {
                                    mNoDataLive.setVisibility(View.GONE);
                                    mLiveModels.clear();
                                    mLiveModels.addAll(liveModels);

                                    mListAdapterLive = new LiveLstAdapter();
                                    mListViewLive.setAdapter(mListAdapterLive);
                                }
                                else
                                {
                                    mNoDataLive.setVisibility(View.VISIBLE);
                                }
                            }
                            else
                            {
                                try
                                {
                                    mNoDataLive.setVisibility(View.VISIBLE);
                                    try{
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                                catch(Exception e)
                                {

                                }
                            }
                        }
                    });
                }
                else mProgressLive.setVisibility(View.GONE);

                return mViewList.get(position);
            }
            else
            {
                container.addView(mViewList.get(position));

                View view = mViewList.get(position);

                mNoDataExplore = (ImageView) view.findViewById(R.id.nodata);
                mProgressExplore = (ProgressBar) view.findViewById(R.id.progress);
                mListViewExplore = (PullToRefreshGridView) view.findViewById(R.id.feed_grid);

                mListViewExplore.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase refreshView)
                    {
                        ApiManager.getHotPost(mCtx, Integer.MAX_VALUE, 18, new ApiManager.GetHotPostsCallback() {
                            @Override
                            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                                mListViewExplore.onRefreshComplete();

                                if (success && feedModel != null) {
                                    if (feedModel.size() != 0) {
                                        mNoDataExplore.setVisibility(View.GONE);
                                        mFeedModels.clear();
                                        mFeedModels.addAll(feedModel);

                                        if (mListAdapterExplore != null) mListAdapterExplore = null;
                                        mListAdapterExplore = new ExploreAdapter();
                                        mListViewExplore.setAdapter(mListAdapterExplore);

                                        isFetchingDataExplore = false;
                                        noMoreDataExplore = false;

                                        if (mFeedModels.size() < 18) {
                                            noMoreDataExplore = true;
                                        }
                                    } else {
                                        mNoDataExplore.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    mNoDataExplore.setVisibility(View.VISIBLE);
                                    try {
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {
                                    }
                                }
                            }
                        });
                    }
                });

                mListViewExplore.setOnScrollListener(new AbsListView.OnScrollListener()
                {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState)
                    {
                        if (scrollState == SCROLL_STATE_FLING) {
                            ImageLoader.getInstance().pause();
                        }

                        if (scrollState == SCROLL_STATE_IDLE) {
                            ImageLoader.getInstance().resume();
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });

                if(mFeedModels.size()==0)
                {
                    mProgressExplore.setVisibility(View.VISIBLE);

                    ApiManager.getHotPost(mCtx, Integer.MAX_VALUE, 18, new ApiManager.GetHotPostsCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                            mProgressExplore.setVisibility(View.GONE);

                            if (success && feedModel != null) {
                                if (feedModel.size() != 0) {
                                    mNoDataExplore.setVisibility(View.GONE);
                                    mFeedModels.clear();
                                    mFeedModels.addAll(feedModel);
                                    mListAdapterExplore = new ExploreAdapter();
                                    mListViewExplore.setAdapter(mListAdapterExplore);

                                    if (mFeedModels.size() < 18) {
                                        noMoreDataExplore = true;
                                    }
                                } else {
                                    mNoDataExplore.setVisibility(View.VISIBLE);
                                }
                            } else {
                                try {
                                    mNoDataExplore.setVisibility(View.VISIBLE);
                                    try {
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }
                    });
                }
                else mProgressExplore.setVisibility(View.GONE);

                return mViewList.get(position);
            }
        }
    };

    private class LiveLstAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mLiveModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.guest_live_row, null);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.message = (LinearLayout) convertView.findViewById(R.id.message);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.image.getLayoutParams().width = mGridHeight;
            holder.image.getLayoutParams().height = mGridHeight;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(position).getUserInfo().getPicture()), holder.image, SelfOptions);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try{
                        HashMap<String,String> mHashMap = new HashMap<String,String>();
                        mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
                        mHashMap.put("version", Singleton.getVersion());
                        mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
                        mHashMap.put("u", Singleton.getPhoneIMEI());
                        mHashMap.put("dn", DevUtils.getDevInfo());
                        LogEventUtil.sendSDKLogEvent(mCtx, "v26_guest_live", mHashMap);
                    }
                    catch (Exception e){
                    }

                    //Umeng monitor
                    String Umeng_id="GuestModeLivestream";
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                    Intent intent = new Intent();
                    intent.setClass(mCtx, LiveStreamActivity.class);
                    intent.putExtra("liveStreamID", mLiveModels.get(position).getLiveStreamID());
                    intent.putExtra("guest", true);
                    //event tracking: to track what page user enter livestream
                    intent.putExtra("enterLiveFrom","guest");
                    startActivity(intent);
//                    mCtx.finish();
                }
            });

            holder.name.setText(mLiveModels.get(position).getUserInfo().getOpenID());

            holder.message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mLiveModels.get(position).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mLiveModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mLiveModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mLiveModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mLiveModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mLiveModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mLiveModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mLiveModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mLiveModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mLiveModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("guest", true);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            if(mLiveModels.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.dio.setVisibility(View.VISIBLE);
            holder.dio.setText(mLiveModels.get(position).getCaption());

//            holder.dio.linkify(new FeedTagActionHandler()
//            {
//                @Override
//                public void handleHashtag(String hashtag)
//                {
//                    if(mApplication!=null)
//                    {
//                        mApplication.showGuestLogin(mCtx);
//                    }
//
////                    Intent intent = new Intent();
////                    intent.setClass(mCtx, TagPostActivity.class);
////                    intent.putExtra("tag", hashtag);
////                    startActivity(intent);
//                }
//
//                @Override
//                public void handleMention(String mention)
//                {
//                    mProgressLive.setVisibility(View.VISIBLE);
//                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback()
//                    {
//                        @Override
//                        public void onResult(boolean success, String message, UserModel user)
//                        {
//                            mProgressLive.setVisibility(View.GONE);
//                            if (success && user != null) {
//                                Intent intent = new Intent();
//                                intent.setClass(mCtx, HomeUserActivity.class);
//                                intent.putExtra("title", user.getName());
//                                intent.putExtra("picture", user.getPicture());
//                                intent.putExtra("isfollowing", user.getIsFollowing());
//                                intent.putExtra("post", user.getPostCount());
//                                intent.putExtra("follow", user.getFollowerCount());
//                                intent.putExtra("following", user.getFollowingCount());
//                                intent.putExtra("open", user.getOpenID());
//                                intent.putExtra("bio", user.getBio());
//                                intent.putExtra("targetUserID", user.getUserID());
//                                intent.putExtra("web", user.getWebsite());
//                                intent.putExtra("guest", true);
//                                startActivity(intent);
//                            }
//                        }
//                    });
//                }
//
//                @Override
//                public void handleEmail(String email) {
//
//                }
//
//                @Override
//                public void handleUrl(String url) {
//                    try
//                    {
//                        Uri uri = Uri.parse(url);
//                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                        startActivity(intent);
//                    }
//                    catch (Exception e)
//                    {
//                        try{
//                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
//                        }
//                        catch (Exception x){
//                        }
//                    }
//                }
//            });
//
//            removeLine(holder.dio);

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView image;
        TextView name;
        ImageView verifie;
        TextView dio;
        LinearLayout message;
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private class ExploreAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderExplore holder = new ViewHolderExplore();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.grid_row, null);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.video = (ImageView) convertView.findViewById(R.id.video);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderExplore) convertView.getTag();

            holder.image.getLayoutParams().width = mGridHeightExpore;
            holder.image.getLayoutParams().height = mGridHeightExpore;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX +mFeedModels.get(position).getPicture()), holder.image,SelfOptions);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mApplication.setGuestFeed(mFeedModels);
                    Intent intent = new Intent();
                    intent.setClass(mCtx, GuestFeedActivity.class);
                    intent.putExtra("guest", true);
                    intent.putExtra("pos", position + 1);
                    startActivity(intent);
//                    mCtx.finish();
                }
            });

            if(mFeedModels.get(position).getType().compareTo("image")==0) holder.video.setVisibility(View.GONE);
            else holder.video.setVisibility(View.VISIBLE);

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    }

    private class ViewHolderExplore
    {
        ImageView image;
        ImageView video;
    }

    public void LoadData(final boolean refresh)
    {
        if(isFetchingDataExplore)
        {
            return;
        }

        if(refresh)
        {
            noMoreDataExplore = false;
        }

        if(noMoreDataExplore)
        {
            return;
        }

        isFetchingDataExplore = true;

        ApiManager.getHotPost(mCtx, mFeedModels.get(mFeedModels.size() - 1).getTimestamp(), 18, new ApiManager.GetHotPostsCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
            {
                isFetchingDataExplore = false;

                if (success && feedModel!=null)
                {
                    if (feedModel.size() != 0)
                    {
                        mFeedModels.addAll(feedModel);

                        if (feedModel.size() < 18)
                        {
                            noMoreDataExplore = true;
                        }
                        else noMoreDataExplore = false;

                        if (mListAdapterExplore != null)
                        {
                            mListAdapterExplore.notifyDataSetChanged();
                        }
                    }
                } else {
                    try{
                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            hideKeyboard();

            if(!mSignOut)
            {
                if(mApplication!=null) mApplication.setSignOutState(false);
            }
            else
            {
                if(mApplication!=null) mApplication.setGuestFinish(true);
            }
            mCtx.finish();

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }
}
