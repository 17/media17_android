package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by POPO on 5/21/15.
 */
public class SignupFullnameActivityV2 extends BaseActivity
{
    private SharedPreferences sharedPreferences;

    private SignupFullnameActivityV2 mCtx = this;
    private EditText mFullname;
    private Button btnNext;
    private Story17Application mApplication;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity_fullname_v2);

        mApplication = (Story17Application) getApplication();
        initTitleBar();

        btnNext = (Button)findViewById(R.id.btn_next);
        mFullname = (EditText) findViewById(R.id.fullname);

        //event tracking
        try{
            LogEventUtil.SuccessfulRegister(mCtx,mApplication);
        }catch (Exception x)
        {

        }

        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);

//        boolean fbLogin = sharedPreferences.getBoolean("fblogin", false);
        String fullname = sharedPreferences.getString(Constants.FULL_NAME, "");
        if(fullname.length()!=0){
            mFullname.setText(fullname);
        }



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNext(false);
            }
        });



        showKeyboard();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.full_name));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        Button btnSkip = (Button)findViewById(R.id.btn_right);
        btnSkip.setText(getString(R.string.skip));
        btnSkip.setVisibility(View.VISIBLE);
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNext(true);
            }
        });
    }

    private void goNext(boolean skip){
        hideKeyboard();

        if(skip){
            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
            sharedPreferences.edit().putString(Constants.FULL_NAME, "").commit();
            Intent intent = new Intent();
            intent.setClass(mCtx, SignupPhotoActivityV2.class);
            startActivity(intent);
            mCtx.finish();
        }
        else
        {
            //account 2-20
            //passwoud 5-20
//            if(mFullname.getText().toString().trim().length()>2)
//            {
                showProgressDialog();

                sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                sharedPreferences.edit().putString(Constants.FULL_NAME, mFullname.getText().toString()).commit();
                Intent intent = new Intent();
                intent.setClass(mCtx, SignupPhotoActivityV2.class);
//        intent.putExtra("account", mAccount.getText().toString());
                startActivity(intent);
                mCtx.finish();
//            }
//            else {
//                try{
////                          showToast(getString(R.string.fullname_size));
//                    Toast.makeText(mCtx, getString(R.string.fullname_size), Toast.LENGTH_SHORT).show();
//                }
//                catch (Exception x){
//                }
//            }
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
//            Intent intent = new Intent();
//            intent.setClass(mCtx, LoginMenuActivity.class);
//            startActivity(intent);
//            mCtx.finish();

            return true;
        }

        if(keyCode == KeyEvent.KEYCODE_ENTER){
            goNext(false);
        }

        return super.onKeyUp(keyCode, event);
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }
}
