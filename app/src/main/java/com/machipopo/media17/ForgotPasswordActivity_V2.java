package com.machipopo.media17;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geetest.gt_sdk.GeetestLib;
import com.geetest.gt_sdk.GtDialog;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


public class ForgotPasswordActivity_V2 extends BaseActivity
{
    private ForgotPasswordActivity_V2 mCtx = this;
    private EditText mAccount, mPhone;
    private ConnectivityManager mConnectivityManager;
    private NetworkInfo mNetworkInfo;
    private RelativeLayout btnNext;
    private TextView mCountryCode, mCountry;
    private String UserCode = "Taiwan";
    private String[] mListCountry ;
    private String[] mListCode ;
    private String[] mListCountryAbbrv;
    private String mCountryAbbrv;
    private HashMap<String,String> CODE = new HashMap<String,String>();
    private HashMap<String,String> NAME = new HashMap<String,String>();
    private String SMSkey;
    private LinearLayout mDialog;
    private TextView mConfirmNumber, mCancel,mConfirm;
    private View mBackground,mTitleBarBackground;
    private SharedPreferences sharedPreferences;
    private String phone_verified;
    private String IPRequestCountRecord;
    private ProgressBar mProgress;
    private LinearLayout mCountryMenu;
    private String type;
    private Boolean mToSearchCountry = false;

    public final static String tag = "signup_setting";
    private GeetestLib gt = new GeetestLib();

    // 设置获取id，challenge，success的URL，需替换成自己的服务器URL
    private String captchaURL = Constants.SERVER_IP + "getHumanTestParams";

    // 设置二次验证的URL，需替换成自己的服务器URL
    private String validateURL = Constants.SERVER_IP + "validateHumanTestResponse";

    //parse verification code from SMS
    private final static String MSG_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());

        if(mToSearchCountry){

            if(Singleton.preferences.getString(Constants.COUNTRY_CALLING_CODE,"").length() >0 && Singleton.preferences.getString(Constants.PROFILE_COUNTRY,"").length() >0 && Singleton.preferences.getString(Constants.PHONE_TWO_DIGIT_ISO,"").length() >0) {
                mCountry.setText(Singleton.preferences.getString(Constants.PROFILE_COUNTRY, ""));
                mCountryCode.setText(Singleton.preferences.getString(Constants.COUNTRY_CALLING_CODE, ""));
                mCountryAbbrv = Singleton.preferences.getString(Constants.PHONE_TWO_DIGIT_ISO, "");
            }

            mToSearchCountry = false;
        }
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    static
    {
        System.loadLibrary("17media");
    }

    public class DataProvider
    {
        public native String sayHellolnC(String s);
    }

    public native String StringFromJNI(String stage);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity_v2);

        initTitleBar();

        mAccount = (EditText) findViewById(R.id.account);
        mPhone = (EditText) findViewById(R.id.phone);
        mCountryCode = (TextView) findViewById(R.id.country_code);
        mCountry = (TextView) findViewById(R.id.country);
        mCountryMenu = (LinearLayout) findViewById(R.id.country_menu);
        btnNext = (RelativeLayout) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(ButtonClickEventListener);
        mConfirmNumber= (TextView) findViewById(R.id.confirm_number);
        mConfirm= (TextView) findViewById(R.id.confirm);
        mCancel= (TextView) findViewById(R.id.cancel);
        mBackground = (View) findViewById(R.id.background);
        mDialog = (LinearLayout) findViewById(R.id.dialog);
        mTitleBarBackground = (View) findViewById(R.id.title_bar_background);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        sharedPreferences = getSharedPreferences(tag, 0);

        showKeyboard();

        DataProvider dp = new DataProvider();
        SMSkey= dp.sayHellolnC("");

//        if (captchaURL.equals(StringFromJNI("captura")))
//        {
//            Log.d("17_g","captura = "+StringFromJNI("captura"));
//        }
//
//        if (validateURL.equals(StringFromJNI("validate")))
//        {
//            Log.d("17_g","validate = "+StringFromJNI("validate"));
//        }

        gt.setCaptchaURL(captchaURL);
        gt.setValidateURL(validateURL);

        mCountryMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToSearchCountry = true;
                Intent intent = new Intent();
                intent.setClass(mCtx, SearchNationActivity.class);
                startActivity(intent);

            }
        });

        if(Singleton.preferences.getString(Constants.COUNTRY_CALLING_CODE, "").length()!=0 && Singleton.preferences.getString(Constants.LOCAL_PHONE_NUMBER, "").length()!=0)
        {
            try{
                mCountry.setText(getResources().getString(getResources().getIdentifier(Singleton.preferences.getString(Constants.PHONE_TWO_DIGIT_ISO, ""), "string", getPackageName())));
                mCountryAbbrv= Singleton.preferences.getString(Constants.PHONE_TWO_DIGIT_ISO, "") ;

                mCountryCode.setText(Singleton.preferences.getString(Constants.COUNTRY_CALLING_CODE, ""));
                mPhone.setText(Singleton.preferences.getString(Constants.LOCAL_PHONE_NUMBER, ""));
            }
            catch (Exception e){

            }
        }
        else
        {
            if(Constants.INTERNATIONAL_VERSION == true)
            {
                mCountry.setText(R.string.TW);
                mCountryCode.setText("886");
                mCountryAbbrv="TW" ;
            }
            else
            {
                mCountry.setText(R.string.CN);
                mCountryCode.setText("86");
                mCountryAbbrv="CN" ;
            }
        }

    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.forgot_password));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.INVISIBLE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_back_black);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mProgress.setVisibility(View.GONE);

                Intent intent = new Intent();
                intent.setClass(mCtx, LoginActivity_V3.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    private View.OnClickListener ButtonClickEventListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            if(mPhone.getText().toString().length()!=0 && mAccount.getText().toString().length() !=0) {

                //檢查使用者是否輸入正確電話格式
                String numberStr = mCountryCode.getText().toString()+mPhone.getText().toString();
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                try {
                    Phonenumber.PhoneNumber numberProto = phoneUtil.parse(numberStr, mCountryAbbrv);
                    boolean isValid = phoneUtil.isValidNumber(numberProto); // returns true
                    if(isValid)
                    {
                        hideKeyboard();
                        mBackground.setVisibility(View.VISIBLE);
                        mDialog.setVisibility(View.VISIBLE);
                        mTitleBarBackground.setVisibility(View.VISIBLE);
                        mConfirmNumber.setText("+ "+ mCountryCode.getText().toString() + " " + mPhone.getText().toString());

                        mCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showKeyboard();
                                mDialog.setVisibility(View.INVISIBLE);
                                mBackground.setVisibility(View.INVISIBLE);
                                mTitleBarBackground.setVisibility(View.INVISIBLE);
                            }
                        });

                        mConfirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                mDialog.setVisibility(View.INVISIBLE);
                                mBackground.setVisibility(View.INVISIBLE);
                                mTitleBarBackground.setVisibility(View.INVISIBLE);

                                //如果輸入台灣號碼，自動將第一個0去除 ex:0912123456 -> 912123456
                                if (mCountryCode.getText().toString().equals("886") && (mPhone.getText().toString().length() == 10)) {
                                    if ((mPhone.getText().toString().indexOf("0") == 0)) {
                                        phone_verified = mPhone.getText().toString().substring(1, 10);
                                    }
                                } else {
                                    phone_verified = mPhone.getText().toString();
                                }

                                Singleton.preferenceEditor.putString(Constants.PROFILE_COUNTRY_CODE, mCountryCode.getText().toString()).commit();
                                Singleton.preferenceEditor.putString(Constants.PROFILE_PHONE, phone_verified).commit();
                                Singleton.preferenceEditor.putString(Constants.PROFILE_COUNTRY, mCountry.getText().toString()).commit();
                                Singleton.preferenceEditor.putString(Constants.NAME, mAccount.getText().toString()).commit();
                                Singleton.preferenceEditor.putString(Constants.PROFILE_COUNTRY_ABBREVIATE, mCountryAbbrv);

//                        sharedPreferences = getSharedPreferences(tag, 0);
                                sharedPreferences.edit().putString(Constants.NAME, mAccount.getText().toString()).commit();

                                IPRequestCountRecord = "sendPhoneVerificationCode";
                                type = "resetPassword";

                                ApiManager.isPhoneNumberOpenIDPaired(mCtx, mCountryCode.getText().toString(), phone_verified, sharedPreferences.getString(Constants.NAME, ""), new ApiManager.IsPhoneNumberOpenIDPairedCallback() {
                                    @Override
                                    public void onResult(boolean success, String result, String isPaired, String message) {
                                        if (success) {
                                            try {
                                                if (result.equals("success")) {
                                                    if (isPaired.equals("yes")) {

                                                        //再送verification code之前，先向server確認是否要做human test
                                                        mProgress.setVisibility(View.VISIBLE);
                                                        ApiManager.checkNeedHumanTest(mCtx, IPRequestCountRecord, new ApiManager.CheckNeedHumanTestCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String result, String isNeeded) {
                                                                if (success) {
                                                                    try {
                                                                        if (isNeeded.equals("no")) {

                                                                            ApiManager.sendPhoneVerificationCode(mCtx, mCountryCode.getText().toString(), phone_verified, SMSkey, type, sharedPreferences.getString(Constants.NAME, ""), new ApiManager.SendPhoneVerificationInfoCallback() {
                                                                                @Override
                                                                                public void onResult(boolean success, String result, String message) {

                                                                                    mProgress.setVisibility(View.GONE);

                                                                                    if (success) {
                                                                                        try {
                                                                                            if (result.equals("success")) {
                                                                                                Toast.makeText(mCtx, getString(R.string.sms_sending), Toast.LENGTH_SHORT).show();
                                                                                                Intent intent = new Intent();
                                                                                                intent.setClass(mCtx, EnterSMSCertificationActivity.class);
                                                                                                intent.putExtra("SMS", 1);

                                                                                                startActivity(intent);
                                                                                                mCtx.finish();
                                                                                            } else {
                                                                                                if (message.equals("ip_rate_limit_exceeds")) {
                                                                                                    Toast.makeText(mCtx, getString(R.string.rate_limit), Toast.LENGTH_SHORT).show();

                                                                                                } else if (message.equals("nexmo_error")) {
                                                                                                    Toast.makeText(mCtx, getString(R.string.nexmo_error), Toast.LENGTH_SHORT).show();
                                                                                                } else if (message.equals("phoneNumber_used")) {
                                                                                                    Toast.makeText(mCtx, getString(R.string.phone_is_registered), Toast.LENGTH_SHORT).show();
                                                                                                } else if (message.equals("phoneNumber_error") || message.equals("openID_phoneNumber_not_paired")) {
                                                                                                    Toast.makeText(mCtx, getString(R.string.phoneNumber_error), Toast.LENGTH_SHORT).show();
                                                                                                } else {
                                                                                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                                                                }
                                                                                            }
                                                                                        } catch (Exception x) {
                                                                                        }
                                                                                    } else {
                                                                                        try {
                                                                                            //                          showToast(getString(R.string.error_failed));
                                                                                            mProgress.setVisibility(View.GONE);
                                                                                            Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                                                        } catch (Exception x) {

                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                        } else {
                                                                            //IPRequestCountRecord = "sendPhoneVerificationCode";
                                                                            mProgress.setVisibility(View.GONE);
                                                                            mDialog.setVisibility(View.INVISIBLE);
                                                                            GtAppDlgTask gtAppDlgTask = new GtAppDlgTask();
                                                                            gtAppDlgTask.execute();

                                                                        }
                                                                    } catch (Exception x) {
                                                                    }
                                                                } else {
                                                                    try {
                                                                        //                          showToast(getString(R.string.error_failed));
                                                                        mProgress.setVisibility(View.GONE);
                                                                        Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                                    } catch (Exception x) {

                                                                    }
                                                                }
                                                            }
                                                        });


                                                    } else {
                                                        mProgress.setVisibility(View.GONE);
                                                        Toast.makeText(mCtx, getString(R.string.phoneNumber_error), Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    mProgress.setVisibility(View.GONE);
                                                    Toast.makeText(mCtx, getString(R.string.phoneNumber_error), Toast.LENGTH_SHORT).show();
                                                }

                                            } catch (Exception x)
                                            {

                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                mProgress.setVisibility(View.GONE);
                                                Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x) {

                                            }
                                        }

                                    }
                                });

                            }
                        });


                    }
                    else
                    {
                        Toast.makeText(mCtx, getString(R.string.nexmo_error), Toast.LENGTH_SHORT).show();
                    }

                } catch (NumberParseException e) {
                    System.err.println("NumberParseException was thrown: " + e.toString());
                }



            }
        }
    };

    private void processLogin()
    {
        if(mAccount.getText().toString().length()!=0 && mPhone.getText().toString().length()!=0)
        {
            mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

            if (mNetworkInfo != null && mNetworkInfo.isConnected())
            {
                hideKeyboard();
                showProgressDialog();

            }
            else
            {
                hideKeyboard();
                try{
//                                                showToast(getString(R.string.login_internet));
                    Toast.makeText(mCtx, getString(R.string.login_internet), Toast.LENGTH_SHORT).show();
                }
                catch (Exception e){
                }
            }
        }
        else
        {
            hideKeyboard();
            try{
//                                                showToast(getString(R.string.login_enter));
                Toast.makeText(mCtx, getString(R.string.login_enter), Toast.LENGTH_SHORT).show();
            }
            catch (Exception e){
            }
        }
    }

    public String ReadFromfile(String fileName, Context context)
    {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try
        {
            fIn = context.getResources().getAssets().open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null)
            {
                returnString.append(line);
            }
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally
        {
            try
            {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            }
            catch (Exception e2)
            {
                e2.getMessage();
            }
        }

        return returnString.toString();
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    class GtAppDlgTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            return gt.startCaptcha();
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if (result) {

                openGtTest(mCtx, gt.getCaptcha(), gt.getChallenge(), true);

            } else {
                // 极验服务器暂时性宕机：
                Toast.makeText(
                        getBaseContext(),
                        getString(R.string.connet_erroe),
                        Toast.LENGTH_LONG).show();


                // 1. 可以选择继续使用极验，去掉下行注释
                // openGtTest(context, gt.getCaptcha(), gt.getChallenge(), false);

                // 2. 使用自己的验证
            }
        }
    }

    public void openGtTest(Context ctx, String captcha, String challenge, boolean success) {

        GtDialog dialog = new GtDialog(ctx, captcha, challenge, success);

        // 启用debug可以在webview上看到验证过程的一些数据
        // dialog.setDebug(true);


        dialog.setGtListener(new GtDialog.GtListener() {

            @Override
            public void gtResult(boolean success, String result) {

                if (success) {

                    //toastMsg("client captcha succeed:" + result);

                    try {
                        JSONObject res_json = new JSONObject(result);

                        //做完human test之後，如果成功就再送一次sendPhoneVerificationCode
                        type = "resetPassword";
                        ApiManager.validateHumanTestResponse(mCtx, res_json.getString("geetest_challenge"), res_json.getString("geetest_validate"), res_json.getString("geetest_seccode"), IPRequestCountRecord, new ApiManager.ValidateHumanTestResponseCallback() {
                            @Override
                            public void onResult(boolean success, String result) {
                                if (success) {
                                    try {
                                        if (result.equals("success")) {

                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();

                                            ApiManager.sendPhoneVerificationCode(mCtx, mCountryCode.getText().toString(), mPhone.getText().toString(), SMSkey, type, sharedPreferences.getString(Constants.NAME, ""), new ApiManager.SendPhoneVerificationInfoCallback() {
                                                @Override
                                                public void onResult(boolean success, String result, String message) {

                                                    mDialog.setVisibility(View.INVISIBLE);
                                                    mBackground.setVisibility(View.INVISIBLE);
                                                    mTitleBarBackground.setVisibility(View.INVISIBLE);

                                                    if (success) {
                                                        try {
                                                            if (result.equals("success")) {
                                                                Toast.makeText(mCtx, getString(R.string.sms_sending), Toast.LENGTH_SHORT).show();
                                                                Intent intent = new Intent();
                                                                intent.setClass(mCtx, EnterSMSCertificationActivity.class);
                                                                intent.putExtra("SMS", 1);

                                                                startActivity(intent);
                                                                mCtx.finish();
                                                            } else {

                                                                if (message.equals("ip_rate_limit_exceeds")) {
                                                                    Toast.makeText(mCtx, getString(R.string.rate_limit), Toast.LENGTH_SHORT).show();
                                                                } else if (message.equals("nexmo_error")) {
                                                                    Toast.makeText(mCtx, getString(R.string.nexmo_error), Toast.LENGTH_SHORT).show();
                                                                } else if (message.equals("phoneNumber_used")) {
                                                                    Toast.makeText(mCtx, getString(R.string.phone_is_registered), Toast.LENGTH_SHORT).show();
                                                                } else if (message.equals("phoneNumber_error") || message.equals("openID_phoneNumber_not_paired")) {
                                                                    Toast.makeText(mCtx, getString(R.string.phoneNumber_error), Toast.LENGTH_SHORT).show();
                                                                } else {
                                                                    Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        } catch (Exception x) {
                                                        }
                                                    } else {
                                                        try {
                                                            //                          showToast(getString(R.string.error_failed));
                                                            mProgress.setVisibility(View.GONE);
                                                            Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                                        } catch (Exception x) {
                                                        }
                                                    }
                                                }
                                            });

                                        } else {
                                            mProgress.setVisibility(View.GONE);
                                            Toast.makeText(mCtx, getString(R.string.human_failure), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (Exception x) {
                                    }
                                } else {
                                    try {
                                        //                          showToast(getString(R.string.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {
                                    }
                                }
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();
                    }


                } else {

                    //toastMsg("client captcha failed:" + result);
                }
            }

            @Override
            public void closeGt() {

                //toastMsg("Close geetest windows");
            }
        });

        dialog.show();
    }

    private void toastMsg(String msg) {

        //Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            mProgress.setVisibility(View.GONE);

            Intent intent = new Intent();
            intent.setClass(mCtx, LoginActivity_V3.class);
            startActivity(intent);
            mCtx.finish();

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

}
