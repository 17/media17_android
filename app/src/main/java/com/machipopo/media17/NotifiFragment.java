package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.View.SocialTextView;
import com.machipopo.media17.fragment.BaseFragment;
import com.machipopo.media17.model.NotifiModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.machipopo.media17.utils.SocialActionHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by POPO on 5/27/15.
 */
public class NotifiFragment extends BaseFragment
{
    private Story17Application mApplication;
    private ViewPager mViewPager, mHidePager;
    private View mViewSelf, mViewFollow;
    private List<View> mViewList;

    private TextView mTab1,mTab2;

    private LayoutInflater inflater;
    private DisplayImageOptions SelfOptions,PoOptions,PeopleOptions;

    private PullToRefreshListView mListViewSelf;
    private SelfLstAdapter mListAdapterSelf;
    private ProgressBar mProgressSelf;
    private ImageView mNoDataSelf;
    private Boolean isFetchingDataSelf = false;
    private Boolean noMoreDataSelf = false;
    public ArrayList<NotifiModel> mNotifiModelSelf = new ArrayList<NotifiModel>();

    private PullToRefreshListView mListViewFollow;
    private listAdapter mListAdapterFollow;
    private ProgressBar mProgressFollow;
    private ImageView mNoDataFollow;
    private Boolean isFetchingDataFoolow = false;
    private Boolean noMoreDataFollow = false;
    private ArrayList<NotifiModel> mNotifiModelFollow = new ArrayList<NotifiModel>();

    private Boolean state = false;
    private NotifiModel mRequestNotifiModel;

    //event tracking
    private int startSelfPage = 0;
    private int startFollowPage = 0;
    private int previousPage = 0;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.notifi_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        mApplication = (Story17Application) getActivity().getApplication();
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        LayoutInflater lf = getActivity().getLayoutInflater().from(getActivity());
        mViewSelf = lf.inflate(R.layout.notifi_self, null);
        mViewFollow = lf.inflate(R.layout.notifi_follow, null);

        mViewList = new ArrayList<View>();
        mViewList.add(mViewSelf);
        mViewList.add(mViewFollow);

        mViewPager = (ViewPager) getView().findViewById(R.id.viewpager);
        mViewPager.setAdapter(mPagerAdapter);

        SmartTabLayout mPagerTab = (SmartTabLayout) getView().findViewById(R.id.viewpagertab);
        mPagerTab.setViewPager(mViewPager);
        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                switch (position)
                {
                    case 0 :
                        //event tracking
                        startSelfPage = Singleton.getCurrentTimestamp();
                        try{
                            LogEventUtil.EnterSelfFollowPage(getActivity(),mApplication);
                            LogEventUtil.LeaveFollowPage(getActivity(),mApplication,startSelfPage-startFollowPage,mNotifiModelSelf.size());
                        }catch (Exception x)
                        {

                        }
                        mTab1.setTextColor(getResources().getColor(R.color.main_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));

                        previousPage = position;
                        break;

                    case 1 :
                        //event tracking
                        startFollowPage = Singleton.getCurrentTimestamp();
                        try{
                            LogEventUtil.EnterFollowPage(getActivity(),mApplication);
                            LogEventUtil.LeaveSelfFollowPage(getActivity(),mApplication,startFollowPage-startSelfPage,0,mNotifiModelFollow.size());
                        }catch (Exception X)
                        {

                        }

                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.main_color));

                        previousPage = position;
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        mTab1 = (TextView) getView().findViewById(R.id.tab1);
        mTab1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mViewPager.setCurrentItem(0);
            }
        });
        mTab2 = (TextView) getView().findViewById(R.id.tab2);
        mTab2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mViewPager.setCurrentItem(1);
            }
        });

        ApiManager.getFollowRequests(getActivity(), Integer.MAX_VALUE, 1, new ApiManager.GetFollowRequestsCallback()
        {
            @Override
            public void onResult(boolean success, ArrayList<UserModel> followRequestModel)
            {
                if (success && followRequestModel != null)
                {
                    if (followRequestModel.size() != 0)
                    {
                        mRequestNotifiModel = new NotifiModel();
                        mRequestNotifiModel.setType("follow_request");
                        mRequestNotifiModel.setMessage(followRequestModel.get(0).getPicture());
                    }
                    else mRequestNotifiModel = null;
                }
            }
        });

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        PoOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        PeopleOptions = new DisplayImageOptions.Builder()
        .displayer(new RoundedBitmapDisplayer(90))
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        mApplication.updateUserBadgeFirst(getActivity());//,((MenuActivity) getActivity()).mBadge
    }

    private void initTitleBar()
    {
        ((RelativeLayout) getView().findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) getView().findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.menu_notifi));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) getView().findViewById(R.id.img_left);
        img.setVisibility(View.VISIBLE);
        img.setImageResource(R.drawable.btn_addfriend_selector);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),AddFriendActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if(!state && !((MenuActivity)getActivity()).mOpen)
        {
            mNotifiModelSelf.clear();
            mNotifiModelFollow.clear();
        }

        MobclickAgent.onPageEnd("NotifiFragment");
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        //event tracking
        if(previousPage == 0)
        {
            try {
                LogEventUtil.LeaveSelfFollowPage(getActivity(), mApplication, Singleton.getCurrentTimestamp() - startSelfPage, 0, mNotifiModelFollow.size());
            }catch (Exception X)
            {

            }
        }
        else
        {
            try {
                LogEventUtil.LeaveFollowPage(getActivity(),mApplication,Singleton.getCurrentTimestamp()-startFollowPage,mNotifiModelSelf.size());
            }catch (Exception X)
            {

            }

        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        state = false;
        if(mPagerAdapter!=null && mViewList!=null)
        {
            if(mViewList.size()!=0) mPagerAdapter.notifyDataSetChanged();
        }

        ((MenuActivity)getActivity()).mOpen = false;

        MobclickAgent.onPageStart("NotifiFragment");
    }

    private PagerAdapter mPagerAdapter = new PagerAdapter()
    {
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            return mViewList.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mViewList.get(position));
        }

        @Override
        public int getItemPosition(Object object)
        {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            if(position==0)
            {
                container.addView(mViewList.get(position));

                //event tracking
                startSelfPage = Singleton.getCurrentTimestamp();
                try{
                    LogEventUtil.EnterSelfFollowPage(getActivity(),mApplication);
                }catch (Exception x)
                {

                }

                View view = mViewList.get(position);

                mNoDataSelf = (ImageView) view.findViewById(R.id.nodata);
                mProgressSelf = (ProgressBar) view.findViewById(R.id.progress);
                mListViewSelf = (PullToRefreshListView) view.findViewById(R.id.list);

                mListViewSelf.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> refreshView)
                    {
                        ApiManager.getFollowRequests(getActivity(), Integer.MAX_VALUE, 1, new ApiManager.GetFollowRequestsCallback()
                        {
                            @Override
                            public void onResult(boolean success, ArrayList<UserModel> followRequestModel)
                            {
                                if (success && followRequestModel != null)
                                {
                                    if (followRequestModel.size() != 0)
                                    {
                                        if(mRequestNotifiModel!=null) mRequestNotifiModel = null;
                                        mRequestNotifiModel = new NotifiModel();
                                        mRequestNotifiModel.setType("follow_request");
                                        mRequestNotifiModel.setMessage(followRequestModel.get(0).getPicture());
                                    }
                                    else mRequestNotifiModel = null;
                                }
                            }
                        });

                        ApiManager.getNotif(getActivity(), Integer.MAX_VALUE, 15, new ApiManager.GetNotifCallback()
                        {
                            @Override
                            public void onResult(boolean success, String message,ArrayList<NotifiModel> notifiModel)
                            {
                                mListViewSelf.onRefreshComplete();

                                if (success && notifiModel != null)
                                {
                                    if(notifiModel.size()!=0)
                                    {
                                        mNoDataSelf.setVisibility(View.GONE);
                                        mNotifiModelSelf.clear();
                                        mNotifiModelSelf.addAll(notifiModel);

                                        if(mRequestNotifiModel!=null)
                                        {
                                            mNotifiModelSelf.add(0,mRequestNotifiModel);
                                        }

                                        if(mListAdapterSelf!=null) mListAdapterSelf = null;
                                        mListAdapterSelf = new SelfLstAdapter();
                                        mListViewSelf.setAdapter(mListAdapterSelf);

                                        isFetchingDataSelf = false;
                                        noMoreDataSelf = false;

                                        if (mNotifiModelSelf.size() < 15) {
                                            noMoreDataSelf = true;
                                        }
                                    }
                                    else
                                    {
                                        mNoDataSelf.setVisibility(View.VISIBLE);
                                    }
                                }
                                else
                                {
                                    mNoDataSelf.setVisibility(View.VISIBLE);
                                    try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                });

                mListViewSelf.setOnScrollListener(new AbsListView.OnScrollListener()
                {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState)
                    {
                        if (scrollState == SCROLL_STATE_FLING) {
                            ImageLoader.getInstance().pause();
                        }

                        if (scrollState == SCROLL_STATE_IDLE) {
                            ImageLoader.getInstance().resume();
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });

                if(mNotifiModelSelf.size()==0)
                {
                    mProgressSelf.setVisibility(View.VISIBLE);
                    ApiManager.getNotif(getActivity(), Integer.MAX_VALUE, 15, new ApiManager.GetNotifCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message,ArrayList<NotifiModel> notifiModel)
                        {
                            mProgressSelf.setVisibility(View.GONE);

                            if (success && notifiModel != null)
                            {
                                if(notifiModel.size()!=0)
                                {
                                    mNoDataSelf.setVisibility(View.GONE);
                                    mNotifiModelSelf.clear();
                                    mNotifiModelSelf.addAll(notifiModel);

                                    if(mRequestNotifiModel!=null)
                                    {
                                        mNotifiModelSelf.add(0, mRequestNotifiModel);
                                    }

                                    mListAdapterSelf = new SelfLstAdapter();
                                    mListViewSelf.setAdapter(mListAdapterSelf);

                                    if (mNotifiModelSelf.size() < 15) {
                                        noMoreDataSelf = true;
                                    }
                                }
                                else
                                {
                                    mNoDataSelf.setVisibility(View.VISIBLE);
                                }
                            }
                            else
                            {
                                try
                                {
                                    mNoDataSelf.setVisibility(View.VISIBLE);
                                    try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                                catch(Exception e)
                                {

                                }
                            }
                        }
                    });
                }
                else mProgressSelf.setVisibility(View.GONE);

                return mViewList.get(position);
            }
            else
            {
                container.addView(mViewList.get(position));

                View view = mViewList.get(position);

                mNoDataFollow = (ImageView) view.findViewById(R.id.nodata);
                mProgressFollow = (ProgressBar) view.findViewById(R.id.progress);
                mListViewFollow = (PullToRefreshListView) view.findViewById(R.id.list);

                mListViewFollow.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> refreshView)
                    {
                        ApiManager.getFriendNotif(getActivity(), Integer.MAX_VALUE, 15, new ApiManager.GetFriendNotifCallback() {
                            @Override
                            public void onResult(boolean success, String message, ArrayList<NotifiModel> notifiModel) {
                                mListViewFollow.onRefreshComplete();

                                if (success && notifiModel != null) {
                                    if (notifiModel.size() != 0) {
                                        mNoDataFollow.setVisibility(View.GONE);
                                        mNotifiModelFollow.clear();
                                        mNotifiModelFollow.addAll(notifiModel);

                                        if (mListAdapterFollow != null) mListAdapterFollow = null;
                                        mListAdapterFollow = new listAdapter();
                                        mListViewFollow.setAdapter(mListAdapterFollow);

                                        isFetchingDataFoolow = false;
                                        noMoreDataFollow = false;

                                        if (mNotifiModelFollow.size() < 15) {
                                            noMoreDataFollow = true;
                                        }
                                    } else {
                                        mNoDataFollow.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    mNoDataFollow.setVisibility(View.VISIBLE);
                                    try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                });

                mListViewFollow.setOnScrollListener(new AbsListView.OnScrollListener()
                {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState)
                    {
                        if (scrollState == SCROLL_STATE_FLING) {
                            ImageLoader.getInstance().pause();
                        }

                        if (scrollState == SCROLL_STATE_IDLE) {
                            ImageLoader.getInstance().resume();
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });

                if(mNotifiModelFollow.size()==0)
                {
                    mProgressFollow.setVisibility(View.VISIBLE);
                    ApiManager.getFriendNotif(getActivity(), Integer.MAX_VALUE, 15, new ApiManager.GetFriendNotifCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<NotifiModel> notifiModel) {
                            mProgressFollow.setVisibility(View.GONE);

                            if (success && notifiModel != null) {
                                if (notifiModel.size() != 0) {
                                    mNoDataFollow.setVisibility(View.GONE);
                                    mNotifiModelFollow.clear();
                                    mNotifiModelFollow.addAll(notifiModel);
                                    mListAdapterFollow = new listAdapter();
                                    mListViewFollow.setAdapter(mListAdapterFollow);

                                    if (mNotifiModelFollow.size() < 15) {
                                        noMoreDataFollow = true;
                                    }
                                } else {
                                    mNoDataFollow.setVisibility(View.VISIBLE);
                                }
                            } else {
                                try {
                                    mNoDataFollow.setVisibility(View.VISIBLE);
                                    try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }
                    });
                }
                else mProgressFollow.setVisibility(View.GONE);

                return mViewList.get(position);
            }
        }
    };

    private class SelfLstAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mNotifiModelSelf.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderSelf holder = new ViewHolderSelf();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.notifi_row, null);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.follow = (ImageView) convertView.findViewById(R.id.follow);
                holder.row_layout = (LinearLayout) convertView.findViewById(R.id.row_layout);
//                holder.news = (TextView) convertView.findViewById(R.id.news);
                holder.line = (View) convertView.findViewById(R.id.line);
                holder.circle = (ImageView) convertView.findViewById(R.id.circle);

                holder.doing = (SocialTextView) convertView.findViewById(R.id.doing);
                holder.badge_layout = (RelativeLayout) convertView.findViewById(R.id.badge_layout);
                holder.badge = (TextView) convertView.findViewById(R.id.badge);

                convertView.setTag(holder);
            }
            else holder = (ViewHolderSelf) convertView.getTag();

            //[Zack]resolved IndexOutOfBoundsException
            if(mNotifiModelSelf != null && mNotifiModelSelf.size() < position){
                return convertView;
            }


            if(mNotifiModelSelf!=null && mNotifiModelSelf.size()!=0 && mNotifiModelSelf.get(position).getType().compareTo("follow_request")==0)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelSelf.get(position).getMessage()), holder.pic, PeopleOptions);
                holder.doing.setText(getString(R.string.follow_request));
                holder.day.setText(getString(R.string.accept_skip));
                holder.follow.setImageResource(R.drawable.nav_arrow_next_down);
                holder.img.setVisibility(View.GONE);
                holder.follow.setVisibility(View.VISIBLE);
                final RelativeLayout mBag = holder.badge_layout;
                holder.row_layout.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mBag.setVisibility(View.INVISIBLE);
                        state = true;
                        Intent intent = new Intent();
                        intent.setClass(getActivity(),FriendRequestActivity.class);
                        startActivity(intent);
                    }
                });

                holder.pic.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mBag.setVisibility(View.INVISIBLE);
                        state = true;
                        Intent intent = new Intent();
                        intent.setClass(getActivity(),FriendRequestActivity.class);
                        startActivity(intent);
                    }
                });


                holder.line.setVisibility(View.GONE);
//                holder.news.setVisibility(View.VISIBLE);
                holder.row_layout.setBackgroundColor(Color.parseColor("#efefef"));
                holder.circle.setVisibility(View.GONE);
                if(mApplication.mRequestBadge!=0)
                {
                    holder.badge_layout.setVisibility(View.VISIBLE);
                    holder.badge.setText(String.valueOf(mApplication.mRequestBadge));
                }
                else holder.badge_layout.setVisibility(View.INVISIBLE);
            }
            else if(mNotifiModelSelf!=null && mNotifiModelSelf.size()!=0)
            {
                holder.badge_layout.setVisibility(View.INVISIBLE);
                holder.circle.setVisibility(View.VISIBLE);
                holder.row_layout.setBackgroundColor(getResources().getColor(R.color.white));
//                holder.news.setVisibility(View.GONE);

                holder.line.setVisibility(View.VISIBLE);
                holder.row_layout.setOnClickListener(null);
                holder.img.setVisibility(View.GONE);
                holder.follow.setVisibility(View.GONE);

                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelSelf.get(position).getUser().getPicture()), holder.pic, SelfOptions);
                holder.pic.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        if(mNotifiModelSelf!=null && mNotifiModelSelf.size()!=0)
                        {
                            if (mNotifiModelSelf.get(position).getUser().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0 && mNotifiModelSelf.size()!=0) {
                                state = true;
                                Intent intent = new Intent();
                                intent.setClass(getActivity(), HomeUserActivity.class);
                                intent.putExtra("title", mNotifiModelSelf.get(position).getUser().getName());
                                intent.putExtra("picture", mNotifiModelSelf.get(position).getUser().getPicture());
                                intent.putExtra("isfollowing", mNotifiModelSelf.get(position).getUser().getIsFollowing());
                                intent.putExtra("post", mNotifiModelSelf.get(position).getUser().getPostCount());
                                intent.putExtra("follow", mNotifiModelSelf.get(position).getUser().getFollowerCount());
                                intent.putExtra("following", mNotifiModelSelf.get(position).getUser().getFollowingCount());
                                intent.putExtra("open", mNotifiModelSelf.get(position).getUser().getOpenID());
                                intent.putExtra("bio", mNotifiModelSelf.get(position).getUser().getBio());
                                intent.putExtra("targetUserID", mNotifiModelSelf.get(position).getUser().getUserID());
                                intent.putExtra("web", mNotifiModelSelf.get(position).getUser().getWebsite());
                                startActivity(intent);
                            }
                        }
                    }
                });
                holder.day.setText(Singleton.getElapsedTimeString(mNotifiModelSelf.get(position).getTimestamp()));
                holder.img.setVisibility(View.GONE);
                holder.follow.setVisibility(View.GONE);

                if(mNotifiModelSelf.get(position).getType().compareTo("follow")==0)
                {
                    holder.doing.setText("@" + mNotifiModelSelf.get(position).getUser().getOpenID() + getString(R.string.notifi_start_me));
                }
                else if(mNotifiModelSelf.get(position).getType().compareTo("postLike")==0)
                {
                    holder.doing.setText("@" + mNotifiModelSelf.get(position).getUser().getOpenID() + getString(R.string.notifi_photo_like_me));

                    if(mNotifiModelSelf.get(position).getPost()!=null)
                    {
                        holder.img.setVisibility(View.VISIBLE);
                        if(mNotifiModelSelf!=null && mNotifiModelSelf.size()!=0)
                        {
                            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelSelf.get(position).getPost().getPicture()), holder.img, PoOptions);
                            holder.img.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View view)
                                {
                                    try {
                                        state = true;
                                        Intent intent = new Intent();
                                        intent.setClass(getActivity(), PhotoActivity.class);
                                        intent.putExtra("photo", mNotifiModelSelf.get(position).getPost().getPicture());
                                        intent.putExtra("open", mNotifiModelSelf.get(position).getPost().getUser().getOpenID());
                                        intent.putExtra("picture", mNotifiModelSelf.get(position).getPost().getUser().getPicture());
                                        intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModelSelf.get(position).getPost().getTimestamp()));
                                        intent.putExtra("view", mNotifiModelSelf.get(position).getPost().getViewCount());
                                        intent.putExtra("money", mNotifiModelSelf.get(position).getPost().getTotalRevenue());
                                        intent.putExtra("dio", mNotifiModelSelf.get(position).getPost().getCaption());
                                        intent.putExtra("likecount", mNotifiModelSelf.get(position).getPost().getLikeCount());
                                        intent.putExtra("commentcount", mNotifiModelSelf.get(position).getPost().getCommentCount());
                                        intent.putExtra("likeed", mNotifiModelSelf.get(position).getPost().getIsLiked());

                                        intent.putExtra("postid", mNotifiModelSelf.get(position).getPostID());
                                        intent.putExtra("userid", mNotifiModelSelf.get(position).getPost().getUser().getUserID());

                                        intent.putExtra("name", mNotifiModelSelf.get(position).getPost().getUser().getName());
                                        intent.putExtra("isFollowing", mNotifiModelSelf.get(position).getPost().getUser().getIsFollowing());
                                        intent.putExtra("postCount", mNotifiModelSelf.get(position).getPost().getUser().getPostCount());
                                        intent.putExtra("followerCount", mNotifiModelSelf.get(position).getPost().getUser().getFollowerCount());
                                        intent.putExtra("followingCount", mNotifiModelSelf.get(position).getPost().getUser().getFollowingCount());
                                        intent.putExtra("goto", false);
                                        intent.putExtra("type", mNotifiModelSelf.get(position).getPost().getType());
                                        intent.putExtra("video", mNotifiModelSelf.get(position).getPost().getVideo());
                                        startActivity(intent);
                                    } catch (Exception e) {

                                    }
                                }
                            });
                        }
                    }
                }
                else if(mNotifiModelSelf.get(position).getType().compareTo("newContactsFriendJoin")==0)
                {
                    holder.doing.setText(getString(R.string.notifi_contast_me) + "@" + mNotifiModelSelf.get(position).getUser().getOpenID() + getString(R.string.notifi_17));

                    holder.follow.setVisibility(View.VISIBLE);
                    if(mNotifiModelSelf.get(position).getUser().getIsFollowing()==0)
                    {
                        if(mNotifiModelSelf.get(position).getUser().getFollowRequestTime()!=0)
                        {
                            holder.follow.setImageResource(R.drawable.follow_wait);
                        }
                        else
                        {
                            holder.follow.setImageResource(R.drawable.follow);
                        }
                    }
                    else
                    {
                        holder.follow.setImageResource(R.drawable.follow_down);
                    }

                    final ImageView img = holder.follow;

                    holder.follow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(mNotifiModelSelf.get(position).getUser().getIsFollowing()==1)
                            {
                                img.setImageResource(R.drawable.follow);
                                mNotifiModelSelf.get(position).getUser().setIsFollowing(0);

                                LogEventUtil.UnfollowUser(getActivity(), mApplication, mNotifiModelFollow.get(position).getUser().getUserID());

                                ApiManager.unfollowUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModelSelf.get(position).getUser().getUserID(), new ApiManager.UnfollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {

                                        }
                                    }
                                });
                            }
                            else
                            {
                                if(mNotifiModelSelf.get(position).getUser().getFollowRequestTime()!=0)
                                {
                                    img.setImageResource(R.drawable.follow);
                                    mNotifiModelSelf.get(position).getUser().setIsFollowing(0);
                                    mNotifiModelSelf.get(position).getUser().setFollowRequestTime(0);
                                    ApiManager.cancelFollowRequests(getActivity(), mNotifiModelSelf.get(position).getUser().getUserID(), new ApiManager.RequestCallback() {
                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                                    {
                                        try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.follow_count_size));
                                            Toast.makeText(getActivity(), getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                        return ;
                                    }

                                    if(mNotifiModelSelf.get(position).getUser().getPrivacyMode().compareTo("private")==0)
                                    {
                                        img.setImageResource(R.drawable.follow_wait);
                                        mNotifiModelSelf.get(position).getUser().setIsFollowing(0);
                                        mNotifiModelSelf.get(position).getUser().setFollowRequestTime(Singleton.getCurrentTimestamp());
                                        ApiManager.sendFollowRequest(getActivity(), mNotifiModelSelf.get(position).getUser().getUserID(), new ApiManager.RequestCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success)
                                            {
                                                if (!success)
                                                {
                                                    img.setImageResource(R.drawable.follow);
                                                    mNotifiModelSelf.get(position).getUser().setIsFollowing(0);
                                                    mNotifiModelSelf.get(position).getUser().setFollowRequestTime(0);
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        img.setImageResource(R.drawable.follow_down);
                                        mNotifiModelSelf.get(position).getUser().setIsFollowing(1);
                                        try{
                                            LogEventUtil.FollowUser(getActivity(), mApplication, mNotifiModelFollow.get(position).getUser().getUserID());}
                                        catch (Exception x)
                                        {

                                        }

                                        ApiManager.followUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModelSelf.get(position).getUser().getUserID(), new ApiManager.FollowUserActionCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
                else if(mNotifiModelSelf.get(position).getType().compareTo("newFBFriendJoin")==0)
                {
                    holder.doing.setText(getString(R.string.notifi_facebook_me) + "@" + mNotifiModelSelf.get(position).getUser().getOpenID() + getString(R.string.notifi_17));

                    holder.follow.setVisibility(View.VISIBLE);
                    if(mNotifiModelSelf.get(position).getUser().getIsFollowing()==0)
                    {
                        if(mNotifiModelSelf.get(position).getUser().getFollowRequestTime()!=0)
                        {
                            holder.follow.setImageResource(R.drawable.follow_wait);
                        }
                        else
                        {
                            holder.follow.setImageResource(R.drawable.follow);
                        }
                    }
                    else
                    {
                        holder.follow.setImageResource(R.drawable.follow_down);
                    }

                    final ImageView img = holder.follow;

                    holder.follow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (mNotifiModelSelf.get(position).getUser().getIsFollowing() == 1) {
                                img.setImageResource(R.drawable.follow);
                                mNotifiModelSelf.get(position).getUser().setIsFollowing(0);

                                LogEventUtil.UnfollowUser(getActivity(), mApplication, mNotifiModelFollow.get(position).getUser().getUserID());
                                ApiManager.unfollowUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModelSelf.get(position).getUser().getUserID(), new ApiManager.UnfollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {

                                        }
                                    }
                                });
                            } else {
                                if (mNotifiModelSelf.get(position).getUser().getFollowRequestTime() != 0) {
                                    img.setImageResource(R.drawable.follow);
                                    mNotifiModelSelf.get(position).getUser().setIsFollowing(0);
                                    mNotifiModelSelf.get(position).getUser().setFollowRequestTime(0);
                                    ApiManager.cancelFollowRequests(getActivity(), mNotifiModelSelf.get(position).getUser().getUserID(), new ApiManager.RequestCallback() {
                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {

                                            }
                                        }
                                    });
                                } else {
                                    if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                                    {
                                        try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.follow_count_size));
                                            Toast.makeText(getActivity(), getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                        return ;
                                    }

                                    if (mNotifiModelSelf.get(position).getUser().getPrivacyMode().compareTo("private") == 0) {
                                        img.setImageResource(R.drawable.follow_wait);
                                        mNotifiModelSelf.get(position).getUser().setIsFollowing(0);
                                        mNotifiModelSelf.get(position).getUser().setFollowRequestTime(Singleton.getCurrentTimestamp());
                                        ApiManager.sendFollowRequest(getActivity(), mNotifiModelSelf.get(position).getUser().getUserID(), new ApiManager.RequestCallback() {
                                            @Override
                                            public void onResult(boolean success) {
                                                if (!success) {
                                                    img.setImageResource(R.drawable.follow);
                                                    mNotifiModelSelf.get(position).getUser().setIsFollowing(0);
                                                    mNotifiModelSelf.get(position).getUser().setFollowRequestTime(0);
                                                }
                                            }
                                        });
                                    } else {
                                        img.setImageResource(R.drawable.follow_down);
                                        mNotifiModelSelf.get(position).getUser().setIsFollowing(1);
                                        try{
                                            LogEventUtil.FollowUser(getActivity(), mApplication, mNotifiModelFollow.get(position).getUser().getUserID());}
                                        catch (Exception x)
                                        {

                                        }

                                        ApiManager.followUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModelSelf.get(position).getUser().getUserID(), new ApiManager.FollowUserActionCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {

                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
                else if(mNotifiModelSelf.get(position).getType().compareTo("postComment")==0)
                {
                    holder.doing.setText("@" + mNotifiModelSelf.get(position).getUser().getOpenID() + getString(R.string.notifi_comment_me));

                    if(mNotifiModelSelf.get(position).getPost()!=null)
                    {
                        holder.img.setVisibility(View.VISIBLE);
                        if(mNotifiModelSelf!=null && mNotifiModelSelf.size()!=0)
                        {
                            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelSelf.get(position).getPost().getPicture()), holder.img, PoOptions);
                            holder.img.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View view)
                                {
                                    state = true;
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), PhotoActivity.class);
                                    intent.putExtra("photo", mNotifiModelSelf.get(position).getPost().getPicture());
                                    intent.putExtra("open", mNotifiModelSelf.get(position).getPost().getUser().getOpenID());
                                    intent.putExtra("picture", mNotifiModelSelf.get(position).getPost().getUser().getPicture());
                                    intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModelSelf.get(position).getPost().getTimestamp()));
                                    intent.putExtra("view", mNotifiModelSelf.get(position).getPost().getViewCount());
                                    intent.putExtra("money", mNotifiModelSelf.get(position).getPost().getTotalRevenue());
                                    intent.putExtra("dio", mNotifiModelSelf.get(position).getPost().getCaption());
                                    intent.putExtra("likecount", mNotifiModelSelf.get(position).getPost().getLikeCount());
                                    intent.putExtra("commentcount", mNotifiModelSelf.get(position).getPost().getCommentCount());
                                    intent.putExtra("likeed", mNotifiModelSelf.get(position).getPost().getIsLiked());

                                    intent.putExtra("postid", mNotifiModelSelf.get(position).getPostID());
                                    intent.putExtra("userid", mNotifiModelSelf.get(position).getPost().getUser().getUserID());

                                    intent.putExtra("name", mNotifiModelSelf.get(position).getPost().getUser().getName());
                                    intent.putExtra("isFollowing", mNotifiModelSelf.get(position).getPost().getUser().getIsFollowing());
                                    intent.putExtra("postCount", mNotifiModelSelf.get(position).getPost().getUser().getPostCount());
                                    intent.putExtra("followerCount", mNotifiModelSelf.get(position).getPost().getUser().getFollowerCount());
                                    intent.putExtra("followingCount", mNotifiModelSelf.get(position).getPost().getUser().getFollowingCount());
                                    intent.putExtra("goto", false);
                                    intent.putExtra("type", mNotifiModelSelf.get(position).getPost().getType());
                                    intent.putExtra("video", mNotifiModelSelf.get(position).getPost().getVideo());
                                    startActivity(intent);
                                }
                            });
                        }
                    }

                }
                else if(mNotifiModelSelf.get(position).getType().compareTo("commentTag")==0)
                {
                    holder.doing.setText(getString(R.string.notifi_friend_me) + "@" + mNotifiModelSelf.get(position).getUser().getOpenID() + getString(R.string.notifi_tag_me));

                    if(mNotifiModelSelf.get(position).getPost()!=null)
                    {
                        holder.img.setVisibility(View.VISIBLE);
                        if(mNotifiModelSelf!=null && mNotifiModelSelf.size()!=0)
                        {
                            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelSelf.get(position).getPost().getPicture()), holder.img, PoOptions);
                            holder.img.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View view)
                                {
                                    state = true;
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), PhotoActivity.class);
                                    intent.putExtra("photo", mNotifiModelSelf.get(position).getPost().getPicture());
                                    intent.putExtra("open", mNotifiModelSelf.get(position).getPost().getUser().getOpenID());
                                    intent.putExtra("picture", mNotifiModelSelf.get(position).getPost().getUser().getPicture());
                                    intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModelSelf.get(position).getPost().getTimestamp()));
                                    intent.putExtra("view", mNotifiModelSelf.get(position).getPost().getViewCount());
                                    intent.putExtra("money", mNotifiModelSelf.get(position).getPost().getTotalRevenue());
                                    intent.putExtra("dio", mNotifiModelSelf.get(position).getPost().getCaption());
                                    intent.putExtra("likecount", mNotifiModelSelf.get(position).getPost().getLikeCount());
                                    intent.putExtra("commentcount", mNotifiModelSelf.get(position).getPost().getCommentCount());
                                    intent.putExtra("likeed", mNotifiModelSelf.get(position).getPost().getIsLiked());

                                    intent.putExtra("postid", mNotifiModelSelf.get(position).getPostID());
                                    intent.putExtra("userid", mNotifiModelSelf.get(position).getPost().getUser().getUserID());

                                    intent.putExtra("name", mNotifiModelSelf.get(position).getPost().getUser().getName());
                                    intent.putExtra("isFollowing", mNotifiModelSelf.get(position).getPost().getUser().getIsFollowing());
                                    intent.putExtra("postCount", mNotifiModelSelf.get(position).getPost().getUser().getPostCount());
                                    intent.putExtra("followerCount", mNotifiModelSelf.get(position).getPost().getUser().getFollowerCount());
                                    intent.putExtra("followingCount", mNotifiModelSelf.get(position).getPost().getUser().getFollowingCount());
                                    intent.putExtra("goto", false);
                                    intent.putExtra("type", mNotifiModelSelf.get(position).getPost().getType());
                                    intent.putExtra("video", mNotifiModelSelf.get(position).getPost().getVideo());
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                }
                else
                {
                    holder.doing.setText("@" + mNotifiModelSelf.get(position).getUser().getOpenID() + getString(R.string.notifi_follow_me));
                }

                holder.doing.linkify(new SocialActionHandler()
                {
                    @Override
                    public void handleHashtag(String hashtag)
                    {

                    }

                    @Override
                    public void handleMention(String mention)
                    {
                        if(mNotifiModelSelf!=null && mNotifiModelSelf.size()!=0)
                        {
                            if(mNotifiModelSelf.get(position).getUser().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0 && mNotifiModelSelf.get(position).getUser().getOpenID().compareTo(mention) == 0)
                            {
                                state = true;
                                Intent intent = new Intent();
                                intent.setClass(getActivity(), HomeUserActivity.class);
                                intent.putExtra("title", mNotifiModelSelf.get(position).getUser().getName());
                                intent.putExtra("picture", mNotifiModelSelf.get(position).getUser().getPicture());
                                intent.putExtra("isfollowing", mNotifiModelSelf.get(position).getUser().getIsFollowing());
                                intent.putExtra("post", mNotifiModelSelf.get(position).getUser().getPostCount());
                                intent.putExtra("follow", mNotifiModelSelf.get(position).getUser().getFollowerCount());
                                intent.putExtra("following", mNotifiModelSelf.get(position).getUser().getFollowingCount());
                                intent.putExtra("open", mNotifiModelSelf.get(position).getUser().getOpenID());
                                intent.putExtra("bio", mNotifiModelSelf.get(position).getUser().getBio());
                                intent.putExtra("targetUserID", mNotifiModelSelf.get(position).getUser().getUserID());
                                intent.putExtra("web", mNotifiModelSelf.get(position).getUser().getWebsite());
                                startActivity(intent);
                            }
                        }
                    }
                });

                removeLine(holder.doing);

                if(position>=getCount()-5)
                {
                    LoadDataSele(false);
                }
            }

            return convertView;
        }
    }

    private class ViewHolderSelf
    {
        ImageView pic;
        TextView day;
        ImageView img;
        ImageView follow;
        LinearLayout row_layout;
//        TextView news;
        View line;
        ImageView circle;

        RelativeLayout badge_layout;
        TextView badge;
        SocialTextView doing;
    }

    public void LoadDataSele(final boolean refresh)
    {
        if(isFetchingDataSelf)
        {
            return;
        }

        if(refresh)
        {
            noMoreDataSelf = false;
        }

        if(noMoreDataSelf)
        {
            return;
        }

        isFetchingDataSelf = true;

        ApiManager.getNotif(getActivity(), mNotifiModelSelf.get(mNotifiModelSelf.size() - 1).getTimestamp(), 15, new ApiManager.GetNotifCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<NotifiModel> notifiModel) {
                isFetchingDataSelf = false;

                if (success && notifiModel != null) {
                    if (notifiModel.size() != 0) {
                        mNotifiModelSelf.addAll(notifiModel);

                        if (mNotifiModelSelf.size() < 15) {
                            noMoreDataSelf = true;
                        }

                        if (mListAdapterSelf != null) {
                            mListAdapterSelf.notifyDataSetChanged();
                        }
                    }
                } else {
                    try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    private class listAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mNotifiModelFollow.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderFollow holder = new ViewHolderFollow();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.notifi_friend_row, null);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.follow = (ImageView) convertView.findViewById(R.id.follow);
                holder.doing = (SocialTextView) convertView.findViewById(R.id.doing);

                convertView.setTag(holder);
            }
            else holder = (ViewHolderFollow) convertView.getTag();

            if(mNotifiModelFollow!=null && mNotifiModelFollow.size()!=0)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelFollow.get(position).getUser().getPicture()), holder.pic, SelfOptions);
                holder.pic.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        if(mNotifiModelFollow!=null && mNotifiModelFollow.size()!=0)
                        {
                            if (mNotifiModelFollow.get(position).getUser().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0 && mNotifiModelFollow.size()!=0) {
                                state = true;
                                Intent intent = new Intent();
                                intent.setClass(getActivity(), HomeUserActivity.class);
                                intent.putExtra("title", mNotifiModelFollow.get(position).getUser().getName());
                                intent.putExtra("picture", mNotifiModelFollow.get(position).getUser().getPicture());
                                intent.putExtra("isfollowing", mNotifiModelFollow.get(position).getUser().getIsFollowing());
                                intent.putExtra("post", mNotifiModelFollow.get(position).getUser().getPostCount());
                                intent.putExtra("follow", mNotifiModelFollow.get(position).getUser().getFollowerCount());
                                intent.putExtra("following", mNotifiModelFollow.get(position).getUser().getFollowingCount());
                                intent.putExtra("open", mNotifiModelFollow.get(position).getUser().getOpenID());
                                intent.putExtra("bio", mNotifiModelFollow.get(position).getUser().getBio());
                                intent.putExtra("targetUserID", mNotifiModelFollow.get(position).getUser().getUserID());
                                intent.putExtra("web", mNotifiModelFollow.get(position).getUser().getWebsite());
                                startActivity(intent);
                            }
                        }
                    }
                });

                holder.day.setText(Singleton.getElapsedTimeString(mNotifiModelFollow.get(position).getTimestamp()));
                holder.img.setVisibility(View.GONE);
                holder.follow.setVisibility(View.GONE);

                if(mNotifiModelFollow.get(position).getType().compareTo("follow")==0)
                {
                    if(mNotifiModelFollow.get(position).getTargetUserInfo()!=null)
                    {
                        holder.doing.setText("@" + mNotifiModelFollow.get(position).getUser().getOpenID() + getString(R.string.notifi_start) + "@" + mNotifiModelFollow.get(position).getTargetUserInfo().getOpenID());

                        holder.img.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelFollow.get(position).getTargetUserInfo().getPicture()), holder.img, PoOptions);
                        holder.img.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                if(mNotifiModelFollow!=null && mNotifiModelFollow.size()!=0)
                                {
                                    state = true;
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), HomeUserActivity.class);
                                    intent.putExtra("title", mNotifiModelFollow.get(position).getTargetUserInfo().getName());
                                    intent.putExtra("picture", mNotifiModelFollow.get(position).getTargetUserInfo().getPicture());
                                    intent.putExtra("isfollowing", mNotifiModelFollow.get(position).getTargetUserInfo().getIsFollowing());
                                    intent.putExtra("post", mNotifiModelFollow.get(position).getTargetUserInfo().getPostCount());
                                    intent.putExtra("follow", mNotifiModelFollow.get(position).getTargetUserInfo().getFollowerCount());
                                    intent.putExtra("following", mNotifiModelFollow.get(position).getTargetUserInfo().getFollowingCount());
                                    intent.putExtra("open", mNotifiModelFollow.get(position).getTargetUserInfo().getOpenID());
                                    intent.putExtra("bio", mNotifiModelFollow.get(position).getTargetUserInfo().getBio());
                                    intent.putExtra("targetUserID", mNotifiModelFollow.get(position).getTargetUserInfo().getUserID());
                                    intent.putExtra("web", mNotifiModelFollow.get(position).getTargetUserInfo().getWebsite());
                                    startActivity(intent);
                                }
                            }
                        });
                    }
                }
                else if(mNotifiModelFollow.get(position).getType().compareTo("postLike")==0)
                {
                    if(mNotifiModelFollow.get(position).getPost()!=null)
                    {
                        holder.doing.setText("@" + mNotifiModelFollow.get(position).getUser().getOpenID() + getString(R.string.notifi_say) + String.format(getString(R.string.notifi_photo_like),"@" + mNotifiModelFollow.get(position).getPost().getUser().getOpenID()));

                        holder.img.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelFollow.get(position).getPost().getPicture()), holder.img, PoOptions);
                        holder.img.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                if(mNotifiModelFollow!=null && mNotifiModelFollow.size()!=0)
                                {
                                    state = true;
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), PhotoActivity.class);
                                    intent.putExtra("photo", mNotifiModelFollow.get(position).getPost().getPicture());
                                    intent.putExtra("open", mNotifiModelFollow.get(position).getPost().getUser().getOpenID());
                                    intent.putExtra("picture", mNotifiModelFollow.get(position).getPost().getUser().getPicture());
                                    intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModelFollow.get(position).getPost().getTimestamp()));
                                    intent.putExtra("view", mNotifiModelFollow.get(position).getPost().getViewCount());
                                    intent.putExtra("money", mNotifiModelFollow.get(position).getPost().getTotalRevenue());
                                    intent.putExtra("dio", mNotifiModelFollow.get(position).getPost().getCaption());
                                    intent.putExtra("likecount", mNotifiModelFollow.get(position).getPost().getLikeCount());
                                    intent.putExtra("commentcount", mNotifiModelFollow.get(position).getPost().getCommentCount());
                                    intent.putExtra("likeed", mNotifiModelFollow.get(position).getPost().getIsLiked());

                                    intent.putExtra("postid", mNotifiModelFollow.get(position).getPostID());
                                    intent.putExtra("userid", mNotifiModelFollow.get(position).getPost().getUser().getUserID());

                                    intent.putExtra("name", mNotifiModelFollow.get(position).getPost().getUser().getName());
                                    intent.putExtra("isFollowing", mNotifiModelFollow.get(position).getPost().getUser().getIsFollowing());
                                    intent.putExtra("postCount", mNotifiModelFollow.get(position).getPost().getUser().getPostCount());
                                    intent.putExtra("followerCount", mNotifiModelFollow.get(position).getPost().getUser().getFollowerCount());
                                    intent.putExtra("followingCount", mNotifiModelFollow.get(position).getPost().getUser().getFollowingCount());
                                    intent.putExtra("goto", true);
                                    intent.putExtra("type", mNotifiModelFollow.get(position).getPost().getType());
                                    intent.putExtra("video", mNotifiModelFollow.get(position).getPost().getVideo());
                                    startActivity(intent);
                                }
                            }
                        });
                    }
                }
                else if(mNotifiModelFollow.get(position).getType().compareTo("newContactsFriendJoin")==0)
                {
                    holder.doing.setText("@" + mNotifiModelFollow.get(position).getUser().getOpenID() + getString(R.string.notifi_contast) + getString(R.string.notifi_17));

                    holder.follow.setVisibility(View.VISIBLE);
                    if(mNotifiModelFollow.get(position).getUser().getIsFollowing()==0)
                    {
                        if(mNotifiModelFollow.get(position).getUser().getFollowRequestTime()!=0)
                        {
                            holder.follow.setImageResource(R.drawable.follow_wait);
                        }
                        else
                        {
                            holder.follow.setImageResource(R.drawable.follow);
                        }
                    }
                    else
                    {
                        holder.follow.setImageResource(R.drawable.follow_down);
                    }

                    final ImageView img = holder.follow;

                    holder.follow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(mNotifiModelFollow.get(position).getUser().getIsFollowing()==1)
                            {
                                img.setImageResource(R.drawable.follow);
                                mNotifiModelFollow.get(position).getUser().setIsFollowing(0);

                                try {
                                    LogEventUtil.UnfollowUser(getActivity(), mApplication, mNotifiModelFollow.get(position).getUser().getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.unfollowUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModelFollow.get(position).getUser().getUserID(), new ApiManager.UnfollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {

                                        }
                                    }
                                });
                            }
                            else
                            {
                                if(mNotifiModelFollow.get(position).getUser().getFollowRequestTime()!=0)
                                {
                                    img.setImageResource(R.drawable.follow);
                                    mNotifiModelFollow.get(position).getUser().setIsFollowing(0);
                                    mNotifiModelFollow.get(position).getUser().setFollowRequestTime(0);
                                    ApiManager.cancelFollowRequests(getActivity(), mNotifiModelFollow.get(position).getUser().getUserID(), new ApiManager.RequestCallback() {
                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                                    {
                                        try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.follow_count_size));
                                            Toast.makeText(getActivity(), getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                        return ;
                                    }

                                    if(mNotifiModelFollow.get(position).getUser().getPrivacyMode().compareTo("private")==0)
                                    {
                                        img.setImageResource(R.drawable.follow_wait);
                                        mNotifiModelFollow.get(position).getUser().setIsFollowing(0);
                                        mNotifiModelFollow.get(position).getUser().setFollowRequestTime(Singleton.getCurrentTimestamp());
                                        ApiManager.sendFollowRequest(getActivity(), mNotifiModelFollow.get(position).getUser().getUserID(), new ApiManager.RequestCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success)
                                            {
                                                if (!success)
                                                {
                                                    img.setImageResource(R.drawable.follow);
                                                    mNotifiModelFollow.get(position).getUser().setIsFollowing(0);
                                                    mNotifiModelFollow.get(position).getUser().setFollowRequestTime(0);
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        img.setImageResource(R.drawable.follow_down);
                                        mNotifiModelFollow.get(position).getUser().setIsFollowing(1);
                                        try{
                                            LogEventUtil.FollowUser(getActivity(), mApplication, mNotifiModelFollow.get(position).getUser().getUserID());}
                                        catch (Exception x)
                                        {

                                        }

                                        ApiManager.followUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModelFollow.get(position).getUser().getUserID(), new ApiManager.FollowUserActionCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
                else if(mNotifiModelFollow.get(position).getType().compareTo("newFBFriendJoin")==0)
                {
                    holder.doing.setText("@" + mNotifiModelFollow.get(position).getUser().getOpenID() + getString(R.string.notifi_facebook) + getString(R.string.notifi_17));

                    holder.follow.setVisibility(View.VISIBLE);
                    if(mNotifiModelFollow.get(position).getUser().getIsFollowing()==0)
                    {
                        if(mNotifiModelFollow.get(position).getUser().getFollowRequestTime()!=0)
                        {
                            holder.follow.setImageResource(R.drawable.follow_wait);
                        }
                        else
                        {
                            holder.follow.setImageResource(R.drawable.follow);
                        }
                    }
                    else
                    {
                        holder.follow.setImageResource(R.drawable.follow_down);
                    }

                    final ImageView img = holder.follow;

                    holder.follow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (mNotifiModelFollow.get(position).getUser().getIsFollowing() == 1) {
                                img.setImageResource(R.drawable.follow);
                                mNotifiModelFollow.get(position).getUser().setIsFollowing(0);

                                try {
                                    LogEventUtil.UnfollowUser(getActivity(), mApplication, mNotifiModelFollow.get(position).getUser().getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.unfollowUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModelFollow.get(position).getUser().getUserID(), new ApiManager.UnfollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {

                                        }
                                    }
                                });
                            } else {
                                if (mNotifiModelFollow.get(position).getUser().getFollowRequestTime() != 0) {
                                    img.setImageResource(R.drawable.follow);
                                    mNotifiModelFollow.get(position).getUser().setIsFollowing(0);
                                    mNotifiModelFollow.get(position).getUser().setFollowRequestTime(0);
                                    ApiManager.cancelFollowRequests(getActivity(), mNotifiModelFollow.get(position).getUser().getUserID(), new ApiManager.RequestCallback() {
                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {

                                            }
                                        }
                                    });
                                } else {
                                    if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                                    {
                                        try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.follow_count_size));
                                            Toast.makeText(getActivity(), getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                        return ;
                                    }

                                    if (mNotifiModelFollow.get(position).getUser().getPrivacyMode().compareTo("private") == 0) {
                                        img.setImageResource(R.drawable.follow_wait);
                                        mNotifiModelFollow.get(position).getUser().setIsFollowing(0);
                                        mNotifiModelFollow.get(position).getUser().setFollowRequestTime(Singleton.getCurrentTimestamp());
                                        ApiManager.sendFollowRequest(getActivity(), mNotifiModelFollow.get(position).getUser().getUserID(), new ApiManager.RequestCallback() {
                                            @Override
                                            public void onResult(boolean success) {
                                                if (!success) {
                                                    img.setImageResource(R.drawable.follow);
                                                    mNotifiModelFollow.get(position).getUser().setIsFollowing(0);
                                                    mNotifiModelFollow.get(position).getUser().setFollowRequestTime(0);
                                                }
                                            }
                                        });
                                    } else {
                                        img.setImageResource(R.drawable.follow_down);
                                        mNotifiModelFollow.get(position).getUser().setIsFollowing(1);
                                        try{
                                            LogEventUtil.FollowUser(getActivity(), mApplication, mNotifiModelFollow.get(position).getUser().getUserID());}
                                        catch (Exception x)
                                        {

                                        }

                                        ApiManager.followUserAction(getActivity(), Singleton.preferences.getString(Constants.USER_ID, ""), mNotifiModelFollow.get(position).getUser().getUserID(), new ApiManager.FollowUserActionCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
                else if(mNotifiModelFollow.get(position).getType().compareTo("postComment")==0)
                {
                    if(mNotifiModelFollow.get(position).getPost()!=null)
                    {
                        holder.doing.setText("@" + mNotifiModelFollow.get(position).getUser().getOpenID() + getString(R.string.notifi_comment) + String.format(getString(R.string.notifi_in_phtot),"@" + mNotifiModelFollow.get(position).getPost().getUser().getOpenID()));

                        holder.img.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelFollow.get(position).getPost().getPicture()), holder.img, PoOptions);
                        holder.img.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                if(mNotifiModelFollow!=null && mNotifiModelFollow.size()!=0)
                                {
                                    state = true;
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), PhotoActivity.class);
                                    intent.putExtra("photo", mNotifiModelFollow.get(position).getPost().getPicture());
                                    intent.putExtra("open", mNotifiModelFollow.get(position).getPost().getUser().getOpenID());
                                    intent.putExtra("picture", mNotifiModelFollow.get(position).getPost().getUser().getPicture());
                                    intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModelFollow.get(position).getPost().getTimestamp()));
                                    intent.putExtra("view", mNotifiModelFollow.get(position).getPost().getViewCount());
                                    intent.putExtra("money", mNotifiModelFollow.get(position).getPost().getTotalRevenue());
                                    intent.putExtra("dio", mNotifiModelFollow.get(position).getPost().getCaption());
                                    intent.putExtra("likecount", mNotifiModelFollow.get(position).getPost().getLikeCount());
                                    intent.putExtra("commentcount", mNotifiModelFollow.get(position).getPost().getCommentCount());
                                    intent.putExtra("likeed", mNotifiModelFollow.get(position).getPost().getIsLiked());

                                    intent.putExtra("postid", mNotifiModelFollow.get(position).getPostID());
                                    intent.putExtra("userid", mNotifiModelFollow.get(position).getPost().getUser().getUserID());

                                    intent.putExtra("name", mNotifiModelFollow.get(position).getPost().getUser().getName());
                                    intent.putExtra("isFollowing", mNotifiModelFollow.get(position).getPost().getUser().getIsFollowing());
                                    intent.putExtra("postCount", mNotifiModelFollow.get(position).getPost().getUser().getPostCount());
                                    intent.putExtra("followerCount", mNotifiModelFollow.get(position).getPost().getUser().getFollowerCount());
                                    intent.putExtra("followingCount", mNotifiModelFollow.get(position).getPost().getUser().getFollowingCount());
                                    intent.putExtra("goto", true);
                                    intent.putExtra("type", mNotifiModelFollow.get(position).getPost().getType());
                                    intent.putExtra("video", mNotifiModelFollow.get(position).getPost().getVideo());
                                    startActivity(intent);
                                }
                            }
                        });
                    }

                }
                else if(mNotifiModelFollow.get(position).getType().compareTo("commentTag")==0)
                {
                    if(mNotifiModelFollow.get(position).getPost()!=null)
                    {
                        holder.doing.setText("@" + mNotifiModelFollow.get(position).getUser().getOpenID() + getString(R.string.notifi_tag) + "@" + mNotifiModelFollow.get(position).getPost().getUser().getOpenID());

                        holder.img.setVisibility(View.VISIBLE);
                        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mNotifiModelFollow.get(position).getPost().getPicture()), holder.img, PoOptions);
                        holder.img.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                if(mNotifiModelFollow!=null && mNotifiModelFollow.size()!=0)
                                {
                                    state = true;
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), PhotoActivity.class);
                                    intent.putExtra("photo", mNotifiModelFollow.get(position).getPost().getPicture());
                                    intent.putExtra("open", mNotifiModelFollow.get(position).getPost().getUser().getOpenID());
                                    intent.putExtra("picture", mNotifiModelFollow.get(position).getPost().getUser().getPicture());
                                    intent.putExtra("day", Singleton.getElapsedTimeString(mNotifiModelFollow.get(position).getPost().getTimestamp()));
                                    intent.putExtra("view", mNotifiModelFollow.get(position).getPost().getViewCount());
                                    intent.putExtra("money", mNotifiModelFollow.get(position).getPost().getTotalRevenue());
                                    intent.putExtra("dio", mNotifiModelFollow.get(position).getPost().getCaption());
                                    intent.putExtra("likecount", mNotifiModelFollow.get(position).getPost().getLikeCount());
                                    intent.putExtra("commentcount", mNotifiModelFollow.get(position).getPost().getCommentCount());
                                    intent.putExtra("likeed", mNotifiModelFollow.get(position).getPost().getIsLiked());

                                    intent.putExtra("postid", mNotifiModelFollow.get(position).getPostID());
                                    intent.putExtra("userid", mNotifiModelFollow.get(position).getPost().getUser().getUserID());

                                    intent.putExtra("name", mNotifiModelFollow.get(position).getPost().getUser().getName());
                                    intent.putExtra("isFollowing", mNotifiModelFollow.get(position).getPost().getUser().getIsFollowing());
                                    intent.putExtra("postCount", mNotifiModelFollow.get(position).getPost().getUser().getPostCount());
                                    intent.putExtra("followerCount", mNotifiModelFollow.get(position).getPost().getUser().getFollowerCount());
                                    intent.putExtra("followingCount", mNotifiModelFollow.get(position).getPost().getUser().getFollowingCount());
                                    intent.putExtra("goto", true);
                                    intent.putExtra("type", mNotifiModelFollow.get(position).getPost().getType());
                                    intent.putExtra("video", mNotifiModelFollow.get(position).getPost().getVideo());
                                    startActivity(intent);
                                }
                            }
                        });
                    }
                }

                holder.doing.linkify(new SocialActionHandler()
                {
                    @Override
                    public void handleHashtag(String hashtag)
                    {

                    }

                    @Override
                    public void handleMention(String mention)
                    {
                        if(mNotifiModelFollow!=null && mNotifiModelFollow.size()!=0)
                        {
                            if (mNotifiModelFollow.get(position).getUser().getOpenID().compareTo(mention) == 0)
                            {
                                if (mNotifiModelFollow.get(position).getUser().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                                {
                                    state = true;
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), HomeUserActivity.class);
                                    intent.putExtra("title", mNotifiModelFollow.get(position).getUser().getName());
                                    intent.putExtra("picture", mNotifiModelFollow.get(position).getUser().getPicture());
                                    intent.putExtra("isfollowing", mNotifiModelFollow.get(position).getUser().getIsFollowing());
                                    intent.putExtra("post", mNotifiModelFollow.get(position).getUser().getPostCount());
                                    intent.putExtra("follow", mNotifiModelFollow.get(position).getUser().getFollowerCount());
                                    intent.putExtra("following", mNotifiModelFollow.get(position).getUser().getFollowingCount());
                                    intent.putExtra("open", mNotifiModelFollow.get(position).getUser().getOpenID());
                                    intent.putExtra("bio", mNotifiModelFollow.get(position).getUser().getBio());
                                    intent.putExtra("targetUserID", mNotifiModelFollow.get(position).getUser().getUserID());
                                    intent.putExtra("web", mNotifiModelFollow.get(position).getUser().getWebsite());
                                    startActivity(intent);
                                }
                            }
                            else
                            {
                                if(mNotifiModelFollow.get(position).getType().compareTo("follow")==0)
                                {
                                    if(mNotifiModelFollow.get(position).getTargetUserInfo()!=null)
                                    {
                                        if (mNotifiModelFollow.get(position).getTargetUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                                        {
                                            state = true;
                                            Intent intent = new Intent();
                                            intent.setClass(getActivity(), HomeUserActivity.class);
                                            intent.putExtra("title", mNotifiModelFollow.get(position).getTargetUserInfo().getName());
                                            intent.putExtra("picture", mNotifiModelFollow.get(position).getTargetUserInfo().getPicture());
                                            intent.putExtra("isfollowing", mNotifiModelFollow.get(position).getTargetUserInfo().getIsFollowing());
                                            intent.putExtra("post", mNotifiModelFollow.get(position).getTargetUserInfo().getPostCount());
                                            intent.putExtra("follow", mNotifiModelFollow.get(position).getTargetUserInfo().getFollowerCount());
                                            intent.putExtra("following", mNotifiModelFollow.get(position).getTargetUserInfo().getFollowingCount());
                                            intent.putExtra("open", mNotifiModelFollow.get(position).getTargetUserInfo().getOpenID());
                                            intent.putExtra("bio", mNotifiModelFollow.get(position).getTargetUserInfo().getBio());
                                            intent.putExtra("targetUserID", mNotifiModelFollow.get(position).getTargetUserInfo().getUserID());
                                            intent.putExtra("web", mNotifiModelFollow.get(position).getTargetUserInfo().getWebsite());
                                            startActivity(intent);
                                        }
                                    }
                                }
                                else
                                {
                                    if(mNotifiModelFollow.get(position).getPost()!=null)
                                    {
                                        if (mNotifiModelFollow.get(position).getPost().getUser().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                                        {
                                            state = true;
                                            Intent intent = new Intent();
                                            intent.setClass(getActivity(), HomeUserActivity.class);
                                            intent.putExtra("title", mNotifiModelFollow.get(position).getPost().getUser().getName());
                                            intent.putExtra("picture", mNotifiModelFollow.get(position).getPost().getUser().getPicture());
                                            intent.putExtra("isfollowing", mNotifiModelFollow.get(position).getPost().getUser().getIsFollowing());
                                            intent.putExtra("post", mNotifiModelFollow.get(position).getPost().getUser().getPostCount());
                                            intent.putExtra("follow", mNotifiModelFollow.get(position).getPost().getUser().getFollowerCount());
                                            intent.putExtra("following", mNotifiModelFollow.get(position).getPost().getUser().getFollowingCount());
                                            intent.putExtra("open", mNotifiModelFollow.get(position).getPost().getUser().getOpenID());
                                            intent.putExtra("bio", mNotifiModelFollow.get(position).getPost().getUser().getBio());
                                            intent.putExtra("targetUserID", mNotifiModelFollow.get(position).getPost().getUser().getUserID());
                                            intent.putExtra("web", mNotifiModelFollow.get(position).getPost().getUser().getWebsite());
                                            startActivity(intent);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });

                removeLine(holder.doing);

                if(position>=getCount()-5)
                {
                    LoadDataFollow(false);
                }
            }

            return convertView;
        }
    }

    private class ViewHolderFollow
    {
        ImageView pic;
        TextView day;
        ImageView img;
        ImageView follow;
        SocialTextView doing;
    }

    private void removeLine(SocialTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    public void LoadDataFollow(final boolean refresh)
    {
        if(isFetchingDataFoolow)
        {
            return;
        }

        if(refresh)
        {
            noMoreDataFollow = false;
        }

        if(noMoreDataFollow)
        {
            return;
        }

        isFetchingDataFoolow = true;

        ApiManager.getFriendNotif(getActivity(), mNotifiModelFollow.get(mNotifiModelFollow.size() - 1).getTimestamp(), 15, new ApiManager.GetFriendNotifCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<NotifiModel> notifiModel) {
                isFetchingDataFoolow = false;

                if (success && notifiModel != null) {
                    if (notifiModel.size() != 0) {
                        mNotifiModelFollow.addAll(notifiModel);

                        if (mNotifiModelFollow.size() < 15) {
                            noMoreDataFollow = true;
                        }

                        if (mListAdapterFollow != null) {
                            mListAdapterFollow.notifyDataSetChanged();
                        }
                    }
                } else {
                    try{
//                                      ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    //    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//
//        if(!hidden)
//        {
//            mViewPager.setAdapter(mAdapter);
//        }
//    }


//    MyPagerAdapter adapterViewPager;
//
//    public class MyPagerAdapter extends FragmentPagerAdapter
//    {
//        public MyPagerAdapter(FragmentManager fragmentManager)
//        {
//            super(fragmentManager);
//        }
//
//        @Override
//        public int getCount()
//        {
//            return 3;
//        }
//
//        @Override
//        public Fragment getItem(int position)
//        {
//            switch (position)
//            {
//                case 0:
//                    return new NotifiSelfFragment();
//                case 1:
//                    return new NotifiFollowFragment();
//                case 2:
//                    return new NotifiSystemFragment();
//                default:
//                    return null;
//            }
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position)
//        {
//            return "Page " + position;
//        }
//    }
//
//    @Override
//    public void onPause()
//    {
//        super.onPause();
//    }

    //        mNotifiSelfFragment = new NotifiSelfFragment();
//        mNotifiFollowFragment = new NotifiFollowFragment();
//        mNotifiSystemFragment = new NotifiSystemFragment();
//
//        mFragmentPagerItems = FragmentPagerItems.with(getActivity())
//        .add("自己", mNotifiSelfFragment.getClass())
//        .add("追蹤中", mNotifiFollowFragment.getClass())
//        .add("系統通知", mNotifiSystemFragment.getClass())
//        .create();
//
//        mAdapter = new FragmentPagerItemAdapter(
//        getActivity().getSupportFragmentManager(),mFragmentPagerItems );
//
//        mViewPager = (ViewPager) getView().findViewById(R.id.viewpager);
//        mViewPager.setAdapter(mAdapter);
//
//        mPagerTab = (SmartTabLayout) getView().findViewById(R.id.viewpagertab);
//        mPagerTab.setViewPager(mViewPager);

//        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
//        {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
//            {
//            }
//
//            @Override
//            public void onPageSelected(int position)
//            {
//                switch (position)
//                {
//                    case 0 :
//                        break;
//                    case 1 :
//                        break;
//                    case 2 :
//                        break;
//                }
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state)
//            {
//            }
//        });

//    private ViewPager mViewPager;
//    private SmartTabLayout mPagerTab;
//    private FragmentPagerItemAdapter mAdapter;
//    private FragmentPagerItems mFragmentPagerItems;
//
//    private NotifiSelfFragment mNotifiSelfFragment;
//    private NotifiFollowFragment mNotifiFollowFragment;
//    private NotifiSystemFragment mNotifiSystemFragment;

}
