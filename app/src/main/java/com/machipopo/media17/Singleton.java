package com.machipopo.media17;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

public class Singleton {

	private static Singleton singleton = null;

	public static Context applicationContext = null;

	/* Download, Upload */
	public static  HashMap<String, String> updateDict = new HashMap<String, String>();

	/* Group & Block update */
	public static ArrayList<String> groupArray = new ArrayList<String>();
	public static ArrayList<String> blockArray = new ArrayList<String>();
	
	public static String chattingUserID = "";

	public static Gson gson = new Gson();
	public static Resources resources = null;
	public static SharedPreferences preferences = null;
	public static Editor preferenceEditor = null;
	public static float SCREEN_DENSITY = 0.0f;
	public static int SCREEN_WIDTH = 0;
	public static int SCREEN_HEIGHT = 0;
	public static Handler handler;
	public static String cdnBaseUrl = Constants.NO_CHT_ALL_URL;
	public static Map<String,Float> mCurrencyRate = new HashMap<>();

	public static TelephonyManager mTelManager;


	private Singleton(Context context) {
		try {
			// universal image loader global config
			File cacheDir = StorageUtils.getCacheDirectory(context);

			DisplayImageOptions defaultDisplayImageOptions = new DisplayImageOptions.Builder()
			.cacheOnDisc(true).build();

			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
					.discCache(new UnlimitedDiskCache(cacheDir))
			.threadPoolSize(5) // default
			.defaultDisplayImageOptions(defaultDisplayImageOptions)
			.discCacheSize(64 * 1024 * 1024)
			.discCacheFileCount(10000)
			.build();

			ImageLoader.getInstance().init(config);

			mCurrencyRate.put("TWD", 31.0f);
			mCurrencyRate.put("CNY", 6f);
			mCurrencyRate.put("MOP", 7.6f);
			mCurrencyRate.put("HKD", 7.4f);
			mCurrencyRate.put("SGD", 1.3f);
			mCurrencyRate.put("MYR", 3.9f);
			mCurrencyRate.put("USD", 1f);
			mCurrencyRate.put("JPY", 124f);

		} catch (Exception e) {

		}
	}

	public static DisplayImageOptions getDisplayImageOptions(int placeholderImageResourceID) {
		return new DisplayImageOptions.Builder()
		.cacheInMemory(false)
		.cacheOnDisc(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.showImageOnLoading(placeholderImageResourceID).build();
	}

	public static boolean isMultiMedia(String type) {
		if(type.equals("image")) {
			return true;
		} else {
			return false;
		}
	}
	
	public static Singleton getInstance(Context appContext) {
		if(applicationContext==null&&appContext!=null) {
			applicationContext = appContext;
			handler = new Handler(appContext.getMainLooper());

			resources = applicationContext.getResources();

			preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
			preferenceEditor = preferences.edit();

			WindowManager wm = (WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE);
			Display disp = wm.getDefaultDisplay();

			SCREEN_WIDTH = disp.getWidth();
			SCREEN_HEIGHT = disp.getHeight();
			SCREEN_DENSITY = resources.getDisplayMetrics().density;

			cdnBaseUrl = preferences.getString(Constants.CDN_BASE_URL, Constants.NO_CHT_ALL_URL);

			mTelManager = (TelephonyManager) appContext.getSystemService(Context.TELEPHONY_SERVICE);
		}

		if (singleton == null) {
			synchronized (Singleton.class) {
				if (singleton == null) {
					singleton = new Singleton(appContext);
				}
			}
		}

		return singleton;
	}

	public static void log(String log) {
		if(Constants.IS_SANDBOX) {
			if(log==null) {
				Log.e(Singleton.applicationContext.getPackageName(), "NULL POINTER!");
			} else {
				Log.e(Singleton.applicationContext.getPackageName(), log);
			}
		}
	}

	/* Get GCM Token */
	public static String getGCMToken() {
		String gcmToken = "";

		try {
//			GCMRegistrar.checkDevice(applicationContext);
//			GCMRegistrar.checkManifest(applicationContext);

//			gcmToken = GCMRegistrar.getRegistrationId(applicationContext);

//			if (gcmToken.equals("")) {
//				GCMRegistrar.register(applicationContext, Constants.GCM_APP_ID);
//			}
		} catch (Exception e) {
			return "";
		}

		return gcmToken;
	}

	public static boolean isSdCardMounted() {
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	}

	public static void createExternalMediaFolder() {
		String mediaFolderPath = getExternalMediaFolderPath();
		new File(mediaFolderPath).mkdirs();
	}

	public static void createPublicFolder() {
		String folderPath = getExternalPublicFolderPath();
		new File(folderPath).mkdirs();
	}

	public static String getExternalMediaFolderPath() {
		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/";
	}

	public static String getExternalPublicFolderPath() {
		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+Singleton.applicationContext.getPackageName()+"/";
	}

	public static String getExternalTempImagePath() {
		return getExternalMediaFolderPath() + "tmp.jpg";
	}

	public static String getExternalPublicLiveStreamVideoFrameFolderPath() {
		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+Singleton.applicationContext.getPackageName()+".livestream/";
	}

	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list =
				packageManager.queryIntentActivities(intent,
						PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	public static String getFileName(String filePath) {
		String[] components = filePath.split("/");
		return components[components.length-1];
	}

	public static int dpToPixel(int x) {
		return (int) (SCREEN_DENSITY * (float)x);
	}

	public static int dpToPixel(float x) {
		return (int) (SCREEN_DENSITY * x);
	}

	public static int pixelToDp(int x) {
		return (int) ((float)x/SCREEN_DENSITY);
	}

	public static String toJSON(Object obj) {
		if(gson==null) {
			gson = new Gson();
		}

		return gson.toJson(obj);
	}

	public static String getRealPathFromURI(Uri uri, Activity activity){
		Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
		cursor.moveToFirst();
		String document_id = cursor.getString(0);
		document_id = document_id.substring(document_id.lastIndexOf(":")+1);
		cursor.close();

		cursor = activity.getContentResolver().query(
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
		cursor.moveToFirst();
		String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
		cursor.close();

		return path;
	}

	public static String getExternalStoragePath() {
		if(externalStorageAvailable()==false) {
			return null;
		} else {
			return Environment.getExternalStorageDirectory().getPath();
		}
	}

	/* check if external sdcard is mounted */
	public static boolean externalStorageAvailable() {
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state))
			return true;
		else
			return false;
	}

	/* Date Formatter */
	private static SimpleDateFormat hour_Minute_DateFormatter = new SimpleDateFormat("HH:mm");;
	private static SimpleDateFormat month_Day_Weekday_DateFormatter = new SimpleDateFormat("M/d (EEE)");;
	private static SimpleDateFormat month_Day_DateFormatter = new SimpleDateFormat("M/d");;
	private static SimpleDateFormat weekday_DateFormatter = new SimpleDateFormat("EEE");
	private static SimpleDateFormat birthday_DateFormatter = new SimpleDateFormat("yyyy/MM/dd");

	public static String toBirthdayDateString(int timestamp) {
		Date date = new Date(timestamp*1000L);
		return birthday_DateFormatter.format(date);
	}

	// => MM/DD (星期幾)
	public static String toDateString(int timestamp) {
		Date date = new Date(timestamp*1000L);
		return month_Day_Weekday_DateFormatter.format(date);
	}

	// => HH:mm
	public static String toMessageTimeString(int timestamp) {
		Date date = new Date(timestamp*1000L);
		return hour_Minute_DateFormatter.format(date);
	}

	// 同一天=> HH:mm, 一個禮拜內=> 星期幾, 超過一個禮拜=> MM/DD
	public static String toChatThreadTimeString(int timestamp) {
		Date date = new Date(timestamp*1000L);

		int currentTime = getCurrentTimestamp();
		int timeOffset = currentTime-timestamp;

		if(timeOffset<86400) {
			return hour_Minute_DateFormatter.format(date);
		} else if(timeOffset<86400*5) {
			return weekday_DateFormatter.format(date);
		} else {
			return month_Day_DateFormatter.format(date);
		}
	}

	public static String getElapsedTimeString(int timestamp) {
		int elapsedTime = getCurrentTimestamp()-timestamp;

		if(elapsedTime<60) {
			return resources.getString(R.string.just_now);
		} else if(elapsedTime<3600) {
			return String.format(resources.getString(R.string.mins_ago_placeholder), (elapsedTime/60));
		} else if(elapsedTime<86400) {
			return String.format(resources.getString(R.string.hrs_ago_placeholder), (elapsedTime/3600));
		} else if(elapsedTime<86400*30) {
			return String.format(resources.getString(R.string.days_ago_placeholder), (elapsedTime/86400));
		} else if(elapsedTime<86400*360) {
			return String.format(resources.getString(R.string.months_ago_placeholder), (elapsedTime/86400/30));
		} else {
			return String.format(resources.getString(R.string.yrs_ago_placeholder), (elapsedTime/86400/300));
		}
	}

	/* Resource */
	public static int getDrawableResourceID(String name) {
		try {
			return resources.getIdentifier(name, "drawable", Singleton.applicationContext.getPackageName());
		} catch (Exception e) {
			return 0;
		}
	}

//	public static int getStringResourceID(String name) {
//		try {
//			int id = resources.getIdentifier(name, "string", Singleton.applicationContext.getPackageName());
//
//			if(id==0) {
//				return R.string.empty;
//			}
//
//			return id;
//		} catch (Exception e) {
//			return R.string.empty;
//		}
//	}

	public static int getRawResourceID(String name) {
		try {
			return resources.getIdentifier(name, "raw", Singleton.applicationContext.getPackageName());
		} catch (Exception e) {
			return 0;
		}
	}

	/* helper methods */
	public static String getUUIDFileName(String fileExtension) {
		return getUUID() + "." + fileExtension;
	}

	public static int getTimeZoneOffset() {
		return TimeZone.getDefault().getOffset(0)/1000;
	}

	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	public static int getCurrentTimestamp() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		return (int) (calendar.getTimeInMillis()/1000L);
	}

	public static long getCurrentTimestamp_min() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		return calendar.getTimeInMillis();
	}

	// file related
	public static boolean fileExist(String filePath) {
		return new File(filePath).exists();
	}

	public static long getFileSize(String filePath) {
		return new File(filePath).length();
	}

	public static void copyFile(File sourceFile, File destFile) {
		FileChannel source = null;
		FileChannel destination = null;

		try {
			if(!destFile.exists()) {
				destFile.createNewFile();
			}

			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());

			if(source != null) {
				source.close();
			}
			if(destination != null) {
				destination.close();
			}
		} catch (Exception e) {
			destFile.delete();
		}
	}
	
	/* Helpers */
//	public static boolean loginCompleted() {
//		String myUserID = myUserID();
//
//		if(myUserID.equals("")) {
//			return false;
//		} else {
//			return true;
//		}
//	}
	
	private static String myUserID = "";

	public static String myUserID() {
		if(myUserID==null || myUserID.equals("")) {
			myUserID = preferences.getString(Constants.USER_ID, ""); 
		}
		
		return myUserID;
	}

//	public static ArrayList<String> jsonToStringArrayList(String jsonString) {
//		ArrayList<String> array = new ArrayList<String>();
//
//		try {
//			JSONArray jsonObjectArray = new JSONArray(jsonString);
//
//			for(int i=0;i<jsonObjectArray.length();i++) {
//				array.add(jsonObjectArray.getString(i));
//			}
//		} catch (Exception e) {
//			log("JSON TO STRING ARRAY EXCEPTION: " + e.toString());
//		}
//
//		return array;
//	}

//	public static HashSet<String> jsonToStringHashSet(String jsonString){
//		HashSet<String> hashSet = new HashSet<String>();
//
//		try {
//			JSONArray jsonObjectArray = new JSONArray(jsonString);
//
//			for(int i=0;i<jsonObjectArray.length();i++) {
//				hashSet.add(jsonObjectArray.getString(i));
//			}
//		} catch (Exception e) {
//
//		}
//
//		return hashSet;
//	}

	public static String timestampToConstellationString(int timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp*1000L);

		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);

		if(month==1) {
			if(day<20) {
				return "capricorn";
			} else {
				return "aquarius";
			}
		} else if(month==2) {
			if(day<19) {
				return "aquarius";
			} else {
				return "pisces";
			}
		} else if(month==3) {
			if(day<21) {
				return "pisces";
			} else {
				return "aries";
			}
		} else if(month==4) {
			if(day<20) {
				return "aries";
			} else {
				return "taurus";
			}
		} else if(month==5) {
			if(day<21) {
				return "taurus";
			} else {
				return "gemini";
			}
		} else if(month==6) {
			if(day<22) {
				return "gemini";
			} else {
				return "cancer";
			}
		} else if(month==7) {
			if(day<23) {
				return "cancer";
			} else {
				return "leo";
			}
		} else if(month==8) {
			if(day<23) {
				return "leo";
			} else {
				return "virgo";
			}
		} else if(month==9) {
			if(day<23) {
				return "virgo";
			} else {
				return "libra";
			}
		} else if(month==10) {
			if(day<24) {
				return "libra";
			} else {
				return "scorpio";
			}
		} else if(month==11) {
			if(day<22) {
				return "scorpio";
			} else {
				return "sagittarius";
			}
		} else if(month==12) {
			if(day<22) {
				return "sagittarius";
			} else {
				return "capricorn";
			}
		}

		return "";
	}

//	public static String convertStreamToString(InputStream is) {
//		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
//		return s.hasNext() ? s.next() : "";
//	}

//	public static int inverseInt(int input) {
//		if(input==0) {
//			return 1;
//		} else {
//			return 0;
//		}
//	}

	public static boolean isVip() {
		int vipExp = Singleton.preferences.getInt(Constants.VIP_EXPIRATION, 0);
		if(vipExp>getCurrentTimestamp()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isPointSubsCription() {
		int pointExp = Singleton.preferences.getInt(Constants.POINT_EXPIRATION, 0);
		if(pointExp>getCurrentTimestamp()) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isNew() {
		int registerExp = Singleton.preferences.getInt(Constants.REGISTER_TIME, 0);

		if(registerExp>getCurrentTimestamp()-86400*7) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public static void logoutAction(Context context, boolean needKillProcess) {
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancelAll();

		// clean all file cache
		new File(Singleton.getExternalMediaFolderPath()).delete();

		preferenceEditor.clear();
		preferenceEditor.commit();
		
		singleton.myUserID = "";
		singleton.myGender = "";

		singleton = null;

		if(needKillProcess) {
			killProcess();
		}
	}

//	public static String getLastGalleryPictureFilePath(Activity activity) {
//		String[] projection = new String[]{MediaStore.Images.ImageColumns._ID,
//				MediaStore.Images.ImageColumns.DATA,
//				MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
//				MediaStore.Images.ImageColumns.DATE_TAKEN,
//				MediaStore.Images.ImageColumns.MIME_TYPE
//		};
//
//		String selection = MediaStore.Images.Media.DATE_TAKEN + "<" + String.valueOf((long) getCurrentTimestamp()*1000L + 10000L);
//
//		final Cursor cursor = activity.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//				projection, selection, null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");
//
//		if (cursor.moveToFirst()) {
//			String imageLocation = cursor.getString(1);
//
//			return imageLocation;
//		}
//
//		return null;
//	}

//	public static void playSound(int soundResourceID) {
//		final MediaPlayer player = MediaPlayer.create(applicationContext, soundResourceID);
//
//		player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//			@Override
//			public void onCompletion(MediaPlayer mp) {
//				player.release();
//			}
//		});
//
//		player.start();
//	}

	public static void killProcess() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public static String loadAssetFile(Context context, String fileName) {
		String tContents = "";

		try {
			InputStream stream = context.getAssets().open(fileName);

			int size = stream.available();
			byte[] buffer = new byte[size];
			stream.read(buffer);
			stream.close();
			tContents = new String(buffer);
		} catch (IOException e) {
			// Handle exceptions here
		}

		return tContents;
	}

//	public static void generateAppUUID() {
//		String filePath = getExternalMediaFolderPath() + "uuid.txt";
//
//		if(!fileExist(filePath)) {
//			String appUUID = Secure.getString(applicationContext.getContentResolver(), Secure.ANDROID_ID);
//
//			if(appUUID==null || appUUID.isEmpty()) {
//				appUUID = getUUID();
//			}
//
//			BufferedWriter writer = null;
//			try {
//				writer = new BufferedWriter( new FileWriter(filePath));
//				writer.write( appUUID);
//
//			} catch (IOException e) {
//
//			} finally {
//				try {
//					if(writer!=null) {
//						writer.close();
//					}
//				} catch (IOException e) {
//
//				}
//			}
//		}
//	}

//	public static String getAppUUID() {
//		String filePath = getExternalMediaFolderPath() + "uuid.txt";
//
//		try {
//			BufferedReader br = new BufferedReader(new FileReader(filePath));
//
//			StringBuilder sb = new StringBuilder();
//			String line = br.readLine();
//
//			while (line != null) {
//				sb.append(line);
//				sb.append("\n");
//				line = br.readLine();
//			}
//
//			br.close();
//
//			return sb.toString().trim();
//		} catch (Exception e) {
//			return "";
//		}
//	}

	public static void generateIsFreezedFile() {
		String filePath = getExternalMediaFolderPath() + "freezed.txt";

		if(!fileExist(filePath)) {
			String appUUID = getUUID();

			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter( new FileWriter(filePath));
				writer.write( appUUID);

			} catch (IOException e) {

			} finally {
				try {
					if(writer!=null) {
						writer.close();
					}
				} catch (IOException e) {

				}
			}
		}
	}

//	public static boolean isFreezed() {
//		return fileExist(getExternalMediaFolderPath() + "freezed.txt");
//	}

	public static String getVersion() {
		try {
			return applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionName;
		} catch(Exception e) {
			return "1.0";
		}
	}

//	public static boolean isTablet() {
//		return (applicationContext.getResources().getConfiguration().screenLayout
//				& Configuration.SCREENLAYOUT_SIZE_MASK)
//				>= Configuration.SCREENLAYOUT_SIZE_LARGE;
//	}

//	public static int getResourceId(String pVariableName, String pResourcename, String pPackageName){
//		try {
//			return applicationContext.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return -1;
//		}
//	}

//	public static String getLineInfo(){
//		StackTraceElement ste = new Throwable().getStackTrace()[1];
//		return ste.getFileName() + ": Line " + ste.getLineNumber() + " ";
//	}

	public static String getS3FileUrl(String filename){
		return cdnBaseUrl + filename;
	}

	public static String getLivestreamS3FileUrl(String filename){
		if(cdnBaseUrl.compareTo(Constants.NO_CHT_ALL_URL)==0){
			return Constants.NO_CHT_ALL_URL + filename;
		}
		else{
			return Constants.NO_CHT_ALL_URL + filename;
		}
	}

	public static String getLivestreamS3FileUrlCheoos(String filename){
		if(Constants.INTERNATIONAL_VERSION){
//			Log.d("17_g","INT URL = "+Constants.NO_CHT_ALL_URL + filename);
			return Constants.NO_CHT_ALL_URL + filename;
		}
		else{
//			Log.d("17_g"," URL = "+Constants.CDN_LIVE_URL_CHINA + filename);
			return Constants.CDN_LIVE_URL_CHINA + filename;
		}
	}

//	public static int fbBirthdayStringToUnixTime(String birthString) {
//		try {
//			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
//			Date birthdayDate = dateFormat.parse(birthString);
//			return (int) (birthdayDate.getTime()/1000);
//		} catch (Exception e) {
//			return getCurrentTimestamp();
//		}
//	}

//	public static int birthdayToAge(int birthTime) {
//		try {
//			Calendar dob = Calendar.getInstance();
//			dob.setTimeInMillis((long)birthTime*1000L);
//			Calendar today = Calendar.getInstance();
//			int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
//			if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
//				age--;
//			} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
//					&& today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
//				age--;
//			}
//
//			return age;
//		} catch (Exception e) {
//			return 0;
//		}
//	}

	// tricky hack method to change NumberPicker text color!
//	public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
//		final int count = numberPicker.getChildCount();
//		for(int i = 0; i < count; i++){
//			View child = numberPicker.getChildAt(i);
//			if(child instanceof EditText){
//				try{
//					Field selectorWheelPaintField = numberPicker.getClass()
//							.getDeclaredField("mSelectorWheelPaint");
//					selectorWheelPaintField.setAccessible(true);
//					((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
//					((EditText)child).setTextColor(color);
//					numberPicker.invalidate();
//					return true;
//				} catch(Exception e){
//
//				}
//			}
//		}
//
//		return false;
//	}

	public static String md5(String str)
	{
		MessageDigest md5 = null;
		try
		{
			md5 = MessageDigest.getInstance("MD5");
		}catch(Exception e)
		{
			e.printStackTrace();
			return "";
		}

		char[] charArray = str.toCharArray();
		byte[] byteArray = new byte[charArray.length];

		for(int i = 0; i < charArray.length; i++)
		{
			byteArray[i] = (byte)charArray[i];
		}
		byte[] md5Bytes = md5.digest(byteArray);

		StringBuffer hexValue = new StringBuffer();
		for( int i = 0; i < md5Bytes.length; i++)
		{
			int val = ((int)md5Bytes[i])&0xff;
			if(val < 16)
			{
				hexValue.append("0");
			}
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();
	}

//	public static String md5(String s) {
//	    try {
//	        // Create MD5 Hash
//	        MessageDigest digest = MessageDigest.getInstance("MD5");
//	        digest.update(s.getBytes());
//	        byte messageDigest[] = digest.digest();
//
//	        // Create Hex String
//	        StringBuffer hexString = new StringBuffer();
//	        for (int i=0; i<messageDigest.length; i++)
//	            hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
//	        return hexString.toString();
//
//	    } catch (NoSuchAlgorithmException e) {
//	        e.printStackTrace();
//	    }
//	    return "";
//	}

	public static int getInfoCompletePercentage() {
		int completeItemCount = 0;
		int totalItemCount = 32;
		
		if(!preferences.getString(Constants.NICKNAME, "").equals("")) {
			completeItemCount++;
		}
		if(!preferences.getString(Constants.RELIGION, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.PICTURE, "").equals("")) {
			completeItemCount++;
		}
		
		if(preferences.getInt(Constants.HEIGHT, 0)!=0) {
			completeItemCount++;
		}
		
		if(preferences.getInt(Constants.WEIGHT, 0)!=0) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.INTRO, "").equals("")) {
			completeItemCount++;
		}
		
//		if(!preferences.getString(Constants.BORN_CITY, "").equals("")) {
//			completeItemCount++;
//		}
		
		if(!preferences.getString(Constants.BORN_NATION, "").equals("")) {
			completeItemCount++;
		}
		
//		if(!preferences.getString(Constants.CURRENT_CITY, "").equals("")) {
//			completeItemCount++;
//		}
//		
		if(!preferences.getString(Constants.CURRENT_NATION, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.BODY_TYPE, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.HOME_RANK, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.DATE_FEE_PAYER, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.JOB_TYPE, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.JOB_POSITION, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.BLOOD_TYPE, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.SCHOOL_NAME, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.EDUCATION, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.COMPANY_NAME, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.LANGUAGES, "[]").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.HAVE_CHILD, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.BEFORE_DATE, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.MARRIAGE_ATTITUDE, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.INCOME, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.INTEREST, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.PERSONALITY, "[]").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.RELATIONSHIP, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.LIVE_WITH, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.SMOKE, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.DRINK, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.WANT_CHILD, "").equals("")) {
			completeItemCount++;
		}
		
		if(!preferences.getString(Constants.HOUSE_WORK, "").equals("")) {
			completeItemCount++;
		}
		
		return (int) ((float)completeItemCount/(float)(totalItemCount-2)*100);
	}
	
//	public static String getLocalizedStringByKey (String str){
//
//		if(!Singleton.preferences.getString(str, "").equals("")){
//			String localized  = resources.getString(Singleton.getStringResourceID(Singleton.preferences.getString(str, "")));
//			return  localized;
//		}
//		return "";
//	}
	
//	public static String getLocalizedString (String str){
//		return resources.getString(Singleton.getStringResourceID(str));
//	}
	
//	public static String getStringFromArrayByKey (String key){
//		String cascadeStr = "";
//		ArrayList<String> strArray  = new ArrayList<String>();
//		strArray =  Singleton.jsonToStringArrayList(Singleton.preferences.getString(key, "[]"));
//		int index = 1;
//		for (String str : strArray)
//		{
//			cascadeStr = cascadeStr +  resources.getString(Singleton.getStringResourceID(str));
//			if(index!=strArray.size()){
//				cascadeStr = cascadeStr + ", ";
//			}
//			index += 1;
//		}
//		return  cascadeStr;
//	}
//	public static String getSelectedStringFromHashSetAndKey(HashSet<Integer> set, ArrayList<String> inputArray){
//		String cascadeStr = "";
//		ArrayList<String> strArray  = inputArray;
//		int index = 0 ;
//		int countFlag = 0;
//		for(String str : strArray){
//			if(set.contains(index)){
//				countFlag += 1;
//				cascadeStr  = cascadeStr +  resources.getString(Singleton.getStringResourceID(str));
//				if(countFlag!=set.size()){
//					cascadeStr = cascadeStr + ", ";
//				}
//			}
//			index += 1;
//		}
//		return cascadeStr;
//	}
	
//	public static String getSelectedJsonStringFromHashSetAndKey(HashSet<Integer> set, ArrayList<String> inputArray){
//		ArrayList<String> cascadeArray = new ArrayList<String>();
//		ArrayList<String> strArray  = inputArray;
//		int index = 0 ;
//		for(String str : strArray){
//			if(set.contains(index)){
//				cascadeArray.add(str);
//			}
//			index += 1;
//		}
//		return  Singleton.toJSON(cascadeArray);
//	}
	
//	public static String getLocalededStringFromArray(ArrayList<String> inputArray){
//		String cascadeStr = "";
//		int countFlag = 0;
//		for(String str : inputArray){
//			countFlag += 1;
//			cascadeStr  = cascadeStr +  resources.getString(Singleton.getStringResourceID(str));
//			if(countFlag!=inputArray.size()){
//				cascadeStr = cascadeStr + ", ";
//			}
//		}
//		return cascadeStr;
//	}
	
//	public static ArrayList<String> getLocalizeStringOfArray(ArrayList<String> inputArray){
//		ArrayList<String> localized = new ArrayList<String>();
//		for(String str : inputArray){
//			String localStr = resources.getString(Singleton.getStringResourceID(str));
//			localized.add(localStr);
//		}
//		return localized;
//	}
	
//	public static UserObject getMyUserObject(){
//		UserObject myUser = new UserObject();
//
//		myUser.userID = Singleton.preferences.getString(Constants.USER_ID, "");
//		myUser.nickname = Singleton.preferences.getString(Constants.NICKNAME, "");
//		myUser.picture = Singleton.preferences.getString(Constants.PICTURE, "");
//		myUser.birthday = Singleton.preferences.getInt(Constants.BIRTHDAY, 0);
//		myUser.gender = Singleton.preferences.getString(Constants.GENDER, "");
//		myUser.constellation = Singleton.preferences.getString(Constants.CONSTELLATION, "");
//		myUser.height = Singleton.preferences.getInt(Constants.HEIGHT, 0);
//		myUser.weight = Singleton.preferences.getInt(Constants.WEIGHT, 0);
//		myUser.intro = Singleton.preferences.getString(Constants.INTRO, "");
//		myUser.bornCity = Singleton.preferences.getString(Constants.BORN_CITY, "");
//		myUser.bornNation = Singleton.preferences.getString(Constants.BORN_NATION, "");
//		myUser.currentCity = Singleton.preferences.getString(Constants.CURRENT_CITY, "");
//		myUser.currentNation = Singleton.preferences.getString(Constants.CURRENT_NATION, "");
//		myUser.religion = Singleton.preferences.getString(Constants.RELIGION, "[]");
//		myUser.bodyType = Singleton.preferences.getString(Constants.BODY_TYPE, "");
//		myUser.homeRank = Singleton.preferences.getString(Constants.HOME_RANK, "");
//		myUser.dateFeePayer = Singleton.preferences.getString(Constants.DATE_FEE_PAYER, "");
//		myUser.jobType = Singleton.preferences.getString(Constants.JOB_TYPE, "");
//		myUser.jobPosition = Singleton.preferences.getString(Constants.JOB_POSITION, "");
//		myUser.facebookFriendCount = Singleton.preferences.getInt(Constants.FACEBOOK_FRIEND_COUNT, 0);
//		myUser.albumPhotoCount = Singleton.preferences.getInt(Constants.ALBUM_PHOTO_COUNT, 0);
//		myUser.bloodType = Singleton.preferences.getString(Constants.BLOOD_TYPE, "");
//		myUser.schoolName = Singleton.preferences.getString(Constants.SCHOOL_NAME, "");
//		myUser.education = Singleton.preferences.getString(Constants.EDUCATION, "");
//		myUser.companyName = Singleton.preferences.getString(Constants.COMPANY_NAME, "");
//		myUser.languages = Singleton.preferences.getString(Constants.LANGUAGES, "");
//		myUser.haveChild = Singleton.preferences.getString(Constants.HAVE_CHILD, "");
//		myUser.beforeDate = Singleton.preferences.getString(Constants.BEFORE_DATE, "");
//		myUser.marriageAttitude = Singleton.preferences.getString(Constants.MARRIAGE_ATTITUDE, "");
//		myUser.income = Singleton.preferences.getString(Constants.INCOME, "");
//		myUser.interest = Singleton.preferences.getString(Constants.INTEREST, "");
//		myUser.personality = Singleton.preferences.getString(Constants.PERSONALITY, "[]");
//		myUser.relationship = Singleton.preferences.getString(Constants.RELATIONSHIP, "");
//		myUser.liveWith = Singleton.preferences.getString(Constants.LIVE_WITH, "");
//		myUser.smoke = Singleton.preferences.getString(Constants.SMOKE, "");
//		myUser.drink = Singleton.preferences.getString(Constants.DRINK, "");
//		myUser.wantChild = Singleton.preferences.getString(Constants.WANT_CHILD, "");
//		myUser.houseWork = Singleton.preferences.getString(Constants.HOUSE_WORK, "");
//		myUser.holiday = Singleton.preferences.getString(Constants.HOLIDAY, "");
//		myUser.point = Singleton.preferences.getInt(Constants.POINT, 0);
//
//		myUser.visitCount = Singleton.preferences.getInt(Constants.VISIT_COUNT, 0);
//		myUser.vipExpiration = Singleton.preferences.getInt(Constants.VIP_EXPIRATION, 0);
//		myUser.isHidden = Singleton.preferences.getInt(Constants.FACEBOOK_FRIEND_COUNT, 0);
//
//		myUser.platinumVipExpiration = Singleton.preferences.getInt(Constants.PLATINUM_VIP_EXPIRATION, 0);
//		myUser.receivedMessageCount = Singleton.preferences.getInt(Constants.RECEIVED_MESSAGE_COUNT, 0);
//
//		return myUser;
//	}
	
//	public static ChatThreadObject tempChatThread;

//	public static void setTempChatThread(ChatThreadObject chatThread){
//		tempChatThread = chatThread;
//	}
//	public static ChatThreadObject getTempChatThread(){
//		return tempChatThread;
//	}
	
	public static boolean hasVipPrivilege() {
		return isVip() || myGender().equals("female");
	}
	
	private static String myGender = "";
	
	public static boolean isFemale(){
		return myGender().equals("female");
	}
	
	public static String myGender() {
		if(myGender==null || myGender.equals("")) {
			myGender = preferences.getString(Constants.GENDER, "");
		}
		
		return myGender;
	}
	
//	public static String myPoint() {
//		return String.valueOf(preferences.getInt(Constants.POINT, 0));
//	}
	
//	public static boolean isPlatinumVip() {
//		int vipExp = Singleton.preferences.getInt(Constants.PLATINUM_VIP_EXPIRATION, 0);
//		if(vipExp>getCurrentTimestamp()) {
//			return true;
//		} else {
//			return false;
//		}
//	}
	
//	public static String getLocation(UserObject userObject){
//		String str = "";
//
//		if(userObject.currentCity.equals("") || userObject.currentCity.equals("other")) {
//			if(!userObject.currentNation.equals("")) {
//				str = getLocalizedString(userObject.currentNation);
//			} else {
//				str = getLocalizedString("other");
//			}
//		} else {
//			str = getLocalizedString(userObject.currentCity);
//		}
//
//		return str;
//	}
	
	
//	public static int getLovePictueFromMatchValue(int matchValue){
//		if(matchValue>=70 && matchValue<80){
//			return R.drawable.love80;
//		}else if(matchValue>=80 && matchValue<90){
//			return R.drawable.love80;
//		}else{
//			return R.drawable.love100;
//		}
//	}
	
//	public static ArrayList<String> getRegionOptions() {
//		ArrayList<String> regionOptions = new ArrayList<String>(Constants.REGION_OPTIONS);
//
//		String myCountry = Singleton.preferences.getString(Constants.MY_COUNTRY, "");
//
//		if(myCountry.equals("TW")) {
//			regionOptions.remove("taiwan");
//			regionOptions.add(0, "taiwan");
//		} else if(myCountry.equals("CN")) {
//			regionOptions.remove("china");
//			regionOptions.add(0, "china");
//		} else if(myCountry.equals("US")) {
//			regionOptions.remove("america");
//			regionOptions.add(0, "america");
//		} else if(myCountry.equals("JP")) {
//			regionOptions.remove("japan");
//			regionOptions.add(0, "japan");
//		} else if(myCountry.equals("HK")) {
//			regionOptions.remove("hongkong");
//			regionOptions.add(0, "hongkong");
//		} else if(myCountry.equals("MY")) {
//			regionOptions.remove("malaysia");
//			regionOptions.add(0, "malaysia");
//		} else if(myCountry.equals("SG")) {
//			regionOptions.remove("singapore");
//			regionOptions.add(0, "singapore");
//		}
//
//		return regionOptions;
//	}
	
//	public static boolean canShowThridPartyPayment() {
//		if(Singleton.preferences.getString(Constants.MY_COUNTRY, "").equals("TW")==false) {
//			return false;
//		}
//
//		return true;
//	}
	public static float getCurrencyRate(){
		return Singleton.mCurrencyRate.get(Singleton.getCurrencyType());
	}

	public static String getCurrencyType(){

		if(Singleton.preferences.getInt(Constants.CURRENCY_RATE_SETTED,0)==0){

			String countryCode = Singleton.preferences.getString(Constants.IP_COUNTRY,"US");
			if(countryCode=="TW"){
				return "TWD";
			}else if(countryCode=="CN"){
				return "CNY";
			}else if(countryCode=="US"){
				return "USD";
			}else if(countryCode=="MY"){
				return "MYR";
			}else if(countryCode=="HK"){
				return "HKD";
			}else if(countryCode=="MO"){
				return "MOP";
			}else if(countryCode=="SG"){
				return "SGD";
			}else if(countryCode=="JP"){
				return "JPY";
			}
			return "USD";
		}else {

			return Singleton.preferences.getString(Constants.CURRENCY_RATE,"USD");
		}
	}

	public static void detectIpGeoLocation(final Context c, final Story17Application application, final Boolean isUpdate)
	{
		try
		{
			OkHttpClient mClient = new OkHttpClient();

			Request request = new Request.Builder().url("http://ip-api.com/json").build();
			mClient.newCall(request).enqueue(new Callback()
			{
				@Override
				public void onResponse(Response response) throws IOException
				{
					if (response.isSuccessful())
					{
						try {
							JsonElement json = new JsonParser().parse(response.body().string());
							String countryCode = json.getAsJsonObject().get("countryCode").getAsString();
							String isp = json.getAsJsonObject().get("isp").getAsString().toLowerCase();
							String ip = json.getAsJsonObject().get("query").getAsString();
							String city = json.getAsJsonObject().get("country").getAsString();
							float lat = json.getAsJsonObject().get("lat").getAsFloat();
							float lon = json.getAsJsonObject().get("lon").getAsFloat();
							if(isUpdate && application!=null && ip.length()!=0) application.updateUserIP(c,ip);

							if(isp.contains("chunghwa")) {
								cdnBaseUrl = Constants.NO_CHT_ALL_URL;
								preferenceEditor.putString(Constants.CDN_BASE_URL, Constants.NO_CHT_ALL_URL);
							} else {
								cdnBaseUrl = Constants.NO_CHT_ALL_URL;
								preferenceEditor.putString(Constants.CDN_BASE_URL, Constants.NO_CHT_ALL_URL);
							}

							preferenceEditor.putString(Constants.IP, ip);
							preferenceEditor.putString(Constants.PAY_COUNTRY_CODE, "USA");//json.getAsJsonObject().get("countryCode").getAsString()
							preferenceEditor.putString(Constants.IP_COUNTRY, countryCode);
							preferenceEditor.putString(Constants.LIVE_MY_CITY, city);
							preferenceEditor.putFloat(Constants.LIVE_MY_LAT, lat);
							preferenceEditor.putFloat(Constants.LIVE_MY_LON, lon);
							preferenceEditor.commit();

							Singleton.log("countryCode: "+countryCode);
							Singleton.log("isp: "+isp);
						} catch(Exception e) {

						}
					}
				}

				@Override
				public void onFailure(Request request, IOException e)
				{

				}
            });
		} catch (Exception e) {

		}
	}


	public static ArrayList<String> getLocalizeStringOfArray(ArrayList<String> inputArray){
		ArrayList<String> localized = new ArrayList<String>();
		for(String str : inputArray){
			String localStr = resources.getString(Singleton.getStringResourceID(str));
			localized.add(localStr);
		}
		return localized;
	}

	public static String getLocalizedString (String str){
		return resources.getString(Singleton.getStringResourceID(str));
	}

	public static int getStringResourceID(String name) {
		try {
			int id = resources.getIdentifier(name, "string", Singleton.applicationContext.getPackageName());

			if(id==0) {
				return R.string.empty;
			}

			return id;
		} catch (Exception e) {
			return R.string.empty;
		}
	}

	public static String getPhoneIMEI()
	{
		try {
			if(mTelManager!=null){
				return mTelManager.getDeviceId();
			}
			else{
				return "";
			}
		}
		catch (Exception e){
			return "";
		}
	}
}