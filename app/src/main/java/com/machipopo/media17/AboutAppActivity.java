package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by POPO on 6/26/15.
 */
public class AboutAppActivity extends BaseNewActivity
{
    private AboutAppActivity mCtx = this;
    private LayoutInflater inflater;

    private ViewPager mPic;
    private ImageView mPage1,mPage2,mPage3,mPage4,mPage5;
    private Button btn,mBtn;

    private int[] mPhotos = {R.drawable.intro_01,R.drawable.intro_02,R.drawable.intro_03,R.drawable.intro_04,R.drawable.intro_05};
    private int mPos = 0;

    private List<View> mViewList;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mPic = (ViewPager) findViewById(R.id.pic);
        mPage1 = (ImageView) findViewById(R.id.page1);
        mPage2 = (ImageView) findViewById(R.id.page2);
        mPage3 = (ImageView) findViewById(R.id.page3);
        mPage4 = (ImageView) findViewById(R.id.page4);
        mPage5 = (ImageView) findViewById(R.id.page5);

        mBtn = (Button) findViewById(R.id.btn);

        mViewList = new ArrayList<View>();
        for(int i = 0 ; i < mPhotos.length ; i++)
        {
            mViewList.add(inflater.inflate(R.layout.image_row, null));
        }

        mPic.setAdapter(mPagerAdapter);
        mPic.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                mPos = position;
                changePage(mPos);

                if(mPos==(mPhotos.length-1))
                {
                    btn.setVisibility(View.GONE);
                    mBtn.setVisibility(View.VISIBLE);
                }
                else
                {
                    btn.setVisibility(View.VISIBLE);
                    mBtn.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.skip));
        btn.setTextColor(getResources().getColor(R.color.main_color));
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig("about","ok");
                Intent intent = new Intent();
                intent.setClass(mCtx, LoginMenuActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });

        mBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setConfig("about","ok");
                Intent intent = new Intent();
                intent.setClass(mCtx, LoginMenuActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });
    }

    private void changePage(int pos)
    {
        mPage1.setImageResource(R.drawable.point_gray);
        mPage2.setImageResource(R.drawable.point_gray);
        mPage3.setImageResource(R.drawable.point_gray);
        mPage4.setImageResource(R.drawable.point_gray);
        mPage5.setImageResource(R.drawable.point_gray);

        switch(pos)
        {
            case 0 : mPage1.setImageResource(R.drawable.point_green);
                break;
            case 1 : mPage2.setImageResource(R.drawable.point_green);
                break;
            case 2 : mPage3.setImageResource(R.drawable.point_green);
                break;
            case 3 : mPage4.setImageResource(R.drawable.point_green);
                break;
            case 4 : mPage5.setImageResource(R.drawable.point_green);
                break;
        }
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    private PagerAdapter mPagerAdapter = new PagerAdapter()
    {
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            return mViewList.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mViewList.get(position));
        }

        @Override
        public int getItemPosition(Object object)
        {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            container.addView(mViewList.get(position));

            ImageView mImg = (ImageView) mViewList.get(position).findViewById(R.id.img);
            mImg.setImageResource(mPhotos[position]);

            return mViewList.get(position);
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            setConfig("about","ok");
            Intent intent = new Intent();
            intent.setClass(mCtx, LoginMenuActivity.class);
            startActivity(intent);
            mCtx.finish();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
