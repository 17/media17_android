package com.machipopo.media17;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by POPO on 6/4/15.
 */
public class AddFriendActivity extends BaseNewActivity
{
    private AddFriendActivity mCtx = this;
    private InputMethodManager inputMethodManager = null;
    private LayoutInflater inflater;

    private ProgressBar mProgress;
    private ImageView mNoData;

    private ImageView mSearch, mContact, mFacebook, mIg, mTwitter;
    private LinearLayout mSearch_layout;
    private EditText mEdit;
    private ListView mSearch_list;

    private RelativeLayout mPhone_layout;
    private Button mBtnPhone;
    private ListView mPhone_list;

    private RelativeLayout mFacebook_layout;
    private Button mBtnFacebook;
    private ListView mFacebook_list;

    private LinearLayout mIgTabLayout;
    private RelativeLayout mIg_layout;
    private Button mBtnIg;
    private ListView mIg_list;

    private LinearLayout mTwitterTabLayout;
    private RelativeLayout mTwitter_layout;
//    private TwitterLoginButton mBtnTwitter;
    private ListView mTwitter_list;

    private ArrayList<HashMap<String, String>> mData;
    private ArrayList<UserModel> mPhoneModel = new ArrayList<UserModel>();
    private ArrayList<UserModel> mFacebookModel = new ArrayList<UserModel>();
    private ArrayList<UserModel> mUserModel = new ArrayList<UserModel>();

    private PhoneFriendAdapter mPhoneFriendAdapter;
    private FacebookFriendAdapter mFacebookFriendAdapter;
    private UserAdapter mUserAdapter;


    private CallbackManager callbackManager;
    private DisplayImageOptions SelfOptions;
    private int isPos = 1;

    private Dialog mInstagramDialog;

    private LinearLayout mFacebookTabLayout;

    private Boolean mTestSearch = false;
    private Story17Application mApplication;
    private String query; //for event tracking to record what keyword user input.

    public void onDestroy()
    {
        //event tracking
        try{
        LogEventUtil.LeaveFindFriendPage(mCtx, mApplication, Singleton.preferences.getString(Constants.USER_ID, ""));
        }
        catch (Exception x)
        {

        }

        super.onDestroy();
    }

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_friend_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        initTitleBar();

        mSearch = (ImageView) findViewById(R.id.search);
        mContact = (ImageView) findViewById(R.id.contact);
        mFacebook = (ImageView) findViewById(R.id.facebook);
        mIg = (ImageView) findViewById(R.id.ig);
        mTwitter = (ImageView) findViewById(R.id.twitter);

        mIgTabLayout = (LinearLayout) findViewById(R.id.ig_tab_layout);
        mTwitterTabLayout = (LinearLayout) findViewById(R.id.twitter_tab_layout);

        mSearch_layout = (LinearLayout) findViewById(R.id.search_layout);
        mPhone_layout = (RelativeLayout) findViewById(R.id.phone_layout);
        mFacebook_layout = (RelativeLayout) findViewById(R.id.facebook_layout);
        mIg_layout = (RelativeLayout) findViewById(R.id.ig_layout);
        mTwitter_layout = (RelativeLayout) findViewById(R.id.twitter_layout);

        mEdit = (EditText) findViewById(R.id.edit);

        mSearch_list = (ListView) findViewById(R.id.search_list);
        mPhone_list = (ListView) findViewById(R.id.phone_list);
        mFacebook_list = (ListView) findViewById(R.id.facebook_list);
        mIg_list = (ListView) findViewById(R.id.ig_list);
        mTwitter_list = (ListView) findViewById(R.id.twitter_list);

        mBtnPhone = (Button) findViewById(R.id.btn_phone);
        mBtnPhone.setVisibility(View.GONE);
        mBtnFacebook = (Button) findViewById(R.id.btn_facebook);
        mBtnIg = (Button) findViewById(R.id.btn_ig);
//        mBtnTwitter = (TwitterLoginButton) findViewById(R.id.btn_twitter);

        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mFacebookTabLayout = (LinearLayout) findViewById(R.id.facebook_tab_layout);
        if(!Constants.INTERNATIONAL_VERSION) mFacebookTabLayout.setVisibility(View.GONE);

        mApplication = (Story17Application) getApplication();

        //event tracking
        try{
        LogEventUtil.EnterFindFriendPage(mCtx,mApplication,Singleton.preferences.getString(Constants.USER_ID, ""));
        }
        catch (Exception x)
        {

        }

        if(mTestSearch)
        {
            mIgTabLayout.setVisibility(View.VISIBLE);
            mTwitterTabLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            mIgTabLayout.setVisibility(View.GONE);
            mTwitterTabLayout.setVisibility(View.GONE);
        }

        mEdit.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(s.toString().length()!=0)
                {
                    mProgress.setVisibility(View.VISIBLE);
                    mSearch_list.setVisibility(View.GONE);

                    //event tracking
                    query = s.toString();

                    ApiManager.getSearchUsers(mCtx, s.toString(), 0, 100, 0, new ApiManager.GetSearchUsersCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<UserModel> userModel)
                        {
                            mProgress.setVisibility(View.GONE);
//                            hideKeyboard();

                            if (success && userModel != null)
                            {
                                if (userModel.size() != 0)
                                {
                                    mNoData.setVisibility(View.GONE);

                                    mUserModel.clear();
                                    mUserModel.addAll(userModel);

                                    mUserAdapter = new UserAdapter();
                                    mSearch_list.setAdapter(mUserAdapter);
                                    mSearch_list.setVisibility(View.VISIBLE);
                                }
                                else
                                {
                                    if(mUserModel.size()==0) mNoData.setVisibility(View.VISIBLE);
                                }
                            }
                            else
                            {
                                if(mUserModel.size()==0) mNoData.setVisibility(View.VISIBLE);
                                try{
//                                    showToast(getString(R.string.search_failed));
                                    Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }
                            }
                        }
                    });
                }
                else
                {
                    mProgress.setVisibility(View.VISIBLE);
                    mSearch_list.setVisibility(View.GONE);

                    ApiManager.getHotUsers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, new ApiManager.GetHotUsersCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<UserModel> userModel) {
                            mProgress.setVisibility(View.GONE);
//                            hideKeyboard();

                            if (success && userModel != null) {
                                if (userModel.size() != 0) {
                                    mNoData.setVisibility(View.GONE);

                                    mUserModel.clear();
                                    mUserModel.addAll(userModel);

                                    mUserAdapter = new UserAdapter();
                                    mSearch_list.setAdapter(mUserAdapter);
                                    mSearch_list.setVisibility(View.VISIBLE);
                                } else {
                                    if(mUserModel.size()==0) mNoData.setVisibility(View.VISIBLE);
                                }
                            } else {
                                if(mUserModel.size()==0) mNoData.setVisibility(View.VISIBLE);
                                try{
//                                    showToast(getString(R.string.search_failed));
                                    Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });

        mSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                isPos = 1;
                mSearch.setImageResource(R.drawable.search_active);
                mContact.setImageResource(R.drawable.contact);
                mFacebook.setImageResource(R.drawable.facebook);
                mIg.setImageResource(R.drawable.ig);
                mTwitter.setImageResource(R.drawable.twitter);

                mSearch_layout.setVisibility(View.VISIBLE);
                mPhone_layout.setVisibility(View.GONE);
                mFacebook_layout.setVisibility(View.GONE);
                mIg_layout.setVisibility(View.GONE);
                mTwitter_layout.setVisibility(View.GONE);

                mProgress.setVisibility(View.GONE);
                mNoData.setVisibility(View.GONE);
            }
        });

        mContact.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
                isPos = 2;
                mSearch.setImageResource(R.drawable.search);
                mContact.setImageResource(R.drawable.contact_active);
                mFacebook.setImageResource(R.drawable.facebook);
                mIg.setImageResource(R.drawable.ig);
                mTwitter.setImageResource(R.drawable.twitter);

                mSearch_layout.setVisibility(View.GONE);
                mPhone_layout.setVisibility(View.VISIBLE);
                mFacebook_layout.setVisibility(View.GONE);
                mIg_layout.setVisibility(View.GONE);
                mTwitter_layout.setVisibility(View.GONE);

                mProgress.setVisibility(View.GONE);
                mNoData.setVisibility(View.GONE);

                if(mPhoneFriendAdapter!=null)
                {
//                    mBtnPhone.setVisibility(View.GONE);
                    mPhone_list.setVisibility(View.VISIBLE);
                }
                else
                {
//                    mBtnPhone.setVisibility(View.VISIBLE);
                    mPhone_list.setVisibility(View.VISIBLE);

                    mProgress.setVisibility(View.VISIBLE);

                    mData = new ArrayList<HashMap<String, String>>();
                    try {
                        Cursor c = mCtx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                        prepareData(c);
                    }
                    catch (Exception e){
                        mProgress.setVisibility(View.GONE);
                        if(isPos==2) mNoData.setVisibility(View.VISIBLE);
                    }

                    final JSONArray mJSONArray = new JSONArray();
                    final JSONArray mFindJSONArray = new JSONArray();

                    for(int i = 0 ; i < mData.size() ; i++)
                    {
                        String num = "";

                        if(mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length()!=0)
                        {
                            if(mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(0,1).contains("0")) num = mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(1,mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length());
                            else num  = mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER);

                            try
                            {
                                JSONObject mObject = new JSONObject();
                                mObject.put("phone",num.replaceAll("[^\\d]", ""));
                                mObject.put("name", mData.get(i).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                                mJSONArray.put(mObject);

                                JSONObject mObject2 = new JSONObject();
                                mObject2.put("phone",getConfig("countryCode","886") + num.replaceAll("[^\\d]", ""));
                                mObject2.put("name", mData.get(i).get(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                                mJSONArray.put(mObject2);
                                mFindJSONArray.put(num.replaceAll("[^\\d]", ""));
                            }
                            catch (JSONException e)
                            {

                            }
                        }
                    }

                    String phone = getConfig("phone","");
                    if(mJSONArray.length()!=0)
                    {
                        ApiManager.uploadPhoneNumbers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), phone, getConfig("countryCode","886"),mJSONArray, new ApiManager.UploadPhoneNumbersCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                mProgress.setVisibility(View.GONE);

                                if (success) {
                                    ApiManager.findFriends(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mFindJSONArray.toString(), "contacts", new ApiManager.FindFriendsCallback() {
                                        @Override
                                        public void onResult(boolean success, ArrayList<UserModel> model) {
                                            if (success && model != null) {
                                                if (model.size() != 0) {
                                                    mPhoneModel.clear();
                                                    mPhoneModel.addAll(model);

                                                    mPhoneFriendAdapter = new PhoneFriendAdapter();
                                                    mPhone_list.setAdapter(mPhoneFriendAdapter);
                                                } else {
                                                    if(isPos==2) mNoData.setVisibility(View.VISIBLE);
//                                                Toast.makeText(mCtx, "無朋友", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                if(isPos==2) mNoData.setVisibility(View.VISIBLE);
//                                            Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {
                                    if(isPos==2) mNoData.setVisibility(View.VISIBLE);
//                                Toast.makeText(mCtx, "電話搜尋失敗", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            }
        });

        mFacebook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
                isPos = 3;
                mSearch.setImageResource(R.drawable.search);
                mContact.setImageResource(R.drawable.contact);
                mFacebook.setImageResource(R.drawable.facebook_active);
                mIg.setImageResource(R.drawable.ig);
                mTwitter.setImageResource(R.drawable.twitter);

                mSearch_layout.setVisibility(View.GONE);
                mPhone_layout.setVisibility(View.GONE);
                mFacebook_layout.setVisibility(View.VISIBLE);
                mIg_layout.setVisibility(View.GONE);
                mTwitter_layout.setVisibility(View.GONE);

                mProgress.setVisibility(View.GONE);
                mNoData.setVisibility(View.GONE);

                if(mFacebookFriendAdapter!=null)
                {
                    mBtnFacebook.setVisibility(View.GONE);
                    mFacebook_list.setVisibility(View.VISIBLE);
                }
                else
                {
                    if(getConfig(Constants.SUMBIT_FACEBOOK,0)==0)
                    {
                        mBtnFacebook.setVisibility(View.VISIBLE);
                        mFacebook_list.setVisibility(View.GONE);
                    }
                    else
                    {
                        mBtnFacebook.setVisibility(View.GONE);
                        mFacebook_list.setVisibility(View.VISIBLE);
                        loadFbFriend();
                    }
                }
            }
        });

        mIg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
                isPos = 4;
                mSearch.setImageResource(R.drawable.search);
                mContact.setImageResource(R.drawable.contact);
                mFacebook.setImageResource(R.drawable.facebook);
                mIg.setImageResource(R.drawable.ig_active);
                mTwitter.setImageResource(R.drawable.twitter);

                mSearch_layout.setVisibility(View.GONE);
                mPhone_layout.setVisibility(View.GONE);
                mFacebook_layout.setVisibility(View.GONE);
                mIg_layout.setVisibility(View.VISIBLE);
                mTwitter_layout.setVisibility(View.GONE);

                mProgress.setVisibility(View.GONE);
                mNoData.setVisibility(View.GONE);

//                if(mIgFriendAdapter!=null)
//                {
//                    mBtnIg.setVisibility(View.GONE);
//                    mIg_list.setVisibility(View.VISIBLE);
//                }
//                else
//                {
//                    if(getConfig(Constants.SUMBIT_IG,0)==0)
//                    {
//                        mBtnIg.setVisibility(View.VISIBLE);
//                        mIg_list.setVisibility(View.GONE);
//                    }
//                    else
//                    {
//                        mBtnIg.setVisibility(View.GONE);
//                        mIg_list.setVisibility(View.VISIBLE);
//                    }
//                }
            }
        });

        mTwitter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
                isPos = 5;
                mSearch.setImageResource(R.drawable.search);
                mContact.setImageResource(R.drawable.contact);
                mFacebook.setImageResource(R.drawable.facebook);
                mIg.setImageResource(R.drawable.ig);
                mTwitter.setImageResource(R.drawable.twitter_active);

                mSearch_layout.setVisibility(View.GONE);
                mPhone_layout.setVisibility(View.GONE);
                mFacebook_layout.setVisibility(View.GONE);
                mIg_layout.setVisibility(View.GONE);
                mTwitter_layout.setVisibility(View.VISIBLE);

                mProgress.setVisibility(View.GONE);
                mNoData.setVisibility(View.GONE);

//                if(mTwitterFriendAdapter!=null)
//                {
//                    mBtnTwitter.setVisibility(View.GONE);
//                    mTwitter_list.setVisibility(View.VISIBLE);
//                }
//                else
//                {
//                    if(getConfig(Constants.SUMBIT_TWITTER,0)==0)
//                    {
//                        mBtnTwitter.setVisibility(View.VISIBLE);
//                        mTwitter_list.setVisibility(View.GONE);
//                    }
//                    else
//                    {
//                        mBtnTwitter.setVisibility(View.GONE);
//                        mTwitter_list.setVisibility(View.VISIBLE);
//                    }
//                }
            }
        });

//        mBtnPhone.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                mBtnPhone.setVisibility(View.GONE);
//                mPhone_list.setVisibility(View.VISIBLE);
//
//                mProgress.setVisibility(View.VISIBLE);
//
//                Cursor c = mCtx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
//
//                mData = new ArrayList<HashMap<String, String>>();
//                prepareData(c);
//
//                final JSONArray mJSONArray = new JSONArray();
//
//                for(int i = 0 ; i < mData.size() ; i++)
//                {
//                    String num = "";
//                    if(mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(0,1).contains("0")) num = mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).substring(1,mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER).length());
//                    else num  = mData.get(i).get(ContactsContract.CommonDataKinds.Phone.NUMBER);
//                    mJSONArray.put(num);
//                }
//
//                String phone = getConfig("phone","");
//
//                ApiManager.uploadPhoneNumbers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), phone, mJSONArray, new ApiManager.UploadPhoneNumbersCallback() {
//                    @Override
//                    public void onResult(boolean success, String message) {
//                        mProgress.setVisibility(View.GONE);
//
//                        if (success) {
//                            ApiManager.findFriends(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mJSONArray.toString(), "contacts", new ApiManager.FindFriendsCallback() {
//                                @Override
//                                public void onResult(boolean success, ArrayList<UserModel> model) {
//                                    if (success && model != null) {
//                                        if (model.size() != 0) {
//                                            mPhoneModel.clear();
//                                            mPhoneModel.addAll(model);
//
//                                            mPhoneFriendAdapter = new PhoneFriendAdapter();
//                                            mPhone_list.setAdapter(mPhoneFriendAdapter);
//                                        } else {
//                                            mNoData.setVisibility(View.VISIBLE);
//                                            Toast.makeText(mCtx, "無朋友", Toast.LENGTH_SHORT).show();
//                                        }
//                                    } else {
//                                        mNoData.setVisibility(View.VISIBLE);
//                                        Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });
//                        } else {
//                            mNoData.setVisibility(View.VISIBLE);
//                            Toast.makeText(mCtx, "電話搜尋失敗", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//            }
//        });

        mBtnFacebook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setConfig(Constants.SUMBIT_FACEBOOK,1);
                mBtnFacebook.setVisibility(View.GONE);
                mFacebook_list.setVisibility(View.VISIBLE);

                loadFbFriend();
            }
        });

        mBtnIg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                setConfig(Constants.SUMBIT_IG, 1);
//                mBtnIg.setVisibility(View.GONE);
//                mIg_list.setVisibility(View.VISIBLE);

                if(mInstagramDialog!=null) mInstagramDialog = null;
                mInstagramDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                mInstagramDialog.setContentView(R.layout.instagram_dialog);
                WebView mWeb = (WebView) mInstagramDialog.findViewById(R.id.web);
                final ProgressBar mProgress = (ProgressBar) mInstagramDialog.findViewById(R.id.progress);
                mWeb.getSettings().setJavaScriptEnabled(true);
                mInstagramDialog.setCancelable(true);
                mWeb.loadUrl("https://instagram.com/oauth/authorize/?client_id=" + Constants.IG_CLIENT_ID + "&redirect_uri=" + Constants.IG_REDIRECT_URI + "&response_type=token");
                mWeb.setWebViewClient(new WebViewClient()
                {
                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon)
                    {
                        super.onPageStarted(view, url, favicon);
                        mProgress.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onPageFinished(WebView view, String url)
                    {
                        super.onPageFinished(view, url);

                        mProgress.setVisibility(View.GONE);

                        if(url.contains(Constants.IG_REDIRECT_URI) && url.contains("#access_token="))
                        {
                            int pos = url.indexOf("=") + 1;
                            final String token = url.substring(pos,url.length());

                            try
                            {
                                OkHttpClient mClient = new OkHttpClient();
                                Request request = new Request.Builder().url("https://api.instagram.com/v1/users/self?access_token=" + token).build();
                                mClient.newCall(request).enqueue(new com.squareup.okhttp.Callback()
                                {
                                    @Override
                                    public void onResponse(Response response) throws IOException
                                    {
                                        if (response.isSuccessful())
                                        {
                                            mInstagramDialog.dismiss();

                                            try
                                            {
                                                String mData = response.body().string();
                                                String id = new JSONObject(mData).getJSONObject("data").getString("id");
                                                final String NAME = new JSONObject(mData).getJSONObject("data").getString("full_name");
                                                OkHttpClient mClient = new OkHttpClient();
                                                Request request = new Request.Builder().url("https://api.instagram.com/v1/users/" + id + "/follows" + "/?access_token=" + token).build();
                                                mClient.newCall(request).enqueue(new com.squareup.okhttp.Callback()
                                                {
                                                    @Override
                                                    public void onResponse(Response response) throws IOException
                                                    {
                                                        if (response.isSuccessful())
                                                        {
                                                            try
                                                            {
                                                                JSONArray data = new JSONObject(response.body().string()).getJSONArray("data");
                                                                final JSONArray mJSONArray = new JSONArray();
                                                                final JSONArray mFindJSONArray = new JSONArray();
                                                                for(int i = 0 ; i < data.length() ; i++)
                                                                {
                                                                    JSONObject mObject = new JSONObject();
                                                                    mObject.put("id",data.getJSONObject(i).getString("id"));
                                                                    mObject.put("name",data.getJSONObject(i).getString("username"));
                                                                    mJSONArray.put(mObject);
                                                                    mFindJSONArray.put(data.getJSONObject(i).getString("id"));
                                                                }

                                                                Log.d("123","mJSONArray : " + mJSONArray.toString());
                                                                Log.d("123", "mFindJSONArray : " + mFindJSONArray.toString());

                                                                ApiManager.uploadInstagramFriends(mCtx, NAME, mJSONArray, new ApiManager.UploadFacebookFriendsCallback() {
                                                                    @Override
                                                                    public void onResult(boolean success, String message) {
                                                                        Log.d("123", "success : " + success);
                                                                        if (success) {

                                                                        } else {
                                                                            mNoData.setVisibility(View.VISIBLE);
//                                                                                    Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            catch (Exception e)
                                                            {

                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Request request, IOException e)
                                                    {

                                                    }
                                                });
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Request request, IOException e)
                                    {

                                    }
                                });
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                });

                mInstagramDialog.show();
            }
        });

//        mBtnTwitter.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
////                setConfig(Constants.SUMBIT_TWITTER, 1);
////                mBtnTwitter.setVisibility(View.GONE);
////                mTwitter_list.setVisibility(View.VISIBLE);
//            }
//        });

//        mBtnTwitter.setCallback(new Callback<TwitterSession>()
//        {
//            @Override
//            public void success(Result<TwitterSession> result)
//            {
//                final String token = result.data.getAuthToken().token;
//                final String secret = result.data.getAuthToken().secret;
//                long UserId = result.data.getUserId();
//
//                TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
//                StatusesService statusesService = twitterApiClient.getStatusesService();
//                statusesService.show(UserId, null, null, null, new Callback<Tweet>()
//                {
//                    @Override
//                    public void success(Result<Tweet> result)
//                    {
////                        String Email = result.data.user.email;
////                        String Pic = result.data.user.profileImageUrl;
////                        String ScreenName = result.data.user.screenName;
//
//                        new Thread(new Runnable()
//                        {
//                            @Override
//                            public void run()
//                            {
//                                try
//                                {
//                                    ConfigurationBuilder builder = new ConfigurationBuilder();
//                                    builder.setOAuthConsumerKey(Constants.TWITTER_KEY);
//                                    builder.setOAuthConsumerSecret(Constants.TWITTER_SECRET);
//
//                                    AccessToken accessToken = new AccessToken(token, secret);
//                                    Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);
//
//                                    long lCursor = -1;
//                                    IDs friendsIDs = twitter.friendsFollowers().getFriendsIDs(lCursor);
//                                    List<User> users = twitter.friendsFollowers().getFriendsList(twitter.getId(), lCursor,friendsIDs.getIDs().length);
//
//                                    final JSONArray mJSONArray = new JSONArray();
//                                    final JSONArray mFindJSONArray = new JSONArray();
//                                    for(int i = 0 ; i < users.size() ; i++)
//                                    {
//                                        JSONObject mObject = new JSONObject();
//                                        mObject.put("id",users.get(i).getId());
//                                        mObject.put("name",users.get(i).getName());
//                                        mJSONArray.put(mObject);
//                                        mFindJSONArray.put(users.get(i).getId());
//                                    }
//                                    Log.d("123","mJSONArray : " + mJSONArray.toString());
//                                    Log.d("123","mFindJSONArray : " + mFindJSONArray.toString());
//                                }
//                                catch (Exception e)
//                                {
//                                }
//                            }
//                        }).start();
//                    }
//
//                    public void failure(TwitterException exception)
//                    {
//                    }
//                });
//            }
//
//            @Override
//            public void failure(TwitterException exception)
//            {
//            }
//        });


        hideKeyboard();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        mProgress.setVisibility(View.VISIBLE);
        ApiManager.getHotUsers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, new ApiManager.GetHotUsersCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<UserModel> userModel)
            {
                mProgress.setVisibility(View.GONE);

                if (success && userModel != null)
                {
                    if (userModel.size() != 0)
                    {
                        mNoData.setVisibility(View.GONE);

                        mUserModel.clear();
                        mUserModel.addAll(userModel);

                        mUserAdapter = new UserAdapter();
                        mSearch_list.setAdapter(mUserAdapter);
                        mSearch_list.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    mNoData.setVisibility(View.VISIBLE);
                    try{
//                        showToast(getString(R.string.search_failed));
                        Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                    }
                }
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }, 200);
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.find_friend));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.add_invite));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                shareTo(getString(R.string.share_subject), getString(R.string.share_body1) + Singleton.preferences.getString(Constants.OPEN_ID, "") + getString(R.string.share_body2) + " " + Constants.SHARE_LINK_APP, getString(R.string.share_title));
            }
        });
    }

    private void shareTo(String subject, String body, String chooserTitle) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    private void prepareData(Cursor c)
    {
        c.moveToFirst();
        while (!c.isAfterLast())
        {
            HashMap hm = new HashMap();
            hm.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
            hm.put(ContactsContract.CommonDataKinds.Phone.NUMBER, c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            hm.put(ContactsContract.CommonDataKinds.Phone.PHOTO_URI, c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
            mData.add(hm);
            c.moveToNext();
        }
    }

    private void loadFbFriend()
    {
        FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        final String token = loginResult.getAccessToken().getToken();

                        GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject user, GraphResponse response) {
                                if (user != null) {
                                    try {
                                        JSONObject params = new JSONObject();
                                        params.put("facebookID", user.optString("id"));
                                        params.put("facebookAccessToken", token);

                                        final String NAME = user.optString("name");

                                        ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {
                                                    GraphRequest.newMyFriendsRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONArrayCallback() {
                                                        @Override
                                                        public void onCompleted(JSONArray users, GraphResponse graphResponse) {
                                                            try {
                                                                final JSONArray mJSONArray = new JSONArray();
                                                                final JSONArray mFindJSONArray = new JSONArray();
                                                                for (int i = 0; i < users.length(); i++)
                                                                {
                                                                    JSONObject mObject = new JSONObject();
                                                                    mObject.put("id",users.getJSONObject(i).getString("id"));
                                                                    mObject.put("name",users.getJSONObject(i).getString("name"));
                                                                    mJSONArray.put(mObject);
                                                                    mFindJSONArray.put(users.getJSONObject(i).getString("id"));
                                                                }

                                                                ApiManager.uploadFacebookFriends(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), NAME, mJSONArray, new ApiManager.UploadFacebookFriendsCallback() {
                                                                    @Override
                                                                    public void onResult(boolean success, String message) {
                                                                        if (success) {
                                                                            ApiManager.findFriends(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mFindJSONArray.toString(), "facebook", new ApiManager.FindFriendsCallback() {
                                                                                @Override
                                                                                public void onResult(boolean success, ArrayList<UserModel> model) {
                                                                                    if (success && model != null) {
                                                                                        if (model.size() != 0) {
                                                                                            mFacebookModel.clear();
                                                                                            mFacebookModel.addAll(model);

                                                                                            mFacebookFriendAdapter = new FacebookFriendAdapter();
                                                                                            mFacebook_list.setAdapter(mFacebookFriendAdapter);
                                                                                        } else {
                                                                                            mNoData.setVisibility(View.VISIBLE);
//                                                                                                    Toast.makeText(mCtx, "無朋友", Toast.LENGTH_SHORT).show();
                                                                                        }
                                                                                    } else {
                                                                                        mNoData.setVisibility(View.VISIBLE);
//                                                                                                Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                                                    }
                                                                                }
                                                                            });
                                                                        } else {
                                                                            mNoData.setVisibility(View.VISIBLE);
//                                                                                    Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                });
                                                            } catch (JSONException e) {
                                                                mNoData.setVisibility(View.VISIBLE);
//                                                                        Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    }).executeAsync();
                                                } else {
                                                    mNoData.setVisibility(View.VISIBLE);
//                                                            Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                        mNoData.setVisibility(View.VISIBLE);
//                                                Toast.makeText(mCtx, "朋友搜尋失敗", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }).executeAsync();
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {

                    }
                });
    }

    public void setConfig(String key, int value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putInt(key, value);
        PE.commit();
    }

    public int getConfig(String key, int def)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getInt(key, def);
    }

    public String getConfig(String key , String def)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, def);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if(callbackManager!=null) callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {

        }

//        mBtnTwitter.onActivityResult(requestCode, resultCode, data);
    }

    public void hideKeyboard()
    {
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard()
    {
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private class PhoneFriendAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mPhoneModel.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.follow_friend_row, null);
                holder.img = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.follow = (Button) convertView.findViewById(R.id.follow);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            final int pos = position;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mPhoneModel.get(position).getPicture()), holder.img, SelfOptions);

            holder.img.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    //event tracking
                    try {
                        LogEventUtil.SearchContactUserAndClick(mCtx, mApplication, Singleton.preferences.getString(Constants.USER_ID, ""), mPhoneModel.get(pos).getBio());
                    }
                    catch (Exception x)
                    {

                    }

                    if (mPhoneModel.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mPhoneModel.get(pos).getName());
                        intent.putExtra("picture", mPhoneModel.get(pos).getPicture());
                        intent.putExtra("isfollowing", mPhoneModel.get(pos).getIsFollowing());
                        intent.putExtra("post", mPhoneModel.get(pos).getPostCount());
                        intent.putExtra("follow", mPhoneModel.get(pos).getFollowerCount());
                        intent.putExtra("following", mPhoneModel.get(pos).getFollowingCount());
                        intent.putExtra("open", mPhoneModel.get(pos).getOpenID());
                        intent.putExtra("bio", mPhoneModel.get(pos).getBio());
                        intent.putExtra("targetUserID", mPhoneModel.get(pos).getUserID());
                        intent.putExtra("web", mPhoneModel.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setText(mPhoneModel.get(position).getOpenID());
            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    //event tracking
                    try {
                        LogEventUtil.SearchContactUserAndClick(mCtx, mApplication, Singleton.preferences.getString(Constants.USER_ID, ""), mPhoneModel.get(pos).getBio());
                    }
                    catch (Exception x)
                    {

                    }

                    if (mPhoneModel.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mPhoneModel.get(pos).getName());
                        intent.putExtra("picture", mPhoneModel.get(pos).getPicture());
                        intent.putExtra("isfollowing", mPhoneModel.get(pos).getIsFollowing());
                        intent.putExtra("post", mPhoneModel.get(pos).getPostCount());
                        intent.putExtra("follow", mPhoneModel.get(pos).getFollowerCount());
                        intent.putExtra("following", mPhoneModel.get(pos).getFollowingCount());
                        intent.putExtra("open", mPhoneModel.get(pos).getOpenID());
                        intent.putExtra("bio", mPhoneModel.get(pos).getBio());
                        intent.putExtra("targetUserID", mPhoneModel.get(pos).getUserID());
                        intent.putExtra("web", mPhoneModel.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.dio.setText(mPhoneModel.get(position).getName());

            if(mPhoneModel.get(position).getIsFollowing()==1)
            {
                holder.follow.setText(getString(R.string.user_profile_following));
                holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
                holder.follow.setTextColor(Color.WHITE);
            }
            else
            {
                if(mPhoneModel.get(position).getFollowRequestTime()!=0)
                {
                    holder.follow.setText(getString(R.string.private_mode_request_send));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    holder.follow.setText("+ " + getString(R.string.user_profile_follow));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
            }

            final Button btn = holder.follow;
            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mPhoneModel.get(pos).getIsFollowing()==1)
                    {
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mPhoneModel.get(pos).getUserID());
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mPhoneModel.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if (success)
                                {
                                    mPhoneModel.get(pos).setIsFollowing(0);
                                    btn.setText("+ " + getString(R.string.user_profile_follow));
                                    btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                }
                                else
                                {
                                    try{
//                                        showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception e){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mPhoneModel.get(pos).getFollowRequestTime()!=0)
                        {
                            btn.setText("+ " + getString(R.string.user_profile_follow));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                            mPhoneModel.get(pos).setIsFollowing(0);
                            mPhoneModel.get(pos).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mPhoneModel.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                                    showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }
                                return ;
                            }

                            if(mPhoneModel.get(pos).getPrivacyMode().compareTo("private")==0)
                            {
                                btn.setText(getString(R.string.private_mode_request_send));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                mPhoneModel.get(pos).setIsFollowing(0);
                                mPhoneModel.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mPhoneModel.get(pos).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mPhoneModel.get(pos).setIsFollowing(0);
                                            mPhoneModel.get(pos).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                //event tracking
                                try {
                                    LogEventUtil.FollowUser(mCtx,mApplication,mPhoneModel.get(pos).getUserID());
                                    LogEventUtil.FollowContactFriend(mCtx,mApplication);
                                }
                                catch (Exception x)
                                {

                                }


                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mPhoneModel.get(pos).getUserID(), new ApiManager.FollowUserActionCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            mPhoneModel.get(pos).setIsFollowing(1);
                                            btn.setText(getString(R.string.user_profile_following));
                                            btn.setBackgroundResource(R.drawable.btn_green_selector);
                                            btn.setTextColor(Color.WHITE);
                                        }
                                        else
                                        {
                                            try{
//                                                showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception e){
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            if(mPhoneModel.get(pos).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView img;
        TextView name;
        TextView dio;
        Button follow;
        ImageView verifie;
    }

    private class FacebookFriendAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFacebookModel.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            FacebookViewHolder holder = new FacebookViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.follow_friend_row, null);
                holder.img = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.follow = (Button) convertView.findViewById(R.id.follow);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else holder = (FacebookViewHolder) convertView.getTag();

            final int pos = position;

            //http://story17.machipopo.com/THUMBNAIL_744FE2D7-9317-41D2-AC2C-5D276E075F8F.jpg
            //Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModel.get(position).getPicture())
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFacebookModel.get(position).getPicture()), holder.img,SelfOptions);
            holder.img.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    //event tracking
                    try {
                        LogEventUtil.SearchFBUserAndClick(mCtx, mApplication, Singleton.preferences.getString(Constants.USER_ID, ""), mPhoneModel.get(pos).getBio());
                    }
                    catch (Exception x)
                    {

                    }

                    if (mFacebookModel.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFacebookModel.get(pos).getName());
                        intent.putExtra("picture", mFacebookModel.get(pos).getPicture());
                        intent.putExtra("isfollowing", mFacebookModel.get(pos).getIsFollowing());
                        intent.putExtra("post", mFacebookModel.get(pos).getPostCount());
                        intent.putExtra("follow", mFacebookModel.get(pos).getFollowerCount());
                        intent.putExtra("following", mFacebookModel.get(pos).getFollowingCount());
                        intent.putExtra("open", mFacebookModel.get(pos).getOpenID());
                        intent.putExtra("bio", mFacebookModel.get(pos).getBio());
                        intent.putExtra("targetUserID", mFacebookModel.get(pos).getUserID());
                        intent.putExtra("web", mFacebookModel.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setText(mFacebookModel.get(position).getOpenID());
            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    //event tracking
                    try {
                        LogEventUtil.SearchFBUserAndClick(mCtx, mApplication, Singleton.preferences.getString(Constants.USER_ID, ""), mPhoneModel.get(pos).getBio());
                    }
                    catch (Exception x)
                    {

                    }

                    if (mFacebookModel.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFacebookModel.get(pos).getName());
                        intent.putExtra("picture", mFacebookModel.get(pos).getPicture());
                        intent.putExtra("isfollowing", mFacebookModel.get(pos).getIsFollowing());
                        intent.putExtra("post", mFacebookModel.get(pos).getPostCount());
                        intent.putExtra("follow", mFacebookModel.get(pos).getFollowerCount());
                        intent.putExtra("following", mFacebookModel.get(pos).getFollowingCount());
                        intent.putExtra("open", mFacebookModel.get(pos).getOpenID());
                        intent.putExtra("bio", mFacebookModel.get(pos).getBio());
                        intent.putExtra("targetUserID", mFacebookModel.get(pos).getUserID());
                        intent.putExtra("web", mFacebookModel.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.dio.setText(mFacebookModel.get(position).getName());

            if(mFacebookModel.get(pos).getIsFollowing()==1)
            {
                holder.follow.setText(getString(R.string.user_profile_following));
                holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
                holder.follow.setTextColor(Color.WHITE);
            }
            else
            {
                if(mFacebookModel.get(pos).getFollowRequestTime()!=0)
                {
                    holder.follow.setText(getString(R.string.private_mode_request_send));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    holder.follow.setText("+ " + getString(R.string.user_profile_follow));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
            }

            final Button btn = holder.follow;
            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mFacebookModel.get(pos).getIsFollowing()==1)
                    {
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mFacebookModel.get(pos).getUserID());
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mFacebookModel.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if (success)
                                {
                                    mFacebookModel.get(pos).setIsFollowing(0);
                                    btn.setText("+ " + getString(R.string.user_profile_follow));
                                    btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                }
                                else
                                {
                                    try{
//                                        showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception e){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mFacebookModel.get(pos).getFollowRequestTime()!=0)
                        {
                            btn.setText("+ " + getString(R.string.user_profile_follow));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                            mFacebookModel.get(pos).setIsFollowing(0);
                            mFacebookModel.get(pos).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mFacebookModel.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                                    showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }
                                return ;
                            }

                            if(mFacebookModel.get(pos).getPrivacyMode().compareTo("private")==0)
                            {
                                btn.setText(getString(R.string.private_mode_request_send));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                mFacebookModel.get(pos).setIsFollowing(0);
                                mFacebookModel.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mFacebookModel.get(pos).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mFacebookModel.get(pos).setIsFollowing(0);
                                            mFacebookModel.get(pos).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                try
                                {
                                    LogEventUtil.FollowUser(mCtx,mApplication,mFacebookModel.get(pos).getUserID());
                                    LogEventUtil.FollowFBFriend(mCtx,mApplication);
                                }
                                catch (Exception X)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mFacebookModel.get(pos).getUserID(), new ApiManager.FollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {
                                            mFacebookModel.get(pos).setIsFollowing(1);
                                            btn.setText(getString(R.string.user_profile_following));
                                            btn.setBackgroundResource(R.drawable.btn_green_selector);
                                            btn.setTextColor(Color.WHITE);
                                        } else {
                                            try {
//                                                showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            if(mFacebookModel.get(pos).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            return convertView;
        }
    }

    private class FacebookViewHolder
    {
        ImageView img;
        TextView name;
        TextView dio;
        Button follow;
        ImageView verifie;
    }

    private class UserAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mUserModel.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            UserViewHolder holder = new UserViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.follow_friend_row, null);
                holder.img = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.follow = (Button) convertView.findViewById(R.id.follow);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else holder = (UserViewHolder) convertView.getTag();

            final int pos = position;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUserModel.get(position).getPicture()), holder.img, SelfOptions);
            holder.img.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    //Umeng Monitor
                    String Umeng_id="SearchUserAndClick";
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                    //event tracking
                    try {
                        LogEventUtil.SearchUserAndClick_FriendPage(mCtx, mApplication, Singleton.preferences.getString(Constants.USER_ID, ""), query, mUserModel.get(pos).getUserID());
                    }
                    catch (Exception x)
                    {

                    }

                    if (mUserModel.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mUserModel.get(pos).getName());
                        intent.putExtra("picture", mUserModel.get(pos).getPicture());
                        intent.putExtra("isfollowing", mUserModel.get(pos).getIsFollowing());
                        intent.putExtra("post", mUserModel.get(pos).getPostCount());
                        intent.putExtra("follow", mUserModel.get(pos).getFollowerCount());
                        intent.putExtra("following", mUserModel.get(pos).getFollowingCount());
                        intent.putExtra("open", mUserModel.get(pos).getOpenID());
                        intent.putExtra("bio", mUserModel.get(pos).getBio());
                        intent.putExtra("targetUserID", mUserModel.get(pos).getUserID());
                        intent.putExtra("web", mUserModel.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setText(mUserModel.get(position).getOpenID());
            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    //Umeng Monitor
                    String Umeng_id="SearchUserAndClick";
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                    //event tracking
                    try
                    {
                        LogEventUtil.SearchUserAndClick_FriendPage(mCtx, mApplication, Singleton.preferences.getString(Constants.USER_ID, ""), query, mUserModel.get(pos).getUserID());
                    }
                    catch (Exception x)
                    {

                    }

                    if(mUserModel.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mUserModel.get(pos).getName());
                        intent.putExtra("picture", mUserModel.get(pos).getPicture());
                        intent.putExtra("isfollowing", mUserModel.get(pos).getIsFollowing());
                        intent.putExtra("post", mUserModel.get(pos).getPostCount());
                        intent.putExtra("follow", mUserModel.get(pos).getFollowerCount());
                        intent.putExtra("following", mUserModel.get(pos).getFollowingCount());
                        intent.putExtra("open", mUserModel.get(pos).getOpenID());
                        intent.putExtra("bio", mUserModel.get(pos).getBio());
                        intent.putExtra("targetUserID", mUserModel.get(pos).getUserID());
                        intent.putExtra("web", mUserModel.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.dio.setText(mUserModel.get(position).getName());

            if(mUserModel.get(pos).getIsFollowing()==1)
            {
                holder.follow.setText(getString(R.string.user_profile_following));
                holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
                holder.follow.setTextColor(Color.WHITE);
            }
            else
            {
                if(mUserModel.get(pos).getFollowRequestTime()!=0)
                {
                    holder.follow.setText(getString(R.string.private_mode_request_send));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    holder.follow.setText("+ " + getString(R.string.user_profile_follow));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
            }

            final Button btn = holder.follow;
            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mUserModel.get(pos).getIsFollowing()==1)
                    {
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mUserModel.get(pos).getUserID());
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mUserModel.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if (success)
                                {
                                    mUserModel.get(pos).setIsFollowing(0);
                                    btn.setText("+ " + getString(R.string.user_profile_follow));
                                    btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                }
                                else
                                {
                                    try{
//                                        showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception e){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mUserModel.get(pos).getFollowRequestTime()!=0)
                        {
                            btn.setText("+ " + getString(R.string.user_profile_follow));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                            mUserModel.get(pos).setIsFollowing(0);
                            mUserModel.get(pos).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mUserModel.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                                    showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }
                                return ;
                            }

                            if(mUserModel.get(pos).getPrivacyMode().compareTo("private")==0)
                            {
                                btn.setText(getString(R.string.private_mode_request_send));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                mUserModel.get(pos).setIsFollowing(0);
                                mUserModel.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mUserModel.get(pos).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mUserModel.get(pos).setIsFollowing(0);
                                            mUserModel.get(pos).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                try {
                                    LogEventUtil.FollowUser(mCtx, mApplication, mUserModel.get(pos).getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mUserModel.get(pos).getUserID(), new ApiManager.FollowUserActionCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            try{
                                                if(mUserModel != null && mUserModel.size() > pos){
                                                    mUserModel.get(pos).setIsFollowing(1);
                                                    btn.setText(getString(R.string.user_profile_following));
                                                    btn.setBackgroundResource(R.drawable.btn_green_selector);
                                                    btn.setTextColor(Color.WHITE);
                                                }
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }
                                        else
                                        {
                                            try{
//                                                showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception e){
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            if(mUserModel.get(pos).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            return convertView;
        }
    }

    private class UserViewHolder
    {
        ImageView img;
        TextView name;
        TextView dio;
        Button follow;
        ImageView verifie;
    }
}
