package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.machipopo.media17.utils.LogEventUtil;
import com.machipopo.media17.utils.ShareUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tencent.connect.UserInfo;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;
import com.warnyul.android.widget.FastVideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by POPO on 6/9/15.
 */
public class PhotoActivity extends BaseActivity
{
    private PhotoActivity mCtx = this;
    private Story17Application mApplication;
    private LinearLayout list_layout;
    private ImageView self;
    private TextView name;
    private TextView day;
    private TextView view_text;
    private TextView money_text;

    private ImageView photo;
    private FeedTagTextView dio;
    private TextView like_text;
    private TextView comment_text;
    private ImageView btn_like;
    private ImageView btn_comment;
    private ImageView btn_more;

    private Button mBtnFollow;

    private float mY = 0 ,mX = 0;

    private String mPhoto,mOpen,mPicture,mDay,mDio,mUserId,mPostId,mWeb = "",type="image",video="";
    private int mView,mLikecount,mCommentcount,mLikeed;
    private float mMoney;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};

    private int bears[] = {R.drawable.bear_1,R.drawable.bear_2,R.drawable.bear_3,R.drawable.bear_4,R.drawable.bear_5,R.drawable.bear_6,R.drawable.bear_7,R.drawable.bear_8};

    private int bubbles[] = {R.drawable.bubble_1,R.drawable.bubble_2,R.drawable.bubble_3,R.drawable.bubble_4,R.drawable.bubble_5,R.drawable.bubble_6,R.drawable.bubble_7,R.drawable.bubble_8,R.drawable.bubble_9,R.drawable.bubble_10};

    private int cats[] = {R.drawable.cat_1,R.drawable.cat_2,R.drawable.cat_3,R.drawable.cat_4,R.drawable.cat_5,R.drawable.cat_6,R.drawable.cat_7,R.drawable.cat_8};

    private int rabbits[] = {R.drawable.rabbit_1,R.drawable.rabbit_2,R.drawable.rabbit_3,R.drawable.rabbit_4,R.drawable.rabbit_5,R.drawable.rabbit_6,R.drawable.rabbit_7,R.drawable.rabbit_8};

    private int logos[] = {R.drawable.logo_1,R.drawable.logo_2,R.drawable.logo_3,R.drawable.logo_4,R.drawable.logo_5,R.drawable.logo_6,R.drawable.logo_7,R.drawable.logo_8};

    private RelativeLayout mBear;

    private DisplayImageOptions BigOptions123, SelfOptions123;
    private DisplayMetrics mDisplayMetrics;

    private Boolean mGoTO = false;

    private String mName;
    private int mIsFollowing,mPostCount,mFollowerCount,mFollowingCount;

    private FastVideoView mVideo;
    private ImageView mVcon;
    private ImageView mDe,mDeMe;

    private int mLikeCount = 0;
    private Timer mTimer;
    private int lastCount = 0;
    private Boolean mGodHand = false;

    private ImageView mVerifie;
    private String[] mListMoreText;
    private String PostID = "";
    private ProgressBar mProgress;
    private ImageView mNoData;
    private PullToRefreshScrollView mScrollLayout;

    private Boolean mGuest = false;

    //event tracking
    private int startPhotoPage = 0;
    private boolean fromDiscovevrPage = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) mCtx.getApplication();
        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        initTitleBar();

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("post_id")) PostID = mBundle.getString("post_id");

            if(mBundle.containsKey("photo")) mPhoto = mBundle.getString("photo");
            if(mBundle.containsKey("open")) mOpen = mBundle.getString("open");
            if(mBundle.containsKey("picture")) mPicture = mBundle.getString("picture");
            if(mBundle.containsKey("day")) mDay = mBundle.getString("day");
            if(mBundle.containsKey("view")) mView = mBundle.getInt("view");
            if(mBundle.containsKey("money")) mMoney = mBundle.getFloat("money");
            if(mBundle.containsKey("dio")) mDio = mBundle.getString("dio");
            if(mBundle.containsKey("likecount")) mLikecount = mBundle.getInt("likecount");
            if(mBundle.containsKey("commentcount")) mCommentcount = mBundle.getInt("commentcount");
            if(mBundle.containsKey("likeed")) mLikeed = mBundle.getInt("likeed");
            if(mBundle.containsKey("postid")) mPostId = mBundle.getString("postid");
            if(mBundle.containsKey("userid")) mUserId = mBundle.getString("userid");
            if(mBundle.containsKey("goto")) mGoTO = mBundle.getBoolean("goto");

            if(mBundle.containsKey("name")) mName = mBundle.getString("name");
            if(mBundle.containsKey("isFollowing")) mIsFollowing = mBundle.getInt("isFollowing");
            if(mBundle.containsKey("postCount")) mPostCount = mBundle.getInt("postCount");
            if(mBundle.containsKey("followerCount")) mFollowerCount = mBundle.getInt("followerCount");
            if(mBundle.containsKey("followingCount")) mFollowingCount = mBundle.getInt("followingCount");
            if(mBundle.containsKey("web")) mWeb = mBundle.getString("web");

            if(mBundle.containsKey("type")) type = mBundle.getString("type");
            if(mBundle.containsKey("video")) video = mBundle.getString("video");

            if(mBundle.containsKey("guest"))
            {
                mGuest = mBundle.getBoolean("guest");
            }
        }

        //event tracking
        startPhotoPage = Singleton.getCurrentTimestamp();
        try{
            if(mGoTO) {
                LogEventUtil.EnterPostPage(mCtx, mApplication, mUserId, mPostId,"discover Page");
            }
            else{
                LogEventUtil.EnterPostPage(mCtx, mApplication, mUserId, mPostId,"profile Page");
                fromDiscovevrPage = false;
            }
        }catch (Exception x)
        {

        }

        mBear = (RelativeLayout) findViewById(R.id.bear);
        mDe = (ImageView) findViewById(R.id.de);
        mDeMe = (ImageView) findViewById(R.id.deme);

        list_layout = (LinearLayout) findViewById(R.id.list_layout);
        self = (ImageView) findViewById(R.id.self);
        name = (TextView) findViewById(R.id.name);
        day = (TextView) findViewById(R.id.day);
        view_text = (TextView) findViewById(R.id.view_text);
        money_text = (TextView) findViewById(R.id.money_text);

        photo = (ImageView) findViewById(R.id.photo);
        dio = (FeedTagTextView) findViewById(R.id.dio);
        like_text = (TextView) findViewById(R.id.like_text);
        comment_text = (TextView) findViewById(R.id.comment_text);

        btn_like = (ImageView) findViewById(R.id.btn_like);
        btn_comment = (ImageView) findViewById(R.id.btn_comment);
        btn_more = (ImageView) findViewById(R.id.btn_more);

        mVideo = (FastVideoView) findViewById(R.id.video);
        mVcon = (ImageView) findViewById(R.id.vcon);

        mBtnFollow = (Button) findViewById(R.id.btn_follow);

        mVerifie = (ImageView) findViewById(R.id.verifie);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);
        mScrollLayout = (PullToRefreshScrollView) findViewById(R.id.scroll_layout);

        BigOptions123 = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SelfOptions123 = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        if(PostID.length()!=0)
        {
            mScrollLayout.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);

            ApiManager.getPostInfo(mCtx, PostID, new ApiManager.GetPostInfoCallback()
            {
                @Override
                public void onResult(boolean success, FeedModel feedModel)
                {
                    mProgress.setVisibility(View.GONE);
                    if (success && feedModel!=null)
                    {
                        mPhoto = feedModel.getPicture();
                        mOpen = feedModel.getUserInfo().getOpenID();
                        mPicture = feedModel.getUserInfo().getPicture();
                        mDay = Singleton.getElapsedTimeString(feedModel.getTimestamp());
                        mView = feedModel.getViewCount();
                        mMoney = feedModel.getTotalRevenue();
                        mDio = feedModel.getCaption();
                        mLikecount = feedModel.getLikeCount();
                        mCommentcount = feedModel.getCommentCount();
                        mLikeed = feedModel.getLiked();
                        mPostId = feedModel.getPostID();
                        mUserId = feedModel.getUserID();
                        if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) mGoTO = true;
                        else mGoTO = false;


                        mName = feedModel.getUserInfo().getName();
                        mIsFollowing = feedModel.getUserInfo().getIsFollowing();
                        mPostCount = feedModel.getUserInfo().getPostCount();
                        mFollowerCount = feedModel.getUserInfo().getFollowerCount();
                        mFollowingCount = feedModel.getUserInfo().getFollowingCount();
                        mWeb = feedModel.getUserInfo().getWebsite();

                        type = feedModel.getType();
                        video = feedModel.getVideo();

                        mScrollLayout.setVisibility(View.VISIBLE);
                        mNoData.setVisibility(View.GONE);
                        runPost();
                    }
                    else
                    {
                        try{
//                                                showToast(getString(R.error_failed.edit));
                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e){
                        }
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
            });
            return;
        }

        runPost();
    }

    private void runPost()
    {
        name.setText(mOpen);
        name.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (mGoTO)
                {
                    if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mName);
                        intent.putExtra("picture", mPicture);
                        intent.putExtra("isfollowing", mIsFollowing);
                        intent.putExtra("post", mPostCount);
                        intent.putExtra("follow", mFollowerCount);
                        intent.putExtra("following", mFollowingCount);
                        intent.putExtra("open", mOpen);
//                        intent.putExtra("bio", mDio);
                        intent.putExtra("targetUserID", mUserId);
                        intent.putExtra("web",mWeb);
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
                    }
                }
            }
        });

        day.setText(mDay);

        if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
        {
            money_text.setVisibility(View.VISIBLE);

            String c = Singleton.getCurrencyType();
            double m = (double) mMoney*(double)Singleton.getCurrencyRate();
            DecimalFormat df=new DecimalFormat("#.####");
            money_text.setText(df.format(m) + " " + c);
            view_text.setText(String.format(getString(R.string.home_views), String.valueOf(mView)));
        }
        else
        {
            money_text.setVisibility(View.GONE);
            view_text.setVisibility(View.GONE);
        }

        if(mDio.length()!=0)
        {
            dio.setVisibility(View.VISIBLE);
            dio.setText(mDio);
        }
        else dio.setVisibility(View.GONE);
        like_text.setText(String.format(getString(R.string.home_like),String.valueOf(mLikecount)));
        comment_text.setText(String.format(getString(R.string.home_comment),String.valueOf(mCommentcount)));

        if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
        {
            ApiManager.getUserInfo(mCtx, mOpen, new ApiManager.GetUserInfoCallback()
            {
                @Override
                public void onResult(boolean success, String message, final UserModel user)
                {
                    if (success && user != null)
                    {
                        if (user.getIsFollowing() == 0)
                        {
                            mBtnFollow.setVisibility(View.VISIBLE);
                            if(user.getFollowRequestTime()!=0)
                            {
                                mBtnFollow.setText(getString(R.string.private_mode_request_send));
                                mBtnFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                mBtnFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                            }
                            else
                            {
                                mBtnFollow.setText("+ " + getString(R.string.user_profile_follow));
                                mBtnFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                mBtnFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                            }
                        }
                        else
                        {
                            mBtnFollow.setVisibility(View.GONE);
                        }

                        mBtnFollow.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                if(mGuest)
                                {
                                    showGuestLogin();

                                    return;
                                }

                                if(user.getFollowRequestTime()!=0)
                                {
                                    mBtnFollow.setText("+ " + getString(R.string.user_profile_follow));
                                    mBtnFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    mBtnFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                                    user.setIsFollowing(0);
                                    user.setFollowRequestTime(0);
                                    ApiManager.cancelFollowRequests(mCtx, user.getUserID(), new ApiManager.RequestCallback() {
                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                                    {
                                        try{
//                                                showToast(getString(R.error_failed.follow_count_size));
                                            Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception e){
                                        }
                                        return ;
                                    }

                                    try{
                                        LogEventUtil.FollowUser(mCtx, mApplication, mUserId);

                                        if(fromDiscovevrPage)
                                        {
                                            LogEventUtil.FollowDiscoverUser(mCtx,mApplication,mUserId);
                                        }

                                    }
                                    catch (Exception x)
                                    {

                                    }


                                    if(user.getPrivacyMode().compareTo("private")==0)
                                    {
                                        mBtnFollow.setText(getString(R.string.private_mode_request_send));
                                        mBtnFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                        mBtnFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                                        user.setIsFollowing(0);
                                        user.setFollowRequestTime(Singleton.getCurrentTimestamp());
                                        ApiManager.sendFollowRequest(mCtx, user.getUserID(), new ApiManager.RequestCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success)
                                            {
                                                if (!success)
                                                {
                                                    mBtnFollow.setText("+ " + getString(R.string.user_profile_follow));
                                                    mBtnFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                                    mBtnFollow.setTextColor(getResources().getColor(R.color.content_text_color));
                                                    user.setIsFollowing(0);
                                                    user.setFollowRequestTime(0);
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mUserId, new ApiManager.FollowUserActionCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {
                                                    mApplication.setGripFollow(true);
                                                    mBtnFollow.setVisibility(View.GONE);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });

                        if(user.getIsVerified()==1) mVerifie.setVisibility(View.VISIBLE);
                        else mVerifie.setVisibility(View.GONE);
                    }
                }
            });
        }

        if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
        {
            like_text.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    Intent intent = new Intent();
                    intent.setClass(mCtx, PostLikerActivity.class);
                    intent.putExtra("post_id", mPostId);
                    intent.putExtra("user_id", mUserId);
                    startActivity(intent);
                }
            });
        }

        comment_text.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                Intent intent = new Intent();
                intent.setClass(mCtx, HomeCommentActivity.class);
                intent.putExtra("post_id", mPostId);
                intent.putExtra("user_id", mUserId);
                startActivity(intent);
            }
        });

        if(mLikeed==0)
        {
            btn_like.setImageResource(R.drawable.btn_like_selector);
        }
        else btn_like.setImageResource(R.drawable.like_down);

        final TextView likeText = like_text;
        final ImageView like = btn_like;

        if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
        {
            btn_like.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {

                    //event tracking
                    try{
                        LogEventUtil.LikePostSinglePost(mCtx, mApplication, mUserId, mPostId);
                    }catch (Exception x)
                    {

                    }

                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    Intent intent = new Intent();
                    intent.setClass(mCtx, PostLikerActivity.class);
                    intent.putExtra("post_id", mPostId);
                    intent.putExtra("user_id", mUserId);
                    startActivity(intent);
                }
            });
        }
        else
        {
            btn_like.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    //event tracking
                    try{
                        LogEventUtil.LikePostSinglePost(mCtx,mApplication,mUserId,mPostId);
                    }catch (Exception x)
                    {

                    }

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        mY = event.getY();
                        mX = event.getX();
                    }

                    if (event.getAction() == MotionEvent.ACTION_UP) {

                        if(mGuest)
                        {
                            showGuestLogin();

                            return true;
                        }

                        double num = Math.sqrt(Math.pow(((double) mX - (double) event.getX()), 2) + Math.pow(((double) mY - (double) event.getY()), 2));

                        if (num < 30) {
                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(mCtx);
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            if (mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                                mLikecount = mLikecount + 1;
                                likeText.setText(String.format(getString(R.string.home_like),String.valueOf(mLikecount)));
                                mLikeed = 1;
                                like.setImageResource(R.drawable.like_down);

                                mLikeCount++;
//                                ApiManager.likePost(mCtx, mPostId, new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            } else {
                                if (mLikeed != 1) {
                                    mLikecount = mLikecount + 1;
                                    likeText.setText(String.format(getString(R.string.home_like),String.valueOf(mLikecount)));
                                    mLikeed = 1;
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(mCtx, mPostId, new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }

                    return true;
                }
            });
        }

        btn_comment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                Intent intent = new Intent();
                intent.setClass(mCtx,HomeCommentActivity.class);
                intent.putExtra("post_id", mPostId);
                intent.putExtra("user_id", mUserId);
                startActivity(intent);
            }
        });

        if (mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
        {
            btn_more.setVisibility(View.VISIBLE);
            btn_more.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1)
                    {
                        if(mListMoreText!=null) mListMoreText = null;

                        mListMoreText = new String[3];
                        mListMoreText[0] = getString(R.string.post_delete);
                        mListMoreText[1] = getString(R.string.promote_post);
                        mListMoreText[2] = getString(R.string.post_share);

                        new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == 0)
                                {
                                    ApiManager.deletePost(mCtx, mPostId, new ApiManager.DeletePostCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {
//                                        Toast.makeText(mCtx, "刪除成功", Toast.LENGTH_SHORT).show();
                                                mCtx.finish();
                                            } else {
//                                        Toast.makeText(mCtx, "刪除失敗", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                                else if(which == 1)
                                {
                                    ApiManager.adminPromotePost(mCtx, mPostId, new ApiManager.RequestCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success)
                                        {
                                            if (success)
                                            {
                                                try{
//                                                showToast(getString(R.error_failed.done));
                                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                            else
                                            {
                                                try{
//                                                showToast(getString(R.error_failed.error_failed));
                                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                        }
                                    });
                                }
                                else if(which == 2)
                                {
                                    if(type.compareTo("image")==0)
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mPostId, R.string.share_post_image, mOpen, mDio, mPhoto), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mOpen);
                                                String mDescription = mTitle + "!" + mDio + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mPhoto);
                                            }
                                        });
                                    }
                                    else
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mPostId, R.string.share_post_video, mOpen, mDio, mPhoto), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mOpen);
                                                String mDescription = mTitle + "!" + mDio + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mPhoto);
                                            }
                                        });
                                    }
                                }
                            }
                        })
                        .show();
                    }
                    else
                    {
                        new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(new String[]{getString(R.string.post_delete),getString(R.string.post_share)}, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which==0)
                                {
                                    ApiManager.deletePost(mCtx, mPostId, new ApiManager.DeletePostCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {
                                                try{
//                                                showToast(getString(R.error_failed.done));
                                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                                mCtx.finish();
                                            } else {
                                                try{
//                                                showToast(getString(R.error_failed.error_failed));
                                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                        }
                                    });
                                }
                                else if(which==1)
                                {
                                    if(type.compareTo("image")==0)
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mPostId, R.string.share_post_image, mOpen, mDio, mPhoto), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mOpen);
                                                String mDescription = mTitle + "!" + mDio + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mPhoto);
                                            }
                                        });
                                    }
                                    else
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mPostId, R.string.share_post_video, mOpen, mDio, mPhoto), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mOpen);
                                                String mDescription = mTitle + "!" + mDio + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mPhoto);
                                            }
                                        });
                                    }
                                }
                            }
                        })
                        .show();
                    }
                }
            });
        }
        else
        {
            if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1)
            {
                btn_more.setVisibility(View.VISIBLE);
                btn_more.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(mGuest)
                        {
                            showGuestLogin();

                            return;
                        }

                        if(mListMoreText!=null) mListMoreText = null;

                        if(mLikeed==0)
                        {
                            mListMoreText = new String[3];
                            mListMoreText[0] = getString(R.string.post_hide);
                            mListMoreText[1] = getString(R.string.promote_post);
                            mListMoreText[2] = getString(R.string.post_share);
                        }
                        else
                        {
                            mListMoreText = new String[4];
                            mListMoreText[0] = getString(R.string.post_hide);
                            mListMoreText[1] = getString(R.string.promote_post);
                            mListMoreText[2] = getString(R.string.post_share);
                            mListMoreText[3] = getString(R.string.back_like);
                        }

                        new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(which == 0)
                                {
                                    ApiManager.hidePostAction(mCtx, mPostId, new ApiManager.RequestCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success)
                                        {
                                            if (success)
                                            {
                                                try{
//                                                showToast(getString(R.error_failed.done));
                                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                            else
                                            {
                                                try{
//                                                showToast(getString(R.error_failed.error_failed));
                                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                        }
                                    });
                                }
                                else if(which == 1)
                                {
                                    ApiManager.adminPromotePost(mCtx, mPostId, new ApiManager.RequestCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success)
                                        {
                                            if (success)
                                            {
                                                try{
//                                                showToast(getString(R.error_failed.done));
                                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                            else
                                            {
                                                try{
//                                                showToast(getString(R.error_failed.error_failed));
                                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                        }
                                    });
                                }
                                else if(which == 2)
                                {
                                    if(type.compareTo("image")==0)
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mPostId, R.string.share_post_image, mOpen, mDio, mPhoto), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mOpen);
                                                String mDescription = mTitle + "!" + mDio + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mPhoto);
                                            }
                                        });
                                    }
                                    else
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mPostId, R.string.share_post_video, mOpen, mDio, mPhoto), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mOpen);
                                                String mDescription = mTitle + "!" + mDio + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mPhoto);
                                            }
                                        });
                                    }
                                }
                                else if(which == 3)
                                {
                                    if(mLikeCount!=0)
                                    {
                                        if(mApplication!=null)
                                        {
                                            if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                                            {
                                                mApplication.sendLikeCountV2(mCtx,mPostId,mLikeCount,mGodHand);

                                                mLikeCount = 0;
                                            }
                                        }
                                    }

                                    mLikeed = 0;
                                    like.setImageResource(R.drawable.btn_like_selector);
                                    ApiManager.unlikePost(mCtx, mPostId, new ApiManager.UnlikePostCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message)
                                        {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                            }
                        })
                        .show();
                    }
                });
            }
            else
            {
                btn_more.setVisibility(View.VISIBLE);
                btn_more.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(mGuest)
                        {
                            showGuestLogin();

                            return;
                        }

                        if(mListMoreText!=null) mListMoreText = null;

                        if(mLikeed==0)
                        {
                            mListMoreText = new String[2];
                            mListMoreText[0] = getString(R.string.post_block);
                            mListMoreText[1] = getString(R.string.post_share);
                        }
                        else
                        {
                            mListMoreText = new String[3];
                            mListMoreText[0] = getString(R.string.post_block);
                            mListMoreText[1] = getString(R.string.post_share);
                            mListMoreText[2] = getString(R.string.back_like);
                        }

                        new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(which == 0)
                                {
                                    //檢舉
                                    ArrayList<String> strArray  = new ArrayList<String>();
                                    strArray =  Singleton.getLocalizeStringOfArray(Constants.REPORT_POST_OPTION);
                                    String[] reportString = strArray.toArray(new String[strArray.size()]);

                                    new AlertDialog.Builder(mCtx).setTitle(getString(R.string.report_reason)).setItems(reportString, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            String reason = Singleton.getLocalizedString(Constants.REPORT_POST_OPTION.get(which));
                                            try {
                                                LogEventUtil.ReportPost(mCtx, mApplication);
                                            }
                                            catch (Exception x)
                                            {

                                            }
                                            ApiManager.reportPostAction(mCtx,  mUserId, reason, mPostId, new ApiManager.ReportPostActionCallback() {

                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if(success){
                                                        try{
//                                                showToast(getString(R.error_failed.complete));
                                                            Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                                        }
                                                        catch (Exception e){
                                                        }
                                                    }else{
                                                        try{
//                                                showToast(getString(R.error_failed.failed));
                                                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                        }
                                                        catch (Exception e){
                                                        }
                                                    }
                                                }
                                            });


                                        }
                                    }).show();
                                }
                                else if(which == 1)
                                {
                                    if(type.compareTo("image")==0)
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mPostId, R.string.share_post_image, mOpen, mDio, mPhoto), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mOpen);
                                                String mDescription = mTitle + "!" + mDio + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mPhoto);
                                            }
                                        });
                                    }
                                    else
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mPostId, R.string.share_post_video, mOpen, mDio, mPhoto), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mOpen);
                                                String mDescription = mTitle + "!" + mDio + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mPhoto);
                                            }
                                        });
                                    }
                                }
                                else if(which == 2)
                                {
                                    if(mLikeCount!=0)
                                    {
                                        if(mApplication!=null)
                                        {
                                            if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                                            {
                                                mApplication.sendLikeCountV2(mCtx,mPostId,mLikeCount,mGodHand);

                                                mLikeCount = 0;
                                            }
                                        }
                                    }

                                    mLikeed = 0;
                                    like.setImageResource(R.drawable.btn_like_selector);
                                    ApiManager.unlikePost(mCtx, mPostId, new ApiManager.UnlikePostCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message)
                                        {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                            }
                        })
                        .show();
                    }
                });
            }
        }
        mDeMe.setImageResource(R.drawable.placehold_profile_s);
        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mPicture), self, SelfOptions123);
        self.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGoTO)
                {
                    if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        //event tracking
                        if(fromDiscovevrPage) {
                            try {
                                LogEventUtil.ClickDiscoverUser(mCtx, mApplication, mUserId);
                            } catch (Exception x) {

                            }
                        }

                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mName);
                        intent.putExtra("picture", mPicture);
                        intent.putExtra("isfollowing", mIsFollowing);
                        intent.putExtra("post", mPostCount);
                        intent.putExtra("follow", mFollowerCount);
                        intent.putExtra("following", mFollowingCount);
                        intent.putExtra("open", mOpen);
//                        intent.putExtra("bio", mDio);
                        intent.putExtra("targetUserID", mUserId);
                        intent.putExtra("web",mWeb);
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
                    }
                }
            }
        });

        mDe.getLayoutParams().width = mDisplayMetrics.widthPixels;
        mDe.getLayoutParams().height = mDisplayMetrics.widthPixels ;

        try
        {
            mDe.setImageResource(R.drawable.placehold_l);
        }
        catch (OutOfMemoryError e)
        {
        }
        catch(Exception f)
        {
        }

        if(type.compareTo("image")==0)
        {
            mVideo.setVisibility(View.GONE);
            mVcon.setVisibility(View.GONE);
            photo.setVisibility(View.VISIBLE);

            photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
            photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

            try
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mPhoto), photo,BigOptions123);
            }
            catch (OutOfMemoryError e)
            {
                photo.setImageResource(R.drawable.placehold_s);
            }
            catch(Exception f)
            {
                photo.setImageResource(R.drawable.placehold_s);
            }

            photo.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        mY = event.getY();
                        mX = event.getX();
                    }

                    if (event.getAction() == MotionEvent.ACTION_UP)
                    {
                        if(mGuest)
                        {
                            showGuestLogin();

                            return true;
                        }

                        double num = Math.sqrt(Math.pow(((double)mX - (double)event.getX()), 2) + Math.pow(((double)mY - (double)event.getY()), 2));

                        if(num<30)
                        {
                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(mCtx);
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            if (mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                                mLikecount = mLikecount + 1;
                                likeText.setText(String.format(getString(R.string.home_like),String.valueOf(mLikecount)));
                                mLikeed = 1;
                                like.setImageResource(R.drawable.like_down);

                                mLikeCount++;
//                                ApiManager.likePost(mCtx, mPostId, new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            } else {
                                if (mLikeed != 1) {
                                    mLikecount = mLikecount + 1;
                                    likeText.setText(String.format(getString(R.string.home_like),String.valueOf(mLikecount)));
                                    mLikeed = 1;
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(mCtx, mPostId, new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }

                    return true;
                }
            });
        }
        else
        {
            mVideo.setVisibility(View.VISIBLE);
            mVcon.setVisibility(View.VISIBLE);
            photo.setVisibility(View.VISIBLE);

            mVideo.getLayoutParams().width = mDisplayMetrics.widthPixels;
            mVideo.getLayoutParams().height = mDisplayMetrics.widthPixels ;
            mVideo.setVideoPath(Singleton.getS3FileUrl(video));

            photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
            photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

            try
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mPhoto), photo,BigOptions123);
            }
            catch (OutOfMemoryError e)
            {
                photo.setImageResource(R.drawable.placehold_l);
            }
            catch(Exception f)
            {
                photo.setImageResource(R.drawable.placehold_l);
            }

            mVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    photo.setVisibility(View.GONE);
                }
            });
            mVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                public void onCompletion(MediaPlayer mp)
                {
                    mVideo.start();
                }
            });

            mVideo.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        mY = event.getY();
                        mX = event.getX();
                    }

                    if (event.getAction() == MotionEvent.ACTION_UP)
                    {
                        if(mGuest)
                        {
                            showGuestLogin();

                            return true;
                        }

                        double num = Math.sqrt(Math.pow(((double)mX - (double)event.getX()), 2) + Math.pow(((double)mY - (double)event.getY()), 2));

                        if(num<30)
                        {
                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(mCtx);
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            if (mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                                mLikecount = mLikecount + 1;
                                likeText.setText(String.format(getString(R.string.home_like),String.valueOf(mLikecount)));
                                mLikeed = 1;
                                like.setImageResource(R.drawable.like_down);

                                mLikeCount++;
//                                ApiManager.likePost(mCtx, mPostId, new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            } else {
                                if (mLikeed != 1) {
                                    mLikecount = mLikecount + 1;
                                    likeText.setText(String.format(getString(R.string.home_like),String.valueOf(mLikecount)));
                                    mLikeed = 1;
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(mCtx, mPostId, new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }

                    return true;
                }
            });
        }

        dio.linkify(new FeedTagActionHandler()
        {
            @Override
            public void handleHashtag(String hashtag)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                Intent intent = new Intent();
                intent.setClass(mCtx, TagPostActivity.class);
                intent.putExtra("tag", hashtag);
                startActivity(intent);
            }

            @Override
            public void handleMention(String mention)
            {
                ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback()
                {
                    @Override
                    public void onResult(boolean success, String message, UserModel user)
                    {
                        if (success && user != null)
                        {
                            Intent intent = new Intent();
                            intent.setClass(mCtx, HomeUserActivity.class);
                            intent.putExtra("title", user.getName());
                            intent.putExtra("picture", user.getPicture());
                            intent.putExtra("isfollowing", user.getIsFollowing());
                            intent.putExtra("post", user.getPostCount());
                            intent.putExtra("follow", user.getFollowerCount());
                            intent.putExtra("following", user.getFollowingCount());
                            intent.putExtra("open", user.getOpenID());
                            intent.putExtra("bio", user.getBio());
                            intent.putExtra("targetUserID", user.getUserID());
                            intent.putExtra("web", user.getWebsite());
                            intent.putExtra("guest", mGuest);
                            startActivity(intent);
                        }
                    }
                });
            }

            @Override
            public void handleEmail(String email)
            {

            }

            @Override
            public void handleUrl(String url)
            {
                try
                {
                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                catch (Exception e)
                {
                    try{
//                                                showToast(getString(R.error_failed.open_uri_error));
                        Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });

        removeLine(dio);

        mTimer = new Timer();
        mTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                if(mLikeCount-lastCount > 17)
                {
                    mGodHand = true;
                }

                lastCount = mLikeCount;
            }
        }, 0, 1000);
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.Photo));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mGuest)
                {
//                    Intent intent = new Intent();
//                    intent.setClass(mCtx, GuestActivity.class);
//                    startActivity(intent);
                    mCtx.finish();
                }
                else mCtx.finish();
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            hideKeyboard();

            if(mGuest)
            {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
            else mCtx.finish();

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if(mBear!=null) mBear.removeAllViews();

        if(mVideo!=null && mVideo.isPlaying())
        {
            mVideo.stopPlayback();
        }

        try {
            if(mUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
            {
                if(mApplication!=null)
                {
                    if(mLikeCount!=0)
                    {
                        mApplication.sendLikeCountV2(mCtx,mPostId,mLikeCount,mGodHand);
                    }
                }
            }
        }
        catch (Exception e) {
        }
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    public void onResume()
    {
        super.onResume();

        photo.setVisibility(View.VISIBLE);
        if(mVideo!=null)
        {
            mVideo.start();
        }

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());

        if(mApplication!=null){
            if(mApplication.getGuestFinish()) mCtx.finish();
            else if(mApplication.getMenuFinish()) mCtx.finish();
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        //event tracking
        try{
            if(mGoTO == true) {
                LogEventUtil.LeavePostPage(mCtx, mApplication, mUserId, Singleton.getCurrentTimestamp() - startPhotoPage, mPostId,mLikecount,"discover Page" );
            }
            else{
                LogEventUtil.LeavePostPage(mCtx, mApplication, mUserId, Singleton.getCurrentTimestamp() - startPhotoPage, mPostId,mLikeCount ,"profile Page" );
            }
        }catch (Exception x)
        {

        }

        if(mTimer!=null)
        {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }

        try {
            if(mTencent!=null) mTencent.logout(mCtx);
        }
        catch (Exception e) {
        }
    }

    private JSONObject BranchSharePostData(String PostID, int PorV, String OpenID, String caption, String Picture)
    {
        JSONObject mShareData = new JSONObject();

        //Umeng Monitor
        String Umeng_id="SharePost";
        HashMap<String,String> mHashMap = new HashMap<String,String>();
        mHashMap.put(Umeng_id, Umeng_id);
        MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

        try
        {
            mShareData.put("page", "p");
            mShareData.put("ID", PostID);
            mShareData.put("$og_title", String.format(getString(PorV), OpenID));
            mShareData.put("$og_description", caption);
            mShareData.put("$og_image_url", Singleton.getS3FileUrl(Picture));
            mShareData.put("$og_type", "website");
            mShareData.put("$fallback_url", Constants.MEDIA17_WEBSITE_SHARE + "p/" + PostID);
        }
        catch (JSONException ex)
        {
        }

        return mShareData;
    }

    private class DownloadTask extends AsyncTask<String, Integer, Boolean>
    {
        private String subject = "";
        private String body = "";
        private String chooserTitle = "";
        private String uri = "";

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            subject = text[0];
            body = text[1];
            chooserTitle = text[2];
            uri = text[3];

            try
            {
                URL url = new URL(Singleton.getS3FileUrl(uri));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "17app.jpg";
                FileOutputStream outStream = new FileOutputStream(file);
                BitmapFactory.decodeStream(input).compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.close();
                return true;
            }
            catch (IOException e)
            {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);
            mProgress.setVisibility(View.GONE);

            try{
                LogEventUtil.SharePost(mCtx,mApplication,mUserId,mPostId,"none");

            }catch (Exception x)
            {

            }

            if(result) shareTo(subject,body,chooserTitle,uri);
            else {
                try{
//                                                showToast(getString(R.error_failed.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
            }
        }
    }

    private void shareTo(String subject, String body, String chooserTitle, String pic)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
//        String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
//        File imageFileToShare = new File(imagePath);
//        Uri uri = Uri.fromFile(imageFileToShare);
//        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    Dialog GuestDialog;
    private SharedPreferences sharedPreferences;
    private CallbackManager callbackManager;
    private void showGuestLogin()
    {
        if(GuestDialog!=null) GuestDialog = null;

        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        GuestDialog.setContentView(R.layout.guest_login_dialog);
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        Button mSingup = (Button)GuestDialog.findViewById(R.id.singup);
        mSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();
                dialog_singup_view();
            }
        });

        Button mLogin = (Button)GuestDialog.findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(mCtx,LoginActivity_V3.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.btnFBLogin);
        mFB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        GuestDialog.show();
    }

    private void dialog_singup_view()
    {
        if(GuestDialog!=null) GuestDialog = null;
        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        if(Constants.INTERNATIONAL_VERSION){
            GuestDialog.setContentView(R.layout.guest_singup_dialog_google);
        }
        else{
            GuestDialog.setContentView(R.layout.guest_singup_dialog_china);
        }
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout mWeibo = (LinearLayout)GuestDialog.findViewById(R.id.weibo_layout);
        mWeibo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupWeibo";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                showWebio();
            }
        });

        LinearLayout mQQ = (LinearLayout)GuestDialog.findViewById(R.id.qq_layout);
        mQQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupQQ";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                mTencent = Tencent.createInstance(Constants.QQAppid, mCtx);
                mTencent.login(mCtx, "all", loginListener);
            }
        });

        LinearLayout mWechat = (LinearLayout)GuestDialog.findViewById(R.id.wechat_layout);
        mWechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                if (ShareUtil.isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI"))
                {
                //Umeng monitor
                String Umeng_id="GuestModeSignupWechat";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                IWXAPI api;
                api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
                api.registerApp(Constants.WECHAT_APP_KEY);
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "wechat_sdk_demo";
                api.sendReq(req);
                }
                else
                {
                    Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                }
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.fb_layout);
        mFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                Intent intent;

                //Umeng monitor
                String Umeng_id_FB="FBlogin";
                HashMap<String,String> mHashMap_FB = new HashMap<String,String>();
                mHashMap_FB.put(Umeng_id_FB, Umeng_id_FB);
                MobclickAgent.onEventValue(mCtx, Umeng_id_FB, mHashMap_FB, 0);

                FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(final LoginResult loginResult) {
                                final String token = loginResult.getAccessToken().getToken();

                                GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject user, GraphResponse response) {
                                        if (user != null) {
                                            try {
                                                showProgressDialog();
                                                final String id = user.optString("id");
                                                final String name = user.optString("name");
//                                                    Log.e("DDD", "ID: " + id);

                                                // Download Profile photo
                                                DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, id);
                                                downloadFBProfileImage.execute();

                                                ApiManager.checkFacebookIDAvailable(mCtx, id, new ApiManager.checkFacebookIDAvailableCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String message) {
                                                        if (success) {

                                                            // Download user profile image
//                                                                DownloadFBProfileImage download = new DownloadFBProfileImage(mCtx, id);
//                                                                download.execute();

                                                            //Signup
                                                            GraphRequest request = GraphRequest.newMeRequest(
                                                                    loginResult.getAccessToken(),
                                                                    new GraphRequest.GraphJSONObjectCallback() {
                                                                        @Override
                                                                        public void onCompleted(
                                                                                JSONObject object,
                                                                                GraphResponse response) {
                                                                            // Application code
                                                                            String email = object.optString("email");
//                                                                                Log.e("DDD", "EMAIL: " + email);


                                                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, id).commit();
                                                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                            sharedPreferences.edit().putString("signup_type", "fb").commit();
                                                                            sharedPreferences.edit().putString("signup_token", token).commit();

                                                                            if (null != email) {
                                                                                if (email.contains("@")) {
                                                                                    String username = email.substring(0, email.indexOf("@"));
                                                                                    sharedPreferences.edit().putString(Constants.EMAIL, email).commit();
                                                                                    sharedPreferences.edit().putString(Constants.NAME, username).commit();
                                                                                }
                                                                            }
                                                                            hideProgressDialog();
                                                                            startActivity(new Intent(mCtx, SignupActivityV2.class));
//                                                                            mCtx.finish();
                                                                        }
                                                                    });

                                                            Bundle parameters = new Bundle();
                                                            parameters.putString("fields", "email");
                                                            request.setParameters(parameters);
                                                            request.executeAsync();

                                                        } else {
                                                            //login directly
                                                            ApiManager.loginAction2(mCtx, "", "", id, new ApiManager.LoginAction2Callback() {
                                                                @Override
                                                                public void onResult(boolean success, String message, UserModel user) {
                                                                    hideProgressDialog();

                                                                    if (success) {
                                                                        if (message.equals("ok")) {

                                                                            Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                            mStory17Application.setUser(user);

                                                                            Intent intent = new Intent();
                                                                            intent.setClass(mCtx, MenuActivity.class);
                                                                            startActivity(intent);
                                                                            mCtx.finish();
                                                                        } else if (message.equals("freezed"))
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                        else
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                    } else {
                                                                        showNetworkUnstableToast();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });


                                            } catch (Exception e) {
                                                hideProgressDialog();
                                                try {
//                              showToast(getString(R.string.failed));
                                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                } catch (Exception x) {
                                                }
                                            }
                                        }
                                    }
                                }).executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        });
            }
        });

        LinearLayout mMessage = (LinearLayout)GuestDialog.findViewById(R.id.messag_layout);
        mMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupSMS";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                sharedPreferences.edit().putString("signup_type", "message").commit();
                sharedPreferences.edit().putString("signup_token", "").commit();
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(mCtx, SignupActivityV2.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        Button mEnd = (Button)GuestDialog.findViewById(R.id.end);
        mEnd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();

            }
        });

        GuestDialog.show();
    }

    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        private final String fbImageDownlaodUrl = "https://graph.facebook.com/";
        private final String attrs = "/picture?width=1080&height=1080";
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

            this.url = fbImageDownlaodUrl + userID + attrs;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {

        }

    }

    private Dialog mWeiboDialog;
    private void showWebio()
    {
        if(mWeiboDialog!=null) mWeiboDialog = null;
        mWeiboDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        mWeiboDialog.setContentView(R.layout.instagram_dialog);
        final WebView mWeb = (WebView) mWeiboDialog.findViewById(R.id.web);
        final ProgressBar mProgress = (ProgressBar) mWeiboDialog.findViewById(R.id.progress);
        mWeb.getSettings().setJavaScriptEnabled(true);
        mWeiboDialog.setCancelable(true);
        mWeb.loadUrl("https://api.weibo.com/oauth2/authorize?client_id=" + Constants.WEIBO_APP_KEY + "&response_type=code&redirect_uri=" + Constants.WEIBO_REDIRECT_URL);
        mWeb.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                super.onPageStarted(view, url, favicon);
                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);

                if(url.contains(Constants.IG_REDIRECT_URI) && url.contains("code="))
                {
                    mWeiboDialog.dismiss();
                    showProgressDialog();

                    int pos = url.indexOf("=") + 1;
                    String token = url.substring(pos,url.length());
                    try
                    {
                        OkHttpClient mClient = new OkHttpClient();

                        RequestBody body = new FormEncodingBuilder()
                                .add("client_id", Constants.WEIBO_APP_KEY)
                                .add("client_secret", Constants.WEIBO_SECRET)
                                .add("grant_type", "authorization_code")
                                .add("code", token)
                                .add("redirect_uri", Constants.WEIBO_REDIRECT_URL)
                                .build();

                        Request request = new Request.Builder().url("https://api.weibo.com/oauth2/access_token").post(body).build();
                        mClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful())
                                {
                                    try
                                    {
                                        JSONObject data = new JSONObject(response.body().string());
                                        final String token = data.getString("access_token");
                                        final String uid = data.getString("uid");

                                        OkHttpClient client = new OkHttpClient();
                                        Request request = new Request.Builder().url("https://api.weibo.com/2/users/show.json?access_token=" + token + "&uid=" + uid).build();
                                        client.newCall(request).enqueue(new Callback()
                                        {
                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                hideProgressDialog();
                                                showToast(getString(R.string.failed));
                                            }

                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if(response.isSuccessful())
                                                {
                                                    try
                                                    {
                                                        JSONObject info = new JSONObject(response.body().string());
                                                        final int id = info.getInt("id");
                                                        final String screen_name = info.getString("screen_name");
                                                        final String pic = info.getString("profile_image_url");

                                                        DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                                        downloadFBProfileImage.execute();

                                                        ApiManager.checkWeiboIDAvailable(mCtx, uid/*String.valueOf(id)*/, new ApiManager.checkFacebookIDAvailableCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                if (success) {
                                                                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, String.valueOf(id)).commit();
                                                                    sharedPreferences.edit().putString(Constants.FULL_NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putString(Constants.NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                    sharedPreferences.edit().putString("is_weibo", "weibo").commit();

                                                                    sharedPreferences.edit().putString("signup_type", "weibo").commit();
                                                                    sharedPreferences.edit().putString("signup_token", token).commit();

                                                                    hideProgressDialog();
                                                                    Intent intent = new Intent();
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                    intent.setClass(mCtx, SignupActivityV2.class);
                                                                    startActivity(intent);
//                                                                    mCtx.finish();
                                                                } else {
                                                                    ApiManager.loginActionChina(mCtx, "", "", "weiboID", uid, new ApiManager.LoginAction2Callback() {
                                                                        @Override
                                                                        public void onResult(boolean success, String message, UserModel user) {
                                                                            hideProgressDialog();

                                                                            if (success) {
                                                                                if (message.equals("ok")) {

                                                                                    mApplication.setUser(user);

                                                                                    Intent intent = new Intent();
                                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                                    intent.setClass(mCtx, MenuActivity.class);
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                } else if (message.equals("freezed"))
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                                else
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                            } else {
                                                                                showNetworkUnstableToast();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                    catch (JSONException e)
                                                    {
                                                        hideProgressDialog();
                                                        showToast(getString(R.string.failed));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    catch (JSONException e)
                                    {
                                        hideProgressDialog();
                                        showToast(getString(R.string.failed));
                                    }
                                }
                                else
                                {
                                    hideProgressDialog();
                                    showToast(getString(R.string.failed));
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        hideProgressDialog();
                        showToast(getString(R.string.failed));
                    }
                }
                else
                {
//                            mWeiboDialog.dismiss();
//                            hideProgressDialog();
//                            showToast(getString(R.string.failed));
                }
            }
        });
        try{
            mWeb.clearCache(true);
            mWeb.getSettings().setCacheMode(WebView.PERSISTENT_NO_CACHE);
        }
        catch (Exception e){
        }
        mWeiboDialog.show();
    }

    //QQ-------

    public static Tencent mTencent;

    private class BaseUiListener implements IUiListener
    {

        @Override
        public void onComplete(Object response) {
            if (null == response) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            if (null != jsonResponse && jsonResponse.length() == 0) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }

            doComplete((JSONObject)response);
        }

        protected void doComplete(JSONObject values) {

        }

        @Override
        public void onError(UiError e) {
            hideProgressDialog();
            showToast(getString(R.string.failed));
        }

        @Override
        public void onCancel() {
            hideProgressDialog();
        }
    }

    private IUiListener loginListener = new BaseUiListener()
    {
        @Override
        protected void doComplete(JSONObject jsonObject)
        {
            try
            {
                final String token = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_ACCESS_TOKEN);
                String expires = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_EXPIRES_IN);
                final String openId = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_OPEN_ID);

                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);

                if (mTencent != null && mTencent.isSessionValid())
                {
                    IUiListener listener = new IUiListener()
                    {
                        @Override
                        public void onError(UiError e) {
                            hideProgressDialog();
                            showToast(getString(R.string.failed));
                        }

                        @Override
                        public void onComplete(final Object response)
                        {
                            try
                            {
                                JSONObject json = (JSONObject)response;

                                final String name = json.getString("nickname");
                                String pic = json.getString("figureurl_qq_1");

                                DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                downloadFBProfileImage.execute();

                                ApiManager.checkQQIDAvailable(mCtx, openId, new ApiManager.checkFacebookIDAvailableCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, openId).commit();
                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                            sharedPreferences.edit().putString(Constants.NAME, name).commit();
                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();
                                            sharedPreferences.edit().putString("signup_type", "qq").commit();
                                            sharedPreferences.edit().putString("signup_token", token).commit();
                                            hideProgressDialog();
                                            Intent intent = new Intent();
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setClass(mCtx, SignupActivityV2.class);
                                            startActivity(intent);
//                                            mCtx.finish();
                                        }
                                        else
                                        {
                                            ApiManager.loginActionChina(mCtx, "", "", "qqID", openId, new ApiManager.LoginAction2Callback() {
                                                @Override
                                                public void onResult(boolean success, String message, UserModel user) {
                                                    hideProgressDialog();

                                                    if (success) {
                                                        if (message.equals("ok")) {

                                                            mApplication.setUser(user);

                                                            Intent intent = new Intent();
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            intent.setClass(mCtx, MenuActivity.class);
                                                            startActivity(intent);
                                                            mCtx.finish();
                                                        } else if (message.equals("freezed"))
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                        else
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                    } else {
                                                        showNetworkUnstableToast();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }
                        }

                        @Override
                        public void onCancel() {
                            hideProgressDialog();
                        }
                    };

                    UserInfo mInfo = new UserInfo(mCtx, mTencent.getQQToken());
                    mInfo.getUserInfo(listener);
                }
            }
            catch(Exception e) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
            }
        }
    };

    class DownloadProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        String url = "";

        public DownloadProfileImage(Context context, String userID) {
            this.context = context;

            this.url = userID.substring(0,userID.length()-2) + 640;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {
//                    java.net.URL url = new java.net.URL(this.url);
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setDoInput(true);
//                    connection.connect();

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == com.tencent.connect.common.Constants.REQUEST_LOGIN || requestCode == com.tencent.connect.common.Constants.REQUEST_APPBAR)
        {
            Tencent.onActivityResultData(requestCode, resultCode, data, loginListener);
        }

        try {
            if(callbackManager!=null) callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        catch (Exception e) {
        }
    }
}
