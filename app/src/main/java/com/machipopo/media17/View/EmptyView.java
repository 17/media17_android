package com.machipopo.media17.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.machipopo.media17.R;

/**
 * Created by POPO on 7/7/13.
 */
public class EmptyView extends FrameLayout {

    public EmptyView(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.empty, this, true);
    }

    public EmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.empty, this, true);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        LayoutInflater.from(context).inflate(R.layout.empty, this, true);
    }
}
