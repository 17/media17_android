package com.machipopo.media17.View;

import android.content.Context;
import android.text.method.MovementMethod;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.util.Patterns;
import android.widget.TextView;

import com.machipopo.media17.utils.FeedTagActionHandler;
import com.machipopo.media17.utils.FeedTagMovementMethod;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FeedTagTextView extends TextView
{
    private static final Pattern MENTION_PATTERN = Pattern.compile("@([\\p{InCJKUnifiedIdeographs}A-Za-z0-9_-]+)");
    private static final Pattern HASHTAG_PATTERN = Pattern.compile("#([\\p{InCJKUnifiedIdeographs}A-Za-z0-9_-]+)");

    public FeedTagTextView(Context context)
    {
        this(context, null);
    }

    public FeedTagTextView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public FeedTagTextView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    protected void init(Context context, AttributeSet attrs, int defStyle)
    {

    }

    public void linkify(FeedTagActionHandler actionHandler)
    {
        Linkify.TransformFilter filter = new Linkify.TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return match.group();
            }
        };

        Linkify.addLinks(this, Patterns.EMAIL_ADDRESS, null, null, filter);
        Linkify.addLinks(this, MENTION_PATTERN, FeedTagMovementMethod.SOCIAL_UI_MENTION_SCHEME, null, filter);
        Linkify.addLinks(this, HASHTAG_PATTERN, FeedTagMovementMethod.SOCIAL_UI_HASHTAG_SCHEME, null, filter);
        Linkify.addLinks(this, Patterns.WEB_URL, null, null, filter);

        MovementMethod movementMethod = null;
        if(actionHandler != null)
        {
            movementMethod = new FeedTagMovementMethod(actionHandler);
        }
        setMovementMethod(movementMethod);
    }
}
