package com.machipopo.media17.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.machipopo.media17.R;

public class LoadMoreView extends RelativeLayout {
	public interface LoadMoreListener {
        void loadMoreAction();
    }

    Button loadMoreButton;
    ProgressBar progressBar;

    LoadMoreListener listener;

    public LoadMoreView(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.load_more, this, true);

        loadMoreButton = (Button) findViewById(R.id.loadMoreButton);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        loadMoreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    listener.loadMoreAction();
                } catch(Exception e) {

                }
            }
        });

        progressBar.setVisibility(View.VISIBLE);
        loadMoreButton.setVisibility(View.GONE);

//        reloadUI(false);
    }

    public LoadMoreView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoadMoreView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setLoadMoreListener(LoadMoreListener listener) {
//        this.listener = listener;
    }

    public void reloadUI(boolean isLoading) {
//        if(isLoading) {
//            progressBar.setVisibility(View.VISIBLE);
//            loadMoreButton.setVisibility(View.GONE);
//        } else {
//            progressBar.setVisibility(View.GONE);
//            loadMoreButton.setVisibility(View.VISIBLE);
//        }
    }
}
