package com.machipopo.media17.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;

/**
 * Created by POPO on 2015/10/28.
 */
public class SquaredSurfaceView extends SurfaceView
{
    private int width;
    private int height;

    public SquaredSurfaceView(Context context)
    {
        super(context);
    }

    public SquaredSurfaceView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SquaredSurfaceView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = (int) (width * 1.33333333333333);
        setMeasuredDimension(width, height);
    }

    public int getViewWidth()
    {
        return width;
    }

    public int getViewHeight()
    {
        return height;
    }
}
