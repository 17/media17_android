package com.machipopo.media17.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by POPO on 5/29/15.
 */
public class TagEditText extends EditText
{
    private Paint mPaint;

    public TagEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLUE);
    }

    public void setTag(int start, int end)
    {

    }

    @Override
    public void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        canvas.drawLine(0, this.getHeight() - 1, this.getWidth() - 1,this.getHeight() - 1, mPaint);
    }
}
