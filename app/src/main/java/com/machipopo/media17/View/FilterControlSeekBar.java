package com.machipopo.media17.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.machipopo.media17.R;

/**
 * TODO: document your custom view class.
 */
public class FilterControlSeekBar extends View {
    public interface OnValueChangeListener {
        void onValueChanged(float newValue);
    }


    private final int PRIMARY_COLOR = getResources().getColor(R.color.pk_white);
    //==================================================
    // position Value
    //==================================================
    private TextPaint mTextPaint = new TextPaint();
    private float value = 0.0f;
    //==================================================
    // Paint
    //==================================================
    private Paint pBackgroundLine = new Paint();
    private Paint pForegroundLine = new Paint();
    private Paint pStartPoint = new Paint();
    private Paint pThumb = new Paint();
    private Paint pThumbBackground = new Paint();
    //==================================================
    // Thumb size
    //==================================================
    private float radius_start_point = 15.0f;
    private float radius_thumb = radius_start_point + 10.0f;
    private float radius_background_thumb = radius_thumb + 7.0f;
    //==================================================
    // Position
    //==================================================
    private float thumbPositionX = 0.0f;
    private float thumbPositionMoveX = 0.0f;
    private float thumbPostionDownX = 0.0f;
    private float thumbPositionFirstDownX = 0.0f;
    private float startPointPosition = 0.0f;
    //==================================================
    // OnValueChangeListener
    //==================================================
    private OnValueChangeListener valueChangeListener;
    private boolean isFirstTimeDraw = true;
    private int width, height;
    private float txtHeight = 0.0f;
    private float minX, maxX;
    private boolean isSetValue = false;

    public FilterControlSeekBar(Context context) {
        super(context);
        init(null, 0);
    }

    public FilterControlSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public FilterControlSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    public void setOnValueChanged(OnValueChangeListener onValueChangeListener){
        valueChangeListener = onValueChangeListener;
    }

    private void init(AttributeSet attrs, int defStyle) {

        width = getWidth();
        height = getHeight() / 2;

        pStartPoint.setColor(PRIMARY_COLOR);
        mTextPaint.setColor(PRIMARY_COLOR);
        mTextPaint.setTextSize(40.0f);
        mTextPaint.setFakeBoldText(true);
        pBackgroundLine.setColor(PRIMARY_COLOR);
        pBackgroundLine.setStrokeWidth(6f);


        pForegroundLine.setColor(PRIMARY_COLOR);
        pThumb.setColor(Color.WHITE);
        pThumb.setStrokeWidth(2.0f);
        pThumbBackground.setColor(PRIMARY_COLOR);
        pThumbBackground.setStrokeWidth(4.0f);
    }


    private void dynamicAdjustThumbRadius(){
//        Log.d("DDD", "W:" + width + " , H:" + height);

        radius_start_point = (width/80);
//        Log.d("DDD", "result:" + radius_start_point);
        radius_thumb = radius_start_point * 1.7f;
        radius_background_thumb = radius_thumb * 1.3f;
        if(radius_background_thumb - radius_thumb < 1) radius_background_thumb = radius_thumb + 1;

        pBackgroundLine.setStrokeWidth(radius_start_point*0.4f);
        mTextPaint.setTextSize(radius_start_point * 2.7f);
        txtHeight = height - (int)(radius_start_point * 2.7f * 2.0f);

    }

    /*
     * 0~100 or -100~100
     */
    public void setRange(float min, float max){
        this.minX = min;
        this.maxX = max;

        value = 0.0f;
        if (minX == 0.0f) {
            startPointPosition = radius_background_thumb;
            thumbPositionX = radius_background_thumb;
        } else {
            startPointPosition = (float) (width / 2);
            thumbPositionX = (float) (width / 2);
        }
    }


    public void setValue(float _value){
        value = _value;
        isSetValue = true;


        postInvalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        width = getWidth();
        height = getHeight()/2;

        if(isFirstTimeDraw) {
            dynamicAdjustThumbRadius();
            isFirstTimeDraw = false;
            if (minX == 0.0f) {
                startPointPosition = radius_background_thumb;
                thumbPositionX = radius_background_thumb;
            } else {
                startPointPosition = (float) (width / 2);
                thumbPositionX = (float) (width / 2);
            }
        }

        if(isSetValue){
            isSetValue = false;
            thumbPositionX = ((value - minX) * (float)(width - radius_background_thumb*2) / (maxX - minX)) + radius_background_thumb;
        }

        //==================================
        // FIXED
        //==================================
//        canvas.drawColor(getResources().getColor(R.color.primary_content_color));
        canvas.drawLine(radius_background_thumb, height, width-radius_background_thumb, height, pBackgroundLine);
        //==================================
        // Draw Circle
        //==================================
        canvas.drawCircle(thumbPositionX, height, radius_background_thumb, pThumbBackground);
        canvas.drawCircle(thumbPositionX, height, radius_thumb, pThumb);
        canvas.drawCircle(startPointPosition, (float)height, radius_start_point, pStartPoint);
        //==================================
        // Draw Text
        //==================================
        if(value>-10.0f && value < 10.0f) {
            canvas.drawText(String.valueOf((int)value), (thumbPositionX - 10.0f), txtHeight, mTextPaint);
        }else if(value >= 10.0f && value <100.0f) {
            canvas.drawText(String.valueOf((int)value), (thumbPositionX - (float)(width/65)), txtHeight, mTextPaint);
        }else if(value == 100.0f){
            canvas.drawText(String.valueOf((int)value), (thumbPositionX - radius_background_thumb-5.0f), txtHeight ,mTextPaint);
        }else{
            canvas.drawText(String.valueOf((int)value), (thumbPositionX - radius_background_thumb), txtHeight ,mTextPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        Log.w("DDD", "getX : " + event.getX() + " , getRawX : " + event.getRawX());
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                thumbPostionDownX = event.getX();
                thumbPositionFirstDownX = event.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                thumbPositionMoveX = event.getX() - thumbPostionDownX;

                if(Math.abs(thumbPositionMoveX) > 1.0f) {

                    thumbPositionX += thumbPositionMoveX;
                    if (thumbPositionX <= radius_background_thumb) thumbPositionX = radius_background_thumb;
                    if (thumbPositionX > width-radius_background_thumb) thumbPositionX = width - radius_background_thumb;

                    convertPositionToValue();
                    thumbPostionDownX = event.getX();
                }
                break;
            case MotionEvent.ACTION_UP:
                // implement tap function
                Log.d("DDD", String.valueOf(event.getX() - thumbPositionFirstDownX));

                if(Math.abs(event.getX() - thumbPositionFirstDownX) <= 15){
                    // Tap on the right side
                    if(event.getX()  > (width/2)){
                        value += 1;
                    }
                    // Tap on the left side
                    else{
                        value -= 1;
                    }
                    thumbPositionX = ((value - minX) * (float)(width - radius_background_thumb*2) / (maxX - minX)) + radius_background_thumb;

                    if (thumbPositionX <= radius_background_thumb) thumbPositionX = radius_background_thumb;
                    if (thumbPositionX > width-radius_background_thumb) thumbPositionX = width - radius_background_thumb;
                }

                break;
        }

        // Force a view to draw again
        postInvalidate();
        return true;
    }

    private void convertPositionToValue(){
        value = ((thumbPositionX-radius_background_thumb) * (maxX - minX)) / (float)(width - radius_background_thumb*2.0f) + minX;
        valueChangeListener.onValueChanged(value);
    }



}