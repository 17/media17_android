package com.machipopo.media17.View;

import android.content.Context;
import android.text.method.MovementMethod;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.widget.TextView;

import com.machipopo.media17.utils.SocialActionHandler;
import com.machipopo.media17.utils.SocialMovementMethod;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SocialTextView extends TextView
{
    private static final Pattern MENTION_PATTERN = Pattern.compile("@([A-Za-z0-9_-]+)");
    private static final Pattern HASHTAG_PATTERN = Pattern.compile("#([A-Za-z0-9_-]+)");

    public SocialTextView(Context context)
    {
        this(context, null);
    }

    public SocialTextView(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public SocialTextView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    protected void init(Context context, AttributeSet attrs, int defStyle)
    {

    }

    public void linkify(SocialActionHandler actionHandler)
    {
        Linkify.TransformFilter filter = new Linkify.TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return match.group();
            }
        };

        Linkify.addLinks(this, MENTION_PATTERN, SocialMovementMethod.SOCIAL_UI_MENTION_SCHEME, null, filter);
        Linkify.addLinks(this, HASHTAG_PATTERN, SocialMovementMethod.SOCIAL_UI_HASHTAG_SCHEME, null, filter);

        MovementMethod movementMethod = null;
        if(actionHandler != null)
        {
            movementMethod = new SocialMovementMethod(actionHandler);
        }
        setMovementMethod(movementMethod);
    }
}
