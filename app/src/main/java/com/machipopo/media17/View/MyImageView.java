package com.machipopo.media17.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.machipopo.media17.BitmapHelper;
import com.machipopo.media17.R;

public class MyImageView extends ImageView {

	public boolean isPlaying = false;
    public boolean makeItSquare = false; // this forces the image to be square
    public boolean makeItPrizeSize = false; // this forces the image to be prize size

	// Threads
	private Handler handler = new Handler();

	// rounded corner image
	int roundedCornerRadius = 0;

	public MyImageView(Context context) {
		super(context);
		initialize();
	}

	public MyImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	public MyImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}

	private void initialize() {
		setBackgroundColor(Color.TRANSPARENT);
	}

	@Override
	public void setImageResource(int resourceID) {
		isPlaying = false;
				
		if(roundedCornerRadius!=0) {
			setImageBitmap(BitmapFactory.decodeResource(getResources(), resourceID));
			return;
		}

		super.setImageResource(resourceID);
	}

	@Override
	public void setImageBitmap(Bitmap bitmap) {
		isPlaying = false;
		
		try {
			if(roundedCornerRadius!=0) {
				bitmap = BitmapHelper.getRoundedCornerBitmap(bitmap, roundedCornerRadius);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		super.setImageBitmap(bitmap);
	}
	
	public void setScaleType(ScaleType scaleType) {
		super.setScaleType(scaleType);
	}

    public void clearImage() {
		super.setImageBitmap(null);
	}

	public void setRoundedCornerRadius(int px) {
		this.roundedCornerRadius = px;
	}

	public void stopPlaying() {
		isPlaying = false;
	}

	public void showLoadingView(Boolean isToMessage) {
		isPlaying = false;
		
		setScaleType(ScaleType.CENTER);
		
		if(isToMessage) {
			playAnimation(R.anim.loading_blue);
		} else {
			playAnimation(R.anim.loading_gray);	
		}
	}

	public void playAnimation(int resourceID) {
		AnimationDrawable loadingDrawable = (AnimationDrawable) getResources().getDrawable(resourceID);
		setImageDrawable(loadingDrawable);
		loadingDrawable.start();
	}

	public boolean isPlayingAnimation() {
		try {
			AnimationDrawable animationDrawable = (AnimationDrawable) getDrawable();
			return animationDrawable.isRunning();
		} catch (Exception e) {
			return false;
		}
	}

    // This solves the problem of auto-height to keep aspect-ratio adjusting when scaleType is set to fitXY
    @Override protected void onMeasure(int widthMeasureSpec,
                                       int heightMeasureSpec) {
        Drawable d = getDrawable();
        ScaleType scaleType = getScaleType();

        if(d!=null && scaleType==ScaleType.FIT_XY) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = (int) Math.ceil((float) width * (float) d.getIntrinsicHeight() / (float) d.getIntrinsicWidth());
            setMeasuredDimension(width, height);
        } else if(makeItSquare) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            setMeasuredDimension(width, width);
        } else if(makeItPrizeSize) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            setMeasuredDimension(width, width/4*3);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
