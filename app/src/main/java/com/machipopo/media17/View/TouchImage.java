package com.machipopo.media17.View;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by POPO on 6/4/15.
 */
public class TouchImage extends ImageView
{
    public TouchImage(Context context)
    {
        super(context);
    }

    public TouchImage(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public TouchImage(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }


}
