package com.machipopo.media17.View;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by Zack on 2/17/16.
 */
public class RelativeSlideLayout extends RelativeLayout {

    public interface SlideGestureListener{
        public void onSwipeRight(View v, MotionEvent e1, MotionEvent e2);
        public void onSwipeLeft(View v, MotionEvent e1, MotionEvent e2);
        public void onSwipeTop(View v, MotionEvent e1, MotionEvent e2);
        public void onSwipeBottom(View v, MotionEvent e1, MotionEvent e2);
        public void onDown(View v, MotionEvent e);
    }

    private GestureDetector mGestureDetector;
    private SlideGestureListener mSlideGestureListener;

    public RelativeSlideLayout(Context context) {
        super(context);
        init();
    }

    public RelativeSlideLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RelativeSlideLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RelativeSlideLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        mGestureDetector = new GestureDetector(getContext(), new GestureListener());
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    public void setSlideGestureListener(SlideGestureListener l){
        mSlideGestureListener = l;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent e) {
        // do what you need to with the event, and then...
        onTouchEvent(e);
        return super.dispatchTouchEvent(e);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 150;
        private static final int SWIPE_VELOCITY_THRESHOLD = 150;

        @Override
        public boolean onDown(MotionEvent e) {
            if(mSlideGestureListener != null){
                mSlideGestureListener.onDown(RelativeSlideLayout.this, e);
            }
            //Log.e("RelativeSlideLayout", "onDown");
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight(e1, e2);
                        } else {
                            onSwipeLeft(e1, e2);
                        }
                    }
                    result = true;
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom(e1, e2);
                    } else {
                        onSwipeTop(e1, e2);
                    }
                }
                result = true;

            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }

        public void onSwipeRight(MotionEvent e1, MotionEvent e2) {
            if(mSlideGestureListener != null){
                mSlideGestureListener.onSwipeRight(RelativeSlideLayout.this, e1, e2);
            }
            //Log.e("RelativeSlideLayout", "onSwipeRight");
        }

        public void onSwipeLeft(MotionEvent e1, MotionEvent e2) {
            if(mSlideGestureListener != null){
                mSlideGestureListener.onSwipeLeft(RelativeSlideLayout.this, e1, e2);
            }
            //Log.e("RelativeSlideLayout", "onSwipeLeft");
        }

        public void onSwipeTop(MotionEvent e1, MotionEvent e2) {
            if(mSlideGestureListener != null){
                mSlideGestureListener.onSwipeTop(RelativeSlideLayout.this, e1, e2);
            }
            //Log.e("RelativeSlideLayout", "onSwipeTop");
        }

        public void onSwipeBottom(MotionEvent e1, MotionEvent e2) {
            if(mSlideGestureListener != null){
                mSlideGestureListener.onSwipeBottom(RelativeSlideLayout.this, e1, e2);
            }
            //Log.e("RelativeSlideLayout", "onSwipeBottom");
        }
    }

}
