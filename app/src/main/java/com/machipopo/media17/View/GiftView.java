package com.machipopo.media17.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.machipopo.media17.Singleton;

import java.util.ArrayList;

/**
 * Created by POPO on 2015/11/19.
 */
@SuppressLint("WrongCall")
public class GiftView extends SurfaceView implements SurfaceHolder.Callback, Runnable
{
    private Context mCtx;
    private SurfaceHolder mSurfaceHolder;
    private Paint mPaint;
    private Boolean RUN = true;
    private int SLEEP;
    private ArrayList<Bitmap> mBitmaps = new ArrayList<Bitmap>();
    private String FileName = "";
    private int mLoadPos = 1;
    private int mNowPos = 0;
    private int mMax;
    private int mPhoneWidth, mPhoneHeight;
    private BitmapFactory.Options mOptions;
    public GiftEndListener mGiftEndListener;
//    GPUImage mGPUImage;
    private Matrix mMatrix;

    public GiftView(Context context, int sleep, int phoneWidth, int phoneHeight, String name, int max, String zipName)
    {
        super(context);

        mCtx = context;
        mSurfaceHolder = getHolder();
        mSurfaceHolder.addCallback(this);

        mPaint = new Paint();
        SLEEP = (int)((sleep / 10) / 2);
        mPhoneWidth = phoneWidth;
        mPhoneHeight = phoneHeight;
        FileName = zipName + "/" + name + "_";
        mMax = max;

        setZOrderMediaOverlay(true);
        getHolder().setFormat(PixelFormat.TRANSLUCENT);

        mOptions = new BitmapFactory.Options();
        mOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        mPaint.setFilterBitmap(true);

//        mGPUImage = new GPUImage(mCtx);
//        mGPUImage.setFilter(new GPUImageSepiaFilter());
//        mGPUImage.setImage(createScaleBitmap(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + "3.png", mOptions)));
//        mGPUImage.deleteImage();

//        mBitmaps.add(createScaleBitmap(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + "1.png", mOptions)));
//        mBitmaps.add(createScaleBitmap(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + "2.png", mOptions)));
//        mBitmaps.add(createScaleBitmap(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + "3.png", mOptions)));

        try
        {
            mBitmaps.add(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + "1.png", mOptions));
            mBitmaps.add(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + "2.png", mOptions));
            mBitmaps.add(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + "3.png", mOptions));
            mLoadPos = 3;

            mMatrix = new Matrix();
            mMatrix.setScale((float)mPhoneWidth / (float)mBitmaps.get(0).getWidth(),(float)mPhoneHeight / (float)mBitmaps.get(0).getHeight());
        }
        catch (OutOfMemoryError o)
        {
            mLoadPos = mMax;
        }
        catch (Exception e)
        {
            mLoadPos = mMax;
        }

//        mWidth = mBitmaps.get(0).getWidth();
//        mHeight = mBitmaps.get(0).getHeight();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,int height)
    {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        RUN = true;
        new Thread(this).start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        RUN = false;
        mSurfaceHolder.removeCallback(this);
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        if(canvas != null)
        {
            try
            {
                if(mBitmaps.size()!=0)
                {
                    if(mBitmaps.get(mNowPos)!=null)
                    {
                        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                        if(canvas!=null && mBitmaps.get(mNowPos)!=null && mMatrix!=null && mPaint!=null) canvas.drawBitmap(mBitmaps.get(mNowPos), mMatrix, mPaint);
//                canvas.drawBitmap(mBitmaps.get(mNowPos), (mPhoneWidth/2) - (mWidth/2), (mPhoneHeight/2) - (mHeight/2), mPaint);
//                canvas.drawBitmap(mGPUImage.getBitmapWithFilterApplied(), (mPhoneWidth/2) - (mWidth/2), (mPhoneHeight/2) - (mHeight/2), mPaint);
                        mNowPos++;
                    }
                    else canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                }
            }
            catch (Exception e)
            {

            }
        }

        if(!RUN)
        {
            if(canvas != null) canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        }
    }

    @Override
    public void run()
    {
        Canvas mCanvas ;

        while(RUN)
        {
            mCanvas = null ;

            try
            {
                mCanvas = mSurfaceHolder.lockCanvas();
                synchronized(mSurfaceHolder)
                {
                    onDraw(mCanvas);

                    mLoadPos++;

                    if(mLoadPos > mMax)
                    {
                        RUN = false;
                        onDraw(mCanvas);
                        mGiftEndListener.OnGiftEnd();
                        return;
                    }

                    if(mBitmaps.get(0)!=null)
                    {
                        mBitmaps.get(0).recycle();
                        mBitmaps.remove(0);
                        mNowPos--;
//                        mBitmaps.add(createScaleBitmap(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + String.valueOf(mLoadPos) + ".png", mOptions)));

                        try
                        {
                            mBitmaps.add(BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + FileName + String.valueOf(mLoadPos) + ".png", mOptions));
                        }
                        catch (OutOfMemoryError o)
                        {
                            mLoadPos = mMax;
                            RUN = false;
                            onDraw(mCanvas);
                            mGiftEndListener.OnGiftEnd();
                        }
                        catch (Exception e)
                        {
                            mLoadPos = mMax;
                            RUN = false;
                            onDraw(mCanvas);
                            mGiftEndListener.OnGiftEnd();
                        }
                    }
                }
            }
            finally
            {
                if (mCanvas != null)
                {
                    try {
                        if(mSurfaceHolder!=null) mSurfaceHolder.unlockCanvasAndPost(mCanvas);
                    }
                    catch (Exception e){
                    }
                }
            }

            try{Thread.sleep(SLEEP);}
            catch(Exception e){}
        }
    }

//    private Bitmap createScaleBitmap(Bitmap src)
//    {
//        if(mBitWidth==0 && mBitHeight==0)
//        {
//            float scale = ((float)mPhoneWidth / (float)src.getWidth());
//            mBitWidth = (int)(scale * (float)src.getWidth());
//            mBitHeight = (int)(scale * (float)src.getHeight());
//        }
//
//        Bitmap bitmap = Bitmap.createScaledBitmap(src, mBitWidth, mBitHeight, false);
//        if (src != bitmap) {
//            src.recycle();
//        }
//        return bitmap;
//    }

    public interface GiftEndListener
    {
        void OnGiftEnd();
    }
}
