package com.machipopo.media17.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.machipopo.media17.CameraFilterViewController;

/**
 * Created by POPO on 15/7/1.
 */
public class TiltEffectDrawingView extends View {
    private CameraFilterViewController.TiltEventListener tiltNotify;
    private final int MODE_DRAG = 1;
    private final int MODE_ZOOM = 2;
    private int mode = 0;
    private int drawType = 1; // default:circle

    private boolean isFirstTimeIn = true;

    private Paint paintCircle1, paintCircle2;
    private Paint paintLine1, paintLine2;

    //=============================================
    // Circle
    //=============================================
    private float radius = 100f, radius2, radiusOrigin;
    private static final float maxTiltShiftRadialRadius = 0.8f;
    private static final float minTiltShiftRadialRadius = 0.1f;
    private float radialDrawRadius = 0.3f; // default
    //=============================================
    // Line
    //=============================================
    private float minDistance = 0f;
    private float maxDistance = 0f;
    private float currentAngle = 0f;
    private float rotateAngle = 0f; //delta value
    private float lastRotateAngle = 0f; //calculate delta value
    private float lineDeltaY = 0f;
    private float oldDeltaY = 0f;
    private float newDeltaY = 0f;
    //=============================================
    private float distance = 0.0f;
    private float olddistance = 0.0f;
    private float currentX, currentY, oldX, oldY;
    private float angle = 0f;
    private boolean isSecondFingerJustUp = false;

    private boolean isClear = false;

    public TiltEffectDrawingView(Context context) {
        super(context);
        circleInit();
        lineInit();
    }

    public TiltEffectDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        circleInit();
        lineInit();
    }

    public TiltEffectDrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        circleInit();
        lineInit();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(isFirstTimeIn){
            isFirstTimeIn = false;
            currentX = getWidth() / 2.0f;
            currentY = getHeight() / 2.0f;

            minDistance = Math.round(getWidth() * 0.05);
            maxDistance = Math.round(getWidth() - minDistance*2);
        }
                    canvas.save();

        try{
            if(mode != 0){
                if(mode == MODE_ZOOM){
                    canvas.rotate(currentAngle, getMeasuredWidth()/2f, getMeasuredWidth()/2f);
                }

                canvas.drawCircle(currentX, currentY, radius, paintCircle1);
                canvas.drawCircle(currentX, currentY, radius2, paintCircle2);

                canvas.drawLine(-getWidth(),currentY-minDistance+(lineDeltaY/2f), getWidth()*2f, currentY-minDistance+(lineDeltaY/2f), paintLine1);
                canvas.drawLine(-getWidth(),currentY+minDistance-(lineDeltaY/2f), getWidth()*2f, currentY+minDistance-(lineDeltaY/2f), paintLine2);

            }
        }catch(Exception e){
//            Log.e(CameraFilterViewController.dTag, "Tilt onDraw Error : " + e.getMessage());
        }
        if(isClear){
//            Log.d(CameraFilterViewController.dTag, "onDraw clear");
            isClear = !isClear;
            if(drawType == DrawType.Circle) {
                paintCircle1.setColor(Color.WHITE);
                paintCircle2.setColor(Color.WHITE);
            }
        }
        canvas.restore();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK){
            case MotionEvent.ACTION_DOWN:
                mode = MODE_DRAG;
                oldX = event.getX();
                oldY = event.getY();

                break;
            case MotionEvent.ACTION_MOVE:
                if(mode == MODE_DRAG) {
                    if ((Math.abs(event.getX() - oldX) >= 10) || (Math.abs(event.getY() - oldY) >= 50)) {

                        //====================================================================
                        // 解決多點觸控每次getX取到錯誤的值
                        // 因為兩點以上，當第二隻手離開後取getX(0)不一定真的會取到原本第一個點下去的座標
                        //====================================================================
                        if (isSecondFingerJustUp){
                            isSecondFingerJustUp = false;
                            oldX = event.getX();
                            oldY = event.getY();
                            break;
                        }
                        //====================================================================

                        currentX += (event.getX() - oldX);
                        currentY += (event.getY() - oldY);

//                        Log.d("DDD", "Angle : " + Math.atan2(currentY-getWidth()/2, currentX-getWidth()/2)/Math.PI*180);

                        if(currentX > getWidth()-20f) currentX = getWidth()-20f;
                        else if(currentX < 20f) currentX = 20f;

                        if(currentY > getHeight() -20f) currentY = getHeight()-20f;
                        else if(currentY < 20f) currentY = 20f;

                        oldX = event.getX();
                        oldY = event.getY();
                    }
                }
                else if(mode == MODE_ZOOM){
                    setRotateAngle(event);
                    if(drawType == DrawType.Circle) updateCircle(event);
                    newDeltaY = Math.abs(event.getY(0) - event.getY(1));
                    lineDeltaY = newDeltaY - oldDeltaY;
//                    oldDeltaY = newDeltaY;
                }
                break;
            case MotionEvent.ACTION_UP:
                mode = 0;

                // clean canvas
                isClear = true;
                paintCircle1.setColor(Color.TRANSPARENT);
                paintCircle2.setColor(Color.TRANSPARENT);


                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                mode = MODE_ZOOM;
                olddistance = getDistance(event);

                oldDeltaY = Math.abs(event.getY(0) - event.getY(1));
                lastRotateAngle = getAngle(event);
                break;
            case MotionEvent.ACTION_POINTER_UP:
                mode = MODE_DRAG;
                isSecondFingerJustUp = true;
                break;
        }


        radialDrawRadius = getDrawRadius();
        // Notify Renderer
        tiltNotify.updateTiltEffect(drawType, 1.0f - (currentY / getWidth()), 1.0f - (currentX / getWidth()), radialDrawRadius, angle);

        // indicate view should be redrawn
        invalidate();
        return true;
    }

    private void setRotateAngle(MotionEvent event) {

        rotateAngle = getAngle(event);
//        Log.e("DDD", "angle: " + rotateAngle);

        float delta = getAngle(event) - lastRotateAngle;
//        Log.e("DDD", "delta: " + delta);
        currentAngle += delta;
        lastRotateAngle = getAngle(event);
//        Log.e("DDD", "Angle: " + currentAngle);
    }

    private float getRotateAngle(){
        return rotateAngle;
    }

    private float getAngle(MotionEvent event){
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        double angle = Math.toDegrees(radians);
        if(angle< -179){
            angle = -179;
        }

        return (float) Math.toDegrees(radians);
    }

    private void circleInit(){
        paintCircle1 = new Paint();
        paintCircle1.setAntiAlias(true);
        paintCircle1.setColor(Color.WHITE);
        paintCircle1.setStyle(Paint.Style.STROKE);
        paintCircle1.setStrokeWidth(10.0f);

        paintCircle2 = new Paint();
        paintCircle2.setAntiAlias(true);
        paintCircle2.setColor(Color.WHITE);
        paintCircle2.setStyle(Paint.Style.STROKE);
        paintCircle2.setStrokeWidth(10.0f);

        radiusOrigin = radius;
        radius2 = radius * 1.25f;
    }

    private void lineInit(){
        paintLine1 = new Paint();
        paintLine1.setAntiAlias(true);
        paintLine1.setColor(Color.WHITE);
        paintLine1.setStyle(Paint.Style.STROKE);
        paintLine1.setStrokeWidth(10.0f);

        paintLine2 = new Paint();
        paintLine2.setAntiAlias(true);
        paintLine2.setColor(Color.WHITE);
        paintLine2.setStyle(Paint.Style.STROKE);
        paintLine2.setStrokeWidth(10.0f);
    }

    private float getDistance(MotionEvent event) {
        float dx = event.getX(1) - event.getX(0);
        float dy = event.getY(1) - event.getY(0);

        return (float)Math.sqrt(dx * dx + dy * dy);
    }

    private PointF getMiddlePoint(MotionEvent event){
        float midX = (event.getX(1) + event.getX(0)) / 2;
        float midY = (event.getY(1) + event.getY(0)) / 2;

        return new PointF(midX, midY);
    }

    private float getDrawRadius(){
        float dr = (radius * maxTiltShiftRadialRadius) / (getWidth() / 2);

        if(dr<0.1f) {
            return 0.1f;
        }else if(dr>0.8){
            return 0.8f;
        }else{
            return dr;
        }
    }

    public void setDrawType(int type){
        this.drawType = type;
    }

    private void updateCircle(MotionEvent event){
        //get the distance of two points
        distance = getDistance(event);
//        Log.d("DDD", "===============================");
//        Log.d("DDD", String.valueOf(radius) + " ; " + String.valueOf(radius2) + " ; " + String.valueOf(radiusOrigin) + " ; " + String.valueOf(distance - olddistance));
        radius = radius + (distance - olddistance);
        radius2 = radius * 1.25f;
//        Log.d("DDD", String.valueOf(radius) + " ; " + String.valueOf(radius2) + " ; " + String.valueOf(radiusOrigin) + " ; " + String.valueOf(distance - olddistance));

        if(radius2 > getWidth() / 2){
            radius2 = getWidth() / 2;
            radius = radius2 / 1.25f;
        }

        if(radius < 100f){
            radius = 100f;
            radius2 = radius * 1.25f;
        }

        olddistance = distance;
//        PointF newPoint = getMiddlePoint(event);
//        currentX = newPoint.x;
//        currentY = newPoint.y;
    }

    private void updateLine(MotionEvent event){

    }

    public void addListener(CameraFilterViewController.TiltEventListener listener) { this.tiltNotify = listener;}

    public static class DrawType{
        public static final int Circle = 1;
        public static final int Line = 2;
    }
















}