package com.machipopo.media17;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.machipopo.media17.model.UserModel;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;


public class SmsSetupNewPasswordActivity extends BaseActivity {
    private SmsSetupNewPasswordActivity mCtx = this;
    private EditText mNewPassword, mConfirmNewPassword;
    private ConnectivityManager mConnectivityManager;
    private NetworkInfo mNetworkInfo;
    private Button btnLogin;
    private boolean focus = false;
    private SharedPreferences sharedPreferences;
    private CallbackManager callbackManager;
    public final static String tag = "signup_setting";
    private Dialog mTimeoutDialog;
    private Button mReturn;
    private ProgressBar mProgress;


    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_setup_password);

        initTitleBar();

        mNewPassword = (EditText) findViewById(R.id.new_password);
        mConfirmNewPassword = (EditText) findViewById(R.id.confirm_new_password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        mTimeoutDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        mTimeoutDialog.setContentView(R.layout.change_password_timeout);
        Window window = mTimeoutDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        mTimeoutDialog.setCancelable(false);
        btnLogin.setOnClickListener(ButtonClickEventListener);
        mReturn = (Button) mTimeoutDialog.findViewById(R.id.return_page);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        sharedPreferences = getSharedPreferences(tag, 0);

        showKeyboard();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(R.string.entet_password);
        ((ImageView) findViewById(R.id.img_left)).setVisibility(View.INVISIBLE);

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            mProgress.setVisibility(View.GONE);
            Toast.makeText(mCtx, getString(R.string.modification_not_complete), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.setClass(mCtx, LoginActivity_V3.class);
            startActivity(intent);
            mCtx.finish();
        }

        if(keyCode == KeyEvent.KEYCODE_ENTER && focus){
            //processLogin();
        }

        return super.onKeyUp(keyCode, event);
    }

    private View.OnClickListener ButtonClickEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if ((mNewPassword.getText().toString().length() >= 5) && (mNewPassword.getText().toString().equals(mConfirmNewPassword.getText().toString())))
            {
                final Bundle bundle = mCtx.getIntent().getExtras();
                String resetPasswordToken = bundle.getString("resetPasswordToken");
                hideKeyboard();
                mProgress.setVisibility(View.VISIBLE);
                ApiManager.resetPassword(mCtx, resetPasswordToken, mNewPassword.getText().toString(), mConfirmNewPassword.getText().toString(), new ApiManager.ResetPasswordCallback() {
                    @Override
                    public void onResult(boolean success, String result, String message) {
                        if (success)
                        {
                            try {
                                if (result.equals("success")) {
                                    mProgress.setVisibility(View.GONE);
                                    Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent();
                                    intent.setClass(mCtx, LoginActivity_V3.class);
                                    startActivity(intent);
                                    mCtx.finish();
                                } else {
                                    mProgress.setVisibility(View.GONE);
                                    if (message.equals("ip_rate_limit_exceeds") || message.equals("maximum_count_exceeds")) {
                                        Toast.makeText(mCtx, getString(R.string.rate_limit), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent();
                                        intent.setClass(mCtx,LoginActivity_V3.class);
                                        startActivity(intent);
                                        mCtx.finish();
                                    }
                                    else if(message.equals("new_password_confirm_error"))
                                    {
                                        Toast.makeText(mCtx, getString(R.string.password_same), Toast.LENGTH_SHORT).show();
                                    }
                                    else if(message.equals("resetPasswordToken_timeout") || message.equals("resetPasswordToken_used") ||message.equals("resetPasswordToken_invalid"))
                                    {
                                        mTimeoutDialog.show();
                                        mReturn.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   Intent intent = new Intent();
                                                   intent.setClass(mCtx,LoginActivity_V3.class);
                                                   startActivity(intent);
                                                   mCtx.finish();
                                                   mTimeoutDialog.cancel();
                                               }
                                        });
                                    }
                                }
                            } catch (Exception x) {
                            }
                        }
                        else
                        {
                            try {
                                //                          showToast(getString(R.string.error_failed));
                                mProgress.setVisibility(View.GONE);
                                Toast.makeText(mCtx, getString(R.string.connet_erroe), Toast.LENGTH_SHORT).show();
                            } catch (Exception x) {
                            }
                        }
                    }
                });

            }
            else
            {
                if ((mNewPassword.getText().toString().length() < 5)) {
                    Toast.makeText(mCtx, getString(R.string.password_size), Toast.LENGTH_SHORT).show();
                } else if (!(mNewPassword.getText().toString().equals(mConfirmNewPassword.getText().toString()))) {
                    Toast.makeText(mCtx, getString(R.string.password_same), Toast.LENGTH_SHORT).show();
                }

            }
        }
    };
}