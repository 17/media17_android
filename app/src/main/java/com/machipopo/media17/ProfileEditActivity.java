package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TimeZone;

/**
 * Created by POPO on 6/10/15.
 */
public class ProfileEditActivity extends BaseNewActivity
{
    private ProfileEditActivity mCtx = this;

    private Story17Application mApplication;
    private InputMethodManager inputMethodManager = null;

    private EditText mName,mWeb,mCaption,mMail,mPhone,mOpen;
    private TextView mSize,mSex,mBirthday,mCountryCode;

    private ArrayList<String> mAge = new ArrayList<String>();
    private ArrayList<String> mGender = new ArrayList<String>();

    private HashMap<String,String> CODE = new HashMap<String,String>();
    private HashMap<String,String> NAME = new HashMap<String,String>();

    private String[] mListCountry ;
    private String[] mListCode ;

    private String UserCode = "886";

    private ProgressDialog progressDialog = null;
    private int mGMT = 0;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        initTitleBar();

        mName = (EditText) findViewById(R.id.name);
        mName.setText(getConfig(Constants.PROFILE_NAME, ""));

        mWeb = (EditText) findViewById(R.id.web);
        mWeb.setText(getConfig(Constants.PROFILE_WEB, ""));

        mCaption = (EditText) findViewById(R.id.caption);
        mCaption.setText(getConfig(Constants.PROFILE_BIO, ""));

        mMail = (EditText) findViewById(R.id.mail);
        mMail.setText(getConfig(Constants.PROFILE_EMAIL, ""));

        mSex = (TextView) findViewById(R.id.sex);

        mOpen = (EditText) findViewById(R.id.open);
        mOpen.setText(Singleton.preferences.getString(Constants.OPEN_ID, ""));
        mOpen.setSelection(mOpen.getText().toString().length());
        mOpen.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() != 0) {
                    String text = charSequence.toString().substring(charSequence.toString().length() - 1, charSequence.toString().length());
                    if (text.compareTo("A") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "a");
                    else if (text.compareTo("B") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "b");
                    else if (text.compareTo("C") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "c");
                    else if (text.compareTo("D") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "d");
                    else if (text.compareTo("E") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "e");
                    else if (text.compareTo("F") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "f");
                    else if (text.compareTo("G") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "g");
                    else if (text.compareTo("H") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "h");
                    else if (text.compareTo("I") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "i");
                    else if (text.compareTo("J") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "j");
                    else if (text.compareTo("K") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "k");
                    else if (text.compareTo("L") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "l");
                    else if (text.compareTo("M") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "m");
                    else if (text.compareTo("N") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "n");
                    else if (text.compareTo("O") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "o");
                    else if (text.compareTo("P") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "p");
                    else if (text.compareTo("Q") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "q");
                    else if (text.compareTo("R") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "r");
                    else if (text.compareTo("S") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "s");
                    else if (text.compareTo("T") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "t");
                    else if (text.compareTo("U") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "u");
                    else if (text.compareTo("V") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "v");
                    else if (text.compareTo("W") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "w");
                    else if (text.compareTo("X") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "x");
                    else if (text.compareTo("Y") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "y");
                    else if (text.compareTo("Z") == 0) mOpen.setText(mOpen.getText().toString().substring(0, mOpen.getText().toString().length() - 1) + "z");

                    mOpen.setSelection(mOpen.getText().toString().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        try
        {
            if(getConfig(Constants.PROFILE_SEX, "male").compareTo("male")==0) mSex.setText(getString(R.string.male));
            else mSex.setText(getString(R.string.female));
        }
        catch (Exception e)
        {

        }

        mSize = (TextView) findViewById(R.id.size);
        mCaption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSize.setText(String.valueOf(250 - s.toString().length()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mBirthday = (TextView) findViewById(R.id.birthday);
        mBirthday.setText(getConfig(Constants.PROFILE_AGE, "12"));

        mPhone = (EditText) findViewById(R.id.phone);
        mPhone.setText(getConfig(Constants.PROFILE_PHONE, ""));
        mCountryCode = (TextView) findViewById(R.id.country_code);
        UserCode = getConfig(Constants.PROFILE_COUNTRY_CODE, "886");
        mCountryCode.setText(UserCode);

//        ViewGroup view = (ViewGroup) getWindow().getDecorView();
//        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }, 100);

        mGender.clear();
        mGender.add(getString(R.string.male));
        mGender.add(getString(R.string.female));
        mSex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int s = 0;
                if (mSex.getText().toString().compareTo(getString(R.string.female)) == 0) s = 1;

                showSingleItemPicker(getString(R.string.gender), mGender, s, new SingleItemPickerCallback() {
                    @Override
                    public void onResult(boolean done, int selectedItem) {
                        mSex.setText(mGender.get(selectedItem));
                    }
                });
            }
        });

        mAge.clear();
        for(int i = 12 ; i < 81 ; i++)
        {
            mAge.add(String.valueOf(i));
        }
        mBirthday.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showSingleItemPicker(getString(R.string.sign_birthday), mAge, 0, new SingleItemPickerCallback()
                {
                    @Override
                    public void onResult(boolean done, int selectedItem)
                    {
                        mBirthday.setText(mAge.get(selectedItem));
                    }
                });
            }
        });

        try
        {
            String code = ReadFromfile("country_code.txt",mCtx);
            JSONArray mCodeArray = new JSONArray(code);
            mListCountry = new String[mCodeArray.length()];
            mListCode = new String[mCodeArray.length()];
            for(int i = 0 ; i < mCodeArray.length() ; i++)
            {
                CODE.put(mCodeArray.getJSONObject(i).getString("country"), mCodeArray.getJSONObject(i).getString("countryCode"));
                NAME.put(mCodeArray.getJSONObject(i).getString("countryCode"), mCodeArray.getJSONObject(i).getString("country"));
                mListCode[i] = mCodeArray.getJSONObject(i).getString("countryCode");
                mListCountry[i] = getResources().getString(getResources().getIdentifier(mCodeArray.getJSONObject(i).getString("country"), "string", getPackageName()));
            }
        }
        catch (JSONException e)
        {
        }

        if(getConfig(Constants.PROFILE_PHONE, "").length()==0)
        {
            ApiManager.getCountry(mCtx, new ApiManager.GetCountryCallback()
            {
                @Override
                public void onResult(boolean success, String message)
                {
                    if (message.length() != 0)
                    {
                        UserCode = CODE.get(message);
                        mCountryCode.setText("+" + CODE.get(message));
                    }
                    else
                    {
                        mCountryCode.setText("+886");
                    }
                }
            });
        }

        mCountryCode.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mListCountry!=null)
                {
                    new AlertDialog.Builder(mCtx).setItems(mListCountry, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(mListCode!=null)
                            {
                                UserCode = mListCode[which];
                                mCountryCode.setText("+" + UserCode);
                            }
                        }
                    })
                    .show();
                }
            }
        });

        try
        {
            Calendar mCalendar = new GregorianCalendar();
            TimeZone mTimeZone = mCalendar.getTimeZone();
            mGMT = mTimeZone.getRawOffset() / 1000;
        }
        catch(Exception e)
        {
        }

        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.edit_profile));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.done));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                showProgressDialog();

                if(mOpen.getText().toString().compareTo(Singleton.preferences.getString(Constants.OPEN_ID, ""))==0)
                {
                    UpdateInfo();
                }
                else
                {
                    if(mOpen.getText().toString().length() > 1 && mOpen.getText().toString().length() != 0)
                    {
                        ApiManager.checkOpenIDAvailable(mCtx, mOpen.getText().toString(), new ApiManager.CheckOpenIDAvaliableCallback()
                        {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if (success)
                                {
                                    ApiManager.updateOpenID(mCtx, mOpen.getText().toString(), new ApiManager.RequestCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success)
                                        {
                                            if (success)
                                            {
                                                Singleton.preferences.edit().putString(Constants.OPEN_ID, mOpen.getText().toString()).commit();
                                                UpdateInfo();
                                            }
                                            else
                                            {
                                                hideProgressDialog();
                                                try{
//                                      showToast(getString(R.string.error_failed));
                                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    hideProgressDialog();
                                    try{
//                                      showToast(getString(R.string.account_same));
                                        Toast.makeText(mCtx, getString(R.string.account_same), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        hideProgressDialog();
                        try{
//                                      showToast(getString(R.string.account_size));
                            Toast.makeText(mCtx, getString(R.string.account_size), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            }
        });

    }

    private void UpdateInfo()
    {
        try
        {
            JSONObject params = new JSONObject();

            params.put("name", mName.getText().toString());
            params.put("website", mWeb.getText().toString());
            params.put("bio", mCaption.getText().toString());

            if (mSex.getText().toString().compareTo(getString(R.string.female)) == 0) params.put("gender", "female");
            else params.put("gender", "male");

            params.put("age", mBirthday.getText().toString());
            params.put("email", mMail.getText().toString());

            params.put("timeZoneOffset", String.valueOf(mGMT));

            if(mPhone.getText().toString().length()!=0)
            {
                params.put("phoneNumber", mPhone.getText().toString());
                params.put("phoneWithCountryCode", UserCode + mPhone.getText().toString());
                params.put("countryCode", UserCode);
            }

            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
            {
                @Override
                public void onResult(boolean success, String message)
                {
                    hideProgressDialog();
                    if(success)
                    {
                        mApplication.getUser().setName(mName.getText().toString());
                        mApplication.getUser().setWebsite(mWeb.getText().toString());
                        mApplication.getUser().setBio(mCaption.getText().toString());
                        if (mSex.getText().toString().compareTo(getString(R.string.female)) == 0) mApplication.getUser().setGender("female");
                        else mApplication.getUser().setGender("male");
                        mApplication.getUser().setAge(Integer.valueOf(mBirthday.getText().toString()));
                        mApplication.getUser().setEmail(mMail.getText().toString());
                        mApplication.getUser().setPhoneNumber(mPhone.getText().toString());
                        if(mPhone.getText().toString().length()!=0) mApplication.getUser().setCountryCode(UserCode);

                        setConfig(Constants.PROFILE_NAME, String.valueOf(mName.getText().toString()));
                        setConfig(Constants.PROFILE_WEB, String.valueOf(mWeb.getText().toString()));
                        setConfig(Constants.PROFILE_BIO, String.valueOf(mCaption.getText().toString()));

                        if (mSex.getText().toString().compareTo(getString(R.string.female)) == 0) setConfig(Constants.PROFILE_SEX, "female");
                        else setConfig(Constants.PROFILE_SEX, "male");
                        setConfig(Constants.PROFILE_AGE, mBirthday.getText().toString());
                        setConfig(Constants.PROFILE_EMAIL,mMail.getText().toString());
                        setConfig(Constants.PROFILE_COUNTRY_CODE, UserCode);
                        if(mPhone.getText().toString().length()!=0) setConfig(Constants.PROFILE_PHONE,mPhone.getText().toString());
                        else setConfig(Constants.PROFILE_PHONE, "");

                        mCtx.finish();
                    }
                    else {
                        try{
//                          showToast(getString(R.string.error_failed));
                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });
        }
        catch (Exception e)
        {
            hideProgressDialog();
            try{
//                          showToast(getString(R.string.error_failed));
                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
            }
            catch (Exception x){
            }
        }
    }

    public interface SingleItemPickerCallback
    {
        public void onResult(boolean done, int selectedItem);
    }

    public void showSingleItemPicker(String title, final ArrayList<String> items, int initialSelectedItemIndex, final SingleItemPickerCallback callback)
    {
        try
        {
            final Dialog dialog = new Dialog(this, R.style.MyDialog);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.item_picker_dialog);

            RelativeLayout rootView = (RelativeLayout) dialog.findViewById(R.id.rootView);
            TextView titleTextView = (TextView) dialog.findViewById(R.id.titleTextView);
            Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
            Button okButton = (Button) dialog.findViewById(R.id.okButton);
            final ListView itemListView = (ListView) dialog.findViewById(R.id.itemListView);

            final HashSet<Integer> selectedIndexSet = new HashSet<Integer>();
            selectedIndexSet.add(new Integer(initialSelectedItemIndex));

            itemListView.setAdapter(new BaseAdapter() {
                @Override
                public View getView(final int position, View rowView, ViewGroup parent) {
                    if(rowView==null||rowView.getClass().equals(ItemRow.class)==false) {
                        rowView = new ItemRow(mCtx);
                    }

                    ItemRow itemRow = (ItemRow) rowView;

                    itemRow.nameTextView.setText(items.get(position));

                    if(selectedIndexSet.contains(new Integer(position))) {
                        itemRow.selectionIndicatorImageView.setImageResource(R.drawable.select_on);
                    } else {
                        itemRow.selectionIndicatorImageView.setImageResource(R.drawable.select_off);
                    }

                    itemRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedIndexSet.clear();
                            selectedIndexSet.add(new Integer(position));

                            notifyDataSetChanged();
                        }
                    });

                    return itemRow;
                }

                @Override
                public long getItemId(int position) {
                    return 0;
                }

                @Override
                public Object getItem(int position) {
                    return null;
                }

                @Override
                public int getCount() {
                    return items.size();
                }
            });

            titleTextView.setText(title);

            rootView.getLayoutParams().width = Singleton.SCREEN_WIDTH * 8 / 10;

            cancelButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        callback.onResult(false, selectedIndexSet.iterator().next());
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    dialog.dismiss();
                }
            });

            okButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        callback.onResult(true, selectedIndexSet.iterator().next());
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                    dialog.dismiss();
                }
            });

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    callback.onResult(false, selectedIndexSet.iterator().next());

                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {
            Singleton.log(e.toString());
        }
    }

    public String ReadFromfile(String fileName, Context context)
    {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try
        {
            fIn = context.getResources().getAssets().open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null)
            {
                returnString.append(line);
            }
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally
        {
            try
            {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            }
            catch (Exception e2)
            {
                e2.getMessage();
            }
        }

        return returnString.toString();
    }

    public void hideProgressDialog()
    {
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showProgressDialog()
    {
        try
        {
            if(progressDialog==null)
            {
                progressDialog = ProgressDialog.show(this, "", getString(R.string.processing));
            }

            progressDialog.setCancelable(true);
        }
        catch (Exception e)
        {
        }
    }

    public String getConfig(String key, String def)
    {
        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
        return settings.getString(key, def);
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }
}
