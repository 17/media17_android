package com.machipopo.media17;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.model.SuggestedUsersModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.machipopo.media17.utils.ShareUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tencent.connect.UserInfo;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by POPO on 7/25/15.
 */
public class FollowSuggestedActivity extends BaseActivity
{
    private FollowSuggestedActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;
    private ArrayList<SuggestedUsersModel> mSuggesteds = new ArrayList<SuggestedUsersModel>();
    private PullToRefreshListView mList;
    private FriendAdapter mFriendAdapter;
    private DisplayMetrics mDisplayMetrics;
    private DisplayImageOptions BigOptions, SelfOptions;
    private ImageView mNoData;
    private ProgressBar mProgress;

//    private Boolean isFetchingData = false;
//    private Boolean noMoreData = false;

    private Boolean toUser = false;
    private int POS = 0;

    private Boolean mGuest = false;

    //event tracking
    private int enterDiscoverUser, leaveDiscoverUser;
    private int listViewReflashCount = 0;

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.follow_suggested_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("guest"))
            {
                mGuest = mBundle.getBoolean("guest");
            }
        }

        mApplication = (Story17Application) mCtx.getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        initTitleBar();

        mList = (PullToRefreshListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mProgress.setVisibility(View.VISIBLE);

        mList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
        {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView)
            {

                //event tracking
                listViewReflashCount++;

                ApiManager.getExploreSuggestedUsers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), Integer.MAX_VALUE, 50, new ApiManager.GetExploreSuggestedUsersCallback()
                {
                    @Override
                    public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested)
                    {
                        mList.onRefreshComplete();
                        if (success && suggested != null)
                        {
                            if (suggested.size() != 0)
                            {
                                mNoData.setVisibility(View.GONE);
                                mSuggesteds.clear();
                                mSuggesteds.addAll(suggested);

                                if(mFriendAdapter!=null) mFriendAdapter = null;
                                mFriendAdapter = new FriendAdapter();
                                mList.setAdapter(mFriendAdapter);
                            }
                            else
                            {
                                mNoData.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });

        //event tracking
        enterDiscoverUser = Singleton.getCurrentTimestamp();
        try{
            LogEventUtil.EnterDiscoverUser(mCtx,mApplication);
        }catch (Exception x)
        {

        }

//        ApiManager.getExploreSuggestedUsers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), Integer.MAX_VALUE, 50, new ApiManager.GetExploreSuggestedUsersCallback()
//        {
//            @Override
//            public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested)
//            {
//                mProgress.setVisibility(View.GONE);
//                if (success && suggested != null)
//                {
//                    if (suggested.size() != 0)
//                    {
//                        mSuggesteds.clear();
//                        mSuggesteds.addAll(suggested);
//
//                        mFriendAdapter = new FriendAdapter();
//                        mList.setAdapter(mFriendAdapter);
//                    }
//                    else
//                    {
//                        mNoData.setVisibility(View.VISIBLE);
//                    }
//                }
//                else
//                {
//                    mNoData.setVisibility(View.VISIBLE);
//                }
//            }
//        });

        mProgress.setVisibility(View.GONE);
        if(mApplication!=null && mApplication.getSuggested().size()!=0)
        {
            mNoData.setVisibility(View.GONE);
            mSuggesteds.clear();
            mSuggesteds.addAll(mApplication.getSuggested());
            mFriendAdapter = new FriendAdapter();
            mList.setAdapter(mFriendAdapter);
        }
        else
        {
            mNoData.setVisibility(View.VISIBLE);
        }

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_c)
        .showImageForEmptyUri(R.drawable.placehold_c)
        .showImageOnFail(R.drawable.placehold_c)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.super_people));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mGuest)
                {
//                    Intent intent = new Intent();
//                    intent.setClass(mCtx, GuestActivity.class);
//                    startActivity(intent);
                    mCtx.finish();
                }
                else mCtx.finish();
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            hideKeyboard();

            if(mGuest)
            {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
            else mCtx.finish();

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

    private class FriendAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mSuggesteds.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.follow_suggeste_row, null);
                holder.img = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.follow = (Button) convertView.findViewById(R.id.follow);

                holder.pic_layout = (LinearLayout) convertView.findViewById(R.id.pic_layout);
                holder.pic1 = (ImageView) convertView.findViewById(R.id.pic1);
                holder.pic2 = (ImageView) convertView.findViewById(R.id.pic2);
                holder.pic3 = (ImageView) convertView.findViewById(R.id.pic3);

                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);

                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(position).getPicture()), holder.img,SelfOptions);

            holder.name.setText(mSuggesteds.get(position).getOpenID());
            holder.dio.setText(mSuggesteds.get(position).getName());
            final int pos = position;

            if(mSuggesteds.get(position).getIsFollowing()==1)
            {
                holder.follow.setText(getString(R.string.user_profile_following));
                holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
                holder.follow.setTextColor(Color.WHITE);
            }
            else
            {
                if(mSuggesteds.get(pos).getFollowRequestTime()!=0)
                {
                    holder.follow.setText(getString(R.string.private_mode_request_send));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
                else
                {
                    holder.follow.setText("+ " + getString(R.string.user_profile_follow));
                    holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                    holder.follow.setTextColor(getResources().getColor(R.color.content_text_color));
                }
            }

            final Button btn = holder.follow;
            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    //event tracking
                    try{
                        LogEventUtil.FollowChoicedUser(mCtx,mApplication,mSuggesteds.get(pos).getUserID());
                    }catch (Exception x)
                    {

                    }

                    if(mSuggesteds.get(position).getIsFollowing()==1)
                    {
                        mSuggesteds.get(position).setIsFollowing(0);
                        btn.setText("+ " + getString(R.string.user_profile_follow));
                        btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                        btn.setTextColor(getResources().getColor(R.color.content_text_color));

                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mSuggesteds.get(pos).getUserID());
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mSuggesteds.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if (success)
                                {

                                }
                            }
                        });
                    }
                    else
                    {
                        if(mSuggesteds.get(pos).getFollowRequestTime()!=0)
                        {
                            btn.setText("+ " + getString(R.string.user_profile_follow));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                            mSuggesteds.get(pos).setIsFollowing(0);
                            mSuggesteds.get(pos).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mSuggesteds.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                                                showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }
                                return ;
                            }

                            if(mSuggesteds.get(pos).getPrivacyMode().compareTo("private")==0)
                            {
                                btn.setText(getString(R.string.private_mode_request_send));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                mSuggesteds.get(pos).setIsFollowing(0);
                                mSuggesteds.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mSuggesteds.get(pos).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mSuggesteds.get(pos).setIsFollowing(0);
                                            mSuggesteds.get(pos).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                mSuggesteds.get(position).setIsFollowing(1);
                                btn.setText(getString(R.string.user_profile_following));
                                btn.setBackgroundResource(R.drawable.btn_green_selector);
                                btn.setTextColor(Color.WHITE);

                                try{
                                LogEventUtil.FollowUser(mCtx, mApplication, mSuggesteds.get(pos).getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mSuggesteds.get(pos).getUserID(), new ApiManager.FollowUserActionCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {

                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            int height = (int) mDisplayMetrics.widthPixels/3;
            holder.pic_layout.getLayoutParams().height = height;
            int size = mSuggesteds.get(position).getPostInfo().size();

            if(size>=3)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(0).getPicture()), holder.pic1,BigOptions);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(1).getPicture()), holder.pic2,BigOptions);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(2).getPicture()), holder.pic3,BigOptions);
            }
            else if(size==2)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(0).getPicture()), holder.pic1,BigOptions);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(1).getPicture()), holder.pic2,BigOptions);
                holder.pic3.setImageResource(R.drawable.placehold_s);
            }
            else if(size==1)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggesteds.get(pos).getPostInfo().get(0).getPicture()), holder.pic1,BigOptions);
                holder.pic2.setImageResource(R.drawable.placehold_s);
                holder.pic3.setImageResource(R.drawable.placehold_s);
            }

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(mSuggesteds.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        toUser = true;
                        POS = position;
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mSuggesteds.get(position).getName());
                        intent.putExtra("picture", mSuggesteds.get(position).getPicture());
                        intent.putExtra("isfollowing", mSuggesteds.get(position).getIsFollowing());
                        intent.putExtra("post", mSuggesteds.get(position).getPostCount());
                        intent.putExtra("follow", mSuggesteds.get(position).getFollowerCount());
                        intent.putExtra("following", mSuggesteds.get(position).getFollowingCount());
                        intent.putExtra("open", mSuggesteds.get(position).getOpenID());
                        intent.putExtra("bio", mSuggesteds.get(position).getBio());
                        intent.putExtra("targetUserID", mSuggesteds.get(position).getUserID());
                        intent.putExtra("web", mSuggesteds.get(position).getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            holder.img.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(mSuggesteds.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        toUser = true;
                        POS = position;
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mSuggesteds.get(position).getName());
                        intent.putExtra("picture", mSuggesteds.get(position).getPicture());
                        intent.putExtra("isfollowing", mSuggesteds.get(position).getIsFollowing());
                        intent.putExtra("post", mSuggesteds.get(position).getPostCount());
                        intent.putExtra("follow", mSuggesteds.get(position).getFollowerCount());
                        intent.putExtra("following", mSuggesteds.get(position).getFollowingCount());
                        intent.putExtra("open", mSuggesteds.get(position).getOpenID());
                        intent.putExtra("bio", mSuggesteds.get(position).getBio());
                        intent.putExtra("targetUserID", mSuggesteds.get(position).getUserID());
                        intent.putExtra("web", mSuggesteds.get(position).getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            holder.pic_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mSuggesteds.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        toUser = true;
                        POS = position;
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mSuggesteds.get(position).getName());
                        intent.putExtra("picture", mSuggesteds.get(position).getPicture());
                        intent.putExtra("isfollowing", mSuggesteds.get(position).getIsFollowing());
                        intent.putExtra("post", mSuggesteds.get(position).getPostCount());
                        intent.putExtra("follow", mSuggesteds.get(position).getFollowerCount());
                        intent.putExtra("following", mSuggesteds.get(position).getFollowingCount());
                        intent.putExtra("open", mSuggesteds.get(position).getOpenID());
                        intent.putExtra("bio", mSuggesteds.get(position).getBio());
                        intent.putExtra("targetUserID", mSuggesteds.get(position).getUserID());
                        intent.putExtra("web", mSuggesteds.get(position).getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            if(mSuggesteds.get(position).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

//            if(position>=getCount()-5)
//            {
//                LoadData(false);
//            }

            return convertView;
        }
    }

//    public void LoadData(final boolean refresh)
//    {
//        if(isFetchingData)
//        {
//            return;
//        }
//
//        if(refresh)
//        {
//            noMoreData = false;
//        }
//
//        if(noMoreData)
//        {
//            return;
//        }
//
//        isFetchingData = true;
//
//        ApiManager.getExploreSuggestedUsers(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mSuggesteds.get(mSuggesteds.size()-1).getLastLogin(), 20, new ApiManager.GetExploreSuggestedUsersCallback() {
//            @Override
//            public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested) {
//                isFetchingData = false;
//
//                if (success && suggested != null) {
//                    if (suggested.size() != 0) {
//                        mSuggesteds.addAll(suggested);
//
//                        if (suggested.size() < 18) {
//                            noMoreData = true;
//                        }
//
//                        mFriendAdapter.notifyDataSetChanged();
//                    }
//                } else Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private class ViewHolder
    {
        ImageView img;
        TextView name;
        TextView dio;
        Button follow;

        LinearLayout pic_layout;
        ImageView pic1;
        ImageView pic2;
        ImageView pic3;

        ImageView verifie;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(toUser)
        {
            if(mSuggesteds!=null)
            {
                if(mSuggesteds.size() > POS)
                {
                    mSuggesteds.get(POS).setIsFollowing(mApplication.getIsFollow());
                    mFriendAdapter.notifyDataSetChanged();
                    toUser = false;
                }
            }
        }

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());

        if(mApplication!=null){
            if(mApplication.getGuestFinish()) mCtx.finish();
            else if(mApplication.getMenuFinish()) mCtx.finish();
        }
    }

    Dialog GuestDialog;
    private SharedPreferences sharedPreferences;
    private CallbackManager callbackManager;
    private void showGuestLogin()
    {
        if(GuestDialog!=null) GuestDialog = null;

        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        GuestDialog.setContentView(R.layout.guest_login_dialog);
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        Button mSingup = (Button)GuestDialog.findViewById(R.id.singup);
        mSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();
                dialog_singup_view();
            }
        });

        Button mLogin = (Button)GuestDialog.findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(mCtx,LoginActivity_V3.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.btnFBLogin);
        mFB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        GuestDialog.show();
    }

    private void dialog_singup_view()
    {
        if(GuestDialog!=null) GuestDialog = null;
        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        if(Constants.INTERNATIONAL_VERSION){
            GuestDialog.setContentView(R.layout.guest_singup_dialog_google);
        }
        else{
            GuestDialog.setContentView(R.layout.guest_singup_dialog_china);
        }
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout mWeibo = (LinearLayout)GuestDialog.findViewById(R.id.weibo_layout);
        mWeibo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Umeng monitor
                String Umeng_id="GuestModeSignupWeibo";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                GuestDialog.dismiss();
                showWebio();
            }
        });

        LinearLayout mQQ = (LinearLayout)GuestDialog.findViewById(R.id.qq_layout);
        mQQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupQQ";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                mTencent = Tencent.createInstance(Constants.QQAppid, mCtx);
                mTencent.login(mCtx, "all", loginListener);
            }
        });

        LinearLayout mWechat = (LinearLayout)GuestDialog.findViewById(R.id.wechat_layout);
        mWechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                if (ShareUtil.isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI"))
                {
                //Umeng monitor
                String Umeng_id="GuestModeSignupWechat";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);


                IWXAPI api;
                api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
                api.registerApp(Constants.WECHAT_APP_KEY);
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "wechat_sdk_demo";
                api.sendReq(req);
                }
                else {
                    Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                }
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.fb_layout);
        mFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                Intent intent;

                //Umeng monitor
                String Umeng_id_FB="FBlogin";
                HashMap<String,String> mHashMap_FB = new HashMap<String,String>();
                mHashMap_FB.put(Umeng_id_FB, Umeng_id_FB);
                MobclickAgent.onEventValue(mCtx, Umeng_id_FB, mHashMap_FB, 0);

                FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(final LoginResult loginResult) {
                                final String token = loginResult.getAccessToken().getToken();

                                GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject user, GraphResponse response) {
                                        if (user != null) {
                                            try {
                                                showProgressDialog();
                                                final String id = user.optString("id");
                                                final String name = user.optString("name");
//                                                    Log.e("DDD", "ID: " + id);

                                                // Download Profile photo
                                                DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, id);
                                                downloadFBProfileImage.execute();

                                                ApiManager.checkFacebookIDAvailable(mCtx, id, new ApiManager.checkFacebookIDAvailableCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String message) {
                                                        if (success) {

                                                            // Download user profile image
//                                                                DownloadFBProfileImage download = new DownloadFBProfileImage(mCtx, id);
//                                                                download.execute();

                                                            //Signup
                                                            GraphRequest request = GraphRequest.newMeRequest(
                                                                    loginResult.getAccessToken(),
                                                                    new GraphRequest.GraphJSONObjectCallback() {
                                                                        @Override
                                                                        public void onCompleted(
                                                                                JSONObject object,
                                                                                GraphResponse response) {
                                                                            // Application code
                                                                            String email = object.optString("email");
//                                                                                Log.e("DDD", "EMAIL: " + email);


                                                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, id).commit();
                                                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                            sharedPreferences.edit().putString("signup_type", "fb").commit();
                                                                            sharedPreferences.edit().putString("signup_token", token).commit();

                                                                            if (null != email) {
                                                                                if (email.contains("@")) {
                                                                                    String username = email.substring(0, email.indexOf("@"));
                                                                                    sharedPreferences.edit().putString(Constants.EMAIL, email).commit();
                                                                                    sharedPreferences.edit().putString(Constants.NAME, username).commit();
                                                                                }
                                                                            }
                                                                            hideProgressDialog();
                                                                            startActivity(new Intent(mCtx, SignupActivityV2.class));
//                                                                            mCtx.finish();
                                                                        }
                                                                    });

                                                            Bundle parameters = new Bundle();
                                                            parameters.putString("fields", "email");
                                                            request.setParameters(parameters);
                                                            request.executeAsync();

                                                        } else {
                                                            //login directly
                                                            ApiManager.loginAction2(mCtx, "", "", id, new ApiManager.LoginAction2Callback() {
                                                                @Override
                                                                public void onResult(boolean success, String message, UserModel user) {
                                                                    hideProgressDialog();

                                                                    if (success) {
                                                                        if (message.equals("ok")) {

                                                                            Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                            mStory17Application.setUser(user);

                                                                            Intent intent = new Intent();
                                                                            intent.setClass(mCtx, MenuActivity.class);
                                                                            startActivity(intent);
                                                                            mCtx.finish();
                                                                        } else if (message.equals("freezed"))
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                        else
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                    } else {
                                                                        showNetworkUnstableToast();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });


                                            } catch (Exception e) {
                                                hideProgressDialog();
                                                try {
//                              showToast(getString(R.string.failed));
                                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                } catch (Exception x) {
                                                }
                                            }
                                        }
                                    }
                                }).executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        });
            }
        });

        LinearLayout mMessage = (LinearLayout)GuestDialog.findViewById(R.id.messag_layout);
        mMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupSMS";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);


                sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                sharedPreferences.edit().putString("signup_type", "message").commit();
                sharedPreferences.edit().putString("signup_token", "").commit();
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(mCtx, SignupActivityV2.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        Button mEnd = (Button)GuestDialog.findViewById(R.id.end);
        mEnd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();

            }
        });

        GuestDialog.show();
    }

    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        private final String fbImageDownlaodUrl = "https://graph.facebook.com/";
        private final String attrs = "/picture?width=1080&height=1080";
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

            this.url = fbImageDownlaodUrl + userID + attrs;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {

        }

    }

    private Dialog mWeiboDialog;
    private void showWebio()
    {
        if(mWeiboDialog!=null) mWeiboDialog = null;
        mWeiboDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        mWeiboDialog.setContentView(R.layout.instagram_dialog);
        final WebView mWeb = (WebView) mWeiboDialog.findViewById(R.id.web);
        final ProgressBar mProgress = (ProgressBar) mWeiboDialog.findViewById(R.id.progress);
        mWeb.getSettings().setJavaScriptEnabled(true);
        mWeiboDialog.setCancelable(true);
        mWeb.loadUrl("https://api.weibo.com/oauth2/authorize?client_id=" + Constants.WEIBO_APP_KEY + "&response_type=code&redirect_uri=" + Constants.WEIBO_REDIRECT_URL);
        mWeb.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                super.onPageStarted(view, url, favicon);
                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);

                if(url.contains(Constants.IG_REDIRECT_URI) && url.contains("code="))
                {
                    mWeiboDialog.dismiss();
                    showProgressDialog();

                    int pos = url.indexOf("=") + 1;
                    String token = url.substring(pos,url.length());
                    try
                    {
                        OkHttpClient mClient = new OkHttpClient();

                        RequestBody body = new FormEncodingBuilder()
                                .add("client_id", Constants.WEIBO_APP_KEY)
                                .add("client_secret", Constants.WEIBO_SECRET)
                                .add("grant_type", "authorization_code")
                                .add("code", token)
                                .add("redirect_uri", Constants.WEIBO_REDIRECT_URL)
                                .build();

                        Request request = new Request.Builder().url("https://api.weibo.com/oauth2/access_token").post(body).build();
                        mClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful())
                                {
                                    try
                                    {
                                        JSONObject data = new JSONObject(response.body().string());
                                        final String token = data.getString("access_token");
                                        final String uid = data.getString("uid");

                                        OkHttpClient client = new OkHttpClient();
                                        Request request = new Request.Builder().url("https://api.weibo.com/2/users/show.json?access_token=" + token + "&uid=" + uid).build();
                                        client.newCall(request).enqueue(new Callback()
                                        {
                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                hideProgressDialog();
                                                showToast(getString(R.string.failed));
                                            }

                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if(response.isSuccessful())
                                                {
                                                    try
                                                    {
                                                        JSONObject info = new JSONObject(response.body().string());
                                                        final int id = info.getInt("id");
                                                        final String screen_name = info.getString("screen_name");
                                                        final String pic = info.getString("profile_image_url");

                                                        DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                                        downloadFBProfileImage.execute();

                                                        ApiManager.checkWeiboIDAvailable(mCtx, uid/*String.valueOf(id)*/, new ApiManager.checkFacebookIDAvailableCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                if (success) {
                                                                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, String.valueOf(id)).commit();
                                                                    sharedPreferences.edit().putString(Constants.FULL_NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putString(Constants.NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                    sharedPreferences.edit().putString("is_weibo", "weibo").commit();

                                                                    sharedPreferences.edit().putString("signup_type", "weibo").commit();
                                                                    sharedPreferences.edit().putString("signup_token", token).commit();

                                                                    hideProgressDialog();
                                                                    Intent intent = new Intent();
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                    intent.setClass(mCtx, SignupActivityV2.class);
                                                                    startActivity(intent);
//                                                                    mCtx.finish();
                                                                } else {
                                                                    ApiManager.loginActionChina(mCtx, "", "", "weiboID", uid, new ApiManager.LoginAction2Callback() {
                                                                        @Override
                                                                        public void onResult(boolean success, String message, UserModel user) {
                                                                            hideProgressDialog();

                                                                            if (success) {
                                                                                if (message.equals("ok")) {

                                                                                    mApplication.setUser(user);

                                                                                    Intent intent = new Intent();
                                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                                    intent.setClass(mCtx, MenuActivity.class);
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                } else if (message.equals("freezed"))
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                                else
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                            } else {
                                                                                showNetworkUnstableToast();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                    catch (JSONException e)
                                                    {
                                                        hideProgressDialog();
                                                        showToast(getString(R.string.failed));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    catch (JSONException e)
                                    {
                                        hideProgressDialog();
                                        showToast(getString(R.string.failed));
                                    }
                                }
                                else
                                {
                                    hideProgressDialog();
                                    showToast(getString(R.string.failed));
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        hideProgressDialog();
                        showToast(getString(R.string.failed));
                    }
                }
                else
                {
//                            mWeiboDialog.dismiss();
//                            hideProgressDialog();
//                            showToast(getString(R.string.failed));
                }
            }
        });
        try{
            mWeb.clearCache(true);
            mWeb.getSettings().setCacheMode(WebView.PERSISTENT_NO_CACHE);
        }
        catch (Exception e){
        }
        mWeiboDialog.show();
    }

    //QQ-------

    public static Tencent mTencent;

    private class BaseUiListener implements IUiListener
    {

        @Override
        public void onComplete(Object response) {
            if (null == response) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            if (null != jsonResponse && jsonResponse.length() == 0) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }

            doComplete((JSONObject)response);
        }

        protected void doComplete(JSONObject values) {

        }

        @Override
        public void onError(UiError e) {
            hideProgressDialog();
            showToast(getString(R.string.failed));
        }

        @Override
        public void onCancel() {
            hideProgressDialog();
        }
    }

    private IUiListener loginListener = new BaseUiListener()
    {
        @Override
        protected void doComplete(JSONObject jsonObject)
        {
            try
            {
                final String token = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_ACCESS_TOKEN);
                String expires = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_EXPIRES_IN);
                final String openId = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_OPEN_ID);

                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);

                if (mTencent != null && mTencent.isSessionValid())
                {
                    IUiListener listener = new IUiListener()
                    {
                        @Override
                        public void onError(UiError e) {
                            hideProgressDialog();
                            showToast(getString(R.string.failed));
                        }

                        @Override
                        public void onComplete(final Object response)
                        {
                            try
                            {
                                JSONObject json = (JSONObject)response;

                                final String name = json.getString("nickname");
                                String pic = json.getString("figureurl_qq_1");

                                DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                downloadFBProfileImage.execute();

                                ApiManager.checkQQIDAvailable(mCtx, openId, new ApiManager.checkFacebookIDAvailableCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, openId).commit();
                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                            sharedPreferences.edit().putString(Constants.NAME, name).commit();
                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();
                                            sharedPreferences.edit().putString("signup_type", "qq").commit();
                                            sharedPreferences.edit().putString("signup_token", token).commit();
                                            hideProgressDialog();
                                            Intent intent = new Intent();
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setClass(mCtx, SignupActivityV2.class);
                                            startActivity(intent);
//                                            mCtx.finish();
                                        }
                                        else
                                        {
                                            ApiManager.loginActionChina(mCtx, "", "", "qqID", openId, new ApiManager.LoginAction2Callback() {
                                                @Override
                                                public void onResult(boolean success, String message, UserModel user) {
                                                    hideProgressDialog();

                                                    if (success) {
                                                        if (message.equals("ok")) {

                                                            mApplication.setUser(user);

                                                            Intent intent = new Intent();
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            intent.setClass(mCtx, MenuActivity.class);
                                                            startActivity(intent);
                                                            mCtx.finish();
                                                        } else if (message.equals("freezed"))
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                        else
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                    } else {
                                                        showNetworkUnstableToast();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }
                        }

                        @Override
                        public void onCancel() {
                            hideProgressDialog();
                        }
                    };

                    UserInfo mInfo = new UserInfo(mCtx, mTencent.getQQToken());
                    mInfo.getUserInfo(listener);
                }
            }
            catch(Exception e) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
            }
        }
    };

    class DownloadProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        String url = "";

        public DownloadProfileImage(Context context, String userID) {
            this.context = context;

            this.url = userID.substring(0,userID.length()-2) + 640;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {
//                    java.net.URL url = new java.net.URL(this.url);
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setDoInput(true);
//                    connection.connect();

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        leaveDiscoverUser=Singleton.getCurrentTimestamp();

        try {
            if(mTencent!=null) mTencent.logout(mCtx);

            //event tracking
            LogEventUtil.LeaveDiscoverUser(mCtx,mApplication,mSuggesteds.size(),leaveDiscoverUser-enterDiscoverUser,listViewReflashCount);
        }
        catch (Exception e) {
        }
        listViewReflashCount = 0;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == com.tencent.connect.common.Constants.REQUEST_LOGIN || requestCode == com.tencent.connect.common.Constants.REQUEST_APPBAR)
        {
            Tencent.onActivityResultData(requestCode, resultCode, data, loginListener);
        }

        try {
            if(callbackManager!=null) callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        catch (Exception e) {
        }
    }
}
