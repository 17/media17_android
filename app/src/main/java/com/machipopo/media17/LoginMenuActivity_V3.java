package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.machipopo.media17.model.UserModel;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;

public class LoginMenuActivity_V3 extends BaseActivity {

    private LoginMenuActivity_V3 mCtx = this;
    private LinearLayout btnDirectLogin, btnFBLogin;//, btnWeiboLogin;
    private RelativeLayout btnRegister;
    private CallbackManager callbackManager;

    private int screenWidth, screenHeight;
    private float logoEndPosition, sloganPosition;
    private SharedPreferences sharedPreferences;


    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_menu_activity_v3);

        initTitleBar();

        btnDirectLogin = (LinearLayout)findViewById(R.id.btnDirectLogin);
        btnRegister = (RelativeLayout)findViewById(R.id.btnRegister);
        btnFBLogin = (LinearLayout)findViewById(R.id.btnFBLogin);
//        btnWeiboLogin = (LinearLayout)findViewById(R.id.btnWeiboLogin);

        //Listener
        btnDirectLogin.setOnClickListener(ButtonClickEventListener);
        btnRegister.setOnClickListener(ButtonClickEventListener);
        btnFBLogin.setOnClickListener(ButtonClickEventListener);

        //get screen size
        //screenWidth = getResources().getDisplayMetrics().widthPixels;
        screenHeight = getResources().getDisplayMetrics().heightPixels;

        //calculate init position
        logoEndPosition = screenHeight/8f;
        //sloganPosition = (screenHeight/2f - logoEndPosition) / 2f + logoEndPosition;


        hideKeyboard();


    }

    private void initTitleBar()
    {
        //((TextView) findViewById(R.id.title_name)).setText(getString(R.string.forgot_password));
        ((TextView) findViewById(R.id.title_name)).setText("建立賬戶");

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_back);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(callbackManager!=null) {
            try
            {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
            catch (Exception e)
            {

            }
        }

//        mBtnTwitter.onActivityResult(requestCode, resultCode, data);
    }

    View.OnClickListener ButtonClickEventListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            Intent intent;
            switch(v.getId()){
                case R.id.btnDirectLogin:
                    //Hunter for new login
                    //startActivity(new Intent(mCtx, LoginActivity_V2.class));
                    startActivity(new Intent(mCtx, LoginActivity_V3.class));
                    mCtx.finish();
                    break;
                case R.id.btnRegister:
                    //Umeng monitor
                    String Umeng_id="Register";
                    HashMap<String,String> mHashMap = new HashMap<String,String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, "").commit();
                    sharedPreferences.edit().putString(Constants.NAME, "").commit();
                    sharedPreferences.edit().putBoolean("fblogin", false).commit();

                    startActivity(new Intent(mCtx,SignupActivityV2.class));
                    mCtx.finish();
                    break;
                case R.id.btnFBLogin:
                    //Umeng monitor
                    String Umeng_id_FB="FBlogin";
                    HashMap<String,String> mHashMap_FB = new HashMap<String,String>();
                    mHashMap_FB.put(Umeng_id_FB, Umeng_id_FB);
                    MobclickAgent.onEventValue(mCtx,Umeng_id_FB,mHashMap_FB,0);

                    FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
                    callbackManager = CallbackManager.Factory.create();
                    LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

                    LoginManager.getInstance().registerCallback(callbackManager,
                            new FacebookCallback<LoginResult>()
                            {
                                @Override
                                public void onSuccess(final LoginResult loginResult)
                                {
                                    final String token = loginResult.getAccessToken().getToken();

                                    GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback()
                                    {
                                        @Override
                                        public void onCompleted(JSONObject user, GraphResponse response)
                                        {
                                            if (user != null)
                                            {
                                                try
                                                {
                                                    showProgressDialog();
                                                    final String id = user.optString("id");
                                                    final String name = user.optString("name");
//                                                    Log.e("DDD", "ID: " + id);

                                                    // Download Profile photo
                                                    DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, id);
                                                    downloadFBProfileImage.execute();

                                                    ApiManager.checkFacebookIDAvailable(mCtx, id, new ApiManager.checkFacebookIDAvailableCallback() {
                                                        @Override
                                                        public void onResult(boolean success, String message) {
                                                            if(success){

                                                                // Download user profile image
//                                                                DownloadFBProfileImage download = new DownloadFBProfileImage(mCtx, id);
//                                                                download.execute();

                                                                //Signup
                                                                GraphRequest request = GraphRequest.newMeRequest(
                                                                        loginResult.getAccessToken(),
                                                                        new GraphRequest.GraphJSONObjectCallback() {
                                                                            @Override
                                                                            public void onCompleted(
                                                                                    JSONObject object,
                                                                                    GraphResponse response) {
                                                                                // Application code
                                                                                String email = object.optString("email");
//                                                                                Log.e("DDD", "EMAIL: " + email);


                                                                                sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                                sharedPreferences.edit().putString(Constants.FACEBOOK_ID, id).commit();
                                                                                sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                                                                sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                                if(null != email) {
                                                                                    if(email.contains("@")) {
                                                                                        String username = email.substring(0, email.indexOf("@"));
                                                                                        sharedPreferences.edit().putString(Constants.EMAIL, email).commit();
                                                                                        sharedPreferences.edit().putString(Constants.NAME, username).commit();
                                                                                    }
                                                                                }
                                                                                hideProgressDialog();
                                                                                startActivity(new Intent(mCtx, SignupActivityV2.class));
                                                                                mCtx.finish();
                                                                            }
                                                                        });

                                                                Bundle parameters = new Bundle();
                                                                parameters.putString("fields", "email");
                                                                request.setParameters(parameters);
                                                                request.executeAsync();

                                                            }else{
                                                                //login directly
                                                                ApiManager.loginAction2(mCtx, "", "", id, new ApiManager.LoginAction2Callback() {
                                                                    @Override
                                                                    public void onResult(boolean success, String message, UserModel user) {
                                                                        hideProgressDialog();

                                                                        if (success) {
                                                                            if (message.equals("ok")) {

                                                                                Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                                mStory17Application.setUser(user);

                                                                                Intent intent = new Intent();
                                                                                intent.setClass(mCtx, MenuActivity.class);
                                                                                startActivity(intent);
                                                                                mCtx.finish();
                                                                            } else if (message.equals("freezed"))
                                                                                showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                            else
                                                                                showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                        } else {
                                                                            showNetworkUnstableToast();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });




                                                }
                                                catch (Exception e)
                                                {
                                                    hideProgressDialog();
                                                    try{
//                              showToast(getString(R.string.failed));
                                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                            }
                                        }
                                    }).executeAsync();
                                }

                                @Override
                                public void onCancel()
                                {
                                    hideProgressDialog();
                                    try{
//                              showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }

                                @Override
                                public void onError(FacebookException exception)
                                {
                                    hideProgressDialog();
                                    try{
//                              showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            });
                    break;
                default:
                    break;
            }

        }
    };


    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }


    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        private final String fbImageDownlaodUrl = "https://graph.facebook.com/";
        private final String attrs = "/picture?width=1080&height=1080";
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

            this.url = fbImageDownlaodUrl + userID + attrs;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }

    }
}