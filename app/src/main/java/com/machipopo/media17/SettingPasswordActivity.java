package com.machipopo.media17;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by POPO on 6/30/15.
 */
public class SettingPasswordActivity extends BaseNewActivity
{
    private SettingPasswordActivity mCtx = this;
    private InputMethodManager inputMethodManager = null;

    private EditText mNow, mNew, mAgain;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }
//
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_password_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        initTitleBar();

        mNow = (EditText) findViewById(R.id.now_pass);
        mNew = (EditText) findViewById(R.id.new_pass);
        mAgain = (EditText) findViewById(R.id.again_pass);
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.change_password));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                mCtx.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.done));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                if(mNow.getText().toString().length()==0 || mNew.getText().toString().length()==0 || mAgain.getText().toString().length()==0)
                {
                    try{
//                          showToast(getString(R.string.setting_password));
                        Toast.makeText(mCtx, getString(R.string.setting_password), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                    return ;
                }

                if(mNew.getText().toString().compareTo(mAgain.getText().toString())!=0)
                {
                    try{
//                          showToast(getString(R.string.password_same));
                        Toast.makeText(mCtx, getString(R.string.password_same), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                    return ;
                }

                if(mNew.getText().toString().length() < 5)
                {
                    try{
//                          showToast(getString(password_size);
                        Toast.makeText(mCtx, getString(R.string.password_size), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                    return ;
                }

                ApiManager.changePassword(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mNow.getText().toString(), mNew.getText().toString(), new ApiManager.ChangePasswordCallback()
                {
                    @Override
                    public void onResult(boolean success, String message)
                    {
                        if(success)
                        {
                            if(message.contains("ok"))
                            {
                                try{
//                          showToast(getString(password_ok);
                                    Toast.makeText(mCtx, getString(R.string.password_ok), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                                mCtx.finish();
                            }
                            else {
                                try{
//                          showToast(getString(password_error);
                                    Toast.makeText(mCtx, getString(R.string.password_error), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                        else {
                            try{
//                          showToast(getString(error_failed);
                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                        }
                    }
                });
            }
        });
    }
}
