package com.machipopo.media17;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TitleBar extends RelativeLayout {

    private TextView titleTextView;
    private RelativeLayout titleBarContainer;

    public TitleBar(Context context) {
        super(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.crop_title_bar, this, true);
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        titleBarContainer = (RelativeLayout) findViewById(R.id.titleBarContainer);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TitleBar, 0, 0);

        try {
            String titleText = typedArray.getString(R.styleable.TitleBar_titleText);
            titleTextView.setText(titleText);
        } finally {
            typedArray.recycle();
        }
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setTitleText(CharSequence titleText) {
        titleTextView.setText(titleText);
    }

    public void setTitleTextSize(int size){
        titleTextView.setTextSize(size);
    }

    public void setTitleBarBackground(int resourceID) {
        titleBarContainer.setBackgroundResource(resourceID);
    }
}
