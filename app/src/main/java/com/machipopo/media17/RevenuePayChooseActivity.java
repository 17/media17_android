package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by POPO on 15/8/25.
 */
public class RevenuePayChooseActivity extends BaseActivity
{
    private RevenuePayChooseActivity mCtx = this;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_pay_choose);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        initTitleBar();

        RelativeLayout mPaypal = (RelativeLayout) findViewById(R.id.paypal);
        RelativeLayout mAlipay = (RelativeLayout) findViewById(R.id.alipay);
        TextView mChange = (TextView) findViewById(R.id.change);
        mChange.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        if(getConfig(Constants.REVENUE_PAYPAL).length()!=0)
        {
            mPaypal.setVisibility(View.VISIBLE);
            mAlipay.setVisibility(View.GONE);
            mChange.setText(getString(R.string.revenue_pay_choose_pay_alipay));
        }
        else
        {
            mPaypal.setVisibility(View.GONE);
            mAlipay.setVisibility(View.VISIBLE);
            mChange.setText(getString(R.string.revenue_pay_choose_pay_paypal));
        }

        mPaypal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenuePaySetActivity.class);
                intent.putExtra("paypal", true);
                startActivity(intent);
                mCtx.finish();
            }
        });

        mAlipay.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenuePaySetActivity.class);
                intent.putExtra("paypal",false);
                startActivity(intent);
                mCtx.finish();
            }
        });

        mChange.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(getConfig(Constants.REVENUE_PAYPAL).length()!=0)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx,RevenuePaySetActivity.class);
                    intent.putExtra("paypal",false);
                    startActivity(intent);
                    mCtx.finish();
                }
                else
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx,RevenuePaySetActivity.class);
                    intent.putExtra("paypal", true);
                    startActivity(intent);
                    mCtx.finish();
                }
            }
        });
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_account));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    public String getConfig(String key)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, "");
    }
}
