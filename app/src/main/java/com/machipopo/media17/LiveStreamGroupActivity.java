package com.machipopo.media17;

import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.machipopo.media17.View.SquaredSurfaceView;
import com.machipopo.media17.model.MyEditText;

/**
 * Created by POPO on 2015/10/27.
 */
public class LiveStreamGroupActivity extends BaseActivity implements SurfaceHolder.Callback, Camera.PreviewCallback, MyEditText.EditKeyBackListener
{
    private LiveStreamGroupActivity mCtx = this;
    private Story17Application mApplication;
    private DisplayMetrics mDisplayMetrics;
    private RelativeLayout mArea1,mArea2,mArea3,mArea4;
    private RelativeLayout mSurfaceLayout1,mSurfaceLayout2,mSurfaceLayout3,mSurfaceLayout4;
    private TextView mAllPeople, mNowPeople;
    private ImageView mCamChange, mClose;
    private Gallery mPeoples;
    private TextView mName1, mName2, mName3, mName4;
    private LinearLayout mOpenLayout1, mOpenLayout2, mOpenLayout3, mOpenLayout4;
    private ImageView mLockImg1, mLockImg2, mLockImg3, mLockImg4;
    private TextView mLockText1, mLockText2 , mLockText3 , mLockText4;
    private LinearLayout mJoinLayout1, mJoinLayout2, mJoinLayout3, mJoinLayout4;
    private TextView mJoinText1, mJoinText2, mJoinText3, mJoinText4;
    private ProgressBar mJoinProgress1, mJoinProgress2, mJoinProgress3, mJoinProgress4;
    private Button mJoinBtn1, mJoinBtn2, mJoinBtn3, mJoinBtn4;
    private LinearLayout mFriendJoinLayout1, mFriendJoinLayout2, mFriendJoinLayout3, mFriendJoinLayout4;
    private ImageView mFriendJoinBack1, mFriendJoinBack2, mFriendJoinBack3, mFriendJoinBack4;
    private ImageView mFriendJoinNext1, mFriendJoinNext2, mFriendJoinNext3, mFriendJoinNext4;
    private Gallery mFriendJoinUser1, mFriendJoinUser2, mFriendJoinUser3, mFriendJoinUser4;
    private ImageView close_user1, close_user2, close_user3, close_user4;
    private LinearLayout mExitLayout1, mExitLayout2, mExitLayout3, mExitLayout4;
    private TextView mExitText1, mExitText2, mExitText3, mExitText4;
    private ImageView mExitOk1, mExitOk2, mExitOk3, mExitOk4;
    private ImageView mExitNo1, mExitNo2, mExitNo3, mExitNo4;

    private SquaredSurfaceView mCamPreview;
    private Camera mCamera;
    private SurfaceHolder mSurfaceHolder;

    private MyEditText mEdit;
    private Button mSend;

    private Handler handler;
    private Boolean isUsingFrontCamera = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_stream_group_activity);

        mApplication = (Story17Application) getApplication();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        mArea1 = (RelativeLayout) findViewById(R.id.area1);
        mArea2 = (RelativeLayout) findViewById(R.id.area2);
        mArea3 = (RelativeLayout) findViewById(R.id.area3);
        mArea4 = (RelativeLayout) findViewById(R.id.area4);

        mAllPeople = (TextView) findViewById(R.id.all_people);
        mNowPeople = (TextView) findViewById(R.id.now_people);
        mCamChange = (ImageView) findViewById(R.id.change);
        mClose = (ImageView) findViewById(R.id.close);
        mPeoples = (Gallery) findViewById(R.id.peoples);

        mName1 = (TextView) findViewById(R.id.name1);
        mName2 = (TextView) findViewById(R.id.name2);
        mName3 = (TextView) findViewById(R.id.name3);
        mName4 = (TextView) findViewById(R.id.name4);

        mOpenLayout1 = (LinearLayout) findViewById(R.id.open_layout1);
        mOpenLayout2 = (LinearLayout) findViewById(R.id.open_layout2);
        mOpenLayout3 = (LinearLayout) findViewById(R.id.open_layout3);
        mOpenLayout4 = (LinearLayout) findViewById(R.id.open_layout4);

        mLockImg1 = (ImageView) findViewById(R.id.lock_img1);
        mLockImg2 = (ImageView) findViewById(R.id.lock_img2);
        mLockImg3 = (ImageView) findViewById(R.id.lock_img3);
        mLockImg4 = (ImageView) findViewById(R.id.lock_img4);

        mLockText1 = (TextView) findViewById(R.id.lock_text1);
        mLockText2 = (TextView) findViewById(R.id.lock_text2);
        mLockText3 = (TextView) findViewById(R.id.lock_text3);
        mLockText4 = (TextView) findViewById(R.id.lock_text4);

        mJoinLayout1 = (LinearLayout) findViewById(R.id.join_layout1);
        mJoinLayout2 = (LinearLayout) findViewById(R.id.join_layout2);
        mJoinLayout3 = (LinearLayout) findViewById(R.id.join_layout3);
        mJoinLayout4 = (LinearLayout) findViewById(R.id.join_layout4);

        mJoinText1 = (TextView) findViewById(R.id.join_text1);
        mJoinText2 = (TextView) findViewById(R.id.join_text2);
        mJoinText3 = (TextView) findViewById(R.id.join_text3);
        mJoinText4 = (TextView) findViewById(R.id.join_text4);

        mJoinProgress1 = (ProgressBar) findViewById(R.id.join_progress1);
        mJoinProgress2 = (ProgressBar) findViewById(R.id.join_progress2);
        mJoinProgress3 = (ProgressBar) findViewById(R.id.join_progress3);
        mJoinProgress4 = (ProgressBar) findViewById(R.id.join_progress4);

        mJoinBtn1 = (Button) findViewById(R.id.join_btn1);
        mJoinBtn2 = (Button) findViewById(R.id.join_btn2);
        mJoinBtn3 = (Button) findViewById(R.id.join_btn3);
        mJoinBtn4 = (Button) findViewById(R.id.join_btn4);

        mFriendJoinLayout1 = (LinearLayout) findViewById(R.id.friend_join_layout1);
        mFriendJoinLayout2 = (LinearLayout) findViewById(R.id.friend_join_layout2);
        mFriendJoinLayout3 = (LinearLayout) findViewById(R.id.friend_join_layout3);
        mFriendJoinLayout4 = (LinearLayout) findViewById(R.id.friend_join_layout4);

        mFriendJoinBack1 = (ImageView) findViewById(R.id.friend_join_back1);
        mFriendJoinBack2 = (ImageView) findViewById(R.id.friend_join_back2);
        mFriendJoinBack3 = (ImageView) findViewById(R.id.friend_join_back3);
        mFriendJoinBack4 = (ImageView) findViewById(R.id.friend_join_back4);

        mFriendJoinNext1 = (ImageView) findViewById(R.id.friend_join_next1);
        mFriendJoinNext2 = (ImageView) findViewById(R.id.friend_join_next2);
        mFriendJoinNext3 = (ImageView) findViewById(R.id.friend_join_next3);
        mFriendJoinNext4 = (ImageView) findViewById(R.id.friend_join_next4);

        mFriendJoinUser1 = (Gallery) findViewById(R.id.friend_join_user1);
        mFriendJoinUser2 = (Gallery) findViewById(R.id.friend_join_user2);
        mFriendJoinUser3 = (Gallery) findViewById(R.id.friend_join_user3);
        mFriendJoinUser4 = (Gallery) findViewById(R.id.friend_join_user4);

        close_user1 = (ImageView) findViewById(R.id.close_user1);
        close_user2 = (ImageView) findViewById(R.id.close_user2);
        close_user3 = (ImageView) findViewById(R.id.close_user3);
        close_user4 = (ImageView) findViewById(R.id.close_user4);

        mExitLayout1 = (LinearLayout) findViewById(R.id.exit_layout1);
        mExitLayout2 = (LinearLayout) findViewById(R.id.exit_layout2);
        mExitLayout3 = (LinearLayout) findViewById(R.id.exit_layout3);
        mExitLayout4 = (LinearLayout) findViewById(R.id.exit_layout4);

        mExitText1 = (TextView) findViewById(R.id.exit_text1);
        mExitText2 = (TextView) findViewById(R.id.exit_text2);
        mExitText3 = (TextView) findViewById(R.id.exit_text3);
        mExitText4 = (TextView) findViewById(R.id.exit_text4);

        mExitOk1 = (ImageView) findViewById(R.id.exit_ok1);
        mExitOk2 = (ImageView) findViewById(R.id.exit_ok2);
        mExitOk3 = (ImageView) findViewById(R.id.exit_ok3);
        mExitOk4 = (ImageView) findViewById(R.id.exit_ok4);

        mExitNo1 = (ImageView) findViewById(R.id.exit_no1);
        mExitNo2 = (ImageView) findViewById(R.id.exit_no2);
        mExitNo3 = (ImageView) findViewById(R.id.exit_no3);
        mExitNo4 = (ImageView) findViewById(R.id.exit_no4);

        mCamPreview = (SquaredSurfaceView) findViewById(R.id.cam_preview);

        mSurfaceLayout1 = (RelativeLayout) findViewById(R.id.surface_layout1);
        mSurfaceLayout2 = (RelativeLayout) findViewById(R.id.surface_layout2);
        mSurfaceLayout3 = (RelativeLayout) findViewById(R.id.surface_layout3);
        mSurfaceLayout4 = (RelativeLayout) findViewById(R.id.surface_layout4);

        mEdit = (MyEditText) findViewById(R.id.edit);
        mEdit.mEditKeyBackListener = this;
        mEdit.clearFocus();
        mSend = (Button) findViewById(R.id.send);

        mArea1.getLayoutParams().height = mDisplayMetrics.widthPixels/2;
        mArea2.getLayoutParams().height = mDisplayMetrics.widthPixels/2;
        mArea3.getLayoutParams().height = mDisplayMetrics.widthPixels/2;
        mArea4.getLayoutParams().height = mDisplayMetrics.widthPixels/2;

        mSurfaceHolder = mCamPreview.getHolder();
        mSurfaceHolder.addCallback(mCtx);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        handler = new Handler(Looper.getMainLooper());

        new ChangePreviewTask().execute(null, null, null);

        mCamChange.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isUsingFrontCamera == false)
                {
                    isUsingFrontCamera = true;
                    startCamera();
                }
                else
                {
                    isUsingFrontCamera = false;
                    startCamera();
                }
            }
        });

        mSend.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d("123","phone : " + mDisplayMetrics.widthPixels/2);

                Log.d("123","w : " + mCamera.getParameters().getPreviewSize().width);
                Log.d("123","h : " + mCamera.getParameters().getPreviewSize().height);

                for(int i = 0 ; i < mCamera.getParameters().getSupportedPreviewSizes().size() ; i++)
                {
                    Log.d("123","ww : " + mCamera.getParameters().getSupportedPreviewSizes().get(i).width);
                    Log.d("123","hh : " + mCamera.getParameters().getSupportedPreviewSizes().get(i).height);
                    Log.d("123","----");
                }
            }
        });
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        stopCamera();
    }

    @Override
    public void OnEditKeyBack()
    {

    }

    private class ChangePreviewTask extends AsyncTask<Integer, Integer, Void>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Integer... pos)
        {
            try
            {
                if(isUsingFrontCamera==true)
                {
                    mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                }
                else
                {
                    mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                }

                if (mCamera != null)
                {
                    Camera.Parameters parameters = mCamera.getParameters();
                    parameters.setPreviewFormat(ImageFormat.NV21);
                    parameters.setPreviewSize(320, 240);

                    if(isUsingFrontCamera==false)
                    {
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    }

                    mCamera.setDisplayOrientation(90);
                    mCamera.setParameters(parameters);

                    mCamera.setPreviewDisplay(mSurfaceHolder);
                    mCamera.startPreview();
                    mCamera.setPreviewCallback(mCtx);
                }
            }
            catch (Exception e)
            {

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void pos)
        {
            super.onPostExecute(pos);
            hideKeyboard();
        }
    }

    private void startCamera()
    {
        stopCamera();
        handler.post(new Runnable()
        {
            public void run()
            {
                try
                {
                    if(isUsingFrontCamera==true)
                    {
                        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    }
                    else
                    {
                        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                    }
                }
                catch (Exception e)
                {
                }

                try
                {
                    if (mCamera != null)
                    {
                        Camera.Parameters parameters = mCamera.getParameters();
                        parameters.setPreviewFormat(ImageFormat.NV21);
                        parameters.setPreviewSize(320, 240);

                        if(isUsingFrontCamera==false)
                        {
                            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                        }

                        mCamera.setDisplayOrientation(90);
                        mCamera.setParameters(parameters);
                        startPreview();
                    }
                }
                catch (Exception e)
                {

                }
            }
        });
    }

    private void startPreview()
    {
        if(mCamera != null)
        {
            stopPreview();

            try
            {
                mCamera.setPreviewDisplay(mSurfaceHolder);
                mCamera.startPreview();
                mCamera.setPreviewCallback(mCtx);
            }
            catch(Exception e)
            {
            }
        }
    }

    private void stopPreview()
    {
        mCamera.setPreviewCallback(null);
        mCamera.stopPreview();
    }

    private void stopCamera()
    {
        if (mCamera != null)
        {
            stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera)
    {

    }
}
