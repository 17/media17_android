package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import java.io.IOException;

public class CropImageActivity extends BaseActivity
{

	private TitleBar titleBarView;
	private CropImageView cropImageView;
	private Button doneButton;

	private String filePath;

	public void onResume()
	{
		super.onResume();
		MobclickAgent.onPageStart(CropImageActivity.this.getClass().getSimpleName());
	}

	public void onPause()
	{
		super.onPause();
		MobclickAgent.onPageEnd(CropImageActivity.this.getClass().getSimpleName());
	}

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try
		{
			if(Build.VERSION.SDK_INT >= 21)
			{
				Window window = CropImageActivity.this.getWindow();
				window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
				window.setStatusBarColor(Color.BLACK);
			}
		}
		catch (Exception e)
		{
		}

		filePath = Singleton.getExternalTempImagePath();

		setContentView(R.layout.crop_image);

		titleBarView = (TitleBar) findViewById(R.id.titleBarView);
		doneButton = (Button) findViewById(R.id.doneButton);
		cropImageView = (CropImageView) findViewById(R.id.cropImageView);

		titleBarView.setTitleText(getString(R.string.cut));
		titleBarView.setTitleBarBackground(R.drawable.title_bar_dark);

		Bitmap b = loadBitmap(filePath,false);

		try {
			cropImageView.loadImage(b);
		} catch (Exception e) {
			finish();
			try{
//                    showToast(getString(R.string.unknown_error_occur));
				Toast.makeText(CropImageActivity.this, getString(R.string.unknown_error_occur), Toast.LENGTH_SHORT).show();
			}
			catch (Exception x){
			}
		}

		doneButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				try {
					Bitmap croppedBitmap = cropImageView.getCroppedImage();
					BitmapHelper.saveBitmap(croppedBitmap, Singleton.getExternalTempImagePath(), Bitmap.CompressFormat.JPEG);

					Intent resultIntent = new Intent();
					setResult(RESULT_OK, resultIntent);
					finish();
				} catch (Exception e) {
					try{
//                    showToast(getString(R.string.unknown_error_occur));
						Toast.makeText(CropImageActivity.this, getString(R.string.unknown_error_occur), Toast.LENGTH_SHORT).show();
					}
					catch (Exception x){
					}
					finish();
				}
			}
		});
	}

	public Bitmap loadBitmap(String imgpath)
	{
		int size = Constants.CROP_LOAD_IMAGE_PIXELS;
		if(getConfig(Constants.PHONE_WIDTH_HEIGHT_SIZE,Constants.DEFAULT_PIXELS) < Constants.CHECK_PIXELS) size = Constants.LOW_CROP_IMAGE_PIXELS;

		return BitmapHelper.getBitmap(imgpath, size);
	}

	public int getConfig(String key, int def)
	{
		SharedPreferences settings = getSharedPreferences("settings",0);
		return settings.getInt(key, def);
	}

	public Bitmap loadBitmap(String imgpath, boolean adjustOritation)
	{
		if (!adjustOritation)
		{
			return loadBitmap(imgpath);
		}
		else
		{
			Bitmap bm = loadBitmap(imgpath);
			int digree = 0;
			ExifInterface exif = null;
			try
			{
				exif = new ExifInterface(imgpath);
			}
			catch (IOException e)
			{
				e.printStackTrace();
				exif = null;
			}

			if (exif != null)
			{
				int ori = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);

				switch (ori)
				{
					case ExifInterface.ORIENTATION_ROTATE_90:
						digree = 90;
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						digree = 180;
						break;
					case ExifInterface.ORIENTATION_ROTATE_270:
						digree = 270;
						break;
					default:
						digree = 0;
						break;
				}

				if (digree != 0)
				{
					Matrix m = new Matrix();
					m.postRotate(digree);
					bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),bm.getHeight(), m, true);
				}
			}
			return bm;
		}
	}
}