package com.machipopo.media17;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.machipopo.media17.fragment.BaseFragment;
import com.machipopo.media17.model.RevenueModel;
import com.umeng.analytics.MobclickAgent;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by POPO on 6/23/15.
 */
public class RevenueDate30DFragment extends BaseFragment
{
    private Story17Application mApplication;

    private LineChartView mChart,mChartView;
    private int mCount = 30;
    private ArrayList<String> mDays = new ArrayList<String>();

    private float mTop = 0.0f, mTopView = 0.0f;

    private TextView mDate, mMoney, mPerson, mMoneyView, mPersonView;
    private ImageView mUpDowm,mUpDowmView;

    private ArrayList<RevenueModel> mRevenue = new ArrayList<RevenueModel>();
    private ArrayList<Float> mTotals = new ArrayList<Float>();
    private ArrayList<Integer> mViews = new ArrayList<Integer>();

    public RevenueDate30DFragment()
    {

    }

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart("RevenueDate30DFragment");
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd("RevenueDate30DFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.revenue_30d_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        mApplication = (Story17Application) getActivity().getApplication();

        mDate = (TextView) getView().findViewById(R.id.date);

        mMoney = (TextView) getView().findViewById(R.id.money);
        mPerson = (TextView) getView().findViewById(R.id.person);
        mUpDowm = (ImageView) getView().findViewById(R.id.updown);
        mChart = (LineChartView) getView().findViewById(R.id.chart);

        mMoneyView = (TextView) getView().findViewById(R.id.money_view);
        mPersonView = (TextView) getView().findViewById(R.id.person_view);
        mUpDowmView = (ImageView) getView().findViewById(R.id.updown_view);
        mChartView = (LineChartView) getView().findViewById(R.id.chart_view);

        mDays.clear();
        mRevenue.clear();
        mTotals.clear();
        mViews.clear();

        SimpleDateFormat mDateFormat = new SimpleDateFormat("MM/dd");
        SimpleDateFormat mDateFormatTo = new SimpleDateFormat("dd/MM/yyyy");

        String mFormatTo = mDateFormatTo.format(new Date((Singleton.getCurrentTimestamp() - (86400 * mCount))*1000L)) + " - " + mDateFormatTo.format(new Date((Singleton.getCurrentTimestamp() - 86400)*1000L));
        mDate.setText(mFormatTo);

//        int now = Singleton.getCurrentTimestamp() - 86400;
//        String mNow = mDateFormat.format(new Date(now*1000L));
//
//        int ago = Singleton.getCurrentTimestamp() - (86400 * 7);
//        String mAgo = mDateFormat.format(new Date(ago*1000L));

        ArrayList<String> mStamps = new ArrayList<String>();

        float mSum = 0.0f;
        int p = 0;
        for(int i = mCount ; i > 0 ; i--)
        {
            p++;
            int date = Singleton.getCurrentTimestamp() - (86400*i);

            if(p==1) mDays.add(mDateFormat.format(new Date(date * 1000L)));
            else if(p==7) mDays.add(mDateFormat.format(new Date(date * 1000L)));
            else if(p==13) mDays.add(mDateFormat.format(new Date(date * 1000L)));
            else if(p==19) mDays.add(mDateFormat.format(new Date(date * 1000L)));
            else if(p==25) mDays.add(mDateFormat.format(new Date(date * 1000L)));
            else if(p==30) mDays.add(mDateFormat.format(new Date(date * 1000L)));
            else mDays.add("");

            mStamps.add(mDateFormatTo.format(new Date(date * 1000L)));
            mTotals.add(0.0f);
            mViews.add(0);
        }

        mRevenue.addAll(mApplication.getRevenueModels());

        DecimalFormat dfs = new DecimalFormat("#.####");

        for(int i = 0 ; i < mRevenue.size() ; i++)
        {
            for(int k = 0 ; k < mStamps.size() ; k++)
            {
                if(mDateFormatTo.format(new Date(mRevenue.get(i).getTimestamp()* 1000L)).compareTo(mStamps.get(k))==0)
                {
                    float all = 0f;
                    all = mRevenue.get(i).getRevenue()*Singleton.getCurrencyRate();

                    mTotals.set(k,Float.valueOf(dfs.format((double) all)));
                    mSum = mSum + Float.valueOf(dfs.format((double) all));
                }
            }
        }

        String c = Singleton.getCurrencyType();

        mMoney.setText("$ " + dfs.format((double) mSum) + " " + c);

        float is = mSum/30.0f;
        Boolean up = true;
        float sun = 0.0f;
        DecimalFormat df_one = new DecimalFormat("#.#");

        if(is > mTotals.get(0))
        {
            mPerson.setTextColor(Color.parseColor("#F07D78"));
            mPersonView.setTextColor(Color.parseColor("#F07D78"));

            if(mTotals.get(0)==0) sun = (is / 0.0001f) - 1;
            else sun = (is / mTotals.get(0)) - 1;

            up = true;
        }
        else
        {
            mPerson.setTextColor(Color.parseColor("#B4D9D5"));
            mPersonView.setTextColor(Color.parseColor("#B4D9D5"));

            if(mTotals.get(0)==0) sun = 1 - (is / 0.0001f);
            else sun = 1 - (is / mTotals.get(0));

            up = false;
        }

        if(up)
        {
            mPerson.setText("▲" + df_one.format((double)sun) + "%");
            mPersonView.setText("▲" + df_one.format((double)sun) + "%");
        }
        else
        {
            mPerson.setText("▼" + df_one.format((double)sun) + "%");
            mPersonView.setText("▼" + df_one.format((double)sun) + "%");
        }

        for(int j = 0 ; j < mTotals.size() ; j++)
        {
            if(mTop < mTotals.get(j))
            {
                mTop = mTotals.get(j);
            }
        }


        intoData();

        mChart.setViewportCalculationEnabled(false);
        mChart.setInteractive(false);

        resetViewport();

//        android.os.Handler mHandler = new android.os.Handler();
//        mHandler.postDelayed(new Runnable()
//        {
//            @Override
//            public void run()
//            {
//                chart.startDataAnimation();
//            }
//        },1000);

        int mSumView = 0;

        for(int i = 0 ; i < mRevenue.size() ; i++)
        {
            for(int k = 0 ; k < mStamps.size() ; k++)
            {
                if(mDateFormatTo.format(new Date(mRevenue.get(i).getTimestamp()* 1000L)).compareTo(mStamps.get(k))==0)
                {
                    mViews.set(k,mRevenue.get(i).getTotalViews());
                    mSumView = mSumView + mRevenue.get(i).getTotalViews();
                }
            }
        }

        mMoneyView.setText(String.valueOf(mSumView) /*+ "次"*/);

        for(int j = 0 ; j < mViews.size() ; j++)
        {
            if(mTopView < mViews.get(j))
            {
                mTopView = mViews.get(j);
            }
        }

        intoDataView();

        mChartView.setViewportCalculationEnabled(false);
        mChartView.setInteractive(false);

        resetViewportView();
    }

    private void resetViewport()
    {
        final Viewport v = new Viewport(mChart.getMaximumViewport());
        v.bottom = 0;
        v.top = mTop + (mTop/10);
        v.left = 0;
        v.right = mCount - 1;

        mChart.setMaximumViewport(v);
        mChart.setCurrentViewport(v);
    }

    private void intoDataView()
    {
        final Viewport v = new Viewport(mChartView.getMaximumViewport());
        v.bottom = 0;
        v.top = mTopView + (mTopView/10);
        v.left = 0;
        v.right = mCount - 1;

        mChartView.setMaximumViewport(v);
        mChartView.setCurrentViewport(v);
    }

    private void intoData()
    {
        List<Line> lines = new ArrayList<Line>();

        List<PointValue> values = new ArrayList<PointValue>();
        for (int j = 0; j < mCount; ++j)
        {
            values.add(new PointValue(j, mTotals.get(j)));
        }

        Line line = new Line(values);
        line.setColor(ChartUtils.COLORS[0]);
        line.setShape(ValueShape.CIRCLE);
        line.setCubic(false);
        line.setFilled(false);
        line.setHasLabels(false);
        line.setHasLabelsOnlyForSelected(false);
        line.setHasLines(true);
        line.setHasPoints(false);
        lines.add(line);

        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        for (int i = 0; i < mCount; ++i)
        {
            axisValues.add(new AxisValue(i).setLabel(mDays.get(i)));
        }

        LineChartData data = new LineChartData(lines);

        Axis axisX = new Axis(axisValues).setTypeface(Typeface.DEFAULT).setHasLines(false).setName("");
        Axis axisY = new Axis().setTypeface(Typeface.DEFAULT).setHasLines(true).setName(" ");

        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);

        data.setBaseValue(Float.NEGATIVE_INFINITY);
        mChart.setLineChartData(data);
    }

    private void resetViewportView()
    {
        List<Line> lines = new ArrayList<Line>();

        List<PointValue> values = new ArrayList<PointValue>();
        for (int j = 0; j < mCount; ++j)
        {
            values.add(new PointValue(j, mViews.get(j)));
        }

        Line line = new Line(values);
        line.setColor(ChartUtils.COLORS[0]);
        line.setShape(ValueShape.CIRCLE);
        line.setCubic(false);
        line.setFilled(false);
        line.setHasLabels(false);
        line.setHasLabelsOnlyForSelected(false);
        line.setHasLines(true);
        line.setHasPoints(true);
        lines.add(line);

        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        for (int i = 0; i < mCount; ++i)
        {
            axisValues.add(new AxisValue(i).setLabel(mDays.get(i)));
        }

        LineChartData data = new LineChartData(lines);

        Axis axisX = new Axis(axisValues).setTypeface(Typeface.DEFAULT).setHasLines(false).setName("");
        Axis axisY = new Axis().setTypeface(Typeface.DEFAULT).setHasLines(true).setName(" ");

        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);

        data.setBaseValue(Float.NEGATIVE_INFINITY);
        mChartView.setLineChartData(data);
    }
}

