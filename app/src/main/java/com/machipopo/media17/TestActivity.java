package com.machipopo.media17;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by POPO on 5/20/15.
 */
public class TestActivity extends  BaseActivity
{
    private TestActivity mCtx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(com.machipopo.media17.R.layout.start_activity);

//        Button mIn = (Button) findViewById(R.id.in);
//        Button mOut = (Button) findViewById(R.id.out);
//
//        mIn.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
////                UUID mUUID = UUID.randomUUID();
////                String mNewUUID = mUUID.toString().replaceAll("-", "");
////
////                String src = "Hello_Android";
////                String encrypted = AESUtils.encrypt(mNewUUID,src);
////                String decrypted = AESUtils.decrypt(mNewUUID,encrypted);
////                Log.d("123","encrypted : " + encrypted);
////                Log.d("123","decrypted : " + decrypted);
//
//
//            }
//        });
//
//        mOut.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//                NetworkInfo info = cm.getActiveNetworkInfo();
//
//                if (info != null && info.isConnected())
//                {
//                    new TestApiTask().execute(null, null, null);
//                }
//                else
//                {
//                    Toast.makeText(mCtx, "尚未開啟網路", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        Singleton.getInstance(getApplicationContext());
    }

    public void showNetworkUnstableToast()
    {
        showToast("網路不穩定", R.drawable.sad);
    }

    public void showToast(String text, int imageResourceID)
    {
        Handler handler = new Handler();

        LayoutInflater inflater = getLayoutInflater();
        RelativeLayout container = (RelativeLayout) inflater.inflate(R.layout.custom_toast, null);
        TextView toastTextView = (TextView) container.findViewById(R.id.toastTextView);
        ImageView toastImageView = (ImageView) container.findViewById(R.id.toastImageView);

        toastTextView.setText(text);
        toastImageView.setImageResource(imageResourceID);

        final Toast toast = new Toast(this);
        toast.setGravity(Gravity.CENTER, 0, dpToPixel(-60));
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(container);
        toast.show();

        handler.postDelayed(new Runnable()
        {
            public void run()
            {
                toast.cancel();
            }
        }, 1250);
    }

    public int dpToPixel(int dp)
    {
        return Singleton.dpToPixel(dp);
    }

    public void showAlertDialog(String title, String message)
    {
        showOKDialog(title, message, null);
    }

    public interface DialogCallback
    {
        public void onResult(boolean didClickOk);
    }

    public void showOKDialog(String title, String message, final DialogCallback callback)
    {
        try
        {
            final Dialog dialog = new Dialog(this, R.style.MyDialog);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.my_dialog);

            RelativeLayout rootView = (RelativeLayout) dialog.findViewById(R.id.rootView);
            Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
            View marginView = (View) dialog.findViewById(R.id.marginView);
            Button okButton = (Button) dialog.findViewById(R.id.okButton);
            TextView titleTextView = (TextView) dialog.findViewById(R.id.titleTextView);
            TextView messageTextView = (TextView) dialog.findViewById(R.id.messageTextView);

            rootView.getLayoutParams().width = Singleton.SCREEN_WIDTH * 8 / 10;

            cancelButton.setVisibility(View.GONE);
            marginView.setVisibility(View.GONE);

            okButton.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    dialog.dismiss();

                    if(callback!=null)
                    {
                        callback.onResult(true);
                    }
                }
            });

            titleTextView.setText(title);
            messageTextView.setText(message);

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener()
            {
                public void onCancel(DialogInterface dialog)
                {
                    dialog.dismiss();
                    callback.onResult(false);
                }
            });

            dialog.show();
        }
        catch(Exception e)
        {

        }
    }

    ProgressDialog progressDialog = null;
    public void showProgressDialog()
    {
        try
        {
            if(progressDialog==null)
            {
                progressDialog = ProgressDialog.show(this, "", getString(R.string.processing));
            }

            progressDialog.setCancelable(true);
        }
        catch (Exception e)
        {
        }
    }

    public void hideProgressDialog()
    {
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(com.machipopo.media17.R.menu.start_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == com.machipopo.media17.R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class TestApiTask extends AsyncTask<String, Integer, Boolean>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
//            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            try
            {
                ApiManager.loginAction(mCtx,"23232", "123456", new ApiManager.LoginActionCallback()
                {
                    @Override
                    public void onResult(boolean success, String message, UserModel user)
                    {
                        Log.d("123", "success : " + success);

                        if(success)
                        {
                            if (message.equals("ok"))
                            {

                            }
                            else if (message.equals("freezed")) showAlertDialog(getString(R.string.prompt), getString(R.string.your_account_freezed));
                            else showAlertDialog(getString(R.string.prompt), getString(R.string.unknown_error_occur));
                        }
                        else showNetworkUnstableToast();
                    }
                });
            }
            catch(Exception e)
            {
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);
//            hideProgressDialog();
        }
    }

    public Bitmap getUrlBitmap(String url)
    {
        try
        {
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();

            InputStream is = conn.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            is.close();

            return bitmap;
        }
        catch(IOException e)
        {
            return null;
        }
    }
}
