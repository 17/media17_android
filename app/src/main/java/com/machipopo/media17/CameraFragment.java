package com.machipopo.media17;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.machipopo.media17.fragment.BaseFragment;

/**
 * Created by POPO on 5/27/15.
 */
public class CameraFragment extends BaseFragment
{
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        MenuActivity mMenuActivity = (MenuActivity) activity;
        int pos  = mMenuActivity.getTabPos();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.camera_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initTitleBar();

    }

    private void initTitleBar()
    {
        ((RelativeLayout) getView().findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) getView().findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.menu_camera));
        mTitle.setTextColor(Color.BLACK);
    }
}
