package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by POPO on 5/21/15.
 */
public class SignupEmailActivityV2 extends BaseActivity
{
    private SharedPreferences sharedPreferences;

    private HashMap<String,String> CODE = new HashMap<String,String>();
    private HashMap<String,String> NAME = new HashMap<String,String>();
    private SignupEmailActivityV2 mCtx = this;
    private EditText mEmail;
    private Button btnNext;

    private String[] mListCountry ;
    private String[] mListCode ;

    private String UserCode = "886";

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity_email_v2);

        initTitleBar();

        btnNext = (Button)findViewById(R.id.btn_next);
        mEmail = (EditText) findViewById(R.id.email);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNext(false);
            }
        });


        // Get country code
        try
        {
            String code = ReadFromfile("country_code.txt",mCtx);
            JSONArray mCodeArray = new JSONArray(code);
            mListCountry = new String[mCodeArray.length()];
            mListCode = new String[mCodeArray.length()];
            for(int i = 0 ; i < mCodeArray.length() ; i++)
            {
                CODE.put(mCodeArray.getJSONObject(i).getString("country"), mCodeArray.getJSONObject(i).getString("countryCode"));
                NAME.put(mCodeArray.getJSONObject(i).getString("countryCode"), mCodeArray.getJSONObject(i).getString("country"));
                mListCode[i] = mCodeArray.getJSONObject(i).getString("countryCode");
                mListCountry[i] = getResources().getString(getResources().getIdentifier(mCodeArray.getJSONObject(i).getString("country"), "string", getPackageName()));
            }
        }
        catch (JSONException e)
        {
        }


        ApiManager.getCountry(mCtx, new ApiManager.GetCountryCallback() {
            @Override
            public void onResult(boolean success, String message) {
                UserCode = CODE.get(message);
            }
        });


        showKeyboard();
    }

    public String ReadFromfile(String fileName, Context context)
    {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try
        {
            fIn = context.getResources().getAssets().open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null)
            {
                returnString.append(line);
            }
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally
        {
            try
            {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            }
            catch (Exception e2)
            {
                e2.getMessage();
            }
        }

        return returnString.toString();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.signup_title_email));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.skip));
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNext(true);
            }
        });


    }

    private void goNext(boolean skip){
        hideKeyboard();

        if(skip){
            showProgressDialog();

            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
            String username = sharedPreferences.getString(Constants.NAME, "");
            String passowrd = sharedPreferences.getString(Constants.PASSWORD, "");
            final String fullname = sharedPreferences.getString(Constants.FULL_NAME, "");
            final String pic = sharedPreferences.getString(Constants.PICTURE, "");
            String facebookID = sharedPreferences.getString(Constants.FACEBOOK_ID, "");
            final String email = "";


            showProgressDialog();

            ApiManager.registerAction_V2(mCtx, username, passowrd, facebookID, new ApiManager.RegisterActionCallback() {
                //                        ApiManager.registerAction(mCtx, mAccount, mPassword, name, "0", sex, pic, new ApiManager.RegisterActionCallback() {
                @Override
                public void onResult(boolean success, String message, UserModel user) {
                    hideProgressDialog();
                    if (success) {

                        try
                        {
                            JSONObject params = new JSONObject();
                            params.put("name", fullname);
                            params.put("picture", pic);
                            params.put("email", email);

                            ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                                @Override
                                public void onResult(boolean success, String message) {
                                    if (success) {
                                    }
                                }
                            });

                            setConfig(Constants.PROFILE_NAME, fullname);
                            Singleton.preferenceEditor.putString(Constants.PICTURE, pic).commit();
                        }
                        catch (JSONException e)
                        {

                        }

                        Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                        mStory17Application.setUser(user);

                        Intent intent = new Intent();
                        intent.setClass(mCtx, PhoneSearchActivity.class);
                        startActivity(intent);

                        resetSharedPreference();
                        mCtx.finish();
                    } else {
                        try{
//                          showToast(getString(R.string.signup_fail));
                            Toast.makeText(mCtx, getString(R.string.signup_fail), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });
        }
        else{
            String strEmail = mEmail.getText().toString().trim();

            if(!strEmail.equals("")){
                //Valid
                if(android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()){
                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);

                    showProgressDialog();

                    String username = sharedPreferences.getString(Constants.NAME, "");
                    String passowrd = sharedPreferences.getString(Constants.PASSWORD, "");
                    final String fullname = sharedPreferences.getString(Constants.FULL_NAME, "");
                    final String pic = sharedPreferences.getString(Constants.PICTURE, "");
                    String facebookID = sharedPreferences.getString(Constants.FACEBOOK_ID, "");
                    final String email = strEmail;

                    showProgressDialog();

                    ApiManager.registerAction_V2(mCtx, username, passowrd, facebookID, new ApiManager.RegisterActionCallback() {
                        //                        ApiManager.registerAction(mCtx, mAccount, mPassword, name, "0", sex, pic, new ApiManager.RegisterActionCallback() {
                        @Override
                        public void onResult(boolean success, String message, UserModel user) {
                            hideProgressDialog();
                            if (success) {

                                try
                                {
                                    JSONObject params = new JSONObject();
                                    params.put("name", fullname);
                                    params.put("picture", pic);
                                    params.put("email", email);

                                    ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {
                                            }
                                        }
                                    });

                                    setConfig(Constants.PROFILE_NAME, fullname);
                                    Singleton.preferenceEditor.putString(Constants.PICTURE, pic).commit();
                                }
                                catch (JSONException e)
                                {

                                }

                                Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                mStory17Application.setUser(user);

                                Intent intent = new Intent();
                                intent.setClass(mCtx, PhoneSearchActivity.class);
                                startActivity(intent);

                                resetSharedPreference();
                                mCtx.finish();
                            } else {
                                try{
//                          showToast(getString(R.string.signup_fail));
                                    Toast.makeText(mCtx, getString(R.string.signup_fail), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                    });

                }
                else{
                    try{
//                          showToast(getString(R.string.wrong_email_format));
                        Toast.makeText(mCtx, getString(R.string.wrong_email_format), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }


            }
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
//            Intent intent = new Intent();
//            intent.setClass(mCtx, LoginMenuActivity.class);
//            startActivity(intent);
//            mCtx.finish();
//            resetSharedPreference();
            return true;
        }

        if(keyCode == KeyEvent.KEYCODE_ENTER){
            goNext(false);
        }

        return super.onKeyUp(keyCode, event);
    }

    private void resetSharedPreference(){
        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
        sharedPreferences.edit().putString(Constants.NAME, "").commit();
        sharedPreferences.edit().putString(Constants.FULL_NAME, "").commit();
        sharedPreferences.edit().putString(Constants.PASSWORD, "").commit();
        sharedPreferences.edit().putString(Constants.EMAIL, "").commit();
        sharedPreferences.edit().putString(Constants.PICTURE, "").commit();
        sharedPreferences.edit().putString(Constants.FACEBOOK_ID, "").commit();
        sharedPreferences.edit().putBoolean("fblogin", false).commit();


    }

    private void signup(){

    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = mCtx.getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }
}
