package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.DevUtils;
import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by POPO on 5/21/15.
 */
public class SignupActivityV2 extends BaseActivity
{
    public final static String tag = "signup_setting";
//    class SignupConst{
//
//        public final static String USERNAME = "username";
//        public final static String PASSWORD = "password";
//        public final static String FULL_NAME = "fullname";
//        public final static String PHOTO = "photo";
//        public final static String FACEBOOK_ID = "facebookid";
//    }
    private SharedPreferences sharedPreferences;

    private SignupActivityV2 mCtx = this;
    private EditText mAccount;
    private Button btnNext;
    private boolean fbLogin;
    private Boolean from = false;
    private Story17Application mApplication;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());

        hideKeyboard();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity_username_v2);
        mApplication =(Story17Application) getApplication();

        initTitleBar();

        //log last step that user start quiting registration
        if(SingupLastStepLog.getInstance().getmSignupLastStep().ordinal() < SingupLastStepLog.SignupLastStep.UserName.ordinal()) {
            SingupLastStepLog.getInstance().setmSignupLastStep(SingupLastStepLog.SignupLastStep.UserName);
        }
        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("from")) from = mBundle.getBoolean("from");
        }

        btnNext = (Button)findViewById(R.id.btn_next);
        mAccount = (EditText) findViewById(R.id.account);

        //event tracking
        try{
            LogEventUtil.StartRegister(mCtx,mApplication);
        }catch (Exception x)
        {

        }

        mAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() != 0) {
                    String text = charSequence.toString().substring(charSequence.toString().length() - 1, charSequence.toString().length());
                    if (text.compareTo("A") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "a");
                    else if (text.compareTo("B") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "b");
                    else if (text.compareTo("C") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "c");
                    else if (text.compareTo("D") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "d");
                    else if (text.compareTo("E") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "e");
                    else if (text.compareTo("F") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "f");
                    else if (text.compareTo("G") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "g");
                    else if (text.compareTo("H") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "h");
                    else if (text.compareTo("I") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "i");
                    else if (text.compareTo("J") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "j");
                    else if (text.compareTo("K") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "k");
                    else if (text.compareTo("L") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "l");
                    else if (text.compareTo("M") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "m");
                    else if (text.compareTo("N") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "n");
                    else if (text.compareTo("O") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "o");
                    else if (text.compareTo("P") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "p");
                    else if (text.compareTo("Q") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "q");
                    else if (text.compareTo("R") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "r");
                    else if (text.compareTo("S") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "s");
                    else if (text.compareTo("T") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "t");
                    else if (text.compareTo("U") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "u");
                    else if (text.compareTo("V") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "v");
                    else if (text.compareTo("W") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "w");
                    else if (text.compareTo("X") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "x");
                    else if (text.compareTo("Y") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "y");
                    else if (text.compareTo("Z") == 0)
                        mAccount.setText(mAccount.getText().toString().substring(0, mAccount.getText().toString().length() - 1) + "z");

                    mAccount.setSelection(mAccount.getText().toString().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        sharedPreferences = getSharedPreferences(tag, 0);
        fbLogin = sharedPreferences.getBoolean("fblogin", false);
        if(fbLogin){
            String name = sharedPreferences.getString(Constants.NAME, "");
            if(!name.equals("")) {

                Boolean state = true;
                for(int i = 0 ; i < name.length() ; i++)
                {
                    String filter = name.substring(i,i+1);
                    if(filter.compareTo("0")==0 || filter.compareTo("1")==0 || filter.compareTo("2")==0 || filter.compareTo("3")==0 || filter.compareTo("4")==0 || filter.compareTo("5")==0
                    || filter.compareTo("6")==0 || filter.compareTo("7")==0 || filter.compareTo("8")==0 || filter.compareTo("9")==0 || filter.compareTo("a")==0 || filter.compareTo("b")==0
                    || filter.compareTo("c")==0 || filter.compareTo("d")==0 || filter.compareTo("e")==0 || filter.compareTo("f")==0 || filter.compareTo("g")==0 || filter.compareTo("h")==0
                    || filter.compareTo("i")==0 || filter.compareTo("j")==0 || filter.compareTo("k")==0 || filter.compareTo("l")==0 || filter.compareTo("m")==0 || filter.compareTo("n")==0
                    || filter.compareTo("o")==0 || filter.compareTo("p")==0 || filter.compareTo("q")==0 || filter.compareTo("r")==0 || filter.compareTo("s")==0 || filter.compareTo("t")==0
                    || filter.compareTo("u")==0 || filter.compareTo("v")==0 || filter.compareTo("w")==0 || filter.compareTo("x")==0 || filter.compareTo("y")==0 || filter.compareTo("z")==0
                    || filter.compareTo("A")==0 || filter.compareTo("B")==0 || filter.compareTo("C")==0 || filter.compareTo("D")==0 || filter.compareTo("E")==0 || filter.compareTo("F")==0
                    || filter.compareTo("G")==0 || filter.compareTo("H")==0 || filter.compareTo("I")==0 || filter.compareTo("J")==0 || filter.compareTo("K")==0 || filter.compareTo("L")==0
                    || filter.compareTo("M")==0 || filter.compareTo("N")==0 || filter.compareTo("O")==0 || filter.compareTo("P")==0 || filter.compareTo("Q")==0 || filter.compareTo("R")==0
                    || filter.compareTo("S")==0 || filter.compareTo("T")==0 || filter.compareTo("U")==0 || filter.compareTo("V")==0 || filter.compareTo("W")==0 || filter.compareTo("X")==0
                    || filter.compareTo("Y")==0 || filter.compareTo("Z")==0 || filter.compareTo(".")==0 || filter.compareTo("_")==0)
                    {

                    }
                    else {
                        state = false;
                        break;
                    }
                }

                if(state){
                    mAccount.setText(name);
                }
            }
        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNext();
            }
        });

        showKeyboard();

        try{
            HashMap<String,String> mHashMap = new HashMap<String,String>();
            mHashMap.put("userId", Singleton.preferences.getString(Constants.USER_ID, ""));
            mHashMap.put("version", Singleton.getVersion());
            mHashMap.put("region", Singleton.preferences.getString(Constants.IP_COUNTRY, ""));
            mHashMap.put("u", Singleton.getPhoneIMEI());
            mHashMap.put("dn", DevUtils.getDevInfo());
            LogEventUtil.sendSDKLogEvent(mCtx, "v26_signup_activity", mHashMap);
        }
        catch (Exception e){
        }
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.account));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_back_black);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();

                //event tracking
                try{
                    LogEventUtil.QuitRegister(mCtx,mApplication, SingupLastStepLog.getInstance().getmSignupLastStep().toString());
                }catch (Exception x)
                {

                }

                if(from)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, LoginActivity_V3.class);
                    startActivity(intent);
                    mCtx.finish();
                }
                else {
                    mCtx.finish();
                }
            }
        });


    }

    private void goNext(){
        //account 2-20
        //passwoud 5-20
        if(mAccount.getText().toString().length()!=0)
        {
            if(mAccount.getText().toString().length()<2)
            {
                try{
//                          showToast(getString(R.string.account_size));
                    Toast.makeText(mCtx, getString(R.string.account_size), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
                return;
            }


            showProgressDialog();
            ApiManager.checkOpenIDAvailable(mCtx, mAccount.getText().toString(), new ApiManager.CheckOpenIDAvaliableCallback() {
                @Override
                public void onResult(boolean success, String message) {
                    hideProgressDialog();

                    if (success) {
                        sharedPreferences = getSharedPreferences(tag, 0);
                        sharedPreferences.edit().putString(Constants.NAME, mAccount.getText().toString()).commit();
                        sharedPreferences.edit().putBoolean("fblogin", false).commit();
//                        Log.d("17_gift","NAME :"+sharedPreferences.getString(Constants.NAME, ""));

                        try{
                            String Umeng_id="signup_type";
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put("Umeng_id", sharedPreferences.getString("signup_type", ""));
                            MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                        }
                        catch (Exception e){
                        }

                        Intent intent = new Intent();
//                        Log.e("DDD", "FBLOGIN: " + fbLogin);
//                        if(fbLogin){
//                            // If FB Login, auto register
//                            autoSignup();
////                            intent.setClass(mCtx, FacebookActivity.class);
//                        }
//                        else {
                            intent.setClass(mCtx, SignupPasswordActivityV2.class);
                            startActivity(intent);
                            mCtx.finish();
//                        }
//                                intent.putExtra("account", mAccount.getText().toString());
                    } else {
                        try{
//                          showToast(getString(R.string.account_same));
                            Toast.makeText(mCtx, getString(R.string.account_same), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });
        }
        else{
            try{
//                          showToast(getString(R.string.enter_wrong_username));
                Toast.makeText(mCtx, getString(R.string.enter_wrong_username), Toast.LENGTH_SHORT).show();
            }
            catch (Exception x){
            }
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            hideKeyboard();

            //event tracking
            try{
                LogEventUtil.QuitRegister(mCtx,mApplication,SingupLastStepLog.getInstance().getmSignupLastStep().toString());
            }catch (Exception x)
            {

            }

            if(from)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, LoginActivity_V3.class);
                startActivity(intent);
                mCtx.finish();
            }
            else {
                mCtx.finish();
            }

            return true;
        }

        if(keyCode == KeyEvent.KEYCODE_ENTER){
            goNext();
        }

        return super.onKeyUp(keyCode, event);
    }

    private void autoSignup(){
        sharedPreferences = getSharedPreferences(tag, 0);
        String username = mAccount.getText().toString();
        final String fullname = sharedPreferences.getString(Constants.FULL_NAME, "");
        String passowrd = sharedPreferences.getString(Constants.PASSWORD, "");
        final String email = sharedPreferences.getString(Constants.EMAIL, "");
        String facebookID = sharedPreferences.getString(Constants.FACEBOOK_ID, "");
        final String photo = sharedPreferences.getString(Constants.PICTURE, "");

        ApiManager.registerAction_V2(mCtx, username, passowrd, facebookID, new ApiManager.RegisterActionCallback() {
            //                        ApiManager.registerAction(mCtx, mAccount, mPassword, name, "0", sex, pic, new ApiManager.RegisterActionCallback() {
            @Override
            public void onResult(boolean success, String message, UserModel user) {
                hideProgressDialog();
                if (success) {

                    // Login success, update user info
                    try
                    {
                        JSONObject params = new JSONObject();
                        params.put("email", email);
                        params.put("picture", photo);
                        params.put("name", fullname);

                        user.setPicture(photo);
                        user.setEmail(email);
                        user.setName(fullname);
                        Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                        mStory17Application.setUser(user);


                        uploadPhoto();


                        ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {
                                }
                            }
                        });
                    }
                    catch (JSONException e)
                    {

                    }
                    hideKeyboard();

                    Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                    mStory17Application.setUser(user);

                    Intent intent = new Intent();
                    intent.setClass(mCtx, PhoneSearchActivity.class);
                    startActivity(intent);

                    resetSharedPreference();
                    mCtx.finish();
                } else {
                    try{
//                          showToast(getString(R.string.signup_fail));
                        Toast.makeText(mCtx, getString(R.string.signup_fail), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    private void resetSharedPreference(){
        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
        sharedPreferences.edit().putString(Constants.NAME, "").commit();
        sharedPreferences.edit().putString(Constants.FULL_NAME, "").commit();
        sharedPreferences.edit().putString(Constants.PASSWORD, "").commit();
        sharedPreferences.edit().putString(Constants.EMAIL, "").commit();
        sharedPreferences.edit().putString(Constants.COVER_PHOTO, "").commit();
        sharedPreferences.edit().putString(Constants.FACEBOOK_ID, "").commit();
        sharedPreferences.edit().putBoolean("fblogin", false).commit();
    }

    private void uploadPhoto(){

        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
        String pictureFileName = sharedPreferences.getString(Constants.PICTURE, "");
        String thumbnailPictureFileName = "THUMBNAIL_" + pictureFileName;

        try {
            Bitmap large  = BitmapFactory.decodeFile(Singleton.getExternalMediaFolderPath() + pictureFileName);
            if(large!=null)
            {
                Bitmap thumbnail = Bitmap.createScaledBitmap(large, 640, 640, false);
                BitmapHelper.saveBitmap(thumbnail, Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName, Bitmap.CompressFormat.JPEG);

                ArrayList<String> filesToUpload = new ArrayList<String>();
                filesToUpload.add(pictureFileName);
                filesToUpload.add(thumbnailPictureFileName);

                MultiFileUploader uploader = new MultiFileUploader(filesToUpload);
                uploader.startUpload(SignupActivityV2.this, new MultiFileUploader.MultiFileUploaderCallback()
                {
                    @Override
                    public void didComplete()
                    {
//                Log.e("DDD", "didComplete");
//                            hideProgressDialog();
//                            showCompleteToast();
//                            imagePickerDialogCallback.onResult(true, true, pictureFileName);
                    }

                    @Override
                    public void didFail()
                    {
//                Log.e("DDD", "Fail");
//                            hideProgressDialog();
//                            showNetworkUnstableToast();
//                            imagePickerDialogCallback.onResult(true, false, pictureFileName);
                    }
                });
            }
        }
        catch (Exception e){

        }
    }

}
