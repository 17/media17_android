package com.machipopo.media17;

import android.content.Context;

import java.util.ArrayList;
import java.util.UUID;

public class MultiFileUploader
{
	public interface MultiFileUploaderCallback
	{
		public void didComplete();
		public void didFail();
	}

	public interface MultiFileUploaderPhotoCallback
	{
		public void didComplete();
		public void didFail();
		public void onProgress(String id, int pos);
	}

	MultiFileUploaderCallback callback;
	ArrayList<String> files;
	boolean hasOutputFailMessage = false;

	int uploadCompleteCount = 0;

	public MultiFileUploader(ArrayList<String> files)
	{
		this.files = files;
	}

	public void startUpload(Context context, final MultiFileUploaderCallback callback)
	{
		for(String filename:files)
		{
			// upload file

			ApiManager.uploadFile(context, Singleton.getExternalMediaFolderPath()+filename , filename, new ApiManager.SuccessCallback()
			{
				@Override
				public void onResult(boolean success)
				{
					if(success)
					{
						uploadCompleteCount++;
						
						if(uploadCompleteCount==files.size())
						{
							callback.didComplete();
						}
					}
					else
					{
						if(hasOutputFailMessage==false)
						{
							hasOutputFailMessage = true;
							callback.didFail();
						}
					}
				}

				@Override
				public void onProgress(int pos)
				{

				}
			});
		}
	}

	public void startUpload(Context context, final MultiFileUploaderPhotoCallback callback)
	{
		for(String filename:files)
		{
			// upload file
			ApiManager.uploadFile(context, Singleton.getExternalMediaFolderPath()+filename , filename, new ApiManager.SuccessCallback()
			{
				String id = UUID.randomUUID().toString();

				@Override
				public void onResult(boolean success)
				{
					if(success)
					{
						uploadCompleteCount++;

						if(uploadCompleteCount==files.size())
						{
							callback.didComplete();
						}
					}
					else
					{
						if(hasOutputFailMessage==false)
						{
							hasOutputFailMessage = true;
							callback.didFail();
						}
					}
				}

				@Override
				public void onProgress(int pos)
				{
					callback.onProgress(id,pos);
				}
			});
		}
	}
}