package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by POPO on 6/22/15.
 */
public class RevenueAccountActivity extends BaseNewActivity
{
    private RevenueAccountActivity mCtx = this;
    private Story17Application mApplication;

    private LinearLayout mNameLayout, mIdCardLayout, mIdCardPicFront, mIdCardPicBack, mBankNameLayout, mBankBranchLayout, mBankNumberLayout, mAccountNameLayout, mBankPicLayout;
    private TextView mName, mId, mBankName, mBankBranch, mAccount, mAccountName;
    private Button mIdFront, mIdBack, mBankPic;

    private int mReload = 0;

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_account_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();

        initTitleBar();

        mNameLayout= (LinearLayout) findViewById(R.id.name_layout);
        mIdCardLayout= (LinearLayout) findViewById(R.id.idcard_layout);
        mIdCardPicFront= (LinearLayout) findViewById(R.id.idcard_pic_front);
        mIdCardPicBack= (LinearLayout) findViewById(R.id.idcard_pic_back);
        mBankNameLayout= (LinearLayout) findViewById(R.id.bank_name_layout);
        mBankBranchLayout= (LinearLayout) findViewById(R.id.bank_branch_layout);
        mBankNumberLayout= (LinearLayout) findViewById(R.id.bank_number_layout);
        mAccountNameLayout= (LinearLayout) findViewById(R.id.account_name_layout);
        mBankPicLayout= (LinearLayout) findViewById(R.id.bank_pic_layout);

        mName = (TextView) findViewById(R.id.name);
        mId = (TextView) findViewById(R.id.id);
        mBankName = (TextView) findViewById(R.id.bank_name);
        mBankBranch = (TextView) findViewById(R.id.bank_branch);
        mAccount = (TextView) findViewById(R.id.account_number);
        mAccountName = (TextView) findViewById(R.id.account_name);

        mIdFront = (Button) findViewById(R.id.id_front);
        mIdBack = (Button) findViewById(R.id.id_back);
        mBankPic = (Button) findViewById(R.id.account_pic);

        setTextColor(mName,getConfig(Constants.REVENUE_NAME));
        setTextColor(mId,getConfig(Constants.REVENUE_ID));
        setTextColor(mBankName,getConfig(Constants.REVENUE_BANK_NAME));
        setTextColor(mBankBranch,getConfig(Constants.REVENUE_BANK_BRANCH));
        setTextColor(mAccount,getConfig(Constants.REVENUE_BANK_NUMBER));
        setTextColor(mAccountName,getConfig(Constants.REVENUE_ACCOUNT_NAME));

        setTextColor(mIdFront,getConfig(Constants.REVENUE_ID_FRONT));
        setTextColor(mIdBack,getConfig(Constants.REVENUE_ID_BACK));
        setTextColor(mBankPic,getConfig(Constants.REVENUE_ACCOUNT_PICTURE));

        ApiManager.getSelfInfo(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), new ApiManager.GetSelfInfoCallback()
        {
            @Override
            public void onResult(boolean success, String message, UserModel userModel)
            {
                if (success && userModel != null)
                {
                    if(userModel.getBankAccountVerified()==1)
                    {
                        mIdFront.setText(getString(R.string.account_load_ok));
                        mIdBack.setText(getString(R.string.account_load_ok));
                        mBankPic.setText(getString(R.string.account_load_ok));
                    }
                }
            }
        });

        mNameLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mReload = 1;
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueUpdateActivity.class);
                intent.putExtra("type",Constants.REVENUE_NAME);
                intent.putExtra("title",getString(R.string.account_name));
                intent.putExtra("default_text",getConfig(Constants.REVENUE_NAME));
                startActivity(intent);
            }
        });

        mIdCardLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mReload = 2;
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueUpdateActivity.class);
                intent.putExtra("type",Constants.REVENUE_ID);
                intent.putExtra("title",getString(R.string.account_id));
                intent.putExtra("default_text",getConfig(Constants.REVENUE_ID));
                startActivity(intent);
            }
        });

        mIdCardPicFront.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback()
                {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, final String imageFileName)
                    {
                        if (uploadComplete)
                        {
                            try
                            {
                                JSONObject params = new JSONObject();
                                params.put("idCardFrontPicture", imageFileName);

                                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            setConfig(Constants.REVENUE_ID_FRONT,imageFileName);
                                            setTextColor(mIdFront,imageFileName);
                                        }
                                        else {
                                            try{
//                                             showToast(getString(R.string.error_failed));
                                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });

                            }
                            catch (JSONException e)
                            {
                            }
                        }
                        else
                        {
                            showNetworkUnstableToast();
                        }
                    }
                });
            }
        });

        mIdFront.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback()
                {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, final String imageFileName)
                    {
                        if (uploadComplete)
                        {
                            try
                            {
                                JSONObject params = new JSONObject();
                                params.put("idCardFrontPicture", imageFileName);

                                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            setConfig(Constants.REVENUE_ID_FRONT,imageFileName);
                                            setTextColor(mIdFront,imageFileName);
                                        }
                                        else {
                                            try{
//                                             showToast(getString(R.string.error_failed));
                                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });

                            }
                            catch (JSONException e)
                            {
                            }
                        }
                        else
                        {
                            showNetworkUnstableToast();
                        }
                    }
                });
            }
        });

        mIdCardPicBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback()
                {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, final String imageFileName)
                    {
                        if (uploadComplete)
                        {
                            try
                            {
                                JSONObject params = new JSONObject();
                                params.put("idCardBackPicture", imageFileName);

                                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            setConfig(Constants.REVENUE_ID_BACK,imageFileName);
                                            setTextColor(mIdBack,imageFileName);
                                        }
                                        else {
                                            try{
//                                             showToast(getString(R.string.error_failed));
                                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });

                            }
                            catch (JSONException e)
                            {
                            }
                        }
                        else
                        {
                            showNetworkUnstableToast();
                        }
                    }
                });
            }
        });

        mIdBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback()
                {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, final String imageFileName)
                    {
                        if (uploadComplete)
                        {
                            try
                            {
                                JSONObject params = new JSONObject();
                                params.put("idCardBackPicture", imageFileName);

                                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            setConfig(Constants.REVENUE_ID_BACK,imageFileName);
                                            setTextColor(mIdBack,imageFileName);
                                        }
                                        else {
                                            try{
//                                             showToast(getString(R.string.error_failed));
                                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });

                            }
                            catch (JSONException e)
                            {
                            }
                        }
                        else
                        {
                            showNetworkUnstableToast();
                        }
                    }
                });
            }
        });

        mBankNameLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mReload = 5;
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueUpdateActivity.class);
                intent.putExtra("type",Constants.REVENUE_BANK_NAME);
                intent.putExtra("title",getString(R.string.account_bank_name));
                startActivity(intent);
            }
        });


        mBankBranchLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(getConfig(Constants.REVENUE_BANK_NAME).length()!=0)
                {
                    mReload = 6;
                    Intent intent = new Intent();
                    intent.setClass(mCtx,RevenueUpdateActivity.class);
                    intent.putExtra("type",Constants.REVENUE_BANK_BRANCH);
                    intent.putExtra("title",getString(R.string.account_bank_branch));
                    intent.putExtra("bank",getConfig(Constants.REVENUE_BANK_NAME));
                    startActivity(intent);
                }
                else
                {
                    try{
//                                             showToast(getString(R.string.choose_bank));
                        Toast.makeText(mCtx, getString(R.string.choose_bank), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });

        mBankNumberLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mReload = 7;
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueUpdateActivity.class);
                intent.putExtra("type",Constants.REVENUE_BANK_NUMBER);
                intent.putExtra("title",getString(R.string.account_bank_id));
                intent.putExtra("default_text",getConfig(Constants.REVENUE_BANK_NUMBER));
                startActivity(intent);
            }
        });

        mAccountNameLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mReload = 8;
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueUpdateActivity.class);
                intent.putExtra("type",Constants.REVENUE_ACCOUNT_NAME);
                intent.putExtra("title",getString(R.string.account_bank_id_name));
                intent.putExtra("default_text", getConfig(Constants.REVENUE_ACCOUNT_NAME));
                startActivity(intent);
            }
        });

        mBankPicLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback()
                {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, final String imageFileName)
                    {
                        if (uploadComplete)
                        {
                            try
                            {
                                JSONObject params = new JSONObject();
                                params.put("passbookPicture", imageFileName);

                                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            setConfig(Constants.REVENUE_ACCOUNT_PICTURE,imageFileName);
                                            setTextColor(mBankPic,imageFileName);
                                        }
                                        else {
                                            try{
//                                             showToast(getString(R.string.error_failed));
                                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });

                            }
                            catch (JSONException e)
                            {
                            }
                        }
                        else
                        {
                            showNetworkUnstableToast();
                        }
                    }
                });
            }
        });

        mBankPic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback()
                {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, final String imageFileName)
                    {
                        if (uploadComplete)
                        {
                            try
                            {
                                JSONObject params = new JSONObject();
                                params.put("passbookPicture", imageFileName);

                                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            setConfig(Constants.REVENUE_ACCOUNT_PICTURE,imageFileName);
                                            setTextColor(mBankPic,imageFileName);
                                        }
                                        else {
                                            try{
//                                             showToast(getString(R.string.error_failed));
                                                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });

                            }
                            catch (JSONException e)
                            {
                            }
                        }
                        else
                        {
                            showNetworkUnstableToast();
                        }
                    }
                });
            }
        });

    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_account));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private void setTextColor(TextView tx, String text)
    {
        if(text.length()==0)
        {
            tx.setText(getString(R.string.account_set));
            tx.setTextColor(Color.parseColor("#F07D78"));
        }
        else
        {
            tx.setText(text);
            tx.setTextColor(getResources().getColor(R.color.second_main_text_color));
        }
    }

    private void setTextColor(Button tx, String text)
    {
        if(text.length()==0)
        {
            tx.setText(getString(R.string.account_load));
            tx.setTextColor(getResources().getColor(R.color.second_main_text_color));
        }
        else
        {
            tx.setText(getString(R.string.account_load_is));
            tx.setTextColor(getResources().getColor(R.color.second_main_text_color));
        }
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    public String getConfig(String key)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, "");
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mReload==1)
        {
            setTextColor(mName,getConfig(Constants.REVENUE_NAME));
            mReload = 0;
        }
        else if(mReload==2)
        {
            setTextColor(mId,getConfig(Constants.REVENUE_ID));
            mReload = 0;
        }
        else if(mReload==5)
        {
            setTextColor(mBankName,getConfig(Constants.REVENUE_BANK_NAME));
            mReload = 0;
        }
        else if(mReload==6)
        {
            setTextColor(mBankBranch,getConfig(Constants.REVENUE_BANK_BRANCH));
            mReload = 0;
        }
        else if(mReload==7)
        {
            setTextColor(mAccount,getConfig(Constants.REVENUE_BANK_NUMBER));
            mReload = 0;
        }
        else if(mReload==8)
        {
            setTextColor(mAccountName,getConfig(Constants.REVENUE_ACCOUNT_NAME));
            mReload = 0;
        }

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    static ImagePickerDialogCallback imagePickerDialogCallback = null;
    boolean needUploadPicture = false;
    boolean needCreateThumbnail = false;
    boolean needCrop = false;
    Handler handler = new Handler();
    ContentResolver contentResolver = null;
    ProgressDialog progressDialog = null;

    public void showImagePickerOptionDialog(boolean needUploadPicture, boolean needCreateThumbnail, boolean needCrop, ImagePickerDialogCallback callback)
    {
        imagePickerDialogCallback = callback;
        this.needUploadPicture = needUploadPicture;
        this.needCreateThumbnail = needCreateThumbnail;
        this.needCrop = needCrop;

        showOptionPickerDialog(new ArrayList<String>(Arrays.asList(getString(R.string.take_pic), getString(R.string.pick_album))), new OptionPickerCallback() {
            @Override
            public void onResult(boolean done, int selectedIndex) {
                if (!done) {
                    return;
                }

                if (selectedIndex == 0) {
                    startTakePictureIntent();
                } else if (selectedIndex == 1) {
                    startPickImageIntent();
                }
            }
        });
    }

    public void showOptionPickerDialog(final ArrayList<String> items, final OptionPickerCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);

        CharSequence[] itemTitles = items.toArray(new CharSequence[items.size()]);

        builder.setItems(itemTitles, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callback.onResult(true, which);
            }
        });

        AlertDialog dialog = builder.create();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();

                callback.onResult(false, 0);
            }
        });

        dialog.show();
    }

    public interface ImagePickerDialogCallback
    {
        public void onResult(boolean imageOk, boolean uploadComplete, String imageFileName);
    }

    public interface OptionPickerCallback
    {
        public void onResult(boolean done, int selectedIndex);
    }

    public void startTakePictureIntent()
    {
        new File(Singleton.getExternalTempImagePath()).delete();

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Singleton.getExternalTempImagePath())));
        startActivityForResult(intent, Constants.TAKE_PICTURE_REQUEST);
    }

    public void startPickImageIntent()
    {
        new File(Singleton.getExternalTempImagePath()).delete();

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Constants.PICK_IMAGE_REQUEST);
    }

    public void showNetworkUnstableToast()
    {
        showToast(getString(R.string.connet_erroe), R.drawable.sad);
    }

    public void showToast(String text, int imageResourceID)
    {
        LayoutInflater inflater = mCtx.getLayoutInflater();
        RelativeLayout container = (RelativeLayout) inflater.inflate(R.layout.custom_toast, null);
        TextView toastTextView = (TextView) container.findViewById(R.id.toastTextView);
        ImageView toastImageView = (ImageView) container.findViewById(R.id.toastImageView);

        toastTextView.setText(text);
        toastImageView.setImageResource(imageResourceID);

        final Toast toast = new Toast(mCtx);
        toast.setGravity(Gravity.CENTER, 0, dpToPixel(-60));
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(container);
        toast.show();

        handler.postDelayed(new Runnable()
        {
            public void run()
            {
                toast.cancel();
            }
        }, 2000);
    }

    public int dpToPixel(int dp)
    {
        return Singleton.dpToPixel(dp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        try
        {
            if(resultCode!=mCtx.RESULT_OK)
            {
                return;
            }

            boolean hasGotPicture = false;

            if(requestCode==Constants.TAKE_PICTURE_REQUEST)
            {
                hasGotPicture = true;
                needCrop = true;
                needCreateThumbnail = true;
            }

            if(requestCode==Constants.PICK_IMAGE_REQUEST)
            {
                if(data!=null && data.getData()!=null)
                {
                    String selectedImageFilePath = Singleton.getRealPathFromURI(data.getData(), mCtx);
                    String tmpImageFilePath = Singleton.getExternalTempImagePath();

                    if(selectedImageFilePath==null)
                    {
                        // copy image to sdcard
                        try {
                            Uri selectedImageURI = data.getData();
                            InputStream input = contentResolver.openInputStream(selectedImageURI);
                            BitmapHelper.saveBitmap(BitmapFactory.decodeStream(input, null, BitmapHelper.getBitmapOptions(2)), tmpImageFilePath, Bitmap.CompressFormat.JPEG);
                            hasGotPicture = true;
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    else
                    {
                        Singleton.copyFile(new File(selectedImageFilePath), new File(tmpImageFilePath));
                        hasGotPicture = true;
                    }
                }
            }

            if(requestCode==Constants.CROP_IMAGE_REQUEST)
            {
                hasGotPicture = true;
                needUploadPicture = true;
            }

            if(hasGotPicture==false)
            {
                try
                {
                    imagePickerDialogCallback.onResult(false, false, "");
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }

                return;
            }

            // crop
            if(needCrop)
            {
                needCrop = false;

                startActivityForResult(new Intent().setClass(mCtx, CropImageActivity.class), Constants.CROP_IMAGE_REQUEST);
                return;
            }

            int largeSize = Constants.PROFILE_PICTURE_PIXELS;
            int thumbnailSize = Constants.PROFILE_PICTURE_THUMBNAIL_PIXELS;
            if(getConfig(Constants.PHONE_WIDTH_HEIGHT_SIZE,Constants.DEFAULT_PIXELS) < Constants.CHECK_PIXELS)
            {
                largeSize = Constants.LOW_PROFILE_PICTURE_PIXELS;
                thumbnailSize = Constants.LOW_PROFILE_PICTURE_THUMBNAIL_PIXELS;
            }

            // generate thumbnail
            Bitmap large = BitmapHelper.getBitmap(Singleton.getExternalTempImagePath(), largeSize);
            Bitmap thumbnail = BitmapHelper.getBitmap(Singleton.getExternalTempImagePath(), thumbnailSize);

            if(large==null || thumbnail==null)
            {
                imagePickerDialogCallback.onResult(false, false, "");
                showAlertDialog(getString(R.string.prompt), getString(R.string.unknown_error_occur));
                return;
            }

            final String pictureFileName = Singleton.getUUIDFileName("jpg");
            final String thumbnailPictureFileName = "THUMBNAIL_" + pictureFileName;

//            Singleton.log("pictureFileName:" + pictureFileName);
//            Singleton.log("thumbnailPictureFileName:" + thumbnailPictureFileName);

            new File(Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName).delete();

            BitmapHelper.saveBitmap(large, Singleton.getExternalMediaFolderPath() + pictureFileName, Bitmap.CompressFormat.JPEG);
            BitmapHelper.saveBitmap(thumbnail, Singleton.getExternalMediaFolderPath() + thumbnailPictureFileName, Bitmap.CompressFormat.JPEG);

            if(!needUploadPicture)
            {
                imagePickerDialogCallback.onResult(true, false, pictureFileName);
                return;
            }

            needCreateThumbnail = true;
            showProgressDialog();

            ArrayList<String> filesToUpload = new ArrayList<String>();
            filesToUpload.add(pictureFileName);

            if(needCreateThumbnail)
            {
                filesToUpload.add(thumbnailPictureFileName);
            }

            MultiFileUploader uploader = new MultiFileUploader(filesToUpload);

            uploader.startUpload(mCtx, new MultiFileUploader.MultiFileUploaderCallback()
            {
                @Override
                public void didComplete()
                {
                    hideProgressDialog();
                    showCompleteToast();
                    imagePickerDialogCallback.onResult(true, true, pictureFileName);
                }

                @Override
                public void didFail()
                {
                    hideProgressDialog();
                    showNetworkUnstableToast();
                    imagePickerDialogCallback.onResult(true, false, pictureFileName);
                }
            });
        }
        catch (Exception e)
        {

        }
    }

    public int getConfig(String key, int def)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        return settings.getInt(key, def);
    }

    public void showAlertDialog(String title, String message)
    {
        showOKDialog(title, message, null);
    }

    public void showOKDialog(String title, String message, final TestActivity.DialogCallback callback) {
        try {
            final Dialog dialog = new Dialog(mCtx, R.style.MyDialog);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.my_dialog);

            RelativeLayout rootView = (RelativeLayout) dialog.findViewById(R.id.rootView);
            Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
            View marginView = (View) dialog.findViewById(R.id.marginView);
            Button okButton = (Button) dialog.findViewById(R.id.okButton);
            TextView titleTextView = (TextView) dialog.findViewById(R.id.titleTextView);
            TextView messageTextView = (TextView) dialog.findViewById(R.id.messageTextView);

            rootView.getLayoutParams().width = Singleton.SCREEN_WIDTH * 8 / 10;

            cancelButton.setVisibility(View.GONE);
            marginView.setVisibility(View.GONE);

            okButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();

                    if(callback!=null) {
                        callback.onResult(true);
                    }
                }
            });

            titleTextView.setText(title);
            messageTextView.setText(message);

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    dialog.dismiss();

                    callback.onResult(false);
                }
            });

            dialog.show();
        } catch(Exception e) {

        }
    }

    public void showProgressDialog()
    {
        try
        {
            if(progressDialog==null)
            {
                progressDialog = ProgressDialog.show(mCtx, "", getString(R.string.processing));
            }

            progressDialog.setCancelable(true);
        }
        catch (Exception e)
        {
        }
    }

    public void hideProgressDialog()
    {
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showCompleteToast() {
        showToast(getString(R.string.done), R.drawable.happy);
    }
}
