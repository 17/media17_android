package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.GiftLeaderboardModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by POPO on 2015/11/17.
 */
public class PresentBoardActivity extends BaseNewActivity
{
    private PresentBoardActivity mCtx = this;
    private ViewPager mViewPager;
    private View mViewDay,mViewWeek;
    private List<View> mViewList;

    private TextView mTab1,mTab2;

    private LayoutInflater inflater;
    private DisplayImageOptions SelfOptions;
    private SmartTabLayout mPagerTab;

    private ArrayList<GiftLeaderboardModel> mModelsTotal = new ArrayList<GiftLeaderboardModel>();
    private ArrayList<GiftLeaderboardModel> mModelsToday = new ArrayList<GiftLeaderboardModel>();

    private int loves[] = {R.drawable.giftpoint_1,R.drawable.giftpoint_2,R.drawable.giftpoint_3,R.drawable.giftpoint_4,R.drawable.giftpoint_5,R.drawable.giftpoint_6,R.drawable.giftpoint_7};

    private String mUserId = "";
    private String mOpenId = "";

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.present_board_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        Bundle mBundle = getIntent().getExtras();
        if (mBundle != null)
        {
            if(mBundle.containsKey("userId")) mUserId = mBundle.getString("userId");
            if(mBundle.containsKey("openId")) mOpenId = mBundle.getString("openId");
        }

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        initTitleBar();

        LayoutInflater lf = getLayoutInflater().from(mCtx);
        mViewDay = lf.inflate(R.layout.leaderboard_list, null);
        mViewWeek = lf.inflate(R.layout.leaderboard_list, null);

        mViewList = new ArrayList<View>();
        mViewList.add(mViewDay);
        mViewList.add(mViewWeek);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);

        mPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mTab1.setTextColor(getResources().getColor(R.color.main_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
                    case 1:
                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.main_color));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mTab1 = (TextView) findViewById(R.id.tab1);
        mTab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0);
            }
        });
        mTab2 = (TextView) findViewById(R.id.tab2);
        mTab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(1);
            }
        });

        mViewPager.setAdapter(mPagerAdapter);
        mPagerTab.setViewPager(mViewPager);

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(String.format(getString(R.string.my_presend_leader_board),mOpenId));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private PagerAdapter mPagerAdapter = new PagerAdapter()
    {
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            return mViewList.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mViewList.get(position));
        }

        @Override
        public int getItemPosition(Object object)
        {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            if(position==0)
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);
                final ListView mList = (ListView) view.findViewById(R.id.list);
                final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
                final ImageView nodata = (ImageView) view.findViewById(R.id.nodata);

                if(mModelsTotal.size()==0)
                {
                    progress.setVisibility(View.VISIBLE);
                    ApiManager.getGiftLeaderboardAll(mCtx, mUserId, 0, 100, "total", new ApiManager.GetGiftLeaderboardCallback() {
                        @Override
                        public void onResult(boolean success, ArrayList<GiftLeaderboardModel> giftLeaderboard)
                        {
                            progress.setVisibility(View.GONE);
                            if (success && giftLeaderboard != null) {
                                if (giftLeaderboard.size() != 0) {
                                    mModelsTotal.clear();
                                    mModelsTotal.addAll(giftLeaderboard);
                                    nodata.setVisibility(View.GONE);
                                    mList.setAdapter(new LeaderAdapter(mModelsTotal));
                                }
                                else nodata.setVisibility(View.VISIBLE);
                            }
                            else {
                                try{
//                                      showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                else
                {
                    mList.setAdapter(new LeaderAdapter(mModelsTotal));
                }

                return mViewList.get(position);
            }
            else
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);
                final ListView mList = (ListView) view.findViewById(R.id.list);
                final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);

                //final ImageView nodata = (ImageView) view.findViewById(R.id.nodata); -- nodata : It will be TextView or ImageView
                final View nodata = view.findViewById(R.id.nodata);

                if(mModelsToday.size()==0)
                {
                    progress.setVisibility(View.VISIBLE);
                    ApiManager.getGiftLeaderboardToday(mCtx, mUserId, 0, 100, "yyyymmdd", new ApiManager.GetGiftLeaderboardCallback() {
                        @Override
                        public void onResult(boolean success, ArrayList<GiftLeaderboardModel> giftLeaderboard) {
                            progress.setVisibility(View.GONE);
                            if (success && giftLeaderboard != null) {
                                if (giftLeaderboard.size() != 0) {
                                    mModelsToday.clear();
                                    mModelsToday.addAll(giftLeaderboard);
                                    nodata.setVisibility(View.GONE);
                                    mList.setAdapter(new LeaderAdapter(mModelsToday));
                                }
                                else nodata.setVisibility(View.VISIBLE);
                            }
                            else {
                                try{
//                                      showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                else
                {
                    mList.setAdapter(new LeaderAdapter(mModelsToday));
                }

                return mViewList.get(position);
            }
        }
    };

    private class LeaderAdapter extends BaseAdapter
    {
        private ArrayList<GiftLeaderboardModel> mModels = new ArrayList<GiftLeaderboardModel>();

        public LeaderAdapter(ArrayList<GiftLeaderboardModel> model)
        {
            mModels.addAll(model);
        }

        @Override
        public int getCount()
        {
            return mModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.present_board_row, null);
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.open = (TextView) convertView.findViewById(R.id.open);
                holder.love = (ImageView) convertView.findViewById(R.id.love);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.like = (TextView) convertView.findViewById(R.id.like);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.king = (ImageView) convertView.findViewById(R.id.king);
                holder.num = (TextView) convertView.findViewById(R.id.num);

                holder.number1_layout = (RelativeLayout) convertView.findViewById(R.id.number1_layout);
                holder.number1_pic = (ImageView) convertView.findViewById(R.id.number1_pic);
                holder.number1_like = (TextView) convertView.findViewById(R.id.number1_like);
                holder.number1_love = (ImageView) convertView.findViewById(R.id.number1_love);
                holder.number1_king = (ImageView) convertView.findViewById(R.id.number1_king);
                holder.number1_open = (TextView) convertView.findViewById(R.id.number1_open);
                holder.number1_verifie = (ImageView) convertView.findViewById(R.id.number1_verifie);
                holder.number1_name = (TextView) convertView.findViewById(R.id.number1_name);

                holder.number2_layout = (RelativeLayout) convertView.findViewById(R.id.number2_layout);
                holder.number2_pic = (ImageView) convertView.findViewById(R.id.number2_pic);
                holder.number2_like = (TextView) convertView.findViewById(R.id.number2_like);
                holder.number2_love = (ImageView) convertView.findViewById(R.id.number2_love);
                holder.number2_king = (ImageView) convertView.findViewById(R.id.number2_king);
                holder.number2_open = (TextView) convertView.findViewById(R.id.number2_open);
                holder.number2_verifie = (ImageView) convertView.findViewById(R.id.number2_verifie);
                holder.number2_name = (TextView) convertView.findViewById(R.id.number2_name);

                holder.number3_layout = (RelativeLayout) convertView.findViewById(R.id.number3_layout);
                holder.number3_pic = (ImageView) convertView.findViewById(R.id.number3_pic);
                holder.number3_like = (TextView) convertView.findViewById(R.id.number3_like);
                holder.number3_love = (ImageView) convertView.findViewById(R.id.number3_love);
                holder.number3_king = (ImageView) convertView.findViewById(R.id.number3_king);
                holder.number3_open = (TextView) convertView.findViewById(R.id.number3_open);
                holder.number3_verifie = (ImageView) convertView.findViewById(R.id.number3_verifie);
                holder.number3_name = (TextView) convertView.findViewById(R.id.number3_name);

                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            final int pos = position;

            if(pos==0)
            {
                holder.number1_layout.setVisibility(View.VISIBLE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.GONE);

                holder.number1_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mModels.get(pos).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                            Intent intent = new Intent();
                            intent.setClass(mCtx, HomeUserActivity.class);
                            intent.putExtra("title", mModels.get(pos).getUserInfo().getName());
                            intent.putExtra("picture", mModels.get(pos).getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mModels.get(pos).getUserInfo().getIsFollowing());
                            intent.putExtra("post", mModels.get(pos).getUserInfo().getPostCount());
                            intent.putExtra("follow", mModels.get(pos).getUserInfo().getFollowerCount());
                            intent.putExtra("following", mModels.get(pos).getUserInfo().getFollowingCount());
                            intent.putExtra("open", mModels.get(pos).getUserInfo().getOpenID());
                            intent.putExtra("bio", mModels.get(pos).getUserInfo().getBio());
                            intent.putExtra("targetUserID", mModels.get(pos).getUserInfo().getUserID());
                            intent.putExtra("web", mModels.get(pos).getUserInfo().getWebsite());
                            startActivity(intent);
                        }
                    }
                });

                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getUserInfo().getPicture()), holder.number1_pic, SelfOptions);

                holder.number1_like.setText(String.valueOf(mModels.get(pos).getPoint()));
                holder.number1_love.setImageResource(loves[(int) (Math.random() * loves.length)]);
                holder.number1_king.setImageResource(R.drawable.no1);
                holder.number1_open.setText(mModels.get(position).getUserInfo().getOpenID());

                holder.number1_name.setText(mModels.get(position).getUserInfo().getName());

                if (mModels.get(pos).getUserInfo().getIsVerified() == 1)
                    holder.number1_verifie.setVisibility(View.VISIBLE);
                else holder.number1_verifie.setVisibility(View.GONE);
            }
            else if(pos==1)
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.VISIBLE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.GONE);

                holder.number2_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mModels.get(pos).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                            Intent intent = new Intent();
                            intent.setClass(mCtx, HomeUserActivity.class);
                            intent.putExtra("title", mModels.get(pos).getUserInfo().getName());
                            intent.putExtra("picture", mModels.get(pos).getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mModels.get(pos).getUserInfo().getIsFollowing());
                            intent.putExtra("post", mModels.get(pos).getUserInfo().getPostCount());
                            intent.putExtra("follow", mModels.get(pos).getUserInfo().getFollowerCount());
                            intent.putExtra("following", mModels.get(pos).getUserInfo().getFollowingCount());
                            intent.putExtra("open", mModels.get(pos).getUserInfo().getOpenID());
                            intent.putExtra("bio", mModels.get(pos).getUserInfo().getBio());
                            intent.putExtra("targetUserID", mModels.get(pos).getUserInfo().getUserID());
                            intent.putExtra("web", mModels.get(pos).getUserInfo().getWebsite());
                            startActivity(intent);
                        }
                    }
                });

                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getUserInfo().getPicture()), holder.number2_pic, SelfOptions);

                holder.number2_like.setText(String.valueOf(mModels.get(pos).getPoint()));
                holder.number2_love.setImageResource(loves[(int) (Math.random() * loves.length)]);
                holder.number2_king.setImageResource(R.drawable.no2);
                holder.number2_open.setText(mModels.get(position).getUserInfo().getOpenID());

                holder.number2_name.setText(mModels.get(position).getUserInfo().getName());

                if (mModels.get(pos).getUserInfo().getIsVerified() == 1)
                    holder.number2_verifie.setVisibility(View.VISIBLE);
                else holder.number2_verifie.setVisibility(View.GONE);
            }
            else if(pos==2)
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.VISIBLE);
                holder.layout.setVisibility(View.GONE);

                holder.number3_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mModels.get(pos).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                            Intent intent = new Intent();
                            intent.setClass(mCtx, HomeUserActivity.class);
                            intent.putExtra("title", mModels.get(pos).getUserInfo().getName());
                            intent.putExtra("picture", mModels.get(pos).getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mModels.get(pos).getUserInfo().getIsFollowing());
                            intent.putExtra("post", mModels.get(pos).getUserInfo().getPostCount());
                            intent.putExtra("follow", mModels.get(pos).getUserInfo().getFollowerCount());
                            intent.putExtra("following", mModels.get(pos).getUserInfo().getFollowingCount());
                            intent.putExtra("open", mModels.get(pos).getUserInfo().getOpenID());
                            intent.putExtra("bio", mModels.get(pos).getUserInfo().getBio());
                            intent.putExtra("targetUserID", mModels.get(pos).getUserInfo().getUserID());
                            intent.putExtra("web", mModels.get(pos).getUserInfo().getWebsite());
                            startActivity(intent);
                        }
                    }
                });

                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getUserInfo().getPicture()), holder.number3_pic, SelfOptions);

                holder.number3_like.setText(String.valueOf(mModels.get(pos).getPoint()));
                holder.number3_love.setImageResource(loves[(int) (Math.random() * loves.length)]);
                holder.number3_king.setImageResource(R.drawable.no3);
                holder.number3_open.setText(mModels.get(position).getUserInfo().getOpenID());

                holder.number3_name.setText(mModels.get(position).getUserInfo().getName());

                if (mModels.get(pos).getUserInfo().getIsVerified() == 1)
                    holder.number3_verifie.setVisibility(View.VISIBLE);
                else holder.number3_verifie.setVisibility(View.GONE);
            }
            else
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.VISIBLE);

                holder.layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mModels.get(pos).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                            Intent intent = new Intent();
                            intent.setClass(mCtx, HomeUserActivity.class);
                            intent.putExtra("title", mModels.get(pos).getUserInfo().getName());
                            intent.putExtra("picture", mModels.get(pos).getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mModels.get(pos).getUserInfo().getIsFollowing());
                            intent.putExtra("post", mModels.get(pos).getUserInfo().getPostCount());
                            intent.putExtra("follow", mModels.get(pos).getUserInfo().getFollowerCount());
                            intent.putExtra("following", mModels.get(pos).getUserInfo().getFollowingCount());
                            intent.putExtra("open", mModels.get(pos).getUserInfo().getOpenID());
                            intent.putExtra("bio", mModels.get(pos).getUserInfo().getBio());
                            intent.putExtra("targetUserID", mModels.get(pos).getUserInfo().getUserID());
                            intent.putExtra("web", mModels.get(pos).getUserInfo().getWebsite());
                            startActivity(intent);
                        }
                    }
                });

                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getUserInfo().getPicture()), holder.pic, SelfOptions);
                holder.pic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mModels.get(pos).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                            Intent intent = new Intent();
                            intent.setClass(mCtx, HomeUserActivity.class);
                            intent.putExtra("title", mModels.get(pos).getUserInfo().getName());
                            intent.putExtra("picture", mModels.get(pos).getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mModels.get(pos).getUserInfo().getIsFollowing());
                            intent.putExtra("post", mModels.get(pos).getUserInfo().getPostCount());
                            intent.putExtra("follow", mModels.get(pos).getUserInfo().getFollowerCount());
                            intent.putExtra("following", mModels.get(pos).getUserInfo().getFollowingCount());
                            intent.putExtra("open", mModels.get(pos).getUserInfo().getOpenID());
                            intent.putExtra("bio", mModels.get(pos).getUserInfo().getBio());
                            intent.putExtra("targetUserID", mModels.get(pos).getUserInfo().getUserID());
                            intent.putExtra("web", mModels.get(pos).getUserInfo().getWebsite());
                            startActivity(intent);
                        }
                    }
                });

                holder.open.setText(mModels.get(position).getUserInfo().getOpenID());
                holder.open.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mModels.get(pos).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                            Intent intent = new Intent();
                            intent.setClass(mCtx, HomeUserActivity.class);
                            intent.putExtra("title", mModels.get(pos).getUserInfo().getName());
                            intent.putExtra("picture", mModels.get(pos).getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mModels.get(pos).getUserInfo().getIsFollowing());
                            intent.putExtra("post", mModels.get(pos).getUserInfo().getPostCount());
                            intent.putExtra("follow", mModels.get(pos).getUserInfo().getFollowerCount());
                            intent.putExtra("following", mModels.get(pos).getUserInfo().getFollowingCount());
                            intent.putExtra("open", mModels.get(pos).getUserInfo().getOpenID());
                            intent.putExtra("bio", mModels.get(pos).getUserInfo().getBio());
                            intent.putExtra("targetUserID", mModels.get(pos).getUserInfo().getUserID());
                            intent.putExtra("web", mModels.get(pos).getUserInfo().getWebsite());
                            startActivity(intent);
                        }
                    }
                });

                if (pos == 0) {
                    holder.king.setImageResource(R.drawable.no1);
                    holder.king.setVisibility(View.VISIBLE);
                    holder.num.setVisibility(View.INVISIBLE);
                } else if (pos == 1) {
                    holder.king.setImageResource(R.drawable.no2);
                    holder.king.setVisibility(View.VISIBLE);
                    holder.num.setVisibility(View.INVISIBLE);
                } else if (pos == 2) {
                    holder.king.setImageResource(R.drawable.no3);
                    holder.king.setVisibility(View.VISIBLE);
                    holder.num.setVisibility(View.INVISIBLE);
                } else {
                    holder.king.setVisibility(View.INVISIBLE);
                    holder.num.setText(String.valueOf(pos + 1));
                    holder.num.setVisibility(View.VISIBLE);
                }
                holder.name.setText(mModels.get(position).getUserInfo().getName());
                holder.like.setText(String.valueOf(mModels.get(pos).getPoint()));
                holder.love.setImageResource(loves[(int) (Math.random() * loves.length)]);

                if (mModels.get(pos).getUserInfo().getIsVerified() == 1)
                    holder.verifie.setVisibility(View.VISIBLE);
                else holder.verifie.setVisibility(View.GONE);

            }

            return convertView;
        }
    }

    private class ViewHolder
    {
        LinearLayout layout;
        ImageView pic;
        TextView open;
        ImageView love;
        TextView name;
        TextView like;
        ImageView verifie;

        ImageView king;
        TextView num;

        RelativeLayout number1_layout;
        ImageView number1_pic;
        TextView number1_like;
        ImageView number1_love;
        ImageView number1_king;
        TextView number1_open;
        ImageView number1_verifie;
        TextView number1_name;

        RelativeLayout number2_layout;
        ImageView number2_pic;
        TextView number2_like;
        ImageView number2_love;
        ImageView number2_king;
        TextView number2_open;
        ImageView number2_verifie;
        TextView number2_name;

        RelativeLayout number3_layout;
        ImageView number3_pic;
        TextView number3_like;
        ImageView number3_love;
        ImageView number3_king;
        TextView number3_open;
        ImageView number3_verifie;
        TextView number3_name;
    }
}
