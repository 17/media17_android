package com.machipopo.media17;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by POPO on 15/8/19.
 */
public class ForgotPasswordActivity extends BaseActivity
{
    private ForgotPasswordActivity mCtx = this;
    private EditText mAccount, mEmail;
    private ConnectivityManager mConnectivityManager;
    private NetworkInfo mNetworkInfo;
    private RelativeLayout btnNext;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);

        initTitleBar();

        mAccount = (EditText) findViewById(R.id.account);
        mEmail = (EditText) findViewById(R.id.email);
        btnNext = (RelativeLayout) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(ButtonClickEventListener);

        showKeyboard();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.forgot_password));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_back);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mCtx.finish();
            }
        });
    }

    private View.OnClickListener ButtonClickEventListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            processLogin();
        }
    };

    private void processLogin()
    {
        if(mAccount.getText().toString().length()!=0 && mEmail.getText().toString().length()!=0)
        {
            mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

            if (mNetworkInfo != null && mNetworkInfo.isConnected())
            {
                hideKeyboard();
                showProgressDialog();
                ApiManager.recoverPassword(mCtx, mAccount.getText().toString(), mEmail.getText().toString(), new ApiManager.RecoverPasswordCallback()
                {
                    @Override
                    public void onResult(boolean networksuccess, boolean success, String message)
                    {
                        hideProgressDialog();

                        if(networksuccess)
                        {
                            if(success)
                            {
                                showOKDialog(getString(R.string.prompt), getString(R.string.forgot_password_new), new TestActivity.DialogCallback() {
                                    @Override
                                    public void onResult(boolean didClickOk) {
                                        mCtx.finish();
                                    }
                                });
                            }
                            else
                            {
                                showAlertDialog(getString(R.string.prompt), getString(R.string.forgot_password_faile));
                            }
                        }
                        else
                        {
                            showAlertDialog(getString(R.string.prompt), getString(R.string.error_failed));
                        }
                    }
                });
            }
            else
            {
                hideKeyboard();
                try{
//                                                showToast(getString(R.string.login_internet));
                    Toast.makeText(mCtx, getString(R.string.login_internet), Toast.LENGTH_SHORT).show();
                }
                catch (Exception e){
                }
            }
        }
        else
        {
            hideKeyboard();
            try{
//                                                showToast(getString(R.string.login_enter));
                Toast.makeText(mCtx, getString(R.string.login_enter), Toast.LENGTH_SHORT).show();
            }
            catch (Exception e){
            }
        }
    }
}
