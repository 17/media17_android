package com.machipopo.media17;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by POPO on 5/21/15.
 */
public class PhoneNumberActivity extends BaseActivity
{
    private PhoneNumberActivity mCtx = this;
    private EditText mPhone;

    private TextView mCountryText, mCountryCode;
    private LinearLayout mCountryLayout;

    private HashMap<String,String> CODE = new HashMap<String,String>();
    private HashMap<String,String> NAME = new HashMap<String,String>();

    private String[] mListCountry ;
    private String[] mListCode ;

    private String UserCode = "886";

    private ImageView mPhoneIcon;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_number_activity);

        initTitleBar();

        mPhone = (EditText) findViewById(R.id.phone);

        mCountryLayout = (LinearLayout) findViewById(R.id.country_layout);
        mCountryText = (TextView) findViewById(R.id.country_text);
        mCountryCode = (TextView) findViewById(R.id.country_code);
        mPhoneIcon = (ImageView) findViewById(R.id.phone_icon);

        mCountryLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mListCountry!=null)
                {
                    new AlertDialog.Builder(mCtx).setItems(mListCountry, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(mListCode!=null)
                            {
                                UserCode = mListCode[which];
                                mCountryCode.setText("+" + UserCode);
                                mCountryText.setText(mListCountry[which]);
                            }
                        }
                    })
                    .show();
                }
            }
        });

        mCountryCode.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mListCountry!=null)
                {
                    new AlertDialog.Builder(mCtx).setItems(mListCountry, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(mListCode!=null)
                            {
                                UserCode = mListCode[which];
                                mCountryCode.setText("+" + UserCode);
                                mCountryText.setText(mListCountry[which]);
                            }
                        }
                    })
                    .show();
                }
            }
        });

        mPhoneIcon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mListCountry!=null)
                {
                    new AlertDialog.Builder(mCtx).setItems(mListCountry, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(mListCode!=null)
                            {
                                UserCode = mListCode[which];
                                mCountryCode.setText("+" + UserCode);
                                mCountryText.setText(mListCountry[which]);
                            }
                        }
                    })
                    .show();
                }
            }
        });

        try
        {
            String code = ReadFromfile("country_code.txt",mCtx);
            JSONArray mCodeArray = new JSONArray(code);
            mListCountry = new String[mCodeArray.length()];
            mListCode = new String[mCodeArray.length()];
            for(int i = 0 ; i < mCodeArray.length() ; i++)
            {
                CODE.put(mCodeArray.getJSONObject(i).getString("country"), mCodeArray.getJSONObject(i).getString("countryCode"));
                NAME.put(mCodeArray.getJSONObject(i).getString("countryCode"), mCodeArray.getJSONObject(i).getString("country"));
                mListCode[i] = mCodeArray.getJSONObject(i).getString("countryCode");
                mListCountry[i] = getResources().getString(getResources().getIdentifier(mCodeArray.getJSONObject(i).getString("country"), "string", getPackageName()));
            }
        }
        catch (JSONException e)
        {
        }

        ApiManager.getCountry(mCtx, new ApiManager.GetCountryCallback()
        {
            @Override
            public void onResult(boolean success, String message)
            {
                if (message.length() != 0)
                {
                    UserCode = CODE.get(message);
                    mCountryCode.setText("+" + CODE.get(message));
                    mCountryText.setText(NAME.get(CODE.get(message)));
                }
                else
                {
                    mCountryCode.setText("+886");
                    mCountryText.setText("Taiwan");
                }
            }
        });
    }

    public String ReadFromfile(String fileName, Context context)
    {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try
        {
            fIn = context.getResources().getAssets().open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null)
            {
                returnString.append(line);
            }
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally
        {
            try
            {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            }
            catch (Exception e2)
            {
                e2.getMessage();
            }
        }

        return returnString.toString();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.phone_number));
        mTitle.setTextColor(Color.WHITE);

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.next));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
                if(mPhone.getText().toString().length()==0)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx,FacebookActivity.class);
                    startActivity(intent);
                    mCtx.finish();
                }
                else
                {
                    try
                    {
                        showProgressDialog();
                        String number = "";
                        if(mPhone.getText().toString().substring(0,1).contains("0"))
                        {
                            number = mPhone.getText().toString().substring(1,mPhone.getText().toString().length());
                        }
                        else number = mPhone.getText().toString();

                        setConfig("phone",number);
                        setConfig("countryCode",UserCode);

                        JSONObject params = new JSONObject();
                        params.put("phoneNumber", number);
                        params.put("countryCode", UserCode);
                        params.put("phoneWithCountryCode", UserCode+number);
                        ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                        {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                hideProgressDialog();
                                if(success)
                                {
                                    Intent intent = new Intent();
                                    intent.setClass(mCtx,FacebookActivity.class);
                                    startActivity(intent);
                                    mCtx.finish();
                                }
                                else {
                                    try{
//                                                showToast(getString(R.error_failed.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        hideProgressDialog();
                        try{
//                                                showToast(getString(R.error_failed.error_failed));
                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            }
        });
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {


            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
