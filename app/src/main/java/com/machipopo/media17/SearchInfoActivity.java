package com.machipopo.media17;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

/**
 * Created by POPO on 6/1/15.
 */
public class SearchInfoActivity extends BaseFragmentActivity
{
    private SearchInfoActivity mCtx = this;

    private EditText mEdit;
    private Button mCancel;

    private InputMethodManager inputMethodManager;

    private ViewPager mViewPager;
    private SmartTabLayout mPagerTab;

    private TextView mTab1,mTab2;

    private SearchPeopleFragment mSearchPeopleFragment;
    private SearchTagFragment mSearchTagFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_info_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mEdit = (EditText) findViewById(R.id.edit);
        mCancel = (Button) findViewById(R.id.cancel);

        mTab1 = (TextView) findViewById(R.id.tab1);
        mTab1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mViewPager.setCurrentItem(0);
            }
        });
        mTab2 = (TextView) findViewById(R.id.tab2);
        mTab2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mViewPager.setCurrentItem(1);
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                mCtx.finish();
            }
        });

        mEdit.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(mViewPager==null || mSearchPeopleFragment.isAdded()==false) {
                    return;
                }

                if (mViewPager.getCurrentItem() == 0) mSearchPeopleFragment.setSearchText(s.toString());
                else mSearchTagFragment.setSearchText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });

        mSearchPeopleFragment = new SearchPeopleFragment();
        mSearchTagFragment = new SearchTagFragment();

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyPagerAdapter(mCtx.getSupportFragmentManager()));

        mPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        mPagerTab.setViewPager(mViewPager);
        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                if(position==0)
                {
                    mTab1.setTextColor(getResources().getColor(R.color.main_color));
                    mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                    mSearchPeopleFragment.setSearchText(mEdit.getText().toString());
                }
                else
                {
                    mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                    mTab2.setTextColor(getResources().getColor(R.color.main_color));
                    mSearchTagFragment.setSearchText(mEdit.getText().toString());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter
    {
        public MyPagerAdapter(FragmentManager fragmentManager)
        {
            super(fragmentManager);
        }

        @Override
        public int getCount()
        {
            return 2;
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
                case 0:
                    return mSearchPeopleFragment;
                case 1:
                    return mSearchTagFragment;
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return "";
        }
    }
}
