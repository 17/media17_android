package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by POPO on 15/8/14.
 */
public class LeaderboardActivity extends BaseNewActivity
{
    private LeaderboardActivity mCtx = this;
    private ViewPager mViewPager;
    private View mViewHour,mViewDay,mViewWeek,mViewMonth,mViewYear;//,mViewAll
    private List<View> mViewList;

    private TextView mTab1,mTab2,mTab3,mTab4,mTab5;//,mTab6

    private LayoutInflater inflater;
    private DisplayImageOptions SelfOptions;
    private SmartTabLayout mPagerTab;
    private Story17Application mApplication;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};

    private ArrayList<UserModel> mModelsHour = new ArrayList<UserModel>();
    private ArrayList<UserModel> mModelsDay = new ArrayList<UserModel>();
    private ArrayList<UserModel> mModelsWeek = new ArrayList<UserModel>();
    private ArrayList<UserModel> mModelsMonth = new ArrayList<UserModel>();
    private ArrayList<UserModel> mModelsYear = new ArrayList<UserModel>();
//    private ArrayList<UserModel> mModelsAll = new ArrayList<UserModel>();

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaderboard_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        initTitleBar();

        LayoutInflater lf = getLayoutInflater().from(mCtx);
        mViewHour = lf.inflate(R.layout.leaderboard_list, null);
        mViewDay = lf.inflate(R.layout.leaderboard_list, null);
        mViewWeek = lf.inflate(R.layout.leaderboard_list, null);
        mViewMonth = lf.inflate(R.layout.leaderboard_list, null);
        mViewYear = lf.inflate(R.layout.leaderboard_list, null);
//        mViewAll = lf.inflate(R.layout.leaderboard_list, null);

        mViewList = new ArrayList<View>();
        mViewList.add(mViewHour);
        mViewList.add(mViewDay);
        mViewList.add(mViewWeek);
        mViewList.add(mViewMonth);
        mViewList.add(mViewYear);
//        mViewList.add(mViewAll);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mApplication = (Story17Application) getApplication();

        mPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mTab1.setTextColor(getResources().getColor(R.color.main_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab4.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab5.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab6.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
                    case 1:
                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.main_color));
                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab4.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab5.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab6.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
                    case 2:
                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab3.setTextColor(getResources().getColor(R.color.main_color));
                        mTab4.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab5.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab6.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
                    case 3:
                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab4.setTextColor(getResources().getColor(R.color.main_color));
                        mTab5.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab6.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
                    case 4:
                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab4.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab5.setTextColor(getResources().getColor(R.color.main_color));
//                        mTab6.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
//                    case 5:
//                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab4.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab5.setTextColor(getResources().getColor(R.color.second_main_text_color));
//                        mTab6.setTextColor(getResources().getColor(R.color.main_color));
//                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mTab1 = (TextView) findViewById(R.id.tab1);
        mTab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0);
            }
        });
        mTab2 = (TextView) findViewById(R.id.tab2);
        mTab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(1);
            }
        });
        mTab3 = (TextView) findViewById(R.id.tab3);
        mTab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(2);
            }
        });
        mTab4 = (TextView) findViewById(R.id.tab4);
        mTab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(3);
            }
        });
        mTab5 = (TextView) findViewById(R.id.tab5);
        mTab5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(4);
            }
        });
//        mTab6 = (TextView) findViewById(R.id.tab6);
//        mTab6.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mViewPager.setCurrentItem(5);
//            }
//        });

        mViewPager.setAdapter(mPagerAdapter);
        mPagerTab.setViewPager(mViewPager);

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.leaderboard_title));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private PagerAdapter mPagerAdapter = new PagerAdapter()
    {
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            return mViewList.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mViewList.get(position));
        }

        @Override
        public int getItemPosition(Object object)
        {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            if(position==0)
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);
                final ListView mList = (ListView) view.findViewById(R.id.list);
                final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
//                final ImageView nodata = (ImageView) view.findViewById(R.id.nodata);

                if(mModelsHour.size()==0)
                {
                    progress.setVisibility(View.VISIBLE);
                    ApiManager.getLikeLeaderboardHour(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, "hourly", new ApiManager.GetLikeLeaderboardCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<UserModel> models) {
                            progress.setVisibility(View.GONE);
                            if (success && models != null) {
                                if (models.size() != 0) {
                                    mModelsHour.clear();
                                    mModelsHour.addAll(models);

                                    mList.setAdapter(new LeaderAdapter(mModelsHour));
                                } else {
//                                    nodata.setVisibility(View.VISIBLE);
                                }
                            } else {
//                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                else
                {
                    mList.setAdapter(new LeaderAdapter(mModelsHour));
                }

                return mViewList.get(position);
            }
            else if(position==1)
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);
                final ListView mList = (ListView) view.findViewById(R.id.list);
                final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
//                final ImageView nodata = (ImageView) view.findViewById(R.id.nodata);

                if(mModelsDay.size()==0)
                {
                    progress.setVisibility(View.VISIBLE);
                    ApiManager.getLikeLeaderboardDay(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, "daily", new ApiManager.GetLikeLeaderboardCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<UserModel> models) {
                            progress.setVisibility(View.GONE);
                            if (success && models != null) {
                                if (models.size() != 0) {
                                    mModelsDay.clear();
                                    mModelsDay.addAll(models);

                                    mList.setAdapter(new LeaderAdapter(mModelsDay));
                                } else {
//                                    nodata.setVisibility(View.VISIBLE);
                                }
                            } else {
//                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                else
                {
                    mList.setAdapter(new LeaderAdapter(mModelsDay));
                }

                return mViewList.get(position);
            }
            else if(position==2)
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);
                final ListView mList = (ListView) view.findViewById(R.id.list);
                final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
//                final ImageView nodata = (ImageView) view.findViewById(R.id.nodata);

                if(mModelsWeek.size()==0)
                {
                    progress.setVisibility(View.VISIBLE);
                    ApiManager.getLikeLeaderboardWeek(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, "weekly", new ApiManager.GetLikeLeaderboardCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<UserModel> models) {
                            progress.setVisibility(View.GONE);
                            if (success && models != null) {
                                if (models.size() != 0) {
                                    mModelsWeek.clear();
                                    mModelsWeek.addAll(models);

                                    mList.setAdapter(new LeaderAdapter(mModelsWeek));
                                } else {
//                                    nodata.setVisibility(View.VISIBLE);
                                }
                            } else {
//                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                else
                {
                    mList.setAdapter(new LeaderAdapter(mModelsWeek));
                }

                return mViewList.get(position);
            }
            else if(position==3)
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);
                final ListView mList = (ListView) view.findViewById(R.id.list);
                final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
//                final ImageView nodata = (ImageView) view.findViewById(R.id.nodata);

                if(mModelsMonth.size()==0)
                {
                    progress.setVisibility(View.VISIBLE);
                    ApiManager.getLikeLeaderboardMonth(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, "monthly", new ApiManager.GetLikeLeaderboardCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<UserModel> models) {
                            progress.setVisibility(View.GONE);
                            if (success && models != null) {
                                if (models.size() != 0) {
                                    mModelsMonth.clear();
                                    mModelsMonth.addAll(models);

                                    mList.setAdapter(new LeaderAdapter(mModelsMonth));
                                } else {
//                                    nodata.setVisibility(View.VISIBLE);
                                }
                            } else {
//                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                else
                {
                    mList.setAdapter(new LeaderAdapter(mModelsMonth));
                }

                return mViewList.get(position);
            }
            else //if(position==4)
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);
                final ListView mList = (ListView) view.findViewById(R.id.list);
                final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
//                final ImageView nodata = (ImageView) view.findViewById(R.id.nodata);

                if(mModelsYear.size()==0)
                {
                    progress.setVisibility(View.VISIBLE);
                    ApiManager.getLikeLeaderboardYear(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, "yearly", new ApiManager.GetLikeLeaderboardCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<UserModel> models) {
                            progress.setVisibility(View.GONE);
                            if (success && models != null) {
                                if (models.size() != 0) {
                                    mModelsYear.clear();
                                    mModelsYear.addAll(models);

                                    mList.setAdapter(new LeaderAdapter(mModelsYear));
                                } else {
//                                    nodata.setVisibility(View.VISIBLE);
                                }
                            } else {
//                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                else
                {
                    mList.setAdapter(new LeaderAdapter(mModelsYear));
                }

                return mViewList.get(position);
            }
//            else
//            {
//                container.addView(mViewList.get(position));
//                View view = mViewList.get(position);
//                final ListView mList = (ListView) view.findViewById(R.id.list);
//                final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
////                final ImageView nodata = (ImageView) view.findViewById(R.id.nodata);
//
//                if(mModelsAll.size()==0)
//                {
//                    progress.setVisibility(View.VISIBLE);
//                    ApiManager.getLikeLeaderboard(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), 0, 100, "allTime", new ApiManager.GetLikeLeaderboardCallback() {
//                        @Override
//                        public void onResult(boolean success, String message, ArrayList<UserModel> models) {
//                            progress.setVisibility(View.GONE);
//                            if (success && models != null) {
//                                if (models.size() != 0) {
//                                    mModelsAll.clear();
//                                    mModelsAll.addAll(models);
//
//                                    mList.setAdapter(new LeaderAdapter(mModelsAll));
//                                } else {
////                                    nodata.setVisibility(View.VISIBLE);
//                                }
//                            } else {
////                                nodata.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    });
//                }
//                else
//                {
//                    mList.setAdapter(new LeaderAdapter(mModelsAll));
//                }
//
//                return mViewList.get(position);
//            }
        }
    };

    private class LeaderAdapter extends BaseAdapter
    {
        private ArrayList<UserModel> mModels = new ArrayList<UserModel>();

        public LeaderAdapter(ArrayList<UserModel> model)
        {
            mModels.addAll(model);
        }

        @Override
        public int getCount()
        {
            return mModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.leaderboard_list_row, null);
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                holder.king = (ImageView) convertView.findViewById(R.id.king);
                holder.num = (TextView) convertView.findViewById(R.id.num);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.open = (TextView) convertView.findViewById(R.id.open);
                holder.love = (ImageView) convertView.findViewById(R.id.love);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.like = (TextView) convertView.findViewById(R.id.like);
                holder.follow = (ImageView) convertView.findViewById(R.id.follow);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            final int pos = position;

            holder.layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (mModels.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mModels.get(pos).getName());
                        intent.putExtra("picture", mModels.get(pos).getPicture());
                        intent.putExtra("isfollowing", mModels.get(pos).getIsFollowing());
                        intent.putExtra("post", mModels.get(pos).getPostCount());
                        intent.putExtra("follow", mModels.get(pos).getFollowerCount());
                        intent.putExtra("following", mModels.get(pos).getFollowingCount());
                        intent.putExtra("open", mModels.get(pos).getOpenID());
                        intent.putExtra("bio", mModels.get(pos).getBio());
                        intent.putExtra("targetUserID", mModels.get(pos).getUserID());
                        intent.putExtra("web", mModels.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getPicture()), holder.pic, SelfOptions);
            holder.pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mModels.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mModels.get(pos).getName());
                        intent.putExtra("picture", mModels.get(pos).getPicture());
                        intent.putExtra("isfollowing", mModels.get(pos).getIsFollowing());
                        intent.putExtra("post", mModels.get(pos).getPostCount());
                        intent.putExtra("follow", mModels.get(pos).getFollowerCount());
                        intent.putExtra("following", mModels.get(pos).getFollowingCount());
                        intent.putExtra("open", mModels.get(pos).getOpenID());
                        intent.putExtra("bio", mModels.get(pos).getBio());
                        intent.putExtra("targetUserID", mModels.get(pos).getUserID());
                        intent.putExtra("web", mModels.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.open.setText(mModels.get(position).getOpenID());
            holder.open.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mModels.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mModels.get(pos).getName());
                        intent.putExtra("picture", mModels.get(pos).getPicture());
                        intent.putExtra("isfollowing", mModels.get(pos).getIsFollowing());
                        intent.putExtra("post", mModels.get(pos).getPostCount());
                        intent.putExtra("follow", mModels.get(pos).getFollowerCount());
                        intent.putExtra("following", mModels.get(pos).getFollowingCount());
                        intent.putExtra("open", mModels.get(pos).getOpenID());
                        intent.putExtra("bio", mModels.get(pos).getBio());
                        intent.putExtra("targetUserID", mModels.get(pos).getUserID());
                        intent.putExtra("web", mModels.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            if(pos==0)
            {
                holder.king.setImageResource(R.drawable.no1);
                holder.king.setVisibility(View.VISIBLE);
                holder.num.setVisibility(View.INVISIBLE);
            }
            else if(pos==1)
            {
                holder.king.setImageResource(R.drawable.no2);
                holder.king.setVisibility(View.VISIBLE);
                holder.num.setVisibility(View.INVISIBLE);
            }
            else if(pos==2)
            {
                holder.king.setImageResource(R.drawable.no3);
                holder.king.setVisibility(View.VISIBLE);
                holder.num.setVisibility(View.INVISIBLE);
            }
            else
            {
                holder.king.setVisibility(View.INVISIBLE);
                holder.num.setText(String.valueOf(pos + 1));
                holder.num.setVisibility(View.VISIBLE);
            }

            holder.name.setText(mModels.get(position).getName());
            holder.like.setText(String.valueOf(mModels.get(pos).getLikeCount()));
            holder.love.setImageResource(loves[(int) (Math.random() * loves.length)]);

            if (mModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
            {
                holder.follow.setVisibility(View.INVISIBLE);
            }
            else holder.follow.setVisibility(View.VISIBLE);

            if(mModels.get(position).getIsFollowing()==1)
            {
                holder.follow.setImageResource(R.drawable.follow_down);
            }
            else
            {
                if(mModels.get(pos).getFollowRequestTime()!=0)
                {
                    holder.follow.setImageResource(R.drawable.follow_wait);
                }
                else
                {
                    holder.follow.setImageResource(R.drawable.follow);
                }
            }

            final ImageView btn = holder.follow;
            holder.follow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mModels.get(pos).getIsFollowing()==1)
                    {
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mModels.get(pos).getUserID());
                        }
                        catch (Exception x)
                        {

                        }
                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mModels.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if (success)
                                {
                                    mModels.get(pos).setIsFollowing(0);
                                    btn.setImageResource(R.drawable.follow);
                                }
                                else {
                                    try {
//                          showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    } catch (Exception x) {
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mModels.get(pos).getFollowRequestTime()!=0)
                        {
                            btn.setImageResource(R.drawable.follow);
                            mModels.get(pos).setIsFollowing(0);
                            mModels.get(pos).setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mModels.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try {
//                          showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                                return ;
                            }

                            if(mModels.get(pos).getPrivacyMode().compareTo("private")==0)
                            {
                                btn.setImageResource(R.drawable.follow_wait);
                                mModels.get(pos).setIsFollowing(0);
                                mModels.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mModels.get(pos).getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            btn.setImageResource(R.drawable.follow);
                                            mModels.get(pos).setIsFollowing(0);
                                            mModels.get(pos).setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                try{
                                LogEventUtil.FollowUser(mCtx,mApplication,mModels.get(pos).getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mModels.get(pos).getUserID(), new ApiManager.FollowUserActionCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            mModels.get(pos).setIsFollowing(1);
                                            btn.setImageResource(R.drawable.follow_down);
                                        }
                                        else {
                                            try {
//                          showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            } catch (Exception x) {
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            if(mModels.get(pos).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            return convertView;
        }
    }

    private class ViewHolder
    {
        LinearLayout layout;
        ImageView king;
        TextView num;
        ImageView pic;
        TextView open;
        ImageView love;
        TextView name;
        TextView like;
        ImageView follow;
        ImageView verifie;
    }
}
