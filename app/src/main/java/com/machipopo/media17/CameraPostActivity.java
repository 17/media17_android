package com.machipopo.media17;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.TagModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by POPO on 6/2/15.
 */
public class CameraPostActivity extends BaseNewActivity
{
    private CameraPostActivity mCtx = this;
    private Story17Application mApplication;
    private DisplayMetrics mDisplayMetrics;
    private LayoutInflater inflater;

    private RelativeLayout mTitleBar;
    private ImageView mImg;
    private EditText mEdit;
    private TextView mSize;
    private ListView mTagList,mUserList;

    private String mFile,mFile2,mName,mName2;

    private int taged = 0;
    private Boolean tagedState = false;

    private Boolean mTagState = false;
    private int mTagPos = 0;
    private Boolean mWho = false;

    private JSONArray mTagArray = new JSONArray();;
    private JSONArray mUserArray = new JSONArray();;
    private ArrayList<TagModel> mTagModel = new ArrayList<TagModel>();
    private ArrayList<UserModel> mUserModel = new ArrayList<UserModel>();

    private UserAdapter mUserAdapter;
    private TagAdapter mTagAdapter;
    private Boolean isPost = false;

    private InputMethodManager inputMethodManager = null;
    private ProgressBar mProgress;

    private Boolean mFilter = false;

    private DisplayImageOptions SelfOptions;
    private int startPostPage = 0;

    private PostRetryThread mPostRetryThread;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_post_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("file")) mFile = mBundle.getString("file");
            if(mBundle.containsKey("file2")) mFile2 = mBundle.getString("file2");
            if(mBundle.containsKey("name")) mName = mBundle.getString("name");
            if(mBundle.containsKey("name2")) mName2 = mBundle.getString("name2");

            if(mBundle.containsKey("filter")) mFilter = mBundle.getBoolean("filter");
        }




        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();


        mImg = (ImageView) findViewById(R.id.img);
        mEdit = (EditText) findViewById(R.id.edit);
        mSize = (TextView) findViewById(R.id.size);
        mTagList = (ListView) findViewById(R.id.tag_list);
        mUserList = (ListView) findViewById(R.id.user_list);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        mImg.setImageBitmap(BitmapHelper.getBitmap(mFile2, 1));

        mEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, final int start, int before, int count) {
                int textSize = 500 - s.toString().length();
                mSize.setText(String.valueOf(textSize));

                if (s.toString().length() < taged) tagedState = false;

                if (!tagedState) {
                    if (s.toString().contains("#") || s.toString().contains("@")) mTagState = true;
                    else {
                        mTagList.setVisibility(View.GONE);
                        mUserList.setVisibility(View.GONE);

                        mTagState = false;
                        if (mTagArray != null) {
                            mTagArray = null;
                            mTagArray = new JSONArray();
                        }

                        if (mUserArray != null) {
                            mUserArray = null;
                            mUserArray = new JSONArray();
                        }
                    }

                    if (mTagState) {
                        if (s.toString().substring(start, s.toString().length()).contains("#")) {
                            mTagPos = start;
                            mWho = false;
                        } else if (s.toString().substring(start, s.toString().length()).contains("@")) {
                            mTagPos = start;
                            mWho = true;
                        } else {
                            try {
                                final String tag = s.toString().substring(mTagPos + 1, s.toString().length());

                                if (tag.length() != 0) {
                                    if (!mWho) {
                                        //Search #
                                        ApiManager.searchHashTag(mCtx, tag, 0, 100, new ApiManager.SearchHashTagCallback() {
                                            @Override
                                            public void onResult(boolean success, String message, ArrayList<TagModel> tagModel) {
                                                if (success) {
                                                    mTagModel.clear();
                                                    mTagModel.addAll(tagModel);

                                                    mTagAdapter = new TagAdapter(mTagPos + 1);
                                                    mTagList.setAdapter(mTagAdapter);
                                                    mTagList.setVisibility(View.VISIBLE);
                                                } else {
                                                    try {
//                    showToast(getString(R.string.search_failed));
                                                        Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                                    } catch (Exception x) {
                                                    }
                                                }
                                            }
                                        });
                                    } else {
                                        //Search @ 1
                                        ApiManager.getSearchUsers(mCtx, tag, 0, 100, 1, new ApiManager.GetSearchUsersCallback() {
                                            @Override
                                            public void onResult(boolean success, String message, ArrayList<UserModel> userModel) {
                                                if (success) {
                                                    mUserModel.clear();
                                                    mUserModel.addAll(userModel);

                                                    mUserAdapter = new UserAdapter(mTagPos + 1);
                                                    mUserList.setAdapter(mUserAdapter);
                                                    mUserList.setVisibility(View.VISIBLE);

                                                } else {
                                                    try {
//                    showToast(getString(R.string.search_failed));
                                                        Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                                    } catch (Exception x) {
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            } catch (Exception e) {

                            }
                        }
                    }
                } else {
                    s = s.toString().substring(taged, s.length());
                    if (s.toString().contains("#") || s.toString().contains("@")) mTagState = true;
                    else {
                        mTagList.setVisibility(View.GONE);
                        mUserList.setVisibility(View.GONE);
                        mTagState = false;
                    }

                    if (mTagState) {
                        try {
                            if (s.toString().substring(start - taged, s.toString().length()).contains("#")) {
                                mTagPos = start - taged;
                                mWho = false;
                            } else if (s.toString().substring(start - taged, s.toString().length()).contains("@")) {
                                mTagPos = start - taged;
                                mWho = true;
                            } else {
                                final String tag = s.toString().substring(mTagPos + 1, s.toString().length());

                                if (tag.length() != 0) {
                                    if (!mWho) {
                                        //Search #
                                        ApiManager.searchHashTag(mCtx, tag, 0, 100, new ApiManager.SearchHashTagCallback() {
                                            @Override
                                            public void onResult(boolean success, String message, ArrayList<TagModel> tagModel) {
                                                if (success) {
                                                    mTagModel.clear();
                                                    mTagModel.addAll(tagModel);

                                                    mTagAdapter = new TagAdapter(start);
                                                    mTagList.setAdapter(mTagAdapter);
                                                    mTagList.setVisibility(View.VISIBLE);
                                                } else {
                                                    try {
//                    showToast(getString(R.string.search_failed));
                                                        Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                                    } catch (Exception x) {
                                                    }
                                                }
                                            }
                                        });
                                    } else {
                                        //Search @ 1
                                        ApiManager.getSearchUsers(mCtx, tag, 0, 100, 1, new ApiManager.GetSearchUsersCallback() {
                                            @Override
                                            public void onResult(boolean success, String message, ArrayList<UserModel> userModel) {
                                                if (success) {
                                                    mUserModel.clear();
                                                    mUserModel.addAll(userModel);

                                                    mUserAdapter = new UserAdapter(start);
                                                    mUserList.setAdapter(mUserAdapter);
                                                    mUserList.setVisibility(View.VISIBLE);

                                                } else {
                                                    try {
//                    showToast(getString(R.string.search_failed));
                                                        Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                                    } catch (Exception x) {
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        } catch (Exception e) {

                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //event tracking
        startPostPage = Singleton.getCurrentTimestamp();
        try{
            LogEventUtil.EnterTextTypePage(mCtx,mApplication,"camera");
        }catch (Exception x)
        {

        }

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.post));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(1);

                //event tracking
                try{
                    LogEventUtil.LeaveTextTypePage(mCtx, mApplication, "camera", false, mEdit.getText().toString(), Singleton.getCurrentTimestamp() - startPostPage);
                    LogEventUtil.LeavePublishPageWithoutPub(mCtx, mApplication, Singleton.getCurrentTimestamp() - startPostPage, "camera");
                }catch (Exception x)
                {

                }

                mCtx.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.done));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ViewGroup view = (ViewGroup) getWindow().getDecorView();
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                if(!isPost)
                {
                    isPost = true;

                    mProgress.setVisibility(View.VISIBLE);

                    //event tracking
                    try{
                        LogEventUtil.LeaveTextTypePage(mCtx, mApplication, "camera", isPost, mEdit.getText().toString(), Singleton.getCurrentTimestamp() - startPostPage);
                        LogEventUtil.PublishPost(mCtx,mApplication);
                    }catch (Exception x)
                    {

                    }

                    String PostText = mEdit.getText().toString() + " ";
                    ArrayList<Integer> mPos = new ArrayList<Integer>();

                    for(int k = 0 ; k < mEdit.getText().toString().length() ; k++)
                    {
                        if(mEdit.getText().toString().substring(k,k+1).compareTo("@")==0)
                        {
                            if(k!=0)
                            {
                                if(mEdit.getText().toString().substring(k-1,k).compareTo(" ")!=0)
                                {
                                    PostText = PostText.substring(0,k + (mPos.size())) + " " + PostText.substring(k + (mPos.size()),PostText.length());
                                    mPos.add(k+ (mPos.size()));
                                }
                            }
                        }
                        else if(mEdit.getText().toString().substring(k,k+1).compareTo("#")==0)
                        {
                            if(k!=0)
                            {
                                if(mEdit.getText().toString().substring(k-1,k).compareTo(" ")!=0)
                                {
                                    PostText = PostText.substring(0,k + (mPos.size())) + " " + PostText.substring(k + (mPos.size()),PostText.length());
                                    mPos.add(k+ (mPos.size()));
                                }
                            }
                        }
                    }

                    ArrayList<Integer> mCheck = new ArrayList<Integer>();

                    for(int p = 0 ; p < PostText.length() ; p++)
                    {
                        if(PostText.substring(p,p+1).compareTo("@")==0)
                        {
                            mCheck.add(p);
                        }
                        else if(PostText.substring(p,p+1).compareTo("#")==0)
                        {
                            mCheck.add(p);
                        }
                    }

                    if(mCheck.size()!=0)
                    {
                        if (mUserArray != null)
                        {
                            mUserArray = null;
                            mUserArray = new JSONArray();
                        }

                        if (mTagArray != null)
                        {
                            mTagArray = null;
                            mTagArray = new JSONArray();
                        }

                        for(int t = 0 ; t < mCheck.size() ; t++)
                        {
                            for(int w = (mCheck.get(t)+1) ; w < PostText.length() ; w++)
                            {
                                if(PostText.substring(w,w+1).compareTo(" ")==0)
                                {
                                    if(PostText.substring(mCheck.get(t),mCheck.get(t)+1).compareTo("@")==0)
                                    {
                                        mUserArray.put(PostText.substring(mCheck.get(t)+1,w));
//                                        Log.d("123","user tag : " + PostText.substring(mCheck.get(t)+1,w));
                                    }
                                    else
                                    {
                                        mTagArray.put(PostText.substring(mCheck.get(t)+1,w));
//                                        Log.d("123","hash tag : " + PostText.substring(mCheck.get(t)+1,w));
                                    }

                                    break;
                                }
                            }
                        }
                    }


                    if (mApplication.getUpLoaderState() && mApplication.getUpLoader() == 100) {
                        if (mPostRetryThread != null) {
                            mPostRetryThread.destroyProcess();
                        }

                        mApplication.setShowLoader(false);
                        mPostRetryThread = new PostRetryThread(PostText);
                        mPostRetryThread.start();
                    } else {
                        setConfig(Constants.UPLOAD_CAPTION, PostText);
                        setConfig(Constants.UPLOAD_USER, mUserArray.toString());
                        setConfig(Constants.UPLOAD_TAG, mTagArray.toString());
                        setConfig(Constants.UPLOAD_PHOTO, mName);
                        setConfig(Constants.UPLOAD_VIDEO, "");
                        setConfig(Constants.UPLOAD_TYPE, "image");

                        mApplication.setmPostType("image");

                        if (mFilter) setResult(3);
                        else {
                            mApplication.setShowLoader(true);
                            mApplication.setReLoad(true);
                        }
                        mCtx.finish();
                    }
                }
            }
        });

    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    private class UserAdapter extends BaseAdapter
    {
        private int mPos;

        public UserAdapter(int pos)
        {
            mPos = pos;
        }

        @Override
        public int getCount()
        {
            return mUserModel.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderUser holder = new ViewHolderUser();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.tag_user_row, null);
                holder.user_layout = (LinearLayout) convertView.findViewById(R.id.user_layout);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.bio = (TextView) convertView.findViewById(R.id.bio);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderUser) convertView.getTag();

            holder.name.setText(mUserModel.get(position).getOpenID());
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUserModel.get(position).getPicture()), holder.img, SelfOptions);
            holder.user_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    taged = mPos + mUserModel.get(position).getOpenID().length() + 1;
                    tagedState = true;
                    mUserArray.put(mUserModel.get(position).getOpenID());
                    String s = mEdit.getText().toString().substring(0,mPos);
                    mEdit.setText(s + mUserModel.get(position).getOpenID() + " ");
                    mEdit.setSelection(mEdit.getText().toString().length());
                    mTagList.setVisibility(View.GONE);
                    mTagState = false;
                }
            });

            if(mUserModel.get(position).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.bio.setVisibility(View.VISIBLE);
            holder.bio.setText(mUserModel.get(position).getName());

            return convertView;
        }
    };

    private class ViewHolderUser
    {
        LinearLayout user_layout;
        ImageView img;
        TextView name;
        ImageView verifie;
        TextView bio;
    }

    private class TagAdapter extends BaseAdapter
    {
        private int mPos;

        public TagAdapter(int pos)
        {
            mPos = pos;
        }

        @Override
        public int getCount()
        {
            return mTagModel.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderTag holder = new ViewHolderTag();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.tag_user_row, null);
                holder.user_layout= (LinearLayout) convertView.findViewById(R.id.user_layout);
                holder.img_layout = (RelativeLayout) convertView.findViewById(R.id.img_layout);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.count = (TextView) convertView.findViewById(R.id.count);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderTag) convertView.getTag();

            holder.img_layout.setVisibility(View.GONE);
            holder.name.setText("#" + mTagModel.get(position).getHashTag());
            holder.user_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    taged = mPos + mTagModel.get(position).getHashTag().length() + 1;
                    tagedState = true;
                    mTagArray.put(mTagModel.get(position).getHashTag());
                    String s = "";
                    try{
                        if(mPos < mEdit.getText().toString().length()){
                            s = mEdit.getText().toString().substring(0,mPos);
                        }
                    }
                    catch (Exception e){
                    }
                    mEdit.setText(s + mTagModel.get(position).getHashTag() + " ");
                    mEdit.setSelection(mEdit.getText().toString().length());
                    mTagList.setVisibility(View.GONE);
                    mTagState = false;
                }
            });

            holder.count.setVisibility(View.VISIBLE);
            holder.count.setText(String.valueOf(mTagModel.get(position).getPostCount()));

            return convertView;
        }
    };

    private void executePostFinish(){
        mProgress.setVisibility(View.GONE);
        if (mFilter) {
            setResult(2);
        } else {
            mApplication.setReLoad(true);
        }
        mCtx.finish();
    }

    private class ViewHolderTag
    {
        LinearLayout user_layout;
        RelativeLayout img_layout;
        TextView name;
        TextView count;
    }


    /**
     * @author Zack
     * @since 2016.02.19
     * To retry 4 times during 30 secs
     */
    private class PostRetryThread extends Thread {

        private int retryMaxTimes = 4;
        private int retryMaxSec = 30 * 1000; //30secs
        private int sleepTime = 2 * 1000; //2secs

        private int retry = 1;
        private int nowRetry = 0;

        private String mPostText = "";

        private boolean isRunning = true;
        private boolean isSuccess;

        public PostRetryThread(String mPostText) {
            this.mPostText = mPostText;
        }

        @Override
        public void run() {
            try {
                long nowTime = System.currentTimeMillis();

                while (isRunning && retry <= retryMaxTimes && (System.currentTimeMillis() - nowTime) < retryMaxSec) {
                    if (nowRetry == retry) {
                        sleep(1000);
                    } else {
                        nowRetry = retry;
                        ApiManager.publishPost(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mPostText, mUserArray.toString(), mTagArray.toString(), mName, "", "image", new ApiManager.PublishPostCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {
                                    isSuccess = success;
                                    isRunning = false;
                                } else {
                                    retry++;
                                }
                            }
                        });
                        sleep(sleepTime);
                    }
                }

                //Log.d("PostRetryThread", "nowRetry="+nowRetry+", isSuccess="+isSuccess);

                mCtx.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            if (!isSuccess) {
                                mProgress.setVisibility(View.GONE);
                                Toast.makeText(mCtx, getString(R.string.post_failed), Toast.LENGTH_SHORT).show();
                            }else{
                                executePostFinish();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                isPost = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void destroyProcess() {
            isRunning = false;
        }
    }


}
