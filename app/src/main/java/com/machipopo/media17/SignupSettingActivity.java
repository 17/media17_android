package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.View.CircleImageView;
import com.machipopo.media17.model.UserModel;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

/**
 * Created by POPO on 5/21/15.
 */
public class SignupSettingActivity extends BaseActivity
{
    private SignupSettingActivity mCtx = this;
    private EditText mName;
    private TextView mBirthday;
    private ImageView mMicon, mFicon;
    private static CircleImageView mPic;

    private String sex = "male";
    private String mAccount = "", mPassword = "";
    private String name = "", birthday = "", pic = "";
    private ArrayList<String> mAge = new ArrayList<String>();

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_setting_activity);

        initTitleBar();

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("account")) mAccount = mBundle.getString("account");
            if(mBundle.containsKey("password")) mPassword = mBundle.getString("password");
        }

        mPic = (CircleImageView) findViewById(R.id.pic);
        mMicon = (ImageView) findViewById(R.id.m_icon);
        mFicon = (ImageView) findViewById(R.id.f_icon);
        mName = (EditText) findViewById(R.id.name);
        mBirthday = (TextView) findViewById(R.id.birthday);

        mMicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sex = "male";
                mMicon.setImageResource(R.drawable.gender_m_active);
                mFicon.setImageResource(R.drawable.gender_f);
            }
        });

        mFicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sex = "female";
                mMicon.setImageResource(R.drawable.gender_m);
                mFicon.setImageResource(R.drawable.gender_f_active);
            }
        });

        mPic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback() {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, String imageFileName) {
                        if (uploadComplete) {
                            pic = imageFileName;
                        } else {
                            showNetworkUnstableToast();
                        }
                        mPic.setImageBitmap(BitmapHelper.getBitmap(Singleton.getExternalMediaFolderPath() + pic, 1));
                    }
                });
            }
        });

        mBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSingleItemPicker(getString(R.string.sign_birthday), mAge, 0, new SingleItemPickerCallback() {
                    @Override
                    public void onResult(boolean done, int selectedItem) {
                        mBirthday.setText(mAge.get(selectedItem));
                    }
                });
            }
        });

        mAge.clear();
        for(int i = 15 ; i < 71 ; i++)
        {
            mAge.add(String.valueOf(i));
        }

        showKeyboard();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.singup));

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.nav_arrow_white_back);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,SignupActivity.class);
                startActivity(intent);
                mCtx.finish();
            }
        });

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.next));
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();

                if (mName.getText().toString().length() != 0)
                {
                    name = mName.getText().toString();
//                    birthday = mBirthday.getText().toString();
//                    pic = "1e88689a-1070-4ad1-a90e-40cee1cbe5ab.jpg";

                    showProgressDialog();
                    ApiManager.registerAction(mCtx, mAccount, mPassword, name, "0", sex, pic, new ApiManager.RegisterActionCallback() {
                        @Override
                        public void onResult(boolean success, String message, UserModel user) {
                            hideProgressDialog();
                            if (success) {
                                hideKeyboard();
                                setConfig(Constants.ACCOUNT, mAccount);
                                setConfig(Constants.PASSWORD, mPassword);

                                Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                mStory17Application.setUser(user);

                                Intent intent = new Intent();
                                intent.setClass(mCtx, PhoneNumberActivity.class);
                                startActivity(intent);
                                mCtx.finish();
                            } else {
                                try{
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                    });
                } else {
                    try{
//                          showToast(getString(R.string.entet_niname));
                        Toast.makeText(mCtx, getString(R.string.entet_niname), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    public void setConfig(String key, String value)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putString(key, value);
        PE.commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intent = new Intent();
            intent.setClass(mCtx, SignupActivity.class);
            startActivity(intent);
            mCtx.finish();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
