package com.machipopo.media17;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.PaymentModel;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

/**
 * Created by POPO on 6/22/15.
 */
public class RevenueMoneyActivity extends BaseNewActivity
{
    private RevenueMoneyActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;

    private ListView mList;
    private ProgressBar mProgress;
    private ImageView mNoData;

    private ArrayList<PaymentModel> mPaymentModels = new ArrayList<PaymentModel>();

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_money_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        mList = (ListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mProgress.setVisibility(View.VISIBLE);
        ApiManager.getPayments(mCtx, 0, 100, new ApiManager.GetPaymentsCallback()
        {
            @Override
            public void onResult(boolean success, ArrayList<PaymentModel> paymentModels)
            {
                mProgress.setVisibility(View.GONE);
                if(success && paymentModels!=null)
                {
                    if(paymentModels.size()!=0)
                    {
                        mNoData.setVisibility(View.GONE);
                        mPaymentModels.clear();
                        mPaymentModels.addAll(paymentModels);
                        mList.setAdapter(new MyAdapter());
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    try{
//                                             showToast(getString(R.string.failed));
                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                    mNoData.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_post_money));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class MyAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mPaymentModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.revenue_payment_row, null);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.money = (TextView) convertView.findViewById(R.id.money);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.account = (TextView) convertView.findViewById(R.id.account);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.day.setText(mPaymentModels.get(position).getYear() + " / " + mPaymentModels.get(position).getMonth());
            holder.money.setText("$ " + mPaymentModels.get(position).getAmount() + " USD");
            holder.name.setText(mPaymentModels.get(position).getBankName());
            holder.account.setText(mPaymentModels.get(position).getAccountNumber());

            return convertView;
        }
    }

    private class ViewHolder
    {
        TextView day;
        TextView money;
        TextView name;
        TextView account;
    }
}
