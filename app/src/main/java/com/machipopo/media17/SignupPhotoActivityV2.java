package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.machipopo.media17.View.CircleImageView;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by POPO on 5/21/15.
 */
public class SignupPhotoActivityV2 extends BaseActivity
{
    private SharedPreferences sharedPreferences;
    private SignupPhotoActivityV2 mCtx = this;
    private static CircleImageView mPic;
    private String pic = "";
    private Button btnNext;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_setting_activity_v2);

        initTitleBar();


        mPic = (CircleImageView) findViewById(R.id.pic);
        btnNext = (Button)findViewById(R.id.btn_next);

        mPic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showImagePickerOptionDialog(true, true, true, new ImagePickerDialogCallback()
                {
                    @Override
                    public void onResult(boolean imageOk, boolean uploadComplete, String imageFileName)
                    {
                        if (uploadComplete)
                        {
                            pic = imageFileName;
//                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
//                            sharedPreferences.edit().putString(Constants.PICTURE, pic).commit();
                            Singleton.preferenceEditor.putString(Constants.PICTURE, pic).commit();

                            mPic.setImageBitmap(BitmapHelper.getBitmap(Singleton.getExternalMediaFolderPath() + pic, 1));

                            try
                            {
                                JSONObject params = new JSONObject();
                                params.put("picture", pic);

                                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            mPic.setImageBitmap(BitmapHelper.getBitmap(Singleton.getExternalMediaFolderPath() + pic, 1));
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {

                            }
                        }
                        else
                        {
                            showNetworkUnstableToast();
                        }
                    }
                });
            }
        });

        if(Singleton.preferences.getString(Constants.PICTURE, "").length()!=0)
        {
            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
//            boolean fbLogin = sharedPreferences.getBoolean("fblogin", false);
//            if(fbLogin)
                mPic.setImageBitmap(BitmapHelper.getBitmap(Singleton.getExternalMediaFolderPath() + Singleton.preferences.getString(Constants.PICTURE, ""), 1));
        }
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNext(false);
            }
        });

        hideKeyboard();
    }

    private void initTitleBar()
    {
        ((TextView) findViewById(R.id.title_name)).setText(getString(R.string.signup_title_picture));

        LinearLayout line = (LinearLayout)findViewById(R.id.under_line);
        line.setVisibility(View.VISIBLE);

        Button btn = (Button) findViewById(R.id.btn_right);
        btn.setText(getString(R.string.skip));
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               goNext(true);
            }
        });
    }

    private void goNext(boolean skip){

        try
        {
            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);

            JSONObject params = new JSONObject();
            if(Singleton.preferences.getString(Constants.PICTURE, "").length()!=0) params.put("picture", Singleton.preferences.getString(Constants.PICTURE, ""));
            if(sharedPreferences.getString(Constants.FULL_NAME, "").length()!=0) params.put("name", sharedPreferences.getString(Constants.FULL_NAME, ""));

            if(params.length()!=0)
            {
                ApiManager.updateUserInfo(mCtx, params, new ApiManager.UpdateUserInfoCallback()
                {
                    @Override
                    public void onResult(boolean success, String message)
                    {
                        if (success)
                        {

                        }
                    }
                });
            }
        }
        catch (JSONException e)
        {

        }

        if(skip){
            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
            sharedPreferences.edit().putString(Constants.COVER_PHOTO, "");

            Intent intent = new Intent();
            intent.setClass(mCtx, PhoneSearchActivity.class);
            startActivity(intent);
            resetSharedPreference();
            mCtx.finish();
        }
        else{
//            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
//            String img = sharedPreferences.getString(Constants.PICTURE, "");
            String img = Singleton.preferences.getString(Constants.PICTURE, "");
            if(!img.trim().equals("")){

                Intent intent = new Intent();
                intent.setClass(mCtx, PhoneSearchActivity.class);
                startActivity(intent);
                resetSharedPreference();
                mCtx.finish();
            }
        }
    }

    private void resetSharedPreference(){
        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
        sharedPreferences.edit().putString(Constants.NAME, "").commit();
        sharedPreferences.edit().putString(Constants.FULL_NAME, "").commit();
        sharedPreferences.edit().putString(Constants.PASSWORD, "").commit();
        sharedPreferences.edit().putString(Constants.EMAIL, "").commit();
        sharedPreferences.edit().putString(Constants.PICTURE, "").commit();
        sharedPreferences.edit().putString(Constants.FACEBOOK_ID, "").commit();
        sharedPreferences.edit().putBoolean("fblogin", false).commit();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
//            Intent intent = new Intent();
//            intent.setClass(mCtx, SignupActivity.class);
//            startActivity(intent);
//            mCtx.finish();

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }
}
