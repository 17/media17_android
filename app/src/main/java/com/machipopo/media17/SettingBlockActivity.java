package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by POPO on 2015/11/9.
 */
public class SettingBlockActivity extends BaseNewActivity
{
    private SettingBlockActivity mCtx = this;
    private LayoutInflater inflater;

    private PullToRefreshListView mList;
    private ProgressBar mProgress;
    private ImageView mNoData;

    private ArrayList<UserModel> mModels = new ArrayList<UserModel>();
    private MyAdapter mMyAdapter;
    private DisplayImageOptions SelfOptions;
    private Story17Application mApplication;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_liker_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        mList = (PullToRefreshListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mProgress.setVisibility(View.VISIBLE);

        ApiManager.getBlockUserList(mCtx, Integer.MAX_VALUE, 200, new ApiManager.GetFriendSuggestionCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<UserModel> models)
            {
                mProgress.setVisibility(View.GONE);

                if (success && models != null)
                {
                    if (models.size() != 0)
                    {
                        mNoData.setVisibility(View.GONE);
                        mModels.clear();
                        mModels.addAll(models);

                        mMyAdapter = new MyAdapter();
                        mList.setAdapter(mMyAdapter);
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    mNoData.setVisibility(View.VISIBLE);
                }
            }
        });

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.block_setting));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class MyAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderGrid holder = new ViewHolderGrid();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.setting_block_row, null);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.follow = (Button) convertView.findViewById(R.id.follow);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderGrid) convertView.getTag();

            final int pos = position;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getPicture()), holder.pic, SelfOptions);
            holder.pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mModels.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mModels.get(pos).getName());
                        intent.putExtra("picture", mModels.get(pos).getPicture());
                        intent.putExtra("isfollowing", mModels.get(pos).getIsFollowing());
                        intent.putExtra("post", mModels.get(pos).getPostCount());
                        intent.putExtra("follow", mModels.get(pos).getFollowerCount());
                        intent.putExtra("following", mModels.get(pos).getFollowingCount());
                        intent.putExtra("open", mModels.get(pos).getOpenID());
                        intent.putExtra("bio", mModels.get(pos).getBio());
                        intent.putExtra("targetUserID", mModels.get(pos).getUserID());
                        intent.putExtra("web", mModels.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setText(mModels.get(position).getOpenID());
            holder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mModels.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mModels.get(pos).getName());
                        intent.putExtra("picture", mModels.get(pos).getPicture());
                        intent.putExtra("isfollowing", mModels.get(pos).getIsFollowing());
                        intent.putExtra("post", mModels.get(pos).getPostCount());
                        intent.putExtra("follow", mModels.get(pos).getFollowerCount());
                        intent.putExtra("following", mModels.get(pos).getFollowingCount());
                        intent.putExtra("open", mModels.get(pos).getOpenID());
                        intent.putExtra("bio", mModels.get(pos).getBio());
                        intent.putExtra("targetUserID", mModels.get(pos).getUserID());
                        intent.putExtra("web", mModels.get(pos).getWebsite());
                        startActivity(intent);
                    }
                }
            });

            if(mModels.get(pos).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.dio.setText(mModels.get(position).getName());
            holder.follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mProgress.setVisibility(View.VISIBLE);

                    mApplication = (Story17Application) getApplication();

                    try {
                        LogEventUtil.UnblockUser(mCtx, mApplication, mModels.get(pos).getUserID());
                    }
                    catch (Exception x)
                    {

                    }

                    ApiManager.unblockUserAction(mCtx, mModels.get(pos).getUserID(), new ApiManager.BlockUserActionCallback() {
                        @Override
                        public void onResult(boolean success, String message) {

                            mProgress.setVisibility(View.GONE);
                            if (success) {
                                mModels.remove(pos);
                                mMyAdapter.notifyDataSetChanged();
                                try {
//                          showToast(getString(R.string.done));
                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                                if (mModels.size() == 0) mNoData.setVisibility(View.VISIBLE);
                            } else {
                                try {
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        }
                    });
                }
            });

            return convertView;
        }
    }

    private class ViewHolderGrid
    {
        ImageView pic;
        TextView name;
        TextView dio;
        Button follow;
        ImageView verifie;
    }
}
