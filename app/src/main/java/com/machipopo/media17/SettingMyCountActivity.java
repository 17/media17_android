package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.iapppay.interfaces.callback.IPayResultCallback;
import com.iapppay.pay.mobile.iapppaysecservice.utils.IAppPaySDKConfig;
import com.iapppay.sdk.main.IAppPay;
import com.iapppay.sdk.main.IAppPayOrderUtils;
import com.machipopo.media17.model.PointGainModel;
import com.machipopo.media17.model.PointUsageModel;
import com.machipopo.media17.model.ProductModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.util.IabHelper;
import com.machipopo.media17.util.IabResult;
import com.machipopo.media17.util.Inventory;
import com.machipopo.media17.util.Purchase;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by POPO on 2015/11/18.
 */
public class SettingMyCountActivity extends BaseNewActivity
{
    private SettingMyCountActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;
    private ViewPager mViewPager;
    private TextView mTab1,mTab2;

    private View mViewBuy, mViewHistory;
    private List<View> mViewList;

    private DisplayImageOptions SelfOptions;
    private PagerBaseAdapter mPagerAdapter;

    private TextView mMyCount;
    private Button mBuy;
    private LinearLayout mFreeLayout;
    private Dialog mBuyDialog;

    private SimpleDateFormat mDateFormat;

    private PullToRefreshListView mGainListView;
    private GainListAdapter mGainListAdapter;
    private ProgressBar mGainProgress;
    private ImageView mGainNoData;
    private Boolean isFetchingDataGain = false;
    private Boolean noMoreDataGain = false;
    public ArrayList<PointGainModel> mGainModels = new ArrayList<PointGainModel>();

    private PullToRefreshListView mUsageListView;
    private UsageListAdapter mUsageListAdapter;
    private ProgressBar mUsageProgress;
    private ImageView mUsageNoData;
    private Boolean isFetchingDataUsage = false;
    private Boolean noMoreDataUsage = false;
    private ArrayList<PointUsageModel> mUsageModel = new ArrayList<PointUsageModel>();

    private int loves[] = {R.drawable.giftpoint_1,R.drawable.giftpoint_2,R.drawable.giftpoint_3,R.drawable.giftpoint_4,R.drawable.giftpoint_5,R.drawable.giftpoint_6,R.drawable.giftpoint_7};
    private int love_pos = 0;
    IabHelper mHelper;
    private int mBuyPos = 0;

    private ImageView mMyCountImg;
    private Dialog mGiftStateDialog;
    private int buyPointCount =0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_my_count_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        LayoutInflater lf = mCtx.getLayoutInflater().from(mCtx);
        mViewBuy = lf.inflate(R.layout.notifi_follow, null);
        mViewHistory = lf.inflate(R.layout.suage_layout, null);

        mViewList = new ArrayList<View>();
        mViewList.add(mViewBuy);
        mViewList.add(mViewHistory);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mPagerAdapter = new PagerBaseAdapter();
        mViewPager.setAdapter(mPagerAdapter);

        SmartTabLayout mPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        mPagerTab.setViewPager(mViewPager);
        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mTab1.setTextColor(getResources().getColor(R.color.main_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
                    case 1:
                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.main_color));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //event tracking for how many times user bought giftpoint
        buyPointCount = Singleton.preferences.getInt(Constants.Buy_Point_Count,0);

        mTab1 = (TextView) findViewById(R.id.tab1);
        mTab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(0);
            }
        });
        mTab2 = (TextView) findViewById(R.id.tab2);
        mTab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(1);
            }
        });

        mMyCount = (TextView) findViewById(R.id.count);
        mBuy = (Button) findViewById(R.id.buy);
        mFreeLayout = (LinearLayout) findViewById(R.id.free_layout);
        mMyCountImg = (ImageView) findViewById(R.id.my_count_img);
        mMyCountImg.setImageResource(loves[(int) (Math.random() * loves.length)]);

        mBuy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(Singleton.preferences.getInt(Constants.GIFT_MODULE_STATE, 1)==0)
                {
                    if(mGiftStateDialog!=null) mGiftStateDialog = null;
                    mGiftStateDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                    mGiftStateDialog.setContentView(R.layout.live_end_dailog);
                    Window window = mGiftStateDialog.getWindow();
                    window.setGravity(Gravity.BOTTOM);
                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                    TextView mTitle = (TextView) mGiftStateDialog.findViewById(R.id.title);
                    mTitle.setText(getString(R.string.gift_module_state));

                    Button mEnd = (Button)mGiftStateDialog.findViewById(R.id.end);
                    mEnd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mGiftStateDialog.dismiss();
                        }
                    });
                    mGiftStateDialog.show();

                    return;
                }
                else if(Singleton.preferences.getInt(Constants.GIFT_MODULE_STATE, 1)==2)
                {
                    if(mGiftStateDialog!=null) mGiftStateDialog = null;
                    mGiftStateDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                    mGiftStateDialog.setContentView(R.layout.live_end_dailog);
                    Window window = mGiftStateDialog.getWindow();
                    window.setGravity(Gravity.BOTTOM);
                    window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                    TextView mTitle = (TextView) mGiftStateDialog.findViewById(R.id.title);
                    mTitle.setText(getString(R.string.gift_module_update));

                    Button mEnd = (Button)mGiftStateDialog.findViewById(R.id.end);
                    mEnd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mGiftStateDialog.dismiss();

                            try
                            {
                                Uri uri = Uri.parse(Singleton.preferences.getString(Constants.APP_UPDATE_LINK, Constants.MEDIA17_WEBSITE));
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    });
                    mGiftStateDialog.show();

                    return;
                }

                if(mApplication.getProductModels().size()!=0)
                {
                    mBuyDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                    mBuyDialog.setContentView(R.layout.buy_dialog);

                    TextView mCount = (TextView) mBuyDialog.findViewById(R.id.my_count);
                    mCount.setText(String.format(getString(R.string.buy_my_p_money), Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));

                    ((LinearLayout) mBuyDialog.findViewById(R.id.free)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            Log.d("123", "free");
                        }
                    });

                    LinearLayout mRow = (LinearLayout) mBuyDialog.findViewById(R.id.row);

                    for(int i = 0 ; i < mApplication.getProductModels().size() ; i ++)
                    {
                        View mView = inflater.inflate(R.layout.buy_dialog_row, null);
                        LinearLayout mLayout = (LinearLayout) mView.findViewById(R.id.buy_layout);
                        ImageView mImg = (ImageView) mView.findViewById(R.id.img);
                        TextView mPoint = (TextView) mView.findViewById(R.id.point);
                        TextView mFreePoint = (TextView) mView.findViewById(R.id.free_point);
                        final TextView mMoney = (TextView) mView.findViewById(R.id.money);

                        final int pos = i;
                        mLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mBuyDialog.dismiss();
                                mBuyPos = pos;

                                if(Constants.INTERNATIONAL_VERSION)
                                {
                                try
                                {
                                    if(mHelper!=null) mHelper.launchPurchaseFlow(mCtx, mApplication.getProductModels().get(pos).getProductID(), mApplication.getProductModels().get(pos).getPoint(), mPurchaseFinishedListener, mApplication.getProductModels().get(pos).getProductID());
                                }
                                catch (Exception e){
                                }
                                }
                                else
                                {
                                    Toast.makeText(mCtx,getString(R.string.processing),Toast.LENGTH_SHORT).show();
                                    ApiManager.createOrder(mCtx, Singleton.preferences.getString(Constants.OPEN_ID, ""), mApplication.getProductModels().get(pos).getProductID(), new ApiManager.checkFacebookIDAvailableCallback() {
                                        @Override
                                        public void onResult(boolean success, String transid) {
                                            if(success && transid.length()!=0)
                                            {
                                                String pay = "transid=" + transid + "&appid=" + IAppPaySDKConfig.APP_ID;
                                                IAppPay.startPay(mCtx, pay, iPayResultCallback);

                                                //event tracking
                                                buyPointCount++;
                                                try{
                                                    LogEventUtil.PurchasePoint(mCtx,mApplication,mApplication.getProductModels().get(pos).getPoint(),buyPointCount);
                                                }catch (Exception x)
                                                {

                                                }

                                            }
                                            else Toast.makeText(mCtx,getString(R.string.error_failed),Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        });

                        if(love_pos == loves.length) love_pos = 0;
                        mImg.setImageResource(loves[love_pos]);
                        love_pos++;

                        if(Constants.INTERNATIONAL_VERSION)
                        {
                            if(mApplication.getProductModels().get(pos).getGoogle_point().length()!=0) mPoint.setText(mApplication.getProductModels().get(i).getGoogle_point());
                            else mPoint.setText(mApplication.getProductModels().get(i).getPoint() + "P");

                            mMoney.setText(mApplication.getProductModels().get(i).getGoogle_price());
                        }
                        else
                        {
                            mPoint.setText(mApplication.getProductModels().get(i).getPoint() + "P");

                            mMoney.setText("¥ " + String.valueOf(mApplication.getProductModels().get(i).getPrice()) + " ");
                        }

                        if(mApplication.getProductModels().get(i).getBonusPercentage()!=0)
                        {
                            mFreePoint.setVisibility(View.VISIBLE);
                            mFreePoint.setText("(" + (mApplication.getProductModels().get(i).getPoint() - mApplication.getProductModels().get(i).getBonusAmount()) + "+" + mApplication.getProductModels().get(i).getBonusPercentage() + "%)");
                        }
                        else mFreePoint.setVisibility(View.GONE);

                        mRow.addView(mView);
                    }

                    mBuyDialog.show();
                }
                else {
                    try{
//                          showToast(getString(R.string.empty));
                        Toast.makeText(mCtx, getString(R.string.empty), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });

        mFreeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        mMyCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_c)
        .showImageForEmptyUri(R.drawable.placehold_c)
        .showImageOnFail(R.drawable.placehold_c)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        if(Constants.INTERNATIONAL_VERSION)
        {
        try
        {
            mHelper = new IabHelper(mCtx, Constants.GiftPublicKey);
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener()
            {
                public void onIabSetupFinished(IabResult result)
                {
                    if(result.isSuccess())
                    {
                        if(mApplication.getProductModels().size()!=0)
                        {
                            ArrayList<String> skuList = new ArrayList<String>();
                            for(int i = 0 ; i < mApplication.getProductModels().size() ; i++)
                            {
                                skuList.add(mApplication.getProductModels().get(i).getProductID());
                            }

                            mHelper.queryInventoryAsync(true, skuList, new IabHelper.QueryInventoryFinishedListener()
                            {
                                public void onQueryInventoryFinished(IabResult result, Inventory inventory)
                                {
                                    if (!result.isFailure())
                                    {
                                        for(int i = 0 ; i < mApplication.getProductModels().size() ; i++)
                                        {
//                                        Log.d("123","data : " + inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).toString());
                                            mApplication.getProductModels().get(i).setGoogle_price(inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).getPrice());
                                            mApplication.getProductModels().get(i).setGoogle_point(inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).getDescription());
                                        }
                                    }
                                }
                            });
                        }
                        else
                        {
                            ApiManager.getPointProductList(mCtx, new ApiManager.ProductListCallback()
                            {
                                @Override
                                public void onResult(boolean success, ArrayList<ProductModel> productModels)
                                {
                                    if(success && productModels!=null)
                                    {
                                        if(productModels.size()!=0)
                                        {
                                            mApplication.setProductModels(productModels);

                                            ArrayList<String> skuList = new ArrayList<String>();
                                            for(int i = 0 ; i < productModels.size() ; i++)
                                            {
                                                skuList.add(productModels.get(i).getProductID());
                                            }

                                            mHelper.queryInventoryAsync(true, skuList, new IabHelper.QueryInventoryFinishedListener()
                                            {
                                                public void onQueryInventoryFinished(IabResult result, Inventory inventory)
                                                {
                                                    if (!result.isFailure())
                                                    {
                                                        for(int i = 0 ; i < mApplication.getProductModels().size() ; i++)
                                                        {
                                                            mApplication.getProductModels().get(i).setGoogle_price(inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).getPrice());
                                                            mApplication.getProductModels().get(i).setGoogle_point(inventory.getSkuDetails(mApplication.getProductModels().get(i).getProductID()).getDescription());
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }
        catch (Exception e)
        {

        }
        }
        else
        {
            IAppPay.init(mCtx, IAppPay.PORTRAIT, IAppPaySDKConfig.APP_ID);
        }
    }

    private IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener()
    {
        public void onIabPurchaseFinished(IabResult result,Purchase purchase)
        {
//            Log.d("123", "onIabPurchaseFinished result.isFailure() : " + result.isFailure());
//            Log.d("123", "mApplication.getProductModels().get(mBuyPos).getProductID() : " + mApplication.getProductModels().get(mBuyPos).getProductID());
            if (result.isFailure())
            {
                try{
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
                return;
            }
            else if (purchase.getSku().equals(mApplication.getProductModels().get(mBuyPos).getProductID()))
            {
//                Log.d("123", "111 OK");
//                Log.d("123", "onQueryInventoryFinished OK");

                //event tracking
                buyPointCount++;
                try{
                    LogEventUtil.PurchasePoint(mCtx,mApplication,mApplication.getProductModels().get(mBuyPos).getPoint(),buyPointCount);
                }catch (Exception x)
                {

                }

                mHelper.queryInventoryAsync(mReceivedInventoryListener);
            }
        }
    };

    private IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener()
    {
        public void onQueryInventoryFinished(IabResult result,Inventory inventory)
        {
//            Log.d("123", "onQueryInventoryFinished result.isFailure() : " + result.isFailure());
            if (result.isFailure())
            {
                try{
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
//                Log.d("123", "222 error");
            }
            else
            {
//                Log.d("123", "onQueryInventoryFinished OK");
                mHelper.consumeAsync(inventory.getPurchase(mApplication.getProductModels().get(mBuyPos).getProductID()),mConsumeFinishedListener);
            }
        }
    };

    private IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener()
    {
        public void onConsumeFinished(Purchase purchase,IabResult result)
        {
//            Log.d("123","onConsumeFinished result.isSuccess() : " + result.isSuccess());
            if (result.isSuccess())
            {
//                Log.d("123","purchase.getSku() : " + purchase.getSku());
//                Log.d("123","purchase.getOrderId() : " + purchase.getOrderId());
//                Log.d("123","purchase.getToken() : " + purchase.getToken());
//                Log.d("123","purchase.getDeveloperPayload() : " + purchase.getDeveloperPayload());

                final String Sku = purchase.getSku();
                final String OrderId = purchase.getOrderId();
                final String Token = purchase.getToken();
                final String Payload = purchase.getDeveloperPayload();

                ApiManager.purchaseProduct(mCtx, purchase.getSku(), purchase.getOrderId(), purchase.getToken(), purchase.getDeveloperPayload(), new ApiManager.PurchaseProductCallback()
                {
                    @Override
                    public void onResult(boolean success, int point)
                    {
//                        Log.d("123","success : " + success);
//                        Log.d("123","point : " + point);
                        if(success)
                        {
                            try{
//                          showToast(getString(R.string.done));
                                Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                            int now = Singleton.preferences.getInt(Constants.GIFT_POINT, 0) + point;
                            Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, now).commit();
                            mMyCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));

                            mGainProgress.setVisibility(View.VISIBLE);
                            ApiManager.getPointGainLog(mCtx, Integer.MAX_VALUE, 15, new ApiManager.GetPointGainLogCallback() {
                                @Override
                                public void onResult(boolean success, ArrayList<PointGainModel> pointGainModels) {
                                    mGainProgress.setVisibility(View.GONE);
                                    if (success && pointGainModels != null) {
                                        if (pointGainModels.size() != 0) {
                                            mGainNoData.setVisibility(View.GONE);
                                            mGainModels.clear();
                                            mGainModels.addAll(pointGainModels);

                                            if (mGainListAdapter != null) mGainListAdapter = null;
                                            mGainListAdapter = new GainListAdapter();
                                            mGainListView.setAdapter(mGainListAdapter);

                                            isFetchingDataGain = false;
                                            noMoreDataGain = false;

                                            if (pointGainModels.size() < 15) {
                                                noMoreDataGain = true;
                                            }
                                            mPagerAdapter.notifyDataSetChanged();

                                        } else {
                                            mGainNoData.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        mGainNoData.setVisibility(View.VISIBLE);
                                        try{
//                          showToast(getString(R.string.failed));
                                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                    }
                                }
                            });
                        }
                        else
                        {
                            ApiManager.purchaseProduct(mCtx, Sku, OrderId, Token, Payload, new ApiManager.PurchaseProductCallback()
                            {
                                @Override
                                public void onResult(boolean success, int point)
                                {
//                        Log.d("123","success : " + success);
//                        Log.d("123","point : " + point);
                                    if(success)
                                    {
                                        try{
//                          showToast(getString(R.string.done));
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                        int now = Singleton.preferences.getInt(Constants.GIFT_POINT, 0) + point;
                                        Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, now).commit();
                                        mMyCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));

                                        mGainProgress.setVisibility(View.VISIBLE);
                                        ApiManager.getPointGainLog(mCtx, Integer.MAX_VALUE, 15, new ApiManager.GetPointGainLogCallback() {
                                            @Override
                                            public void onResult(boolean success, ArrayList<PointGainModel> pointGainModels) {
                                                mGainProgress.setVisibility(View.GONE);
                                                if (success && pointGainModels != null) {
                                                    if (pointGainModels.size() != 0) {
                                                        mGainNoData.setVisibility(View.GONE);
                                                        mGainModels.clear();
                                                        mGainModels.addAll(pointGainModels);

                                                        if (mGainListAdapter != null) mGainListAdapter = null;
                                                        mGainListAdapter = new GainListAdapter();
                                                        mGainListView.setAdapter(mGainListAdapter);

                                                        isFetchingDataGain = false;
                                                        noMoreDataGain = false;

                                                        if (pointGainModels.size() < 15) {
                                                            noMoreDataGain = true;
                                                        }
                                                        mPagerAdapter.notifyDataSetChanged();

                                                    } else {
                                                        mGainNoData.setVisibility(View.VISIBLE);
                                                    }
                                                } else {
                                                    mGainNoData.setVisibility(View.VISIBLE);
                                                    try{
//                          showToast(getString(R.string.failed));
                                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        payPointError();

                                        Singleton.preferenceEditor.putString(Constants.IAP_USER, Singleton.preferences.getString(Constants.USER_ID, "")).commit();
                                        Singleton.preferenceEditor.putString(Constants.IAP_SKU, Sku).commit();
                                        Singleton.preferenceEditor.putString(Constants.IAP_ORDERID, OrderId).commit();
                                        Singleton.preferenceEditor.putString(Constants.IAP_TOKEN, Token).commit();
                                        Singleton.preferenceEditor.putString(Constants.IAP_PAYLOAD, Payload).commit();
                                    }
                                }
                            });
                        }
                    }
                });

//                Log.d("123", "purchase : " + purchase.toString());
//                Log.d("123", "333 ok");
            }
            else
            {
                try{
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
//                Log.d("123", "333 error");
            }
        }
    };

    IPayResultCallback iPayResultCallback = new IPayResultCallback()
    {
        @Override
        public void onPayResult(int resultCode, String signvalue, String resultInfo)
        {
            switch (resultCode)
            {
                case IAppPay.PAY_SUCCESS:
                    boolean payState = IAppPayOrderUtils.checkPayResult(signvalue, IAppPaySDKConfig.PLATP_KEY);
                    if(payState){
                        Toast.makeText(mCtx, "支付成功", Toast.LENGTH_LONG).show();
                        ApiManager.getSelfInfo(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), new ApiManager.GetSelfInfoCallback()
                        {
                            @Override
                            public void onResult(boolean success, String message, UserModel userModel)
                            {
                                if (success && userModel != null)
                                {
                                    try {
                                        Singleton.preferenceEditor.putInt(Constants.GIFT_POINT, userModel.getPoint()).commit();
                                        mMyCount.setText(String.valueOf(Singleton.preferences.getInt(Constants.GIFT_POINT, 0)));
                                    } catch (Exception e) {
                                        payPointError();
                                    }
                                }
                                else payPointError();
                            }
                        });

                        try{
                            String Umeng_id="Pay_Product_ipay";
                            HashMap<String,String> mHashMap = new HashMap<String,String>();
                            mHashMap.put("Point", String.valueOf(mApplication.getProductModels().get(mBuyPos).getPoint()));
                            mHashMap.put("userID", Singleton.preferences.getString(Constants.USER_ID, ""));
                            mHashMap.put("Price", "¥ " + String.valueOf(mApplication.getProductModels().get(mBuyPos).getPrice()));
                            MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);
                        }
                        catch (Exception e){
                        }
                    }
                    break;
                case IAppPay.PAY_ING:
                    Toast.makeText(mCtx, "成功下单", Toast.LENGTH_LONG).show();
                    break ;
                default:
                    Toast.makeText(mCtx, resultInfo, Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    private void payPointError()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
        builder.setMessage(getString(R.string.buy_point_error));
        builder.setTitle(getString(R.string.system_notif));
        builder.setPositiveButton("OK", null);
        builder.setCancelable(false);
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        try
        {
            if(mHelper!=null)
            {
            if (!mHelper.handleActivityResult(requestCode,resultCode, data))
            {
                super.onActivityResult(requestCode, resultCode, data);
            }
            }
        }
        catch (Exception e)
        {

        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        Singleton.preferenceEditor.putInt(Constants.Buy_Point_Count,buyPointCount).commit();
        buyPointCount =0;

        try
        {
            if (mHelper != null) mHelper.dispose();
            mHelper = null;
        }
        catch (Exception e)
        {

        }
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.setting_my_count));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class PagerBaseAdapter extends PagerAdapter
    {
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            return mViewList.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mViewList.get(position));
        }

        @Override
        public int getItemPosition(Object object)
        {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            if(position==0)
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);

                mGainNoData = (ImageView) view.findViewById(R.id.nodata);
                mGainProgress = (ProgressBar) view.findViewById(R.id.progress);
                mGainListView = (PullToRefreshListView) view.findViewById(R.id.list);

                mGainListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> refreshView)
                    {
                        ApiManager.getPointGainLog(mCtx, Integer.MAX_VALUE, 15, new ApiManager.GetPointGainLogCallback() {
                            @Override
                            public void onResult(boolean success, ArrayList<PointGainModel> pointGainModels) {
                                mGainListView.onRefreshComplete();

                                if (success && pointGainModels != null) {
                                    if (pointGainModels.size() != 0) {
                                        mGainNoData.setVisibility(View.GONE);
                                        mGainModels.clear();
                                        mGainModels.addAll(pointGainModels);

                                        if (mGainListAdapter != null) mGainListAdapter = null;
                                        mGainListAdapter = new GainListAdapter();
                                        mGainListView.setAdapter(mGainListAdapter);

                                        isFetchingDataGain = false;
                                        noMoreDataGain = false;

                                        if (pointGainModels.size() < 15) {
                                            noMoreDataGain = true;
                                        }
                                    } else {
                                        mGainNoData.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    mGainNoData.setVisibility(View.VISIBLE);
                                    try{
//                          showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                });

                mGainListView.setOnScrollListener(new AbsListView.OnScrollListener()
                {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState)
                    {
                        if (scrollState == SCROLL_STATE_FLING) {
                            ImageLoader.getInstance().pause();
                        }

                        if (scrollState == SCROLL_STATE_IDLE) {
                            ImageLoader.getInstance().resume();
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });

                if(mGainModels.size()==0)
                {
                    mGainProgress.setVisibility(View.VISIBLE);
                    ApiManager.getPointGainLog(mCtx, Integer.MAX_VALUE, 15, new ApiManager.GetPointGainLogCallback() {
                        @Override
                        public void onResult(boolean success, ArrayList<PointGainModel> pointGainModels) {
                            mGainProgress.setVisibility(View.GONE);

                            if (success && pointGainModels != null) {
                                if (pointGainModels.size() != 0) {
                                    mGainNoData.setVisibility(View.GONE);
                                    mGainModels.clear();
                                    mGainModels.addAll(pointGainModels);

                                    mGainListAdapter = new GainListAdapter();
                                    mGainListView.setAdapter(mGainListAdapter);

                                    if (pointGainModels.size() < 15) {
                                        noMoreDataGain = true;
                                    }
                                } else {
                                    mGainNoData.setVisibility(View.VISIBLE);
                                }
                            } else {
                                try {
                                    mGainNoData.setVisibility(View.VISIBLE);
                                    try{
//                          showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }
                    });
                }
                else mGainProgress.setVisibility(View.GONE);

                return mViewList.get(position);
            }
            else
            {
                container.addView(mViewList.get(position));
                View view = mViewList.get(position);

                mUsageNoData = (ImageView) view.findViewById(R.id.nodata);
                mUsageProgress = (ProgressBar) view.findViewById(R.id.progress);
                mUsageListView = (PullToRefreshListView) view.findViewById(R.id.list);

                mUsageListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
                {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> refreshView)
                    {
                        ApiManager.getPointUsageLog(mCtx, Integer.MAX_VALUE, 15, new ApiManager.GetPointUsageLogCallback() {
                            @Override
                            public void onResult(boolean success, ArrayList<PointUsageModel> pointUsageModels) {
                                mUsageListView.onRefreshComplete();

                                if (success && pointUsageModels != null) {
                                    if (pointUsageModels.size() != 0) {
                                        mUsageNoData.setVisibility(View.GONE);
                                        mUsageModel.clear();
                                        mUsageModel.addAll(pointUsageModels);

                                        if (mUsageListAdapter != null) mUsageListAdapter = null;
                                        mUsageListAdapter = new UsageListAdapter();
                                        mUsageListView.setAdapter(mUsageListAdapter);

                                        isFetchingDataUsage = false;
                                        noMoreDataUsage = false;

                                        if (pointUsageModels.size() < 15) {
                                            noMoreDataUsage = true;
                                        }
                                    } else {
                                        mUsageNoData.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    mUsageNoData.setVisibility(View.VISIBLE);
                                    try{
//                          showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                });

                mUsageListView.setOnScrollListener(new AbsListView.OnScrollListener()
                {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState)
                    {
                        if (scrollState == SCROLL_STATE_FLING) {
                            ImageLoader.getInstance().pause();
                        }

                        if (scrollState == SCROLL_STATE_IDLE) {
                            ImageLoader.getInstance().resume();
                        }
                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                    }
                });

                if(mUsageModel.size()==0)
                {
                    mUsageProgress.setVisibility(View.VISIBLE);
                    ApiManager.getPointUsageLog(mCtx, Integer.MAX_VALUE, 15, new ApiManager.GetPointUsageLogCallback() {
                        @Override
                        public void onResult(boolean success, ArrayList<PointUsageModel> pointUsageModels) {
                            mUsageProgress.setVisibility(View.GONE);

                            if (success && pointUsageModels != null) {
                                if (pointUsageModels.size() != 0) {
                                    mUsageNoData.setVisibility(View.GONE);
                                    mUsageModel.clear();
                                    mUsageModel.addAll(pointUsageModels);
                                    mUsageListAdapter = new UsageListAdapter();
                                    mUsageListView.setAdapter(mUsageListAdapter);

                                    if (pointUsageModels.size() < 15) {
                                        noMoreDataUsage = true;
                                    }
                                } else {
                                    mUsageNoData.setVisibility(View.VISIBLE);
                                }
                            } else {
                                try {
                                    mUsageNoData.setVisibility(View.VISIBLE);
                                    try{
//                          showToast(getString(R.string.failed));
                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                } catch (Exception e) {

                                }
                            }
                        }
                    });
                }
                else mUsageProgress.setVisibility(View.GONE);

                return mViewList.get(position);
            }
        }
    };

    private class GainListAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mGainModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderFollow holder = new ViewHolderFollow();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.gain_list_row, null);
                holder.type = (TextView) convertView.findViewById(R.id.type);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.love = (ImageView) convertView.findViewById(R.id.love);
                holder.count = (TextView) convertView.findViewById(R.id.count);

                convertView.setTag(holder);
            }
            else holder = (ViewHolderFollow) convertView.getTag();

            if(mGainModels.get(position).getType().compareTo("freePoint")==0) holder.type.setText(getString(R.string.gift_freePoint));
            else holder.type.setText(getString(R.string.gift_purchase));

            Date date = new Date(mGainModels.get(position).getTimestamp()*1000L);
            holder.date.setText(mDateFormat.format(date));
            holder.count.setText(String.valueOf(mGainModels.get(position).getPoint()));

            holder.love.setImageResource(loves[(int) (Math.random() * loves.length)]);

//            if(position>=getCount()-5)
//            {
//                LoadDataFollow(false);
//            }

            return convertView;
        }
    }

    private class ViewHolderFollow
    {
        TextView type;
        TextView date;
        ImageView love;
        TextView count;
    }

    private class UsageListAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mUsageModel.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderUsage holder = new ViewHolderUsage();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.usage_list_row, null);
                holder.type = (TextView) convertView.findViewById(R.id.type);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.love = (ImageView) convertView.findViewById(R.id.love);
                holder.count = (TextView) convertView.findViewById(R.id.count);
                holder.user_layout = (LinearLayout) convertView.findViewById(R.id.user_layout);

                convertView.setTag(holder);
            }
            else holder = (ViewHolderUsage) convertView.getTag();

//            if(mUsageModel.get(position).getType().compareTo("purchaseGift")==0) holder.type.setText(getString(R.string.gift_purchaseGift));
            holder.type.setText(getString(R.string.gift_purchaseGift));

            Date date = new Date(mUsageModel.get(position).getTimestamp()*1000L);
            holder.date.setText(mDateFormat.format(date));
            holder.count.setText(String.valueOf(mUsageModel.get(position).getPoint()));

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUsageModel.get(position).getUserInfo().getPicture()), holder.pic, SelfOptions);
            holder.name.setText(mUsageModel.get(position).getUserInfo().getOpenID());

            if(mUsageModel.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.user_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if (mUsageModel.get(position).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mUsageModel.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mUsageModel.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mUsageModel.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mUsageModel.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mUsageModel.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mUsageModel.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mUsageModel.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mUsageModel.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mUsageModel.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mUsageModel.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.love.setImageResource(loves[(int) (Math.random() * loves.length)]);

//            if(position>=getCount()-5)
//            {
//                LoadDataFollow(false);
//            }

            return convertView;
        }
    }

    private class ViewHolderUsage
    {
        TextView type;
        TextView date;
        ImageView pic;
        TextView name;
        ImageView verifie;
        ImageView love;
        TextView count;
        LinearLayout user_layout;
    }
}
