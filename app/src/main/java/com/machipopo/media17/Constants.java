package com.machipopo.media17;

import com.squareup.okhttp.Protocol;
import java.util.ArrayList;
import java.util.Arrays;

public class Constants {
	public static final Boolean INTERNATIONAL_VERSION = false;

	public static final boolean IS_SANDBOX = false;

	public static final int WRAP_CONTENT = -2;
	public static final int MATCH_PARENT = -1;

	/* Server */
	public static final String SERVER_IP = (INTERNATIONAL_VERSION) ?  Constants.SERVER_IP_INTERNATIONAL : Constants.SERVER_IP_CHINA;
	public static final String SERVER_IP_INTERNATIONAL = "http://api-dsa.17app.co/";
	public static final String SERVER_IP_CHINA = "http://api.media17.cn/";
//	public static final String SERVER_IP = "http://52.11.159.139:8000/";
//	public static final String SERVER_IP = "http://52.25.82.144:8000/";

	/* 審核 */
	public static final Boolean USERKING = true;

	/* Protocol */
	public static final Protocol PROTOCOL = Protocol.HTTP_1_1;

	/* AWS & UPLOAD & DOWNLOAD */
	public static final String THUMBNAIL_PREFIX = "THUMBNAIL_";

	/* ImageLoader Cache */
	public static final Boolean PHOTO_CACHE = true;

	/* Requests */
	public static final int PICK_IMAGE_REQUEST = 100;
	public static final int TAKE_PICTURE_REQUEST = 200;
	public static final int CROP_IMAGE_REQUEST = 300;

	/* Image Pixels */
	public static final int CROP_LOAD_IMAGE_PIXELS = 1166400; // 1080x1080
	public static final int PROFILE_PICTURE_PIXELS = 1166400; // 1080x1080
	public static final int PROFILE_PICTURE_THUMBNAIL_PIXELS = 409600; // 640x640

	//Curry SDK version 6.2.0
	public static final String FLURRY_API_KEY = (INTERNATIONAL_VERSION) ?  Constants.FLURRY_API_KEY_INTERNATIONAL : Constants.FLURRY_API_KEY_CHINA;
	public static final String FLURRY_API_KEY_INTERNATIONAL = "B3J5S9CWSJHQDZQJYHNP";
	public static final String FLURRY_API_KEY_CHINA = "2PBT8KVKXZ8MBW4N94KN";
	
	/* low Pixels */
	public static final String PHONE_WIDTH_HEIGHT_SIZE = "phone_width_height_size";
	public static final int CHECK_PIXELS = 921601;
	public static final int DEFAULT_PIXELS = 2073600;
	public static final int LOW_CROP_IMAGE_PIXELS = 409600;
	public static final int LOW_PROFILE_PICTURE_PIXELS = 409600;
	public static final int LOW_PROFILE_PICTURE_THUMBNAIL_PIXELS = 102400;

	/* Dean add Keys */
	public static final String KEY = "key";
	public static final String DATA = "data";
	public static final String ACCOUNT = "account";
	public static final String PASSWORD = "password";
	public static final String SAVE_PICTURE = "save_picture";
	public static final String SETTING_PRIVATE_MODE = "setting_private_mode";
	public static final String SETTING_FOLLOW_MODE = "setting_follow_mode";

	/*SMS verification code*/

	public static final String VERIFIED_PHONE_NUMBER = "verified_phone_number";

	public static final String COUNTRY_CALLING_CODE = "countryCallingCode";
	public static final String LOCAL_PHONE_NUMBER = "localPhoneNumber";
	public static final String PHONE_TWO_DIGIT_ISO = "phoneTwoDigitISO";

	public static final String SHOW_AD = "show_ad";
	public static final String LOGOUT = "logout_state";

	public static final String SUMBIT_FACEBOOK = "sumbit_facebook";
	public static final String SUMBIT_INSTAGRAM = "sumbit_instagram";
	public static final String SUMBIT_TWITTER = "sumbit_twitter";
	public static final String SUMBIT_WECHAT= "sumbit_wechat";
	public static final String SUMBIT_QQ = "sumbit_qq";
	public static final String SUMBIT_WEIBO = "sumbit_weibo";

	public static final String PAY_COUNTRY_CODE = "pay_country_code";

	public static final String SETTING_NOTIFI_LIKE = "setting_notifi_like_v2";
	public static final String SETTING_NOTIFI_COMMENT = "setting_notifi_comment_v2";
	public static final String SETTING_NOTIFI_FANS = "setting_notifi_fans_v2";
	public static final String SETTING_NOTIFI_TAG = "setting_notifi_tag_v2";
	public static final String SETTING_NOTIFI_FRIEND = "setting_notifi_friend_v2";
	public static final String SETTING_NOTIFI_SYSTEM = "setting_notifi_system_v2";

	public static final String SETTING_NOTIFI_LIVE = "setting_notifi_live_v2";
	public static final String SETTING_NOTIFI_RELIVE = "setting_notifi_relive_v2";

	public static final String FOLLOWING_COUNT_V2 = "FOLLOWING_COUNT_V2";

	public static final String REVENUE_TODAY = "revenue_today";
	public static final String REVENUE_MONEY_TOTAL = "revenue_money_total";

	public static final String PROFILE_POST_COUNT = "profile_post_count";
	public static final String PROFILE_FOLLOWER_COUNT = "profile_follower_count";
	public static final String PROFILE_FOLLOWING_COUNT = "profile_following_count";
	public static final String PROFILE_LIKE_COUNT = "profile_like_count";
	public static final String PROFILE_NAME = "profile_name";
	public static final String PROFILE_WEB = "profile_web";
	public static final String PROFILE_BIO = "profile_bio";

	public static final String PROFILE_SEX = "profile_sex";
	public static final String PROFILE_AGE = "profile_age";
	public static final String PROFILE_EMAIL = "profile_email";
	public static final String PROFILE_COUNTRY_CODE = "profile_country_code";
	public static final String PROFILE_COUNTRY = "profile_country";
	public static final String PROFILE_COUNTRY_ABBREVIATE = "profile_country_abbreviate";
	public static final String PROFILE_PHONE = "profile_phone";

	public static final String UPLOAD_CAPTION = "upload_caption";
	public static final String UPLOAD_USER = "upload_user";
	public static final String UPLOAD_TAG = "upload_tag";
	public static final String UPLOAD_PHOTO = "upload_photo";
	public static final String UPLOAD_VIDEO = "upload_video";
	public static final String UPLOAD_TYPE = "upload_type";

	public static final String USER_ID = "USER_ID";
	public static final String OPEN_ID = "OPEN_ID";
	public static final String PRIVACY_MODE = "PRIVACY_MODE";
	public static final String PHONE_NUMBER = "PHONE_NUMBER";
	public static final String NAME = "NAME";
	public static final String FULL_NAME = "FULL_NAME";
	public static final String BIO = "BIO";
	public static final String PICTURE = "PICTURE";
	public static final String COVER_PHOTO = "COVER_PHOTO";
	public static final String WEBSITE = "WEBSITE";
	public static final String AGE = "AGE";
	public static final String GENDER = "GENDER";
	public static final String IS_VERIFIED = "IS_VERIFIED";
	public static final String FOLLOWER_COUNT = "FOLLOWER_COUNT";
	public static final String FOLLOWING_COUNT = "FOLLOWING_COUNT";
	public static final String POST_COUNT = "POST_COUNT";
	public static final String REPOST_COUNT = "REPOST_COUNT";
	public static final String LIKE_POST_COUNT = "LIKE_POST_COUNT";
	public static final String IS_CHOICE = "IS_CHOICE";
	public static final String LAST_LOGIN = "LAST_LOGIN";
	public static final String EMAIL = "EMAIL";
	public static final String COUNTRY_CODE = "COUNTRY_CODE";
	public static final String COUNTRY = "COUNTRY";
	public static final String IS_ADMIN = "IS_ADMIN";
	public static final String IS_ADMIN_V2 = "IS_ADMIN_V2";
	public static final String IS_FREEZED = "IS_FREEZED";
	public static final String PUSH_LIKE = "PUSH_LIKE";
	public static final String PUSH_FOLLOW = "PUSH_FOLLOW";
	public static final String PUSH_COMMENT = "PUSH_COMMENT";
	public static final String PUSH_TAG = "PUSH_TAG";
	public static final String PUSH_FRIEND_JOIN = "PUSH_FRIEND_JOIN";
	public static final String PUSH_FRIEND_FIRST_POST = "PUSH_FRIEND_FIRST_POST";
	public static final String PUSH_FRIEND_REQUEST = "PUSH_FRIEND_REQUEST";
	public static final String PUSH_SYSTEM_NOTIF = "PUSH_SYSTEM_NOTIF";
	public static final String NOTIF_BADGE = "NOTIF_BADGE";
	public static final String SYSTEM_NOTIF_BADGE = "SYSTEM_NOTIF_BADGE";
	public static final String PUSH_FOLLOW_REQUEST = "PUSH_FOLLOW_REQUEST";
	public static final String MESSAGE_BADGE = "MESSAGE_BADGE";
	public static final String BANK_NAME = "BANK_NAME";
	public static final String BANK_ADDRESS = "BANK_ADDRESS";
	public static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
	public static final String BENEFICIARY_NAME = "BENEFICIARY_NAME";
	public static final String SWIFT_CODE = "SWIFT_CODE";
	public static final String ROUNTING_NUMBER = "ROUNTING_NUMBER";
	public static final String BANK_COUNTRY = "BANK_COUNTRY";
	public static final String ID_CARD_NUMBER = "ID_CARD_NUMBER";
	public static final String ADDRESS = "ADDRESS";
	public static final String BANK_CODE = "BANK_CODE";
	public static final String BANK_BRANCH_CODE = "BANK_BRANCH_CODE";
	public static final String ID_CARD_FRONT_PICTURE = "ID_CARD_FRONT_PICTURE";
	public static final String ID_CARD_BACK_PICTURE = "ID_CARD_BACK_PICTURE";
	public static final String PASSBOOK_PICTURE = "PASSBOOK_PICTURE";
	public static final String BANK_ACCOUNT_VERIFIED = "BANK_ACCOUNT_VERIFIED";
	public static final String TOTAL_REVENUE_EARNED = "TOTAL_REVENUE_EARNED";
	public static final String TWITTER_ID = "TWITTER_ID";
	public static final String FACEBOOK_ID = "FACEBOOK_ID";
	public static final String INSTAGRAM_ID = "INSTAGRAM_ID";
	public static final String PHONE_VERIFICATION_UUID = "PHONE_VERIFICATION_UUID";
	public static final String GIFT_POINT = "gift_point";
	public static final String Buy_Point_Count = "buy_point_count";
	public static final String GIFT_TOTAL_REVENUE = "gift_totalGiftRevenueEarned";

	public static final String ECEIVEDLIKECOUNT = "ECEIVED_LIKE_COUNT";

	public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
	public static final String DEVICE_ID = "DEVICE_ID";

	public static final String REVENUE_PAYPAL = "revenue_paypal";
	public static final String REVENUE_ALIPAY = "revenue_alipay";

	public static final String REVENUE_NAME = "revenue_name";
	public static final String REVENUE_ID = "revenue_id";
	public static final String REVENUE_ID_FRONT = "revenue_id_front";
	public static final String REVENUE_ID_BACK = "revenue_id_back";
	public static final String REVENUE_BANK_NAME = "revenue_bank_name";
	public static final String REVENUE_BANK_BRANCH = "revenue_bank_branch";
	public static final String REVENUE_BANK_NUMBER = "revenue_bank_number";
	public static final String REVENUE_ACCOUNT_NAME = "revenue_account_name";
	public static final String REVENUE_ACCOUNT_PICTURE = "revenue_account_picture";

	public static final String GIFT_MODULE_STATE = "giftModuleState";
	public static final String HAS_SHOWN_GIFT_MODULE_TUTORIAL = "hasShownGiftModuleTutorial";
	public static final String APP_UPDATE_LINK = "appUpdateLink";
	public static final String FORCE_UPDATE_APP = "forceUpdateApp";
	public static final String LATEST_APP_VERSION = "latestAppVersion";

	/* Keys */
	public static final String FREEZED = "FREEZED";
	public static final String POINT = "POINT";
	//	public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
//	public static final String DEVICE_ID = "DEVICE_ID";
//	public static final String USER_ID = "USER_ID";
	public static final String NICKNAME = "NICKNAME";
	//	public static final String PICTURE = "PICTURE";
	public static final String BIRTHDAY = "BIRTHDAY";
	//	public static final String GENDER = "GENDER";
	public static final String CONSTELLATION = "CONSTELLATION";
	public static final String HEIGHT = "HEIGHT";
	public static final String WEIGHT = "WEIGHT";
	public static final String INTRO = "INTRO";
	public static final String BORN_CITY = "BORN_CITY";
	public static final String BORN_NATION = "BORN_NATION";
	public static final String CURRENT_CITY = "CURRENT_CITY";
	public static final String CURRENT_NATION = "CURRENT_NATION";
	public static final String BODY_TYPE = "BODY_TYPE";
	public static final String HOME_RANK = "HOME_RANK";
	public static final String DATE_FEE_PAYER = "DATE_FEE_PAYER";
	public static final String JOB_TYPE = "JOB_TYPE";
	public static final String JOB_POSITION = "JOB_POSITION";
	public static final String FACEBOOK_FRIEND_COUNT = "FACEBOOK_FRIEND_COUNT";
	public static final String ALBUM_PHOTO_COUNT = "ALBUM_PHOTO_COUNT";
	public static final String BLOOD_TYPE = "BLOOD_TYPE";
	public static final String SCHOOL_NAME = "SCHOOL_NAME";
	public static final String EDUCATION = "EDUCATION";
	public static final String COMPANY_NAME = "COMPANY_NAME";
	public static final String LANGUAGES = "LANGUAGES";
	public static final String HAVE_CHILD = "HAVE_CHILD";
	public static final String RELIGION = "RELIGION";
	public static final String BEFORE_DATE = "BEFORE_DATE";
	public static final String MARRIAGE_ATTITUDE = "MARRIAGE_ATTITUDE";
	public static final String INCOME = "INCOME";
	public static final String INTEREST = "INTEREST";
	public static final String HOLIDAY = "HOLIDAY";
	public static final String PERSONALITY = "PERSONALITY";
	public static final String RELATIONSHIP = "RELATIONSHIP";
	public static final String LIVE_WITH = "LIVE_WITH";
	public static final String SMOKE = "SMOKE";
	public static final String DRINK = "DRINK";
	public static final String WANT_CHILD = "WANT_CHILD";
	public static final String HOUSE_WORK = "HOUSE_WORK";
	//	public static final String LAST_LOGIN = "LAST_LOGIN";
	public static final String POINT_EXPIRATION = "POINT_EXPIRATION";


	public static final String VISIT_COUNT = "VISIT_COUNT";
	public static final String VIP_EXPIRATION = "VIP_EXPIRATION";
	public static final String INFO_COMPLETE_PERCENTAGE = "INFO_COMPLETE_PERCENTAGE";

	//	public static final String IS_FREEZED = "IS_FREEZED";
	public static final String IS_HIDDEN = "IS_HIDDEN";
	public static final String PUSH_NOTIFICATION = "PUSH_NOTIFICATION";

	//	public static final String EMAIL = "EMAIL";
	public static final String EMAIL_PUSH_NOTIFICATION = "EMAIL_PUSH_NOTIFICATION";
	public static final String REGISTER_TIME = "REGISTER_TIME";
	public static final String FIRST_MESSAGE_TIME = "FIRST_MESSAGE_TIME";
	public static final String FAVORITE_TIME = "FAVORITE_TIME";

	public static final String NO_DISTUB = "NO_DISTUB";
	public static final String NO_DISTUB_FROM = "NO_DISTUB_FROM";
	public static final String NO_DISTUB_TO = "NO_DISTUB_TO";
	public static final String MAX_DAILY_LIKE_COUNT = "MAX_DAILY_LIKE_COUNT";
	public static final String VISIT_BADGE = "VISIT_BADGE";
	public static final String BIRTHDAY_CONFIRMED = "BIRTHDAY_CONFIRMED";

	public static final String MATCH_COUNT = "MATCH_COUNT";
	public static final String RECEIVED_MESSAGE_COUNT = "RECEIVED_MESSAGE_COUNT";
	public static final String PLATINUM_VIP_EXPIRATION = "PLATINUM_VIP_EXPIRATION";

	public static final String CARD_NUMBER = "CARD_NUMBER";
	public static final String CARD_EXP = "CARD_EXP";
	public static final String CARD_SECURE_CODE = "CARD_SECURE_CODE";

	public static final String MY_COUNTRY = "MY_COUNTRY";

	/* Notification */
	public static final String LAST_NOTIFICATION_SOUND_AND_VIBRATE_TIME = "LAST_NOTIFICATION_SOUND_AND_VIBRATE_TIME";

	/* Tabs of Home */
	public static final int SEARCH_TAB = 1;
	public static final int LIKE_ME_TAB = 2;
	public static final int CHAT_THREAD_TAB = 3;
	public static final int MORE_TAB = 4;
	public static final String LAST_TAB = "LAST_TAB";

	/* Search System Keys */
	public static final String IS_SEARCHING = "IS_SEARCHING";
	public static final String SEARCH_LOW_AGE = "SEARCH_LOW_AGE";
	public static final String SEARCH_HIGH_AGE = "SEARCH_HIGH_AGE";
	public static final String SEARCH_LOW_HEIGHT = "SEARCH_LOW_HEIGHT";
	public static final String SEARCH_HIGH_HEIGHT = "SEARCH_HIGH_HEIGHT";
	public static final String SEARCH_CURRENT_CITY = "SEARCH_CURRENT_CITY2";
	public static final String SEARCH_CURRENT_NATION = "SEARCH_CURRENT_NATION2";
	public static final String SEARCH_BODY_TYPE = "SEARCH_BODY_TYPE";
	public static final String SEARCH_EDUCATION = "SEARCH_EDUCATION";
	public static final String SEARCH_INCOME = "SEARCH_INCOME";
	public static final String SEARCH_JOB_TYPE = "SEARCH_JOB_TYPE";
	public static final String SEARCH_CONSTELLATION = "SEARCH_CONSTELLATION";
	public static final String SEARCH_BLOOD_TYPE = "SEARCH_BLOOD_TYPE";
	public static final String SEARCH_RELIGION = "SEARCH_RELIGION";
	public static final String SEARCH_DRINK = "SEARCH_DRINK";
	public static final String SEARCH_SMOKE = "SEARCH_SMOKE";
	public static final String SEARCH_RELATIONSHIP = "SEARCH_RELATIONSHIP";
	public static final String SEARCH_MARRIAGE_ATTITUDE = "SEARCH_MARRIAGE_ATTITUDE";
	public static final String SEARCH_BEFORE_DATE = "SEARCH_BEFORE_DATE";
	public static final String SEARCH_LIVE_WITH = "SEARCH_LIVE_WITH";
	public static final String SEARCH_HOLIDAY = "SEARCH_HOLIDAY";
	public static final String SEARCH_HAVE_CHILD = "SEARCH_HAVE_CHILD";
	public static final String SEARCH_NEW_REGISTER = "SEARCH_NEW_REGISTER";
	public static final String SEARCH_HAVE_COMMON_INTEREST = "SEARCH_HAVE_COMMON_INTEREST";
	public static final String SEARCH_HAVE_ALBUM_PHOTO = "SEARCH_HAVE_ALBUM_PHOTO";
	public static final String SEARCH_HAVE_INTRO = "SEARCH_HAVE_INTRO";
	public static final String SEARCH_SORT = "SEARCH_SORT";
	public static final String SEARCH_LAST_LOGIN = "SEARCH_LAST_LOGIN";

	public static final String SENDED_MESSAGE = "SENDED_MESSAGE";

	public static final String CURRENCY_RATE_SETTED = "CURRENCY_RATE_SETTED";
	public static final String CURRENCY_RATE = "CURRENCY_RATE";

	/* System Settings */
	public static final String LAST_SEARCH_MODE = "LAST_SEARCH_MODE";
	public static final String LAST_SHOW_DAILY_SUGGEST_TIME = "LAST_SHOW_DAILY_SUGGEST_TIME";
	public static final String RATED = "RATED";
	public static final String LAST_SHOW_RATE_TIME = "LAST_SHOW_RATE_TIME";
	public static final String LAST_SHOW_OFFER_WALL_TIME = "LAST_SHOW_OFFER_WALL_TIME";
	public static final String LAST_PLAY_NOTIFICATION_SOUND_TIME = "LAST_PLAY_NOTIFICATION_SOUND_TIME";
	public static final String LAST_CHECK_LOGIN_TIME = "LAST_CHECK_LOGIN_TIME";
	public static final String TOKEN_UPLOADED = "TOKEN_UPLOADED";
	public static final String IN_APP_VIBRATION_ON = "IN_APP_VIBRATION_ON";
	public static final String IN_APP_SOUND_ON = "IN_APP_SOUND_ON";
	public static final String IN_APP_LED_ON = "IN_APP_LED_ON";

	/* Product IDs */
	public static final String VIP_30_DAY_PRODUCT_ID = "koikoi_vip_30days";
	public static final String VIP_90_DAY_PRODUCT_ID = "koikoi_vip_90days";
	public static final String VIP_180_DAY_PRODUCT_ID = "koikoi_vip_180days";
	public static final String VIP_360_DAY_PRODUCT_ID = "koikoi_vip_360days";

	public static final String POINT_10_PRODUCT_ID = "koikoi_10_points";
	public static final String POINT_30_PRODUCT_ID = "koikoi_30_points";
	public static final String POINT_50_PRODUCT_ID = "koikoi_50_points";
	public static final String POINT_100_PRODUCT_ID = "koikoi_100_points";

	/* Picker Related */
	public static final ArrayList<String> BODY_TYPE_OPTIONS = new ArrayList<String>(Arrays.asList("slim", "skinny", "normal", "plump", "strong", "fat"));
	public static final ArrayList<String> RELATIONSHIP_OPTIONS = new ArrayList<String>(Arrays.asList("single", "widowed", "divorced"));
	public static final ArrayList<String> REGION_OPTIONS = new ArrayList<String>(Arrays.asList("china", "america", "japan", "taiwan", "hongkong", "singapore", "malaysia", "macau", "korea", "thailand", "indonesia", "vietnam", "philippines", "myanmar", "southeast_asia", "canada", "south_america", "central_america", "united_kingdom", "newzeeland", "australia", "russia", "europe", "africa", "south_africa", "other"));
	public static final ArrayList<String> TW_CITY_OPTIONS = new ArrayList<String>(Arrays.asList("taipei", "keelung", "newtaipei", "ilan", "hsinchu", "hsinchu_county", "taoyuan", "miaoli", "taichung", "changhua", "nantou", "chiayi", "chiayi_county", "yunlin", "tainan", "kaohsiung", "penghu", "pingtung", "taitung", "hualien", "kinmen", "mazu", "other"));
	public static final ArrayList<String> CN_CITY_OPTIONS = new ArrayList<String>(Arrays.asList("beijing", "tianjin", "hebei", "shanxi", "inner_mongolia", "liaoning", "jilin", "heilongjiang", "shanghai", "jiangsu", "zhejiang", "anhui", "fujian", "jiangxi", "shandong", "henan", "hubei", "guangdong", "guangxi", "hainan", "chongqing", "sichuan", "guizhou", "yunnan", "tibet", "shaanxi", "gansu", "qinghai", "ningxia", "xinjiang", "other"));
	public static final ArrayList<String> JP_CITY_OPTIONS = new ArrayList<String>(Arrays.asList("tokyo", "kanagawa", "sakitama", "chiba", "osaka", "kyoto", "hyougo", "aichi", "gifushi", "miekenn", "fukuoka", "hokaidou", "amemiya", "aomorikenn", "iwate", "akita", "yamagata", "fukushima", "ibaraki", "tochigi", "gunnma", "niigata", "yamanashi", "nagano", "shitsuoka", "fukuyama", "ishikawa", "fukui", "shiga", "nara", "wakayama", "hiroshima", "okayama", "tottori", "shimane", "yamakuchi", "kagawa", "tokushima", "ehinu", "kouchi", "sagashi", "ooitashi", "kumamotoshi", "miyasaki", "nagasaki", "kagoshimashi", "okinawa", "other"));
	public static final ArrayList<String> US_CITY_OPTIONS = new ArrayList<String>(Arrays.asList("alabama", "alaska", "arizona", "arkansas", "california", "colorado", "connecticut", "delaware", "florida", "georgia", "hawaii", "idaho", "illinois", "indiana", "iowa", "kansas", "kentucky", "louisiana", "maine", "maryland", "massachusetts", "michigan", "minnesota", "mississippi", "missouri", "montana", "nebraska", "nevada", "new_hampshire", "new_jersey", "new_mexico", "new_york", "north_carolina", "north_dakota", "ohio", "oklahoma", "oregon", "pennsylvania", "rhode_island", "south_carolina", "south_dakota", "tennessee", "texas", "utah", "vermont", "virginia", "washington", "west_virginia", "wisconsin", "wyoming", "other"));
	public static final ArrayList<String> MALE_HOME_RANK_OPTIONS = new ArrayList<String>(Arrays.asList("first_boy", "second_boy", "third_boy"));
	public static final ArrayList<String> FEMALE_HOME_RANK_OPTIONS = new ArrayList<String>(Arrays.asList("first_girl", "second_girl", "third_girl"));
	public static final ArrayList<String> DATE_FEE_PAYER_OPTIONS = new ArrayList<String>(Arrays.asList("male_pay_all", "female_pay_all", "go_dutch", "pay_after_discuss"));
	public static final ArrayList<String> HOLIDAY_OPTIONS = new ArrayList<String>(Arrays.asList("weekend", "weekday", "anytime"));
	public static final ArrayList<String> SMOKE_OPTIONS = new ArrayList<String>(Arrays.asList("never_smoke", "smoke_a_lot", "smoke_when_needed", "sometimes_smoke", "hate_smoke"));
	public static final ArrayList<String> DRINK_OPTIONS = new ArrayList<String>(Arrays.asList("never_drink", "sometimes_drink", "drink_for_work", "good_at_drinking", "alcoholic", "professional_drinker"));
	public static final ArrayList<String> JOB_TYPE_OPTIONS = new ArrayList<String>(Arrays.asList("student", "government", "police", "education", "business", "construction", "financial", "manufacturing", "real_estate", "information", "service", "agriculture", "housekeeping", "medical", "legal", "retail", "transportation", "tourism", "entertainment", "publication", "news", "marketing", "art", "other"));
	public static final ArrayList<String> LANGUAGE_OPTIONS = new ArrayList<String>(Arrays.asList("chinese", "taiwanese", "cantonese", "english", "japanese", "korean", "spanish", "portuguese", "french", "german", "italian", "arabic", "malaysian", "vietnamese", "thai", "russian", "turkish", "other"));
	public static final ArrayList<String> HAVE_CHILD_OPTIONS = new ArrayList<String>(Arrays.asList("no_child", "live_with_child", "live_separately"));
	public static final ArrayList<String> EDUCATION_OPTIONS = new ArrayList<String>(Arrays.asList("below_junior_high", "junior_high", "high_school", "college", "master", "doctor", "other"));
	public static final ArrayList<String> BEFORE_DATE_OPTIONS = new ArrayList<String>(Arrays.asList("meet_asap", "meet_if_feel_good", "meet_after_know_well"));
	public static final ArrayList<String> MARRIAGE_ATTITUDE_OPTIONS = new ArrayList<String>(Arrays.asList("marry_asap", "marry_soon", "marry_if_suitable", "not_marry_now", "not_sure_to_marry"));
	public static final ArrayList<String> INCOME_OPTIONS = new ArrayList<String>(Arrays.asList("income_below_2w", "income_2w_4w", "income_4w_6w", "income_6w_8w", "income_8w_10w", "income_10w_15w", "income_15w_20w", "income_above_20w"));
	public static final ArrayList<String> RELIGION_OPTIONS = new ArrayList<String>(Arrays.asList("no_religion","buddhism", "taoism", "islam", "catholic", "christian", "other"));
	public static final ArrayList<String> PERSONALITY_OPTIONS = new ArrayList<String>(Arrays.asList("enthusiastic", "passionate", "cheerful", "quiet", "energetic", "introvert", "outgoing", "like_communication", "slow_familiar", "shy", "easygoing", "methodical", "compassionate", "perfectionism", "sensitive", "careless", "generous", "honest", "conservative", "open_minded", "caring", "challenging", "independent", "kind", "brave", "tender", "romantic", "patience", "modest", "obedience", "gregarious", "thoughtful", "responsible", "staid", "humorous", "interesting", "overachiever", "straightforward", "content", "callous", "elusive", "cold_hands_warm_heart", "serious", "optimistic", "strict", "audacious", "diligent", "egoistic", "open_hearted", "in_the_mood", "outdoorsy", "indoorsy", "workaholic"));
	public static final ArrayList<String> LIVE_WITH_OPTIONS = new ArrayList<String>(Arrays.asList("live_by_self", "live_with_friend", "live_with_pet", "live_with_family", "other"));
	public static final ArrayList<String> WANT_CHILD_OPTIONS = new ArrayList<String>(Arrays.asList("want_child", "want_no_child", "child_not_sure"));
	public static final ArrayList<String> HOUSEWORK_OPTIONS = new ArrayList<String>(Arrays.asList("always_do_housework", "do_housework_if_have_time", "hope_not_do_housework", "not_do_house_work"));
	public static final ArrayList<String> SORT_OPTIONS = new ArrayList<String>(Arrays.asList("newLogin","newRegister"));
	public static final ArrayList<String> CONSTELLATION_OPTIONS = new ArrayList<String>(Arrays.asList("aquarius","pisces","aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn"));
	public static final ArrayList<String> BLOOD_OPTIONS = new ArrayList<String>(Arrays.asList("O","A","B","AB"));
	public static final ArrayList<String> SEARCH_BODY_TYPE_OPTIONS = new ArrayList<String>(Arrays.asList("non","slim", "skinny", "normal", "plump", "strong", "fat"));
	public static final ArrayList<String> SEARCH_RELATIONSHIP_OPTIONS = new ArrayList<String>(Arrays.asList("non","single", "widowed", "divorced"));
	public static final ArrayList<String> SEARCH_CITY_OPTIONS = new ArrayList<String>(Arrays.asList("non","taipei", "keelung", "newtaipei", "ilan", "hsinchu", "hsinchu_county", "taoyuan", "miaoli", "taichung", "changhua", "nantou", "chiayi", "chiayi_county", "yunlin", "tainan", "kaohsiung", "penghu", "pingtung", "taitung", "hualien", "kinmen", "mazu", "other"));
	public static final ArrayList<String> SEARCH_NATION_OPTIONS = new ArrayList<String>(Arrays.asList("non","taiwan", "hongkong", "macau", "china", "japan", "korea", "singapore", "tailand", "indonesia", "malaysia", "vietnam", "philippines", "myanmar", "southeast_asia", "america", "canada", "south_america", "newzeeland", "australia", "russia", "europe", "africa", "south_africa", "other"));
	public static final ArrayList<String> SEARCH_MALE_HOME_RANK_OPTIONS = new ArrayList<String>(Arrays.asList("non","first_boy", "second_boy", "third_boy"));
	public static final ArrayList<String> SEARCH_FEMALE_HOME_RANK_OPTIONS = new ArrayList<String>(Arrays.asList("non","first_girl", "second_girl", "third_girl"));
	public static final ArrayList<String> SEARCH_DATE_FEE_PAYER_OPTIONS = new ArrayList<String>(Arrays.asList("non","male_pay_all", "female_pay_all", "go_dutch", "pay_after_discuss"));
	public static final ArrayList<String> SEARCH_HOLIDAY_OPTIONS = new ArrayList<String>(Arrays.asList("non","weekend", "weekday", "anytime"));
	public static final ArrayList<String> SEARCH_SMOKE_OPTIONS = new ArrayList<String>(Arrays.asList("non","never_smoke", "smoke_a_lot", "smoke_when_needed", "sometimes_smoke", "hate_smoke"));
	public static final ArrayList<String> SEARCH_DRINK_OPTIONS = new ArrayList<String>(Arrays.asList("non","never_drink", "sometimes_drink", "drink_for_work", "good_at_drinking", "alcoholic", "professional_drinker"));
	public static final ArrayList<String> SEARCH_JOB_TYPE_OPTIONS = new ArrayList<String>(Arrays.asList("non","student", "government", "police", "education", "business", "construction", "financial", "manufacturing", "real_estate", "information", "service", "agriculture", "housekeeping", "medical", "legal", "retail", "transportation", "tourism", "entertainment", "publication", "news", "marketing", "art", "other"));
	public static final ArrayList<String> SEARCH_LANGUAGE_OPTIONS = new ArrayList<String>(Arrays.asList("non","chinese", "taiwanese", "cantonese", "english", "japanese", "korean", "spanish", "portuguese", "french", "german", "italian", "arabic", "malaysian", "vietnamese", "thai", "russian", "turkish", "other"));
	public static final ArrayList<String> SEARCH_HAVE_CHILD_OPTIONS = new ArrayList<String>(Arrays.asList("non","no_child", "live_with_child", "live_separately"));
	public static final ArrayList<String> SEARCH_EDUCATION_OPTIONS = new ArrayList<String>(Arrays.asList("non","below_junior_high", "junior_high", "high_school", "college", "master", "doctor", "other"));
	public static final ArrayList<String> SEARCH_BEFORE_DATE_OPTIONS = new ArrayList<String>(Arrays.asList("non","meet_asap", "meet_if_feel_good", "meet_after_know_well"));
	public static final ArrayList<String> SEARCH_MARRIAGE_ATTITUDE_OPTIONS = new ArrayList<String>(Arrays.asList("non","marry_asap", "marry_soon", "marry_if_suitable", "not_marry_now", "not_sure_to_marry"));
	public static final ArrayList<String> SEARCH_INCOME_OPTIONS = new ArrayList<String>(Arrays.asList("non","income_below_2w", "income_2w_4w", "income_4w_6w", "income_6w_8w", "income_8w_10w", "income_10w_15w", "income_15w_20w", "income_above_20w"));
	public static final ArrayList<String> SEARCH_RELIGION_OPTIONS = new ArrayList<String>(Arrays.asList("non","buddhism", "taoism", "islam", "catholic", "christian", "other"));
	public static final ArrayList<String> SEARCH_PERSONALITY_OPTIONS = new ArrayList<String>(Arrays.asList("non","enthusiastic", "passionate", "cheerful", "quiet", "energetic", "introvert", "outgoing", "like_communication", "slow_familiar", "shy", "easygoing", "methodical", "compassionate", "perfectionism", "sensitive", "careless", "generous", "honest", "conservative", "open_minded", "caring", "challenging", "independent", "kind", "brave", "tender", "romantic", "patience", "modest", "obedience", "gregarious", "thoughtful", "responsible", "staid", "humorous", "interesting", "overachiever", "straightforward", "content", "callous", "elusive", "cold_hands_warm_heart", "serious", "optimistic", "strict", "audacious", "diligent", "egoistic", "open_hearted", "in_the_mood", "outdoorsy", "indoorsy", "workaholic"));
	public static final ArrayList<String> SEARCH_LIVE_WITH_OPTIONS = new ArrayList<String>(Arrays.asList("non","live_by_self", "live_with_friend", "live_with_pet", "live_with_family", "other"));
	public static final ArrayList<String> SEARCH_WANT_CHILD_OPTIONS = new ArrayList<String>(Arrays.asList("non","want_child", "want_no_child", "child_not_sure"));
	public static final ArrayList<String> SEARCH_HOUSEWORK_OPTIONS = new ArrayList<String>(Arrays.asList("non","always_do_housework", "do_housework_if_have_time", "hope_not_do_housework", "not_do_house_work"));
	public static final ArrayList<String> SEARCH_CONSTELLATION_OPTIONS = new ArrayList<String>(Arrays.asList("non","aquarius","pisces","aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn"));
	public static final ArrayList<String> SEARCH_BLOOD_OPTIONS = new ArrayList<String>(Arrays.asList("non","O","A","B","AB"));


	public static final ArrayList<String> REPORT_POST_OPTION = new ArrayList<String>(Arrays.asList("i_dont_like","garbage_message", "bad_to_others", "not_good_for"));
	public static final ArrayList<String> REPORT_USER_OPTION = new ArrayList<String>(Arrays.asList("i_dont_like_user","i_dont_like_user_search", "user_violate"));
	public static final ArrayList<String> REPORT_COMMENT_OPTION = new ArrayList<String>(Arrays.asList("i_dont_like_comment","this_comment_is_spam_scam", "puts_people_at_risk", "comment_should_not_be_in_17"));


//	public static final ArrayList<String> REPORT_OPTIONS = new ArrayList<String>(Arrays.asList(
//			 Singleton.applicationContext.getString(R.string.report_1),Singleton.applicationContext.getString(R.string.report_2),
//			 Singleton.applicationContext.getString(R.string.report_3),Singleton.applicationContext.getString(R.string.report_4),
//			 Singleton.applicationContext.getString(R.string.report_5),Singleton.applicationContext.getString(R.string.report_6),
//			 Singleton.applicationContext.getString(R.string.report_7),Singleton.applicationContext.getString(R.string.report_8),
//			 Singleton.applicationContext.getString(R.string.report_9)));

	public static final String SEND_WITH_RETURN_KEY = "SEND_WITH_RETURN_KEY";
	public static final String IMAGE_FILE_PATH_ARRAY = "IMAGE_FILE_PATH_ARRAY";
	public static final String INITIAL_IMAGE_POSTION = "INITIAL_IMAGE_POSTION";

	// multi photo viewer mode
	public static final String IS_BROWSING_USER_PROFILE_PICTURES = "IS_BROWSING_USER_PROFILE_PICTURES";

	// purchase from machi
	public static final int APP_DRIVER_SITE_ID = 970;
	public static final String APP_DRIVER_SITE_KEY = "8bce4fc71900f572c21cc86835aa2cbd";
	public static final int APP_DRIVER_MEDIA_ID = 308;
	/* Ad Types */
	public static final String ADMOB_BANNER_ID = "ca-app-pub-4572371432324546/8662233719";
	public static final String ADMOB_INTERSTITIAL_ID = "ca-app-pub-4572371432324546/1138966911";

	public static final String TAPJOY_APP_ID = "478e2585-ac1f-4b82-9a96-db1a501dcf3c";
	public static final String TAPJOY_SECRET = "PmqETpeN3VC0AhIM6GD7";
	/* Product IDs */
	public static final String VIP_30_DAY_SUBSCRIPTION = "koikoi_30days_vip";
	public static final String VIP_90_DAY = "koikoi_90days_vip";
	public static final String VIP_180_DAY = "koikoi_180days_vip";
	public static final String VIP_360_DAY = "koikoi_360days_vip";

	public static final String COIN_10_PRODUCT_ID = "koikoi_10_points";
	public static final String COIN_30_PRODUCT_ID = "koikoi_30_points";
	public static final String COIN_50_PRODUCT_ID = "koikoi_50_points";
	public static final String COIN_100_PRODUCT_ID = "koikoi_100_points";

	public static final String PLATINUM_VIP_30_DAY = "koikoi_30days_platinum_vip";
	public static final String POINTS_SUBSCRIPTION ="koikoi_points_subscription";

	public static final int PURCHASE_REQUEST = 1200;
	public static final String GROUPDATA = "GROUPDATA";
	public static final String BLOCKDATA = "BLOCKDATA";
	public static final String GROUP_UPDATE_TIME = "GROUP_UPDATE_TIME";
	public static final String BLOCK_UPDATE_TIME = "BLOCK_UPDATE_TIME";

	/* Notifications */
	public static final int NEW_MESSAGE_NOTIFICATION = 100;
	public static final String CURRENT_MESSAGE_NOTIFICATION_USER_ID = "CURRENT_MESSAGE_NOTIFICATION_USER_ID";

	public static final String ALBUM_JSON_STRING = "ALBUM_JSON_STRING";
	public static final String UPDATE_USER_AT_POSITION = "UPDATE_USER_AT_POSITION";

	public static final int CAPTURE_MODE_PHOTO = 0;
	public static final int CAPTURE_MODE_MOVIE = 1;

	/* Live Streaming */
	public static final int LIVE_STREAM_UDP_SERVER_PORT = 10000;
    public static final int LIVE_STREAM_TCP_SERVER_PORT = 20000;

	public static final String LIVE_STREAM_UDP_SERVER_IP = "liveudp.17app.co";
	public static final String LIVE_STREAM_TCP_SERVER_IP = (INTERNATIONAL_VERSION) ?  Constants.LIVE_STREAM_TCP_SERVER_IP_INTERNATIONAL : Constants.LIVE_STREAM_TCP_SERVER_IP_CHINA;
	public static final String LIVE_STREAM_TCP_SERVER_IP_INTERNATIONAL = "livebroadcast.17app.co";
	public static final String LIVE_STREAM_TCP_SERVER_IP_CHINA = "livebroadcast.media17.cn";
//    public static final String LIVE_STREAM_UDP_SERVER_IP = "story17livestream.machipopo.com";
//    public static final String LIVE_STREAM_TCP_SERVER_IP = "story17server.machipopo.com";

//	public static final String LIVE_STREAM_TCP_SERVER_IP = "10.0.1.86";
//	public static final String LIVE_STREAM_UDP_SERVER_IP = "10.0.1.86";

	public static final int LIVE_STREAM_FRAME_WIDTH = 640;
	public static final int LIVE_STREAM_FRAME_HEIGHT = 360;
	public static final int LIVE_STREAM_FRAME_RATE = 30;
	public static final int LIVE_STREAM_BIT_RATE = 512000;
	public static final int LIVE_STREAM_GOP_SIZE = 15;
	public static final int LIVE_STREAM_AUDIO_SAMPLE_RATE = 48000;

	/*preference for Livestream gift tutorial during first launch*/
	public static final String LIVE_STREAM_TUTOR = "live_stream_tutor";

	/* Movie */
	public static final int MOVIE_WIDTH = 480;
	public static final int MOVIE_HEIGHT = 480;
	public static final int MOVIE_FRAME_RATE = 30;
	public static final int MOVIE_GOP_SIZE = 30;
	public static final int MOVIE_VIDEO_BIT_RATE = 1024000;
	public static final int MOVIE_AUDIO_BIT_RATE = 96000;
	public static final int MOVIE_AUDIO_SAMPLE_RATE = 44100;
	public static final float MOVIE_MIN_RECORD_TIME = 3.0f;
	public static final float MOVIE_MAX_RECORD_TIME = 15.0f;
	public static final String MOVIE_FILE_NAME = "movie.mp4";

	public static final String IP = "IP";
	public static final String CDN_BASE_URL = "CDN_BASE_URL";
	public static final String IP_COUNTRY = "IP_COUNTRY";
	public static final String LIVE_MY_CITY = "LIVE_MY_CITY";
	public static final String LIVE_MY_LAT = "LIVE_MY_LAT";
	public static final String LIVE_MY_LON = "LIVE_MY_LON";
//	public static final String CLOUD_FRONT_CDN_URL = "http://d61s7l1z53iwn.cloudfront.net/";
//	public static final String CLOUD_FLARE_CDN_URL = "http://story17.machipopo.com/";

//	public static final String CHT_IMAGE_URL = "http://story17.machipopo.com.s3.amazonaws.com/";
//	public static final String CHT_LIVE_URL = "http://livechunkcdn.17app.co/";
	public static final String CDN_LIVE_URL_CHINA = "http://livecdn.media17.cn/";
	public static final String NO_CHT_ALL_URL = (INTERNATIONAL_VERSION) ?  Constants.NO_CHT_ALL_URL_INTERNATIONAL : Constants.NO_CHT_ALL_URL_CHINA_;
	public static final String NO_CHT_ALL_URL_INTERNATIONAL = "http://cdn.17app.co/";
	public static final String NO_CHT_ALL_URL_CHINA_ = "http://cdn.media17.cn/";

	public static final String AD_LIVE_ID = "898257960220808_960369190676351";
	public static final String AD_FEED_ID = "898257960220808_960385934008010";

	public static final String UMENG_ID = (INTERNATIONAL_VERSION) ?  Constants.UMENG_ID_INTERNATIONAL : Constants.UMENG_ID_CHINA;
	public static final String UMENG_ID_INTERNATIONAL = "563a280b67e58e19c40012fa";
	public static final String UMENG_ID_CHINA = "566f902c67e58eed6f0000a5";
	public static final String BAIDU_ID = "Xkw3Cyv0k0EKe8MshKwVadPM";

	// Note: Your consumer key and secret should be obfuscated in your source code before shipping.
	public static final String TWITTER_KEY = "CEcNgBKFEE91PukDb87w5TXir";
	public static final String TWITTER_SECRET = "wljFSU8LP6xWApQDrTpiVWN3Mh2AOWF9XmjzVFNKvjecKaXaRv";

	public static final String IG_CLIENT_ID = "8a3f7bbecb5b405c87c2ae846f58bb6d";
	public static final String IG_REDIRECT_URI= "http://17app.co"; //(INTERNATIONAL_VERSION) ?  Constants.MEDIA17_WEBSITE_INTERNATIONAL : Constants.MEDIA17_WEBSITE_CHINA
	public static final String GiftPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAit6IzcRZJXEyS8pjoREaBL5TRv6bPiw1S540XaYTM29c3DSsPOrmixYpl4tmDUd1TtR6qyAdIvs1s4Dqs2/dPIAAvChqugqsiopE6EAmpu2KPXvL5mX1uVI3aLPn2aSlDN26GxGw3sYTeg+EY4duybjIOKNTK/zepdAB6YtnSMsC0HVATA2sFVFoVwBKK2gxN6RQMxy01UjHcoU31Yri1tobtVSl6hvp16sZKXV3jVzDwE3owCu3zxKTGu43cmk7JrEi8T24sxcKTvAqwmvSruq9kN/g7qPkGOGXmx3zYFBCZ1XlZ3H+r5qMvIGd0/IZZ/k6fc4CfcS4C4+mXuQb7wIDAQAB";

	public static final String PackgeName = "com.machipopo.media17";

	public static final String IAP_USER = "IAP_USER";
	public static final String IAP_SKU = "IAP_SKU";
	public static final String IAP_ORDERID = "IAP_ORDERID";
	public static final String IAP_TOKEN = "IAP_TOKEN";
	public static final String IAP_PAYLOAD = "IAP_PAYLOAD";

	public static final String SETTING_NOTIFICATION_OPEN = "SETTING_NOTIFICATION_OPEN";
	public static final String SETTING_NOTIFICATION_SOUND = "SETTING_NOTIFICATION_SOUND";
	public static final String SETTING_NOTIFICATION_VIBRATE = "SETTING_NOTIFICATION_VIBRATE";

	public static final String QQAppid = "1104946300";

	public static final String WEIBO_APP_KEY = "3776632650";
	public static final String WEIBO_SECRET = "a02f0e655a80fc467ebf35864cacc8aa";
	public static final String WEIBO_REDIRECT_URL = "http://17app.co";//(INTERNATIONAL_VERSION) ?  Constants.MEDIA17_WEBSITE_INTERNATIONAL : Constants.MEDIA17_WEBSITE_CHINA;;
	public static final String WEIBO_SCOPE = "email,direct_messages_read,direct_messages_write,"+ "friendships_groups_read,friendships_groups_write,statuses_to_me_read," + "follow_app_official_microblog," + "invitation_write";

	public static final String WECHAT_APP_KEY = "wxc48d4e480a3d0c0e";
	public static final String WECHAT_SECRET = "d4624c36b6795d1d99dcf0547af5443d";

	public static final String EVENT_SESSION_ID = "EVENT_SESSION_ID";

	public static final String MEDIA17_WEBSITE = (INTERNATIONAL_VERSION) ?  Constants.MEDIA17_WEBSITE_INTERNATIONAL : Constants.MEDIA17_WEBSITE_CHINA;
	public static final String MEDIA17_WEBSITE_INTERNATIONAL = "http://17.media/";
	public static final String MEDIA17_WEBSITE_CHINA = "http://media17.cn/";

	public static final String MEDIA17_WEBSITE_SHARE = (INTERNATIONAL_VERSION) ?  Constants.MEDIA17_WEBSITE_SHARE_INTERNATIONAL : Constants.MEDIA17_WEBSITE_SHARE_CHINA;
	public static final String MEDIA17_WEBSITE_SHARE_INTERNATIONAL = "http://17.media/";
	public static final String MEDIA17_WEBSITE_SHARE_CHINA = "http://share.media17.cn/";

	public static final String OPEN_APP_COUNT = "OPEN_APP_COUNT";

	public static final String LOG_EVENT_ID = "LOG_EVENT_ID";
	public static final String LOG_EVENT_NAME = "LOG_EVENT_NAME";

	public static final String LOG_EVENT_ID_FROM = "LOG_EVENT_ID_FROM";
	public static final String LOG_EVENT_NAME_FROM = "LOG_EVENT_NAME_FROM";

	public static final String LOG_EVENT_OPEN_TIME = "LOG_EVENT_OPEN_TIME";

	public static final String SHARE_LINK_APP = (INTERNATIONAL_VERSION) ?  Constants.SHARE_LINK_APP_INTERNATIONAL : Constants.SHARE_LINK_APP_CHINA;
	public static final String SHARE_LINK_APP_INTERNATIONAL = "http://17.media/";
	public static final String SHARE_LINK_APP_CHINA = "http://media17.cn/";
}