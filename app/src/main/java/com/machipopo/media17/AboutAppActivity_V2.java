    package com.machipopo.media17;

    import android.animation.Animator;
    import android.animation.AnimatorSet;
    import android.animation.ObjectAnimator;
    import android.app.Activity;
    import android.content.Intent;
    import android.content.SharedPreferences;
    import android.graphics.drawable.AnimationDrawable;
    import android.os.Build;
    import android.os.Bundle;
    import android.os.Handler;
    import android.view.View;
    import android.view.ViewTreeObserver;
    import android.view.Window;
    import android.view.WindowManager;
    import android.view.animation.AccelerateInterpolator;
    import android.view.animation.BounceInterpolator;
    import android.widget.ImageView;
    import android.widget.LinearLayout;
    import android.widget.RelativeLayout;
    import android.widget.TextView;

    import com.umeng.analytics.MobclickAgent;

    import java.util.ArrayList;
    import java.util.List;

    public class AboutAppActivity_V2 extends Activity {
        //======================
        //Animation
        //======================
        //Background Image
        ObjectAnimator bgImageFadeIn, bgImageFadeOut, bgImageTranslate;

        //Logo
        private AnimationDrawable animationDrawable;
        ObjectAnimator logoFadeIn, logoTranslateAnimator, logoScaleX, logoScaleY;
        AnimatorSet logoAnimationSet;

        //First Pic
        AnimatorSet introFadeInSet, introFadeOutSet, introFirstFadeInSet;
        ObjectAnimator solganFadeIn, solganFadeOut;

        //Button
        AnimatorSet btnFadeInSet, btnFadeOutSet, btnFadeScaleSet;
        ObjectAnimator btnFadeIn, btnFadeOut, btnSpecialScaleX, btnSpecialScaleY;

        //Final View
        AnimatorSet finalAnimationSet;
        ObjectAnimator finalViewFadeIn;

        //======================
        private ImageView imgLogo;
        private TextView txtSolgan, btnDirectLogin;
        private ImageView imgBackground, imgBlurBackground;
        private RelativeLayout btnContinue, sloganParent,btnRegister, btnFBLogin;
        private LinearLayout loginViewGroup;

        private List<Animator> animationListSet;

        private int screenWidth, screenHeight, bgImageWidth, bgImageHeight;
        private float logoStartPosition, logoEndPosition;
        private float solganPosition;
        private int totalAnimationCount = 0;
        private final int MAX_ANIMATION_COUNT = 4;

        private AboutAppActivity_V2 mCtx;

        private boolean hasShownEnterAppAnimation = false;

        public void onResume()
        {
            super.onResume();
            MobclickAgent.onPageStart(AboutAppActivity_V2.this.getClass().getSimpleName());
        }

        public void onPause()
        {
            super.onPause();
            MobclickAgent.onPageEnd(AboutAppActivity_V2.this.getClass().getSimpleName());
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.about_activity_v2);

            try
            {
                if(Build.VERSION.SDK_INT >= 21)
                {
                    Window window = AboutAppActivity_V2.this.getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    window.setStatusBarColor(getResources().getColor(R.color.main_color));
                }
            }
            catch (Exception e)
            {
            }

            imgLogo = (ImageView)findViewById(R.id.imgLogo);
            txtSolgan = (TextView)findViewById(R.id.txtSlogan);
            imgBackground = (ImageView)findViewById(R.id.imgBackground);
            imgBlurBackground = (ImageView)findViewById(R.id.imgBlurBackground);
            btnContinue = (RelativeLayout)findViewById(R.id.btnContinue);
            loginViewGroup = (LinearLayout)findViewById(R.id.loginViewGroup);
            sloganParent = (RelativeLayout)findViewById(R.id.sloganParent);
//            btnDirectLogin = (TextView)findViewById(R.id.btnDirectLogin);
//            btnRegister = (RelativeLayout)findViewById(R.id.btnRegister);
//            btnFBLogin = (RelativeLayout)findViewById(R.id.btnFBLogin);


            screenWidth = getResources().getDisplayMetrics().widthPixels;
            screenHeight = getResources().getDisplayMetrics().heightPixels;

            // calculate init position
            logoStartPosition = screenHeight/2f;
            logoEndPosition = logoStartPosition/4f;
            solganPosition = (logoStartPosition - logoEndPosition) / 2f + logoEndPosition;

            // setup Image Size
            bgImageWidth = Math.round(screenWidth * 1.05f);
            bgImageHeight = Math.round(screenHeight * 1.05f);
            imgBackground.getLayoutParams().width = bgImageWidth;
            imgBackground.getLayoutParams().height = bgImageHeight;
            imgBlurBackground.getLayoutParams().width = bgImageWidth;
            imgBlurBackground.getLayoutParams().height = bgImageHeight;
            imgBlurBackground.setVisibility(View.INVISIBLE);

            // setup logo animation to play frame by frame
//            imgLogo.setImageResource(R.drawable.logo_animation);

            // init animation and listener
            logoAnimationSet = new AnimatorSet();
            logoAnimationSet.addListener(logoAnimationListener);

            introFadeInSet = new AnimatorSet();
            introFadeInSet.addListener(introFadeInListener);

            introFadeOutSet = new AnimatorSet();
            introFadeOutSet.addListener(introFadeOutListener);

            introFirstFadeInSet = new AnimatorSet();
            introFirstFadeInSet.addListener(introFirstFadeInListener);

            finalAnimationSet = new AnimatorSet();
            finalAnimationSet.addListener(finalAnimationListener);

            btnFadeInSet = new AnimatorSet();
            btnFadeOutSet = new AnimatorSet();
            btnFadeScaleSet = new AnimatorSet();




            setupButtonAnimation();
            setupLogoAnimation();
            setupIntroAnimation();
            setupFinalAnimation();
            //set slogan position
            sloganParent.setY(solganPosition);
//            CustomAnimationDrawableNew cad = new CustomAnimationDrawableNew(
//                    (AnimationDrawable) getResources().getDrawable(
//                            R.drawable.logo_animation)) {
//                @Override
//                void onAnimationFinish() {
//                    //logoAnimationSet.start();
//                }
//            };
//
//            // Set the views drawable to our custom drawable
//            imgLogo.setBackgroundDrawable(cad);
//
//            // Start the animation
//            cad.start();

            btnContinue.setOnClickListener(ButtonClickEventListener);

            imgLogo.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if(hasShownEnterAppAnimation) {
                        return;
                    }

                    hasShownEnterAppAnimation = true;

                    // set start position
                    imgLogo.setY(logoStartPosition);
                    imgLogo.setX(screenWidth/2f - imgLogo.getWidth()/2f);
                    imgLogo.setAlpha(0f);
                    logoAnimationSet.start();
                }
            });
        }

        public void setConfig(String key, String value)
        {
            SharedPreferences settings = getSharedPreferences("settings", 0);
            SharedPreferences.Editor PE = settings.edit();
            PE.putString(key, value);
            PE.commit();
        }

        private void setupLogoAnimation(){
            logoFadeIn = ObjectAnimator.ofFloat(imgLogo, "alpha", 0f, 1f);
            logoFadeIn.setDuration(800);

            logoTranslateAnimator = ObjectAnimator.ofFloat(imgLogo, "translationY", logoStartPosition, logoEndPosition);
            logoTranslateAnimator.setDuration(1000);

            //Logo alpha --> translate
            List<Animator> logoAnimation = new ArrayList<>();
            logoAnimation.add(logoFadeIn);
            logoAnimation.add(logoTranslateAnimator);

            logoAnimationSet.playSequentially(logoAnimation);
        }

        private void setupIntroAnimation(){
            bgImageFadeIn = ObjectAnimator.ofFloat(imgBackground, "alpha", 0.3f, 1f);
            bgImageFadeIn.setDuration(500);

            solganFadeIn = ObjectAnimator.ofFloat(txtSolgan, "alpha", 0f, 1f);
            solganFadeIn.setDuration(500);

            bgImageTranslate = ObjectAnimator.ofFloat(imgBackground, "translationX", screenWidth-bgImageWidth, 0f);
            bgImageTranslate.setDuration(1500);

            //show button at the same time


            bgImageFadeOut = ObjectAnimator.ofFloat(imgBackground, "alpha", 1f, 0f);
            bgImageFadeOut.setDuration(500);

            solganFadeOut = ObjectAnimator.ofFloat(txtSolgan, "alpha", 1f, 0f);
            solganFadeOut.setDuration(500);


            animationListSet = new ArrayList<>();
            animationListSet.add(bgImageFadeIn);
            animationListSet.add(solganFadeIn);
//            animationListSet.add(bgImageTranslate);
//            animationListSet.add(btnFadeInSet);

            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playSequentially(animationListSet);


            introFirstFadeInSet.play(animatorSet).before(bgImageTranslate);
            introFirstFadeInSet.play(bgImageTranslate).with(btnFadeInSet);


            introFadeInSet.play(bgImageFadeIn).before(solganFadeIn);
            introFadeInSet.play(solganFadeIn).with(bgImageTranslate);

            introFadeOutSet.play(bgImageFadeOut).with(solganFadeOut);

        }

        private void setupButtonAnimation(){
            btnFadeIn = ObjectAnimator.ofFloat(btnContinue, "alpha", 0f, 1f);
            btnFadeIn.setDuration(1000);
            btnFadeInSet.play(btnFadeIn);

            btnFadeOut = ObjectAnimator.ofFloat(btnContinue, "alpha", 1f, 0f);
            btnFadeOut.setDuration(500);
            btnFadeOut.setInterpolator(new AccelerateInterpolator());
            btnFadeOutSet.play(btnFadeOut);

            btnSpecialScaleX = ObjectAnimator.ofFloat(btnContinue, "scaleX", 0.98f);
            btnSpecialScaleX.setInterpolator(new BounceInterpolator());
            btnSpecialScaleX.setInterpolator(new AccelerateInterpolator());
            btnSpecialScaleX.setRepeatCount(2);
            btnSpecialScaleY = ObjectAnimator.ofFloat(btnContinue, "scaleY", 0.98f);
            btnSpecialScaleY.setInterpolator(new BounceInterpolator());
            btnSpecialScaleY.setInterpolator(new AccelerateInterpolator());
            btnSpecialScaleY.setRepeatCount(2);
            btnFadeScaleSet.play(btnSpecialScaleX).with(btnSpecialScaleY);
        }

        private void setupFinalAnimation(){
            finalViewFadeIn = ObjectAnimator.ofFloat(imgBackground, "alpha", 0.3f, 1f);
            finalViewFadeIn.setDuration(500);

            finalAnimationSet.play(btnFadeScaleSet).before(btnFadeOutSet);
//            finalAnimationSet.play(btnFadeOutSet).with(finalViewFadeIn);
//            finalAnimationSet.play(finalViewFadeIn).with(solganFadeIn);
        }

        private void setBackgroundImage(int resource_id){
            imgBackground.setAlpha(0f);
            imgBackground.setX(screenWidth-bgImageWidth);
            imgBackground.setY(screenHeight-bgImageHeight);
            imgBackground.setImageResource(resource_id);
        }

        private void resetBackgroundImage(int resource_id){
            imgBackground.setAlpha(0f);
            imgBackground.getLayoutParams().width = screenWidth;
            imgBackground.getLayoutParams().height = screenHeight;
            imgBackground.setX(0f);
            imgBackground.setY(0f);
            imgBackground.setImageResource(resource_id);
        }

        private void setBlurBackgroundImage(int resource_id, float x_position){
            imgBlurBackground.setAlpha(1f);
            imgBlurBackground.setX(x_position);
            imgBlurBackground.setY(screenHeight-bgImageHeight);
            imgBlurBackground.setImageResource(resource_id);
        }

        View.OnClickListener ButtonClickEventListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.btnContinue:
                        setConfig("about","ok");
                        logoAnimationSet.removeAllListeners();
                        logoAnimationSet.end();
                        logoAnimationSet.cancel();

                        introFadeOutSet.removeAllListeners();
                        introFadeOutSet.end();
                        introFadeOutSet.cancel();

                        introFadeInSet.removeAllListeners();
                        introFadeInSet.end();
                        introFadeInSet.cancel();
//                    introFirstFadeInSet.cancel();

//                        setBackgroundImage(R.drawable.bg5);
                        txtSolgan.setVisibility(View.VISIBLE);
                        txtSolgan.setAlpha(0);
                        txtSolgan.setText(getText(R.string.slogand4));

//                        resetBackgroundImage(R.drawable.bg5);
                        finalAnimationSet.start();

                        break;
                }
            }
        };

        Animator.AnimatorListener logoAnimationListener = new Animator.AnimatorListener(){

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                txtSolgan.setAlpha(0f);
                txtSolgan.setText(getText(R.string.slogand1));
//                txtSolgan.setX(screenWidth / 2f - (txtSolgan.getWidth() / 2f + 10));
//                txtSolgan.setY(solganPosition);
//                sloganParent.setY(solganPosition);

                setBackgroundImage(R.drawable.bg1a);
                totalAnimationCount++;
                introFirstFadeInSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };

        //only execute at first time
        Animator.AnimatorListener introFirstFadeInListener = new Animator.AnimatorListener(){

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                totalAnimationCount++;
                //prepare a blur pic
                setBlurBackgroundImage(R.drawable.bg1b, 0f);
                introFadeOutSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };


        Animator.AnimatorListener introFadeInListener = new Animator.AnimatorListener(){

            @Override
            public void onAnimationStart(Animator animation) {
                if(totalAnimationCount >= 2 && totalAnimationCount <= MAX_ANIMATION_COUNT){
                    switch (totalAnimationCount){
                        case 2:
                            setBlurBackgroundImage(R.drawable.bg2b, screenWidth-bgImageWidth);
                            break;
                        case 3:
                            setBlurBackgroundImage(R.drawable.bg3b, screenWidth-bgImageWidth);
                            break;
                        case 4:
                            setBlurBackgroundImage(R.drawable.bg4b, screenWidth-bgImageWidth);
                            break;
                    }
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {

                totalAnimationCount++;
                if(totalAnimationCount <= MAX_ANIMATION_COUNT) {

//                    txtSolgan.setAlpha(0f);

                    switch (totalAnimationCount) {
                        case 2:
                            //prepare a blur pic
                            setBlurBackgroundImage(R.drawable.bg1b, 0f);
                            introFadeOutSet.start();

                            break;
                        case 3:
                            //prepare a blur pic
                            setBlurBackgroundImage(R.drawable.bg2b, 0f);
                            introFadeOutSet.start();
                            break;
                        case 4:
                            //prepare a blur pic
                            setBlurBackgroundImage(R.drawable.bg3b, 0f);
                            introFadeOutSet.start();
                            break;
                    }



//                    introFadeInSet.start();
                }



                if(totalAnimationCount == 5){
//                    resetBackgroundImage(R.drawable.bg5);
                    finalAnimationSet.start();
                }

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };

        Animator.AnimatorListener introFadeOutListener = new Animator.AnimatorListener(){

            @Override
            public void onAnimationStart(Animator animation) {
                imgBlurBackground.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(totalAnimationCount <= MAX_ANIMATION_COUNT) {
                    switch (totalAnimationCount){
                        case 2:
                            txtSolgan.setText(getText(R.string.slogand2));
//                            txtSolgan.setX(screenWidth / 2f - (txtSolgan.getWidth() / 2f + 10));
//                            txtSolgan.setY(solganPosition);
                            setBackgroundImage(R.drawable.bg2a);
                            introFadeInSet.start();
                            break;
                        case 3:
                            txtSolgan.setText(getText(R.string.slogand3));
//                            txtSolgan.setX(screenWidth / 2f - (txtSolgan.getWidth() / 2f + 10));
//                            txtSolgan.setY(solganPosition);
                            setBackgroundImage(R.drawable.bg3a);
                            introFadeInSet.start();
                            break;
                        case 4:
                            txtSolgan.setText(getText(R.string.slogand4));
//                            txtSolgan.setX(screenWidth / 2f - (txtSolgan.getWidth() / 2f + 10));
//                            txtSolgan.setY(solganPosition);
                            setBackgroundImage(R.drawable.bg4a);
                            introFadeInSet.start();
                            break;
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };

        Animator.AnimatorListener finalAnimationListener = new Animator.AnimatorListener(){

            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                loginViewGroup.setAlpha(1f);
//                loginViewGroup.setVisibility(View.VISIBLE);
//                startActivity(new Intent(AboutAppActivity_V2.this, LoginMenuActivity_V2.class));
                startActivity(new Intent(AboutAppActivity_V2.this, LoginMenuActivity_V3.class));
                finish();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };


        abstract class CustomAnimationDrawableNew extends AnimationDrawable {

            /** Handles the animation callback. */
            Handler mAnimationHandler;

            public CustomAnimationDrawableNew(AnimationDrawable aniDrawable) {
        /* Add each frame to our animation drawable */
                Singleton.log("Total frames : " + aniDrawable.getNumberOfFrames());
                for (int i = 0; i < aniDrawable.getNumberOfFrames(); i++) {
                    this.addFrame(aniDrawable.getFrame(i), aniDrawable.getDuration(i));
                }
            }

            @Override
            public void start() {
                super.start();
        /*
         * Call super.start() to call the base class start animation method.
         * Then add a handler to call onAnimationFinish() when the total
         * duration for the animation has passed
         */
                mAnimationHandler = new Handler();
                mAnimationHandler.postDelayed(new Runnable() {

                    public void run() {
                        onAnimationFinish();
                    }
                }, getTotalDuration());

            }

            /**
             * Gets the total duration of all frames.
             *
             * @return The total duration.
             */
            public int getTotalDuration() {

                int iDuration = 0;

                for (int i = 0; i < this.getNumberOfFrames(); i++) {
                    iDuration += this.getDuration(i);
                }

                return iDuration;
            }

            /**
             * Called when the animation finishes.
             */
            abstract void onAnimationFinish();
        }

    }
