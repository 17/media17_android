package com.machipopo.media17.notify;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by Zack on 2/16/16.
 */
public class NotifyProvider {

    private static final Bus BUS = new Bus(ThreadEnforcer.ANY);

    public static Bus getInstance() {
        return BUS;
    }

    private NotifyProvider() {
        // No instances.
    }
}
