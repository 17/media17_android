package com.machipopo.media17;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdsManager;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshHeaderGridView;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.View.EmptyView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.View.LoadMoreView;
import com.machipopo.media17.View.TouchImage;
import com.machipopo.media17.fragment.BaseFragment;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.LiveFeedModel;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.SuggestedUsersModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;
import com.warnyul.android.widget.FastVideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by POPO on 5/27/15.
 */
public class HomeFragment extends BaseFragment
{
    private MenuActivity mMenuActivity;
    public Story17Application mApplication;
    private LayoutInflater inflater;

    private PullToRefreshListView mListView;
    private EmptyView emptyFooterView;
    private LoadMoreView loadMoreView;
    private listAdapter mListAdapter;

    private ArrayList<LiveFeedModel> mFeedModels = new ArrayList<LiveFeedModel>();
    private DisplayImageOptions BigOptions, SelfOptions, GroupOptions;

    private DisplayMetrics mDisplayMetrics;
    private ProgressBar mProgress;

    private Boolean isFetchingData = false;
    private Boolean noMoreData = false;

    private RelativeLayout mBear;
    private Button mFollowFriend;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};

    private int bears[] = {R.drawable.bear_1,R.drawable.bear_2,R.drawable.bear_3,R.drawable.bear_4,R.drawable.bear_5,R.drawable.bear_6,R.drawable.bear_7,R.drawable.bear_8};

    private int bubbles[] = {R.drawable.bubble_1,R.drawable.bubble_2,R.drawable.bubble_3,R.drawable.bubble_4,R.drawable.bubble_5,R.drawable.bubble_6,R.drawable.bubble_7,R.drawable.bubble_8,R.drawable.bubble_9,R.drawable.bubble_10};

    private int cats[] = {R.drawable.cat_1,R.drawable.cat_2,R.drawable.cat_3,R.drawable.cat_4,R.drawable.cat_5,R.drawable.cat_6,R.drawable.cat_7,R.drawable.cat_8};

    private int rabbits[] = {R.drawable.rabbit_1,R.drawable.rabbit_2,R.drawable.rabbit_3,R.drawable.rabbit_4,R.drawable.rabbit_5,R.drawable.rabbit_6,R.drawable.rabbit_7,R.drawable.rabbit_8};

    private int logos[] = {R.drawable.logo_1,R.drawable.logo_2,R.drawable.logo_3,R.drawable.logo_4,R.drawable.logo_5,R.drawable.logo_6,R.drawable.logo_7,R.drawable.logo_8};

    private ImageView mNoData;
    private String[] mListMoreText;

    private float mY = 0 ,mX = 0;

    private Handler mHandler;
    private LinearLayout.LayoutParams mLoadParams;
    private int persion;
    private LinearLayout mLoadLayout;
    private TextView mLoadBar;
    private TextView mLoadState;
    private ImageView mLoadReload;

    private int NowPos = 0;
    private ArrayList<FastVideoView> mVideoLists = new ArrayList<FastVideoView>();

    private Boolean mCommentCount = false;
    private int mCimmentPos = 0;

    private int mLikeCount = 0;
    private int mLikePos = 0;
    private Timer mTimer;
    private int lastCount = 0;
    private Boolean mGodHand = false;

    private NativeAdsManager manager;
    private Boolean mLoadAd = false;
    private int mAdPos = 0;
    private ArrayList<NativeAd> mNativeAds = new ArrayList<NativeAd>();

    private ImageView mFeedGroup;
    private Boolean mFeedGroupState = false;
    private PullToRefreshGridView mFeedGrid;
    private GridAdapter mGridAdapter;
    private int mGridHeight;

    private PostRetryThread mPostRetryThread;

//    @Override
//    public void onAttach(Activity activity)
//    {
//        super.onAttach(activity);
//
//        mMenuActivity = (MenuActivity) activity;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

//        Log.d("123","mFeedModels : " + mFeedModels.size());
        mApplication = (Story17Application) getActivity().getApplication();
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        initTitleBar();

        mNoData = (ImageView) getView().findViewById(R.id.nodata);
        mNoData.setVisibility(View.GONE);
        mFollowFriend = (Button) getView().findViewById(R.id.follow_friend);
        mProgress = (ProgressBar) getView().findViewById(R.id.progress);
        mListView = (PullToRefreshListView) getView().findViewById(R.id.favoriteListView);
        mFeedGrid = (PullToRefreshGridView) getView().findViewById(R.id.feed_grid);
        mFeedGrid.setVisibility(View.GONE);
        mGridHeight = (int)((float)mDisplayMetrics.widthPixels/(float)3);
        emptyFooterView = new EmptyView(getActivity());
        loadMoreView = new LoadMoreView(getActivity());

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
        {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView)
            {
                if(mLikeCount!=0)
                {
                    if(mFeedModels!=null && mApplication!=null)
                    {
                        if(mFeedModels.size() > mLikePos)
                        {
                            if(mFeedModels.get(mLikePos) !=null && mFeedModels.get(mLikePos).getPosts() !=null && mFeedModels.get(mLikePos).getPosts().getUserInfo()!=null)
                            {
                                if(mFeedModels.get(mLikePos).getPosts().getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                                {
                                    mApplication.sendLikeCountV2(getActivity(),mFeedModels.get(mLikePos).getPosts().getPostID(),mLikeCount,mGodHand);

                                    mLikePos = 0;
                                    mLikeCount = 0;
                                }
                            }
                        }
                    }
                }

                mNoData.setVisibility(View.GONE);
                ApiManager.getPostFeed(getActivity(), Integer.MAX_VALUE, 18, 1,new ApiManager.GetPostFeedCallback()
                {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedModel)
                    {

                        mListView.onRefreshComplete();

                        if(success && feedModel!=null)
                        {
                            if(feedModel.size()!=0)
                            {
                                mNoData.setVisibility(View.GONE);
                                mFollowFriend.setVisibility(View.GONE);
                                mFeedModels.clear();
                                mFeedModels.addAll(feedModel);

                                if(mListAdapter!=null) mListAdapter = null;
                                mListAdapter = new listAdapter();

                                mListView.setAdapter(mListAdapter);

                                isFetchingData = false;
                                noMoreData = false;

                                if(mFeedModels.size() < 18)
                                {
                                    noMoreData = true;
                                }
                            }
                            else
                            {
                                mNoData.setVisibility(View.GONE);
                                mFollowFriend.setVisibility(View.VISIBLE);
                                mFollowFriend.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Intent intent = new Intent();
                                        intent.setClass(getActivity(),FollowFriendActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                });
                            }
                        }
                        else
                        {
                            if(mFeedModels.size()==0) mNoData.setVisibility(View.VISIBLE);

                            if(!isAdded()) {
                                return;
                            }

                            try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e){
                            }
                        }
                    }
                });
            }
        });

        mFeedGrid.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshBase refreshView) {
                if (mLikeCount != 0) {
                    if (mFeedModels != null && mApplication != null) {
                        if (mFeedModels.size() > mLikePos) {
                            if (mFeedModels.get(mLikePos) != null && mFeedModels.get(mLikePos).getPosts() != null && mFeedModels.get(mLikePos).getPosts().getUserInfo() != null) {
                                if (mFeedModels.get(mLikePos).getPosts().getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                                    mApplication.sendLikeCountV2(getActivity(), mFeedModels.get(mLikePos).getPosts().getPostID(), mLikeCount, mGodHand);

                                    mLikePos = 0;
                                    mLikeCount = 0;
                                }
                            }
                        }
                    }
                }

                mNoData.setVisibility(View.GONE);
                ApiManager.getPostFeed(getActivity(), Integer.MAX_VALUE, 18, 1, new ApiManager.GetPostFeedCallback() {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedModel) {

                        mFeedGrid.onRefreshComplete();

                        if (success && feedModel != null) {
                            if (feedModel.size() != 0) {
                                mNoData.setVisibility(View.GONE);
                                mFollowFriend.setVisibility(View.GONE);
                                mFeedModels.clear();
                                mFeedModels.addAll(feedModel);

                                if (mGridAdapter != null) mGridAdapter = null;
                                mGridAdapter = new GridAdapter();

                                mFeedGrid.setAdapter(mGridAdapter);

                                isFetchingData = false;
                                noMoreData = false;

                                if (mFeedModels.size() < 18) {
                                    noMoreData = true;
                                }
                            } else {
                                mNoData.setVisibility(View.GONE);
                                mFollowFriend.setVisibility(View.VISIBLE);
                                mFollowFriend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent();
                                        intent.setClass(getActivity(), FollowFriendActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                });
                            }
                        } else {
                            if (mFeedModels.size() == 0)
                                mNoData.setVisibility(View.VISIBLE);

                            if (!isAdded()) {
                                return;
                            }

                            try {
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                            }
                        }
                    }
                });
            }
        });

        mListView.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {
                if (scrollState == SCROLL_STATE_FLING)
                {
                    ImageLoader.getInstance().pause();
                }

                if (scrollState == SCROLL_STATE_IDLE)
                {
                    ImageLoader.getInstance().resume();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {

            }
        });

        if(mFeedModels.size()==0)
        {
            mProgress.setVisibility(View.VISIBLE);
            ApiManager.getPostFeed(getActivity(), Integer.MAX_VALUE, 18, 1,new ApiManager.GetPostFeedCallback()
            {
                @Override
                public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedModel)
                {
//                    mMenuActivity.setHomeLoadding(false);
                    mProgress.setVisibility(View.GONE);

                    if(success && feedModel!=null)
                    {
                        if(feedModel.size()!=0)
                        {
                            mFollowFriend.setVisibility(View.GONE);
                            mFeedModels.clear();
                            mFeedModels.addAll(feedModel);
                            mListAdapter = new listAdapter();
                            mListView.setAdapter(mListAdapter);

                            if(mFeedModels.size() < 18)
                            {
                                noMoreData = true;
                            }
                        }
                        else
                        {
                            mFollowFriend.setVisibility(View.VISIBLE);
                            mFollowFriend.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(),FollowFriendActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            });
                        }
                    }
                    else
                    {
                        try
                        {
                            mNoData.setVisibility(View.VISIBLE);

                            if(getActivity()!=null)
                            {
                                try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                    Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                }

                                ApiManager.getPostFeed(getActivity(), Integer.MAX_VALUE, 18, 1,new ApiManager.GetPostFeedCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedModel)
                                    {

                                        if (success && feedModel != null)
                                        {
                                            if (feedModel.size() != 0)
                                            {
                                                mNoData.setVisibility(View.GONE);
                                                mFollowFriend.setVisibility(View.GONE);
                                                mFeedModels.clear();
                                                mFeedModels.addAll(feedModel);
                                                mListAdapter = new listAdapter();
                                                mListView.setAdapter(mListAdapter);

                                                if (mFeedModels.size() < 18) {
                                                    noMoreData = true;
                                                }
                                            } else {
                                                mNoData.setVisibility(View.GONE);
                                                mFollowFriend.setVisibility(View.VISIBLE);
                                                mFollowFriend.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent();
                                                        intent.setClass(getActivity(), FollowFriendActivity.class);
                                                        startActivity(intent);
                                                        getActivity().finish();
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        catch(Exception e)
                        {

                        }
                    }
                }
            });
        }
        else
        {
            mProgress.setVisibility(View.GONE);
            mFollowFriend.setVisibility(View.GONE);

            if(mFeedGroupState)
            {
                mFeedGroup.setImageResource(R.drawable.profile_list_white);
                mFeedGrid.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
            }
            else
            {
                mFeedGroup.setImageResource(R.drawable.profile_grid_white);
                mFeedGrid.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
            }

            mListAdapter = new listAdapter();
            mListView.setAdapter(mListAdapter);

            mGridAdapter = new GridAdapter();
            mFeedGrid.setAdapter(mGridAdapter);

            if(mFeedModels.size() < 18)
            {
                noMoreData = true;
            }
        }

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        GroupOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        mBear = (RelativeLayout) getView().findViewById(R.id.bear);

//        if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1 && mNativeAds.size()==0) getManager();

//        ((MenuActivity) getActivity()).setListView(mListView);

        LogEventUtil.EnterFeedPage(getActivity(),mApplication);
    }

    private void getManager()
    {
        manager = new NativeAdsManager(getActivity(), Constants.AD_FEED_ID, 10);
        manager.setListener(new NativeAdsManager.Listener()
        {
            @Override
            public void onAdsLoaded()
            {
                mLoadAd = true;
                mNativeAds.clear();

                for(int i = 0 ; i < manager.getUniqueNativeAdCount() ; i++)
                {
                    mNativeAds.add(manager.nextNativeAd());
                }
            }

            @Override
            public void onAdError(AdError adError)
            {

            }
        });

        manager.loadAds(NativeAd.MediaCacheFlag.ALL);
    }

    private void initTitleBar()
    {
        ((RelativeLayout) getView().findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) getView().findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.menu_home));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) getView().findViewById(R.id.img_left);
        img.setVisibility(View.VISIBLE);
        img.setImageResource(R.drawable.btn_addfriend_selector);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), AddFriendActivity.class);
                startActivity(intent);
            }
        });

        ImageView leader = (ImageView) getView().findViewById(R.id.img_right);
        leader.setVisibility(View.VISIBLE);
        leader.setImageResource(R.drawable.btn_leaderboard_selector);
        leader.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),LeaderboardActivity.class);
                startActivity(intent);
            }
        });

        mFeedGroup = (ImageView) getView().findViewById(R.id.feed_group);
        mFeedGroup.setVisibility(View.VISIBLE);
        mFeedGroup.setImageResource(R.drawable.profile_grid_white);
        mFeedGroup.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mFeedGroupState)
                {
                    mFeedGroupState = false;
                    mFeedGroup.setImageResource(R.drawable.profile_grid_white);
                    mFeedGrid.setVisibility(View.GONE);
                    mListView.setVisibility(View.VISIBLE);
                    if(mListAdapter!=null) mListAdapter.notifyDataSetChanged();
                }
                else
                {
                    mFeedGroupState = true;
                    mFeedGroup.setImageResource(R.drawable.profile_list_white);

                    if(mGridAdapter==null)
                    {
                        mGridAdapter = new GridAdapter();
                        mFeedGrid.setAdapter(mGridAdapter);
                    }
                    else mGridAdapter.notifyDataSetChanged();

                    mFeedGrid.setVisibility(View.VISIBLE);
                    mListView.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if(mBear!=null) mBear.removeAllViews();

        if(mVideoLists!=null && mVideoLists.size()!=0)
        {
            for(int i = 0 ; i < mVideoLists.size() ; i++)
            {
                if(mVideoLists.get(i).isPlaying())
                {
                    mVideoLists.get(i).stopPlayback();
                }
            }
        }

        if(mLikeCount!=0)
        {
            if(mFeedModels!=null && mApplication!=null)
            {
                if(mFeedModels.size() > mLikePos)
                {
                    if(mFeedModels.get(mLikePos) !=null && mFeedModels.get(mLikePos).getPosts() !=null && mFeedModels.get(mLikePos).getPosts().getUserInfo()!=null)
                    {
                        if(mFeedModels.get(mLikePos).getPosts().getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                        {
                            mApplication.sendLikeCountV2(getActivity(),mFeedModels.get(mLikePos).getPosts().getPostID(),mLikeCount,mGodHand);

                            mLikePos = 0;
                            mLikeCount = 0;
                        }
                    }
                }
            }
        }

        if(mTimer!=null)
        {
            mLikeCount = 0;
            mLikePos = 0;
            lastCount = 0;
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }

        MobclickAgent.onPageEnd("HomeFragment");

        LogEventUtil.LeaveFeedPage(getActivity(), mApplication, 100, 30, 2, 2);
    }

    private class listAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.titleCell = (LinearLayout) convertView.findViewById(R.id.titleCell);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.restream_open = (Button) convertView.findViewById(R.id.restream_open);

                holder.view_layout = (LinearLayout) convertView.findViewById(R.id.view_layout);
                holder.ad_layout = (LinearLayout) convertView.findViewById(R.id.ad_layout);

                holder.ad_btn = (Button) convertView.findViewById(R.id.ad_btn);
                holder.social = (TextView) convertView.findViewById(R.id.social);
                holder.live_more = (ImageView) convertView.findViewById(R.id.live_more);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            if(mFeedModels.get(position).getType()==1)
            {
                holder.view_layout.setVisibility(View.GONE);
                holder.ad_layout.setVisibility(View.VISIBLE);

                ImageLoader.getInstance().displayImage(mFeedModels.get(position).getNativeAd().getAdIcon().getUrl(), holder.self, SelfOptions);
                ImageLoader.getInstance().displayImage(mFeedModels.get(position).getNativeAd().getAdCoverImage().getUrl(), holder.photo, BigOptions);
                holder.photo.setOnClickListener(null);
                holder.photo.setOnTouchListener(null);
                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = (mDisplayMetrics.widthPixels/2) ;
                holder.name.setText(mFeedModels.get(position).getNativeAd().getAdTitle());
                holder.day.setText(mFeedModels.get(position).getNativeAd().getAdSubtitle());
                holder.dio.setText(mFeedModels.get(position).getNativeAd().getAdBody());
                holder.down_layout.setVisibility(View.GONE);
                holder.verifie.setVisibility(View.GONE);

                holder.name.setOnClickListener(null);
                holder.self.setOnClickListener(null);

                holder.ad_btn.setText(mFeedModels.get(position).getNativeAd().getAdCallToAction());
                holder.social.setVisibility(View.VISIBLE);
                holder.social.setText(mFeedModels.get(position).getNativeAd().getAdSocialContext());

                mFeedModels.get(position).getNativeAd().registerViewForInteraction(convertView);

                return convertView;
            }

            holder.social.setVisibility(View.GONE);
            if(mFeedModels.get(position).getLiveStreams()!=null)
            {
                holder.view_layout.setVisibility(View.VISIBLE);
                holder.ad_layout.setVisibility(View.GONE);
                holder.name.setText(mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID());
                holder.day.setText(Singleton.getElapsedTimeString(mFeedModels.get(position).getLiveStreams().getBeginTime()));

                holder.view_text.setText(mFeedModels.get(position).getLiveStreams().getLiveViewerCount() + " " + getString(R.string.live_viewing));
                holder.view_text.setVisibility(View.VISIBLE);
                holder.money_text.setVisibility(View.GONE);

                if(mFeedModels.get(position).getLiveStreams().getCaption().length()!=0)
                {
                    holder.dio.setVisibility(View.VISIBLE);
                    holder.dio.setText(mFeedModels.get(position).getLiveStreams().getCaption());
                }
                else holder.dio.setVisibility(View.INVISIBLE);

                holder.live_more.setVisibility(View.VISIBLE);
                holder.live_more.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        String[] items = {getString(R.string.live_share),getString(R.string.report_inappropriate)};
                        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.more)).setItems(items, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(which == 0)
                                {
                                    mProgress.setVisibility(View.VISIBLE);
                                    Branch.getInstance(getActivity()).getContentUrl("facebook", BranchShareLiveData(mFeedModels.get(position).getLiveStreams().getLiveStreamID(),mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID(),mFeedModels.get(position).getLiveStreams().getCaption(),mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture()), new Branch.BranchLinkCreateListener()
                                    {
                                        @Override
                                        public void onLinkCreate(String url, BranchError error)
                                        {
                                            mProgress.setVisibility(View.GONE);
                                            String mTitle = String.format(getString(R.string.live_relive2), mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID());
                                            String mDescription = mTitle + "!" + mFeedModels.get(position).getLiveStreams().getCaption() + "  " + url;

                                            DownloadTask mDownloadTask = new DownloadTask();
                                            mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture());
                                        }
                                    });
                                }
                                else if(which == 1)
                                {
                                    final String[] report = {getString(R.string.live_report_message1),getString(R.string.live_report_message2),getString(R.string.live_report_message3),getString(R.string.live_report_message4)};
                                    new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.report_reason)).setItems(report, new DialogInterface.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which)
                                        {
                                            try {
                                                LogEventUtil.ReportLive(getActivity(), mApplication);
                                            }
                                            catch (Exception x)
                                            {

                                            }

                                            ApiManager.reportLiveAction(getActivity(), mFeedModels.get(position).getLiveStreams().getUserInfo().getUserID(), mFeedModels.get(position).getLiveStreams().getLiveStreamID(), report[which], new ApiManager.RequestCallback() {
                                                @Override
                                                public void onResult(boolean success) {
                                                    if (success) {
                                                        try {
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.done));
                                                            Toast.makeText(getActivity(), getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                        } catch (Exception e) {
                                                        }
                                                    } else {
                                                        try {
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.error_failed));
                                                            Toast.makeText(getActivity(), getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                        } catch (Exception e) {
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    })
                                    .show();
                                }
                            }
                        }).show();
                    }
                });

                holder.down_layout.setVisibility(View.GONE);
                holder.live.setVisibility(View.VISIBLE);

                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture()), holder.self, SelfOptions);
                holder.titleCell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mFeedModels.get(position).getLiveStreams().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), HomeUserActivity.class);
                            intent.putExtra("title", mFeedModels.get(position).getLiveStreams().getUserInfo().getName());
                            intent.putExtra("picture", mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mFeedModels.get(position).getLiveStreams().getUserInfo().getIsFollowing());
                            intent.putExtra("post", mFeedModels.get(position).getLiveStreams().getUserInfo().getPostCount());
                            intent.putExtra("follow", mFeedModels.get(position).getLiveStreams().getUserInfo().getFollowerCount());
                            intent.putExtra("following", mFeedModels.get(position).getLiveStreams().getUserInfo().getFollowingCount());
                            intent.putExtra("open", mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID());
                            intent.putExtra("bio", mFeedModels.get(position).getLiveStreams().getUserInfo().getBio());
                            intent.putExtra("targetUserID", mFeedModels.get(position).getLiveStreams().getUserInfo().getUserID());
                            intent.putExtra("web", mFeedModels.get(position).getLiveStreams().getUserInfo().getWebsite());
                            intent.putExtra("live", true);

                            intent.putExtra("live_id", mFeedModels.get(position).getLiveStreams().getLiveStreamID());
                            intent.putExtra("live_openid", mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID());
                            intent.putExtra("live_caption", mFeedModels.get(position).getLiveStreams().getCaption());
                            intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture());
                            startActivity(intent);
                        }
                    }
                });

                holder.name.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(mFeedModels.get(position).getLiveStreams().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), HomeUserActivity.class);
                            intent.putExtra("title", mFeedModels.get(position).getLiveStreams().getUserInfo().getName());
                            intent.putExtra("picture", mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mFeedModels.get(position).getLiveStreams().getUserInfo().getIsFollowing());
                            intent.putExtra("post", mFeedModels.get(position).getLiveStreams().getUserInfo().getPostCount());
                            intent.putExtra("follow", mFeedModels.get(position).getLiveStreams().getUserInfo().getFollowerCount());
                            intent.putExtra("following", mFeedModels.get(position).getLiveStreams().getUserInfo().getFollowingCount());
                            intent.putExtra("open", mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID());
                            intent.putExtra("bio", mFeedModels.get(position).getLiveStreams().getUserInfo().getBio());
                            intent.putExtra("targetUserID", mFeedModels.get(position).getLiveStreams().getUserInfo().getUserID());
                            intent.putExtra("web", mFeedModels.get(position).getLiveStreams().getUserInfo().getWebsite());
                            intent.putExtra("live", true);

                            intent.putExtra("live_id", mFeedModels.get(position).getLiveStreams().getLiveStreamID());
                            intent.putExtra("live_openid", mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID());
                            intent.putExtra("live_caption", mFeedModels.get(position).getLiveStreams().getCaption());
                            intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture());
                            startActivity(intent);
                        }
                    }
                });

                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.GONE);
                holder.vcon.setVisibility(View.GONE);

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1 && Singleton.preferences.getString(Constants.OPEN_ID, "").compareTo("hi.dean")==0) ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedModels.get(position).getLiveStreams().getCoverPhoto()), holder.photo,BigOptions);
                    else ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_s);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_s);
                }

                holder.photo.setOnTouchListener(null);
                holder.photo.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {

                         Intent intent = new Intent();
                         intent.setClass(getActivity(), LiveStreamActivity.class);
                         intent.putExtra("liveStreamID", mFeedModels.get(position).getLiveStreams().getLiveStreamID());
                         intent.putExtra("name", mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID());
                         intent.putExtra("caption", mFeedModels.get(position).getLiveStreams().getCaption());
                         intent.putExtra("picture", Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture());
                         intent.putExtra("user", Singleton.preferences.getString(Constants.USER_ID, ""));
                         intent.putExtra("myopen", Singleton.preferences.getString(Constants.OPEN_ID, ""));
                         //event tracking: to track what page user enter livestream
                         intent.putExtra("enterLiveFrom","HomeFragment");
                         startActivity(intent);
                    }
                });

                if(mFeedModels.get(position).getLiveStreams().getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
                else holder.verifie.setVisibility(View.GONE);

                if(mFeedModels.get(position).getLiveStreams().getRestreamerOpenID()!=null)
                {
                    if(mFeedModels.get(position).getLiveStreams().getRestreamerOpenID().length()!=0)
                    {
                        holder.restream_open.setText(mFeedModels.get(position).getLiveStreams().getRestreamerOpenID() + getString(R.string.feed_live_restream));
                        holder.restream_open.setVisibility(View.VISIBLE);
                        holder.restream_open.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {

                                Intent intent = new Intent();
                                intent.setClass(getActivity(), LiveStreamActivity.class);
                                intent.putExtra("liveStreamID", mFeedModels.get(position).getLiveStreams().getLiveStreamID());
                                intent.putExtra("name", mFeedModels.get(position).getLiveStreams().getUserInfo().getOpenID());
                                intent.putExtra("caption", mFeedModels.get(position).getLiveStreams().getCaption());
                                intent.putExtra("picture", Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture());
                                intent.putExtra("user", Singleton.preferences.getString(Constants.USER_ID, ""));
                                intent.putExtra("myopen", Singleton.preferences.getString(Constants.OPEN_ID, ""));
                                //event tracking: to track what page user enter livestream
                                intent.putExtra("enterLiveFrom","HomeFragment");
                                startActivity(intent);
                            }
                        });
                    }
                    else holder.restream_open.setVisibility(View.GONE);
                }
            }
            else
            {
                holder.view_layout.setVisibility(View.VISIBLE);
                holder.ad_layout.setVisibility(View.GONE);
                holder.restream_open.setVisibility(View.GONE);
                holder.live_more.setVisibility(View.GONE);

                if(mFeedModels.get(position).getPosts().getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
                else holder.verifie.setVisibility(View.GONE);

                holder.down_layout.setVisibility(View.VISIBLE);
                holder.live.setVisibility(View.GONE);

                holder.name.setText(mFeedModels.get(position).getPosts().getUserInfo().getOpenID());
                holder.day.setText(Singleton.getElapsedTimeString(mFeedModels.get(position).getPosts().getTimestamp()));

                if(mFeedModels.get(position).getPosts().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
                {
                    holder.money_text.setVisibility(View.VISIBLE);

                    String c = Singleton.getCurrencyType();
                    double m = (double) mFeedModels.get(position).getPosts().getTotalRevenue()*(double)Singleton.getCurrencyRate();
                    DecimalFormat df=new DecimalFormat("#.####");
                    holder.money_text.setText(df.format(m) + " " + c);
                    holder.view_text.setText(String.format(getString(R.string.home_views),String.valueOf(mFeedModels.get(position).getPosts().getViewCount())));
                    holder.view_text.setVisibility(View.VISIBLE);
                }
                else
                {
                    holder.money_text.setVisibility(View.GONE);
                    holder.view_text.setVisibility(View.GONE);
                }

                if(mFeedModels.get(position).getPosts().getCaption().length()!=0)
                {
                    holder.dio.setVisibility(View.VISIBLE);
                    holder.dio.setText(mFeedModels.get(position).getPosts().getCaption());
                }
                else holder.dio.setVisibility(View.GONE);

                holder.like_text.setText(String.format(getString(R.string.home_like),String.valueOf(mFeedModels.get(position).getPosts().getLikeCount())));
                holder.comment_text.setText(String.format(getString(R.string.home_comment), String.valueOf(mFeedModels.get(position).getPosts().getCommentCount())));

                final String thisUserId = mFeedModels.get(position).getPosts().getUserID();

                if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
                {
                    holder.like_text.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), PostLikerActivity.class);
                            intent.putExtra("post_id", mFeedModels.get(position).getPosts().getPostID());
                            intent.putExtra("user_id", mFeedModels.get(position).getPosts().getUserID());
                            startActivity(intent);
                        }
                    });
                }
                else
                {
                    holder.like_text.setOnClickListener(null);
                }

                holder.comment_text.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        mCimmentPos = position;
                        mCommentCount = true;
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeCommentActivity.class);
                        intent.putExtra("post_id", mFeedModels.get(position).getPosts().getPostID());
                        intent.putExtra("user_id", mFeedModels.get(position).getPosts().getUserID());
                        startActivity(intent);
                    }
                });

                if(mFeedModels.get(position).getPosts().getLiked()==0)
                {
                    holder.btn_like.setImageResource(R.drawable.btn_like_selector);
                }
                else holder.btn_like.setImageResource(R.drawable.like_down);

                final TextView likeText = holder.like_text;
                final ImageView like = holder.btn_like;
                if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
                {
                    holder.btn_like.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {

                            //event tracking
                            try{
                                LogEventUtil.LikePost(getActivity(),mApplication,mFeedModels.get(position).getPosts().getUserID(),mFeedModels.get(position).getPosts().getPostID());
                            }catch (Exception x)
                            {

                            }

                            Intent intent = new Intent();
                            intent.setClass(getActivity(), PostLikerActivity.class);
                            intent.putExtra("post_id", mFeedModels.get(position).getPosts().getPostID());
                            intent.putExtra("user_id", mFeedModels.get(position).getPosts().getUserID());
                            startActivity(intent);
                        }
                    });
                    holder.btn_like.setOnTouchListener(null);
                }
                else
                {
                    holder.btn_like.setOnClickListener(null);
                    holder.btn_like.setOnTouchListener(new View.OnTouchListener()
                    {
                        @Override
                        public boolean onTouch(View v, MotionEvent event)
                        {

                            //event tracking
                            try{
                                LogEventUtil.LikePost(getActivity(),mApplication,mFeedModels.get(position).getPosts().getUserID(),mFeedModels.get(position).getPosts().getPostID());
                            }catch (Exception x)
                            {

                            }

                            if (event.getAction() == MotionEvent.ACTION_DOWN)
                            {
                                mY = event.getY();
                                mX = event.getX();
                            }

                            if (event.getAction() == MotionEvent.ACTION_UP)
                            {
                                double num = Math.sqrt(Math.pow(((double)mX - (double)event.getX()), 2) + Math.pow(((double)mY - (double)event.getY()), 2));

                                if(num<30)
                                {
                                    int run = (int) (Math.random() * 10000);
                                    int img;
                                    int mW, mH;

                                    if (run < 500) {
                                        img = logos[(int) (Math.random() * logos.length)];
                                        mW = 80;
                                        mH = 80;
                                    } else if (run < 1000) {
                                        img = rabbits[(int) (Math.random() * rabbits.length)];
                                        mW = 80;
                                        mH = 80;
                                    } else if (run <2000) {
                                        img = cats[(int) (Math.random() * cats.length)];
                                        mW = 80;
                                        mH = 80;
                                    } else if (run < 3000) {
                                        img = bears[(int) (Math.random() * bears.length)];
                                        mW = 80;
                                        mH = 80;
                                    } else if (run < 4000) {
                                        img = bubbles[(int) (Math.random() * bubbles.length)];
                                        mW = 120;
                                        mH = 120;
                                    } else {
                                        img = loves[(int) (Math.random() * loves.length)];
                                        mW = 60;
                                        mH = 60;
                                    }

                                    int mS = -30 + ((int) (Math.random() * 60));
                                    int mTx = -100 + ((int) (Math.random() * 200));
                                    int mTy = 500 + ((int) (Math.random() * 1200));

                                    ImageView cat = new ImageView(getActivity());
                                    cat.setImageResource(img);
                                    cat.setVisibility(View.INVISIBLE);

                                    RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                    mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                    mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                    cat.setLayoutParams(mParams);

                                    AnimationSet mAnimationSet = new AnimationSet(true);

                                    ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                    scale.setDuration(500);

                                    AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                    alpha.setDuration(3000);

                                    RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                    rotate.setDuration(750);

                                    TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                    translate.setDuration(2000);

                                    mAnimationSet.addAnimation(scale);
                                    mAnimationSet.addAnimation(rotate);
                                    mAnimationSet.addAnimation(translate);
                                    mAnimationSet.addAnimation(alpha);
                                    cat.startAnimation(mAnimationSet);

                                    mBear.addView(cat);

                                    if(mFeedModels.get(position).getPosts().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                                    {

                                        mFeedModels.get(position).getPosts().setLikeCount(mFeedModels.get(position).getPosts().getLikeCount() + 1);
                                        likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getPosts().getLikeCount())));
                                        mFeedModels.get(position).getPosts().setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        if(mLikePos==position)
                                        {
                                            mLikeCount++;
                                        }
                                        else
                                        {
                                            if(mLikeCount!=0)
                                            {
                                                mApplication.sendLikeCountV2(getActivity(),mFeedModels.get(mLikePos).getPosts().getPostID(),mLikeCount,mGodHand);
                                            }

                                            mLikePos = position;
                                            mLikeCount = 1;
                                        }
//                                        ApiManager.likePost(getActivity(), mFeedModels.get(position).getPosts().getPostID(), new ApiManager.LikePostCallback() {
//                                            @Override
//                                            public void onResult(boolean success, String message) {
//                                                if (success) {
//
//                                                }
//                                            }
//                                        });
                                    }
                                    else
                                    {
                                        if(mFeedModels.get(position).getPosts().getLiked()!=1)
                                        {
                                            mFeedModels.get(position).getPosts().setLikeCount(mFeedModels.get(position).getPosts().getLikeCount() + 1);
                                            likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getPosts().getLikeCount())));
                                            mFeedModels.get(position).getPosts().setLiked(1);
                                            like.setImageResource(R.drawable.like_down);

                                            if(!mApplication.getIsSbtools())
                                            {
                                                ApiManager.likePost(getActivity(), mFeedModels.get(position).getPosts().getPostID(), new ApiManager.LikePostCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String message) {
                                                        if (success) {

                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }

                            return true;
                        }
                    });
                }

                holder.btn_comment.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        mCimmentPos = position;
                        mCommentCount = true;
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeCommentActivity.class);
                        intent.putExtra("post_id", mFeedModels.get(position).getPosts().getPostID());
                        intent.putExtra("user_id", mFeedModels.get(position).getPosts().getUserID());
                        startActivity(intent);
                    }
                });

                holder.btn_more.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(mListMoreText!=null) mListMoreText = null;

                        if(mFeedModels.get(position).getPosts().getLiked()==0) mListMoreText = new String[2];
                        else
                        {
                            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
                            {
                                mListMoreText = new String[2];
                            }
                            else
                            {
                                mListMoreText = new String[3];
                                mListMoreText[2] = getString(R.string.back_like);
                            }
                        }

                        if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) mListMoreText[0] = getString(R.string.post_delete);
                        else mListMoreText[0] = getString(R.string.post_block);

                        mListMoreText[1] = getString(R.string.post_share);

                        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                if(which == 0)
                                {


                                    if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
                                    {
                                        ApiManager.deletePost(getActivity(), mFeedModels.get(position).getPosts().getPostID(), new ApiManager.DeletePostCallback()
                                        {
                                            @Override
                                            public void onResult(boolean success, String message)
                                            {
                                                if (success)
                                                {
                                                    mProgress.setVisibility(View.VISIBLE);
                                                    ApiManager.getPostFeed(getActivity(), Integer.MAX_VALUE, 18, 1, new ApiManager.GetPostFeedCallback()
                                                    {
                                                        @Override
                                                        public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedModel)
                                                        {
                                                            mProgress.setVisibility(View.GONE);
                                                            if (success && feedModel != null)
                                                            {
                                                                mFeedModels.clear();
                                                                mFeedModels.addAll(feedModel);

                                                                if (mListAdapter != null) mListAdapter = null;

                                                                mListAdapter = new listAdapter();
                                                                mListView.setAdapter(mListAdapter);

                                                                isFetchingData = false;
                                                                noMoreData = false;

                                                                if (mFeedModels.size() < 18) {
                                                                    noMoreData = true;
                                                                }
                                                            } else{
                                                                try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                                                    Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                                }
                                                                catch (Exception e){
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                                else {
                                                    try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception e){
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        //檢舉
                                        ArrayList<String> strArray  = new ArrayList<String>();
                                        strArray =  Singleton.getLocalizeStringOfArray(Constants.REPORT_POST_OPTION);
                                        String[] reportString = strArray.toArray(new String[strArray.size()]);

                                        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.report_reason)).setItems(reportString, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                String reason = Singleton.getLocalizedString(Constants.REPORT_POST_OPTION.get(which));

                                                try {
                                                    LogEventUtil.ReportPost(getActivity(), mApplication);
                                                }
                                                catch (Exception x)
                                                {

                                                }


                                                ApiManager.reportPostAction(getActivity(),  mFeedModels.get(position).getPosts().getUserID(), reason, mFeedModels.get(position).getPosts().getPostID(), new ApiManager.ReportPostActionCallback() {

                                                    @Override
                                                    public void onResult(boolean success, String message) {
                                                        if(success){
                                                            try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.complete));
                                                                Toast.makeText(getActivity(), getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                                            }
                                                            catch (Exception e){
                                                            }
                                                        }else{
                                                            try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                                                Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                            }
                                                            catch (Exception e){
                                                            }
                                                        }
                                                    }
                                                });


                                            }
                                        }).show();
                                    }
                                }
                                else if(which == 1)
                                {
                                    if(mFeedModels.get(position).getPosts().getType().compareTo("image")==0)
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(getActivity()).getContentUrl("facebook", BranchSharePostData(mFeedModels.get(position).getPosts().getPostID(), R.string.share_post_image, mFeedModels.get(position).getPosts().getUserInfo().getOpenID(), mFeedModels.get(position).getPosts().getCaption(), mFeedModels.get(position).getPosts().getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mFeedModels.get(position).getPosts().getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedModels.get(position).getPosts().getCaption() + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedModels.get(position).getPosts().getPicture());
                                            }
                                        });
                                    }
                                    else
                                    {
                                        mProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(getActivity()).getContentUrl("facebook", BranchSharePostData(mFeedModels.get(position).getPosts().getPostID(),R.string.share_post_video,mFeedModels.get(position).getPosts().getUserInfo().getOpenID(),mFeedModels.get(position).getPosts().getCaption(),mFeedModels.get(position).getPosts().getPicture()), new Branch.BranchLinkCreateListener()
                                        {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error)
                                            {
                                                mProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mFeedModels.get(position).getPosts().getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedModels.get(position).getPosts().getCaption() + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedModels.get(position).getPosts().getPicture());
                                            }
                                        });
                                    }
                                }
                                else if(which == 2)
                                {
                                    if(mLikeCount!=0)
                                    {
                                        if(mFeedModels!=null && mApplication!=null)
                                        {
                                            if(mFeedModels.size() > mLikePos)
                                            {
                                                if(mFeedModels.get(mLikePos) !=null && mFeedModels.get(mLikePos).getPosts() !=null && mFeedModels.get(mLikePos).getPosts().getUserInfo()!=null)
                                                {
                                                    if(mFeedModels.get(mLikePos).getPosts().getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                                                    {
                                                        mApplication.sendLikeCountV2(getActivity(),mFeedModels.get(mLikePos).getPosts().getPostID(),mLikeCount,mGodHand);

                                                        mLikePos = 0;
                                                        mLikeCount = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    mFeedModels.get(position).getPosts().setLiked(0);
                                    like.setImageResource(R.drawable.like);
                                    ApiManager.unlikePost(getActivity(), mFeedModels.get(position).getPosts().getPostID(), new ApiManager.UnlikePostCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message)
                                        {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                            }
                        }).show();
                    }
                });


                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getPosts().getUserInfo().getPicture()), holder.self, SelfOptions);
                holder.titleCell.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mFeedModels.get(position).getPosts().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), HomeUserActivity.class);
                            intent.putExtra("title", mFeedModels.get(position).getPosts().getUserInfo().getName());
                            intent.putExtra("picture", mFeedModels.get(position).getPosts().getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mFeedModels.get(position).getPosts().getUserInfo().getIsFollowing());
                            intent.putExtra("post", mFeedModels.get(position).getPosts().getUserInfo().getPostCount());
                            intent.putExtra("follow", mFeedModels.get(position).getPosts().getUserInfo().getFollowerCount());
                            intent.putExtra("following", mFeedModels.get(position).getPosts().getUserInfo().getFollowingCount());
                            intent.putExtra("open", mFeedModels.get(position).getPosts().getUserInfo().getOpenID());
                            intent.putExtra("bio", mFeedModels.get(position).getPosts().getUserInfo().getBio());
                            intent.putExtra("targetUserID", mFeedModels.get(position).getPosts().getUserInfo().getUserID());
                            intent.putExtra("web", mFeedModels.get(position).getPosts().getUserInfo().getWebsite());
                            startActivity(intent);
                        }
                    }
                });

                holder.name.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(mFeedModels.get(position).getPosts().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                            Intent intent = new Intent();
                            intent.setClass(getActivity(), HomeUserActivity.class);
                            intent.putExtra("title", mFeedModels.get(position).getPosts().getUserInfo().getName());
                            intent.putExtra("picture", mFeedModels.get(position).getPosts().getUserInfo().getPicture());
                            intent.putExtra("isfollowing", mFeedModels.get(position).getPosts().getUserInfo().getIsFollowing());
                            intent.putExtra("post", mFeedModels.get(position).getPosts().getUserInfo().getPostCount());
                            intent.putExtra("follow", mFeedModels.get(position).getPosts().getUserInfo().getFollowerCount());
                            intent.putExtra("following", mFeedModels.get(position).getPosts().getUserInfo().getFollowingCount());
                            intent.putExtra("open", mFeedModels.get(position).getPosts().getUserInfo().getOpenID());
                            intent.putExtra("bio", mFeedModels.get(position).getPosts().getUserInfo().getBio());
                            intent.putExtra("targetUserID", mFeedModels.get(position).getPosts().getUserInfo().getUserID());
                            intent.putExtra("web", mFeedModels.get(position).getPosts().getUserInfo().getWebsite());
                            startActivity(intent);
                        }
                    }
                });

                if(NowPos >= (position+2) || NowPos <= (position-2))
                {
                    for(int p = 0 ; p < mVideoLists.size() ; p++)
                    {
                        if(mVideoLists.get(p)!=null)
                        {
                            if(mVideoLists.get(p).isPlaying())
                            {
                                mVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mVideoLists.clear();
                }

                holder.photo.setOnClickListener(null);
                if(mFeedModels.get(position).getPosts().getType().compareTo("image")==0)
                {
                    holder.photo.setVisibility(View.VISIBLE);
                    holder.video.setVisibility(View.GONE);
                    holder.vcon.setVisibility(View.GONE);

                    holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                    holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedModels.get(position).getPosts().getPicture()), holder.photo,BigOptions);
                    holder.photo.setOnTouchListener(new View.OnTouchListener()
                    {
                        @Override
                        public boolean onTouch(View v, MotionEvent event)
                        {

                            if (event.getAction() == MotionEvent.ACTION_UP) {

                                LogEventUtil.ClickInFeedPage(getActivity(), mApplication, "USER_ID", "post", "123-456-789", "", 90);

                                int run = (int) (Math.random() * 10000);
                                int img;
                                int mW, mH;

                                if (run < 500) {
                                    img = logos[(int) (Math.random() * logos.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 1000) {
                                    img = rabbits[(int) (Math.random() * rabbits.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run <2000) {
                                    img = cats[(int) (Math.random() * cats.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 3000) {
                                    img = bears[(int) (Math.random() * bears.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 4000) {
                                    img = bubbles[(int) (Math.random() * bubbles.length)];
                                    mW = 120;
                                    mH = 120;
                                } else {
                                    img = loves[(int) (Math.random() * loves.length)];
                                    mW = 60;
                                    mH = 60;
                                }

                                int mS = -30 + ((int) (Math.random() * 60));
                                int mTx = -100 + ((int) (Math.random() * 200));
                                int mTy = 500 + ((int) (Math.random() * 1200));

                                ImageView cat = new ImageView(getActivity());
                                cat.setImageResource(img);
                                cat.setVisibility(View.INVISIBLE);

                                RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                cat.setLayoutParams(mParams);

                                AnimationSet mAnimationSet = new AnimationSet(true);

                                ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                scale.setDuration(500);

                                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                alpha.setDuration(3000);

                                RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                rotate.setDuration(750);

                                TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                translate.setDuration(2000);

                                mAnimationSet.addAnimation(scale);
                                mAnimationSet.addAnimation(rotate);
                                mAnimationSet.addAnimation(translate);
                                mAnimationSet.addAnimation(alpha);
                                cat.startAnimation(mAnimationSet);

                                mBear.addView(cat);

                                if(mFeedModels.get(position).getPosts().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                                {

                                    mFeedModels.get(position).getPosts().setLikeCount(mFeedModels.get(position).getPosts().getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getPosts().getLikeCount())));
                                    mFeedModels.get(position).getPosts().setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(mLikePos==position)
                                    {
                                        mLikeCount++;
                                    }
                                    else
                                    {
                                        if(mLikeCount!=0)
                                        {
                                            mApplication.sendLikeCountV2(getActivity(),mFeedModels.get(mLikePos).getPosts().getPostID(),mLikeCount,mGodHand);
                                        }

                                        mLikePos = position;
                                        mLikeCount = 1;
                                    }
//                                    ApiManager.likePost(getActivity(), mFeedModels.get(position).getPosts().getPostID(), new ApiManager.LikePostCallback() {
//                                        @Override
//                                        public void onResult(boolean success, String message) {
//                                            if (success) {
//
//                                            }
//                                        }
//                                    });
                                }
                                else
                                {
                                    if(mFeedModels.get(position).getPosts().getLiked()!=1)
                                    {
                                        mFeedModels.get(position).getPosts().setLikeCount(mFeedModels.get(position).getPosts().getLikeCount() + 1);
                                        likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getPosts().getLikeCount())));
                                        mFeedModels.get(position).getPosts().setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        if(!mApplication.getIsSbtools())
                                        {
                                            ApiManager.likePost(getActivity(), mFeedModels.get(position).getPosts().getPostID(), new ApiManager.LikePostCallback() {
                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }


                            return true;
                        }
                    });
                }
                else
                {
                    NowPos = position;
                    mVideoLists.add(holder.video);

                    holder.photo.setVisibility(View.VISIBLE);
                    holder.video.setVisibility(View.VISIBLE);
                    holder.vcon.setVisibility(View.VISIBLE);

                    holder.video.getLayoutParams().width = mDisplayMetrics.widthPixels;
                    holder.video.getLayoutParams().height = mDisplayMetrics.widthPixels ;
                    try {
                        holder.video.setVideoPath(Singleton.getS3FileUrl(mFeedModels.get(position).getPosts().getVideo()));
                    }
                    catch (Exception e) {
                    }

                    holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                    holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedModels.get(position).getPosts().getPicture()), holder.photo,BigOptions);
                    final ImageView mPhotoV = holder.photo;
                    holder.video.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
                    {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer)
                        {
                            mPhotoV.setVisibility(View.GONE);
                        }
                    });

                    final FastVideoView mVideoV = holder.video;
                    holder.video.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
                    {
                        public void onCompletion(MediaPlayer mp)
                        {
                            mVideoV.start();
                        }
                    });

                    holder.video.start();

                    holder.video.setOnTouchListener(new View.OnTouchListener()
                    {
                        @Override
                        public boolean onTouch(View v, MotionEvent event)
                        {

                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                int run = (int) (Math.random() * 10000);
                                int img;
                                int mW, mH;

                                if (run < 500) {
                                    img = logos[(int) (Math.random() * logos.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 1000) {
                                    img = rabbits[(int) (Math.random() * rabbits.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run <2000) {
                                    img = cats[(int) (Math.random() * cats.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 3000) {
                                    img = bears[(int) (Math.random() * bears.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 4000) {
                                    img = bubbles[(int) (Math.random() * bubbles.length)];
                                    mW = 120;
                                    mH = 120;
                                } else {
                                    img = loves[(int) (Math.random() * loves.length)];
                                    mW = 60;
                                    mH = 60;
                                }

                                int mS = -30 + ((int) (Math.random() * 60));
                                int mTx = -100 + ((int) (Math.random() * 200));
                                int mTy = 500 + ((int) (Math.random() * 1200));

                                ImageView cat = new ImageView(getActivity());
                                cat.setImageResource(img);
                                cat.setVisibility(View.INVISIBLE);

                                RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                cat.setLayoutParams(mParams);

                                AnimationSet mAnimationSet = new AnimationSet(true);

                                ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                scale.setDuration(500);

                                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                alpha.setDuration(3000);

                                RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                rotate.setDuration(750);

                                TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                translate.setDuration(2000);

                                mAnimationSet.addAnimation(scale);
                                mAnimationSet.addAnimation(rotate);
                                mAnimationSet.addAnimation(translate);
                                mAnimationSet.addAnimation(alpha);
                                cat.startAnimation(mAnimationSet);

                                mBear.addView(cat);

                                if(mFeedModels.get(position).getPosts().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                                {

                                    mFeedModels.get(position).getPosts().setLikeCount(mFeedModels.get(position).getPosts().getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getPosts().getLikeCount())));
                                    mFeedModels.get(position).getPosts().setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(mLikePos==position)
                                    {
                                        mLikeCount++;
                                    }
                                    else
                                    {
                                        if(mLikeCount!=0)
                                        {
                                            mApplication.sendLikeCountV2(getActivity(),mFeedModels.get(mLikePos).getPosts().getPostID(),mLikeCount,mGodHand);
                                        }

                                        mLikePos = position;
                                        mLikeCount = 1;
                                    }
//                                    ApiManager.likePost(getActivity(), mFeedModels.get(position).getPosts().getPostID(), new ApiManager.LikePostCallback() {
//                                        @Override
//                                        public void onResult(boolean success, String message) {
//                                            if (success) {
//
//                                            }
//                                        }
//                                    });
                                }
                                else
                                {
                                    if(mFeedModels.get(position).getPosts().getLiked()!=1)
                                    {
                                        mFeedModels.get(position).getPosts().setLikeCount(mFeedModels.get(position).getPosts().getLikeCount() + 1);
                                        likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getPosts().getLikeCount())));
                                        mFeedModels.get(position).getPosts().setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        if(!mApplication.getIsSbtools())
                                        {
                                            ApiManager.likePost(getActivity(), mFeedModels.get(position).getPosts().getPostID(), new ApiManager.LikePostCallback() {
                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }


                            return true;
                        }
                    });
                }
            }

            holder.dio.linkify(new FeedTagActionHandler()
            {
                @Override
                public void handleHashtag(String hashtag)
                {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention)
                {
                    mProgress.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(getActivity(), mention, new ApiManager.GetUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, UserModel user)
                        {
                            mProgress.setVisibility(View.GONE);
                            if (success && user != null)
                            {
                                Intent intent = new Intent();
                                intent.setClass(getActivity(), HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                startActivity(intent);
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email)
                {

                }

                @Override
                public void handleUrl(String url)
                {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.open_uri_error));
                            Toast.makeText(getActivity(), getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    };

    private JSONObject BranchShareLiveData(int liveStreamID, String OpenID, String caption, String Picture)
    {
        JSONObject mShareData = new JSONObject();
        try
        {
            mShareData.put("page", "live");
            mShareData.put("ID", liveStreamID);
            mShareData.put("$og_title", String.format(getString(R.string.live_relive2), OpenID));
            mShareData.put("$og_description", caption);
            mShareData.put("$og_image_url", Singleton.getS3FileUrl(Picture));
            mShareData.put("$og_type", "website");
            mShareData.put("$fallback_url", Constants.MEDIA17_WEBSITE_SHARE + "live/" + liveStreamID);
        }
        catch (JSONException ex)
        {
        }

        return mShareData;
    }

    private JSONObject BranchSharePostData(String PostID, int PorV, String OpenID, String caption, String Picture)
    {
        JSONObject mShareData = new JSONObject();
        try
        {
            mShareData.put("page", "p");
            mShareData.put("ID", PostID);
            mShareData.put("$og_title", String.format(getString(PorV), OpenID));
            mShareData.put("$og_description", caption);
            mShareData.put("$og_image_url", Singleton.getS3FileUrl(Picture));
            mShareData.put("$og_type", "website");
            mShareData.put("$fallback_url", Constants.MEDIA17_WEBSITE_SHARE + "p/" + PostID);
        }
        catch (JSONException ex)
        {
        }

        return mShareData;
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private void shareTo(String subject, String body, String chooserTitle) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    private void shareTo(String subject, String body, String chooserTitle, String pic)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
//        String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
//        File imageFileToShare = new File(imagePath);
//        Uri uri = Uri.fromFile(imageFileToShare);
//        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    private class DownloadTask extends AsyncTask<String, Integer, Boolean>
    {
        private String subject = "";
        private String body = "";
        private String chooserTitle = "";
        private String uri = "";

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            subject = text[0];
            body = text[1];
            chooserTitle = text[2];
            uri = text[3];

            try
            {
                URL url = new URL(Singleton.getS3FileUrl(uri));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "17app.jpg";
                FileOutputStream outStream = new FileOutputStream(file);
                BitmapFactory.decodeStream(input).compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.close();
                return true;
            }
            catch (IOException e)
            {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);
            mProgress.setVisibility(View.GONE);
            if(result) shareTo(subject,body,chooserTitle,uri);
            else {
                try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.error_failed));
                    Toast.makeText(getActivity(), getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                }
                catch (Exception x){
                }
            }
        }
    }

    private class ViewHolder
    {
        LinearLayout titleCell;

        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
        Button restream_open;
        LinearLayout view_layout;
        LinearLayout ad_layout;

        Button ad_btn;
        TextView social;

        ImageView live_more;
    }

    public View getFooterView()
    {
        if (noMoreData ==true)
        {
            return emptyFooterView;
        }
        else
        {
            return loadMoreView;
        }
    }

    public void LoadData(final boolean refresh)
    {
        if(isFetchingData)
        {
            return;
        }

        if(refresh)
        {
            noMoreData = false;
        }

        if(noMoreData)
        {
            return;
        }

        isFetchingData = true;

        ApiManager.getPostFeed(getActivity(), mFeedModels.get(mFeedModels.size()-1).getPosts().getTimestamp(), 18, 0,new ApiManager.GetPostFeedCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedModel)
            {
                isFetchingData = false;

                if (success && feedModel!=null)
                {
                    if (feedModel.size() != 0)
                    {
                        if(Singleton.preferences.getInt(Constants.SHOW_AD, 0)==1)
                        {
                            if(mLoadAd && mNativeAds!=null && mNativeAds.size()!=0)
                            {
                                if(mAdPos >= mNativeAds.size()) mAdPos = 0;
                                LiveFeedModel mLiveFeedModel = new LiveFeedModel();
                                mLiveFeedModel.setType(1);
                                mLiveFeedModel.setNativeAd(mNativeAds.get(mAdPos));
                                mFeedModels.add(mLiveFeedModel);

                                mAdPos++;
                            }
                        }

                        mFeedModels.addAll(feedModel);

                        if (feedModel.size() < 18)
                        {
                            noMoreData = true;
                        }

                        if(mListAdapter!=null) mListAdapter.notifyDataSetChanged();
                        if(mGridAdapter!=null) mGridAdapter.notifyDataSetChanged();
                    }
                } else {
                    try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mLikeCount - lastCount > 17) {
                    mGodHand = true;
                }

                lastCount = mLikeCount;
            }
        }, 0, 1000);

        if(mApplication==null) mApplication = (Story17Application) getActivity().getApplication();

        if(mApplication.getshowToast())
        {
            try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.liveend));
                Toast.makeText(getActivity(), getString(R.string.liveend), Toast.LENGTH_SHORT).show();
            }
            catch (Exception x){
            }
            mApplication.setshowToast(false);
        }

        if(mApplication.getShowLoader())
        {
            try{
                mFeedGroupState = false;
                mFeedGroup.setImageResource(R.drawable.profile_grid_white);
                mFeedGrid.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
                if(mListAdapter!=null) mListAdapter.notifyDataSetChanged();
            }
            catch (Exception e){
            }

            mLoadLayout = (LinearLayout) getView().findViewById(R.id.load_layout);
            mLoadLayout.setVisibility(View.VISIBLE);

            ImageView mLoadImg = (ImageView) getView().findViewById(R.id.load_img);
            mLoadState = (TextView) getView().findViewById(R.id.load_state);
            TextView mLoadCaption = (TextView) getView().findViewById(R.id.load_caption);
            mLoadReload = (ImageView) getView().findViewById(R.id.load_reload);
            ImageView mLoadCancel = (ImageView) getView().findViewById(R.id.load_cancel);
            mLoadBar = (TextView) getView().findViewById(R.id.load_bar);

            mLoadImg.setImageBitmap(BitmapHelper.getBitmap(Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + Constants.THUMBNAIL_PREFIX + getConfig(Constants.UPLOAD_PHOTO), 1));
            mLoadCaption.setText(getConfig(Constants.UPLOAD_CAPTION));

            if(mApplication.getUpLoaderState())
            {
                if(mApplication.getEncodeState()) mLoadState.setText(getString(R.string.ready));
                else mLoadState.setText(getString(R.string.uploading));

                mLoadReload.setVisibility(View.GONE);

                mLoadReload.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {

                    }
                });

                mLoadCancel.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        mApplication.setShowLoader(false);
                        mLoadLayout.setVisibility(View.GONE);
                        mHandler.removeCallbacks(LoadRun);
                        mHandler = null;
                    }
                });

                persion = (int)((double)mDisplayMetrics.widthPixels / (double)100);

                if(mLoadParams!=null) mLoadParams = null;
                mLoadParams = new LinearLayout.LayoutParams(mApplication.getUpLoader() * persion,5);
                mLoadBar.setLayoutParams(mLoadParams);
                mLoadBar.setBackgroundColor(getResources().getColor(R.color.main_color));

                if(mHandler==null) mHandler = new Handler();
                mHandler.postDelayed(LoadRun,100);
            }
            else
            {
                mLoadState.setText(getString(R.string.upload_error));
                mLoadReload.setVisibility(View.VISIBLE);

                persion = (int)((double)mDisplayMetrics.widthPixels / (double)100);
                if(mLoadParams!=null) mLoadParams = null;
                mLoadParams = new LinearLayout.LayoutParams(100 * persion,5);
                mLoadBar.setLayoutParams(mLoadParams);
                mLoadBar.setBackgroundColor(Color.parseColor("#F07D78"));

                mLoadReload.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        mLoadReload.setVisibility(View.GONE);

                        mApplication.setLoader(0);
                        mApplication.setUpLoaderState(true);

                        mLoadBar.getLayoutParams().width = mApplication.getUpLoader() * persion;
                        mLoadBar.setBackgroundColor(getResources().getColor(R.color.main_color));

                        mApplication.setEncodeState(false);

                        ArrayList<String> filesToUpload = new ArrayList<String>();
                        filesToUpload.add(getConfig(Constants.UPLOAD_PHOTO));
                        filesToUpload.add("THUMBNAIL_" + getConfig(Constants.UPLOAD_PHOTO));
                        mApplication.setUpLoad(getActivity(), filesToUpload);

                        if(mHandler==null) mHandler = new Handler();
                        mHandler.postDelayed(LoadRun,100);
                    }
                });

                mLoadCancel.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        mApplication.setShowLoader(false);
                        mLoadLayout.setVisibility(View.GONE);
                    }
                });
            }
        }
        else
        {
            if(((MenuActivity)getActivity()).getHomeLoadding())
            {
//                mProgress.setVisibility(View.VISIBLE);

                if(!isAdded()) {
                    return;
                }

                ApiManager.getPostFeed(getActivity(), Integer.MAX_VALUE, 18, 1,new ApiManager.GetPostFeedCallback()
                {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedModel)
                    {
                        mProgress.setVisibility(View.GONE);
                        if (success && feedModel != null)
                        {
                            if (feedModel.size() != 0)
                            {
                                mFeedModels.clear();
                                mFeedModels.addAll(feedModel);

                                if (mListAdapter != null) mListAdapter = null;
                                mListAdapter = new listAdapter();

                                mListView.setAdapter(mListAdapter);

                                isFetchingData = false;
                                noMoreData = false;

                                if(mFeedModels.size() < 18)
                                {
                                    noMoreData = true;
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                if(!isAdded()) {
                                    return;
                                }
                                try{
//                              ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                    Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                });

                ((MenuActivity)getActivity()).setHomeLoadding(false);
            }
        }

        if(mCommentCount)
        {
            mCommentCount = false;

            mFeedModels.get(mCimmentPos).getPosts().setCommentCount(mApplication.getCommentCount());
            if(mListAdapter != null){
                mListAdapter.notifyDataSetChanged();
            }
        }

        MobclickAgent.onPageStart("HomeFragment");
    }

    private Runnable LoadRun = new Runnable()
    {
        @Override
        public void run()
        {
            if(!isAdded()) {
                return;
            }

            if(mApplication.getEncodeState()) mLoadState.setText(getString(R.string.ready));
            else mLoadState.setText(getString(R.string.uploading));

            if(mLoadParams!=null) mLoadParams = null;
            mLoadParams = new LinearLayout.LayoutParams(mApplication.getUpLoader() * persion,5);
            mLoadBar.setLayoutParams(mLoadParams);

            if(!mApplication.getUpLoaderState())
            {
                if(mLoadParams!=null) mLoadParams = null;
                mLoadParams = new LinearLayout.LayoutParams(100 * persion,5);
                mLoadBar.setLayoutParams(mLoadParams);
                mLoadBar.setBackgroundColor(Color.parseColor("#F07D78"));

                mLoadState.setText(getString(R.string.upload_error));
                mLoadReload.setVisibility(View.VISIBLE);
                return ;
            }

            if(mApplication.getUpLoader() < 100){
                mHandler.postDelayed(LoadRun,100);
            } else{

                if(!isAdded()) {
                    return;
                }

                mApplication.setShowLoader(false);
                mLoadLayout.setVisibility(View.GONE);

                mProgress.setVisibility(View.VISIBLE);

                if(mPostRetryThread != null){
                    mPostRetryThread.destroyProcess();
                }

                mPostRetryThread = new PostRetryThread();
                mPostRetryThread.start();
            }
        }
    };

    private void executePostFinish(){

        if (!isAdded()) {
            return;
        }

        ApiManager.getPostFeed(getActivity(), Integer.MAX_VALUE, 18, 1, new ApiManager.GetPostFeedCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<LiveFeedModel> feedModel) {
                mProgress.setVisibility(View.GONE);
                if (success && feedModel != null) {
                    if (!isAdded()) {
                        return;
                    }

                    mFeedModels.clear();
                    mFeedModels.addAll(feedModel);

                    if (mListAdapter != null) mListAdapter = null;
                    mListAdapter = new listAdapter();

                    mListView.setAdapter(mListAdapter);

                    isFetchingData = false;
                    noMoreData = false;

                    if (mFeedModels.size() < 18) {
                        noMoreData = true;
                    }
                } else {
                    try {
                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    } catch (Exception x) {
                    }
                }
            }
        });
    }

    public String getConfig(String key)
    {
        SharedPreferences settings = getActivity().getSharedPreferences("settings",0);
        return settings.getString(key, "");
    }

    private class GridAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewGroupHolder holder = new ViewGroupHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.grid_row, null);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.video = (ImageView) convertView.findViewById(R.id.video);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                convertView.setTag(holder);
            }
            else holder = (ViewGroupHolder) convertView.getTag();

            holder.image.getLayoutParams().width = mGridHeight;
            holder.image.getLayoutParams().height = mGridHeight;

            final int pos = position + 1;

            if(mFeedModels.get(position).getLiveStreams()!=null)
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getLiveStreams().getUserInfo().getPicture()), holder.image, GroupOptions);
                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFeedGroupState = false;
                        mFeedGroup.setImageResource(R.drawable.profile_grid_white);
                        mFeedGrid.setVisibility(View.GONE);
                        mListView.setVisibility(View.VISIBLE);
                        if(mListAdapter != null){
                            mListAdapter.notifyDataSetChanged();
                        }
                        mListView.getRefreshableView().setSelection(pos);

                    }
                });

                holder.live.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.GONE);
            }
            else
            {
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getPosts().getPicture()), holder.image, GroupOptions);
                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFeedGroupState = false;
                        mFeedGroup.setImageResource(R.drawable.profile_grid_white);
                        mFeedGrid.setVisibility(View.GONE);
                        mListView.setVisibility(View.VISIBLE);
                        if(mListAdapter != null){
                            mListAdapter.notifyDataSetChanged();
                        }
                        mListView.getRefreshableView().setSelection(pos);
                    }
                });

                if(mFeedModels.get(position).getPosts().getType().compareTo("image")==0)
                {
                    holder.video.setVisibility(View.GONE);
                    holder.live.setVisibility(View.GONE);
                }
                else
                {
                    holder.video.setVisibility(View.VISIBLE);
                    holder.live.setVisibility(View.GONE);
                }
            }

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    }

    private class ViewGroupHolder
    {
        ImageView image;
        ImageView video;
        ImageView live;
    }



    /**
     * @author Zack
     * @since 2016.02.19
     * To retry 4 times during 30 secs
     */
    private class PostRetryThread extends Thread {

        private int retryMaxTimes = 6;
        private int retryMaxSec = 30 * 1000; //30secs
        private int sleepTime = 2 * 1000; //2secs

        private int retry = 1;
        private int nowRetry = 0;

        private boolean isRunning = true;
        private boolean isSuccess;

        private Activity mCtx;

        public PostRetryThread() {
            mCtx = getActivity();
        }

        @Override
        public void run() {
            try {
                long nowTime = System.currentTimeMillis();

                while (isRunning && retry <= retryMaxTimes && (System.currentTimeMillis() - nowTime) < retryMaxSec) {
                    if (nowRetry == retry) {
                        sleep(1000);
                    } else {
                        nowRetry = retry;
                        ApiManager.publishPost(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""),
                                getConfig(Constants.UPLOAD_CAPTION), getConfig(Constants.UPLOAD_USER),
                                getConfig(Constants.UPLOAD_TAG),getConfig(Constants.UPLOAD_PHOTO),getConfig(Constants.UPLOAD_VIDEO),
                                getConfig(Constants.UPLOAD_TYPE), new ApiManager.PublishPostCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {
                                    isSuccess = success;
                                    isRunning = false;
                                } else {
                                    retry++;
                                }
                            }
                        });
                        sleep(sleepTime);
                    }
                }

                //Log.d("PostRetryThread", "nowRetry="+nowRetry+", isSuccess="+isSuccess);

                mCtx.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (!isSuccess) {
                                mProgress.setVisibility(View.GONE);
                                Toast.makeText(mCtx, getString(R.string.post_failed), Toast.LENGTH_SHORT).show();
                            } else {
                                executePostFinish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void destroyProcess() {
            isRunning = false;
        }
    }
}

/*
TestBaseAdapter mListAdapter;
StickyListHeadersListView mListView;

public class TestBaseAdapter extends BaseAdapter implements StickyListHeadersAdapter {
        private final Context mContext;
        //        private String[] mCountries;
        private LayoutInflater mInflater;

        public TestBaseAdapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
//            mCountries = context.getResources().getStringArray(R.array.countries);
        }

        @Override
        public int getCount() {
            return mFeedModels.size();
        }

        @Override
        public Object getItem(int position) {
            return mFeedModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.home_list_sticky_down, parent, false);
                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.dio = (TextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (mFeedModels.get(position).getCaption().length() != 0) {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mFeedModels.get(position).getCaption());
            } else holder.dio.setVisibility(View.GONE);

            holder.like_text.setText(String.valueOf(mFeedModels.get(position).getLikeCount()) + getString(R.string.home_like));
            holder.comment_text.setText(String.valueOf(mFeedModels.get(position).getCommentCount()) + getString(R.string.home_comment));

            if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) {
                holder.like_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedModels.get(position).getPostID());
                        intent.putExtra("user_id", mFeedModels.get(position).getUserID());
                        startActivity(intent);
                    }
                });
            }

            holder.comment_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedModels.get(position).getPostID());

                    if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) {
                        intent.putExtra("user_id", mFeedModels.get(position).getUserID());
                    }

                    startActivity(intent);
                }
            });

            if (mFeedModels.get(position).getLiked() == 0) {
                holder.btn_like.setImageResource(R.drawable.btn_like_selector);
            } else holder.btn_like.setImageResource(R.drawable.like_down);

            final TextView likeText = holder.like_text;
            final ImageView like = holder.btn_like;
            if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) {
                holder.btn_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedModels.get(position).getPostID());
                        intent.putExtra("user_id", mFeedModels.get(position).getUserID());
                        startActivity(intent);
                    }
                });
            } else {
                holder.btn_like.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            mY = event.getY();
                            mX = event.getX();
                        }

                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            double num = Math.sqrt(Math.pow(((double) mX - (double) event.getX()), 2) + Math.pow(((double) mY - (double) event.getY()), 2));

                            if (num < 30) {
                                int run = (int) (Math.random() * 10000);
                                int img;
                                int mW, mH;

                                if (run < 100) {
                                    img = rabbits[(int) (Math.random() * rabbits.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 500) {
                                    img = cats[(int) (Math.random() * cats.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 1000) {
                                    img = bears[(int) (Math.random() * bears.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 2000) {
                                    img = bubbles[(int) (Math.random() * bubbles.length)];
                                    mW = 120;
                                    mH = 120;
                                } else {
                                    img = loves[(int) (Math.random() * loves.length)];
                                    mW = 60;
                                    mH = 60;
                                }

                                int mS = -30 + ((int) (Math.random() * 60));
                                int mTx = -100 + ((int) (Math.random() * 200));
                                int mTy = 500 + ((int) (Math.random() * 1200));

                                ImageView cat = new ImageView(getActivity());
                                cat.setImageResource(img);
                                cat.setVisibility(View.INVISIBLE);

                                RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                cat.setLayoutParams(mParams);

                                AnimationSet mAnimationSet = new AnimationSet(true);

                                ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                scale.setDuration(500);

                                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                alpha.setDuration(3000);

                                RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                rotate.setDuration(750);

                                TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                translate.setDuration(2000);

                                mAnimationSet.addAnimation(scale);
                                mAnimationSet.addAnimation(rotate);
                                mAnimationSet.addAnimation(translate);
                                mAnimationSet.addAnimation(alpha);
                                cat.startAnimation(mAnimationSet);

                                mBear.addView(cat);

                                if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                                    mFeedModels.get(position).setLikeCount(mFeedModels.get(position).getLikeCount() + 1);
                                    likeText.setText(String.valueOf(mFeedModels.get(position).getLikeCount()) + getString(R.string.home_like));
                                    mFeedModels.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    ApiManager.likePost(getActivity(), mFeedModels.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {

                                            }
                                        }
                                    });
                                } else {
                                    if (mFeedModels.get(position).getLiked() != 1) {
                                        mFeedModels.get(position).setLikeCount(mFeedModels.get(position).getLikeCount() + 1);
                                        likeText.setText(String.valueOf(mFeedModels.get(position).getLikeCount()) + getString(R.string.home_like));
                                        mFeedModels.get(position).setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        ApiManager.likePost(getActivity(), mFeedModels.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }

                        return true;
                    }
                });
            }

            holder.btn_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedModels.get(position).getPostID());

                    if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) {
                        intent.putExtra("user_id", mFeedModels.get(position).getUserID());
                    }

                    startActivity(intent);
                }
            });

            if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) {
                holder.btn_more.setVisibility(View.VISIBLE);

                holder.btn_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0)
                            mListMoreText[0] = getString(R.string.post_delete);
                        else mListMoreText[0] = getString(R.string.post_block);

                        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) {
                                    ApiManager.deletePost(getActivity(), mFeedModels.get(position).getPostID(), new ApiManager.DeletePostCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {
                                                mProgress.setVisibility(View.VISIBLE);

                                                ApiManager.getPostFeed(getActivity(), Integer.MAX_VALUE, 18, new ApiManager.GetPostFeedCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                                                        mProgress.setVisibility(View.GONE);
                                                        if (success) {
                                                            mFeedModels.clear();
                                                            mFeedModels.addAll(feedModel);

                                                            if (mListAdapter != null)
                                                                mListAdapter = null;
                                                            mListAdapter = new TestBaseAdapter(getActivity());

                                                            mListView.setAdapter(mListAdapter);

                                                            isFetchingData = false;
                                                            noMoreData = false;

                                                            if (mFeedModels.size() < 18) {
                                                                noMoreData = true;
                                                            }
                                                        } else
                                                            Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            } else {
                                                Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {

                                }
                            }
                        })
                                .show();
                    }
                });
            } else {
                holder.btn_more.setVisibility(View.GONE);
            }

            holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
            holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels;
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedModels.get(position).getPicture()), holder.photo, BigOptions);
            holder.photo.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        int run = (int) (Math.random() * 10000);
                        int img;
                        int mW, mH;

                        if (run < 100) {
                            img = rabbits[(int) (Math.random() * rabbits.length)];
                            mW = 80;
                            mH = 80;
                        } else if (run < 500) {
                            img = cats[(int) (Math.random() * cats.length)];
                            mW = 80;
                            mH = 80;
                        } else if (run < 1000) {
                            img = bears[(int) (Math.random() * bears.length)];
                            mW = 80;
                            mH = 80;
                        } else if (run < 2000) {
                            img = bubbles[(int) (Math.random() * bubbles.length)];
                            mW = 120;
                            mH = 120;
                        } else {
                            img = loves[(int) (Math.random() * loves.length)];
                            mW = 60;
                            mH = 60;
                        }

                        int mS = -30 + ((int) (Math.random() * 60));
                        int mTx = -100 + ((int) (Math.random() * 200));
                        int mTy = 500 + ((int) (Math.random() * 1200));

                        ImageView cat = new ImageView(getActivity());
                        cat.setImageResource(img);
                        cat.setVisibility(View.INVISIBLE);

                        RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                        mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                        cat.setLayoutParams(mParams);

                        AnimationSet mAnimationSet = new AnimationSet(true);

                        ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        scale.setDuration(500);

                        AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                        alpha.setDuration(3000);

                        RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                        rotate.setDuration(750);

                        TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                        translate.setDuration(2000);

                        mAnimationSet.addAnimation(scale);
                        mAnimationSet.addAnimation(rotate);
                        mAnimationSet.addAnimation(translate);
                        mAnimationSet.addAnimation(alpha);
                        cat.startAnimation(mAnimationSet);

                        mBear.addView(cat);

                        if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                            mFeedModels.get(position).setLikeCount(mFeedModels.get(position).getLikeCount() + 1);
                            likeText.setText(String.valueOf(mFeedModels.get(position).getLikeCount()) + getString(R.string.home_like));
                            mFeedModels.get(position).setLiked(1);
                            like.setImageResource(R.drawable.like_down);

                            ApiManager.likePost(getActivity(), mFeedModels.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                @Override
                                public void onResult(boolean success, String message) {
                                    if (success) {

                                    }
                                }
                            });
                        } else {
                            if (mFeedModels.get(position).getLiked() != 1) {
                                mFeedModels.get(position).setLikeCount(mFeedModels.get(position).getLikeCount() + 1);
                                likeText.setText(String.valueOf(mFeedModels.get(position).getLikeCount()) + getString(R.string.home_like));
                                mFeedModels.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                ApiManager.likePost(getActivity(), mFeedModels.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {

                                        }
                                    }
                                });
                            }
                        }
                    }


                    return true;
                }
            });

            return convertView;
        }

        @Override
        public View getHeaderView(final int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;

            if (convertView == null) {
                holder = new HeaderViewHolder();
                convertView = mInflater.inflate(R.layout.home_list_sticky_top, parent, false);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);
                convertView.setTag(holder);
            } else {
                holder = (HeaderViewHolder) convertView.getTag();
            }

            holder.name.setText(mFeedModels.get(position).getUserInfo().getName());
            holder.day.setText(Singleton.getElapsedTimeString(mFeedModels.get(position).getTimestamp()));

            if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) == 0) {
                holder.money_text.setVisibility(View.VISIBLE);
                double m = (double) mFeedModels.get(position).getTotalRevenue();
                DecimalFormat df = new DecimalFormat("#.####");
                holder.money_text.setText(df.format(m) + " USD");

                holder.view_text.setText(String.valueOf(mFeedModels.get(position).getViewCount()) + getString(R.string.home_views));
                holder.view_text.setVisibility(View.VISIBLE);
            } else {
                holder.money_text.setVisibility(View.GONE);
                holder.view_text.setVisibility(View.GONE);
            }

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getUserInfo().getPicture()), holder.self, SelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeUserActivity.class);
                        intent.putExtra("title", mFeedModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedModels.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), HomeUserActivity.class);
                        intent.putExtra("title", mFeedModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedModels.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            return mFeedModels.get(position).getPostID().charAt(0);
        }
    }
        class HeaderViewHolder {
            ImageView self;
            TextView name;
            TextView day;
            TextView view_text;
            TextView money_text;
        }

        class ViewHolder {
            TouchImage photo;
            TextView dio;
            TextView like_text;
            TextView comment_text;

            ImageView btn_like;
            ImageView btn_comment;
            ImageView btn_more;
        }

 */
