package com.machipopo.media17;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshHeaderGridView;
import com.machipopo.media17.fragment.BaseFragment;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.SuggestedUsersModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

/**
 * Created by POPO on 5/27/15.
 */
public class SearchFragment extends BaseFragment
{
    private LayoutInflater inflater;
    private Story17Application mApplication;
    private ProgressBar mProgress;
    private PullToRefreshHeaderGridView mGrid;
    private RelativeLayout mEdit;
    private ArrayList<FeedModel> mFeedModels = new ArrayList<FeedModel>();
    private GridAdapter mGridAdapter;

    private int mGridHeight;
    private DisplayMetrics mDisplayMetrics;

    private Boolean isFetchingData = false;
    private Boolean noMoreData = false;

    private DisplayImageOptions SelfOptions,SmaleSelfOptions;
    private int mPOS = 0;

    private LinearLayout mLiveLayout, mUserLayout;
    private ImageView mUser1,mUser2,mUser3;
    private ImageView mLive1,mLive2,mLive3;

    private ArrayList<SuggestedUsersModel> mSuggested = new ArrayList<SuggestedUsersModel>();
    private ArrayList<LiveModel> mLiveModels = new ArrayList<LiveModel>();

    private String mUserId = "";
    private View mHeader;

    private Boolean mSetHeaderHeight = true;

    //event tracking
    private int enterSearchPage,leaveSearchPage;
    private int gridViewReflashCount = 0;

//    @Override
//    public void onAttach(Activity activity)
//    {
//        super.onAttach(activity);
//
//        MenuActivity mMenuActivity = (MenuActivity) activity;
//        int pos  = mMenuActivity.getTabPos();
//    }

    public SearchFragment(){
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.search_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        mApplication = (Story17Application) getActivity().getApplication();
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDisplayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        mGridHeight = (int)((float)mDisplayMetrics.widthPixels/(float)3);

        mUserId = Singleton.preferences.getString(Constants.USER_ID, "");

        initTitleBar();

        mEdit = (RelativeLayout) getView().findViewById(R.id.edit);
        mProgress = (ProgressBar) getView().findViewById(R.id.progress);
        mGrid = (PullToRefreshHeaderGridView) getView().findViewById(R.id.grid);
        mHeader = inflater.inflate(R.layout.search_header, null);
        mGrid.getRefreshableView().addHeaderView(mHeader);

        mLiveLayout = (LinearLayout) mHeader.findViewById(R.id.live_layout);
        mUserLayout = (LinearLayout) mHeader.findViewById(R.id.user_layout);
        mUser1 = (ImageView) mHeader.findViewById(R.id.user1);
        mUser2 = (ImageView) mHeader.findViewById(R.id.user2);
        mUser3 = (ImageView) mHeader.findViewById(R.id.user3);
        mLive1 = (ImageView) mHeader.findViewById(R.id.live1);
        mLive2 = (ImageView) mHeader.findViewById(R.id.live2);
        mLive3 = (ImageView) mHeader.findViewById(R.id.live3);

        //event tracking
        try{
            LogEventUtil.EnterDiscoverPage(getActivity(),mApplication);
        }catch (Exception x)
        {

        }

        enterSearchPage = Singleton.getCurrentTimestamp();

        LinearLayout mKingLayout = (LinearLayout) mHeader.findViewById(R.id.user_king_layout);
        if(Constants.USERKING && Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1)
        {
            mKingLayout.setVisibility(View.VISIBLE);
            TextView mKingLive = (TextView) mHeader.findViewById(R.id.user_live);
            TextView mKingPhoto = (TextView) mHeader.findViewById(R.id.user_photo);

            mKingLive.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(),ReviewLiveStreamsActivity.class);
                    startActivity(intent);
                }
            });

            mKingPhoto.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(),ReviewPicturesActivity.class);
                    startActivity(intent);
                }
            });
        }
        else mKingLayout.setVisibility(View.GONE);

        mLiveLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mApplication.setNowRegion(getString(R.string.local));
                Intent intent = new Intent();
                intent.setClass(getActivity(),NowLiveStreamActivity.class);
                intent.putExtra("user_id",mUserId);
                startActivity(intent);
            }
        });

        mUserLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mApplication.setSuggested(mSuggested);
                Intent intent = new Intent();
                intent.setClass(getActivity(),FollowSuggestedActivity.class);
                startActivity(intent);
            }
        });

        mGrid.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener()
        {
            @Override
            public void onRefresh(PullToRefreshBase refreshView)
            {

                //event tracking
                gridViewReflashCount++;
                ApiManager.getHotPost(getActivity(), Integer.MAX_VALUE, 18, new ApiManager.GetHotPostsCallback()
                {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
                    {
                        mGrid.onRefreshComplete();
                        if (success && feedModel!=null)
                        {
                            mFeedModels.clear();
                            mFeedModels.addAll(feedModel);
                            mGridAdapter = new GridAdapter();
                            mGrid.setAdapter(mGridAdapter);

                            if (mFeedModels.size() < 18) {
                                noMoreData = true;
                            }
                            else noMoreData = false;

                        }
                        else
                        {
                            try
                            {
                                if(!isAdded()) {
                                    return;
                                }
                                try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                    Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                });

                ApiManager.getHotLiveStreams2(getActivity(),"local" ,Integer.MAX_VALUE, 3, new ApiManager.GetHotLiveStreamsCallback()
                {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels)
                    {
                        if (success && liveModels!=null)
                        {
                            if(liveModels.size()!=0)
                            {
                                mLiveModels.clear();
                                mLiveModels.addAll(liveModels);

                                mLiveLayout.setVisibility(View.VISIBLE);
                                if(liveModels.size()>2)
                                {
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(1).getUserInfo().getPicture()), mLive2, SmaleSelfOptions);
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(2).getUserInfo().getPicture()), mLive3, SmaleSelfOptions);
                                }
                                else if(liveModels.size()>1)
                                {
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(1).getUserInfo().getPicture()), mLive2, SmaleSelfOptions);
                                    mLive3.setImageResource(R.drawable.placehold_c);
                                }
                                else
                                {
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                                    mLive2.setImageResource(R.drawable.placehold_c);
                                    mLive3.setImageResource(R.drawable.placehold_c);
                                }
                            }
                            else mLiveLayout.setVisibility(View.GONE);
                        }
                        else mLiveLayout.setVisibility(View.GONE);

                        mSetHeaderHeight = true;
                        mHeader.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
                        {
                            @Override
                            public void onGlobalLayout()
                            {
                                if(mSetHeaderHeight)
                                {
                                    mGrid.getRefreshableView().setHeaderViewHeight(mHeader.getHeight());
                                    mSetHeaderHeight = false;
                                }
                            }
                        });
                    }
                });

                ApiManager.getExploreSuggestedUsers(getActivity(), mUserId,Integer.MAX_VALUE,50,new ApiManager.GetExploreSuggestedUsersCallback()
                {
                    @Override
                    public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested)
                    {
                        if (success && suggested != null)
                        {
                            if(suggested.size()!=0)
                            {
                                mSuggested.clear();
                                mSuggested.addAll(suggested);

                                mUserLayout.setVisibility(View.VISIBLE);

                                if(suggested.size()>2)
                                {
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(1).getPicture()), mUser2, SmaleSelfOptions);
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(2).getPicture()), mUser3, SmaleSelfOptions);
                                }
                                else if(suggested.size()>1)
                                {
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(1).getPicture()), mUser2, SmaleSelfOptions);
                                    mUser3.setImageResource(R.drawable.placehold_c);
                                }
                                else
                                {
                                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                                    mUser2.setImageResource(R.drawable.placehold_c);
                                    mUser3.setImageResource(R.drawable.placehold_c);
                                }
                            }
                            else mUserLayout.setVisibility(View.GONE);
                        }
                        else mUserLayout.setVisibility(View.GONE);

                        mSetHeaderHeight = true;
                        mHeader.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
                        {
                            @Override
                            public void onGlobalLayout()
                            {
                                if(mSetHeaderHeight)
                                {
                                    mGrid.getRefreshableView().setHeaderViewHeight(mHeader.getHeight());
                                    mSetHeaderHeight = false;
                                }
                            }
                        });
                    }
                });
            }
        });


        mEdit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),SearchInfoActivity.class);
                startActivity(intent);
            }
        });

        if(mFeedModels.size()==0)
        {
            mProgress.setVisibility(View.VISIBLE);
            ApiManager.getHotPost(getActivity(), Integer.MAX_VALUE, 18, new ApiManager.GetHotPostsCallback() {
                @Override
                public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                    mProgress.setVisibility(View.GONE);
                    if (success && feedModel!=null) {
                        mFeedModels.addAll(feedModel);
                        mGridAdapter = new GridAdapter();
                        mGrid.setAdapter(mGridAdapter);

                        if (mFeedModels.size() < 18) {
                            noMoreData = true;
                        }
                        else noMoreData = false;
                    }
                    else
                    {
                        try
                        {
                            if(getActivity()!=null)
                            {
                                try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                                    Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                        catch(Exception e)
                        {

                        }
                    }
                    }
            });

            ApiManager.getHotLiveStreams2(getActivity(),"local" ,Integer.MAX_VALUE, 3 , new ApiManager.GetHotLiveStreamsCallback()
            {
                @Override
                public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels)
                {
                    if (success && liveModels!=null)
                    {
                        if(liveModels.size()!=0)
                        {
                            mLiveModels.clear();
                            mLiveModels.addAll(liveModels);

                            mLiveLayout.setVisibility(View.VISIBLE);
                            if(liveModels.size()>2)
                            {
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(1).getUserInfo().getPicture()), mLive2, SmaleSelfOptions);
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(2).getUserInfo().getPicture()), mLive3, SmaleSelfOptions);
                            }
                            else if(liveModels.size()>1)
                            {
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(1).getUserInfo().getPicture()), mLive2, SmaleSelfOptions);
                                mLive3.setImageResource(R.drawable.placehold_c);
                            }
                            else
                            {
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                                mLive2.setImageResource(R.drawable.placehold_c);
                                mLive3.setImageResource(R.drawable.placehold_c);
                            }
                        }
                        else mLiveLayout.setVisibility(View.GONE);
                    }
                    else mLiveLayout.setVisibility(View.GONE);

                    mSetHeaderHeight = true;
                    mHeader.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
                    {
                        @Override
                        public void onGlobalLayout()
                        {
                            if(mSetHeaderHeight)
                            {
                                mGrid.getRefreshableView().setHeaderViewHeight(mHeader.getHeight());
                                mSetHeaderHeight = false;
                            }
                        }
                    });
                }
            });

            ApiManager.getExploreSuggestedUsers(getActivity(), mUserId,Integer.MAX_VALUE,50,new ApiManager.GetExploreSuggestedUsersCallback()
            {
                @Override
                public void onResult(boolean success, ArrayList<SuggestedUsersModel> suggested)
                {
                    if (success && suggested != null)
                    {
                        if(suggested.size()!=0)
                        {
                            mSuggested.clear();
                            mSuggested.addAll(suggested);

                            mUserLayout.setVisibility(View.VISIBLE);

                            if(suggested.size()>2)
                            {
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(1).getPicture()), mUser2, SmaleSelfOptions);
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(2).getPicture()), mUser3, SmaleSelfOptions);
                            }
                            else if(suggested.size()>1)
                            {
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(1).getPicture()), mUser2, SmaleSelfOptions);
                                mUser3.setImageResource(R.drawable.placehold_c);
                            }
                            else
                            {
                                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                                mUser2.setImageResource(R.drawable.placehold_c);
                                mUser3.setImageResource(R.drawable.placehold_c);
                            }
                        }
                        else mUserLayout.setVisibility(View.GONE);
                    }
                    else mUserLayout.setVisibility(View.GONE);

                    mSetHeaderHeight = true;
                    mHeader.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
                    {
                        @Override
                        public void onGlobalLayout()
                        {
                            if(mSetHeaderHeight)
                            {
                                mGrid.getRefreshableView().setHeaderViewHeight(mHeader.getHeight());
                                mSetHeaderHeight = false;
                            }
                        }
                    });
                }
            });
        }
        else
        {
            mProgress.setVisibility(View.GONE);
            mGridAdapter = new GridAdapter();
            mGrid.setAdapter(mGridAdapter);

            if (mFeedModels.size() < 18) {
                noMoreData = true;
            }
            else noMoreData = false;

            if(mLiveModels.size()!=0)
            {
                mLiveLayout.setVisibility(View.VISIBLE);
                if(mLiveModels.size()>2)
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(1).getUserInfo().getPicture()), mLive2, SmaleSelfOptions);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(2).getUserInfo().getPicture()), mLive3, SmaleSelfOptions);
                }
                else if(mLiveModels.size()>1)
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(1).getUserInfo().getPicture()), mLive2, SmaleSelfOptions);
                    mLive3.setImageResource(R.drawable.placehold_c);
                }
                else
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(0).getUserInfo().getPicture()), mLive1, SmaleSelfOptions);
                    mLive2.setImageResource(R.drawable.placehold_c);
                    mLive3.setImageResource(R.drawable.placehold_c);
                }
            }
            else mLiveLayout.setVisibility(View.GONE);

            if(mSuggested.size()!=0)
            {
                mUserLayout.setVisibility(View.VISIBLE);

                if(mSuggested.size()>2)
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(1).getPicture()), mUser2, SmaleSelfOptions);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(2).getPicture()), mUser3, SmaleSelfOptions);
                }
                else if(mSuggested.size()>1)
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(1).getPicture()), mUser2, SmaleSelfOptions);
                    mUser3.setImageResource(R.drawable.placehold_c);
                }
                else
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggested.get(0).getPicture()), mUser1, SmaleSelfOptions);
                    mUser2.setImageResource(R.drawable.placehold_c);
                    mUser3.setImageResource(R.drawable.placehold_c);
                }
            }
            else mUserLayout.setVisibility(View.GONE);

            mSetHeaderHeight = true;
            mHeader.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
            {
                @Override
                public void onGlobalLayout()
                {
                    if(mSetHeaderHeight)
                    {
                        mGrid.getRefreshableView().setHeaderViewHeight(mHeader.getHeight());
                        mSetHeaderHeight = false;
                    }
                }
            });
        }

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SmaleSelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_c)
        .showImageForEmptyUri(R.drawable.placehold_c)
        .showImageOnFail(R.drawable.placehold_c)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) getView().findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) getView().findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.feed_search_discover));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) getView().findViewById(R.id.img_left);
        img.setVisibility(View.VISIBLE);
        img.setImageResource(R.drawable.btn_addfriend_selector);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(getActivity(),AddFriendActivity.class);
                startActivity(intent);
            }
        });
    }

    private class GridAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.grid_row, null);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.video = (ImageView) convertView.findViewById(R.id.video);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.image.getLayoutParams().width = mGridHeight;
            holder.image.getLayoutParams().height = mGridHeight;
            //Constants.THUMBNAIL_PREFIX +
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getPicture()), holder.image, SelfOptions);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPOS = position;

                    //event tracking
                    try {
                        LogEventUtil.ClickDiscoverPictures(getActivity(), mApplication, position, mFeedModels.size());
                    }catch (Exception x)
                    {

                    }
                    
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), PhotoActivity.class);
                    intent.putExtra("photo", mFeedModels.get(position).getPicture());
                    intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                    intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                    intent.putExtra("day", Singleton.getElapsedTimeString(mFeedModels.get(position).getTimestamp()));
                    intent.putExtra("view", mFeedModels.get(position).getViewCount());
                    intent.putExtra("money", mFeedModels.get(position).getTotalRevenue());
                    intent.putExtra("dio", mFeedModels.get(position).getCaption());
                    intent.putExtra("likecount", mFeedModels.get(position).getLikeCount());
                    intent.putExtra("commentcount", mFeedModels.get(position).getCommentCount());
                    intent.putExtra("likeed", mFeedModels.get(position).getLiked());

                    intent.putExtra("postid", mFeedModels.get(position).getPostID());
                    intent.putExtra("userid", mFeedModels.get(position).getUserID());

                    intent.putExtra("name", mFeedModels.get(position).getUserInfo().getName());
                    intent.putExtra("isFollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                    intent.putExtra("postCount", mFeedModels.get(position).getUserInfo().getPostCount());
                    intent.putExtra("followerCount", mFeedModels.get(position).getUserInfo().getFollowerCount());
                    intent.putExtra("followingCount", mFeedModels.get(position).getUserInfo().getFollowingCount());
                    intent.putExtra("web", mFeedModels.get(position).getUserInfo().getWebsite());
                    intent.putExtra("goto", true);
                    intent.putExtra("type", mFeedModels.get(position).getType());
                    intent.putExtra("video", mFeedModels.get(position).getVideo());
                    startActivity(intent);
                }
            });

            if(mFeedModels.get(position).getType().compareTo("image")==0) holder.video.setVisibility(View.GONE);
            else holder.video.setVisibility(View.VISIBLE);

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView image;
        ImageView video;
    }

    public void LoadData(final boolean refresh)
    {
        if(isFetchingData)
        {
            return;
        }

        if(refresh)
        {
            noMoreData = false;
        }

        if(noMoreData)
        {
            return;
        }

        isFetchingData = true;

        ApiManager.getHotPost(getActivity(), mFeedModels.get(mFeedModels.size() - 1).getTimestamp(), 18, new ApiManager.GetHotPostsCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
            {
                isFetchingData = false;

                if (success && feedModel!=null)
                {
                    if (feedModel.size() != 0)
                    {
                        mFeedModels.addAll(feedModel);

                        if (feedModel.size() < 18)
                        {
                            noMoreData = true;
                        }
                        else noMoreData = false;

                        if (mGridAdapter != null)
                        {
                            mGridAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    if(!isAdded()) {
                        return;
                    }

                    try{
//                                  ((MenuActivity)getActivity()).showToast(getString(R.string.failed));
                        Toast.makeText(getActivity(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mApplication.getGripFollow())
        {
            if(mFeedModels!=null && mFeedModels.size()!=0)
            {
                if(mFeedModels.size() > mPOS)
                {
                    mFeedModels.get(mPOS).getUserInfo().setIsFollowing(1);
                    mApplication.setGripFollow(false);
                }
            }
        }

        MobclickAgent.onPageStart("SearchFragment");
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd("SearchFragment");
    }

    public void onDestory()
    {
        super.onDestroy();

        leaveSearchPage = Singleton.getCurrentTimestamp();

        //event tracking
        try {
            LogEventUtil.LeaveDiscoverPage(getActivity(),mApplication,leaveSearchPage-enterSearchPage,gridViewReflashCount,mFeedModels.size());
        }catch (Exception x)
        {

        }
    }
}
