package com.machipopo.media17.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.ApiManager;
import com.machipopo.media17.Constants;
import com.machipopo.media17.HomeUserActivity;
import com.machipopo.media17.R;
import com.machipopo.media17.Singleton;
import com.machipopo.media17.Story17Application;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Zack on 1/20/16.
 */
public class UserAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<UserModel> mDataList;
    private DisplayImageOptions mSelfOptions;
    private String mUmeng_id;
    private Story17Application mApplication;

    private class ViewHolder {
        ImageView img;
        TextView name;
        TextView dio;
        Button follow;
        ImageView verifie;
    }

    public UserAdapter(Context context, ArrayList<UserModel> dataList, DisplayImageOptions mSelfOptions) {
        this(context, dataList, mSelfOptions, null);
    }

    public UserAdapter(Context context, ArrayList<UserModel> dataList, DisplayImageOptions mSelfOptions, String mUmeng_id) {
        mDataList = dataList;
        this.context = context;
        this.mSelfOptions = mSelfOptions;
        this.mUmeng_id = mUmeng_id;
        this.mApplication = (Story17Application) context.getApplicationContext();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (mDataList == null) {
            mDataList = new ArrayList<>();
        }
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final UserModel mUserModel = mDataList.get(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.follow_friend_row, null);
            holder = new ViewHolder();
            holder.img = (ImageView) convertView.findViewById(R.id.pic);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.dio = (TextView) convertView.findViewById(R.id.dio);
            holder.follow = (Button) convertView.findViewById(R.id.follow);
            holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUserModel.getPicture()), holder.img, mSelfOptions);
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mUmeng_id != null && !"".equals(mUmeng_id)){
                    //Umeng Monitor
                    String Umeng_id = "SearchUserAndClick";
                    HashMap<String, String> mHashMap = new HashMap<String, String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);
                }

                if (mUserModel.getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                    goToHomeUser(mUserModel);
                }
            }
        });

        holder.name.setText(mUserModel.getOpenID());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mUmeng_id != null && !"".equals(mUmeng_id)){
                    //Umeng Monitor
                    String Umeng_id = "SearchUserAndClick";
                    HashMap<String, String> mHashMap = new HashMap<String, String>();
                    mHashMap.put(Umeng_id, Umeng_id);
                    MobclickAgent.onEventValue(context, Umeng_id, mHashMap, 0);
                }


                if (mUserModel.getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                    goToHomeUser(mUserModel);
                }
            }
        });
        holder.dio.setText(mUserModel.getName());
        if (mUserModel.getIsFollowing() == 1) {
            holder.follow.setText(context.getString(R.string.user_profile_following));
            holder.follow.setBackgroundResource(R.drawable.btn_green_selector);
            holder.follow.setTextColor(Color.WHITE);
        } else {
            if (mUserModel.getFollowRequestTime() != 0) {
                holder.follow.setText(context.getString(R.string.private_mode_request_send));
                holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                holder.follow.setTextColor(context.getResources().getColor(R.color.content_text_color));
            } else {
                holder.follow.setText("+ " + context.getString(R.string.user_profile_follow));
                holder.follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                holder.follow.setTextColor(context.getResources().getColor(R.color.content_text_color));
            }
        }

        final Button btn = holder.follow;
        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserModel.getIsFollowing() == 1) {

                    LogEventUtil.UnfollowUser(context, mApplication, mUserModel.getUserID());
                    ApiManager.unfollowUserAction(context, Singleton.preferences.getString(Constants.USER_ID, ""), mUserModel.getUserID(), new ApiManager.UnfollowUserActionCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            if (success) {
                                mUserModel.setIsFollowing(0);
                                btn.setText("+ " + context.getString(R.string.user_profile_follow));
                                btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                btn.setTextColor(context.getResources().getColor(R.color.content_text_color));
                            } else {
                                try {
                                    Toast.makeText(context, context.getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                }
                            }
                        }
                    });
                } else {
                    if (mUserModel.getFollowRequestTime() != 0) {
                        btn.setText("+ " + context.getString(R.string.user_profile_follow));
                        btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                        btn.setTextColor(context.getResources().getColor(R.color.content_text_color));
                        mUserModel.setIsFollowing(0);
                        mUserModel.setFollowRequestTime(0);
                        ApiManager.cancelFollowRequests(context, mUserModel.getUserID(), new ApiManager.RequestCallback() {
                            @Override
                            public void onResult(boolean success) {
                                if (success) {

                                }
                            }
                        });
                    } else {
                        if (Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000) {
                            try {
                                Toast.makeText(context, context.getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                            }
                            return;
                        }

                        if (mUserModel.getPrivacyMode().compareTo("private") == 0) {
                            btn.setText(context.getString(R.string.private_mode_request_send));
                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                            btn.setTextColor(context.getResources().getColor(R.color.content_text_color));
                            mUserModel.setIsFollowing(0);
                            mUserModel.setFollowRequestTime(Singleton.getCurrentTimestamp());
                            ApiManager.sendFollowRequest(context, mUserModel.getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (!success) {
                                        btn.setText("+ " + context.getString(R.string.user_profile_follow));
                                        btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                        btn.setTextColor(context.getResources().getColor(R.color.content_text_color));
                                        mUserModel.setIsFollowing(0);
                                        mUserModel.setFollowRequestTime(0);
                                    }
                                }
                            });
                        } else {
                            LogEventUtil.FollowUser(context, mApplication, mUserModel.getUserID());
                            ApiManager.followUserAction(context, Singleton.preferences.getString(Constants.USER_ID, ""), mUserModel.getUserID(), new ApiManager.FollowUserActionCallback() {
                                @Override
                                public void onResult(boolean success, String message) {
                                    if (success) {
                                        mUserModel.setIsFollowing(1);
                                        btn.setText(context.getString(R.string.user_profile_following));
                                        btn.setBackgroundResource(R.drawable.btn_green_selector);
                                        btn.setTextColor(Color.WHITE);
                                    } else {
                                        try {
                                            Toast.makeText(context, context.getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

        if (mUserModel.getIsVerified() == 1){
            holder.verifie.setVisibility(View.VISIBLE);
        } else {
            holder.verifie.setVisibility(View.GONE);
        }

        return convertView;
    }

    private void goToHomeUser(UserModel mUserModel) {
        Intent intent = new Intent();
        intent.setClass(context, HomeUserActivity.class);
        intent.putExtra("title", mUserModel.getName());
        intent.putExtra("picture", mUserModel.getPicture());
        intent.putExtra("isfollowing", mUserModel.getIsFollowing());
        intent.putExtra("post", mUserModel.getPostCount());
        intent.putExtra("follow", mUserModel.getFollowerCount());
        intent.putExtra("following", mUserModel.getFollowingCount());
        intent.putExtra("open", mUserModel.getOpenID());
        intent.putExtra("bio", mUserModel.getBio());
        intent.putExtra("targetUserID", mUserModel.getUserID());
        intent.putExtra("web", mUserModel.getWebsite());
        context.startActivity(intent);
    }


}

