package com.machipopo.media17;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.machipopo.media17.model.RevenueModel;
import com.umeng.analytics.MobclickAgent;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by POPO on 6/22/15.
 */
public class RevenueMonthActivity extends BaseNewActivity
{
    private RevenueMonthActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;

    private ArrayList<RevenueModel> mRevenueModel = new ArrayList<RevenueModel>();

    private TextView mTotal;
    private ListView mList;

    private String c = "USD";
    private DecimalFormat df;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_month_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        mTotal = (TextView) findViewById(R.id.total);
        mList = (ListView) findViewById(R.id.list);

        mRevenueModel.addAll(mApplication.getRevenueModels());

        c = Singleton.getCurrencyType();

        double total = (double) getMoneyConfig(Constants.REVENUE_MONEY_TOTAL, 0f) *Singleton.getCurrencyRate();
        df = new DecimalFormat("#.####");
        mTotal.setText("$ " + df.format(total) + " " + c);

        Calendar mCalendar = new GregorianCalendar();
        int year = mCalendar.get(Calendar.YEAR) ;
        int mon = mCalendar.get(Calendar.MONTH) + 1 ;
        float[] monthMoneys = new float[mon];
        String[] monthText = new String[mon];

        for(int k = 0 ; k < monthMoneys.length ; k++)
        {
            monthMoneys[k] = 0;
        }

        for(int p = 0 ; p < monthText.length ; p++)
        {
            monthText[p] = String.valueOf(year) + "/" + String.valueOf(mon-p);
        }

        for(int i = 0 ; i < mRevenueModel.size() ; i++)
        {
            if(mRevenueModel.get(i).getYear() == year)
            {
                if(mRevenueModel.get(i).getMonth() == mon)
                {
                    monthMoneys[0] = monthMoneys[0] + mRevenueModel.get(i).getRevenue();
                }
                else
                {
                    if(mRevenueModel.get(i).getMonth() == mon-1)
                    {
                        monthMoneys[1] = monthMoneys[1] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-2)
                    {
                        monthMoneys[2] = monthMoneys[2] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-3)
                    {
                        monthMoneys[3] = monthMoneys[3] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-4)
                    {
                        monthMoneys[4] = monthMoneys[4] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-5)
                    {
                        monthMoneys[5] = monthMoneys[5] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-6)
                    {
                        monthMoneys[6] = monthMoneys[6] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-7)
                    {
                        monthMoneys[7] = monthMoneys[7] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-8)
                    {
                        monthMoneys[8] = monthMoneys[8] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-9)
                    {
                        monthMoneys[9] = monthMoneys[9] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-10)
                    {
                        monthMoneys[10] = monthMoneys[10] + mRevenueModel.get(i).getRevenue();
                    }
                    else if(mRevenueModel.get(i).getMonth() == mon-11)
                    {
                        monthMoneys[11] = monthMoneys[11] + mRevenueModel.get(i).getRevenue();
                    }
                }
            }
        }

        for(int j = 0 ; j < monthMoneys.length ; j++)
        {
            monthMoneys[j] = monthMoneys[j] * Singleton.getCurrencyRate();
        }

        mList.setAdapter(new MyAdapter(monthMoneys,monthText));
    }

    public Float getMoneyConfig(String key, Float def)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        return settings.getFloat(key, def);
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_get_month));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class MyAdapter extends BaseAdapter
    {
        private float[] mMonths;
        private String[] mTexts;

        public MyAdapter(float[] months, String[] texts)
        {
            mMonths = months;
            mTexts = texts;
        }

        @Override
        public int getCount()
        {
            return mMonths.length;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.revenue_month_row, null);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.money = (TextView) convertView.findViewById(R.id.money);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.day.setText(mTexts[position]);
            holder.money.setText("$ " + df.format((double)mMonths[position]) + " " + c);

            return convertView;
        }
    }

    private class ViewHolder
    {
        TextView day;
        TextView money;
    }
}
