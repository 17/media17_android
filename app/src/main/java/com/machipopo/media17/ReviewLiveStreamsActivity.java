package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by POPO on 15/9/30.
 */
public class ReviewLiveStreamsActivity extends BaseNewActivity
{
    private ReviewLiveStreamsActivity mCtx = this;
    private LayoutInflater inflater;
    private ArrayList<LiveModel> mLiveModels = new ArrayList<LiveModel>();
    private PullToRefreshListView mList;
    private FriendAdapter mFriendAdapter;
    private DisplayMetrics mDisplayMetrics;
    private DisplayImageOptions SmaleSelfOptions;
    private ImageView mNoData;
    private ProgressBar mProgress;
    private Story17Application mApplication;

    private int mCount = 0, mPos = 0;
    private Dialog mDialog;
    private int[] mAllCount;
    private int[] mAllPos;

    private Button btn;
    private PosAdapter mPosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_livestreams_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        initTitleBar();

        mList = (PullToRefreshListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mProgress.setVisibility(View.VISIBLE);
        mList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
        {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView)
            {
                ApiManager.getLiveStreamsForReview(mCtx, Singleton.preferences.getString(Constants.IP_COUNTRY, ""), Integer.MAX_VALUE, 50, mPos, (mCount+1), new ApiManager.GetHotLiveStreamsCallback() {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
                        mList.onRefreshComplete();
                        if (success && liveModels != null) {
                            if (liveModels.size() != 0) {
                                mNoData.setVisibility(View.GONE);
                                mLiveModels.clear();
                                mLiveModels.addAll(liveModels);

                                if (mFriendAdapter != null) mFriendAdapter = null;
                                mFriendAdapter = new FriendAdapter();
                                mList.setAdapter(mFriendAdapter);
                            } else {
                                mLiveModels.clear();
                                if (mFriendAdapter != null) mFriendAdapter = null;
                                mFriendAdapter = new FriendAdapter();
                                mList.setAdapter(mFriendAdapter);
                                mNoData.setVisibility(View.VISIBLE);
                            }
                        } else {
                            mLiveModels.clear();
                            if (mFriendAdapter != null) mFriendAdapter = null;
                            mFriendAdapter = new FriendAdapter();
                            mList.setAdapter(mFriendAdapter);
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });

        ApiManager.getLiveStreamsForReview(mCtx, Singleton.preferences.getString(Constants.IP_COUNTRY, ""), Integer.MAX_VALUE, 50, mPos, (mCount + 1), new ApiManager.GetHotLiveStreamsCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
                mProgress.setVisibility(View.GONE);
                if (success && liveModels != null) {
                    if (liveModels.size() != 0) {
                        mLiveModels.clear();
                        mLiveModels.addAll(liveModels);

                        mFriendAdapter = new FriendAdapter();
                        mList.setAdapter(mFriendAdapter);
                    } else {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    mNoData.setVisibility(View.VISIBLE);
                }
            }
        });

        SmaleSelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_c)
        .showImageForEmptyUri(R.drawable.placehold_c)
        .showImageOnFail(R.drawable.placehold_c)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText("審核直播");
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });

        btn = (Button) findViewById(R.id.btn_right);
        btn.setText((mPos+1) + " / " + (mCount+1));
        btn.setTextColor(Color.WHITE);
        btn.setVisibility(View.VISIBLE);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mAllCount==null)
                {
                    mAllCount = new int[50];

                    for(int i = 1; i < 51 ; i++)
                    {
                        mAllCount[i-1] = i;
                    }
                }

                if(mAllPos==null)
                {
                    mAllPos = new int[1];
                    mAllPos[0] = 1;
                }

                if(mDialog==null)
                {
                    mDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                    mDialog.setContentView(R.layout.review_picker);

                    ListView mCountList = (ListView) mDialog.findViewById(R.id.count);
                    ListView mPosList = (ListView) mDialog.findViewById(R.id.pos);

                    mCountList.setAdapter(new CountAdapter());

                    mPosAdapter = new PosAdapter();
                    mPosList.setAdapter(mPosAdapter);

                    Button mOk = (Button)mDialog.findViewById(R.id.ok);
                    mOk.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            btn.setText((mPos+1) + " / " + (mCount+1));
                            mDialog.dismiss();

                            mProgress.setVisibility(View.VISIBLE);
                            ApiManager.getLiveStreamsForReview(mCtx, Singleton.preferences.getString(Constants.IP_COUNTRY, ""), Integer.MAX_VALUE, 50, mPos, (mCount + 1), new ApiManager.GetHotLiveStreamsCallback() {
                                @Override
                                public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
                                    mProgress.setVisibility(View.GONE);
                                    if (success && liveModels != null) {
                                        if (liveModels.size() != 0) {
                                            mNoData.setVisibility(View.GONE);
                                            mLiveModels.clear();
                                            mLiveModels.addAll(liveModels);

                                            if (mFriendAdapter != null) mFriendAdapter = null;
                                            mFriendAdapter = new FriendAdapter();
                                            mList.setAdapter(mFriendAdapter);
                                        } else {
                                            mLiveModels.clear();
                                            if (mFriendAdapter != null) mFriendAdapter = null;
                                            mFriendAdapter = new FriendAdapter();
                                            mList.setAdapter(mFriendAdapter);
                                            mNoData.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        mLiveModels.clear();
                                        if (mFriendAdapter != null) mFriendAdapter = null;
                                        mFriendAdapter = new FriendAdapter();
                                        mList.setAdapter(mFriendAdapter);
                                        mNoData.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                        }
                    });
                }

                mDialog.show();
            }
        });
    }

    private class FriendAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mLiveModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.review_livestreams_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.live_text = (TextView) convertView.findViewById(R.id.live_text);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);

                holder.user_hide = (TextView) convertView.findViewById(R.id.user_hide);
                holder.user_freeze = (TextView) convertView.findViewById(R.id.user_freeze);
                holder.user_verified = (TextView) convertView.findViewById(R.id.user_verified);
                holder.live_hide = (TextView) convertView.findViewById(R.id.live_hide);
                holder.live_pass = (TextView) convertView.findViewById(R.id.live_pass);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.name.setText(mLiveModels.get(position).getUserInfo().getOpenID());

            try
            {
                holder.day.setText(Singleton.getElapsedTimeString(mLiveModels.get(position).getBeginTime()));
            }
            catch (Exception e)
            {
                holder.day.setText("");
            }

            holder.live_text.setText(mLiveModels.get(position).getLiveViewerCount() + " " + getString(R.string.live_viewing));
            holder.live_text.setVisibility(View.VISIBLE);

            holder.view_text.setText(mLiveModels.get(position).getUserInfo().getFollowerCount() + " " + getString(R.string.user_profile_followers));
            holder.view_text.setVisibility(View.VISIBLE);

            if(mLiveModels.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mLiveModels.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.GONE);

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(position).getUserInfo().getPicture()), holder.self, SmaleSelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLiveModels.size() != 0 && mLiveModels.size() > position) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, LiveStreamActivity.class);
                        intent.putExtra("liveStreamID", mLiveModels.get(position).getLiveStreamID());
                        intent.putExtra("name", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("caption", mLiveModels.get(position).getCaption());
                        intent.putExtra("picture", Constants.THUMBNAIL_PREFIX + mLiveModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("user", Singleton.preferences.getString(Constants.USER_ID, ""));
                        intent.putExtra("myopen", Singleton.preferences.getString(Constants.OPEN_ID, ""));
                        //event tracking: to track what page user enter livestream
                        intent.putExtra("enterLiveFrom","reviewLive");
                        startActivity(intent);
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLiveModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mLiveModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mLiveModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mLiveModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mLiveModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mLiveModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mLiveModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mLiveModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mLiveModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mLiveModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("live", true);

                        intent.putExtra("live_id", mLiveModels.get(position).getLiveStreamID());
                        intent.putExtra("live_openid", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("live_caption", mLiveModels.get(position).getCaption());
                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveModels.get(position).getUserInfo().getPicture());
                        startActivity(intent);
                    }
                }
            });

            if(mLiveModels.get(position).getUserInfo().getIsVerified()==1)
            {
                holder.user_verified.setText("unVerified");
                holder.verifie.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.user_verified.setText("Verified");
                holder.verifie.setVisibility(View.GONE);
            }

            holder.dio.linkify(new FeedTagActionHandler() {
                @Override
                public void handleHashtag(String hashtag) {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention) {
                    mProgress.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message, UserModel user) {
                            mProgress.setVisibility(View.GONE);
                            if (success && user != null) {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                startActivity(intent);
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email) {

                }

                @Override
                public void handleUrl(String url) {
                    try {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (Exception e) {
                        try{
//                          showToast(getString(R.string.open_uri_error));
                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            final TextView mUHide = holder.user_hide;
            holder.user_hide.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mUHide.getText().toString().compareTo("Hide")==0)
                    {
                        mProgress.setVisibility(View.VISIBLE);
                        ApiManager.hideUserFromLiveAction(mCtx, mLiveModels.get(position).getUserInfo().getUserID(), 1, new ApiManager.RequestCallback() {
                            @Override
                            public void onResult(boolean success) {
                                mProgress.setVisibility(View.GONE);
                                if (success) {
                                    try{
//                          showToast(getString(R.string.done));
                                        Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                    mUHide.setText("unHide");
                                } else {
                                    try{
//                          showToast(getString(R.string.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        mProgress.setVisibility(View.VISIBLE);
                        ApiManager.hideUserFromLiveAction(mCtx, mLiveModels.get(position).getUserInfo().getUserID(), 0, new ApiManager.RequestCallback() {
                            @Override
                            public void onResult(boolean success) {
                                mProgress.setVisibility(View.GONE);
                                if (success) {
                                    try{
//                          showToast(getString(R.string.done));
                                        Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                    mUHide.setText("Hide");
                                } else {
                                    try{
//                          showToast(getString(R.string.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                }
            });

            holder.user_freeze.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                    builder.setMessage(getString(R.string.user_freeze));
                    builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mProgress.setVisibility(View.VISIBLE);
                            ApiManager.freezeUserAction(mCtx, mLiveModels.get(position).getUserInfo().getUserID(), "", new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    mProgress.setVisibility(View.GONE);
                                    if (success) {
                                        try{
//                          showToast(getString(R.string.done));
                                            Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                    } else {
                                        try{
//                          showToast(getString(R.string.error_failed));
                                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                        }
                                        catch (Exception x){
                                        }
                                    }
                                }
                            });
                        }
                    });
                    builder.setNeutralButton(R.string.cancel, null);
                    builder.create().show();
                }
            });

            final TextView mUVerified = holder.user_verified;
            holder.user_verified.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mUVerified.getText().toString().compareTo("Verified")==0)
                    {
                        mProgress.setVisibility(View.VISIBLE);
                        ApiManager.verifyUserAction(mCtx, mLiveModels.get(position).getUserInfo().getUserID(), new ApiManager.RequestCallback() {
                            @Override
                            public void onResult(boolean success)
                            {
                                mProgress.setVisibility(View.GONE);
                                if (success)
                                {
                                    try{
//                          showToast(getString(R.string.done));
                                        Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                    mUVerified.setText("unVerified");

                                    mLiveModels.get(position).getUserInfo().setIsVerified(1);
                                    mFriendAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    try{
//                          showToast(getString(R.string.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        mProgress.setVisibility(View.VISIBLE);
                        ApiManager.removeVerifiedUserAction(mCtx, mLiveModels.get(position).getUserInfo().getUserID(), new ApiManager.RequestCallback() {
                            @Override
                            public void onResult(boolean success) {
                                mProgress.setVisibility(View.GONE);
                                if (success) {
                                    try{
//                          showToast(getString(R.string.done));
                                        Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                    mUVerified.setText("Verified");

                                    mLiveModels.get(position).getUserInfo().setIsVerified(0);
                                    mFriendAdapter.notifyDataSetChanged();
                                } else {
                                    try{
//                          showToast(getString(R.string.error_failed));
                                        Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                    }
                                    catch (Exception x){
                                    }
                                }
                            }
                        });
                    }
                }
            });

            holder.live_hide.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    mProgress.setVisibility(View.VISIBLE);
                    ApiManager.hideLiveStream(mCtx, mLiveModels.get(position).getLiveStreamID(), new ApiManager.RequestCallback() {
                        @Override
                        public void onResult(boolean success) {
                            mProgress.setVisibility(View.GONE);
                            if (success) {
                                try{
//                          showToast(getString(R.string.done));
                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }

                                mLiveModels.remove(position);
                                mFriendAdapter.notifyDataSetChanged();
                            } else {
                                try{
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                    });
                }
            });

            holder.live_pass.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    mProgress.setVisibility(View.VISIBLE);
                    ApiManager.passLiveStream(mCtx, mLiveModels.get(position).getLiveStreamID(), new ApiManager.RequestCallback() {
                        @Override
                        public void onResult(boolean success) {
                            mProgress.setVisibility(View.GONE);
                            if (success) {
                                try{
//                          showToast(getString(R.string.done));
                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }

                                mLiveModels.remove(position);
                                mFriendAdapter.notifyDataSetChanged();
                            } else {
                                try{
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                        }
                    });
                }
            });

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView self;
        TextView name;
        ImageView verifie;

        TextView day;
        FeedTagTextView dio;
        TextView live_text;
        TextView view_text;

        TextView user_hide;
        TextView user_freeze;
        TextView user_verified;

        TextView live_hide;
        TextView live_pass;
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private class CountAdapter extends BaseAdapter
    {
        private CountAdapter mAdapter;

        private CountAdapter()
        {
            mAdapter = this;
        }

        @Override
        public int getCount()
        {
            return mAllCount.length;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            PickerViewHolder holder = new PickerViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.review_picker_row, null);
                holder.picker_layout = (LinearLayout) convertView.findViewById(R.id.picker_layout);
                holder.picker_selset = (ImageView) convertView.findViewById(R.id.picker_selset);
                holder.picker_text = (TextView) convertView.findViewById(R.id.picker_text);

                convertView.setTag(holder);
            }
            else
            {
                holder = (PickerViewHolder) convertView.getTag();
            }

            holder.picker_text.setText(String.valueOf(mAllCount[position]));

            if(mCount == position)
            {
                holder.picker_selset.setImageResource(R.drawable.select_on);
            }
            else
            {
                holder.picker_selset.setImageResource(R.drawable.select_off);
            }

            final int pos = position;
            holder.picker_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    mCount =  pos;
                    mAdapter.notifyDataSetChanged();

                    if(mAllPos!=null) mAllPos = null;
                    mAllPos = new int[mCount+1];
                    for(int p = 1 ; p < (mAllPos.length + 1) ; p++)
                    {
                        mAllPos[p-1] = p;
                    }
                    mPosAdapter.notifyDataSetChanged();
                }
            });

            return convertView;
        }
    }

    private class PickerViewHolder
    {
        LinearLayout picker_layout;
        ImageView picker_selset;
        TextView picker_text;
    }

    private class PosAdapter extends BaseAdapter
    {

        @Override
        public int getCount()
        {
            return mAllPos.length;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            PosViewHolder holder = new PosViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.review_picker_row, null);
                holder.picker_layout = (LinearLayout) convertView.findViewById(R.id.picker_layout);
                holder.picker_selset = (ImageView) convertView.findViewById(R.id.picker_selset);
                holder.picker_text = (TextView) convertView.findViewById(R.id.picker_text);

                convertView.setTag(holder);
            }
            else
            {
                holder = (PosViewHolder) convertView.getTag();
            }

            holder.picker_text.setText(String.valueOf(mAllPos[position]));

            if(mPos == position)
            {
                holder.picker_selset.setImageResource(R.drawable.select_on);
            }
            else
            {
                holder.picker_selset.setImageResource(R.drawable.select_off);
            }

            final int pos = position;
            holder.picker_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    mPos = pos;
                    mPosAdapter.notifyDataSetChanged();
                }
            });

            return convertView;
        }
    }

    private class PosViewHolder
    {
        LinearLayout picker_layout;
        ImageView picker_selset;
        TextView picker_text;
    }
}
