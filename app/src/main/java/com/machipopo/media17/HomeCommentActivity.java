package com.machipopo.media17;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.CommentModel;
import com.machipopo.media17.model.TagModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by POPO on 5/28/15.
 */
public class HomeCommentActivity extends BaseActivity
{
    private HomeCommentActivity mCtx = this;
    public Story17Application mApplication;
    private LayoutInflater inflater;
    private String mPostID = "";
    private String mUserID = "";

    private ListView mList, mTagList;
    private ProgressBar mProgress;
    private EditText mEdit;
    private Button mSend;

    private listAdapter mListAdapter;
    private TabAdapter mTabAdapter;
    private TagAdapter mTagAdapter;

    private ArrayList<CommentModel> mCommentModel = new ArrayList<CommentModel>();

    private ArrayList<UserModel> mUserModel = new ArrayList<UserModel>();
    private ArrayList<TagModel> mTagModel = new ArrayList<TagModel>();

    private Boolean mTagState = false;
    private int mTagPos = 0;
    private Boolean mWho = false;
    private JSONArray mJSONArray;

    private int taged = 0;
    private Boolean tagedState = false;

    private DisplayImageOptions SelfOptions;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_comment_activity);

        mApplication = (Story17Application) mCtx.getApplication();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("post_id")) mPostID = mBundle.getString("post_id");
            if(mBundle.containsKey("user_id")) mUserID = mBundle.getString("user_id");
        }

        mList = (ListView) findViewById(R.id.comment_list);
        mTagList = (ListView) findViewById(R.id.tag_list);
        mEdit = (EditText) findViewById(R.id.edit);
        mSend = (Button) findViewById(R.id.send);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id)
            {
                try
                {
                    if(mCommentModel.get(position).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                        builder.setMessage(getString(R.string.comment_delete));
                        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String CommentID = mCommentModel.get(position).getCommentID();

                                mCommentModel.remove(position);
                                mListAdapter.notifyDataSetChanged();
                                mList.smoothScrollToPosition(mCommentModel.size());

                                ApiManager.deleteComment(mCtx, mPostID, CommentID, new ApiManager.DeleteCommentCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {
                                            ApiManager.getComments(mCtx, mPostID, Integer.MAX_VALUE, 500, new ApiManager.GetCommentsCallback()
                                            {
                                                @Override
                                                public void onResult(boolean success, String message, ArrayList<CommentModel> commentModel)
                                                {
                                                    if (success && commentModel!=null)
                                                    {
                                                        mCommentModel.clear();
                                                        mCommentModel.addAll(commentModel);
                                                        Collections.reverse(mCommentModel);

                                                        mListAdapter = new listAdapter();
                                                        mList.setAdapter(mListAdapter);
                                                        mList.smoothScrollToPosition(mCommentModel.size());
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, null);
                        builder.create().show();
                    }
                    else
                    {
                        if(mUserID.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                            builder.setMessage(getString(R.string.comment_delete));
                            builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String CommentID = mCommentModel.get(position).getCommentID();

                                    mCommentModel.remove(position);
                                    mListAdapter.notifyDataSetChanged();
                                    mList.smoothScrollToPosition(mCommentModel.size());

                                    ApiManager.deleteComment(mCtx, mPostID, CommentID, new ApiManager.DeleteCommentCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {
                                                ApiManager.getComments(mCtx, mPostID, Integer.MAX_VALUE, 500, new ApiManager.GetCommentsCallback()
                                                {
                                                    @Override
                                                    public void onResult(boolean success, String message, ArrayList<CommentModel> commentModel)
                                                    {
                                                        if (success && commentModel!=null)
                                                        {
                                                            mCommentModel.clear();
                                                            mCommentModel.addAll(commentModel);
                                                            Collections.reverse(mCommentModel);

                                                            mListAdapter = new listAdapter();
                                                            mList.setAdapter(mListAdapter);
                                                            mList.smoothScrollToPosition(mCommentModel.size());
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                            builder.setNegativeButton(R.string.cancel, null);
                            builder.create().show();
                        }
                    }
                }
                catch (Exception e)
                {

                }

                return false;
            }
        });

        mJSONArray = new JSONArray();

        if(mPostID.length()!=0)
        {
            ApiManager.getComments(mCtx, mPostID, Integer.MAX_VALUE, 500, new ApiManager.GetCommentsCallback()
            {
                @Override
                public void onResult(boolean success, String message, ArrayList<CommentModel> commentModel)
                {
                    mProgress.setVisibility(View.GONE);

                    if (success && commentModel!=null)
                    {
                        mCommentModel.addAll(commentModel);
                        Collections.reverse(mCommentModel);

                        mListAdapter = new listAdapter();
                        mList.setAdapter(mListAdapter);
                    }
                    else {
                        try{
//                                                showToast(getString(R.string.failed));
                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e){
                        }
                    }
                }
            });
        }

        mEdit.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, final int start, int before, int count)
            {
                if(s.toString().length() < taged) tagedState = false;

                if(!tagedState)
                {
                    if(s.toString().contains("#") || s.toString().contains("@")) mTagState = true;
                    else
                    {
                        mTagList.setVisibility(View.GONE);
                        mTagState = false;
                        if(mJSONArray!=null)
                        {
                            mJSONArray = null;
                            mJSONArray = new JSONArray();
                        }
                    }

                    if(mTagState)
                    {
                        if(s.toString().substring(start,s.toString().length()).contains("#"))
                        {
                            mTagPos = start;
                            mWho = false;
                        }
                        else if(s.toString().substring(start,s.toString().length()).contains("@"))
                        {
                            mTagPos = start;
                            mWho = true;
                        }
                        else
                        {
                            final String tag = s.toString().substring(mTagPos + 1, s.toString().length());

                            if(tag.length()!=0)
                            {
                                if (!mWho)
                                {
                                    //Search #
                                    ApiManager.searchHashTag(mCtx, tag, 0, 100, new ApiManager.SearchHashTagCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message, ArrayList<TagModel> tagModel)
                                        {
                                            if (success && tagModel!=null)
                                            {
                                                if(tagModel.size()!=0)
                                                {
                                                    mTagModel.clear();
                                                    mTagModel.addAll(tagModel);
//                                                Log.d("123", "tag size : " + mTagModel.size());
                                                    mTagAdapter = new TagAdapter(mTagPos+1);
                                                    mTagList.setAdapter(mTagAdapter);
                                                    mTagList.setVisibility(View.VISIBLE);
                                                }
                                                else
                                                {
                                                    mTagList.setVisibility(View.GONE);
                                                }
                                            }
                                            else {
                                                try{
//                                                showToast(getString(R.string.search_failed));
                                                    Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    //Search @ 1
                                    ApiManager.getSearchUsers(mCtx, tag, 0, 100, 1, new ApiManager.GetSearchUsersCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message, ArrayList<UserModel> userModel)
                                        {
                                            if (success && userModel!=null)
                                            {
                                                if(userModel.size()!=0)
                                                {
                                                    mUserModel.clear();
                                                    mUserModel.addAll(userModel);
//                                                Log.d("123", "user size : " + mUserModel.size());
                                                    mTabAdapter = new TabAdapter(mTagPos+1);
                                                    mTagList.setAdapter(mTabAdapter);
                                                    mTagList.setVisibility(View.VISIBLE);
                                                }
                                                else
                                                {
                                                    mTagList.setVisibility(View.GONE);
                                                }
                                            }
                                            else {
                                                try{
//                                                showToast(getString(R.string.search_failed));
                                                    Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                else
                {
                    s = s.toString().substring(taged,s.length());
                    if(s.toString().contains("#") || s.toString().contains("@")) mTagState = true;
                    else
                    {
                        mTagList.setVisibility(View.GONE);
                        mTagState = false;
                    }

                    if(mTagState)
                    {
                        if(s.toString().substring(start-taged,s.toString().length()).contains("#"))
                        {
                            mTagPos = start-taged;
                            mWho = false;
                        }
                        else if(s.toString().substring(start-taged,s.toString().length()).contains("@"))
                        {
                            mTagPos = start-taged;
                            mWho = true;
                        }
                        else
                        {
                            final String tag = s.toString().substring(mTagPos + 1, s.toString().length());

                            if(tag.length()!=0)
                            {
                                if (!mWho)
                                {
                                    //Search #
                                    ApiManager.searchHashTag(mCtx, tag, 0, 100, new ApiManager.SearchHashTagCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message, ArrayList<TagModel> tagModel)
                                        {
                                            if (success && tagModel!=null)
                                            {
                                                if(tagModel.size()!=0)
                                                {
                                                    mTagModel.clear();
                                                    mTagModel.addAll(tagModel);
//                                                Log.d("123", "tag size : " + mTagModel.size());
                                                    mTagAdapter = new TagAdapter(start);
                                                    mTagList.setAdapter(mTagAdapter);
                                                    mTagList.setVisibility(View.VISIBLE);
                                                }
                                                else
                                                {
                                                    mTagList.setVisibility(View.GONE);
                                                }
                                            }
                                            else {
                                                try{
//                                                showToast(getString(R.string.search_failed));
                                                    Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    //Search @ 1
                                    ApiManager.getSearchUsers(mCtx, tag, 0, 100, 1, new ApiManager.GetSearchUsersCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message, ArrayList<UserModel> userModel)
                                        {
                                            if (success && userModel!=null)
                                            {
                                                if(userModel.size()!=0)
                                                {
                                                    mUserModel.clear();
                                                    mUserModel.addAll(userModel);
//                                                Log.d("123", "user size : " + mUserModel.size());
                                                    mTabAdapter = new TabAdapter(start);
                                                    mTagList.setAdapter(mTabAdapter);
                                                    mTagList.setVisibility(View.VISIBLE);
                                                }
                                                else
                                                {
                                                    mTagList.setVisibility(View.GONE);
                                                }
                                            }
                                            else {
                                                try{
//                                                showToast(getString(R.string.search_failed));
                                                    Toast.makeText(mCtx, getString(R.string.search_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception e){
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEdit.getText().toString().length() != 0) {

                    //event tracking
                    try{
                        LogEventUtil.CommentPost(mCtx,mApplication,mUserID,mEdit.getText().toString());
                    }catch (Exception x)
                    {

                    }

                    try {

                        String PostText = mEdit.getText().toString() + " ";
                        ArrayList<Integer> mPos = new ArrayList<Integer>();

                        for(int k = 0 ; k < mEdit.getText().toString().length() ; k++)
                        {
                            if(mEdit.getText().toString().substring(k,k+1).compareTo("@")==0)
                            {
                                if(k!=0)
                                {
                                    if(mEdit.getText().toString().substring(k-1,k).compareTo(" ")!=0)
                                    {
                                        PostText = PostText.substring(0,k + (mPos.size())) + " " + PostText.substring(k + (mPos.size()),PostText.length());
                                        mPos.add(k+ (mPos.size()));
                                    }
                                }
                            }
                            else if(mEdit.getText().toString().substring(k,k+1).compareTo("#")==0)
                            {
                                if(k!=0)
                                {
                                    if(mEdit.getText().toString().substring(k-1,k).compareTo(" ")!=0)
                                    {
                                        PostText = PostText.substring(0,k + (mPos.size())) + " " + PostText.substring(k + (mPos.size()),PostText.length());
                                        mPos.add(k+ (mPos.size()));
                                    }
                                }
                            }
                        }

                        ArrayList<Integer> mCheck = new ArrayList<Integer>();

                        for(int p = 0 ; p < PostText.length() ; p++)
                        {
                            if(PostText.substring(p,p+1).compareTo("@")==0)
                            {
                                mCheck.add(p);
                            }
                            else if(PostText.substring(p,p+1).compareTo("#")==0)
                            {
                                mCheck.add(p);
                            }
                        }

                        if(mCheck.size()!=0)
                        {
                            if (mJSONArray != null)
                            {
                                mJSONArray = null;
                                mJSONArray = new JSONArray();
                            }

                            for(int t = 0 ; t < mCheck.size() ; t++)
                            {
                                for(int w = (mCheck.get(t)+1) ; w < PostText.length() ; w++)
                                {
                                    if(PostText.substring(w,w+1).compareTo(" ")==0)
                                    {
                                        if(PostText.substring(mCheck.get(t),mCheck.get(t)+1).compareTo("@")==0)
                                        {
                                            mJSONArray.put(PostText.substring(mCheck.get(t)+1,w));
//                                        Log.d("123","user tag : " + PostText.substring(mCheck.get(t)+1,w));
                                        }
//                                        else
//                                        {
//                                            mTagArray.put(PostText.substring(mCheck.get(t)+1,w));
//                                        Log.d("123","hash tag : " + PostText.substring(mCheck.get(t)+1,w));
//                                        }

                                        break;
                                    }
                                }
                            }
                        }

                        CommentModel commentModel = new CommentModel();
                        commentModel.setComment(PostText);
                        commentModel.setTimestamp(Singleton.getCurrentTimestamp());
                        UserModel user = new UserModel();
                        user.setPicture(Singleton.preferences.getString(Constants.PICTURE, ""));
                        user.setOpenID(Singleton.preferences.getString(Constants.OPEN_ID, ""));
                        user.setUserID(Singleton.preferences.getString(Constants.USER_ID, ""));
                        commentModel.setUserInfo(user);

                        mCommentModel.add(commentModel);
                        mListAdapter.notifyDataSetChanged();
                        mList.smoothScrollToPosition(mCommentModel.size());

                        ApiManager.commentPost(mCtx, mPostID, PostText, mJSONArray.toString(), new ApiManager.CommentPostCallback() {
                            @Override
                            public void onResult(boolean success, String message)
                            {
                                if(success)
                                {
                                    ApiManager.getComments(mCtx, mPostID, Integer.MAX_VALUE, 500, new ApiManager.GetCommentsCallback()
                                    {
                                        @Override
                                        public void onResult(boolean success, String message, ArrayList<CommentModel> commentModel)
                                        {
                                            if (success && commentModel!=null)
                                            {
                                                mCommentModel.clear();
                                                mCommentModel.addAll(commentModel);
                                                Collections.reverse(mCommentModel);

                                                mListAdapter = new listAdapter();
                                                mList.setAdapter(mListAdapter);
                                                mList.smoothScrollToPosition(mCommentModel.size());
                                            }
                                        }
                                    });
                                }
                            }
                        });

                        mEdit.setText("");
                    } catch(Exception e) {

                    }
                } else {
                    try{
//                                                showToast(getString(R.string.edit));
                        Toast.makeText(mCtx, getString(R.string.edit), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                    }
                };
            }
        });

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.comment));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mApplication.setCommentCount(mCommentModel.size());
                mCtx.finish();
            }
        });
    }

    private class listAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mCommentModel.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_comment_row, null);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.comment = (TextView) convertView.findViewById(R.id.comment);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.name.setText(mCommentModel.get(position).getUserInfo().getOpenID());
            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mCommentModel.get(position).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mCommentModel.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mCommentModel.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mCommentModel.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mCommentModel.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mCommentModel.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mCommentModel.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mCommentModel.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mCommentModel.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mCommentModel.get(position).getUserID());
                        intent.putExtra("web", mCommentModel.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.day.setText(Singleton.getElapsedTimeString(mCommentModel.get(position).getTimestamp()));
            holder.comment.setText(String.valueOf(mCommentModel.get(position).getComment()));

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mCommentModel.get(position).getUserInfo().getPicture()), holder.img, SelfOptions);
            holder.img.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mCommentModel.get(position).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mCommentModel.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mCommentModel.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mCommentModel.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mCommentModel.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mCommentModel.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mCommentModel.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mCommentModel.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mCommentModel.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mCommentModel.get(position).getUserID());
                        intent.putExtra("web", mCommentModel.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            if(mCommentModel.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            return convertView;
        }
    };

    private class ViewHolder
    {
        ImageView img;
        TextView name;
        TextView comment;
        TextView day;
        ImageView verifie;
    }

    private class TabAdapter extends BaseAdapter
    {
        private int mPos;

        public TabAdapter(int pos)
        {
            mPos = pos;
        }

        @Override
        public int getCount()
        {
            return mUserModel.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderUser holder = new ViewHolderUser();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.tag_user_row, null);
                holder.user_layout = (LinearLayout) convertView.findViewById(R.id.user_layout);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                holder.bio = (TextView) convertView.findViewById(R.id.bio);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderUser) convertView.getTag();

            holder.name.setText(mUserModel.get(position).getOpenID());
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUserModel.get(position).getPicture()), holder.img,SelfOptions);
            holder.user_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    taged = mPos + mUserModel.get(position).getOpenID().length() + 1;
                    tagedState = true;
                    mJSONArray.put(mUserModel.get(position).getOpenID());
                    String s = mEdit.getText().toString().substring(0,mPos);
                    mEdit.setText(s + mUserModel.get(position).getOpenID() + " ");
                    mEdit.setSelection(mEdit.getText().toString().length());
                    mTagList.setVisibility(View.GONE);
                    mTagState = false;
                }
            });

            if(mUserModel.get(position).getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.bio.setVisibility(View.VISIBLE);
            holder.bio.setText(mUserModel.get(position).getName());

            return convertView;
        }
    };

    private class ViewHolderUser
    {
        LinearLayout user_layout;
        ImageView img;
        TextView name;
        ImageView verifie;
        TextView bio;
    }

    private class TagAdapter extends BaseAdapter
    {
        private int mPos;

        public TagAdapter(int pos)
        {
            mPos = pos;
        }

        @Override
        public int getCount()
        {
            return mTagModel.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderTag holder = new ViewHolderTag();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.tag_user_row, null);
                holder.user_layout= (LinearLayout) convertView.findViewById(R.id.user_layout);
                holder.img_layout = (RelativeLayout) convertView.findViewById(R.id.img_layout);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.count = (TextView) convertView.findViewById(R.id.count);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderTag) convertView.getTag();

            holder.img_layout.setVisibility(View.GONE);
            holder.name.setText("#" + mTagModel.get(position).getHashTag());
            holder.user_layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    taged = mPos + mTagModel.get(position).getHashTag().length() + 1;
                    tagedState = true;
                    String s = mEdit.getText().toString().substring(0,mPos);
                    mEdit.setText(s + mTagModel.get(position).getHashTag() + " ");
                    mEdit.setSelection(mEdit.getText().toString().length());
                    mTagList.setVisibility(View.GONE);
                    mTagState = false;
                }
            });

            holder.count.setVisibility(View.VISIBLE);
            holder.count.setText(String.valueOf(mTagModel.get(position).getPostCount()));

            return convertView;
        }
    };

    private class ViewHolderTag
    {
        LinearLayout user_layout;
        RelativeLayout img_layout;
        TextView name;
        TextView count;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if(mTagState)
            {
                mTagList.setVisibility(View.GONE);
                mTagState = false;

//                if(mJSONArray!=null)
//                {
//                    mJSONArray = null;
//                    mJSONArray = new JSONArray();
//                }
            }
            else
            {
                mApplication.setCommentCount(mCommentModel.size());
                mCtx.finish();
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
