package com.machipopo.media17;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.machipopo.media17.model.PageDurationModel;
import com.machipopo.media17.model.RevenueModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.umeng.analytics.MobclickAgent;

import java.sql.Time;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by POPO on 6/10/15.
 */
public class RevenueActivity extends BaseNewActivity
{
    private RevenueActivity mCtx = this;
    private Story17Application mApplication;

    private TextView mTotal,mToday,mMonth,mGetMoney;

    private ArrayList<RevenueModel> mRevenueModel = new ArrayList<RevenueModel>();

    private LinearLayout mNoinfo, mMonthLayout, mDataLayout, mPostLayout, mLiveLayout, mAccountLayout, mMoneyLayout,mShow_getmoney,mPresendLayout;

    private PageDurationModel mStartPage, mEndPage;
    private int duration;

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();

        initTitleBar();

        mTotal = (TextView) findViewById(R.id.total);
        mToday = (TextView) findViewById(R.id.today);
        mMonth = (TextView) findViewById(R.id.month);
        mGetMoney = (TextView) findViewById(R.id.get_money);

        mNoinfo = (LinearLayout) findViewById(R.id.noinfo);
        mMonthLayout = (LinearLayout) findViewById(R.id.month_layout);
        mDataLayout = (LinearLayout) findViewById(R.id.data_layout);
        mPostLayout = (LinearLayout) findViewById(R.id.post_layout);
        mAccountLayout = (LinearLayout) findViewById(R.id.account_layout);
        mMoneyLayout = (LinearLayout) findViewById(R.id.money_layout);
        mShow_getmoney = (LinearLayout) findViewById(R.id.show_getmoney);
        mLiveLayout = (LinearLayout) findViewById(R.id.live_layout);
        mPresendLayout = (LinearLayout) findViewById(R.id.presend_layout);

        //event tracking to get the duration that user stays in the page.
        mStartPage = new PageDurationModel();
        getCurrentTime(mStartPage);

        mRevenueModel.addAll(mApplication.getRevenueModels());

        String c = Singleton.getCurrencyType();
        double total = (double) getMoneyConfig(Constants.REVENUE_MONEY_TOTAL,0f) *Singleton.getCurrencyRate();
        double today = (double) getMoneyConfig(Constants.REVENUE_TODAY,0f) *Singleton.getCurrencyRate();
        DecimalFormat df=new DecimalFormat("#.####");

        if(Constants.INTERNATIONAL_VERSION) {
            mTotal.setText("$ " + df.format(total) + " " + c);
            mToday.setText("$ " + df.format(today) + " " + c);
            mGetMoney.setText("$ " + df.format(total) + " " + c);
        }
        else{
            if(c.compareTo("CNY")==0){
                mTotal.setText("¥ " + df.format(total));
                mToday.setText("¥ " + df.format(today));
                mGetMoney.setText("¥ " + df.format(total));
            }
            else{
                mTotal.setText("$ " + df.format(total) + " " + c);
                mToday.setText("$ " + df.format(today) + " " + c);
                mGetMoney.setText("$ " + df.format(total) + " " + c);
            }
        }


        Calendar mCalendar = new GregorianCalendar();
        int year = mCalendar.get(Calendar.YEAR) ;
        int mon = mCalendar.get(Calendar.MONTH) + 1 ;
        float monMoney = 0;

        for(int i = 0 ; i < mRevenueModel.size() ; i++)
        {
            if(mRevenueModel.get(i).getYear() == year)
            {
                if(mRevenueModel.get(i).getMonth() == mon)
                {
                    monMoney = monMoney + mRevenueModel.get(i).getRevenue();
                }
            }
        }

        float NewmonMoney = monMoney*Singleton.getCurrencyRate();

        if(Constants.INTERNATIONAL_VERSION) {
            mMonth.setText("$ " + df.format((double)NewmonMoney) + " " + c);
        }
        else{
            if(c.compareTo("CNY")==0){
                mMonth.setText("¥ " + df.format((double)NewmonMoney));
            }
            else{
                mMonth.setText("$ " + df.format((double)NewmonMoney) + " " + c);
            }
        }

        if(Singleton.preferences.getString(Constants.PAY_COUNTRY_CODE, "TW").compareTo("TW")==0)
        {
            if(getConfig(Constants.REVENUE_NAME).length()!=0 && getConfig(Constants.REVENUE_ID).length()!=0 && getConfig(Constants.REVENUE_BANK_NAME).length()!=0 && getConfig(Constants.REVENUE_BANK_BRANCH).length()!=0 && getConfig(Constants.REVENUE_BANK_NUMBER).length()!=0 && getConfig(Constants.REVENUE_ACCOUNT_NAME).length()!=0 && getConfig(Constants.REVENUE_ID_FRONT).length()!=0 && getConfig(Constants.REVENUE_ID_BACK).length()!=0 && getConfig(Constants.REVENUE_ACCOUNT_PICTURE).length()!=0) mNoinfo.setVisibility(View.GONE);
            else mNoinfo.setVisibility(View.VISIBLE);
        }
        else
        {
            if(getConfig(Constants.REVENUE_PAYPAL).length()!=0 || getConfig(Constants.REVENUE_ALIPAY).length()!=0) mNoinfo.setVisibility(View.GONE);
            else mNoinfo.setVisibility(View.VISIBLE);
        }

        mNoinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Constants.INTERNATIONAL_VERSION)
                {
//                if(Singleton.preferences.getString(Constants.PAY_COUNTRY_CODE, "TW").compareTo("TW")==0)
//                {
//                    Intent intent = new Intent();
//                    intent.setClass(mCtx, RevenueAccountActivity.class);
//                    startActivity(intent);
//                }
//                else
//                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, RevenuePayActivity.class);
                    startActivity(intent);
//                }
                }
                else
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx,RevenuePaySetActivity.class);
                    intent.putExtra("paypal", false);
                    startActivity(intent);
                }
            }
        });

        mMonthLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueMonthActivity.class);
                startActivity(intent);
            }
        });

        mDataLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueDateActivity.class);
                startActivity(intent);
            }
        });

        mPostLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenuePostActivity.class);
                startActivity(intent);
            }
        });

        mLiveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueLiveActivity.class);
                startActivity(intent);
            }
        });

        mAccountLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(Constants.INTERNATIONAL_VERSION)
                {
//                if(Singleton.preferences.getString(Constants.PAY_COUNTRY_CODE, "TW").compareTo("TW")==0)
//                {
//                    Intent intent = new Intent();
//                    intent.setClass(mCtx, RevenueAccountActivity.class);
//                    startActivity(intent);
//                }
//                else
//                {
                    if(getConfig(Constants.REVENUE_PAYPAL).length()!=0 || getConfig(Constants.REVENUE_ALIPAY).length()!=0)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx,RevenuePayChooseActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx,RevenuePayActivity.class);
                        startActivity(intent);
                    }
//                }
                }
                else
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx,RevenuePaySetActivity.class);
                    intent.putExtra("paypal",false);
                    startActivity(intent);
                }
            }
        });

        mMoneyLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,RevenueMoneyActivity.class);
                startActivity(intent);
            }
        });

        mShow_getmoney.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,HtmlActivity.class);
                startActivity(intent);
            }
        });

        mPresendLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx,MyPresendActivity.class);
                startActivity(intent);
            }
        });

        if(!mApplication.getIsgetMoney())
        {
            mApplication.getRevenueMoney(this, Singleton.preferences.getString(Constants.USER_ID, ""));
            mApplication.setIsgetMoney(true);
        }

        //event tracking
        try{
            LogEventUtil.EnterRevenuePage(mCtx,mApplication);
        }
        catch (Exception x)
        {

        }
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_all_data));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    public Float getMoneyConfig(String key, Float def)
    {
        SharedPreferences settings = getSharedPreferences("settings", 0);
        return settings.getFloat(key, def);
    }

    public String getConfig(String key)
    {
        SharedPreferences settings = getSharedPreferences("settings",0);
        return settings.getString(key, "");
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mNoinfo!=null)
        {
            if(Singleton.preferences.getString(Constants.PAY_COUNTRY_CODE, "TW").compareTo("TW")==0)
            {
                if(getConfig(Constants.REVENUE_NAME).length()!=0 && getConfig(Constants.REVENUE_ID).length()!=0 && getConfig(Constants.REVENUE_BANK_NAME).length()!=0 && getConfig(Constants.REVENUE_BANK_BRANCH).length()!=0 && getConfig(Constants.REVENUE_BANK_NUMBER).length()!=0 && getConfig(Constants.REVENUE_ACCOUNT_NAME).length()!=0 && getConfig(Constants.REVENUE_ID_FRONT).length()!=0 && getConfig(Constants.REVENUE_ID_BACK).length()!=0 && getConfig(Constants.REVENUE_ACCOUNT_PICTURE).length()!=0) mNoinfo.setVisibility(View.GONE);
                else mNoinfo.setVisibility(View.VISIBLE);
            }
            else
            {
                if(getConfig(Constants.REVENUE_PAYPAL).length()!=0 || getConfig(Constants.REVENUE_ALIPAY).length()!=0) mNoinfo.setVisibility(View.GONE);
                else mNoinfo.setVisibility(View.VISIBLE);
            }
        }

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    @Override
    public void onDestroy()
    {
        mEndPage = new PageDurationModel();
        getCurrentTime(mEndPage);
        //如果日期在同一天，把startpage和endpage的時間換算成秒數相減;不同天就直接相加
        if(mStartPage.getDay() == mEndPage.getDay())
        {
            duration = (mEndPage.getHour() * 60 * 60 + mEndPage.getMinute() * 60 + mEndPage.getSecond())-(mStartPage.getHour() * 60 * 60 + mStartPage.getMinute() * 60 + mStartPage.getSecond());
        }
        else
        {
            duration = (mEndPage.getHour() * 60 * 60 + mEndPage.getMinute() * 60 + mEndPage.getSecond())+ (24*60*60 - (mStartPage.getHour() * 60 * 60 + mStartPage.getMinute() * 60 + mStartPage.getSecond()));
        }

        LogEventUtil.LeaveRevenuePage(mCtx,mApplication,duration);

        super.onDestroy();
    }

    void getCurrentTime(PageDurationModel pageTime)
    {
        Calendar mCalendar = new GregorianCalendar();

        pageTime.setDay(mCalendar.get(Calendar.DAY_OF_MONTH));
        pageTime.setHour(mCalendar.get(Calendar.HOUR));
        pageTime.setMinute(mCalendar.get(Calendar.MINUTE));
        pageTime.setSecond(mCalendar.get(Calendar.SECOND));
    }
}
