package com.machipopo.media17;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media.FloatPCMAudioRecorder;
import com.machipopo.media.OpusCodec;
import com.machipopo.media.PCMAudioRecorder;
import com.machipopo.media.VP8Codec;
import com.machipopo.media.VideoRenderer;
import com.machipopo.media17.View.GiftView;
import com.machipopo.media17.model.GiftLeaderboardModel;
import com.machipopo.media17.model.GiftLivestreamLeaderBoardModel;
import com.machipopo.media17.model.GiftModel;
import com.machipopo.media17.model.LiveComment;
import com.machipopo.media17.model.LiveGiftsModel;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.SubscribeOptionModel;
import com.machipopo.media17.model.SubscriberUserModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.LogEventUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.plattysoft.leonids.ParticleSystem;
import com.plattysoft.leonids.modifiers.ScaleModifier;
import com.umeng.analytics.MobclickAgent;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class LiveBroadcastActivity extends BaseActivity implements PCMAudioRecorder.PCMAudioRecorderDelegate, SurfaceHolder.Callback, Camera.PreviewCallback, GiftView.GiftEndListener{

    @Override
    public void OnGiftEnd()
    {
        //leaderboard
        if(isInLeaderboard) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    isInLeaderboard = false;
                    LeaderboardLog.show();

                }
            });

            return;
        }
        mGiftsSummary++;

        mGiftShowState = false;

        mLiveGiftsModels.remove(0);

        if(mLiveGiftsModels.size()!=0)
        {
            mGiftShowState = true;

            File file = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
            if (file.exists())
            {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mGiftComment.setVisibility(View.GONE);
                        mBestGiftComment.setVisibility(View.GONE);
                        mGiftCommentBest.setVisibility(View.GONE);
                        mGiftAni.removeAllViews();
                        GiftView mGiftView = new GiftView(mCtx, mLiveGiftsModels.get(0).getGiftInfo().getFrameDuration(), mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels, mLiveGiftsModels.get(0).getGiftInfo().getGiftID(), mLiveGiftsModels.get(0).getGiftInfo().getNumOfImages(),mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
                        mGiftView.mGiftEndListener = mCtx;
                        mGiftAni.addView(mGiftView);
                        showGiftComment(mLiveGiftsModels.get(0).getGiftInfo(), mLiveGiftsModels.get(0).getUserInfo().getOpenID(),mLiveGiftsModels.get(0).getUserInfo().getPicture());

                        try {
                            if(mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack().length()!=0) {
                                File sound = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                if(sound.exists()){
                                    if(mMp3.isPlaying()){
                                        mMp3.stop();
                                    }

                                    mMp3 = null;
                                    mMp3 = new MediaPlayer();
                                    mMp3.setDataSource(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                    mMp3.prepare();
                                    mMp3.start();
                                }
                            }
                        }
                        catch (IOException i) {
                        }
                        catch (Exception e){
                        }
                    }
                });
            }
        }
        else
        {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mGiftComment.setVisibility(View.GONE);
                    mBestGiftComment.setVisibility(View.GONE);
                    mGiftCommentBest.setVisibility(View.GONE);
                }
            });
        }
    }

    private class VideoFrameObject extends Object {

        public VideoFrameObject() {
            yDataByteBuffer = ByteBuffer.allocateDirect(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT);
            uDataByteBuffer = ByteBuffer.allocateDirect(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT / 4);
            vDataByteBuffer = ByteBuffer.allocateDirect(Constants.LIVE_STREAM_FRAME_WIDTH * Constants.LIVE_STREAM_FRAME_HEIGHT / 4);
        }

        public long absTimestamp;
        public int videoFrameByteBufferLength;

        ByteBuffer videoFrameByteBuffer; // encoded data
        ByteBuffer yDataByteBuffer; // decoded data
        ByteBuffer uDataByteBuffer; // decoded data
        ByteBuffer vDataByteBuffer; // decoded data
    }

    private class AudioFrameObject extends Object {

        public AudioFrameObject() {
            audioDataByteBuffer = ByteBuffer.allocateDirect(1920 * 2);
        }

        long absTimestamp;
        public int audioFrameByteBufferLength;

        ByteBuffer audioFrameByteBuffer; // encoded data
        ByteBuffer audioDataByteBuffer; // decoded data
    }

    private class ChunkDataObject extends Object implements Comparable<ChunkDataObject> {

        public ChunkDataObject() {

        }

        public int compareTo(ChunkDataObject c)
        {
            return (chunkID - c.chunkID);
        }

        public int chunkID;
        byte[] chunkDataByteBuffer;
    }

    //TCP
    final byte TCP_COMMENT_CODE_BEGIN = 0x01;
    final byte TCP_COMMENT_CODE_DATA = 0x02;
    final byte TCP_COMMENT_CODE_UPLOAD = 0x03;

    final byte TCP_HEADER = 0x0A;

    // Chunking Params
    final byte PACKET_TYPE_AUDIO = 0x00;
    final byte PACKET_TYPE_VIDEO = 0x01;
    final byte PACKET_TYPE_KEEP_ALIVE = 0x07;
    final byte PACKET_TYPE_NEW_LIVE_CHUNK_READY = 0x0B;

    final byte PACKET_TYPE_LIKE = 0x02;
    final byte PACKET_TYPE_COMMENT = 0x03;
    final byte PACKET_TYPE_BLOCK = 0x04;
    final byte PACKET_TYPE_STREAM_END = 0x05;
    final byte PACKET_TYPE_STREAM_INFO_CHANGE = 0x06;
    final byte PACKET_TYPE_KEEP_ALIVE_PUBLISHER = 0x08;
    final byte PACKET_TYPE_END_WATCH = 0x09;
    final byte PACKET_TYPE_PUBLISHER_DATA_ECHO_BACK = 0x0A;
    final byte PACKET_TYPE_GIFT = 0x0D;

    int liveStreamID = 0;
//    int audioSequenceNumber = 0;
    int videoSequenceNumber = 0;
    long lastReceiveAudioPacketTime = 0;
    long lastReceiveVideoPacketTime = 0;
    int receivedAudioSequenceNumber = 0;
    int receivedVideoSequenceNumber = 0;
    int decodedVideoSequenceNumber = 0;
    int decodedAudioSequenceNumber = 0;

    int lastRequestedChunkID = 0;
    long minChunkStartDecodeTime = 0;
    int lastParsedChunkID;
    long lastReceivedAudioDataTime;
    HashSet<Integer> retriedChunkIDSet = new HashSet<Integer>();

    Camera camera;
    SurfaceView previewView;
    SurfaceHolder surfaceHolder;
    VideoRenderer videoRenderer;

    Timer keepAliveTimer;

    ByteBuffer decodedVideoDataBuffer;
    ByteBuffer encodedVideoDataBuffer;
    ByteBuffer yuvDataBuffer;
    ByteBuffer decodedAudioDataBuffer;
    ByteBuffer audioDataBuffer;

    boolean isUsingFrontCamera = true;

    PCMAudioRecorder pcmAudioRecorder;
//    PCMAudioPlayer pcmAudioPlayer;

    DatagramSocket datagramSocket;

    HandlerThread networkSendHandlerThread;
    Handler networkSendHandler;
    HandlerThread videoEncodeHandlerThread;
    Handler videoEncodeHandler;
    HandlerThread networkReceiveHandlerThread;
    Handler networkReceiveHandler;
    HandlerThread audioRecordHandlerThread;
    Handler audioRecordHandler;
    HandlerThread audioEncodeHandlerThread;
    Handler audioEncodeHandler;
    Handler handler;

    ArrayList<VideoFrameObject> queuedVideoFrameObjects = new ArrayList<VideoFrameObject>();
    ArrayList<ChunkDataObject> receivedChunkDataArray = new ArrayList<ChunkDataObject>();

    private String user="";
    private DisplayImageOptions PeopleOptions;

    private Boolean mAllState = false;

    private int mTimestamp = 0;
    private String mColorCode = "";

    private LiveBroadcastActivity mCtx = this;
    private ArrayList<UserModel> mUsers = new ArrayList<UserModel>();
    private PeopleAdapter mPeopleAdapter;

    private SubscribePeopleAdapter mLeaderPeopleAdapter;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};

//    private int years[] = {R.drawable.money_1,R.drawable.money_2,R.drawable.money_3,R.drawable.money_4,R.drawable.money_5,R.drawable.money_6,R.drawable.money_7,R.drawable.money_8,R.drawable.money_9,R.drawable.money_10};
//    private int snows[] = {R.drawable.snow_1,R.drawable.snow_2,R.drawable.snow_3,R.drawable.snow_4,R.drawable.snow_5,R.drawable.snow_6,R.drawable.snow_7,R.drawable.snow_8};
    private int xmass[] = {R.drawable.bear_2,R.drawable.bubble_9,R.drawable.cat_5,R.drawable.rabbit_7,R.drawable.xmas_1,R.drawable.logo_7};
//    private int xmass[] = {R.drawable.new_year_1,R.drawable.new_year_2,R.drawable.new_year_3,R.drawable.new_year_4,R.drawable.new_year_5,R.drawable.new_year_6,R.drawable.new_year_7,R.drawable.new_year_8};

    private int mColorPos = 0;

    private LayoutInflater inflater;
    private Message mMessage;
    private int TenPos = 0;

    private ArrayList<LiveComment> mLiveComments = new ArrayList<LiveComment>();
    private CommentAdapter mCommentAdapter;

    private Boolean isLiveState = false;
    private Story17Application mApplication;
//    private int lag = 0;

    private RelativeLayout mShow;
    private LinearLayout mTopLayout;
    private TextView mCount,mNow;
    private Gallery mGallery;

    //leaderboard
    private Gallery mLeaderGallery;

    private ImageView mAllview;
    private ImageView mLove;

    private LinearLayout mChangeLayout;
    private ImageView switchButton;
    private ImageView light;

    private ListView mListView;
//    private ImageView mLoadding;
//    private AnimationDrawable mAnimaition;

    private RelativeLayout mStartLayout;
    private EditText mEditCap;
    private Button mStart;
    
    //leaderboard
    LinearLayout mLiveGiftLeaderboard;

    private ImageView mClose;
    private int mStartCurrentTime = 0;

    private int mBitRateLevel = 2;

    private Socket tcpSocket;
    private int audioSequenceNumber = 0;
    private int NUM_OF_AUDIO_SAMPLES_PER_CHUNK = 20;
    private int chunkID = 0;

    private int mLiveStreamAbsTime = 0 ;
    private Boolean isEncoding = false;

    private Boolean mAutoFocusState = true;
    
    private boolean isInLeaderboard = false;

    private Dialog UserDialog;
    private int nowLoadPeople = 0;
    private Dialog FinishDialog;
    private Boolean mEndstate = false;

    private Boolean isPublish = false;
    private ImageView mCamChange;
    private int[] mCommentColors = {R.color.live_comment_color2,R.color.live_comment_color3,R.color.live_comment_color4,R.color.live_comment_color5,R.color.live_comment_color6,R.color.live_comment_color7,R.color.live_comment_color1};

    private Boolean mGetCommentState = false;
    private int mCommentTime = 0;
//    private ArrayList<LiveComment> mLastComment = new ArrayList<LiveComment>();

    //leaderboard
    private ArrayList<GiftLeaderboardModel> mGiftLeaderBoard = new ArrayList<GiftLeaderboardModel>();
    private ArrayList<GiftLeaderboardModel> mModelsLive = new ArrayList<GiftLeaderboardModel>();
    //private ArrayList<GiftLeaderboardModel> mModelsLiveAll = new ArrayList<GiftLeaderboardModel>();
    private ArrayList<LiveGiftsModel> mModels = new ArrayList<LiveGiftsModel>();
    private List<View> mLeaderBoardPage;
    private View mGiftPage1,mGiftPage2;
    private ViewPager mViewPager;
    private SmartTabLayout mPagerTab;
    private TextView mTab1, mTab2;
    //private MyAdapter mMyAdapter;
    private TextView mLiveTotalPoint;
    private Dialog Subscriberlog,SubscriberMenulog,LeaderboardLog;

    private String mUDP_IP = Constants.LIVE_STREAM_UDP_SERVER_IP;

    private int mLiveNowPeople = 0;
    private Timer mLoveTimer;
    private int lastFPS = 0;
    private int LagSix = 0;
    private int mLoveCount = 0;
    private Boolean mEnd = false;
    private ParticleSystem particle;
    private ScaleModifier mScaleModifier;
    private ImageView mShowlove;
    private FrameLayout mGiftAni;

    private Boolean mGetGiftState = false;
    private long mGiftTime = 0;
    private ArrayList<LiveGiftsModel> mLiveGifts = new ArrayList<LiveGiftsModel>();
    private Boolean mPlayGiftState = true;
    private DisplayMetrics mDisplayMetrics;

    private int mGiftToken = 0;
    private ArrayList<LiveGiftsModel> mLiveGiftsModels = new ArrayList<LiveGiftsModel>();
    private Boolean mGiftShowState = false;
    private Boolean mGiftLoad = false;

    private LinearLayout mGiftComment;
    private ImageView mGiftIconPic;
    private TextView mGiftUserName;
    private TextView mGiftGiftName;
    private int gift_text_colors[] = {R.color.live_gift_color1,R.color.live_gift_color2,R.color.live_gift_color3,R.color.live_gift_color4,R.color.live_gift_color5,R.color.live_gift_color6,R.color.live_gift_color7};
    private int gift_loves[] = {R.drawable.giftpoint_1,R.drawable.giftpoint_2,R.drawable.giftpoint_3,R.drawable.giftpoint_4,R.drawable.giftpoint_5,R.drawable.giftpoint_6,R.drawable.giftpoint_7};

    private LinearLayout mBestGiftComment;
    private ImageView mBestGiftIconPicLeft;
    private TextView mBestGiftUserName;
    private TextView mBestGiftGiftName;
    private ImageView mBestGiftIconPicRight;

    private LinearLayout mGiftCommentBest;
    private ImageView mGiftIconPicBest;
    private TextView mGiftUserNameBest;
    private TextView mGiftGiftNameBest;

    private RelativeLayout mEndLayout;
    private TextView mViewer;
    private TextView mLike;
    private TextView mTimeCount;
    private TextView mTime;
    private TextView mSharedCount;
    private TextView mGiftsCount;
    private TextView mRevenue;
    private TextView mCurrency;
    private Button mEndBtn;

    private Handler mHandler = new Handler();


    private int mUnRead = 0;
    private MediaPlayer mMp3 = new MediaPlayer();

    private Boolean ShowLove = true;

    private LeaderPagerAdapter mPagerAdapter;

    private int mGiftsSummary = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_broadcast);

        mApplication = (Story17Application) getApplication();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        user = Singleton.preferences.getString(Constants.USER_ID, "");

        // alloc native direct buffers
        encodedVideoDataBuffer = ByteBuffer.allocateDirect(640 * 360);
        yuvDataBuffer = ByteBuffer.allocateDirect(640 * 360 * 3 / 2);
        decodedVideoDataBuffer = ByteBuffer.allocateDirect(640 * 360 *3/2);
        decodedAudioDataBuffer = ByteBuffer.allocateDirect(512); // TRICKY: cannot be 480, because ffmpeg use line size 512
        audioDataBuffer = ByteBuffer.allocateDirect(3840);

        isUsingFrontCamera = true;
        
        //leaderboard
        mLiveGiftLeaderboard = (LinearLayout) findViewById(R.id.live_leaderboard_layout);

        AudioManager audioManager;
        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_NORMAL);
        audioManager.setSpeakerphoneOn(true);

        mDisplayMetrics = new DisplayMetrics();
        mCtx.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        // setup views
        switchButton = (ImageView) findViewById(R.id.switchButton);
        previewView = (SurfaceView) findViewById(R.id.previewView);

        surfaceHolder = previewView.getHolder();
        surfaceHolder.addCallback(mCtx);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        // setup video renderer
        videoRenderer = new VideoRenderer();
        videoRenderer.setContext(mCtx);

        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mShow = (RelativeLayout) findViewById(R.id.show);
        mTopLayout = (LinearLayout) findViewById(R.id.top_layout);
        mCount = (TextView) findViewById(R.id.count);
        mNow = (TextView) findViewById(R.id.now);
        mGallery = (Gallery) findViewById(R.id.people);
        mGallery.setSpacing(10);
        mGallery.setUnselectedAlpha(255);
        mGallery.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                try
                {
                    if(mUsers.size() > position)
                    {
                        showUserDialog(position, 0,mUsers.get(position),null);
                    }
                }
                catch (Exception e)
                {

                }
            }
        });

        mLeaderGallery = (Gallery) findViewById(R.id.leaderGallery);
        mLeaderGallery.setSpacing(10);
        mLeaderGallery.setUnselectedAlpha(255);

        mAllview = (ImageView) findViewById(R.id.allview);
        mLove = (ImageView) findViewById(R.id.love);
        mShowlove = (ImageView) findViewById(R.id.showlove);
        mChangeLayout = (LinearLayout) findViewById(R.id.change_layout);
        light = (ImageView) findViewById(R.id.light);
        mListView = (ListView) findViewById(R.id.list);
        mGiftAni = (FrameLayout) findViewById(R.id.gift_ani);

        light.setVisibility(View.GONE);

//        mLoadding = (ImageView) findViewById(R.id.loadding);

        mStartLayout = (RelativeLayout) findViewById(R.id.start_layout);
        mEditCap = (EditText) findViewById(R.id.edit);
        mStart = (Button) findViewById(R.id.start);

        mGiftComment = (LinearLayout) findViewById(R.id.gift_comment);
        mGiftIconPic = (ImageView) findViewById(R.id.gift_icon_pic);
        mGiftUserName = (TextView) findViewById(R.id.gift_user_name);
        mGiftGiftName = (TextView) findViewById(R.id.gift_gift_name);

        mBestGiftComment = (LinearLayout) findViewById(R.id.best_gift_comment);
        mBestGiftIconPicLeft = (ImageView) findViewById(R.id.best_gift_icon_pic_left);
        mBestGiftUserName = (TextView) findViewById(R.id.best_gift_user_name);
        mBestGiftGiftName = (TextView) findViewById(R.id.best_gift_gift_name);
        mBestGiftIconPicRight = (ImageView) findViewById(R.id.best_gift_icon_pic_right);

        mGiftCommentBest = (LinearLayout) findViewById(R.id.gift_comment_best);
        mGiftIconPicBest = (ImageView) findViewById(R.id.gift_icon_pic_best);
        mGiftUserNameBest = (TextView) findViewById(R.id.gift_user_name_best);
        mGiftGiftNameBest = (TextView) findViewById(R.id.gift_gift_name_best);


        mEndLayout = (RelativeLayout) findViewById(R.id.end_layout);
        mViewer = (TextView) findViewById(R.id.viewer_count);
        mLike = (TextView) findViewById(R.id.liker_count);
        mTimeCount = (TextView) findViewById(R.id.time_count);
        mTime = (TextView) findViewById(R.id.time);

        mSharedCount = (TextView) findViewById(R.id.end_info_shared_count);
        mGiftsCount = (TextView) findViewById(R.id.end_info_gifts_count);
        mRevenue = (TextView) findViewById(R.id.end_info_revenue);
        mCurrency = (TextView) findViewById(R.id.end_info_currency);
        mEndBtn = (Button) findViewById(R.id.end);
        mEndBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mApplication.setReLoad(true);
                hideKeyboard();
                mCtx.finish();
            }
        });



        mClose = (ImageView) findViewById(R.id.close);
        mCamChange = (ImageView) findViewById(R.id.cam_change);
        mCamChange.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isUsingFrontCamera == false)
                {
                    isUsingFrontCamera = true;
                    startCamera();
                }
                else
                {
                    isUsingFrontCamera = false;
                    startCamera();
                }
            }
        });

        // setup threads
        networkSendHandlerThread = new HandlerThread("NETWORK_SEND");
        networkSendHandlerThread.start();
        networkSendHandler = new Handler(networkSendHandlerThread.getLooper());

        networkReceiveHandlerThread = new HandlerThread("NETWORK_RECEIVE");
        networkReceiveHandlerThread.start();
        networkReceiveHandler = new Handler(networkReceiveHandlerThread.getLooper());

        videoEncodeHandlerThread = new HandlerThread("VIDEO_ENCODE");
        videoEncodeHandlerThread.start();
        videoEncodeHandler = new Handler(videoEncodeHandlerThread.getLooper());

        audioEncodeHandlerThread = new HandlerThread("AUDIO_ENCODE");
        audioEncodeHandlerThread.start();
        audioEncodeHandler = new Handler(audioEncodeHandlerThread.getLooper());

        audioRecordHandlerThread = new HandlerThread("AUDIO_RECODE");
        audioRecordHandlerThread.start();
        audioRecordHandler = new Handler(audioRecordHandlerThread.getLooper());

        videoEncodeHandler.post(new Runnable() {
            @Override
            public void run() {
                VP8Codec.instance().setup(getVideoBitRateLevel(mBitRateLevel));
            }
        });

        audioEncodeHandler.post(new Runnable() {
            @Override
            public void run() {
                OpusCodec.instance().setup(getAudioBitRateLevel(mBitRateLevel));
            }
        });


        mStartLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try
                {
                    if(camera!=null && mAutoFocusState)
                    {
                        mAutoFocusState = false;
                        camera.autoFocus(new Camera.AutoFocusCallback()
                        {
                            @Override
                            public void onAutoFocus(boolean success, Camera cam)
                            {
                                mAutoFocusState = true;
                            }
                        });
                    }
                }
                catch (Exception e)
                {

                }
            }
        });

        mStart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(isPublish) return;

                isPublish = true;
                mCamChange.setVisibility(View.GONE);

                //event tracking
                try{
                    LogEventUtil.StartLiveStreaming(mCtx,mApplication);
                }catch (Exception x)
                {

                }

                ApiManager.publishLiveStream(mCtx, user, mEditCap.getText().toString(), new ApiManager.PublishLiveStreamCallback()
                {
                    @Override
                    public void onResult(boolean success, int id, int timestamp, String colorCode, String ip)
                    {
                        hideKeyboard();

                        if(success) {
                            isLiveState = true;

                            liveStreamID = id;

                            Singleton.log("liveStreamID: "+liveStreamID);

                            mTimestamp = timestamp;
                            mColorCode = colorCode;

                            if(ip!=null && ip.length()!=0)
                            {
                                mUDP_IP = ip;
                            }

                            mCount.setText("0" + getString(R.string.live_people));
                            mNow.setText(String.format(getString(R.string.live_now), 0));

                            mStartLayout.setVisibility(View.GONE);
                            mShow.setVisibility(View.VISIBLE);

                            mStartCurrentTime = Singleton.getCurrentTimestamp();

                            openSocket();

                            switchButton.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    if (isUsingFrontCamera == false) {
                                        isUsingFrontCamera = true;
                                        startCamera();
                                    } else {
                                        isUsingFrontCamera = false;
                                        startCamera();
                                    }
                                }
                            });

                            keepAliveTimer = new Timer();
                            keepAliveTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    sendKeepAliveData();
                                }
                            }, 0, 1000L);

                            audioRecordHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    try{
                                        pcmAudioRecorder = new PCMAudioRecorder();
                                        pcmAudioRecorder.delegate = mCtx;
                                        pcmAudioRecorder.startRecording();
                                    }
                                    catch (Exception e){
                                    }
                                }
                            });
                        }
                        else
                        {
                            try {
                                Dialog Enddialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                                Enddialog.setContentView(R.layout.live_end_dailog);
                                Window window = Enddialog.getWindow();
                                window.setGravity(Gravity.BOTTOM);
                                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                Enddialog.setCancelable(false);

                                TextView mTitle = (TextView) Enddialog.findViewById(R.id.title);
                                mTitle.setText(getString(R.string.live_broadcast_error));

                                Button mEnd = (Button)Enddialog.findViewById(R.id.end);
                                mEnd.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        hideKeyboard();
                                        mCtx.finish();
                                    }
                                });
                                Enddialog.show();
                            }
                            catch (Exception e){
                                hideKeyboard();
                                mCtx.finish();
                            }
                        }
                    }
                });
            }
        });

        mShow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    if(camera!=null && mAutoFocusState)
                    {
                        mAutoFocusState = false;
                        camera.autoFocus(new Camera.AutoFocusCallback()
                        {
                            @Override
                            public void onAutoFocus(boolean success, Camera cam)
                            {
                                mAutoFocusState = true;
                            }
                        });
                    }
                }
                catch (Exception e)
                {

                }
            }
        });

        mAllview.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(!mAllState)
                {
                    mAllState = true;
                    mAllview.setImageResource(R.drawable.btn_live_fullscreen2_selector);
                    mTopLayout.setVisibility(View.GONE);
                    mListView.setVisibility(View.GONE);
                    mLove.setVisibility(View.GONE);
                    mChangeLayout.setVisibility(View.GONE);
                    mGallery.setVisibility(View.GONE);
                    mLiveGiftLeaderboard.setVisibility(View.GONE);
                }
                else
                {
                    mAllState = false;
                    mAllview.setImageResource(R.drawable.btn_live_fullscreen_selector);
                    mTopLayout.setVisibility(View.VISIBLE);
                    mListView.setVisibility(View.VISIBLE);
                    mLove.setVisibility(View.VISIBLE);
                    mChangeLayout.setVisibility(View.VISIBLE);
                    mGallery.setVisibility(View.VISIBLE);
                    mLiveGiftLeaderboard.setVisibility(View.VISIBLE);
                }
            }
        });

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        handler = new Handler(Looper.getMainLooper());

//        startCamera();
        new ChangePreviewTask().execute(null,null,null);
        
        //leaderboard
        mLiveGiftLeaderboard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(LeaderboardLog == null) {

                    mPagerAdapter = new LeaderPagerAdapter();
                    LeaderboardLog = new Dialog(mCtx, R.style.LivePlayerDialog);
                    LeaderboardLog.setContentView(R.layout.present_board_activity_live);

                    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    LayoutInflater lf = getLayoutInflater().from(mCtx);
                    mGiftPage1 = lf.inflate(R.layout.leaderboard_list_live, null);
                    mGiftPage2 = lf.inflate(R.layout.leaderboard_list_pulltoreflash, null);
                    mLeaderBoardPage = new ArrayList<View>();
                    mLeaderBoardPage.add(mGiftPage1);
                    mLeaderBoardPage.add(mGiftPage2);

                    mViewPager = (ViewPager) LeaderboardLog.findViewById(R.id.viewpager);
                    mPagerTab = (SmartTabLayout) LeaderboardLog.findViewById(R.id.viewpagertab);
                    mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        }

                        @Override
                        public void onPageSelected(int position) {
                            switch (position) {
                                case 0:
                                    mTab1.setTextColor(getResources().getColor(R.color.main_color));
                                    mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                                    break;
                                case 1:
                                    mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                                    mTab2.setTextColor(getResources().getColor(R.color.main_color));
                                    break;
                            }
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {
                        }
                    });

                    mTab1 = (TextView) LeaderboardLog.findViewById(R.id.tab1);

                    mTab1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(0);
                        }
                    });

                    mTab2 = (TextView) LeaderboardLog.findViewById(R.id.tab2);


                    mTab2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mViewPager.setCurrentItem(1);
                        }
                    });
                }

                mViewPager.setAdapter(mPagerAdapter);
                mPagerTab.setViewPager(mViewPager);
                mPagerAdapter.notifyDataSetChanged();
                LeaderboardLog.show();
            }
        });

        mClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                if(isLiveState)
                {
                    if(!mEndstate)
                    {
                        ShowFinish();
                    }
                }
                else
                {
                    hideKeyboard();
                    finish();
                }
            }
        });

        mLove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ShowLove)
                {
                    ShowLove = false;
                }
                else
                {
                    ShowLove = true;
                }
            }
        });

        PeopleOptions = new DisplayImageOptions.Builder()
        .displayer(new RoundedBitmapDisplayer(90))
        .showImageOnLoading(R.drawable.placehold_c)
        .showImageForEmptyUri(R.drawable.placehold_c)
        .showImageOnFail(R.drawable.placehold_c)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

//        showKeyboard();
    }

    private int getLoveFPS(int people)
    {
        if(people < 50)
        {
            return (int)(Math.random()* 50 + 300);
        }
        else if(people < 150)
        {
            return (int)(Math.random()* 50 + 200);
        }
        else if(people < 400)
        {
            return (int)(Math.random()* 50 + 150);
        }
        else if(people < 600)
        {
            return (int)(Math.random()* 25 + 100);
        }
        else if(people < 800)
        {
            return (int)(Math.random()* 25 + 80);
        }
        else if(people < 1000)
        {
            return (int)(Math.random()* 20 + 60);
        }
        else
        {
            return (int)(Math.random()* 5 + 40);
        }
    }

    private void showLove()
    {
        if(mLiveNowPeople > 10)
        {
            if(!mAllState && ShowLove)
            {
                if(LagSix < 7 && !mEnd)
                {
                    int miss = (int)(Math.random()* 100);
                    if(miss > 10)
                    {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {

                                if(particle!=null) particle = null;
                                if(mScaleModifier!=null) mScaleModifier = null;

                                try {
                                    if(((int)(Math.random()*10)) < 2)
                                    {
                                        mScaleModifier = new ScaleModifier((float)0.8 + (float)(Math.random() * 0.2), (float)1.0 + (float)(Math.random() * 0.2), 0, 1500);
                                        particle = new ParticleSystem(mCtx, 1,xmass[(int) (Math.random() * xmass.length)], 3000);
                                        particle.setSpeedByComponentsRange(-0.02f, 0.02f, -0.2f, -0.1f);
                                        particle.setAcceleration(0.000003f, 90);
                                        particle.setInitialRotationRange(-20, 20);
                                        particle.setRotationSpeed(-20 + ((int) (Math.random() * 40)));
                                        particle.setFadeOut(2000);
                                        particle.addModifier(mScaleModifier);
                                        particle.oneShot(mShowlove, 10);
                                    }
                                    else
                                    {
                                        mScaleModifier = new ScaleModifier((float)0.8 + (float)(Math.random() * 0.2), (float)1.0 + (float)(Math.random() * 0.2), 0, 1500);
                                        particle = new ParticleSystem(mCtx, 1,loves[(int) (Math.random() * loves.length)], 3000);
                                        particle.setSpeedByComponentsRange(-0.02f, 0.02f, -0.2f, -0.1f);
                                        particle.setAcceleration(0.000003f, 90);
                                        particle.setInitialRotationRange(-20, 20);
                                        particle.setRotationSpeed(-20 + ((int) (Math.random() * 40)));
                                        particle.setFadeOut(2000);
                                        particle.addModifier(mScaleModifier);
                                        particle.oneShot(mShowlove, 10);
                                    }
                                }
                                catch (OutOfMemoryError o) {
                                }
                                catch (Exception e){
                                }

                                mLoveCount++;
                            }
                        });
                    }
                }
            }
        }
    }

    public float convertDpToPixel(float dp)
    {
        Resources resources = mCtx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    boolean socketIsOpenning = false;

    private void openSocket()
    {
        if(isSocketEstablished()) {
            return;
        }

        if(socketIsOpenning) {
            return;
        }

        socketIsOpenning = true;

        networkSendHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                if(isSocketEstablished()) {
                    return;
                }

                try {
                    if (tcpSocket != null) {
                        tcpSocket.close();
                    }

                    tcpSocket = null;

                    tcpSocket = new Socket();
                    tcpSocket.connect(new InetSocketAddress(Constants.LIVE_STREAM_TCP_SERVER_IP, Constants.LIVE_STREAM_TCP_SERVER_PORT));
                    tcpSocket.setTcpNoDelay(true);
                    tcpSocket.setKeepAlive(true);
                    Singleton.log("TCP SOCKET ESTABLISHED!");
                } catch (IOException e) {
                    Singleton.log("OPEN TCP SOCKET ERROR: "+e.toString());
                }

                try
                {
                    if(datagramSocket!=null) {
                        datagramSocket.close();
                    }

                    datagramSocket = null;

                    datagramSocket = new DatagramSocket(0);
//                    datagramSocket.connect(new InetSocketAddress(Constants.LIVE_STREAM_UDP_SERVER_IP, Constants.LIVE_STREAM_UDP_SERVER_PORT));
                    Singleton.log("UDP SOCKET ESTABLISHED!");
                } catch (Exception e) {
                    Singleton.log("UDP SOCKET ESTABLISH FAILED!");
                }

                socketIsOpenning = false;

                if (datagramSocket != null) {
                    sendKeepAliveData();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            startReceivingData();
                        }
                    });
                }
            }
        });
    }

    private void closeSocket()
    {
        networkSendHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    if (tcpSocket != null) {
                        tcpSocket.close();
                    }

                    tcpSocket = null;
                } catch (IOException e) {

                }

                try
                {
                    if(datagramSocket!=null) {
                        datagramSocket.close();
                    }

                    datagramSocket = null;
                } catch (Exception e) {

                }
            }
        });
    }

    private boolean isSocketEstablished () {
        return tcpSocket!=null && tcpSocket.isConnected() && datagramSocket!=null && datagramSocket.isBound();
    }

    public int getVideoBitRateLevel(int level)
    {
        if(Connectivity.isConnectedFast(this)) {
            return 1024000;
        } else {
            return 256000;
        }

//        int bit = 768000;
//        switch (level)
//        {
//            case 1: bit =  1536000;
//                break;
//            case 2: bit =  768000;
//                break;
//            case 3: bit =  512000;
//                break;
//            case 4: bit =  128000;
//                break;
//        }
//
//        return bit;
    }

    public int getAudioBitRateLevel(int level)
    {
        if(Connectivity.isConnectedFast(this)) {
            return 48000;
        } else {
            return 16000;
        }

//        int bit = 64000;
//        switch (level)
//        {
//            case 1: bit = 96000;
//                break;
//            case 2: bit = 64000;
//                break;
//            case 3: bit = 48000;
//                break;
//            case 4: bit = 16000;
//                break;
//        }
//
//        return bit;
    }

    private class SubscribePeopleAdapter extends BaseAdapter
    {

        private ArrayList<SubscriberUserModel> mSubscriber = new ArrayList<SubscriberUserModel>();

        public SubscribePeopleAdapter(ArrayList<SubscriberUserModel> model)
        {
            mSubscriber.addAll(model);
        }
        @Override
        public int getCount()
        {
            return mSubscriber.size();
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup)
        {
            SubscribeViewHolder holder = new SubscribeViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.subscriber_live_people, null);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                convertView.setTag(holder);
            }
            else holder = (SubscribeViewHolder) convertView.getTag();

            try
            {

                if( i<mSubscriber.size() )
                {
                    if(mSubscriber.get(i).getUserInfo().getPicture().length()!=0) {
                        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSubscriber.get(i).getUserInfo().getPicture()), holder.pic, PeopleOptions);
                        holder.name.setText(mSubscriber.get(i).getUserInfo().getName());
                    }
//                    else holder.pic.setImageResource(R.drawable.placehold_c);
                }
//                else holder.pic.setImageResource(R.drawable.placehold_c);
            }
            catch(Exception e)
            {
                holder.pic.setImageResource(R.drawable.placehold_c);
            }

            return convertView;
        }
    }

    private class PeopleAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mUsers.size();
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.live_people, null);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            try
            {
                if(mUsers.size() > i)
                {
                    if(mUsers.get(i).getPicture().length()!=0) ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUsers.get(i).getPicture()), holder.pic, PeopleOptions);
                    else holder.pic.setImageResource(R.drawable.placehold_c);
                }
                else holder.pic.setImageResource(R.drawable.placehold_c);
            }
            catch(Exception e)
            {
                holder.pic.setImageResource(R.drawable.placehold_c);
            }

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView pic;
    }

    //subscriber
    private class SubscribeViewHolder
    {
        ImageView pic;
        TextView name;
    }


    @Override
    public void onResume() {
        super.onResume();

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    @Override
    public void onPause() {
        super.onPause();

        if(pcmAudioRecorder!=null) pcmAudioRecorder.stopRecording();

        //stopCamera();
        try{
            if (camera != null) {
                stopPreview();
                camera.release();
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        if(isLiveState)
        {
            if(!mEndstate)
            {
                endLive();
            }
        }
        else
        {
            hideKeyboard();
            finish();
        }
//        finish();

        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    private void ShowFinish()
    {
        if(FinishDialog==null)
        {
            FinishDialog = new Dialog(mCtx, R.style.LivePlayerDialog);
            FinishDialog.setContentView(R.layout.live_end_sure_dailog);
            Window window = FinishDialog.getWindow();
            window.setGravity(Gravity.BOTTOM);
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            FinishDialog.setCancelable(true);


            Button mYes = (Button)FinishDialog.findViewById(R.id.yes);
            mYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEndstate) return;

                    try {
                        final ByteBuffer EndByteBuffer = ByteBuffer.allocate(5);
                        EndByteBuffer.order(ByteOrder.LITTLE_ENDIAN);

                        EndByteBuffer.put(PACKET_TYPE_STREAM_END);
                        EndByteBuffer.putInt(liveStreamID);

                        if (networkSendHandler != null) {
                            networkSendHandler.post(new Runnable() {
                                public void run() {
                                    try {
                                        datagramSocket.send(new DatagramPacket(EndByteBuffer.array(), EndByteBuffer.position(), new InetSocketAddress(mUDP_IP, Constants.LIVE_STREAM_UDP_SERVER_PORT)));
                                    } catch (Exception e) {

                                    }
                                }
                            });
                        }
                    } catch (Exception e) {

                    }
                    mEnd = true;
                    if (mLoveTimer != null) {
                        mLoveTimer.cancel();
                        mLoveTimer.purge();
                        mLoveTimer = null;
                    }

                    final int duration = Singleton.getCurrentTimestamp() - mStartCurrentTime;

                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                ApiManager.endLiveStream(mCtx, user, liveStreamID, duration, new ApiManager.EndLiveStreamCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        mEndstate = true;

                                        ApiManager.getLiveStreamInfo(mCtx, user, liveStreamID, new ApiManager.GetLiveStreamInfoCallback() {
                                            @Override
                                            public void onResult(final boolean success, final LiveModel mLiveModel) {
                                                mHandler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        refreshEndInfo(success, mLiveModel);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();

                    FinishDialog.dismiss();
                }
            });
            Button mNo = (Button)FinishDialog.findViewById(R.id.no);
            mNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FinishDialog.dismiss();
                }
            });
            FinishDialog.show();
        }
        else FinishDialog.show();
    }

    private void refreshEndInfo(boolean success, LiveModel mLiveModel){

        if (success && mLiveModel != null){
            mClose.setVisibility(View.GONE);

            mSharedCount.setText(String.valueOf(mLiveModel.getShareLocation()));

            int summary = mGiftsSummary + (mLiveGiftsModels != null ? mLiveGiftsModels.size() : 0);
            mGiftsCount.setText(String.valueOf(summary));

            String currencyStr = Singleton.getCurrencyType();
            mCurrency.setText(currencyStr);

            DecimalFormat df = new DecimalFormat("##.####");
            float total = mLiveModel.getRevenue() * Singleton.getCurrencyRate();
            mRevenue.setText(df.format(total));

            mEndLayout.setVisibility(View.VISIBLE);
            mViewer.setText(String.valueOf(mLiveModel.getViewerCount()));
            mLike.setText(String.valueOf(mLoveCount));

            SimpleDateFormat mDateFormatTo = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            mTimeCount.setText(mDateFormatTo.format(new Date((mLiveModel.getTotalViewTime() * 1000L) - TimeZone.getDefault().getRawOffset())));
            mTime.setText(mDateFormatTo.format(new Date((mLiveModel.getDuration() * 1000L) - TimeZone.getDefault().getRawOffset())));
        }else{
            mApplication.setReLoad(true);
            hideKeyboard();
            mCtx.finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if(isLiveState)
            {
                if(!mEndstate)
                {
                    ShowFinish();
                }
            }
            else
            {
                hideKeyboard();
                finish();
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void showUserDialog(final int pos,int type, UserModel userModel, LiveComment liveComment)
    {
        if(type==0)
        {
            if(Singleton.preferences.getString(Constants.USER_ID, "").compareTo(userModel.getUserID())==0) return ;
        }
        else
        {
            if(Singleton.preferences.getString(Constants.USER_ID, "").compareTo(liveComment.getUserInfo().getUserID())==0) return ;
        }

        if(UserDialog!=null) UserDialog = null;

        UserDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        UserDialog.setContentView(R.layout.live_player);
        Window window = UserDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        UserDialog.setCancelable(true);

        ImageView mPic = (ImageView)UserDialog.findViewById(R.id.pic);
        Button mReport = (Button)UserDialog.findViewById(R.id.report);
        TextView mName = (TextView)UserDialog.findViewById(R.id.name);
        TextView mCaption = (TextView)UserDialog.findViewById(R.id.caption);
        LinearLayout mUserLay = (LinearLayout) UserDialog.findViewById(R.id.user_lay);
        mUserLay.setVisibility(View.VISIBLE);
        Button mUserCom = (Button)UserDialog.findViewById(R.id.user_com);
        final Button mUserFoll = (Button)UserDialog.findViewById(R.id.user_foll);
        ImageView mClose = (ImageView)UserDialog.findViewById(R.id.close);

        if(type==0)
        {
            final UserModel mUser = userModel;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mUser.getPicture()), mPic, PeopleOptions);
            mName.setText(mUser.getOpenID());
            mCaption.setText(mUser.getBio());

            mClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserDialog.dismiss();
                }
            });

            mReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        LogEventUtil.ReportUser(mCtx, mApplication);
                    }
                    catch (Exception X)
                    {

                    }

                    ApiManager.reportUserAction(mCtx, mUser.getUserID(), "Livestream User Report", "", new ApiManager.ReportUserActionCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            UserDialog.dismiss();
                            if (success) {
                                try {
//                          showToast(getString(R.string.complete));
                                    Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            } else {
                                try {
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        }
                    });
                }
            });
            mUserCom.setText(getString(R.string.live_block_user));

            mUserCom.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //event tracking
                    try {
                        LogEventUtil.BlockUser(mCtx, mApplication, mUser.getUserID());
                    }
                    catch (Exception x)
                    {

                    }

                    ApiManager.blockUserAction(mCtx, mUser.getUserID(), new ApiManager.BlockUserActionCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {

                            UserDialog.dismiss();
                            if (success) {
                                try {
//                          showToast(getString(R.string.complete));
                                    Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                            else {
                                try {
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        }
                    });
                }
            });

            if(mUser.getIsFollowing()==1)
            {
                mUserFoll.setText(getString(R.string.user_profile_following));
                mUserFoll.setBackgroundResource(R.drawable.btn_green_selector);
                mUserFoll.setTextColor(getResources().getColor(R.color.white));
            }
            else
            {
                if(mUser.getFollowRequestTime()!=0)
                {
                    mUserFoll.setText(getString(R.string.private_mode_request_send));
                    mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                    mUserFoll.setTextColor(getResources().getColor(R.color.black));
                }
                else
                {
                    mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                    mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                    mUserFoll.setTextColor(getResources().getColor(R.color.black));
                }
            }

            mUserFoll.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mUser.getIsFollowing()==1)
                    {
                        mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                        mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                        mUserFoll.setTextColor(getResources().getColor(R.color.black));
                        mUser.setIsFollowing(0);
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mUser.getUserID());
                        }
                        catch (Exception x){

                        }
                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mUser.getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message) {

                                if (success) {
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mUser.getFollowRequestTime()!=0)
                        {
                            mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                            mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                            mUserFoll.setTextColor(getResources().getColor(R.color.black));
                            mUser.setIsFollowing(0);
                            mUser.setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mUser.getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try {
//                          showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                                return ;
                            }

                            if(mUser.getPrivacyMode().compareTo("private")==0)
                            {
                                mUserFoll.setText(getString(R.string.private_mode_request_send));
                                mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                                mUserFoll.setTextColor(getResources().getColor(R.color.black));
                                mUser.setIsFollowing(0);
                                mUser.setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mUser.getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                                            mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            mUserFoll.setTextColor(getResources().getColor(R.color.black));
                                            mUser.setIsFollowing(0);
                                            mUser.setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                mUserFoll.setText(getString(R.string.user_profile_following));
                                mUserFoll.setBackgroundResource(R.drawable.btn_green_selector);
                                mUserFoll.setTextColor(getResources().getColor(R.color.white));
                                mUser.setIsFollowing(1);

                                try{
                                LogEventUtil.FollowUser(mCtx,mApplication,mUser.getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mUser.getUserID(), new ApiManager.FollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
        else
        {
            final LiveComment mComment = liveComment;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mComment.getUserInfo().getPicture()), mPic, PeopleOptions);
            mName.setText(mComment.getUserInfo().getOpenID());
            mCaption.setText(mComment.getUserInfo().getBio());

            mClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserDialog.dismiss();
                }
            });

            mReport.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    try {
                        LogEventUtil.ReportUser(mCtx, mApplication);
                    }
                    catch (Exception X)
                    {

                    }

                    ApiManager.reportUserAction(mCtx, mComment.getUserInfo().getUserID(), "Livestream Comment Report", mComment.getMessage(),new ApiManager.ReportUserActionCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message)
                        {
                            UserDialog.dismiss();
                            if (success) {
                                try {
//                          showToast(getString(R.string.complete));
                                    Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                            else {
                                try {
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        }
                    });
                }
            });

            mUserCom.setText(getString(R.string.live_block_user));
            mUserCom.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //event tracking
                    try {
                        LogEventUtil.BlockUser(mCtx, mApplication, mComment.getUserInfo().getUserID());
                    }
                    catch (Exception x)
                    {

                    }

                    ApiManager.blockUserAction(mCtx, mComment.getUserInfo().getUserID(), new ApiManager.BlockUserActionCallback() {
                        @Override
                        public void onResult(boolean success, String message) {
                            UserDialog.dismiss();

                            //Umeng monitor
                            String Umeng_id = "BlockUser";
                            HashMap<String, String> mHashMap = new HashMap<String, String>();
                            mHashMap.put(Umeng_id, Umeng_id);
                            MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                            if (success) {
                                try {
//                          showToast(getString(R.string.complete));
                                    Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            } else {
                                try {
//                          showToast(getString(R.string.error_failed));
                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        }
                    });
                }
            });

            if(mComment.getUserInfo().getIsFollowing()==1)
            {
                mUserFoll.setText(getString(R.string.user_profile_following));
                mUserFoll.setBackgroundResource(R.drawable.btn_green_selector);
                mUserFoll.setTextColor(getResources().getColor(R.color.white));
            }
            else
            {
                if(mComment.getUserInfo().getFollowRequestTime()!=0)
                {
                    mUserFoll.setText(getString(R.string.private_mode_request_send));
                    mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                    mUserFoll.setTextColor(getResources().getColor(R.color.black));
                }
                else
                {
                    mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                    mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                    mUserFoll.setTextColor(getResources().getColor(R.color.black));
                }
            }

            mUserFoll.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mComment.getUserInfo().getIsFollowing()==1)
                    {
                        mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                        mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                        mUserFoll.setTextColor(getResources().getColor(R.color.black));
                        mComment.getUserInfo().setIsFollowing(0);
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, mComment.getUserInfo().getUserID());
                        }
                        catch (Exception x){

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mComment.getUserInfo().getUserID(), new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {
                                }
                            }
                        });
                    }
                    else
                    {
                        if(mComment.getUserInfo().getFollowRequestTime()!=0)
                        {
                            mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                            mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                            mUserFoll.setTextColor(getResources().getColor(R.color.black));
                            mComment.getUserInfo().setIsFollowing(0);
                            mComment.getUserInfo().setFollowRequestTime(0);
                            ApiManager.cancelFollowRequests(mCtx, mComment.getUserInfo().getUserID(), new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try {
//                          showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                                return ;
                            }

                            if(mComment.getUserInfo().getPrivacyMode().compareTo("private")==0)
                            {
                                mUserFoll.setText(getString(R.string.private_mode_request_send));
                                mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                                mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                                mComment.getUserInfo().setIsFollowing(0);
                                mComment.getUserInfo().setFollowRequestTime(Singleton.getCurrentTimestamp());
                                ApiManager.sendFollowRequest(mCtx, mComment.getUserInfo().getUserID(), new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            mUserFoll.setText("+ " + getString(R.string.user_profile_follow));
                                            mUserFoll.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            mUserFoll.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mComment.getUserInfo().setIsFollowing(0);
                                            mComment.getUserInfo().setFollowRequestTime(0);
                                        }
                                    }
                                });
                            }
                            else
                            {
                                mUserFoll.setText(getString(R.string.user_profile_following));
                                mUserFoll.setBackgroundResource(R.drawable.btn_green_selector);
                                mUserFoll.setTextColor(getResources().getColor(R.color.white));
                                mComment.getUserInfo().setIsFollowing(1);

                                try{
                                LogEventUtil.FollowUser(mCtx,mApplication,mComment.getUserInfo().getUserID());
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mComment.getUserInfo().getUserID(), new ApiManager.FollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }

        UserDialog.show();
    }

    @Override
    protected void onDestroy() {
        try
        {
            if(keepAliveTimer!=null)
            {
                keepAliveTimer.cancel();
                keepAliveTimer.purge();
                keepAliveTimer = null;
            }

            closeSocket();

            videoEncodeHandler.post(new Runnable() {
                @Override
                public void run() {
                    VP8Codec.instance().release();
                }
            });

            audioEncodeHandler.post(new Runnable() {
                @Override
                public void run() {
                    OpusCodec.instance().release();
                }
            });

            audioRecordHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (pcmAudioRecorder != null) {
                        pcmAudioRecorder.stopRecording();
                    }
                }
            });

            // TRICKY => this ensures that audio player is properly released!
            networkSendHandler.post(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(networkSendHandlerThread!=null) networkSendHandlerThread.getLooper().quit();
                        }
                    });
                }
            });

            networkReceiveHandler.post(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (networkReceiveHandlerThread != null)
                                networkReceiveHandlerThread.getLooper().quit();
                        }
                    });
                }
            });

            videoEncodeHandler.post(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (videoEncodeHandlerThread != null)
                                videoEncodeHandlerThread.getLooper().quit();
                        }
                    });
                }
            });

            audioEncodeHandler.post(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (audioEncodeHandlerThread != null)
                                audioEncodeHandlerThread.getLooper().quit();
                        }
                    });
                }
            });

            audioRecordHandler.post(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (audioRecordHandlerThread != null)
                                audioRecordHandlerThread.getLooper().quit();
                        }
                    });
                }
            });
        } catch (Exception e) {

        }

        try{
            if(mMp3!=null) mMp3.release();
        }
        catch (Exception e){
        }


        super.onDestroy();
    }

    private void startReceivingData() {
        new Thread(new Runnable() {
            public void run() {
                while(isSocketEstablished()) {
                    try {
                        // start receiving response
                        final ByteBuffer receivedPacketBuffer = ByteBuffer.allocate(512);
                        receivedPacketBuffer.order(ByteOrder.LITTLE_ENDIAN);

                        final DatagramPacket responsePacket = new DatagramPacket(receivedPacketBuffer.array(), 512);
                        datagramSocket.receive(responsePacket);

                        networkReceiveHandler.post(new Runnable() {
                            @Override
                            public void run() {
//                                final int receivedDataLength = responsePacket.getLength();

                                final long currentTime = SystemClock.uptimeMillis();
                                final byte packetType = receivedPacketBuffer.get();
                                final int packetLiveStreamID = receivedPacketBuffer.getInt();

                                if(packetLiveStreamID!=liveStreamID) {
                                    return;
                                }

                                if (packetType == PACKET_TYPE_LIKE)
                                {
                                    if(mLiveNowPeople < 11)
                                    {
                                        String hex = String.format("#%02x%02x%02x", receivedPacketBuffer.getInt(5), receivedPacketBuffer.getInt(9), receivedPacketBuffer.getInt(13));
                                        try
                                        {
                                            mColorPos = (Integer.valueOf(hex.substring(1,hex.length()), 16).intValue()%28);
                                        }
                                        catch (Exception e)
                                        {
                                            mColorPos = (int) (Math.random() * loves.length);
                                        }

                                        if(mMessage!=null) mMessage = null;
                                        mMessage = new Message();
                                        mMessage.what = 1;
                                        VideoHandler.sendMessage(mMessage);
                                    }
                                }
                                else if (packetType == PACKET_TYPE_STREAM_INFO_CHANGE)
                                {
                                    if(mMessage!=null) mMessage = null;
                                    mMessage = new Message();
                                    mMessage.what = 2;
                                    VideoHandler.sendMessage(mMessage);
                                }
                                else if(packetType == PACKET_TYPE_COMMENT)
                                {
                                    if(mMessage!=null) mMessage = null;
                                    mMessage = new Message();
                                    mMessage.what = 4;
                                    VideoHandler.sendMessage(mMessage);
                                }
                                else if(packetType == PACKET_TYPE_GIFT)
                                {
                                    mGiftToken = receivedPacketBuffer.getInt();

                                    if(mMessage!=null) mMessage = null;
                                    mMessage = new Message();
                                    mMessage.what = 5;
                                    VideoHandler.sendMessage(mMessage);
                                }
                            }
                        });
                    } catch (Exception e) {

                    }
                }
            }
        }).start();
    }

    private Handler VideoHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 1:
                    if(!mAllState)
                    {
                        if(particle!=null) particle = null;
                        if(mScaleModifier!=null) mScaleModifier = null;

                        mScaleModifier = new ScaleModifier((float)0.8 + (float)(Math.random() * 0.2), (float)1.0 + (float)(Math.random() * 0.2), 0, 1500);
                        particle = new ParticleSystem(mCtx, 1,loves[(int) (Math.random() * loves.length)], 3000);//snows[mColorPos]
                        particle.setSpeedByComponentsRange(-0.02f, 0.02f, -0.2f, -0.1f);
                        particle.setAcceleration(0.000003f, 90);
                        particle.setInitialRotationRange(-20, 20);
                        particle.setRotationSpeed(-20 + ((int) (Math.random() * 40)));
                        particle.setFadeOut(2000);
                        particle.addModifier(mScaleModifier);
                        particle.oneShot(mShowlove, 10);
                        mLoveCount++;
                    }

                    break;
                case 2 :
                    if(!mAllState)
                    {
                        ApiManager.getLiveStreamInfo(mCtx, user, liveStreamID, new ApiManager.GetLiveStreamInfoCallback()
                        {
                            @Override
                            public void onResult(boolean success, LiveModel mLiveModel)
                            {
                                if (success)
                                {
                                    mCount.setText(mLiveModel.getViewerCount() + getString(R.string.live_people));
                                    mNow.setText(String.format(getString(R.string.live_now), mLiveModel.getLiveViewerCount()));

                                    mLiveNowPeople = mLiveModel.getLiveViewerCount();

                                    int fps = getLoveFPS(mLiveNowPeople);
                                    if(mLoveTimer!=null)
                                    {
                                        mLoveTimer.cancel();
                                        mLoveTimer.purge();
                                        mLoveTimer = null;
                                    }
                                    mLoveTimer = new Timer();
                                    mLoveTimer.schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            showLove();
                                        }
                                    }, 0, fps);

                                    lastFPS = fps;
                                }
                            }
                        });

                        if(nowLoadPeople != TenPos)
                        {
                            nowLoadPeople = TenPos;

                            ApiManager.getLiveStreamViewers(mCtx, user, liveStreamID, 0, 100, new ApiManager.GetLiveStreamViewersCallback()
                            {
                                @Override
                                public void onResult(boolean success, String message,ArrayList<UserModel> model)
                                {
                                    if (success && model!=null)
                                    {
                                        if(model.size()!=0)
                                        {
                                            mUsers.clear();
                                            mUsers.addAll(model);

                                            try
                                            {
                                                if(mPeopleAdapter==null)
                                                {
                                                    mPeopleAdapter = new PeopleAdapter();
                                                    mGallery.setAdapter(mPeopleAdapter);
                                                }
                                                else mPeopleAdapter.notifyDataSetChanged();
                                                mGallery.setSelection(mUsers.size()/2);
                                            }
                                            catch (Exception e)
                                            {

                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                    break;
                case 3 :
                    break;
                case 4 :
                    if(mGetCommentState) return;

                    int c = mTimestamp;
                    if(mLiveComments.size()!=0) c = mCommentTime - 1;

                    mGetCommentState = true;
                    ApiManager.getLiveStreamComments(mCtx, user, liveStreamID, c, 100, new ApiManager.GetLiveStreamCommentsCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<LiveComment> liveComments)
                        {
                            if(success && liveComments!=null)
                            {
                                if(liveComments.size()!=0)
                                {
                                    mCommentTime = liveComments.get(0).getTimestamp();

                                    if(mLiveComments.size()==0)
                                    {
                                        mLiveComments.addAll(liveComments);
//                                        mLastComment.clear();
//                                        mLastComment.addAll(liveComments);
                                    }
                                    else
                                    {
                                        for(int p = (liveComments.size()-1) ; p > -1 ; p--)
                                        {
                                            for(int k = 0 ; k < mLiveComments.size() ; k++)
                                            {
                                                if(liveComments.get(p).getMessage().compareTo(mLiveComments.get(k).getMessage())==0 && liveComments.get(p).getUserInfo().getOpenID().compareTo(mLiveComments.get(k).getUserInfo().getOpenID())==0 && liveComments.get(p).getLiveStreamChatID().compareTo(mLiveComments.get(k).getLiveStreamChatID())==0)
                                                {
                                                    liveComments.remove(p);
                                                    break;
                                                }
                                            }
                                        }
                                        Collections.reverse(liveComments);
//                                        mLastComment.clear();
//                                        mLastComment.addAll(liveComments);
                                        mLiveComments.addAll(liveComments);
                                    }

                                    if(mLiveComments.size() > 100)
                                    {
                                        int remove = mLiveComments.size() - 100;
                                        for(int k = 0 ; k < remove ; k++)
                                        {
                                            mLiveComments.remove(0);
                                        }
                                    }

                                    if(mCommentAdapter==null)
                                    {
                                        mCommentAdapter = new CommentAdapter();
                                        mListView.setAdapter(mCommentAdapter);
                                        mCommentAdapter.notifyDataSetChanged();
                                        mListView.smoothScrollToPosition(mLiveComments.size());
                                    }
                                    else
                                    {
                                        mCommentAdapter.notifyDataSetChanged();
                                        mListView.smoothScrollToPosition(mLiveComments.size());
                                    }

                                    mGetCommentState = false;
                                }
                                else mGetCommentState = false;
                            }
                            else mGetCommentState = false;
                        }
                    });

                    break;

                case 5 :

                    if(mGiftLoad) return;

                    mGiftLoad = true;
                    ApiManager.getLiveStreamGiftInfo(mCtx, liveStreamID, mGiftToken, new ApiManager.GetLiveStreamGiftInfoCallback() {
                        @Override
                        public void onResult(boolean success, LiveGiftsModel liveGiftsModel)
                        {
                            mGiftLoad = false;

                            if(success && liveGiftsModel!=null)
                            {
                                ApiManager.readGift(mCtx,mGiftToken);

                                if(mLiveGiftsModels.size()==0 || mLiveGiftsModels.size()==1) mLiveGiftsModels.add(liveGiftsModel);
                                else
                                {
                                    int pos = mLiveGiftsModels.size()-1;
                                    for(int i = 1 ; i < mLiveGiftsModels.size() ; i++)
                                    {
                                        if(liveGiftsModel.getGiftInfo().getPoint() > mLiveGiftsModels.get(i).getGiftInfo().getPoint())
                                        {
                                            pos = i;
                                            break;
                                        }
                                    }
                                    mLiveGiftsModels.add(pos,liveGiftsModel);
                                }

                                if(!mGiftShowState)
                                {
                                    mGiftShowState = true;

                                    File file = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
                                    if (file.exists())
                                    {
                                        mGiftAni.removeAllViews();
                                        GiftView mGiftView = new GiftView(mCtx, mLiveGiftsModels.get(0).getGiftInfo().getFrameDuration(), mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels, mLiveGiftsModels.get(0).getGiftInfo().getGiftID(), mLiveGiftsModels.get(0).getGiftInfo().getNumOfImages(),mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
                                        mGiftView.mGiftEndListener = mCtx;
                                        mGiftAni.addView(mGiftView);
                                        showGiftComment(mLiveGiftsModels.get(0).getGiftInfo(),mLiveGiftsModels.get(0).getUserInfo().getOpenID(),mLiveGiftsModels.get(0).getUserInfo().getPicture());

                                        try {
                                            if(mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack().length()!=0) {
                                                File sound = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                                if(sound.exists()){
                                                    if(mMp3.isPlaying()){
                                                        mMp3.stop();
                                                    }

                                                    mMp3 = null;
                                                    mMp3 = new MediaPlayer();
                                                    mMp3.setDataSource(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getSoundTrack());
                                                    mMp3.prepare();
                                                    mMp3.start();
                                                }
                                            }
                                        }
                                        catch (IOException i) {
                                        }
                                        catch (Exception e){
                                        }
                                    }
                                }
                            }
                        }
                    });

                    break;
            }
            super.handleMessage(msg);
        }
    };

    GiftView mGiftView;
    private void play_gift_animation()
    {
        mGiftsSummary++;

        mPlayGiftState = false;

        File file = new File(Singleton.getExternalMediaFolderPath() + mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
        if (file.exists())
        {
            mGiftAni.removeAllViews();
            if(mGiftView!=null) mGiftView = null;
            mGiftView = new GiftView(mCtx, mLiveGifts.get(0).getGiftInfo().getFrameDuration(), mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels, mLiveGifts.get(0).getGiftInfo().getGiftID(), mLiveGifts.get(0).getGiftInfo().getNumOfImages(),mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().substring(0,mLiveGiftsModels.get(0).getGiftInfo().getArchiveFileName().length()-4));
            mGiftView.mGiftEndListener = mCtx;
            mGiftAni.addView(mGiftView);
        }
        else
        {
            mPlayGiftState = true;
            try {
//                          showToast(getString(R.string.error_failed));
                Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
            } catch (Exception x) {
            }

            mLiveGifts.remove(0);
            if(mLiveGifts.size()!=0)
            {
                play_gift_animation();
            }
        }
    }

    public class CommentAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mLiveComments.size();
        }

        @Override
        public Object getItem(int i)
        {
            return null;
        }

        @Override
        public long getItemId(int i)
        {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup)
        {
            ViewHolderComment holder = new ViewHolderComment();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.live_comment_row, null);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.message = (TextView) convertView.findViewById(R.id.message);
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderComment) convertView.getTag();

            if(mLiveComments.get(i).getMessage().contains("* 分享了這則直播! *") || mLiveComments.get(i).getMessage().contains("* 分享了这则直播! *") || mLiveComments.get(i).getMessage().contains("* Shared This Livestream *") || mLiveComments.get(i).getMessage().contains("* Share This Livestream *"))
            {
                holder.layout.setBackgroundResource(R.drawable.btn_black_c2);
                holder.name.setText("  " + mLiveComments.get(i).getUserInfo().getOpenID() + " ");
                holder.message.setText(getString(R.string.feed_live_restream) + " ");
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getMessage().length() > 4)
            {
                if(mLiveComments.get(i).getMessage().substring(0,2).compareTo("* ")==0 && mLiveComments.get(i).getMessage().substring(mLiveComments.get(i).getMessage().length()-2,mLiveComments.get(i).getMessage().length()).compareTo(" *")==0)
                {
                    holder.layout.setBackgroundResource(R.drawable.btn_black_c2);
                    holder.name.setText("  " + mLiveComments.get(i).getUserInfo().getOpenID() + " ");
                    holder.message.setText(mLiveComments.get(i).getMessage().substring(2,mLiveComments.get(i).getMessage().length()-2) + " ");
                    holder.name.setTextColor(getResources().getColor(R.color.white));
                    holder.message.setTextColor(getResources().getColor(R.color.white));
                }
                else
                {
                    holder.layout.setBackgroundResource(R.drawable.btn_white_c2);
                    holder.name.setText("@" + mLiveComments.get(i).getUserInfo().getOpenID() + " ");
                    holder.message.setText(mLiveComments.get(i).getMessage());
                    holder.message.setTextColor(getResources().getColor(R.color.friend_txt_color));

                    try
                    {
                        if(mLiveComments.size() > i)
                        {
                            holder.name.setTextColor(getResources().getColor(mCommentColors[(Integer.valueOf(mLiveComments.get(i).getColorCode().substring(1, mLiveComments.get(i).getColorCode().length()), 16).intValue()%7)]));
                        }
                        else holder.name.setTextColor(getResources().getColor(mCommentColors[0]));
                    }
                    catch (Exception e)
                    {
                        holder.name.setTextColor(getResources().getColor(mCommentColors[0]));
                    }
                }
            }
            else
            {
                holder.layout.setBackgroundResource(R.drawable.btn_white_c2);
                holder.name.setText("@" + mLiveComments.get(i).getUserInfo().getOpenID() + " ");
                holder.message.setText(mLiveComments.get(i).getMessage());
                holder.message.setTextColor(getResources().getColor(R.color.friend_txt_color));

                try
                {
                    if(mLiveComments.size() > i)
                    {
                        holder.name.setTextColor(getResources().getColor(mCommentColors[(Integer.valueOf(mLiveComments.get(i).getColorCode().substring(1, mLiveComments.get(i).getColorCode().length()), 16).intValue()%7)]));
                    }
                    else holder.name.setTextColor(getResources().getColor(mCommentColors[0]));
                }
                catch (Exception e)
                {
                    holder.name.setTextColor(getResources().getColor(mCommentColors[0]));
                }
            }

            if(mLiveComments.get(i).getIsSystem()==1)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_red_c);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getIsSystem()==2)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_orange_c);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getIsSystem()==3)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_green_c);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getIsSystem()==4)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_blue_c);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }
            else if(mLiveComments.get(i).getIsSystem()==5)
            {
                holder.layout.setBackgroundResource(R.drawable.btn_black_c2);
                holder.name.setTextColor(getResources().getColor(R.color.white));
                holder.message.setTextColor(getResources().getColor(R.color.white));
            }

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    try
                    {
                        if(mLiveComments.size() > i)
                        {
                            showUserDialog(i,1,null,mLiveComments.get(i));
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            });

            return convertView;
        }
    }

    private class ViewHolderComment
    {
        TextView name;
        TextView message;
        LinearLayout layout;
    }

    private int getLiveStreamAbsTime()
    {
        Calendar time = Calendar.getInstance(TimeZone.getDefault());

        if(mLiveStreamAbsTime==0)
        {
            mLiveStreamAbsTime = (int)time.getTimeInMillis();
        }

        return (int)time.getTimeInMillis() - mLiveStreamAbsTime;
    }

    /* PCMAudioRecorderDelegate */
    @Override
    public void didGetAudioData(final byte[] buffer, final int readBytes)
    {
//        Singleton.log("readBytes: "+readBytes);

        if(!isLiveState) return ;

        if(readBytes < 0 ) return;

        if(isSocketEstablished()==false){
            openSocket();
            return;
        }

        audioEncodeHandler.post(new Runnable() {
            @Override
            public void run() {
                int totalBytesConsumed = 0;

                while(totalBytesConsumed<readBytes) {
                    // fill data into audioDatBuffer
                    int consumedBytes = Math.min(readBytes-totalBytesConsumed, audioDataBuffer.remaining());
//            Singleton.log("audioDataBuffer.remaing: "+audioDataBuffer.remaining());
//            Singleton.log("consumedBytes: " + consumedBytes);

                    audioDataBuffer.put(buffer, totalBytesConsumed, consumedBytes);
                    totalBytesConsumed += consumedBytes;
//            Singleton.log("totalBytesConsumed: "+totalBytesConsumed);

                    if(audioDataBuffer.remaining()>0) {
//                Singleton.log("BREAK");
                        break;
                    }

                    // encode audio
                    final ByteBuffer encodedAudioDataBuffer;
                    try {
                        encodedAudioDataBuffer = ByteBuffer.allocateDirect(1024);
                    }
                    catch (OutOfMemoryError o){
                        break;
                    }
                    catch (Exception e){
                        break;
                    }

                    final int encodedAudoDataLength = OpusCodec.instance().encodeAudio(audioDataBuffer, encodedAudioDataBuffer);

//            Singleton.log("=====================================================encodedAudoDataLength: "+encodedAudoDataLength);

                    audioDataBuffer.rewind();

                    networkSendHandler.post(new Runnable() {
                        public void run() {
                            // check if need to slice a new chunk
                            if(audioSequenceNumber % NUM_OF_AUDIO_SAMPLES_PER_CHUNK==0) {
                                chunkID++;

                                // upload chunk signal
                                if(audioSequenceNumber>0)
                                {
                                    try
                                    {
                                        ByteBuffer uploadByteBuffer = ByteBuffer.allocate(1);
                                        uploadByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                                        uploadByteBuffer.put(TCP_COMMENT_CODE_UPLOAD);

                                        if(isSocketEstablished()==false){
                                            openSocket();
                                            return;
                                        }

                                        OutputStream os = tcpSocket.getOutputStream();
                                        os.write(uploadByteBuffer.array(), 0, uploadByteBuffer.array().length);
                                        os.flush();
                                    } catch (Exception e) {
                                        Singleton.log("e: "+ e.getMessage());
                                    }
                                }

                                // begin chunk
                                try {
                                    final ByteBuffer beginByteBuffer = ByteBuffer.allocate(9);
                                    beginByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                                    beginByteBuffer.put(TCP_COMMENT_CODE_BEGIN);
                                    beginByteBuffer.putInt(liveStreamID);
                                    beginByteBuffer.putInt(chunkID);

                                    if(isSocketEstablished()==false){
                                        openSocket();
                                        return;
                                    }

                                    OutputStream os = tcpSocket.getOutputStream();
                                    os.write(beginByteBuffer.array(), 0, beginByteBuffer.array().length);
                                    os.flush();
                                } catch (IOException e) {
                                    Singleton.log("e: " + e.getMessage());
                                }
                            }

                            audioSequenceNumber++;

                            // send audio data
                            try
                            {
                                final ByteBuffer headerByteBuffer = ByteBuffer.allocate(11+encodedAudoDataLength);
                                headerByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                                headerByteBuffer.put(TCP_COMMENT_CODE_DATA);
                                headerByteBuffer.putInt(encodedAudoDataLength);
                                headerByteBuffer.putInt(getLiveStreamAbsTime());
                                headerByteBuffer.put(TCP_HEADER);
                                headerByteBuffer.put(PACKET_TYPE_AUDIO);
                                headerByteBuffer.put(encodedAudioDataBuffer.array(), encodedAudioDataBuffer.arrayOffset(), encodedAudoDataLength);

                                if(isSocketEstablished()==false){
                                    openSocket();
                                    return;
                                }

                                OutputStream os = tcpSocket.getOutputStream();
                                os.write(headerByteBuffer.array(), 0, headerByteBuffer.array().length);
                                os.flush();
                            } catch (IOException e) {
                                Singleton.log("e: "+ e.getMessage());
                            }
                        }
                    });
                }
            }
        });
    }

    private class ChangePreviewTask extends AsyncTask<Integer, Integer, Void>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Integer... pos)
        {
            try
            {
                if(isUsingFrontCamera==true)
                {
                    camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                }
                else
                {
                    camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                }

                if (camera != null)
                {
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setPreviewFormat(ImageFormat.NV21);
                    parameters.setPreviewSize(1280, 720);

                    if(isUsingFrontCamera==false)
                    {
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    }

                    camera.setDisplayOrientation(90);

                    try {
                        camera.setParameters(parameters);
                    }
                    catch (Exception e){
                    }

                    camera.setPreviewDisplay(surfaceHolder);
                    camera.startPreview();
                    camera.setPreviewCallback(mCtx);
                }
            }
            catch (Exception e)
            {

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void pos)
        {
            super.onPostExecute(pos);
            showKeyboard();
        }
    }

    private void startCamera() {
        stopCamera();

        handler.post(new Runnable() {
            public void run() {
                try {
                    if(isUsingFrontCamera==true) {
                        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    } else {
                        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                    }
                } catch (Exception e) {
                }

                if (camera != null) {
                    Camera.Parameters parameters = camera.getParameters();

                    parameters.setPreviewFormat(ImageFormat.NV21);
                    parameters.setPreviewSize(1280, 720);

                    if(isUsingFrontCamera==false) {
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    }

                    camera.setDisplayOrientation(90);

                    try {
                        camera.setParameters(parameters);
                    }
                    catch (Exception e){
                    }
                    startPreview();
                }
            }
        });
    }

    private void startPreview() {
        if(camera != null) {
            stopPreview();

            try {
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                camera.setPreviewCallback(mCtx);
            } catch(Exception e) {

            }
        }
    }

    private void stopPreview() {
        try{
            camera.setPreviewCallback(null);
            camera.stopPreview();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void stopCamera() {
        try{
            if (camera != null) {
                stopPreview();
                camera.release();
                camera = null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /* Surface Callback */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopCamera();
    }

    int dropFrameCount = 0;
//    int encodeFrameCount = 0;
    boolean needUploadCoverPhoto = true;

    /* Preview Callback */
    @Override
    public void onPreviewFrame(final byte[] sourceYUVData, Camera camera)
    {
        if(sourceYUVData==null) {
            return;
        }

        if(!isLiveState) return ;

        if(isSocketEstablished()==false){
            openSocket();
            return;
        }

        if(isEncoding) {
//            Singleton.log("DROP FRAME!");
//            dropFrameCount++;
            return;
        }

//        Singleton.log("ENCODE FRAME!");

        if(needUploadCoverPhoto) {
            needUploadCoverPhoto = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
//                    Singleton.log("UPLOADING COVER PHOTO");

                    try {
                        // compress YUV data to JPEG at disk
                        final String coverPhotoFileName = Singleton.getUUIDFileName("jpg");
                        String coverPhotoFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + coverPhotoFileName;
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

                        YuvImage yuvImage = new YuvImage(sourceYUVData, ImageFormat.NV21, 1280, 720, null);
                        yuvImage.compressToJpeg(new Rect(280, 0, 1000, 720), 95, outputStream);

                        Matrix matrix = new Matrix();
                        if(isUsingFrontCamera) matrix.postRotate(-90);
                        else matrix.postRotate(90);
                        byte[] bytes = outputStream.toByteArray();
                        Bitmap bitmap = BitmapFactory.decodeByteArray(outputStream.toByteArray(), 0, bytes.length);
                        outputStream.close();

                        bitmap = Bitmap.createBitmap(bitmap, 0 , 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 95, new FileOutputStream(coverPhotoFilePath));

                        // change jpeg orientation
//                        ExifInterface exifInterface = new ExifInterface(coverPhotoFilePath);

//                        if(Integer.parseInt(exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION))!=ExifInterface.ORIENTATION_ROTATE_90) {
//                            exifInterface.setAttribute(ExifInterface., "" + ExifInterface.ORIENTATION_NORMAL);
//                            exifInterface.saveAttributes();
//                        }

                        // upload cover photo file
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<String> filesToUpload = new ArrayList<String>();
                                filesToUpload.add(coverPhotoFileName);

                                MultiFileUploader uploader = new MultiFileUploader(filesToUpload);
                                uploader.startUpload(mCtx, new MultiFileUploader.MultiFileUploaderCallback() {
                                    @Override
                                    public void didComplete() {
//                                        Singleton.log("UPLOAD COVER PHOTO SUCCESS");

                                        ApiManager.updateLiveStreamInfo(mCtx, user, liveStreamID, 0, coverPhotoFileName, new ApiManager.UpdateLiveStreamInfoCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {
//                                                    Singleton.log("UPDATE COVER PHOTO SUCCESS");
                                                }
                                            }
                                        });
                                    }

                                    @Override
                                    public void didFail() {
//                                        Singleton.log("UPLOAD COVER PHOTO FAIL");
                                    }
                                });
                            }
                        });
                    } catch (Exception e) {
//                        Singleton.log("UPLOAD COVER PHOTO EXCEPTION: "+e.toString());
                    }
                }
            }).start();
        }

//        encodeFrameCount++;
//        Singleton.log("ENCODE FRAME: "+encodeFrameCount+", DROP FRAME: "+dropFrameCount);

        isEncoding = true;
        videoEncodeHandler.post(new Runnable() {
            public void run() {
                /* IMPORTANT HACK: front camera buffer need fill in reversed order! */
                byte[] yuvData = yuvDataBuffer.array();
                int bufferOffset = yuvDataBuffer.arrayOffset();

                // fill y data
                int yDestOffset = 0;
                int ySrcOffset = 0;

                if (isUsingFrontCamera) {
                    yDestOffset = 640 * 360 - 1;
                }

                yDestOffset += bufferOffset;

                for (int y = 0; y < 360; y++) {
                    for (int x = 0; x < 640; x++) {
                        try {
                            yuvData[yDestOffset] = sourceYUVData[ySrcOffset];

                            if (isUsingFrontCamera) {
                                yDestOffset -= 1;
                            } else {
                                yDestOffset += 1;
                            }

                            ySrcOffset += 2;
                        } catch (Exception e) {

                        }
                    }

                    ySrcOffset += 1280; // skip entire line
                }

                // fill uv data
                int uDestOffset = 640 * 360;
                int vDestOffset = 640 * 360 * 5 / 4;
                int uvSrcOffset = 1280 * 720;

                if (isUsingFrontCamera) {
                    uDestOffset = (640 * 360 * 5 / 4 - 1);
                    vDestOffset = (640 * 360 * 3 / 2 - 1);
                }

                uDestOffset += bufferOffset;
                vDestOffset += bufferOffset;

                for (int y = 0; y < 180; y++) {
                    for (int x = 0; x < 320; x++) {

                        try {
                            yuvData[uDestOffset] = sourceYUVData[uvSrcOffset];
                            yuvData[vDestOffset] = sourceYUVData[uvSrcOffset + 1];

                            if (isUsingFrontCamera) {
                                uDestOffset -= 1;
                                vDestOffset -= 1;
                            } else {
                                uDestOffset += 1;
                                vDestOffset += 1;
                            }

                            uvSrcOffset += 4;
                        } catch (Exception e) {

                        }
                    }

//                    if(isUsingFrontCamera) {
//                        uDestOffset += 320*2;
//                        vDestOffset += 320*2;
//                    }

                    uvSrcOffset += 1280; // skip entire line
                }

//                Singleton.log("START VIDEO ENCODING");
                final int encodedDataLength = VP8Codec.instance().encodeFrame(yuvDataBuffer, encodedVideoDataBuffer);
                isEncoding = false;

//                Singleton.log("encodedDataLength: "+encodedDataLength);

                if (encodedDataLength == 0) {
                    return;
                }

                try {
//                    Singleton.log("chunkID : "+chunkID);

                    if (isSocketEstablished() && chunkID > 0) {

                        videoSequenceNumber++;
                        Singleton.log("chunkID : " + chunkID + ", ");

//                        Singleton.log("allocate : "  + 11 + encodedDataLength);

                        final ByteBuffer headerByteBuffer = ByteBuffer.allocate(11 + encodedDataLength);

                        headerByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                        headerByteBuffer.put(TCP_COMMENT_CODE_DATA);
                        headerByteBuffer.putInt(encodedDataLength);
                        headerByteBuffer.putInt(getLiveStreamAbsTime());
                        headerByteBuffer.put(TCP_HEADER);
                        headerByteBuffer.put(PACKET_TYPE_VIDEO);
                        headerByteBuffer.put(encodedVideoDataBuffer.array(), encodedVideoDataBuffer.arrayOffset(), encodedDataLength);
//                      currentTcpDataTag++;

//                        Singleton.log("headerByteBuffer : "  + headerByteBuffer);

//                        queuedTcpDataCount++;
//                        Singleton.log("queuedTcpDataCount: --------------------------------------"+queuedTcpDataCount);

                        networkSendHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
//                                    if(queuedTcpDataCount > 50)
//                                    {
//                                        downBitRate();
//                                        return;
//                                    }

                                    if (isSocketEstablished() == false) {
                                        openSocket();
                                        return;
                                    }

                                    OutputStream os = tcpSocket.getOutputStream();
                                    os.write(headerByteBuffer.array(), 0, headerByteBuffer.array().length);
                                    os.flush();

//                                    queuedTcpDataCount--;

                                } catch (IOException e) {

                                }
                            }
                        });

                    } else {
                        Singleton.log("LOSS VIDEO CHUNK");
                    }
                } catch (Exception e) {

                }
            }
        });
    }

//    int queuedTcpDataCount = 0;

    private void downBitRate()
    {
        if(mBitRateLevel == 4) return;

        Singleton.log("DOWN BITRATE!");
        mBitRateLevel ++;

        audioSequenceNumber = 0;

//        queuedTcpDataCount = 0;
        openSocket();

        videoEncodeHandler.post(new Runnable() {
            @Override
            public void run() {
                VP8Codec.instance().setup(getVideoBitRateLevel(mBitRateLevel));
            }
        });

        audioEncodeHandler.post(new Runnable() {
            @Override
            public void run() {
                OpusCodec.instance().setup(getAudioBitRateLevel(mBitRateLevel));
            }
        });
    }

    private void sendKeepAliveData() {
//        Singleton.log("SEND KEEP ALIVE!");
        final ByteBuffer keepAliveByteBuffer = ByteBuffer.allocate(5);
        keepAliveByteBuffer.order(ByteOrder.LITTLE_ENDIAN);

        keepAliveByteBuffer.put(PACKET_TYPE_KEEP_ALIVE);
        keepAliveByteBuffer.putInt(liveStreamID);

        // send encoded video data
        if(networkSendHandler==null) {
            return;
        }

        if(!isSocketEstablished()) {
            return;
        }

        ApiManager.keepLiveStream(mCtx, user, liveStreamID, new ApiManager.KeepLiveStreamCallback()
        {
            @Override
            public void onResult(boolean success, String message)
            {
                if (success)
                {
                    LagSix = 0;

                    if(message.compareTo("live_killed")==0)
                    {
                        if(mEnd) return;

//                        try
//                        {
//                            final ByteBuffer EndByteBuffer = ByteBuffer.allocate(5);
//                            EndByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
//
//                            EndByteBuffer.put(PACKET_TYPE_STREAM_END);
//                            EndByteBuffer.putInt(liveStreamID);
//
//                            if(networkSendHandler!=null)
//                            {
//                                networkSendHandler.post(new Runnable()
//                                {
//                                    public void run()
//                                    {
//                                        try
//                                        {
//                                            datagramSocket.send(new DatagramPacket(EndByteBuffer.array(), EndByteBuffer.position(), new InetSocketAddress(mUDP_IP, Constants.LIVE_STREAM_UDP_SERVER_PORT)));
//                                        }
//                                        catch (Exception e)
//                                        {
//
//                                        }
//                                    }
//                                });
//                            }
//                        }
//                        catch (Exception e)
//                        {
//
//                        }

                        mEnd = true;
                        if(mLoveTimer!=null)
                        {
                            mLoveTimer.cancel();
                            mLoveTimer.purge();
                            mLoveTimer = null;
                        }

                        closeSocket();

                        if(pcmAudioRecorder!=null) pcmAudioRecorder.stopRecording();

                        stopCamera();

                        try {
                            Dialog Enddialog = new Dialog(mCtx,R.style.LivePlayerDialog);
                            Enddialog.setContentView(R.layout.live_end_dailog);
                            Window window = Enddialog.getWindow();
                            window.setGravity(Gravity.BOTTOM);
                            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            Enddialog.setCancelable(false);

                            TextView mTitle = (TextView) Enddialog.findViewById(R.id.title);
                            mTitle.setText(getString(R.string.live_publish_kill));

                            Button mEnd = (Button)Enddialog.findViewById(R.id.end);
                            mEnd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    hideKeyboard();
                                    mCtx.finish();
                                }
                            });
                            Enddialog.show();
                        }
                        catch (Exception e){
                            hideKeyboard();
                            mCtx.finish();
                        }
                    }
                }
                else
                {
                    LagSix++;
                }
            }
        });

        networkSendHandler.post(new Runnable() {
            public void run()
            {
                try
                {
                    datagramSocket.send(new DatagramPacket(keepAliveByteBuffer.array(), keepAliveByteBuffer.position(), new InetSocketAddress(mUDP_IP, Constants.LIVE_STREAM_UDP_SERVER_PORT)));

//                    if(lag==0)
//                    {
//                        if(!mLoadding.isShown())
//                        {
//                            handler.post(new Runnable()
//                            {
//                                @Override
//                                public void run()
//                                {
//                                    mLoadding.setBackgroundResource(R.drawable.live_loading);
//                                    mAnimaition = (AnimationDrawable) mLoadding.getBackground();
//                                    mAnimaition.start();
//                                    mLoadding.setVisibility(View.VISIBLE);
//                                }
//                            });
//                        }
//                    }
//                    else
//                    {
//                        lag=0;
//                        if(mLoadding.isShown())
//                        {
//                            handler.post(new Runnable()
//                            {
//                                @Override
//                                public void run()
//                                {
//                                    mLoadding.clearAnimation();
//                                    mAnimaition.stop();
//                                    mLoadding.setVisibility(View.GONE);
//                                }
//                            });
//                        }
//                    }
                }
                catch (Exception e)
                {

                }
            }
        });

        TenPos++;
        if(TenPos > 30) TenPos = 0;

//        if(isLiveState)
//        {
//            mUnRead++;
//            if(mUnRead > 10)
//            {
//                mUnRead = 0;
//
//                ApiManager.getUnreadReceivedGift(mCtx, liveStreamID, , 15, new ApiManager.GetUnreadReceivedGiftCallback()
//                {
//                    @Override
//                    public void onResult(boolean success, ArrayList<ReadGiftModel> readGiftModels)
//                    {
//                        if(success && readGiftModels!=null)
//                        {
//                            if(readGiftModels.size()!=0)
//                            {
//                                for(int i = 0 ; i < readGiftModels.size() ; i++)
//                                {
//                                    LiveGiftsModel mLiveGiftsModel = new LiveGiftsModel();
//                                    mLiveGiftsModel.setUserInfo(readGiftModels.get(i).getUserInfo());
//                                    mLiveGiftsModel.setGiftInfo(readGiftModels.get(i).getGiftInfo());
//                                    mLiveGiftsModel.setTimestamp(readGiftModels.get(i).getTimestamp());
//
//                                    if(mLiveGiftsModels.size()==0 || mLiveGiftsModels.size()==1) mLiveGiftsModels.add(mLiveGiftsModel);
//                                    else
//                                    {
//                                        int pos = mLiveGiftsModels.size()-1;
//                                        for(int k = 1 ; k < mLiveGiftsModels.size() ; k++)
//                                        {
//                                            if(mLiveGiftsModel.getGiftInfo().getPoint() > mLiveGiftsModels.get(k).getGiftInfo().getPoint())
//                                            {
//                                                pos = k;
//                                                break;
//                                            }
//                                        }
//                                        mLiveGiftsModels.add(pos,mLiveGiftsModel);
//                                    }
//                                }
//
//
//                            }
//                        }
//                    }
//                });
//            }
//        }
    }

    private void endLive()
    {
        if(mEndstate) return;

        try
        {
            final ByteBuffer EndByteBuffer = ByteBuffer.allocate(5);
            EndByteBuffer.order(ByteOrder.LITTLE_ENDIAN);

            EndByteBuffer.put(PACKET_TYPE_STREAM_END);
            EndByteBuffer.putInt(liveStreamID);

            if(networkSendHandler!=null)
            {
                networkSendHandler.post(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            datagramSocket.send(new DatagramPacket(EndByteBuffer.array(), EndByteBuffer.position(), new InetSocketAddress(mUDP_IP, Constants.LIVE_STREAM_UDP_SERVER_PORT)));
                        }
                        catch (Exception e)
                        {

                        }
                    }
                });
            }
        }
        catch (Exception e)
        {

        }
        mEnd = true;
        if(mLoveTimer!=null)
        {
            mLoveTimer.cancel();
            mLoveTimer.purge();
            mLoveTimer = null;
        }

//                    networkSendHandler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            ApiManager.likeLivestreamBatchUpdate(mCtx, user, liveStreamID, mLoveCount);
//                        }
//                    });

        int duration = Singleton.getCurrentTimestamp() - mStartCurrentTime;

        ApiManager.endLiveStream(mCtx, user, liveStreamID, duration, new ApiManager.EndLiveStreamCallback()
        {
            @Override
            public void onResult(boolean success, String message)
            {
                mEndstate = true;

                Handler mEndHandler = new Handler();
                mEndHandler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ApiManager.getLiveStreamInfo(mCtx, user, liveStreamID, new ApiManager.GetLiveStreamInfoCallback()
                        {
                            @Override
                            public void onResult(boolean success, LiveModel mLiveModel)
                            {
                                if (success && mLiveModel != null)
                                {
                                    mClose.setVisibility(View.GONE);
                                    RelativeLayout mEndLayout = (RelativeLayout) findViewById(R.id.end_layout);
                                    TextView mViewer = (TextView) findViewById(R.id.viewer_count);
                                    TextView mLike = (TextView) findViewById(R.id.liker_count);
                                    TextView mTimeCount = (TextView) findViewById(R.id.time_count);
                                    TextView mTime = (TextView) findViewById(R.id.time);
                                    Button mEnd = (Button) findViewById(R.id.end);

                                    mEndLayout.setVisibility(View.VISIBLE);
                                    mViewer.setText(String.valueOf(mLiveModel.getViewerCount()));
                                    mLike.setText(String.valueOf(mLoveCount));

                                    SimpleDateFormat mDateFormatTo = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                                    mTimeCount.setText(mDateFormatTo.format(new Date((mLiveModel.getTotalViewTime() * 1000L) - TimeZone.getDefault().getRawOffset())));
                                    mTime.setText(mDateFormatTo.format(new Date((mLiveModel.getDuration() * 1000L) - TimeZone.getDefault().getRawOffset())));

                                    mEnd.setOnClickListener(new View.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(View v)
                                        {
                                            mApplication.setReLoad(true);
                                            hideKeyboard();
                                            mCtx.finish();
                                        }
                                    });
                                }
                                else
                                {
                                    mApplication.setReLoad(true);
                                    hideKeyboard();
                                    mCtx.finish();
                                }
                            }
                        });
                    }
                }, 500);
            }
        });
    }

    private void showGiftComment(GiftModel gift, String open, String pic)
    {
        if(gift.getPoint() >= 500)
        {
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + pic), mGiftIconPicBest, PeopleOptions);
            mGiftUserNameBest.setText(open);
            mGiftUserNameBest.setTextColor(getResources().getColor(gift_text_colors[(int) (Math.random() * gift_text_colors.length)]));
            mGiftGiftNameBest.setText(gift.getName());
            mGiftCommentBest.setVisibility(View.VISIBLE);
        }
        else
        {
            mGiftIconPic.setImageResource(gift_loves[(int) (Math.random() * gift_loves.length)]);
            mGiftUserName.setText(open);
            mGiftUserName.setTextColor(getResources().getColor(gift_text_colors[(int) (Math.random() * gift_text_colors.length)]));
            mGiftGiftName.setText(gift.getName());
            mGiftComment.setVisibility(View.VISIBLE);
        }

//        int img = gift_loves[(int) (Math.random() * gift_loves.length)];
//        mBestGiftIconPicLeft.setImageResource(img);
//        mBestGiftIconPicRight.setImageResource(img);
//        mBestGiftUserName.setText(open);
//        mBestGiftUserName.setTextColor(getResources().getColor(gift_text_colors[(int) (Math.random() * gift_text_colors.length)]));
//        mBestGiftGiftName.setText(getString(R.string.gift_give_text) + " " + gift.getName());
//        mBestGiftComment.setVisibility(View.VISIBLE);

//        mGiftIconPic.setImageResource(gift_loves[(int) (Math.random() * gift_loves.length)]);
//        mGiftUserName.setText(open);
//        mGiftUserName.setTextColor(getResources().getColor(gift_text_colors[(int) (Math.random() * gift_text_colors.length)]));
//        mGiftGiftName.setText(getString(R.string.gift_give_text) + " " + gift.getName());
//        mGiftComment.setVisibility(View.VISIBLE);
    }

    //leaderboard
    //private PagerAdapter mPagerAdapter = new PagerAdapter()
    private class LeaderPagerAdapter extends PagerAdapter
    {
        @Override
        public boolean isViewFromObject(View arg0, Object arg1)
        {
            return arg0 == arg1;
        }

        @Override
        public int getCount()
        {
            return mLeaderBoardPage.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object)
        {
            container.removeView(mLeaderBoardPage.get(position));
        }

        @Override
        public int getItemPosition(Object object)
        {
            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            if(position==0)
            {
                container.addView(mLeaderBoardPage.get(position));
                View view = mLeaderBoardPage.get(position);
                mLiveTotalPoint =(TextView) view.findViewById(R.id.total_gift_point);
                final ListView mList = (ListView) view.findViewById(R.id.list);
                final ProgressBar progressPage1 = (ProgressBar) view.findViewById(R.id.progress);
                final TextView nodata = (TextView) view.findViewById(R.id.nodata);

                progressPage1.setVisibility(View.VISIBLE);

                ApiManager.getLiveStreamGiftLeaderboard(mCtx, user, liveStreamID, 0, 100, new ApiManager.GetLivestreamGiftLeaderboardCallback() {
                    @Override
                    public void onResult(boolean success, GiftLivestreamLeaderBoardModel liveGiftLeaderboard) {

                        progressPage1.setVisibility(View.INVISIBLE);

                        if (success && liveGiftLeaderboard != null) {

                            mLiveTotalPoint.setText(String.valueOf(liveGiftLeaderboard.getTotalPoint()));
                            if(liveGiftLeaderboard.getTotalPoint() !=0) {

                                nodata.setVisibility(View.GONE);
                                mModelsLive.clear();
                                mModelsLive.addAll(liveGiftLeaderboard.getGiftLeaderboardInfo());
                                mList.setAdapter(mLeaderAdapter);
                            }
                            else {
                                nodata.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            mLiveTotalPoint.setText("0");
                            nodata.setVisibility(View.VISIBLE);
                        }
                    }
                });

                return mLeaderBoardPage.get(position);
            }
            else
            {
                container.addView(mLeaderBoardPage.get(position));
                View view = mLeaderBoardPage.get(position);
                final PullToRefreshListView mListGift = (PullToRefreshListView) view.findViewById(R.id.list);
                final TextView mNoData = (TextView) view.findViewById(R.id.nodata_tab2);
                final ProgressBar progressPage2 = (ProgressBar) view.findViewById(R.id.progress);

                progressPage2.setVisibility(View.VISIBLE);

                ApiManager.getLiveStreamReceivedGifts(mCtx, liveStreamID, Integer.MAX_VALUE, 100, new ApiManager.GetLiveStreamReceivedGiftsCallback() {
                    @Override
                    public void onResult(boolean success, ArrayList<LiveGiftsModel> liveGiftsModels) {
                        //mProgress.setVisibility(View.GONE);
                        progressPage2.setVisibility(View.INVISIBLE);

                        if (success && liveGiftsModels != null) {
                            if (liveGiftsModels.size() != 0) {
                                mNoData.setVisibility(View.GONE);
                                mModels.clear();
                                mModels.addAll(liveGiftsModels);
                                //mMyAdapter = new MyAdapter();
                                mListGift.setAdapter(mMyAdapter);

                                if (liveGiftsModels.size() < 15) {
                                    // noMoreData = true;
                                }
                            } else {
                                mNoData.setVisibility(View.VISIBLE);
                            }
                        } else {
                            try {
                                mNoData.setVisibility(View.VISIBLE);
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            } catch (Exception e) {

                            }
                        }
                    }
                });
                return mLeaderBoardPage.get(position);
            }
        }
    };

    LeaderAdapter mLeaderAdapter = new LeaderAdapter();
    private class LeaderAdapter extends BaseAdapter
    {
        //private ArrayList<GiftLeaderboardModel> mModels = new ArrayList<GiftLeaderboardModel>();

//        public LeaderAdapter(ArrayList<GiftLeaderboardModel> model)
//        {
//            mModelsLiveAll.addAll(model);
//        }

        @Override
        public int getCount()
        {
            return mModelsLive.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            LiveGiftViewHolder holder = new LiveGiftViewHolder();


            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.present_board_row_live, null);
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.open = (TextView) convertView.findViewById(R.id.open);
                holder.like = (TextView) convertView.findViewById(R.id.like);
                holder.num = (TextView) convertView.findViewById(R.id.num);
                holder.number1_layout = (RelativeLayout) convertView.findViewById(R.id.number1_layout);
                holder.number1_pic = (ImageView) convertView.findViewById(R.id.number1_pic);
                holder.number1_like = (TextView) convertView.findViewById(R.id.number1_like);
                holder.number1_open = (TextView) convertView.findViewById(R.id.number1_open);
                holder.number2_layout = (RelativeLayout) convertView.findViewById(R.id.number2_layout);
                holder.number2_pic = (ImageView) convertView.findViewById(R.id.number2_pic);
                holder.number2_like = (TextView) convertView.findViewById(R.id.number2_like);
                holder.number2_open = (TextView) convertView.findViewById(R.id.number2_open);
                holder.number3_layout = (RelativeLayout) convertView.findViewById(R.id.number3_layout);
                holder.number3_pic = (ImageView) convertView.findViewById(R.id.number3_pic);
                holder.number3_like = (TextView) convertView.findViewById(R.id.number3_like);
                holder.number3_open = (TextView) convertView.findViewById(R.id.number3_open);
                convertView.setTag(holder);
            }
            else holder = (LiveGiftViewHolder) convertView.getTag();

            final int pos = position;

            if(pos==0)
            {
                holder.number1_layout.setVisibility(View.VISIBLE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModelsLive.get(position).getUserInfo().getPicture()), holder.number1_pic, PeopleOptions);
                holder.number1_like.setText(String.valueOf(mModelsLive.get(pos).getPoint()));
                holder.number1_open.setText(mModelsLive.get(position).getUserInfo().getOpenID());
            }
            else if(pos==1)
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.VISIBLE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModelsLive.get(position).getUserInfo().getPicture()), holder.number2_pic, PeopleOptions);
                holder.number2_like.setText(String.valueOf(mModelsLive.get(pos).getPoint()));
                holder.number2_open.setText(mModelsLive.get(position).getUserInfo().getOpenID());
            }
            else if(pos==2)
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.VISIBLE);
                holder.layout.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModelsLive.get(position).getUserInfo().getPicture()), holder.number3_pic, PeopleOptions);
                holder.number3_like.setText(String.valueOf(mModelsLive.get(pos).getPoint()));
                holder.number3_open.setText(mModelsLive.get(position).getUserInfo().getOpenID());
            }
            else
            {
                holder.number1_layout.setVisibility(View.GONE);
                holder.number2_layout.setVisibility(View.GONE);
                holder.number3_layout.setVisibility(View.GONE);
                holder.layout.setVisibility(View.VISIBLE);
                ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModelsLive.get(position).getUserInfo().getPicture()), holder.pic, PeopleOptions);
                holder.open.setText(mModelsLive.get(position).getUserInfo().getOpenID());
                holder.like.setText(String.valueOf(mModelsLive.get(pos).getPoint()));
                holder.num.setText("NO."+(position+1));
            }

            return convertView;
        }
    }

    private class LiveGiftViewHolder
    {
        LinearLayout layout;
        ImageView pic;
        TextView open;
        ImageView love;
        TextView name;
        TextView like;
        ImageView verifie;

        ImageView king;
        TextView num;

        RelativeLayout number1_layout;
        ImageView number1_pic;
        TextView number1_like;
        ImageView number1_love;
        ImageView number1_king;
        TextView number1_open;
        ImageView number1_verifie;
        TextView number1_name;

        RelativeLayout number2_layout;
        ImageView number2_pic;
        TextView number2_like;
        ImageView number2_love;
        ImageView number2_king;
        TextView number2_open;
        ImageView number2_verifie;
        TextView number2_name;

        RelativeLayout number3_layout;
        ImageView number3_pic;
        TextView number3_like;
        ImageView number3_love;
        ImageView number3_king;
        TextView number3_open;
        ImageView number3_verifie;
        TextView number3_name;
    }

    MyAdapter mMyAdapter = new MyAdapter();
    private class MyAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderGrid holder = new ViewHolderGrid();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.my_presend_row_live, null);
                holder.presend_layout = (LinearLayout) convertView.findViewById(R.id.presend_layout_live);
                holder.pic = (ImageView) convertView.findViewById(R.id.pic);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.image = (ImageView) convertView.findViewById(R.id.image);

                convertView.setTag(holder);
            }
            else holder = (ViewHolderGrid) convertView.getTag();

            final int pos = position;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mModels.get(position).getUserInfo().getPicture()), holder.pic, PeopleOptions);
            holder.name.setText(mModels.get(position).getUserInfo().getOpenID());

            //holder.image.setText(mModels.get(position).getGiftInfo().getName());
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mModels.get(position).getGiftInfo().getIcon()), holder.image, PeopleOptions);

            holder.presend_layout.getScaleX();



            holder.presend_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isInLeaderboard = true;
                    //LeaderboardLog.cancel();
                    LeaderboardLog.hide();
                    //leaderboardani.removeAllViews();
                    mGiftAni.removeAllViews();
                    GiftView mGiftView = new GiftView(mCtx, mModels.get(position).getGiftInfo().getFrameDuration(), mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels, mModels.get(position).getGiftInfo().getGiftID(), mModels.get(position).getGiftInfo().getNumOfImages(),mModels.get(position).getGiftInfo().getArchiveFileName().substring(0,mModels.get(position).getGiftInfo().getArchiveFileName().length()-4));
                    mGiftView.mGiftEndListener = mCtx;
                    mGiftAni.addView(mGiftView);
                }
            });

//            if(position>=getCount()-5)
//            {
//                LoadDataFollow(false);
//            }

            return convertView;
        }
    }

    private class ViewHolderGrid
    {
        LinearLayout presend_layout;
        ImageView pic;
        TextView name;
        ImageView image;
    }
}