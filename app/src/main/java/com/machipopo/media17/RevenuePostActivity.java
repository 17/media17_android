package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.model.FeedModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by POPO on 6/22/15.
 */
public class RevenuePostActivity extends BaseNewActivity
{
    private RevenuePostActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;

    private String c = "USD";
    private DecimalFormat df;

    private ListView mList;
    private ProgressBar mProgress;
    private ImageView mNoData;

    private ArrayList<FeedModel> mFeedModels = new ArrayList<FeedModel>();
    private DisplayImageOptions imgOptions;

    private SimpleDateFormat birthday;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_post_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        mList = (ListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        c = Singleton.getCurrencyType();
        df = new DecimalFormat("#.####");

        mProgress.setVisibility(View.VISIBLE);
        mNoData.setVisibility(View.GONE);
        ApiManager.getUserPost(mCtx, mApplication.getUser().getUserID(), Integer.MAX_VALUE, 100, new ApiManager.GetUserPostCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
            {
                mProgress.setVisibility(View.GONE);
                if (success && feedModel!=null)
                {
                    if(feedModel.size()!=0)
                    {
                        mNoData.setVisibility(View.GONE);

                        mFeedModels.clear();
                        mFeedModels.addAll(feedModel);

                        mList.setAdapter(new MyAdapter());
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    try
                    {
                        if(mCtx!=null)
                        {
                            try{
//                                             showToast(getString(R.string.failed));
                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                    catch(Exception e)
                    {

                    }
                }
            }
        });

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, PhotoActivity.class);
                intent.putExtra("photo", mFeedModels.get(position).getPicture());
                intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                intent.putExtra("day", Singleton.getElapsedTimeString(mFeedModels.get(position).getTimestamp()));
                intent.putExtra("view", mFeedModels.get(position).getViewCount());
                intent.putExtra("money", mFeedModels.get(position).getTotalRevenue());
                intent.putExtra("dio", mFeedModels.get(position).getCaption());
                intent.putExtra("likecount", mFeedModels.get(position).getLikeCount());
                intent.putExtra("commentcount", mFeedModels.get(position).getCommentCount());
                intent.putExtra("likeed", mFeedModels.get(position).getLiked());

                intent.putExtra("postid", mFeedModels.get(position).getPostID());
                intent.putExtra("userid", mFeedModels.get(position).getUserID());

                intent.putExtra("name", mFeedModels.get(position).getUserInfo().getName());
                intent.putExtra("isFollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                intent.putExtra("postCount", mFeedModels.get(position).getUserInfo().getPostCount());
                intent.putExtra("followerCount", mFeedModels.get(position).getUserInfo().getFollowerCount());
                intent.putExtra("followingCount", mFeedModels.get(position).getUserInfo().getFollowingCount());
                intent.putExtra("goto", false);
                intent.putExtra("type", mFeedModels.get(position).getType());
                intent.putExtra("video", mFeedModels.get(position).getVideo());
                startActivity(intent);
            }
        });

        birthday = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        imgOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_post));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class MyAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.revenue_post_row, null);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.info = (TextView) convertView.findViewById(R.id.info);
                holder.views = (TextView) convertView.findViewById(R.id.views);
                holder.money = (TextView) convertView.findViewById(R.id.money);
                holder.likes = (TextView) convertView.findViewById(R.id.likes);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            Date date = new Date(mFeedModels.get(position).getTimestamp()*1000L);
            holder.day.setText(birthday.format(date));
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getPicture()), holder.img, imgOptions);
            holder.info.setText(mFeedModels.get(position).getCaption());
            holder.views.setText(String.valueOf(mFeedModels.get(position).getViewCount()));
            holder.likes.setText(String.valueOf(mFeedModels.get(position).getLikeCount()));

            float all = 0f;
            all = mFeedModels.get(position).getTotalRevenue()*Singleton.getCurrencyRate();
            holder.money.setText(/*"$ " + */df.format((double) all) + " " + c);

            return convertView;
        }
    }

    private class ViewHolder
    {
        TextView day;
        ImageView img;
        TextView info;
        TextView views;
        TextView money;
        TextView likes;
    }
}
