package com.machipopo.media17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshHeaderGridView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.View.TouchImage;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.machipopo.media17.utils.LogEventUtil;
import com.machipopo.media17.utils.ShareUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.tencent.connect.UserInfo;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendAuth;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;
import com.warnyul.android.widget.FastVideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by POPO on 5/29/15.
 */
public class HomeUserActivity extends BaseActivity
{
    private UserModel userModel;

    private HomeUserActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;

    private ImageView mImg;
    private TextView mFollowers, mFollowing,mFollowersText,mFollowingText;
    private Button mFollow;
    private ImageView mDown;
    private LinearLayout mSuggestion;
    private TextView mOpen, mDio,mWeb;
    private ImageView mGrid, mList, mLike;
    private ImageView mVerifie;
    private ImageView mLikecountIcon;
    private TextView mLikecount;

    private PullToRefreshHeaderGridView mScroll;
    private RelativeLayout mBear;
    private ProgressBar mProgress;
    private ImageView mNoData;

    private String targetUserID = "";
    private String title = "", picture = "", open = "", bio = "",web = "";
    private int follow = 0, following = 0;
    private String likecount = "0";

    private int isfollowing;
    private int mGridHeight;

    private DisplayMetrics mDisplayMetrics;
    private DisplayImageOptions BigOptions, SelfOptions,imgOptions,GorpOptions;

    private GridAdapter mGridAdapter;
    private MyListAdapter mListAdapter;
    private LikeListAdapter mLikeListAdapter;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};
    private int bears[] = {R.drawable.bear_1,R.drawable.bear_2,R.drawable.bear_3,R.drawable.bear_4,R.drawable.bear_5,R.drawable.bear_6,R.drawable.bear_7,R.drawable.bear_8};
    private int bubbles[] = {R.drawable.bubble_1,R.drawable.bubble_2,R.drawable.bubble_3,R.drawable.bubble_4,R.drawable.bubble_5,R.drawable.bubble_6,R.drawable.bubble_7,R.drawable.bubble_8,R.drawable.bubble_9,R.drawable.bubble_10};
    private int cats[] = {R.drawable.cat_1,R.drawable.cat_2,R.drawable.cat_3,R.drawable.cat_4,R.drawable.cat_5,R.drawable.cat_6,R.drawable.cat_7,R.drawable.cat_8};
    private int rabbits[] = {R.drawable.rabbit_1,R.drawable.rabbit_2,R.drawable.rabbit_3,R.drawable.rabbit_4,R.drawable.rabbit_5,R.drawable.rabbit_6,R.drawable.rabbit_7,R.drawable.rabbit_8};
    private int logos[] = {R.drawable.logo_1,R.drawable.logo_2,R.drawable.logo_3,R.drawable.logo_4,R.drawable.logo_5,R.drawable.logo_6,R.drawable.logo_7,R.drawable.logo_8};

    private float mY = 0 ,mX = 0;

    private ArrayList<FeedModel> mFeedModels = new ArrayList<FeedModel>();
    private ArrayList<FeedModel> mLikMoedels = new ArrayList<FeedModel>();

    private View mHeader;

    private String[] mListMoreText;

    private Boolean MyisFetchingData = false;
    private Boolean MynoMoreData = false;
    private int MyNowPos = 0;
    private ArrayList<FastVideoView> mMyVideoLists = new ArrayList<FastVideoView>();

    private Boolean LikeisFetchingData = false;
    private Boolean LikenoMoreData = false;
    private int LikeNowPos = 0;
    private ArrayList<FastVideoView> mLikeVideoLists = new ArrayList<FastVideoView>();

    private Boolean mVideoPlayState = true;
    private int mMyPagte = 0;

    private Boolean mSetHeaderHeight = true;

    private LinearLayout mFollowerLayout,mFollowingLayout,mLikeLayout;

    private ArrayList<UserModel> mSuggestionModel = new ArrayList<UserModel>();
    private Button mNowLive;
    private Boolean live = false;
    private String live_openid="",live_caption="",live_picture="";
    private int live_id = 0;
    private Boolean mLiveState = false;

    private int mLikeCount = 0;
    private String mLikePos = "";
    private String mLikePosUser = "";
    private Timer mTimer;
    private int lastCount = 0;
    private Boolean mGodHand = false;

    private Boolean isGetInfo = false;
    private Boolean isPrivate = false;
    private Boolean isRequest = false;
    private RelativeLayout mLoadLayout;
    private Boolean mFollowPrivate = false;
    private ProgressBar mBigProgress;
    private ImageView mBigNodata;
    private String mOpenID = "";
    private ImageView mPresent;
    private LinearLayout mPostPrivate;

    private Boolean mGuest = false;
    private int tabSwitchCount =0;  //to record how many times user switch between grid/list/love fields.

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_user_activity);

        mApplication = (Story17Application) mCtx.getApplication();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        mGridHeight = (int)((float)mDisplayMetrics.widthPixels/(float)3);

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("open_id")) mOpenID = mBundle.getString("open_id");
            if(mBundle.containsKey("title")) title = mBundle.getString("title");
            if(mBundle.containsKey("picture")) picture = mBundle.getString("picture");
            if(mBundle.containsKey("follow")) follow = mBundle.getInt("follow");
            if(mBundle.containsKey("following")) following = mBundle.getInt("following");
            if(mBundle.containsKey("open")) open = mBundle.getString("open");
            if(mBundle.containsKey("bio")) bio = mBundle.getString("bio");
            if(mBundle.containsKey("isfollowing")) isfollowing = mBundle.getInt("isfollowing");
            if(mBundle.containsKey("targetUserID")) targetUserID = mBundle.getString("targetUserID");
            if(mBundle.containsKey("web")) web = mBundle.getString("web");
            if(mBundle.containsKey("live")) live = mBundle.getBoolean("live");

            if(mBundle.containsKey("live_id")) live_id = mBundle.getInt("live_id");
            if(mBundle.containsKey("live_openid")) live_openid = mBundle.getString("live_openid");
            if(mBundle.containsKey("live_caption")) live_caption = mBundle.getString("live_caption");
            if(mBundle.containsKey("live_picture")) live_picture = mBundle.getString("live_picture");

            if(mBundle.containsKey("guest"))
            {
                mGuest = mBundle.getBoolean("guest");
            }
        }

        //event tracking
        try {
            LogEventUtil.EnterProfilePage(mCtx, mApplication, targetUserID);
        }
        catch (Exception x)
        {

        }

        if(mOpenID.length()!=0) initTitleBar(mOpenID);
        else initTitleBar(open);

        mScroll = (PullToRefreshHeaderGridView) findViewById(R.id.scroll);

        try {
            mHeader = inflater.inflate(R.layout.home_user_header, null);
            mScroll.getRefreshableView().addHeaderView(mHeader);
        }
        catch (OutOfMemoryError o){
            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
            System.gc();
            mCtx.finish();
            return;
        }
        catch (Exception e){
            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
            System.gc();
            mCtx.finish();
            return;
        }

        mImg = (ImageView) findViewById(R.id.img);
        mFollowers = (TextView) findViewById(R.id.followers);
        mFollowing = (TextView) findViewById(R.id.following);
        mFollowersText = (TextView) findViewById(R.id.followers_text);
        mFollowingText = (TextView) findViewById(R.id.following_text);
        mFollow = (Button) findViewById(R.id.follow);
        mDown = (ImageView) findViewById(R.id.down);
        mVerifie = (ImageView) findViewById(R.id.verifie);
        mSuggestion = (LinearLayout) findViewById(R.id.suggestion);
        mOpen = (TextView) findViewById(R.id.open);
        mDio = (TextView) findViewById(R.id.dio);
        mWeb = (TextView) findViewById(R.id.web);
        mLikecountIcon = (ImageView) findViewById(R.id.likecount_icon);
        mLikecount = (TextView) findViewById(R.id.likecount);
        mGrid = (ImageView) findViewById(R.id.grid);
        mList = (ImageView) findViewById(R.id.list);
        mLike = (ImageView) findViewById(R.id.like);

        mProgress = (ProgressBar) findViewById(R.id.progress);
        mBear = (RelativeLayout) findViewById(R.id.bear);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mFollowerLayout = (LinearLayout) findViewById(R.id.follower_layout);
        mFollowingLayout = (LinearLayout) findViewById(R.id.following_layout);
        mLikeLayout = (LinearLayout) findViewById(R.id.like_layout);

        mNowLive = (Button) findViewById(R.id.now_live);
        mBigProgress = (ProgressBar) findViewById(R.id.big_progress);

        mLoadLayout = (RelativeLayout) findViewById(R.id.load_layout);
        mBigNodata = (ImageView) findViewById(R.id.big_nodata);
        mPresent = (ImageView) findViewById(R.id.present);

        mPostPrivate = (LinearLayout) findViewById(R.id.post_private);

        imgOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        GorpOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        if(mOpenID.length()!=0)
        {
            mScroll.setVisibility(View.GONE);
            mBigProgress.setVisibility(View.VISIBLE);

            ApiManager.getUserInfo(mCtx, mOpenID, new ApiManager.GetUserInfoCallback()
            {
                @Override
                public void onResult(boolean success, String message, UserModel user)
                {
                    mBigProgress.setVisibility(View.GONE);

                    if (success && user != null)
                    {

                        title = user.getName();
                        picture = user.getPicture();
                        follow = user.getFollowerCount();
                        following = user.getFollowingCount();
                        open = user.getOpenID();
                        bio = user.getBio();
                        isfollowing = user.getIsFollowing();
                        targetUserID = user.getUserID();
                        web = user.getWebsite();

                        mScroll.setVisibility(View.VISIBLE);
                        mBigNodata.setVisibility(View.GONE);

                        runUser();

                        userModel = user;
                        isfollowing = user.getIsFollowing();

                        if (user.getFollowPrivacyMode() == 1) {
                            mFollowPrivate = true;
                            mFollowers.setTextColor(getResources().getColor(R.color.primary_content_color));
                            mFollowersText.setTextColor(getResources().getColor(R.color.primary_content_color));
                            mFollowing.setTextColor(getResources().getColor(R.color.primary_content_color));
                            mFollowingText.setTextColor(getResources().getColor(R.color.primary_content_color));
                        } else mFollowPrivate = false;

                        isGetInfo = true;
                        if (user.getPrivacyMode().compareTo("private") == 0) isPrivate = true;
                        if (user.getFollowRequestTime() != 0) isRequest = true;

                        if (isfollowing == 0) {
                            if (isRequest) {
                                mFollow.setTextColor(Color.BLACK);
                                mFollow.setBackgroundResource(R.drawable.btn_greenline_selector);
                                mFollow.setText(getString(R.string.private_mode_request));
                                mDown.setVisibility(View.GONE);
                            } else {
                                mFollow.setTextColor(Color.BLACK);
                                mFollow.setBackgroundResource(R.drawable.btn_greenline_selector);
                                mFollow.setText(getString(R.string.user_profile_follow));
                                mDown.setVisibility(View.GONE);
                            }
                        } else {
                            mFollow.setTextColor(Color.WHITE);
                            mFollow.setBackgroundResource(R.drawable.btn_green_selector);
                            mFollow.setText(getString(R.string.user_profile_unfollow));
                            if (mSuggestionModel.size() != 0) mDown.setVisibility(View.VISIBLE);
                            else mDown.setVisibility(View.GONE);
                        }

                        if (user.getIsVerified() == 1) mVerifie.setVisibility(View.VISIBLE);
                        else mVerifie.setVisibility(View.GONE);

                        mLikecount.setText(String.valueOf(user.getReceivedLikeCount()));

                        if (user.getBio().length() != 0) {
                            mDio.setVisibility(View.VISIBLE);
                            mDio.setText(user.getBio());
                        } else mDio.setVisibility(View.GONE);

                        reHeaderHeight();
                    }
                    else
                    {
                        try{
//                          showToast(getString(R.string.error_failed));
                            Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                        mBigNodata.setVisibility(View.VISIBLE);
                    }
                }
            });

            return;
        }

        runUser();
    }

    private void runUser()
    {
        if(mGuest) mPresent.setVisibility(View.GONE);
        mPresent.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setClass(mCtx, PresentBoardActivity.class);
                intent.putExtra("userId", targetUserID);
                if(mOpenID.length()!=0) intent.putExtra("openId", mOpenID);
                else intent.putExtra("openId", open);
                startActivity(intent);
            }
        });

        mScroll.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshBase refreshView) {
                ApiManager.getUserPost(mCtx, targetUserID, Integer.MAX_VALUE, 15, new ApiManager.GetUserPostCallback() {
                    @Override
                    public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                        mScroll.onRefreshComplete();

                        if (success && feedModel != null) {
                            if (feedModel.size() != 0) {
                                mNoData.setVisibility(View.GONE);
                                mPostPrivate.setVisibility(View.GONE);

                                MyisFetchingData = false;
                                MynoMoreData = false;
                                mFeedModels.clear();
                                mFeedModels.addAll(feedModel);

                                mGrid.setImageResource(R.drawable.profile_grid_active);
                                mList.setImageResource(R.drawable.profile_list);
                                mLike.setImageResource(R.drawable.profile_like);

                                if (mGridAdapter != null) mGridAdapter = null;
                                mGridAdapter = new GridAdapter();
                                mScroll.setAdapter(mGridAdapter);

                                mMyPagte = 0;

                                mScroll.getRefreshableView().setNumColumns(3);

                                if (feedModel.size() < 15) {
                                    MynoMoreData = true;
                                }
                            }
                        }
                        reHeaderHeight();
                    }
                });
            }
        });

        mScroll.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_FLING) {
                    ImageLoader.getInstance().pause();
                    mVideoPlayState = false;
                }

                if (scrollState == SCROLL_STATE_IDLE) {
                    ImageLoader.getInstance().resume();
                    mVideoPlayState = true;
                    if (mMyPagte == 1 || mMyPagte == 2) {
//                        if(mListAdapter!=null) mListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        ApiManager.isUserOnLiveStream(mCtx, targetUserID, new ApiManager.IsUserOnLiveStreamCallback()
        {
            @Override
            public void onResult(String liveStreamID, String userIsOnLive)
            {
                if(userIsOnLive.length() != 0)
                {
                    int isLive = Integer.valueOf(userIsOnLive);

                    if(isLive==1)
                    {
                        if(liveStreamID.length()!=0)
                        {
                            final int LiveID = Integer.valueOf(liveStreamID);

                            mNowLive.setText(open + getString(R.string.user_live_now));
                            mNowLive.setVisibility(View.VISIBLE);
                            mNowLive.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mLiveState = true;
                                    mApplication.isUserLive = true;

                                    Intent intent = new Intent();
                                    intent.setClass(mCtx, LiveStreamActivity.class);
                                    intent.putExtra("liveStreamID", LiveID);
                                    intent.putExtra("guest", mGuest);
                                    //event tracking: to track what page user enter livestream
                                    intent.putExtra("enterLiveFrom","HomeUser");

                                    startActivity(intent);
//                                    mCtx.finish();
                                }
                            });
                            reHeaderHeight();
                        }
                    }
                    else mApplication.isUserLive = false;
                }
            }
        });

//        if(live)
//        {
//            mNowLive.setText(open + getString(R.string.user_live_now));
//            mNowLive.setVisibility(View.VISIBLE);
//            mNowLive.setOnClickListener(new View.OnClickListener()
//            {
//                @Override
//                public void onClick(View v)
//                {
//                    mLiveState = true;
//                    mApplication.isUserLive = true;
//                    Intent intent = new Intent();
//                    intent.setClass(mCtx, LiveStreamActivity.class);
//                    intent.putExtra("liveStreamID", live_id);
//                    intent.putExtra("name", live_openid);
//                    intent.putExtra("caption", live_caption);
//                    intent.putExtra("picture", live_picture);
//                    intent.putExtra("user", Singleton.preferences.getString(Constants.USER_ID, ""));
//                    intent.putExtra("myopen", Singleton.preferences.getString(Constants.OPEN_ID, ""));
//                    startActivity(intent);
//                }
//            });
//        }
//        else mApplication.isUserLive = false;

        ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + picture), mImg, imgOptions);
        mFollowers.setText(String.valueOf(follow));
        mFollowing.setText(String.valueOf(following));
        mLikecount.setText(String.valueOf(likecount));

        if(isfollowing==0)
        {
            mFollow.setText(getString(R.string.user_profile_follow));
            mFollow.setTextColor(Color.BLACK);
            mFollow.setBackgroundResource(R.drawable.btn_greenline_selector);
            mDown.setVisibility(View.GONE);
        }
        else
        {
            mFollow.setTextColor(Color.WHITE);
            mFollow.setBackgroundResource(R.drawable.btn_green_selector);
            mFollow.setText(getString(R.string.user_profile_unfollow));
            if(mSuggestionModel.size()!=0) mDown.setVisibility(View.VISIBLE);
            else mDown.setVisibility(View.GONE);
        }

        mOpen.setText(title);
        if(bio.length()!=0)
        {
            mDio.setVisibility(View.VISIBLE);
            mDio.setText(bio);
        }
        else mDio.setVisibility(View.GONE);

        if(web!=null && web.length()!=0)
        {
            mWeb.setText(web);
            mWeb.setVisibility(View.VISIBLE);
        }
        else mWeb.setVisibility(View.GONE);

        mFollowers.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mFollowPrivate) return;
                Intent intent = new Intent();
                intent.setClass(mCtx,ProfileFollowActivity.class);
                intent.putExtra("follower",true);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",targetUserID);
                startActivity(intent);
            }
        });

        mFollowersText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mFollowPrivate) return;
                Intent intent = new Intent();
                intent.setClass(mCtx,ProfileFollowActivity.class);
                intent.putExtra("follower",true);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",targetUserID);
                startActivity(intent);
            }
        });

        mFollowing.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mFollowPrivate) return;
                Intent intent = new Intent();
                intent.setClass(mCtx,ProfileFollowActivity.class);
                intent.putExtra("follower",false);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",targetUserID);
                startActivity(intent);
            }
        });

        mFollowingText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mFollowPrivate) return;
                Intent intent = new Intent();
                intent.setClass(mCtx,ProfileFollowActivity.class);
                intent.putExtra("follower",false);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",targetUserID);
                startActivity(intent);
            }
        });

        mDown.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mSuggestion.isShown())
                {
                    mSuggestion.setVisibility(View.GONE);
                    if(mFeedModels.size()==0) {
                        if(isPrivate)
                        {
                            mPostPrivate.setVisibility(View.VISIBLE);
                            mNoData.setVisibility(View.GONE);
                        }
                        else
                        {
                            mNoData.setVisibility(View.VISIBLE);
                            mPostPrivate.setVisibility(View.GONE);
                        }
                    }

                    reHeaderHeight();
                }
                else
                {
                    mSuggestion.setVisibility(View.VISIBLE);
                    mNoData.setVisibility(View.GONE);
                    mPostPrivate.setVisibility(View.GONE);

                    if(mSuggestion.getChildCount()==0)
                    {
                        for(int i = 0 ; i < mSuggestionModel.size() ; i++)
                        {
                            View view = inflater.inflate(R.layout.user_suggeste_row, null);

                            ImageView img = (ImageView) view.findViewById(R.id.pic);
                            TextView name = (TextView) view.findViewById(R.id.name);
                            TextView dio = (TextView) view.findViewById(R.id.dio);
                            Button follow = (Button) view.findViewById(R.id.follow);

                            final int pos = i;
                            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mSuggestionModel.get(pos).getPicture()), img, SelfOptions);
                            img.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    if (mSuggestionModel.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                                    {
                                        Intent intent = new Intent();
                                        intent.setClass(mCtx, HomeUserActivity.class);
                                        intent.putExtra("title", mSuggestionModel.get(pos).getName());
                                        intent.putExtra("picture", mSuggestionModel.get(pos).getPicture());
                                        intent.putExtra("isfollowing", mSuggestionModel.get(pos).getIsFollowing());
                                        intent.putExtra("post", mSuggestionModel.get(pos).getPostCount());
                                        intent.putExtra("follow", mSuggestionModel.get(pos).getFollowerCount());
                                        intent.putExtra("following", mSuggestionModel.get(pos).getFollowingCount());
                                        intent.putExtra("open", mSuggestionModel.get(pos).getOpenID());
                                        intent.putExtra("bio", mSuggestionModel.get(pos).getBio());
                                        intent.putExtra("targetUserID", mSuggestionModel.get(pos).getUserID());
                                        intent.putExtra("web", mSuggestionModel.get(pos).getWebsite());
                                        startActivity(intent);
                                    }
                                }
                            });

                            name.setText(mSuggestionModel.get(pos).getOpenID());
                            name.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    if(mSuggestionModel.get(pos).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                                    {
                                        Intent intent = new Intent();
                                        intent.setClass(mCtx, HomeUserActivity.class);
                                        intent.putExtra("title", mSuggestionModel.get(pos).getName());
                                        intent.putExtra("picture", mSuggestionModel.get(pos).getPicture());
                                        intent.putExtra("isfollowing", mSuggestionModel.get(pos).getIsFollowing());
                                        intent.putExtra("post", mSuggestionModel.get(pos).getPostCount());
                                        intent.putExtra("follow", mSuggestionModel.get(pos).getFollowerCount());
                                        intent.putExtra("following", mSuggestionModel.get(pos).getFollowingCount());
                                        intent.putExtra("open", mSuggestionModel.get(pos).getOpenID());
                                        intent.putExtra("bio", mSuggestionModel.get(pos).getBio());
                                        intent.putExtra("targetUserID", mSuggestionModel.get(pos).getUserID());
                                        intent.putExtra("web", mSuggestionModel.get(pos).getWebsite());
                                        startActivity(intent);
                                    }
                                }
                            });

                            dio.setText(mSuggestionModel.get(pos).getName());

                            if(mSuggestionModel.get(pos).getIsFollowing()==1)
                            {
                                follow.setText(getString(R.string.user_profile_following));
                                follow.setBackgroundResource(R.drawable.btn_green_selector);
                                follow.setTextColor(Color.WHITE);
                            }
                            else
                            {
                                if(mSuggestionModel.get(pos).getFollowRequestTime()!=0)
                                {
                                    follow.setText(getString(R.string.private_mode_request_send));
                                    follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    follow.setTextColor(getResources().getColor(R.color.content_text_color));
                                }
                                else
                                {
                                    follow.setText("+ " + getString(R.string.user_profile_follow));
                                    follow.setBackgroundResource(R.drawable.btn_grayline_selector);
                                    follow.setTextColor(getResources().getColor(R.color.content_text_color));
                                }
                            }

                            final Button btn = follow;
                            follow.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    if(mSuggestionModel.get(pos).getIsFollowing()==1)
                                    {
                                        try {
                                            LogEventUtil.UnfollowUser(mCtx, mApplication, mSuggestionModel.get(pos).getUserID());
                                        }
                                        catch (Exception x)
                                        {

                                        }

                                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mSuggestionModel.get(pos).getUserID(), new ApiManager.UnfollowUserActionCallback() {
                                            @Override
                                            public void onResult(boolean success, String message)
                                            {
                                                if (success)
                                                {
                                                    mSuggestionModel.get(pos).setIsFollowing(0);
                                                    btn.setText("+ " + getString(R.string.user_profile_follow));
                                                    btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                                    btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                                }
                                                else {
                                                    try{
//                          showToast(getString(R.string.failed));
                                                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                    }
                                                    catch (Exception x){
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        if(mSuggestionModel.get(pos).getFollowRequestTime()!=0)
                                        {
                                            btn.setText("+ " + getString(R.string.user_profile_follow));
                                            btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                            btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                            mSuggestionModel.get(pos).setIsFollowing(0);
                                            mSuggestionModel.get(pos).setFollowRequestTime(0);
                                            ApiManager.cancelFollowRequests(mCtx, mSuggestionModel.get(pos).getUserID(), new ApiManager.RequestCallback() {
                                                @Override
                                                public void onResult(boolean success) {
                                                    if (success) {

                                                    }
                                                }
                                            });
                                        }
                                        else
                                        {
                                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                                            {
                                                try{
//                          showToast(getString(R.string.follow_count_size));
                                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                                return ;
                                            }

                                           if(mSuggestionModel.get(pos).getPrivacyMode().compareTo("private")==0)
                                           {
                                               btn.setText(getString(R.string.private_mode_request_send));
                                               btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                               btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                               mSuggestionModel.get(pos).setIsFollowing(0);
                                               mSuggestionModel.get(pos).setFollowRequestTime(Singleton.getCurrentTimestamp());
                                               ApiManager.sendFollowRequest(mCtx, mSuggestionModel.get(pos).getUserID(), new ApiManager.RequestCallback()
                                               {
                                                   @Override
                                                   public void onResult(boolean success)
                                                   {
                                                       if (!success)
                                                       {
                                                           btn.setText("+ " + getString(R.string.user_profile_follow));
                                                           btn.setBackgroundResource(R.drawable.btn_grayline_selector);
                                                           btn.setTextColor(getResources().getColor(R.color.content_text_color));
                                                           mSuggestionModel.get(pos).setIsFollowing(0);
                                                           mSuggestionModel.get(pos).setFollowRequestTime(0);
                                                       }
                                                   }
                                               });
                                           }
                                           else
                                           {
                                               try{
                                               LogEventUtil.FollowUser(mCtx, mApplication, mSuggestionModel.get(pos).getUserID());
                                               }
                                               catch (Exception x)
                                               {

                                               }

                                               ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), mSuggestionModel.get(pos).getUserID(), new ApiManager.FollowUserActionCallback()
                                               {
                                                   @Override
                                                   public void onResult(boolean success, String message)
                                                   {
                                                       if (success)
                                                       {
                                                           mSuggestionModel.get(pos).setIsFollowing(1);
                                                           btn.setText(getString(R.string.user_profile_following));
                                                           btn.setBackgroundResource(R.drawable.btn_green_selector);
                                                           btn.setTextColor(Color.WHITE);
                                                       }
                                                       else {
                                                           try{
//                          showToast(getString(R.string.failed));
                                                               Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                           }
                                                           catch (Exception x){
                                                           }
                                                       }
                                                   }
                                               });
                                           }
                                        }
                                    }
                                }
                            });

                            mSuggestion.addView(view);
                        }
                    }

                    reHeaderHeight();
                }
            }
        });

        mFollow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(isGetInfo)
                {
                    if (isfollowing == 1)
                    {
                        mFollow.setText(getString(R.string.user_profile_follow));
                        mFollow.setTextColor(Color.BLACK);
                        mFollow.setBackgroundResource(R.drawable.btn_grayline_selector);
                        isfollowing = 0;
                        try {
                            LogEventUtil.UnfollowUser(mCtx, mApplication, targetUserID);
                        }
                        catch (Exception x)
                        {

                        }

                        ApiManager.unfollowUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), targetUserID, new ApiManager.UnfollowUserActionCallback() {
                            @Override
                            public void onResult(boolean success, String message) {
                                if (success) {

                                }
                            }
                        });
                        mDown.setVisibility(View.GONE);
                    }
                    else
                    {
                        if(isRequest)
                        {
                            mFollow.setText(getString(R.string.user_profile_follow));
                            mFollow.setTextColor(Color.BLACK);
                            mFollow.setBackgroundResource(R.drawable.btn_greenline_selector);
                            isfollowing = 0;
                            isRequest = false;
                            ApiManager.cancelFollowRequests(mCtx, targetUserID, new ApiManager.RequestCallback() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {

                                    }
                                }
                            });
                            mDown.setVisibility(View.GONE);
                        }
                        else
                        {
                            if(Singleton.preferences.getInt(Constants.FOLLOWING_COUNT_V2, 0) > 5000)
                            {
                                try{
//                          showToast(getString(R.string.follow_count_size));
                                    Toast.makeText(mCtx, getString(R.string.follow_count_size), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                                return ;
                            }

                            if(isPrivate)
                            {
                                mFollow.setTextColor(Color.BLACK);
                                mFollow.setBackgroundResource(R.drawable.btn_greenline_selector);
                                mFollow.setText(getString(R.string.private_mode_request));
                                isfollowing = 0;
                                isRequest = true;
                                ApiManager.sendFollowRequest(mCtx, targetUserID, new ApiManager.RequestCallback()
                                {
                                    @Override
                                    public void onResult(boolean success)
                                    {
                                        if (!success)
                                        {
                                            mFollow.setText(getString(R.string.user_profile_follow));
                                            mFollow.setTextColor(Color.BLACK);
                                            mFollow.setBackgroundResource(R.drawable.btn_greenline_selector);
                                            isfollowing = 0;
                                            isRequest = false;
                                            mDown.setVisibility(View.GONE);
                                        }
                                    }
                                });
                                mDown.setVisibility(View.GONE);
                            }
                            else
                            {
                                mFollow.setText(getString(R.string.user_profile_unfollow));
                                isfollowing = 1;
                                mFollow.setTextColor(Color.WHITE);
                                mFollow.setBackgroundResource(R.drawable.btn_green_selector);

                                try{
                                LogEventUtil.FollowUser(mCtx, mApplication, targetUserID);
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.followUserAction(mCtx, Singleton.preferences.getString(Constants.USER_ID, ""), targetUserID, new ApiManager.FollowUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {
                                        if (success) {

                                        }
                                    }
                                });
                                if(mSuggestionModel.size()!=0) mDown.setVisibility(View.VISIBLE);
                                else mDown.setVisibility(View.GONE);
                            }
                        }
                    }
                }
            }
        });

        mGrid.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tabSwitchCount++;
                mNoData.setVisibility(View.GONE);
                mPostPrivate.setVisibility(View.GONE);
                mGrid.setImageResource(R.drawable.profile_grid_active);
                mList.setImageResource(R.drawable.profile_list);
                mLike.setImageResource(R.drawable.profile_like);

                if(mGridAdapter!=null) mGridAdapter = null;
                mGridAdapter = new GridAdapter();
                mScroll.setAdapter(mGridAdapter);

                mScroll.getRefreshableView().setNumColumns(3);

                if(mFeedModels.size()==0)
                {
                    if(isPrivate)
                    {
                        mPostPrivate.setVisibility(View.VISIBLE);
                        mNoData.setVisibility(View.GONE);
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                        mPostPrivate.setVisibility(View.GONE);
                    }
                }

                if(mBear!=null) mBear.removeAllViews();

                mMyPagte = 0;
                reHeaderHeight();
            }
        });

        mList.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tabSwitchCount++;
                mNoData.setVisibility(View.GONE);
                mPostPrivate.setVisibility(View.GONE);
                mGrid.setImageResource(R.drawable.profile_grid);
                mList.setImageResource(R.drawable.profile_list_active);
                mLike.setImageResource(R.drawable.profile_like);
                mScroll.setPullToRefreshEnabled(true);

                if(mBear!=null) mBear.removeAllViews();

                if(mFeedModels.size()==0)
                {
                    if(isPrivate)
                    {
                        mPostPrivate.setVisibility(View.VISIBLE);
                        mNoData.setVisibility(View.GONE);
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                        mPostPrivate.setVisibility(View.GONE);
                    }
                }

                if(mListAdapter!=null) mListAdapter = null;
                mListAdapter = new MyListAdapter(mFeedModels,true);
                mScroll.setAdapter(mListAdapter);

                mScroll.getRefreshableView().setNumColumns(1);

                mMyPagte = 1;
                reHeaderHeight();
            }
        });

        mLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabSwitchCount++;
                mNoData.setVisibility(View.GONE);
                mPostPrivate.setVisibility(View.GONE);
                mGrid.setImageResource(R.drawable.profile_grid);
                mList.setImageResource(R.drawable.profile_list);
                mLike.setImageResource(R.drawable.profile_like_active);
                mScroll.setPullToRefreshEnabled(true);

                if (mBear != null) mBear.removeAllViews();

                mMyPagte = 2;

                if (mLikMoedels.size() == 0) {
                    mProgress.setVisibility(View.VISIBLE);
                    ApiManager.getLikedPost(mCtx, targetUserID, Integer.MAX_VALUE, 15, new ApiManager.GetLikedPostsCallback() {
                        @Override
                        public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                            mProgress.setVisibility(View.GONE);
                            if (success && feedModel!=null) {
                                mLikMoedels.clear();
                                mLikMoedels.addAll(feedModel);

                                if (mLikMoedels.size() == 0) {
                                    if(isPrivate)
                                    {
                                        mPostPrivate.setVisibility(View.VISIBLE);
                                        mNoData.setVisibility(View.GONE);
                                    }
                                    else
                                    {
                                        mNoData.setVisibility(View.VISIBLE);
                                        mPostPrivate.setVisibility(View.GONE);
                                    }
                                }

                                if (mLikeListAdapter != null) mLikeListAdapter = null;
                                mLikeListAdapter = new LikeListAdapter(mLikMoedels, false);
                                mScroll.setAdapter(mLikeListAdapter);

                                mScroll.getRefreshableView().setNumColumns(1);

                                LikeisFetchingData = false;
                                LikenoMoreData = false;

                                if (feedModel.size() < 15) {
                                    LikenoMoreData = true;
                                }
                            } else{
                                try{
//                          showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception x){
                                }
                            }
                            reHeaderHeight();
                        }
                    });
                } else {
                    if (mLikeListAdapter != null) mLikeListAdapter = null;
                    mLikeListAdapter = new LikeListAdapter(mLikMoedels, false);
                    mScroll.setAdapter(mLikeListAdapter);

                    mScroll.getRefreshableView().setNumColumns(1);

                    if (mLikMoedels.size() < 15) {
                        LikenoMoreData = true;
                    }
                }
                reHeaderHeight();
            }
        });

        if(mOpenID.length()==0)
        {
        ApiManager.getUserInfo(mCtx, open, new ApiManager.GetUserInfoCallback() {
            @Override
            public void onResult(boolean success, String message, UserModel user) {
                if (success && user != null) {
                    userModel = user;
                    isfollowing = user.getIsFollowing();

                    if(user.getFollowPrivacyMode()==1)
                    {
                        mFollowPrivate = true;
                        mFollowers.setTextColor(getResources().getColor(R.color.primary_content_color));
                        mFollowersText.setTextColor(getResources().getColor(R.color.primary_content_color));
                        mFollowing.setTextColor(getResources().getColor(R.color.primary_content_color));
                        mFollowingText.setTextColor(getResources().getColor(R.color.primary_content_color));
                    }
                    else mFollowPrivate = false;

                    isGetInfo = true;
                    if(user.getPrivacyMode().compareTo("private")==0) isPrivate = true;
                    if(user.getFollowRequestTime()!=0) isRequest = true;

                    if (isfollowing == 0)
                    {
                        if(isRequest)
                        {
                            mFollow.setTextColor(Color.BLACK);
                            mFollow.setBackgroundResource(R.drawable.btn_greenline_selector);
                            mFollow.setText(getString(R.string.private_mode_request));
                            mDown.setVisibility(View.GONE);
                        }
                        else
                        {
                            mFollow.setTextColor(Color.BLACK);
                            mFollow.setBackgroundResource(R.drawable.btn_greenline_selector);
                            mFollow.setText(getString(R.string.user_profile_follow));
                            mDown.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        mFollow.setTextColor(Color.WHITE);
                        mFollow.setBackgroundResource(R.drawable.btn_green_selector);
                        mFollow.setText(getString(R.string.user_profile_unfollow));
                        if(mSuggestionModel.size()!=0) mDown.setVisibility(View.VISIBLE);
                        else mDown.setVisibility(View.GONE);
                    }

                    if (user.getIsVerified() == 1) mVerifie.setVisibility(View.VISIBLE);
                    else mVerifie.setVisibility(View.GONE);

                    mLikecount.setText(String.valueOf(user.getReceivedLikeCount()));

                    if(user.getBio().length()!=0)
                    {
                        mDio.setVisibility(View.VISIBLE);
                        mDio.setText(user.getBio());
                    }
                    else mDio.setVisibility(View.GONE);

                    reHeaderHeight();
                }
            }
        });
        }

        mBigProgress.setVisibility(View.VISIBLE);
        mProgress.setVisibility(View.VISIBLE);
        ApiManager.getUserPost(mCtx, targetUserID, Integer.MAX_VALUE, 15, new ApiManager.GetUserPostCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                mProgress.setVisibility(View.GONE);
                mBigProgress.setVisibility(View.GONE);
                if (success && feedModel != null) {
                    if (feedModel.size() != 0) {
                        mFeedModels.clear();
                        mFeedModels.addAll(feedModel);

                        mGridAdapter = new GridAdapter();
                        mScroll.setAdapter(mGridAdapter);

                        if (feedModel.size() < 15) {
                            MynoMoreData = true;
                        }
                    } else {
                        if(isPrivate)
                        {
                            mPostPrivate.setVisibility(View.VISIBLE);
                            mNoData.setVisibility(View.GONE);
                        }
                        else
                        {
                            mNoData.setVisibility(View.VISIBLE);
                            mPostPrivate.setVisibility(View.GONE);
                        }
                    }
                } else {
                    try {
                        if (mCtx != null) {
                            mNoData.setVisibility(View.VISIBLE);
                            mPostPrivate.setVisibility(View.GONE);
                            try{
//                          showToast(getString(R.string.failed));
                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception x){
                            }
                        }
                    } catch (Exception e) {

                    }
                }
                reHeaderHeight();
            }
        });

        reHeaderHeight();

        mFollowerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mFollowPrivate) return;
                Intent intent = new Intent();
                intent.setClass(mCtx, ProfileFollowActivity.class);
                intent.putExtra("follower", true);
                intent.putExtra("userid", Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target", targetUserID);
                startActivity(intent);
            }
        });

        mFollowingLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(mFollowPrivate) return;
                Intent intent = new Intent();
                intent.setClass(mCtx,ProfileFollowActivity.class);
                intent.putExtra("follower",false);
                intent.putExtra("userid",Singleton.preferences.getString(Constants.USER_ID, ""));
                intent.putExtra("target",targetUserID);
                startActivity(intent);
            }
        });

        mLikeLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                try
                {
                    if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, PostLikerActivity.class);
                        intent.putExtra("target_id", targetUserID);
                        intent.putExtra("user_id", Singleton.preferences.getString(Constants.USER_ID, ""));
                        startActivity(intent);
                    }
                }
                catch (Exception e)
                {

                }
            }
        });

        ApiManager.getFriendSuggestion(mCtx, targetUserID, 0, 3, new ApiManager.GetFriendSuggestionCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<UserModel> models)
            {
                if (success && models != null)
                {
                    if (models.size() != 0)
                    {
                        mSuggestionModel.clear();
                        mSuggestionModel.addAll(models);
                        if(mFollow.getText().toString().compareTo(getString(R.string.user_profile_unfollow))==0) mDown.setVisibility(View.VISIBLE);
                        else mDown.setVisibility(View.GONE);
                    }
                }
            }
        });

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mLikeCount - lastCount > 17) {
                    mGodHand = true;
                }

                lastCount = mLikeCount;
            }
        }, 0, 1000);
    }

    private void reHeaderHeight()
    {
        mLoadLayout.setVisibility(View.VISIBLE);
        if(mProgress.isShown() || mNoData.isShown() || mPostPrivate.isShown()) mLoadLayout.setVisibility(View.VISIBLE);
        else mLoadLayout.setVisibility(View.GONE);

        mSetHeaderHeight = true;
        mHeader.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (mSetHeaderHeight) {
                    mScroll.getRefreshableView().setHeaderViewHeight(mHeader.getHeight());
                    mSetHeaderHeight = false;
                }
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            hideKeyboard();

            if(mGuest)
            {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, GuestActivity.class);
//                startActivity(intent);
                mCtx.finish();
            }
            else mCtx.finish();

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

    private void initTitleBar(String open)
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(open);
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGuest)
                {
//                    Intent intent = new Intent();
//                    intent.setClass(mCtx, GuestActivity.class);
//                    startActivity(intent);
                    mCtx.finish();
                }
                else mCtx.finish();
            }
        });

        ImageView imgMore = (ImageView) findViewById(R.id.img_right);
        imgMore.setImageResource(R.drawable.more);
        imgMore.setVisibility(View.VISIBLE);
        imgMore.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mGuest)
                {
                    showGuestLogin();

                    return;
                }

                if(userModel==null) return;

                String[] mListMoreText;

                if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1)
                {
                    mListMoreText = new String[5];
                    mListMoreText[3] = getString(R.string.user_ban);
                    mListMoreText[4] = getString(R.string.user_freeze);
                }
                else mListMoreText = new String[3];

                if(userModel.getIsBlocked()==1){
                    mListMoreText[0] = getResources().getString(R.string.unblock_user);
                }else{
                    mListMoreText[0] = getResources().getString(R.string.block_user);
                }

                mListMoreText[1] = getResources().getString(R.string.report_user);
                mListMoreText[2] = getResources().getString(R.string.user_share);

                new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (which == 0) {

                            // Block
                            if (userModel.getIsBlocked() == 0) {

                                Singleton.log("blockUserAction");

                                //event tracking
                                try {
                                    LogEventUtil.BlockUser(mCtx, mApplication, targetUserID);
                                }
                                catch (Exception x)
                                {

                                }

                                ApiManager.blockUserAction(mCtx, targetUserID, new ApiManager.BlockUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {

                                        if (success) {
                                            userModel.setIsBlocked(1);
                                            try{
//                          showToast(getString(R.string.complete));
                                                Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        } else {
                                            try{
//                          showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });
                            } else {
                                Singleton.log("unblockUserAction");

                                //event tracking
                                try {
                                    LogEventUtil.UnblockUser(mCtx, mApplication, targetUserID);
                                }
                                catch (Exception x)
                                {

                                }


                                ApiManager.unblockUserAction(mCtx, targetUserID, new ApiManager.BlockUserActionCallback() {
                                    @Override
                                    public void onResult(boolean success, String message) {


                                        if (success) {
                                            userModel.setIsBlocked(0);
                                            try{
//                          showToast(getString(R.string.complete));
                                                Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        } else {
                                            try{
//                          showToast(getString(R.string.failed));
                                                Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                            }
                                            catch (Exception x){
                                            }
                                        }
                                    }
                                });
//                                ApiManager.unfollowUserAction(mCtx, targetUserID, new ApiManager.UnfollowUserActionCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//
//                                    }
//                                });
                            }

                        } else if (which == 1) {

                            // Report
                            ArrayList<String> strArray = new ArrayList<String>();
                            strArray = Singleton.getLocalizeStringOfArray(Constants.REPORT_USER_OPTION);
                            String[] reportString = strArray.toArray(new String[strArray.size()]);

                            new AlertDialog.Builder(mCtx).setTitle(getString(R.string.report_user)).setItems(reportString, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    try {
                                        LogEventUtil.ReportUser(mCtx, mApplication);
                                    }
                                    catch (Exception x)
                                    {

                                    }

                                    String reason = Singleton.getLocalizedString(Constants.REPORT_USER_OPTION.get(which));
                                    ApiManager.reportUserAction(mCtx, targetUserID, reason, "",new ApiManager.ReportUserActionCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {
                                                try{
//                          showToast(getString(R.string.complete));
                                                    Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                            } else {
                                                try{
//                          showToast(getString(R.string.failed));
                                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                            }
                                        }
                                    });

                                }
                            }).show();

                        } else if (which == 2) {
                            mBigProgress.setVisibility(View.VISIBLE);
                            Branch.getInstance(mCtx).getContentUrl("facebook", BranchShareUserData(userModel.getOpenID(),userModel.getBio(),userModel.getPicture()), new Branch.BranchLinkCreateListener() {
                                @Override
                                public void onLinkCreate(String url, BranchError error) {
                                    mBigProgress.setVisibility(View.GONE);
                                    String mTitle = String.format(getString(R.string.share_post_user), userModel.getOpenID());
                                    String mDescription = mTitle + "!" + userModel.getBio() + "  " + url;

                                    DownloadTask mDownloadTask = new DownloadTask();
                                    mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), userModel.getPicture());
                                }
                            });
//                            // Share_link
//                            String uriString = getResources().getString(R.string.copy_link)+userModel.getOpenID();
//                            int sdk = android.os.Build.VERSION.SDK_INT;
//
//                            if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
//                                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) mCtx.getSystemService(Context.CLIPBOARD_SERVICE);
//                                clipboard.setText(uriString);
//                            } else {
//                                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) mCtx.getSystemService(Context.CLIPBOARD_SERVICE);
//                                android.content.ClipData clip = android.content.ClipData.newPlainText("text", uriString);
//                                clipboard.setPrimaryClip(clip);
//                            }

                        }
                        else if(which == 3) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                            builder.setMessage(getString(R.string.user_ban));
                            builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ApiManager.banUserAction(mCtx, userModel.getUserID(), "", new ApiManager.RequestCallback() {
                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {
                                                try{
//                          showToast(getString(R.string.done));
                                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                            } else {
                                                try{
//                          showToast(getString(R.string.error_failed));
                                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            builder.setNeutralButton(R.string.cancel, null);
                            builder.create().show();
                        }
                        else if(which == 4) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                            builder.setMessage(getString(R.string.user_freeze));
                            builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ApiManager.freezeUserAction(mCtx, userModel.getUserID(), "", new ApiManager.RequestCallback() {
                                        @Override
                                        public void onResult(boolean success) {
                                            if (success) {
                                                try{
//                          showToast(getString(R.string.done));
                                                    Toast.makeText(mCtx, getString(R.string.done), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                            } else {
                                                try{
//                          showToast(getString(R.string.error_failed));
                                                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                                                }
                                                catch (Exception x){
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            builder.setNeutralButton(R.string.cancel, null);
                            builder.create().show();
                        }

                    }
                }).show();
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mLiveState)
        {
            if(mApplication!=null)
            {
                if(mApplication.isUserLive) mNowLive.setVisibility(View.VISIBLE);
                else mNowLive.setVisibility(View.GONE);
            }

            mLiveState = false;
        }

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());

        if(mApplication!=null){
            if(mApplication.getGuestFinish()) mCtx.finish();
            else if(mApplication.getMenuFinish()) mCtx.finish();
        }

        //event tracking
        try{
            if (isfollowing == 1) {
                LogEventUtil.LeaveProfilePage(mCtx, mApplication, mOpenID, true, mFeedModels.size(), mLikeCount, tabSwitchCount);
            }
            else
            {
                LogEventUtil.LeaveProfilePage(mCtx, mApplication, mOpenID, false, mFeedModels.size(), mLikeCount, tabSwitchCount);
            }
        }
        catch (Exception x)
        {

        }

    }

    @Override
    public void onPause()
    {
        super.onPause();
        if(mBear!=null) mBear.removeAllViews();

        if(mApplication!=null) mApplication.setIsFollow(isfollowing);

        if(mMyVideoLists!=null && mMyVideoLists.size()!=0)
        {
            for(int i = 0 ; i < mMyVideoLists.size() ; i++)
            {
                if(mMyVideoLists.get(i).isPlaying())
                {
                    mMyVideoLists.get(i).stopPlayback();
                }
            }
        }

        if(mLikeVideoLists!=null && mLikeVideoLists.size()!=0)
        {
            for(int i = 0 ; i < mLikeVideoLists.size() ; i++)
            {
                if(mLikeVideoLists.get(i).isPlaying())
                {
                    mLikeVideoLists.get(i).stopPlayback();
                }
            }
        }

        if(mLikeCount!=0)
        {
            if(mApplication!=null)
            {
                if(mLikePosUser.compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                {
                    mApplication.sendLikeCountV2(mCtx,mLikePos,mLikeCount,mGodHand);

                    mLikePos = "";
                    mLikeCount = 0;
                    mLikePosUser = "";
                }
            }
        }

        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    private class GridAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.grid_row, null);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.video = (ImageView) convertView.findViewById(R.id.video);
                convertView.setTag(holder);
            }
            else holder = (ViewHolder) convertView.getTag();

            holder.image.getLayoutParams().width = mGridHeight;
            holder.image.getLayoutParams().height = mGridHeight;

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getPicture()), holder.image,GorpOptions);
            holder.image.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, PhotoActivity.class);
                    intent.putExtra("photo", mFeedModels.get(position).getPicture());
                    intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                    intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                    intent.putExtra("day", Singleton.getElapsedTimeString(mFeedModels.get(position).getTimestamp()));
                    intent.putExtra("view", mFeedModels.get(position).getViewCount());
                    intent.putExtra("money", mFeedModels.get(position).getTotalRevenue());
                    intent.putExtra("dio", mFeedModels.get(position).getCaption());
                    intent.putExtra("likecount", mFeedModels.get(position).getLikeCount());
                    intent.putExtra("commentcount", mFeedModels.get(position).getCommentCount());
                    intent.putExtra("likeed", mFeedModels.get(position).getLiked());

                    intent.putExtra("postid", mFeedModels.get(position).getPostID());
                    intent.putExtra("userid", mFeedModels.get(position).getUserID());

                    intent.putExtra("name", mFeedModels.get(position).getUserInfo().getName());
                    intent.putExtra("isFollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                    intent.putExtra("postCount", mFeedModels.get(position).getUserInfo().getPostCount());
                    intent.putExtra("followerCount", mFeedModels.get(position).getUserInfo().getFollowerCount());
                    intent.putExtra("followingCount", mFeedModels.get(position).getUserInfo().getFollowingCount());
                    intent.putExtra("goto", false);
                    intent.putExtra("type", mFeedModels.get(position).getType());
                    intent.putExtra("video", mFeedModels.get(position).getVideo());
                    intent.putExtra("guest", mGuest);
                    startActivity(intent);
//                    mCtx.finish();
                }
            });

            if(mFeedModels.get(position).getType().compareTo("image")==0) holder.video.setVisibility(View.GONE);
            else holder.video.setVisibility(View.VISIBLE);

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView image;
        ImageView video;
    }

    private class MyListAdapter extends BaseAdapter
    {
        private ArrayList<FeedModel> mFeedAdapterArray = new ArrayList<FeedModel>();
        private Boolean mSelf;

        public MyListAdapter(ArrayList<FeedModel> models,Boolean self)
        {
            mFeedAdapterArray.clear();
            mFeedAdapterArray.addAll(models);

            mSelf = self;
        }

        public void addModel(ArrayList<FeedModel> models)
        {
            mFeedAdapterArray.clear();
            mFeedAdapterArray.addAll(models);
        }

        @Override
        public int getCount()
        {
            return mFeedAdapterArray.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ListViewHolder holder = new ListViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ListViewHolder) convertView.getTag();
            }

            try{
                if(mFeedAdapterArray!=null && mFeedAdapterArray.get(position)!=null && mFeedAdapterArray.get(position).getUserInfo()!=null && holder.verifie!=null){
                    if(mFeedAdapterArray.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
                    else holder.verifie.setVisibility(View.GONE);
                }
            }
            catch (Exception e){
            }

            holder.down_layout.setVisibility(View.VISIBLE);
            holder.live.setVisibility(View.GONE);

            holder.name.setText(mFeedAdapterArray.get(position).getUserInfo().getOpenID());
            holder.day.setText(Singleton.getElapsedTimeString(mFeedAdapterArray.get(position).getTimestamp()));

            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.money_text.setVisibility(View.VISIBLE);

                String c = Singleton.getCurrencyType();
                double m = (double) mFeedAdapterArray.get(position).getTotalRevenue()*(double)Singleton.getCurrencyRate();
                DecimalFormat df=new DecimalFormat("#.####");
                holder.money_text.setText(df.format(m) + " " + c);

                holder.view_text.setText(String.format(getString(R.string.home_views), String.valueOf(mFeedAdapterArray.get(position).getViewCount())));
                holder.view_text.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.money_text.setVisibility(View.GONE);
                holder.view_text.setVisibility(View.GONE);
            }

            if(mFeedAdapterArray.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mFeedAdapterArray.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.GONE);
            holder.like_text.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
            holder.comment_text.setText(String.format(getString(R.string.home_comment), String.valueOf(mFeedAdapterArray.get(position).getCommentCount())));

            final String thisUserId = mFeedAdapterArray.get(position).getUserID();

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.like_text.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                        intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                        startActivity(intent);
                    }
                });
            }
            else
            {
                holder.like_text.setOnClickListener(null);
            }

            holder.comment_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    mCimmentPos = position;
//                    mCommentCount = true;

                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    Intent intent = new Intent();
                    intent.setClass(mCtx, HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                    intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(mFeedAdapterArray.get(position).getLiked()==0)
            {
                holder.btn_like.setImageResource(R.drawable.btn_like_selector);
            }
            else holder.btn_like.setImageResource(R.drawable.like_down);

            final TextView likeText = holder.like_text;
            final ImageView like = holder.btn_like;
            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                        intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                        startActivity(intent);
                    }
                });
                holder.btn_like.setOnTouchListener(null);
            }
            else
            {
                holder.btn_like.setOnClickListener(null);
                holder.btn_like.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            mY = event.getY();
                            mX = event.getX();
                        }

                        if (event.getAction() == MotionEvent.ACTION_UP) {

                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }

                            double num = Math.sqrt(Math.pow(((double) mX - (double) event.getX()), 2) + Math.pow(((double) mY - (double) event.getY()), 2));

                            if (num < 30) {
                                int run = (int) (Math.random() * 10000);
                                int img;
                                int mW, mH;

                                if (run < 500) {
                                    img = logos[(int) (Math.random() * logos.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 1000) {
                                    img = rabbits[(int) (Math.random() * rabbits.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 2000) {
                                    img = cats[(int) (Math.random() * cats.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 3000) {
                                    img = bears[(int) (Math.random() * bears.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 4000) {
                                    img = bubbles[(int) (Math.random() * bubbles.length)];
                                    mW = 120;
                                    mH = 120;
                                } else {
                                    img = loves[(int) (Math.random() * loves.length)];
                                    mW = 60;
                                    mH = 60;
                                }

                                int mS = -30 + ((int) (Math.random() * 60));
                                int mTx = -100 + ((int) (Math.random() * 200));
                                int mTy = 500 + ((int) (Math.random() * 1200));

                                ImageView cat = new ImageView(mCtx);
                                cat.setImageResource(img);
                                cat.setVisibility(View.INVISIBLE);

                                RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                cat.setLayoutParams(mParams);

                                AnimationSet mAnimationSet = new AnimationSet(true);

                                ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                scale.setDuration(500);

                                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                alpha.setDuration(3000);

                                RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                rotate.setDuration(750);

                                TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                translate.setDuration(2000);

                                mAnimationSet.addAnimation(scale);
                                mAnimationSet.addAnimation(rotate);
                                mAnimationSet.addAnimation(translate);
                                mAnimationSet.addAnimation(alpha);
                                cat.startAnimation(mAnimationSet);

                                mBear.addView(cat);

                                mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                                if (mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                    {
                                        mLikeCount++;
                                    }
                                    else
                                    {
                                        if(mLikeCount!=0)
                                        {
                                            mApplication.sendLikeCountV2(mCtx,mLikePos,mLikeCount,mGodHand);
                                        }

                                        mLikePos = mFeedAdapterArray.get(position).getPostID();
                                        mLikeCount = 1;
                                    }
//                                    ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                        @Override
//                                        public void onResult(boolean success, String message) {
//                                            if (success) {
//
//                                            }
//                                        }
//                                    });
                                } else {
                                    if (mFeedAdapterArray.get(position).getLiked() != 1) {
                                        mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                        likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                        mFeedAdapterArray.get(position).setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        if(!mApplication.getIsSbtools())
                                        {
                                            ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }

                        return true;
                    }
                });
            }

            holder.btn_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mCimmentPos = position;
//                    mCommentCount = true;

                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    Intent intent = new Intent();
                    intent.setClass(mCtx, HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                    intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_more.setVisibility(View.GONE);
            }
            else
            {
                holder.btn_more.setVisibility(View.VISIBLE);
                holder.btn_more.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(mGuest)
                        {
                            showGuestLogin();

                            return;
                        }

                        if (mListMoreText != null) mListMoreText = null;

                        if (mFeedAdapterArray.get(position).getLiked() == 0) {
                            mListMoreText = new String[2];
                            mListMoreText[0] = getString(R.string.post_block);
                            mListMoreText[1] = getString(R.string.post_share);
                        } else {
                            mListMoreText = new String[3];
                            mListMoreText[0] = getString(R.string.post_block);
                            mListMoreText[1] = getString(R.string.post_share);
                            mListMoreText[2] = getString(R.string.back_like);
                        }

                        new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    //檢舉
                                    ArrayList<String> strArray = new ArrayList<String>();
                                    strArray = Singleton.getLocalizeStringOfArray(Constants.REPORT_POST_OPTION);
                                    String[] reportString = strArray.toArray(new String[strArray.size()]);

                                    new AlertDialog.Builder(mCtx).setTitle(getString(R.string.report_reason)).setItems(reportString, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            String reason = Singleton.getLocalizedString(Constants.REPORT_POST_OPTION.get(which));
                                            try {
                                                LogEventUtil.ReportPost(mCtx, mApplication);
                                            }
                                            catch (Exception x)
                                            {

                                            }

                                            ApiManager.reportPostAction(mCtx, mFeedAdapterArray.get(position).getUserInfo().getUserID(), reason, mFeedAdapterArray.get(position).getPostID(), new ApiManager.ReportPostActionCallback() {

                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {
                                                        try{
//                          showToast(getString(R.string.complete));
                                                            Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                                        }
                                                        catch (Exception x){
                                                        }
                                                    } else {
                                                        try{
//                          showToast(getString(R.string.failed));
                                                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                        }
                                                        catch (Exception x){
                                                        }
                                                    }
                                                }
                                            });


                                        }
                                    }).show();
                                } else if (which == 1) {
                                    if (mFeedAdapterArray.get(position).getType().compareTo("image") == 0) {
                                        mBigProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mFeedAdapterArray.get(position).getPostID(), R.string.share_post_image, mFeedAdapterArray.get(position).getUserInfo().getOpenID(), mFeedAdapterArray.get(position).getCaption(), mFeedAdapterArray.get(position).getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mBigProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedAdapterArray.get(position).getCaption() + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedAdapterArray.get(position).getPicture());
                                            }
                                        });
                                    } else {
                                        mBigProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mFeedAdapterArray.get(position).getPostID(), R.string.share_post_video, mFeedAdapterArray.get(position).getUserInfo().getOpenID(), mFeedAdapterArray.get(position).getCaption(), mFeedAdapterArray.get(position).getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mBigProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedAdapterArray.get(position).getCaption() + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedAdapterArray.get(position).getPicture());
                                            }
                                        });
                                    }
                                } else if (which == 2) {
                                    if (mLikeCount != 0) {
                                        if (mApplication != null) {
                                            if (mFeedAdapterArray.get(position).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                                                mApplication.sendLikeCountV2(mCtx, mFeedAdapterArray.get(position).getPostID(), mLikeCount, mGodHand);

                                                mLikeCount = 0;
                                            }
                                        }
                                    }

                                    mFeedAdapterArray.get(position).setLiked(0);
                                    like.setImageResource(R.drawable.btn_like_selector);
                                    ApiManager.unlikePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.UnlikePostCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                            }
                        })
                        .show();
                    }
                });

            }

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedAdapterArray.get(position).getUserInfo().getPicture()), holder.self, SelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFeedAdapterArray.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedAdapterArray.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedAdapterArray.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedAdapterArray.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedAdapterArray.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedAdapterArray.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedAdapterArray.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedAdapterArray.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedAdapterArray.get(position).getUserInfo().getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFeedAdapterArray.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedAdapterArray.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedAdapterArray.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedAdapterArray.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedAdapterArray.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedAdapterArray.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedAdapterArray.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedAdapterArray.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedAdapterArray.get(position).getUserInfo().getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            if(mSelf)
            {
                if(MyNowPos >= (position+2) || MyNowPos <= (position-2))
                {
                    for(int p = 0 ; p < mMyVideoLists.size() ; p++)
                    {
                        if(mMyVideoLists.get(p)!=null)
                        {
                            if(mMyVideoLists.get(p).isPlaying())
                            {
                                mMyVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mMyVideoLists.clear();
                }
            }
            else
            {
                if(LikeNowPos >= (position+2) || LikeNowPos <= (position-2))
                {
                    for(int p = 0 ; p < mLikeVideoLists.size() ; p++)
                    {
                        if(mLikeVideoLists.get(p)!=null)
                        {
                            if(mLikeVideoLists.get(p).isPlaying())
                            {
                                mLikeVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mLikeVideoLists.clear();
                }
            }

            holder.photo.setOnClickListener(null);
            if(mFeedAdapterArray.get(position).getType().compareTo("image")==0)
            {
                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.GONE);
                holder.vcon.setVisibility(View.GONE);

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }

                holder.photo.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {

                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }

                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(mCtx);
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                            {

                                mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                mFeedAdapterArray.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                {
                                    mLikeCount++;
                                }
                                else
                                {
                                    if(mLikeCount!=0)
                                    {
                                        mApplication.sendLikeCountV2(mCtx,mLikePos,mLikeCount,mGodHand);
                                    }

                                    mLikePos = mFeedAdapterArray.get(position).getPostID();
                                    mLikeCount = 1;
                                }
//                                ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            }
                            else
                            {
                                if(mFeedAdapterArray.get(position).getLiked()!=1)
                                {
                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }
            else
            {
                if(mSelf)
                {
                    MyNowPos = position;
                    mMyVideoLists.add(holder.video);
                }
                else
                {
                    LikeNowPos = position;
                    mLikeVideoLists.add(holder.video);
                }

                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.VISIBLE);
                holder.vcon.setVisibility(View.VISIBLE);

                holder.video.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.video.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }

                final ImageView mPhotoV = holder.photo;
                holder.video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mPhotoV.setVisibility(View.GONE);
                    }
                });

                final FastVideoView mVideoV = holder.video;
                holder.video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mVideoV.start();
                    }
                });

                if(mVideoPlayState)
                {
                    holder.video.setVideoPath(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getVideo()));
                    holder.video.start();
                }

                holder.video.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {

                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }

                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(mCtx);
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                            {

                                mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                mFeedAdapterArray.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                {
                                    mLikeCount++;
                                }
                                else
                                {
                                    if(mLikeCount!=0)
                                    {
                                        mApplication.sendLikeCountV2(mCtx,mLikePos,mLikeCount,mGodHand);
                                    }

                                    mLikePos = mFeedAdapterArray.get(position).getPostID();
                                    mLikeCount = 1;
                                }
//                                ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            }
                            else
                            {
                                if(mFeedAdapterArray.get(position).getLiked()!=1)
                                {
                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }

            holder.dio.linkify(new FeedTagActionHandler()
            {
                @Override
                public void handleHashtag(String hashtag)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    Intent intent = new Intent();
                    intent.setClass(mCtx, TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention)
                {
                    mBigProgress.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, UserModel user)
                        {
                            mBigProgress.setVisibility(View.GONE);
                            if (success && user != null)
                            {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                intent.putExtra("guest", mGuest);
                                startActivity(intent);
//                                mCtx.finish();
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email)
                {

                }

                @Override
                public void handleUrl(String url)
                {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                          showToast(getString(R.string.open_uri_error));
                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            if(mSelf)
            {
                if(position>=getCount()-5)
                {
                    LoadData(false);
                }
            }
            else
            {
                if(position>=getCount()-5)
                {
                    LoadDataLike(false);
                }
            }


            return convertView;
        }
    }

    private void shareTo(String subject, String body, String chooserTitle) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    private class ListViewHolder
    {
        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
    }

    private class LikeListAdapter extends BaseAdapter
    {
        private ArrayList<FeedModel> mFeedAdapterArray = new ArrayList<FeedModel>();
        private Boolean mSelf;

        public LikeListAdapter(ArrayList<FeedModel> models,Boolean self)
        {
            mFeedAdapterArray.clear();
            mFeedAdapterArray.addAll(models);

            mSelf = self;
        }

        @Override
        public int getCount()
        {
            return mFeedAdapterArray.size();
        }

        public void addModel(ArrayList<FeedModel> models)
        {
            mFeedAdapterArray.clear();
            mFeedAdapterArray.addAll(models);
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ListViewHolderLike holder = new ListViewHolderLike();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ListViewHolderLike) convertView.getTag();
            }

            try{
                if(mFeedAdapterArray!=null && mFeedAdapterArray.get(position)!=null && mFeedAdapterArray.get(position).getUserInfo()!=null && holder.verifie!=null){
                    if(mFeedAdapterArray.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
                    else holder.verifie.setVisibility(View.GONE);
                }
            }
            catch (Exception e){
            }

            holder.down_layout.setVisibility(View.VISIBLE);
            holder.live.setVisibility(View.GONE);

            holder.name.setText(mFeedAdapterArray.get(position).getUserInfo().getOpenID());
            holder.day.setText(Singleton.getElapsedTimeString(mFeedAdapterArray.get(position).getTimestamp()));

            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.money_text.setVisibility(View.VISIBLE);

                String c = Singleton.getCurrencyType();
                double m = (double) mFeedAdapterArray.get(position).getTotalRevenue()*(double)Singleton.getCurrencyRate();
                DecimalFormat df=new DecimalFormat("#.####");
                holder.money_text.setText(df.format(m) + " " + c);
                holder.view_text.setText(String.format(getString(R.string.home_views), String.valueOf(mFeedAdapterArray.get(position).getViewCount())));
                holder.view_text.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.money_text.setVisibility(View.GONE);
                holder.view_text.setVisibility(View.GONE);
            }

            if(mFeedAdapterArray.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mFeedAdapterArray.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.GONE);
            holder.like_text.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
            holder.comment_text.setText(String.format(getString(R.string.home_comment), String.valueOf(mFeedAdapterArray.get(position).getCommentCount())));

            final String thisUserId = mFeedAdapterArray.get(position).getUserID();

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.like_text.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                        intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                        startActivity(intent);
                    }
                });
            }
            else
            {
                holder.like_text.setOnClickListener(null);
            }

            holder.comment_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    mCimmentPos = position;
//                    mCommentCount = true;

                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    Intent intent = new Intent();
                    intent.setClass(mCtx, HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                    intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(mFeedAdapterArray.get(position).getLiked()==0)
            {
                holder.btn_like.setImageResource(R.drawable.btn_like_selector);
            }
            else holder.btn_like.setImageResource(R.drawable.like_down);

            final TextView likeText = holder.like_text;
            final ImageView like = holder.btn_like;
            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                        intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                        startActivity(intent);
                    }
                });
                holder.btn_like.setOnTouchListener(null);
            }
            else
            {
                holder.btn_like.setOnClickListener(null);
                holder.btn_like.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            mY = event.getY();
                            mX = event.getX();
                        }

                        if (event.getAction() == MotionEvent.ACTION_UP) {

                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }

                            double num = Math.sqrt(Math.pow(((double) mX - (double) event.getX()), 2) + Math.pow(((double) mY - (double) event.getY()), 2));

                            if (num < 30) {
                                int run = (int) (Math.random() * 10000);
                                int img;
                                int mW, mH;

                                if (run < 500) {
                                    img = logos[(int) (Math.random() * logos.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 1000) {
                                    img = rabbits[(int) (Math.random() * rabbits.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 2000) {
                                    img = cats[(int) (Math.random() * cats.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 3000) {
                                    img = bears[(int) (Math.random() * bears.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 4000) {
                                    img = bubbles[(int) (Math.random() * bubbles.length)];
                                    mW = 120;
                                    mH = 120;
                                } else {
                                    img = loves[(int) (Math.random() * loves.length)];
                                    mW = 60;
                                    mH = 60;
                                }

                                int mS = -30 + ((int) (Math.random() * 60));
                                int mTx = -100 + ((int) (Math.random() * 200));
                                int mTy = 500 + ((int) (Math.random() * 1200));

                                ImageView cat = new ImageView(mCtx);
                                cat.setImageResource(img);
                                cat.setVisibility(View.INVISIBLE);

                                RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                cat.setLayoutParams(mParams);

                                AnimationSet mAnimationSet = new AnimationSet(true);

                                ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                scale.setDuration(500);

                                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                alpha.setDuration(3000);

                                RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                rotate.setDuration(750);

                                TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                translate.setDuration(2000);

                                mAnimationSet.addAnimation(scale);
                                mAnimationSet.addAnimation(rotate);
                                mAnimationSet.addAnimation(translate);
                                mAnimationSet.addAnimation(alpha);
                                cat.startAnimation(mAnimationSet);

                                mBear.addView(cat);

                                mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                                if (mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {

                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                    {
                                        mLikeCount++;
                                    }
                                    else
                                    {
                                        if(mLikeCount!=0)
                                        {
                                            mApplication.sendLikeCountV2(mCtx,mLikePos,mLikeCount,mGodHand);
                                        }

                                        mLikePos = mFeedAdapterArray.get(position).getPostID();
                                        mLikeCount = 1;
                                    }
//                                    ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                        @Override
//                                        public void onResult(boolean success, String message) {
//                                            if (success) {
//
//                                            }
//                                        }
//                                    });
                                } else {
                                    if (mFeedAdapterArray.get(position).getLiked() != 1) {
                                        mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                        likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                        mFeedAdapterArray.get(position).setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        if(!mApplication.getIsSbtools())
                                        {
                                            ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }

                        return true;
                    }
                });
            }

            holder.btn_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mCimmentPos = position;
//                    mCommentCount = true;

                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    Intent intent = new Intent();
                    intent.setClass(mCtx, HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedAdapterArray.get(position).getPostID());
                    intent.putExtra("user_id", mFeedAdapterArray.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(thisUserId.compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_more.setVisibility(View.GONE);
            }
            else
            {
                holder.btn_more.setVisibility(View.VISIBLE);
                holder.btn_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mGuest)
                        {
                            showGuestLogin();

                            return;
                        }

                        if (mListMoreText != null) mListMoreText = null;

                        if (mFeedAdapterArray.get(position).getLiked() == 0) {
                            mListMoreText = new String[2];
                            mListMoreText[0] = getString(R.string.post_block);
                            mListMoreText[1] = getString(R.string.post_share);
                        } else {
                            mListMoreText = new String[3];
                            mListMoreText[0] = getString(R.string.post_block);
                            mListMoreText[1] = getString(R.string.post_share);
                            mListMoreText[2] = getString(R.string.back_like);
                        }

                        new AlertDialog.Builder(mCtx).setTitle(getString(R.string.more)).setItems(mListMoreText, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    //檢舉
                                    ArrayList<String> strArray = new ArrayList<String>();
                                    strArray = Singleton.getLocalizeStringOfArray(Constants.REPORT_POST_OPTION);
                                    String[] reportString = strArray.toArray(new String[strArray.size()]);

                                    new AlertDialog.Builder(mCtx).setTitle(getString(R.string.report_reason)).setItems(reportString, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            String reason = Singleton.getLocalizedString(Constants.REPORT_POST_OPTION.get(which));

                                            try {
                                                LogEventUtil.ReportPost(mCtx, mApplication);
                                            }
                                            catch (Exception x)
                                            {

                                            }

                                            ApiManager.reportPostAction(mCtx, mFeedAdapterArray.get(position).getUserInfo().getUserID(), reason, mFeedAdapterArray.get(position).getPostID(), new ApiManager.ReportPostActionCallback() {

                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {
                                                        try{
//                          showToast(getString(R.string.complete));
                                                            Toast.makeText(mCtx, getString(R.string.complete), Toast.LENGTH_SHORT).show();
                                                        }
                                                        catch (Exception x){
                                                        }
                                                    } else {
                                                        try{
//                          showToast(getString(R.string.failed));
                                                            Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                        }
                                                        catch (Exception x){
                                                        }
                                                    }
                                                }
                                            });


                                        }
                                    }).show();
                                } else if (which == 1) {
                                    if (mFeedAdapterArray.get(position).getType().compareTo("image") == 0) {
                                        mBigProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mFeedAdapterArray.get(position).getPostID(), R.string.share_post_image, mFeedAdapterArray.get(position).getUserInfo().getOpenID(), mFeedAdapterArray.get(position).getCaption(), mFeedAdapterArray.get(position).getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mBigProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_image), mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedAdapterArray.get(position).getCaption() + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedAdapterArray.get(position).getPicture());
                                            }
                                        });
                                    } else {
                                        mBigProgress.setVisibility(View.VISIBLE);
                                        Branch.getInstance(mCtx).getContentUrl("facebook", BranchSharePostData(mFeedAdapterArray.get(position).getPostID(), R.string.share_post_video, mFeedAdapterArray.get(position).getUserInfo().getOpenID(), mFeedAdapterArray.get(position).getCaption(), mFeedAdapterArray.get(position).getPicture()), new Branch.BranchLinkCreateListener() {
                                            @Override
                                            public void onLinkCreate(String url, BranchError error) {
                                                mBigProgress.setVisibility(View.GONE);
                                                String mTitle = String.format(getString(R.string.share_post_video), mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                                                String mDescription = mTitle + "!" + mFeedAdapterArray.get(position).getCaption() + "  " + url;

                                                DownloadTask mDownloadTask = new DownloadTask();
                                                mDownloadTask.execute(mTitle, mDescription, getString(R.string.share_title), mFeedAdapterArray.get(position).getPicture());
                                            }
                                        });
                                    }
                                } else if (which == 2) {
                                    if (mLikeCount != 0) {
                                        if (mApplication != null) {
                                            if (mFeedAdapterArray.get(position).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0) {
                                                mApplication.sendLikeCountV2(mCtx, mFeedAdapterArray.get(position).getPostID(), mLikeCount, mGodHand);

                                                mLikeCount = 0;
                                            }
                                        }
                                    }

                                    mFeedAdapterArray.get(position).setLiked(0);
                                    like.setImageResource(R.drawable.btn_like_selector);
                                    ApiManager.unlikePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.UnlikePostCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                            }
                        })
                        .show();
                    }
                });
            }

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedAdapterArray.get(position).getUserInfo().getPicture()), holder.self, SelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFeedAdapterArray.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedAdapterArray.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedAdapterArray.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedAdapterArray.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedAdapterArray.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedAdapterArray.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedAdapterArray.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedAdapterArray.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedAdapterArray.get(position).getUserInfo().getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFeedAdapterArray.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedAdapterArray.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedAdapterArray.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedAdapterArray.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedAdapterArray.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedAdapterArray.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedAdapterArray.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedAdapterArray.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedAdapterArray.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedAdapterArray.get(position).getUserInfo().getWebsite());
                        intent.putExtra("guest", mGuest);
                        startActivity(intent);
//                        mCtx.finish();
                    }
                }
            });

            if(mSelf)
            {
                if(MyNowPos >= (position+2) || MyNowPos <= (position-2))
                {
                    for(int p = 0 ; p < mMyVideoLists.size() ; p++)
                    {
                        if(mMyVideoLists.get(p)!=null)
                        {
                            if(mMyVideoLists.get(p).isPlaying())
                            {
                                mMyVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mMyVideoLists.clear();
                }
            }
            else
            {
                if(LikeNowPos >= (position+2) || LikeNowPos <= (position-2))
                {
                    for(int p = 0 ; p < mLikeVideoLists.size() ; p++)
                    {
                        if(mLikeVideoLists.get(p)!=null)
                        {
                            if(mLikeVideoLists.get(p).isPlaying())
                            {
                                mLikeVideoLists.get(p).stopPlayback();
                            }
                        }
                    }

                    mLikeVideoLists.clear();
                }
            }

            holder.photo.setOnClickListener(null);
            if(mFeedAdapterArray.get(position).getType().compareTo("image")==0)
            {
                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.GONE);
                holder.vcon.setVisibility(View.GONE);

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }

                holder.photo.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {

                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }

                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(mCtx);
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                            {

                                mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                mFeedAdapterArray.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                {
                                    mLikeCount++;
                                }
                                else
                                {
                                    if(mLikeCount!=0)
                                    {
                                        mApplication.sendLikeCountV2(mCtx,mLikePos,mLikeCount,mGodHand);
                                    }

                                    mLikePos = mFeedAdapterArray.get(position).getPostID();
                                    mLikeCount = 1;
                                }
//                                ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            }
                            else
                            {
                                if(mFeedAdapterArray.get(position).getLiked()!=1)
                                {
                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }
            else
            {
                if(mSelf)
                {
                    MyNowPos = position;
                    mMyVideoLists.add(holder.video);
                }
                else
                {
                    LikeNowPos = position;
                    mLikeVideoLists.add(holder.video);
                }

                holder.photo.setVisibility(View.VISIBLE);
                holder.video.setVisibility(View.VISIBLE);
                holder.vcon.setVisibility(View.VISIBLE);

                holder.video.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.video.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
                holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

                try
                {
                    ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getPicture()), holder.photo,BigOptions);
                }
                catch (OutOfMemoryError e)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }
                catch(Exception f)
                {
                    holder.photo.setImageResource(R.drawable.placehold_l);
                }

                final ImageView mPhotoV = holder.photo;
                holder.video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mPhotoV.setVisibility(View.GONE);
                    }
                });

                final FastVideoView mVideoV = holder.video;
                holder.video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mVideoV.start();
                    }
                });

                if(mVideoPlayState)
                {
                    holder.video.setVideoPath(Singleton.getS3FileUrl(mFeedAdapterArray.get(position).getVideo()));
                    holder.video.start();
                }

                holder.video.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (event.getAction() == MotionEvent.ACTION_UP) {

                            if(mGuest)
                            {
                                showGuestLogin();

                                return true;
                            }

                            int run = (int) (Math.random() * 10000);
                            int img;
                            int mW, mH;

                            if (run < 500) {
                                img = logos[(int) (Math.random() * logos.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 1000) {
                                img = rabbits[(int) (Math.random() * rabbits.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run <2000) {
                                img = cats[(int) (Math.random() * cats.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 3000) {
                                img = bears[(int) (Math.random() * bears.length)];
                                mW = 80;
                                mH = 80;
                            } else if (run < 4000) {
                                img = bubbles[(int) (Math.random() * bubbles.length)];
                                mW = 120;
                                mH = 120;
                            } else {
                                img = loves[(int) (Math.random() * loves.length)];
                                mW = 60;
                                mH = 60;
                            }

                            int mS = -30 + ((int) (Math.random() * 60));
                            int mTx = -100 + ((int) (Math.random() * 200));
                            int mTy = 500 + ((int) (Math.random() * 1200));

                            ImageView cat = new ImageView(mCtx);
                            cat.setImageResource(img);
                            cat.setVisibility(View.INVISIBLE);

                            RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                            mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                            cat.setLayoutParams(mParams);

                            AnimationSet mAnimationSet = new AnimationSet(true);

                            ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            scale.setDuration(500);

                            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                            alpha.setDuration(3000);

                            RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                            rotate.setDuration(750);

                            TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                            translate.setDuration(2000);

                            mAnimationSet.addAnimation(scale);
                            mAnimationSet.addAnimation(rotate);
                            mAnimationSet.addAnimation(translate);
                            mAnimationSet.addAnimation(alpha);
                            cat.startAnimation(mAnimationSet);

                            mBear.addView(cat);

                            mLikePosUser = mFeedAdapterArray.get(position).getUserID();
                            if(mFeedAdapterArray.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                            {

                                mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                mFeedAdapterArray.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(mLikePos.compareTo(mFeedAdapterArray.get(position).getPostID())==0)
                                {
                                    mLikeCount++;
                                }
                                else
                                {
                                    if(mLikeCount!=0)
                                    {
                                        mApplication.sendLikeCountV2(mCtx,mLikePos,mLikeCount,mGodHand);
                                    }

                                    mLikePos = mFeedAdapterArray.get(position).getPostID();
                                    mLikeCount = 1;
                                }
//                                ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                    @Override
//                                    public void onResult(boolean success, String message) {
//                                        if (success) {
//
//                                        }
//                                    }
//                                });
                            }
                            else
                            {
                                if(mFeedAdapterArray.get(position).getLiked()!=1)
                                {
                                    mFeedAdapterArray.get(position).setLikeCount(mFeedAdapterArray.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedAdapterArray.get(position).getLikeCount())));
                                    mFeedAdapterArray.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(!mApplication.getIsSbtools())
                                    {
                                        ApiManager.likePost(mCtx, mFeedAdapterArray.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                            @Override
                                            public void onResult(boolean success, String message) {
                                                if (success) {

                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }

            holder.dio.linkify(new FeedTagActionHandler()
            {
                @Override
                public void handleHashtag(String hashtag)
                {
                    if(mGuest)
                    {
                        showGuestLogin();

                        return;
                    }

                    Intent intent = new Intent();
                    intent.setClass(mCtx, TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention)
                {
                    mBigProgress.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback()
                    {
                        @Override
                        public void onResult(boolean success, String message, UserModel user)
                        {
                            mBigProgress.setVisibility(View.GONE);
                            if (success && user != null)
                            {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                intent.putExtra("guest", mGuest);
                                startActivity(intent);
//                                mCtx.finish();
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email)
                {

                }

                @Override
                public void handleUrl(String url)
                {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                          showToast(getString(R.string.open_uri_error));
                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            if(mSelf)
            {
                if(position>=getCount()-5)
                {
                    LoadData(false);
                }
            }
            else
            {
                if(position>=getCount()-5)
                {
                    LoadDataLike(false);
                }
            }


            return convertView;
        }
    }

    private class ListViewHolderLike
    {
        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    public void LoadData(final boolean refresh)
    {
        if(MyisFetchingData)
        {
            return;
        }

        if(refresh)
        {
            MynoMoreData = false;
        }

        if(MynoMoreData)
        {
            return;
        }

        MyisFetchingData = true;
        ApiManager.getUserPost(mCtx, targetUserID, mFeedModels.get(mFeedModels.size()-1).getTimestamp(), 15, new ApiManager.GetUserPostCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel)
            {
                MyisFetchingData = false;

                if (success && feedModel!=null)
                {
                    if (feedModel.size() != 0)
                    {
                        mFeedModels.addAll(feedModel);

                        if (feedModel.size() < 15)
                        {
                            MynoMoreData = true;
                        }
                        if(mMyPagte==0)
                        {
                            if(mGridAdapter!=null)
                            {
                                mGridAdapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            if(mListAdapter!=null)
                            {
                                mListAdapter.addModel(mFeedModels);
                                mListAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                } else {
                    try {
//                          showToast(getString(R.string.failed));
                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    } catch (Exception x) {
                    }
                }
            }
        });
    }

    public void LoadDataLike(final boolean refresh)
    {
        if(LikeisFetchingData)
        {
            return;
        }

        if(refresh)
        {
            LikenoMoreData = false;
        }

        if(LikenoMoreData)
        {
            return;
        }

        LikeisFetchingData = true;

        ApiManager.getLikedPost(mCtx, targetUserID, mLikMoedels.get(mLikMoedels.size() - 1).getTimestamp(), 15, new ApiManager.GetLikedPostsCallback() {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModel) {
                LikeisFetchingData = false;

                if (success && feedModel != null) {
                    if (feedModel.size() != 0) {
                        mLikMoedels.addAll(feedModel);

                        if (feedModel.size() < 15) {
                            LikenoMoreData = true;
                        }

                        if (mLikeListAdapter != null) {
                            //[Zack]resolved IndexOutOfBoundsException
                            mLikeListAdapter = new LikeListAdapter(mLikMoedels, false);
                            mScroll.setAdapter(mLikeListAdapter);
                        }
                    }
                } else {
                    try {
//                          showToast(getString(R.string.failed));
                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    } catch (Exception x) {
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if(mTimer!=null)
        {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }

        try {
            if(mTencent!=null) mTencent.logout(mCtx);
        }
        catch (Exception e) {
        }
    }

    private JSONObject BranchSharePostData(String PostID, int PorV, String OpenID, String caption, String Picture)
    {
        JSONObject mShareData = new JSONObject();

        //Umeng Monitor
        String Umeng_id="SharePost";
        HashMap<String,String> mHashMap = new HashMap<String,String>();
        mHashMap.put(Umeng_id, Umeng_id);
        MobclickAgent.onEventValue(mCtx,Umeng_id,mHashMap,0);

        try
        {
            mShareData.put("page", "p");
            mShareData.put("ID", PostID);
            mShareData.put("$og_title", String.format(getString(PorV), OpenID));
            mShareData.put("$og_description", caption);
            mShareData.put("$og_image_url", Singleton.getS3FileUrl(Picture));
            mShareData.put("$og_type", "website");
            mShareData.put("$fallback_url", Constants.MEDIA17_WEBSITE_SHARE + "p/" + PostID);
        }
        catch (JSONException ex)
        {
        }

        return mShareData;
    }

    private JSONObject BranchShareUserData(String OpenID, String caption, String Picture)
    {
        JSONObject mShareData = new JSONObject();
        try
        {
            mShareData.put("page", "u");
            mShareData.put("ID", OpenID);
            mShareData.put("$og_title", String.format(getString(R.string.share_post_user), OpenID));
            mShareData.put("$og_description", caption);
            mShareData.put("$og_image_url", Singleton.getS3FileUrl(Picture));
            mShareData.put("$og_type", "website");
            mShareData.put("$fallback_url", Constants.MEDIA17_WEBSITE_SHARE + OpenID);
        }
        catch (JSONException ex)
        {
        }

        return mShareData;
    }

    private class DownloadTask extends AsyncTask<String, Integer, Boolean>
    {
        private String subject = "";
        private String body = "";
        private String chooserTitle = "";
        private String uri = "";

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            mBigProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... text)
        {
            subject = text[0];
            body = text[1];
            chooserTitle = text[2];
            uri = text[3];

            try
            {
                URL url = new URL(Singleton.getS3FileUrl(uri));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/."+Singleton.applicationContext.getPackageName()+"/" + "17app.jpg";
                FileOutputStream outStream = new FileOutputStream(file);
                BitmapFactory.decodeStream(input).compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.close();
                return true;
            }
            catch (OutOfMemoryError o)
            {
                return false;
            }
            catch (IOException e)
            {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);
            mBigProgress.setVisibility(View.GONE);
            if(result) shareTo(subject,body,chooserTitle,uri);
            else {
                try {
//                          showToast(getString(R.string.error_failed));
                    Toast.makeText(mCtx, getString(R.string.error_failed), Toast.LENGTH_SHORT).show();
                } catch (Exception x) {
                }
            }
        }
    }

    private void shareTo(String subject, String body, String chooserTitle, String pic)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
//        String imagePath = Environment.getExternalStorageDirectory()+"/."+Singleton.applicationContext.getPackageName()+"/"+"17app.jpg";
//        File imageFileToShare = new File(imagePath);
//        Uri uri = Uri.fromFile(imageFileToShare);
//        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(sharingIntent, chooserTitle));
    }

    Dialog GuestDialog;
    private SharedPreferences sharedPreferences;
    private CallbackManager callbackManager;
    private void showGuestLogin()
    {
        if(GuestDialog!=null) GuestDialog = null;

        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        GuestDialog.setContentView(R.layout.guest_login_dialog);
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        Button mSingup = (Button)GuestDialog.findViewById(R.id.singup);
        mSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();
                dialog_singup_view();
            }
        });

        Button mLogin = (Button)GuestDialog.findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();
                Intent intent = new Intent();
                intent.setClass(mCtx,LoginActivity_V3.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.btnFBLogin);
        mFB.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        GuestDialog.show();
    }

    private void dialog_singup_view()
    {
        if(GuestDialog!=null) GuestDialog = null;
        GuestDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        if(Constants.INTERNATIONAL_VERSION){
            GuestDialog.setContentView(R.layout.guest_singup_dialog_google);
        }
        else{
            GuestDialog.setContentView(R.layout.guest_singup_dialog_china);
        }
        Window window = GuestDialog.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout mWeibo = (LinearLayout)GuestDialog.findViewById(R.id.weibo_layout);
        mWeibo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupWeibo";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                showWebio();
            }
        });

        LinearLayout mQQ = (LinearLayout)GuestDialog.findViewById(R.id.qq_layout);
        mQQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupQQ";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                mTencent = Tencent.createInstance(Constants.QQAppid, mCtx);
                mTencent.login(mCtx, "all", loginListener);
            }
        });

        LinearLayout mWechat = (LinearLayout)GuestDialog.findViewById(R.id.wechat_layout);
        mWechat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                if (ShareUtil.isInstalled(mCtx, "com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI"))
                {
                //Umeng monitor
                String Umeng_id="GuestModeSignupWechat";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                IWXAPI api;
                api = WXAPIFactory.createWXAPI(mCtx, Constants.WECHAT_APP_KEY, true);
                api.registerApp(Constants.WECHAT_APP_KEY);
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "wechat_sdk_demo";
                api.sendReq(req);
                }
                else {
                    Toast.makeText(mCtx, "您还没有安装微信！", Toast.LENGTH_SHORT).show();
                }
            }
        });

        LinearLayout mFB = (LinearLayout)GuestDialog.findViewById(R.id.fb_layout);
        mFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                Intent intent;

                //Umeng monitor
                String Umeng_id_FB="FBlogin";
                HashMap<String,String> mHashMap_FB = new HashMap<String,String>();
                mHashMap_FB.put(Umeng_id_FB, Umeng_id_FB);
                MobclickAgent.onEventValue(mCtx, Umeng_id_FB, mHashMap_FB, 0);

                FacebookSdk.sdkInitialize(mCtx.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().logInWithReadPermissions(mCtx, Arrays.asList("public_profile", "user_friends"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(final LoginResult loginResult) {
                                final String token = loginResult.getAccessToken().getToken();

                                GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject user, GraphResponse response) {
                                        if (user != null) {
                                            try {
                                                showProgressDialog();
                                                final String id = user.optString("id");
                                                final String name = user.optString("name");
//                                                    Log.e("DDD", "ID: " + id);

                                                // Download Profile photo
                                                DownloadFBProfileImage downloadFBProfileImage = new DownloadFBProfileImage(mCtx, id);
                                                downloadFBProfileImage.execute();

                                                ApiManager.checkFacebookIDAvailable(mCtx, id, new ApiManager.checkFacebookIDAvailableCallback() {
                                                    @Override
                                                    public void onResult(boolean success, String message) {
                                                        if (success) {

                                                            // Download user profile image
//                                                                DownloadFBProfileImage download = new DownloadFBProfileImage(mCtx, id);
//                                                                download.execute();

                                                            //Signup
                                                            GraphRequest request = GraphRequest.newMeRequest(
                                                                    loginResult.getAccessToken(),
                                                                    new GraphRequest.GraphJSONObjectCallback() {
                                                                        @Override
                                                                        public void onCompleted(
                                                                                JSONObject object,
                                                                                GraphResponse response) {
                                                                            // Application code
                                                                            String email = object.optString("email");
//                                                                                Log.e("DDD", "EMAIL: " + email);


                                                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, id).commit();
                                                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                            sharedPreferences.edit().putString("signup_type", "fb").commit();
                                                                            sharedPreferences.edit().putString("signup_token", token).commit();

                                                                            if (null != email) {
                                                                                if (email.contains("@")) {
                                                                                    String username = email.substring(0, email.indexOf("@"));
                                                                                    sharedPreferences.edit().putString(Constants.EMAIL, email).commit();
                                                                                    sharedPreferences.edit().putString(Constants.NAME, username).commit();
                                                                                }
                                                                            }
                                                                            hideProgressDialog();
                                                                            startActivity(new Intent(mCtx, SignupActivityV2.class));
//                                                                            mCtx.finish();
                                                                        }
                                                                    });

                                                            Bundle parameters = new Bundle();
                                                            parameters.putString("fields", "email");
                                                            request.setParameters(parameters);
                                                            request.executeAsync();

                                                        } else {
                                                            //login directly
                                                            ApiManager.loginAction2(mCtx, "", "", id, new ApiManager.LoginAction2Callback() {
                                                                @Override
                                                                public void onResult(boolean success, String message, UserModel user) {
                                                                    hideProgressDialog();

                                                                    if (success) {
                                                                        if (message.equals("ok")) {

                                                                            Story17Application mStory17Application = (Story17Application) mCtx.getApplication();
                                                                            mStory17Application.setUser(user);

                                                                            Intent intent = new Intent();
                                                                            intent.setClass(mCtx, MenuActivity.class);
                                                                            startActivity(intent);
                                                                            mCtx.finish();
                                                                        } else if (message.equals("freezed"))
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                        else
                                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                    } else {
                                                                        showNetworkUnstableToast();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });


                                            } catch (Exception e) {
                                                hideProgressDialog();
                                                try {
//                              showToast(getString(R.string.failed));
                                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                                } catch (Exception x) {
                                                }
                                            }
                                        }
                                    }
                                }).executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                hideProgressDialog();
                                try {
//                              showToast(getString(R.string.failed));
                                    Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                                } catch (Exception x) {
                                }
                            }
                        });
            }
        });

        LinearLayout mMessage = (LinearLayout)GuestDialog.findViewById(R.id.messag_layout);
        mMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestDialog.dismiss();

                //Umeng monitor
                String Umeng_id="GuestModeSignupSMS";
                HashMap<String,String> mHashMap = new HashMap<String,String>();
                mHashMap.put(Umeng_id, Umeng_id);
                MobclickAgent.onEventValue(mCtx, Umeng_id, mHashMap, 0);

                sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                sharedPreferences.edit().putString("signup_type", "message").commit();
                sharedPreferences.edit().putString("signup_token", "").commit();
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClass(mCtx, SignupActivityV2.class);
                startActivity(intent);
//                mCtx.finish();
            }
        });

        Button mEnd = (Button)GuestDialog.findViewById(R.id.end);
        mEnd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                GuestDialog.dismiss();

            }
        });

        GuestDialog.show();
    }

    class DownloadFBProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        private final String fbImageDownlaodUrl = "https://graph.facebook.com/";
        private final String attrs = "/picture?width=1080&height=1080";
        String url = "";

        public DownloadFBProfileImage(Context context, String userID) {
            this.context = context;

            this.url = fbImageDownlaodUrl + userID + attrs;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {

        }

    }

    private Dialog mWeiboDialog;
    private void showWebio()
    {
        if(mWeiboDialog!=null) mWeiboDialog = null;
        mWeiboDialog = new Dialog(mCtx,R.style.LivePlayerDialog);
        mWeiboDialog.setContentView(R.layout.instagram_dialog);
        final WebView mWeb = (WebView) mWeiboDialog.findViewById(R.id.web);
        final ProgressBar mProgress = (ProgressBar) mWeiboDialog.findViewById(R.id.progress);
        mWeb.getSettings().setJavaScriptEnabled(true);
        mWeiboDialog.setCancelable(true);
        mWeb.loadUrl("https://api.weibo.com/oauth2/authorize?client_id=" + Constants.WEIBO_APP_KEY + "&response_type=code&redirect_uri=" + Constants.WEIBO_REDIRECT_URL);
        mWeb.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                super.onPageStarted(view, url, favicon);
                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);

                if(url.contains(Constants.IG_REDIRECT_URI) && url.contains("code="))
                {
                    mWeiboDialog.dismiss();
                    showProgressDialog();

                    int pos = url.indexOf("=") + 1;
                    String token = url.substring(pos,url.length());
                    try
                    {
                        OkHttpClient mClient = new OkHttpClient();

                        RequestBody body = new FormEncodingBuilder()
                                .add("client_id", Constants.WEIBO_APP_KEY)
                                .add("client_secret", Constants.WEIBO_SECRET)
                                .add("grant_type", "authorization_code")
                                .add("code", token)
                                .add("redirect_uri", Constants.WEIBO_REDIRECT_URL)
                                .build();

                        Request request = new Request.Builder().url("https://api.weibo.com/oauth2/access_token").post(body).build();
                        mClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                if (response.isSuccessful())
                                {
                                    try
                                    {
                                        JSONObject data = new JSONObject(response.body().string());
                                        final String token = data.getString("access_token");
                                        final String uid = data.getString("uid");

                                        OkHttpClient client = new OkHttpClient();
                                        Request request = new Request.Builder().url("https://api.weibo.com/2/users/show.json?access_token=" + token + "&uid=" + uid).build();
                                        client.newCall(request).enqueue(new Callback()
                                        {
                                            @Override
                                            public void onFailure(Request request, IOException e) {
                                                hideProgressDialog();
                                                showToast(getString(R.string.failed));
                                            }

                                            @Override
                                            public void onResponse(Response response) throws IOException {
                                                if(response.isSuccessful())
                                                {
                                                    try
                                                    {
                                                        JSONObject info = new JSONObject(response.body().string());
                                                        final int id = info.getInt("id");
                                                        final String screen_name = info.getString("screen_name");
                                                        final String pic = info.getString("profile_image_url");

                                                        DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                                        downloadFBProfileImage.execute();

                                                        ApiManager.checkWeiboIDAvailable(mCtx, uid/*String.valueOf(id)*/, new ApiManager.checkFacebookIDAvailableCallback() {
                                                            @Override
                                                            public void onResult(boolean success, String message) {
                                                                if (success) {
                                                                    sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                                                    sharedPreferences.edit().putString(Constants.FACEBOOK_ID, String.valueOf(id)).commit();
                                                                    sharedPreferences.edit().putString(Constants.FULL_NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putString(Constants.NAME, screen_name).commit();
                                                                    sharedPreferences.edit().putBoolean("fblogin", true).commit();

                                                                    sharedPreferences.edit().putString("is_weibo", "weibo").commit();

                                                                    sharedPreferences.edit().putString("signup_type", "weibo").commit();
                                                                    sharedPreferences.edit().putString("signup_token", token).commit();

                                                                    hideProgressDialog();
                                                                    Intent intent = new Intent();
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                    intent.setClass(mCtx, SignupActivityV2.class);
                                                                    startActivity(intent);
//                                                                    mCtx.finish();
                                                                } else {
                                                                    ApiManager.loginActionChina(mCtx, "", "", "weiboID", uid, new ApiManager.LoginAction2Callback() {
                                                                        @Override
                                                                        public void onResult(boolean success, String message, UserModel user) {
                                                                            hideProgressDialog();

                                                                            if (success) {
                                                                                if (message.equals("ok")) {

                                                                                    mApplication.setUser(user);

                                                                                    Intent intent = new Intent();
                                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                                    intent.setClass(mCtx, MenuActivity.class);
                                                                                    startActivity(intent);
                                                                                    mCtx.finish();
                                                                                } else if (message.equals("freezed"))
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                                                else
                                                                                    showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                                            } else {
                                                                                showNetworkUnstableToast();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                    catch (JSONException e)
                                                    {
                                                        hideProgressDialog();
                                                        showToast(getString(R.string.failed));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    catch (JSONException e)
                                    {
                                        hideProgressDialog();
                                        showToast(getString(R.string.failed));
                                    }
                                }
                                else
                                {
                                    hideProgressDialog();
                                    showToast(getString(R.string.failed));
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        hideProgressDialog();
                        showToast(getString(R.string.failed));
                    }
                }
                else
                {
//                            mWeiboDialog.dismiss();
//                            hideProgressDialog();
//                            showToast(getString(R.string.failed));
                }
            }
        });
        try{
            mWeb.clearCache(true);
            mWeb.getSettings().setCacheMode(WebView.PERSISTENT_NO_CACHE);
        }
        catch (Exception e){
        }
        mWeiboDialog.show();
    }

    //QQ-------

    public static Tencent mTencent;

    private class BaseUiListener implements IUiListener
    {

        @Override
        public void onComplete(Object response) {
            if (null == response) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            if (null != jsonResponse && jsonResponse.length() == 0) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
                return;
            }

            doComplete((JSONObject)response);
        }

        protected void doComplete(JSONObject values) {

        }

        @Override
        public void onError(UiError e) {
            hideProgressDialog();
            showToast(getString(R.string.failed));
        }

        @Override
        public void onCancel() {
            hideProgressDialog();
        }
    }

    private IUiListener loginListener = new BaseUiListener()
    {
        @Override
        protected void doComplete(JSONObject jsonObject)
        {
            try
            {
                final String token = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_ACCESS_TOKEN);
                String expires = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_EXPIRES_IN);
                final String openId = jsonObject.getString(com.tencent.connect.common.Constants.PARAM_OPEN_ID);

                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);

                if (mTencent != null && mTencent.isSessionValid())
                {
                    IUiListener listener = new IUiListener()
                    {
                        @Override
                        public void onError(UiError e) {
                            hideProgressDialog();
                            showToast(getString(R.string.failed));
                        }

                        @Override
                        public void onComplete(final Object response)
                        {
                            try
                            {
                                JSONObject json = (JSONObject)response;

                                final String name = json.getString("nickname");
                                String pic = json.getString("figureurl_qq_1");

                                DownloadProfileImage downloadFBProfileImage = new DownloadProfileImage(mCtx, pic);
                                downloadFBProfileImage.execute();

                                ApiManager.checkQQIDAvailable(mCtx, openId, new ApiManager.checkFacebookIDAvailableCallback()
                                {
                                    @Override
                                    public void onResult(boolean success, String message)
                                    {
                                        if (success)
                                        {
                                            sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                                            sharedPreferences.edit().putString(Constants.FACEBOOK_ID, openId).commit();
                                            sharedPreferences.edit().putString(Constants.FULL_NAME, name).commit();
                                            sharedPreferences.edit().putString(Constants.NAME, name).commit();
                                            sharedPreferences.edit().putBoolean("fblogin", true).commit();
                                            sharedPreferences.edit().putString("signup_type", "qq").commit();
                                            sharedPreferences.edit().putString("signup_token", token).commit();
                                            hideProgressDialog();
                                            Intent intent = new Intent();
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setClass(mCtx, SignupActivityV2.class);
                                            startActivity(intent);
//                                            mCtx.finish();
                                        }
                                        else
                                        {
                                            ApiManager.loginActionChina(mCtx, "", "", "qqID", openId, new ApiManager.LoginAction2Callback() {
                                                @Override
                                                public void onResult(boolean success, String message, UserModel user) {
                                                    hideProgressDialog();

                                                    if (success) {
                                                        if (message.equals("ok")) {

                                                            mApplication.setUser(user);

                                                            Intent intent = new Intent();
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            intent.setClass(mCtx, MenuActivity.class);
                                                            startActivity(intent);
                                                            mCtx.finish();
                                                        } else if (message.equals("freezed"))
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_noopen));
                                                        else
                                                            showAlertDialog(getString(R.string.prompt), getString(R.string.login_error));
                                                    } else {
                                                        showNetworkUnstableToast();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            catch (JSONException e)
                            {
                                hideProgressDialog();
                                showToast(getString(R.string.failed));
                            }
                        }

                        @Override
                        public void onCancel() {
                            hideProgressDialog();
                        }
                    };

                    UserInfo mInfo = new UserInfo(mCtx, mTencent.getQQToken());
                    mInfo.getUserInfo(listener);
                }
            }
            catch(Exception e) {
                hideProgressDialog();
                showToast(getString(R.string.failed));
            }
        }
    };

    class DownloadProfileImage extends AsyncTask<Void, Void, String> {

        Context context;
        String url = "";

        public DownloadProfileImage(Context context, String userID) {
            this.context = context;

            this.url = userID.substring(0,userID.length()-2) + 640;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                if(!url.equals("")) {
//                    java.net.URL url = new java.net.URL(this.url);
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setDoInput(true);
//                    connection.connect();

                    OkHttpClient mOkHttpClient = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(this.url)
                            .build();

                    Response response = mOkHttpClient.newCall(request).execute();

                    if (!response.isSuccessful()) return null;

                    InputStream input = response.body().byteStream();
                    Bitmap FB_Profile_IMG = BitmapFactory.decodeStream(input);

                    try {

                        final String pictureFileName = Singleton.getUUIDFileName("jpg");

                        String filePath = Singleton.getExternalMediaFolderPath() + pictureFileName;
                        FileOutputStream outStream = new FileOutputStream(filePath);
                        FB_Profile_IMG.compress(Bitmap.CompressFormat.JPEG, 95, outStream);

                        sharedPreferences = getSharedPreferences(SignupActivityV2.tag, 0);
                        sharedPreferences.edit().putString(Constants.PICTURE, pictureFileName).commit();
                        Singleton.preferenceEditor.putString(Constants.PICTURE, pictureFileName).commit();

                        outStream.close();
                        FB_Profile_IMG.recycle();
                    }
                    catch(Exception e){}
                }
            } catch (Exception e) {}

            return null;
        }

        protected void onPostExecute(String result) {
//            if (result.equals("done")) {
//
//
//            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == com.tencent.connect.common.Constants.REQUEST_LOGIN || requestCode == com.tencent.connect.common.Constants.REQUEST_APPBAR)
        {
            Tencent.onActivityResultData(requestCode, resultCode, data, loginListener);
        }

        try {
            if(callbackManager!=null) callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        catch (Exception e) {
        }
    }
}
