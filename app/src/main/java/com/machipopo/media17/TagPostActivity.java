package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.View.TouchImage;
import com.machipopo.media17.model.FeedModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by POPO on 6/25/15.
 */
public class TagPostActivity extends BaseNewActivity
{
    private TagPostActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;

    private String tag = "";

    private ImageView mGrid,mList;
    private GridView mGridLayout;
    private ListView mListLayout;
    private ProgressBar mProgress;
    private ImageView mNodata;

    private ArrayList<FeedModel> mFeedModels = new ArrayList<FeedModel>();

    private GridAdapter mGridAdapter;

    private RelativeLayout mBear;

    private DisplayImageOptions BigOptions, SelfOptions,GorpOptions;

    private Boolean isFetchingData = false;
    private Boolean noMoreData = false;

    private int mGridHeight;
    private DisplayMetrics mDisplayMetrics;

    private int loves[] = {R.drawable.colorheart_1,R.drawable.colorheart_2,R.drawable.colorheart_3,R.drawable.colorheart_4,R.drawable.colorheart_5,R.drawable.colorheart_6,R.drawable.colorheart_7,R.drawable.colorheart_8,
            R.drawable.colorheart_9,R.drawable.colorheart_10,R.drawable.colorheart_11,R.drawable.colorheart_12,R.drawable.colorheart_13,R.drawable.colorheart_14,R.drawable.colorheart_15,R.drawable.colorheart_16,
            R.drawable.colorheart_17,R.drawable.colorheart_18,R.drawable.colorheart_19,R.drawable.colorheart_20,R.drawable.colorheart_21,R.drawable.colorheart_22,R.drawable.colorheart_23,R.drawable.colorheart_24,
            R.drawable.colorheart_25,R.drawable.colorheart_26,R.drawable.colorheart_27,R.drawable.colorheart_28};

    private int bears[] = {R.drawable.bear_1,R.drawable.bear_2,R.drawable.bear_3,R.drawable.bear_4,R.drawable.bear_5,R.drawable.bear_6,R.drawable.bear_7,R.drawable.bear_8};

    private int bubbles[] = {R.drawable.bubble_1,R.drawable.bubble_2,R.drawable.bubble_3,R.drawable.bubble_4,R.drawable.bubble_5,R.drawable.bubble_6,R.drawable.bubble_7,R.drawable.bubble_8,R.drawable.bubble_9,R.drawable.bubble_10};

    private int cats[] = {R.drawable.cat_1,R.drawable.cat_2,R.drawable.cat_3,R.drawable.cat_4,R.drawable.cat_5,R.drawable.cat_6,R.drawable.cat_7,R.drawable.cat_8};

    private int rabbits[] = {R.drawable.rabbit_1,R.drawable.rabbit_2,R.drawable.rabbit_3,R.drawable.rabbit_4,R.drawable.rabbit_5,R.drawable.rabbit_6,R.drawable.rabbit_7,R.drawable.rabbit_8};

    private int logos[] = {R.drawable.logo_1,R.drawable.logo_2,R.drawable.logo_3,R.drawable.logo_4,R.drawable.logo_5,R.drawable.logo_6,R.drawable.logo_7,R.drawable.logo_8};

    private float mY = 0 ,mX = 0;

    private listAdapter mListAdapter;

    private int mLikeCount = 0;
    private int mLikePos = 0;
    private Timer mTimer;
    private int lastCount = 0;
    private Boolean mGodHand = false;

    public void onResume()
    {
        super.onResume();
        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tag_post_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        Bundle mBundle = getIntent().getExtras();
        if (mBundle != null)
        {
            if (mBundle.containsKey("tag")) tag = mBundle.getString("tag");
        }

        initTitleBar();

        mGridHeight = (int)((float)mDisplayMetrics.widthPixels/(float)3);

        mGrid = (ImageView) findViewById(R.id.grid);
        mList = (ImageView) findViewById(R.id.list);

        mGridLayout = (GridView) findViewById(R.id.grid_layout);
        mListLayout = (ListView) findViewById(R.id.list_layout);

        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNodata = (ImageView) findViewById(R.id.nodata);

        mBear = (RelativeLayout) findViewById(R.id.bear);

        mGrid.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mGrid.setImageResource(R.drawable.profile_grid_active);
                mList.setImageResource(R.drawable.profile_list);

                mGridLayout.setVisibility(View.VISIBLE);
                mListLayout.setVisibility(View.GONE);
            }
        });

        mList.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mGrid.setImageResource(R.drawable.profile_grid);
                mList.setImageResource(R.drawable.profile_list_active);

                mGridLayout.setVisibility(View.GONE);
                mListLayout.setVisibility(View.VISIBLE);

                if(mListAdapter==null)
                {
                    mListAdapter = new listAdapter();
                    mListLayout.setAdapter(mListAdapter);
                }
            }
        });

        mProgress.setVisibility(View.VISIBLE);
        ApiManager.getPostByHashTag(mCtx, mApplication.getUser().getUserID(),tag, Integer.MAX_VALUE, 27, new ApiManager.GetPostByHashTagCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModels)
            {
                mProgress.setVisibility(View.GONE);

                if (success && feedModels!=null)
                {
                    if(feedModels.size()!=0)
                    {
                        mNodata.setVisibility(View.GONE);
                        mFeedModels.clear();
                        mFeedModels.addAll(feedModels);

                        mGridAdapter = new GridAdapter();
                        mGridLayout.setAdapter(mGridAdapter);

                        if (mFeedModels.size() < 27)
                        {
                            noMoreData = true;
                        }
                    }
                    else
                    {
                        mNodata.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    mNodata.setVisibility(View.VISIBLE);
                    try{
//                          showToast(getString(R.string.failed));
                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mLikeCount - lastCount > 17) {
                    mGodHand = true;
                }

                lastCount = mLikeCount;
            }
        }, 0, 1000);

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_profile_s)
        .showImageForEmptyUri(R.drawable.placehold_profile_s)
        .showImageOnFail(R.drawable.placehold_profile_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        GorpOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_s)
        .showImageForEmptyUri(R.drawable.placehold_s)
        .showImageOnFail(R.drawable.placehold_s)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }



    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.search_tag));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if(mBear!=null) mBear.removeAllViews();

        if(mLikeCount!=0)
        {
            if(mFeedModels!=null && mApplication!=null)
            {
                if(mFeedModels.size() > mLikePos)
                {
                    if(mFeedModels.get(mLikePos).getUserInfo().getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, "")) != 0)
                    {
                        mApplication.sendLikeCountV2(mCtx,mFeedModels.get(mLikePos).getPostID(),mLikeCount,mGodHand);

                        mLikePos = 0;
                        mLikeCount = 0;
                    }
                }
            }
        }

        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    private class GridAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderGrid holder = new ViewHolderGrid();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.grid_row, null);
                holder.image = (ImageView) convertView.findViewById(R.id.image);
                holder.video = (ImageView) convertView.findViewById(R.id.video);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderGrid) convertView.getTag();

            holder.image.getLayoutParams().width = mGridHeight;
            holder.image.getLayoutParams().height = mGridHeight;
            //Constants.THUMBNAIL_PREFIX +
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX +mFeedModels.get(position).getPicture()), holder.image,GorpOptions);
            holder.image.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, PhotoActivity.class);
                    intent.putExtra("photo", mFeedModels.get(position).getPicture());
                    intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                    intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                    intent.putExtra("day", Singleton.getElapsedTimeString(mFeedModels.get(position).getTimestamp()));
                    intent.putExtra("view", mFeedModels.get(position).getViewCount());
                    intent.putExtra("money", mFeedModels.get(position).getTotalRevenue());
                    intent.putExtra("dio", mFeedModels.get(position).getCaption());
                    intent.putExtra("likecount", mFeedModels.get(position).getLikeCount());
                    intent.putExtra("commentcount", mFeedModels.get(position).getCommentCount());
                    intent.putExtra("likeed", mFeedModels.get(position).getLiked());

                    intent.putExtra("postid", mFeedModels.get(position).getPostID());
                    intent.putExtra("userid", mFeedModels.get(position).getUserID());

                    intent.putExtra("name", mFeedModels.get(position).getUserInfo().getName());
                    intent.putExtra("isFollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                    intent.putExtra("postCount", mFeedModels.get(position).getUserInfo().getPostCount());
                    intent.putExtra("followerCount", mFeedModels.get(position).getUserInfo().getFollowerCount());
                    intent.putExtra("followingCount", mFeedModels.get(position).getUserInfo().getFollowingCount());
                    intent.putExtra("goto", true);

                    startActivity(intent);
                }
            });

            if(mFeedModels.get(position).getType().compareTo("image")==0) holder.video.setVisibility(View.GONE);
            else holder.video.setVisibility(View.VISIBLE);

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    }

    private class ViewHolderGrid
    {
        ImageView image;
        ImageView video;
    }

    public void LoadData(final boolean refresh)
    {
        if(isFetchingData)
        {
            return;
        }

        if(refresh)
        {
            noMoreData = false;
        }

        if(noMoreData)
        {
            return;
        }

        isFetchingData = true;

        ApiManager.getPostByHashTag(mCtx, mApplication.getUser().getUserID(),tag, mFeedModels.get(mFeedModels.size()-1).getTimestamp(), 27, new ApiManager.GetPostByHashTagCallback()
        {
            @Override
            public void onResult(boolean success, String message, ArrayList<FeedModel> feedModels)
            {
                isFetchingData = false;

                if (success && feedModels!=null)
                {
                    if(feedModels.size() != 0)
                    {
                        mFeedModels.addAll(feedModels);

                        if (mFeedModels.size() < 27)
                        {
                            noMoreData = true;
                        }

                        if(mGridLayout.isShown())
                        {
                            if(mGridAdapter!=null)
                            {
                                mGridAdapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            if(mListAdapter!=null)
                            {
                                mListAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
                else
                {
                    try{
//                          showToast(getString(R.string.failed));
                        Toast.makeText(mCtx, getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception x){
                    }
                }
            }
        });
    }

    private class listAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mFeedModels.size();
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.name.setText(mFeedModels.get(position).getUserInfo().getName());
            holder.day.setText(Singleton.getElapsedTimeString(mFeedModels.get(position).getTimestamp()));

            if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.money_text.setVisibility(View.VISIBLE);

                String c = Singleton.getCurrencyType();
                double m = (double) mFeedModels.get(position).getTotalRevenue()*(double)Singleton.getCurrencyRate();
                DecimalFormat df=new DecimalFormat("#.####");
                holder.money_text.setText(df.format(m) + " " + c);
                holder.view_text.setText(String.format(getString(R.string.home_views), String.valueOf(mFeedModels.get(position).getViewCount())));
                holder.view_text.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.money_text.setVisibility(View.GONE);
                holder.view_text.setVisibility(View.GONE);
            }

            if(mFeedModels.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mFeedModels.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.GONE);

            holder.like_text.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getLikeCount())));
            holder.comment_text.setText(String.format(getString(R.string.home_comment), String.valueOf(mFeedModels.get(position).getCommentCount())));

            if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.like_text.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedModels.get(position).getPostID());
                        intent.putExtra("user_id", mFeedModels.get(position).getUserID());
                        startActivity(intent);
                    }
                });
            }

            holder.comment_text.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedModels.get(position).getPostID());
                    intent.putExtra("user_id", mFeedModels.get(position).getUserID());
                    startActivity(intent);
                }
            });

            if(mFeedModels.get(position).getLiked()==0)
            {
                holder.btn_like.setImageResource(R.drawable.btn_like_selector);
            }
            else holder.btn_like.setImageResource(R.drawable.like_down);

            final TextView likeText = holder.like_text;
            final ImageView like = holder.btn_like;
//            final int mylike = mFeedModels.get(position).getLikeCount();

            if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))==0)
            {
                holder.btn_like.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, PostLikerActivity.class);
                        intent.putExtra("post_id", mFeedModels.get(position).getPostID());
                        intent.putExtra("user_id", mFeedModels.get(position).getUserID());
                        startActivity(intent);
                    }
                });
                holder.btn_like.setOnTouchListener(null);
            }
            else
            {
                holder.btn_like.setOnClickListener(null);
                holder.btn_like.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {
                        if (event.getAction() == MotionEvent.ACTION_DOWN)
                        {
                            mY = event.getY();
                            mX = event.getX();
                        }

                        if (event.getAction() == MotionEvent.ACTION_UP)
                        {
                            double num = Math.sqrt(Math.pow(((double)mX - (double)event.getX()), 2) + Math.pow(((double)mY - (double)event.getY()), 2));

                            if(num<30)
                            {
                                int run = (int) (Math.random() * 10000);
                                int img;
                                int mW, mH;

                                if (run < 500) {
                                    img = logos[(int) (Math.random() * logos.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 1000) {
                                    img = rabbits[(int) (Math.random() * rabbits.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run <2000) {
                                    img = cats[(int) (Math.random() * cats.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 3000) {
                                    img = bears[(int) (Math.random() * bears.length)];
                                    mW = 80;
                                    mH = 80;
                                } else if (run < 4000) {
                                    img = bubbles[(int) (Math.random() * bubbles.length)];
                                    mW = 120;
                                    mH = 120;
                                } else {
                                    img = loves[(int) (Math.random() * loves.length)];
                                    mW = 60;
                                    mH = 60;
                                }

                                int mS = -30 + ((int) (Math.random() * 60));
                                int mTx = -100 + ((int) (Math.random() * 200));
                                int mTy = 500 + ((int) (Math.random() * 1200));

                                ImageView cat = new ImageView(mCtx);
                                cat.setImageResource(img);
                                cat.setVisibility(View.INVISIBLE);

                                RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                                mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                                cat.setLayoutParams(mParams);

                                AnimationSet mAnimationSet = new AnimationSet(true);

                                ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                scale.setDuration(500);

                                AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                                alpha.setDuration(3000);

                                RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                                rotate.setDuration(750);

                                TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                                translate.setDuration(2000);

                                mAnimationSet.addAnimation(scale);
                                mAnimationSet.addAnimation(rotate);
                                mAnimationSet.addAnimation(translate);
                                mAnimationSet.addAnimation(alpha);
                                cat.startAnimation(mAnimationSet);

                                mBear.addView(cat);

                                if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                                {

                                    mFeedModels.get(position).setLikeCount(mFeedModels.get(position).getLikeCount() + 1);
                                    likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getLikeCount())));
                                    mFeedModels.get(position).setLiked(1);
                                    like.setImageResource(R.drawable.like_down);

                                    if(mLikePos==position)
                                    {
                                        mLikeCount++;
                                    }
                                    else
                                    {
                                        if(mLikeCount!=0)
                                        {
                                            mApplication.sendLikeCountV2(mCtx,mFeedModels.get(mLikePos).getPostID(),mLikeCount,mGodHand);
                                        }

                                        mLikePos = position;
                                        mLikeCount = 1;
                                    }
//                                    ApiManager.likePost(mCtx, mFeedModels.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                        @Override
//                                        public void onResult(boolean success, String message) {
//                                            if (success) {
//
//                                            }
//                                        }
//                                    });
                                }
                                else
                                {
                                    if(mFeedModels.get(position).getLiked()!=1)
                                    {
                                        mFeedModels.get(position).setLikeCount(mFeedModels.get(position).getLikeCount() + 1);
                                        likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getLikeCount())));
                                        mFeedModels.get(position).setLiked(1);
                                        like.setImageResource(R.drawable.like_down);

                                        if(!mApplication.getIsSbtools())
                                        {
                                            ApiManager.likePost(mCtx, mFeedModels.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                                @Override
                                                public void onResult(boolean success, String message) {
                                                    if (success) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }


                        return true;
                    }
                });
            }

            holder.btn_comment.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, HomeCommentActivity.class);
                    intent.putExtra("post_id", mFeedModels.get(position).getPostID());
                    intent.putExtra("user_id", mFeedModels.get(position).getUserID());
                    startActivity(intent);
                }
            });

            holder.btn_more.setVisibility(View.GONE);

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mFeedModels.get(position).getUserInfo().getPicture()), holder.self, SelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFeedModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedModels.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mFeedModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mFeedModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mFeedModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mFeedModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mFeedModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mFeedModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mFeedModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mFeedModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mFeedModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mFeedModels.get(position).getUserInfo().getWebsite());
                        startActivity(intent);
                    }
                }
            });

            holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
            holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;
            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mFeedModels.get(position).getPicture()), holder.photo,BigOptions);
            holder.photo.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        int run = (int) (Math.random() * 10000);
                        int img;
                        int mW, mH;

                        if (run < 500) {
                            img = logos[(int) (Math.random() * logos.length)];
                            mW = 80;
                            mH = 80;
                        } else if (run < 1000) {
                            img = rabbits[(int) (Math.random() * rabbits.length)];
                            mW = 80;
                            mH = 80;
                        } else if (run <2000) {
                            img = cats[(int) (Math.random() * cats.length)];
                            mW = 80;
                            mH = 80;
                        } else if (run < 3000) {
                            img = bears[(int) (Math.random() * bears.length)];
                            mW = 80;
                            mH = 80;
                        } else if (run < 4000) {
                            img = bubbles[(int) (Math.random() * bubbles.length)];
                            mW = 120;
                            mH = 120;
                        } else {
                            img = loves[(int) (Math.random() * loves.length)];
                            mW = 60;
                            mH = 60;
                        }

                        int mS = -30 + ((int) (Math.random() * 60));
                        int mTx = -100 + ((int) (Math.random() * 200));
                        int mTy = 500 + ((int) (Math.random() * 1200));

                        ImageView cat = new ImageView(mCtx);
                        cat.setImageResource(img);
                        cat.setVisibility(View.INVISIBLE);

                        RelativeLayout.LayoutParams mParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        mParams.leftMargin = (int) (event.getRawX() - (mW / 2));
                        mParams.topMargin = (int) (event.getRawY() - (mH / 2));

                        cat.setLayoutParams(mParams);

                        AnimationSet mAnimationSet = new AnimationSet(true);

                        ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        scale.setDuration(500);

                        AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
                        alpha.setDuration(3000);

                        RotateAnimation rotate = new RotateAnimation(0.0f, mS);
                        rotate.setDuration(750);

                        TranslateAnimation translate = new TranslateAnimation(0, mTx, 0, -mTy);
                        translate.setDuration(2000);

                        mAnimationSet.addAnimation(scale);
                        mAnimationSet.addAnimation(rotate);
                        mAnimationSet.addAnimation(translate);
                        mAnimationSet.addAnimation(alpha);
                        cat.startAnimation(mAnimationSet);

                        mBear.addView(cat);

                        if(mFeedModels.get(position).getUserID().compareTo(Singleton.preferences.getString(Constants.USER_ID, ""))!=0)
                        {

                            mFeedModels.get(position).setLikeCount(mFeedModels.get(position).getLikeCount() + 1);
                            likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getLikeCount())));
                            mFeedModels.get(position).setLiked(1);
                            like.setImageResource(R.drawable.like_down);

                            if(mLikePos==position)
                            {
                                mLikeCount++;
                            }
                            else
                            {
                                if(mLikeCount!=0)
                                {
                                    mApplication.sendLikeCountV2(mCtx,mFeedModels.get(mLikePos).getPostID(),mLikeCount,mGodHand);
                                }

                                mLikePos = position;
                                mLikeCount = 1;
                            }
//                            ApiManager.likePost(mCtx, mFeedModels.get(position).getPostID(), new ApiManager.LikePostCallback() {
//                                @Override
//                                public void onResult(boolean success, String message) {
//                                    if (success) {
//
//                                    }
//                                }
//                            });
                        }
                        else
                        {
                            if(mFeedModels.get(position).getLiked()!=1)
                            {
                                mFeedModels.get(position).setLikeCount(mFeedModels.get(position).getLikeCount() + 1);
                                likeText.setText(String.format(getString(R.string.home_like), String.valueOf(mFeedModels.get(position).getLikeCount())));
                                mFeedModels.get(position).setLiked(1);
                                like.setImageResource(R.drawable.like_down);

                                if(!mApplication.getIsSbtools())
                                {
                                    ApiManager.likePost(mCtx, mFeedModels.get(position).getPostID(), new ApiManager.LikePostCallback() {
                                        @Override
                                        public void onResult(boolean success, String message) {
                                            if (success) {

                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }


                    return true;
                }
            });

            if(mFeedModels.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.dio.linkify(new FeedTagActionHandler() {
                @Override
                public void handleHashtag(String hashtag) {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention) {
                    mProgress.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message, UserModel user) {
                            mProgress.setVisibility(View.GONE);
                            if (success && user != null) {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                startActivity(intent);
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email) {

                }

                @Override
                public void handleUrl(String url) {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                          showToast(getString(R.string.open_uri_error));
                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            if(position>=getCount()-5)
            {
                LoadData(false);
            }

            return convertView;
        }
    };

    private class ViewHolder
    {
        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        ImageView verifie;
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if(mTimer!=null)
        {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
    }
}
