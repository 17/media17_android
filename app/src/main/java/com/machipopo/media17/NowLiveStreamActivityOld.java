package com.machipopo.media17;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.View.FeedTagTextView;
import com.machipopo.media17.View.TouchImage;
import com.machipopo.media17.model.LiveModel;
import com.machipopo.media17.model.UserModel;
import com.machipopo.media17.utils.FeedTagActionHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;
import com.warnyul.android.widget.FastVideoView;

import java.util.ArrayList;

/**
 * Created by POPO on 7/25/15.
 */
public class NowLiveStreamActivityOld extends BaseNewActivity
{
    private NowLiveStreamActivityOld mCtx = this;
    private LayoutInflater inflater;
    private ArrayList<LiveModel> mLiveModels = new ArrayList<LiveModel>();
    private PullToRefreshListView mList;
    private FriendAdapter mFriendAdapter;
    private DisplayMetrics mDisplayMetrics;
    private DisplayImageOptions BigOptions,SmaleSelfOptions;
    private ImageView mNoData;
    private ProgressBar mProgress;
    private Story17Application mApplication;

    private String mUserId = "";

    public void onPause()
    {
        super.onPause();
        MobclickAgent.onPageEnd(mCtx.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.now_livestream_activity_old);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        Bundle mBundle = getIntent().getExtras();
        if(mBundle!=null)
        {
            if(mBundle.containsKey("user_id")) mUserId = mBundle.getString("user_id");
        }

        mApplication = (Story17Application) getApplication();
        inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        initTitleBar();

        mList = (PullToRefreshListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mProgress.setVisibility(View.VISIBLE);
        mList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
        {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView)
            {
                ApiManager.getHotLiveStreams(mCtx, mUserId, new ApiManager.GetHotLiveStreamsCallback()
                {
                    @Override
                    public void onResult(boolean success,  String message, ArrayList<LiveModel> liveModels)
                    {
                        mList.onRefreshComplete();
                        if (success && liveModels != null)
                        {
                            if (liveModels.size() != 0)
                            {
                                mNoData.setVisibility(View.GONE);
                                mLiveModels.clear();
                                mLiveModels.addAll(liveModels);

                                if(mFriendAdapter!=null) mFriendAdapter = null;
                                mFriendAdapter = new FriendAdapter();
                                mList.setAdapter(mFriendAdapter);
                            }
                            else
                            {
                                mLiveModels.clear();
                                if(mFriendAdapter!=null) mFriendAdapter = null;
                                mFriendAdapter = new FriendAdapter();
                                mList.setAdapter(mFriendAdapter);
                                mNoData.setVisibility(View.VISIBLE);
                            }
                        }
                        else
                        {
                            mLiveModels.clear();
                            if(mFriendAdapter!=null) mFriendAdapter = null;
                            mFriendAdapter = new FriendAdapter();
                            mList.setAdapter(mFriendAdapter);
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });

        ApiManager.getHotLiveStreams(mCtx, mUserId, new ApiManager.GetHotLiveStreamsCallback()
        {
            @Override
            public void onResult(boolean success,  String message, ArrayList<LiveModel> liveModels)
            {
                mProgress.setVisibility(View.GONE);
                if (success && liveModels != null)
                {
                    if (liveModels.size() != 0)
                    {
                        mLiveModels.clear();
                        mLiveModels.addAll(liveModels);

                        mFriendAdapter = new FriendAdapter();
                        mList.setAdapter(mFriendAdapter);
                    }
                    else
                    {
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    mNoData.setVisibility(View.VISIBLE);
                }
            }
        });

        BigOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_l)
        .showImageForEmptyUri(R.drawable.placehold_l)
        .showImageOnFail(R.drawable.placehold_l)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();

        SmaleSelfOptions = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.placehold_c)
        .showImageForEmptyUri(R.drawable.placehold_c)
        .showImageOnFail(R.drawable.placehold_c)
        .cacheInMemory(Constants.PHOTO_CACHE)
        .cacheOnDisk(Constants.PHOTO_CACHE)
        .bitmapConfig(Bitmap.Config.RGB_565)
        .build();
    }

    private void initTitleBar()
    {
        ((RelativeLayout) findViewById(R.id.title_bar)).setBackgroundResource(R.drawable.actionbar_normal);
        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.search_hot_live_title));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class FriendAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mLiveModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = new ViewHolder();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.home_list_row, null);
                holder.self = (ImageView) convertView.findViewById(R.id.self);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.view_text = (TextView) convertView.findViewById(R.id.view_text);
                holder.money_text = (TextView) convertView.findViewById(R.id.money_text);

                holder.photo = (TouchImage) convertView.findViewById(R.id.photo);
                holder.video = (FastVideoView) convertView.findViewById(R.id.video);
                holder.vcon = (ImageView) convertView.findViewById(R.id.vcon);
                holder.live = (ImageView) convertView.findViewById(R.id.live);
                holder.dio = (FeedTagTextView) convertView.findViewById(R.id.dio);
                holder.like_text = (TextView) convertView.findViewById(R.id.like_text);
                holder.comment_text = (TextView) convertView.findViewById(R.id.comment_text);

                holder.btn_like = (ImageView) convertView.findViewById(R.id.btn_like);
                holder.btn_comment = (ImageView) convertView.findViewById(R.id.btn_comment);
                holder.btn_more = (ImageView) convertView.findViewById(R.id.btn_more);

                holder.down_layout = (LinearLayout) convertView.findViewById(R.id.down_layout);
                holder.verifie = (ImageView) convertView.findViewById(R.id.verifie);

                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.name.setText(mLiveModels.get(position).getUserInfo().getOpenID());

            try
            {
                holder.day.setText(Singleton.getElapsedTimeString(mLiveModels.get(position).getBeginTime()));
            }
            catch (Exception e)
            {
                holder.day.setText("");
            }

            holder.view_text.setText(mLiveModels.get(position).getLiveViewerCount() + " " + getString(R.string.live_viewing));
            holder.view_text.setVisibility(View.VISIBLE);
            holder.money_text.setVisibility(View.GONE);

            if(mLiveModels.get(position).getCaption().length()!=0)
            {
                holder.dio.setVisibility(View.VISIBLE);
                holder.dio.setText(mLiveModels.get(position).getCaption());
            }
            else holder.dio.setVisibility(View.GONE);

            holder.down_layout.setVisibility(View.GONE);
            holder.live.setVisibility(View.VISIBLE);

            ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(Constants.THUMBNAIL_PREFIX + mLiveModels.get(position).getUserInfo().getPicture()), holder.self, SmaleSelfOptions);
            holder.self.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mLiveModels.get(position).getUserID().compareTo(mUserId)!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mLiveModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mLiveModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mLiveModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mLiveModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mLiveModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mLiveModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mLiveModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mLiveModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mLiveModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("live", true);

                        intent.putExtra("live_id", mLiveModels.get(position).getLiveStreamID());
                        intent.putExtra("live_openid", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("live_caption", mLiveModels.get(position).getCaption());
                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveModels.get(position).getUserInfo().getPicture());
                        startActivity(intent);
                    }
                }
            });

            holder.name.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mLiveModels.get(position).getUserID().compareTo(mUserId)!=0) {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, HomeUserActivity.class);
                        intent.putExtra("title", mLiveModels.get(position).getUserInfo().getName());
                        intent.putExtra("picture", mLiveModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("isfollowing", mLiveModels.get(position).getUserInfo().getIsFollowing());
                        intent.putExtra("post", mLiveModels.get(position).getUserInfo().getPostCount());
                        intent.putExtra("follow", mLiveModels.get(position).getUserInfo().getFollowerCount());
                        intent.putExtra("following", mLiveModels.get(position).getUserInfo().getFollowingCount());
                        intent.putExtra("open", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("bio", mLiveModels.get(position).getUserInfo().getBio());
                        intent.putExtra("targetUserID", mLiveModels.get(position).getUserInfo().getUserID());
                        intent.putExtra("web", mLiveModels.get(position).getUserInfo().getWebsite());
                        intent.putExtra("live", true);

                        intent.putExtra("live_id", mLiveModels.get(position).getLiveStreamID());
                        intent.putExtra("live_openid", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("live_caption", mLiveModels.get(position).getCaption());
                        intent.putExtra("live_picture", Constants.THUMBNAIL_PREFIX + mLiveModels.get(position).getUserInfo().getPicture());
                        startActivity(intent);
                    }
                }
            });

            holder.photo.setVisibility(View.VISIBLE);
            holder.video.setVisibility(View.GONE);
            holder.vcon.setVisibility(View.GONE);

            holder.photo.getLayoutParams().width = mDisplayMetrics.widthPixels;
            holder.photo.getLayoutParams().height = mDisplayMetrics.widthPixels ;

            try
            {
                if(Singleton.preferences.getInt(Constants.IS_ADMIN_V2, 0)==1) ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mLiveModels.get(position).getCoverPhoto()), holder.photo,BigOptions);
                else ImageLoader.getInstance().displayImage(Singleton.getS3FileUrl(mLiveModels.get(position).getUserInfo().getPicture()), holder.photo,BigOptions);
            }
            catch (OutOfMemoryError e)
            {
                System.gc();
                holder.photo.setImageResource(R.drawable.placehold_s);
            }
            catch(Exception f)
            {
                System.gc();
                holder.photo.setImageResource(R.drawable.placehold_s);
            }

            holder.photo.setOnTouchListener(null);
            holder.photo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(mLiveModels.size()!=0 && mLiveModels.size() > position)
                    {
                        Intent intent = new Intent();
                        intent.setClass(mCtx, LiveStreamActivity.class);
                        intent.putExtra("liveStreamID", mLiveModels.get(position).getLiveStreamID());
                        intent.putExtra("name", mLiveModels.get(position).getUserInfo().getOpenID());
                        intent.putExtra("caption", mLiveModels.get(position).getCaption());
                        intent.putExtra("picture", Constants.THUMBNAIL_PREFIX + mLiveModels.get(position).getUserInfo().getPicture());
                        intent.putExtra("user", mUserId);
                        intent.putExtra("myopen", Singleton.preferences.getString(Constants.OPEN_ID, ""));
                        startActivity(intent);
                    }
                }
            });

            if(mLiveModels.get(position).getUserInfo().getIsVerified()==1) holder.verifie.setVisibility(View.VISIBLE);
            else holder.verifie.setVisibility(View.GONE);

            holder.dio.linkify(new FeedTagActionHandler() {
                @Override
                public void handleHashtag(String hashtag) {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, TagPostActivity.class);
                    intent.putExtra("tag", hashtag);
                    startActivity(intent);
                }

                @Override
                public void handleMention(String mention) {
                    mProgress.setVisibility(View.VISIBLE);
                    ApiManager.getUserInfo(mCtx, mention, new ApiManager.GetUserInfoCallback() {
                        @Override
                        public void onResult(boolean success, String message, UserModel user) {
                            mProgress.setVisibility(View.GONE);
                            if (success && user != null) {
                                Intent intent = new Intent();
                                intent.setClass(mCtx, HomeUserActivity.class);
                                intent.putExtra("title", user.getName());
                                intent.putExtra("picture", user.getPicture());
                                intent.putExtra("isfollowing", user.getIsFollowing());
                                intent.putExtra("post", user.getPostCount());
                                intent.putExtra("follow", user.getFollowerCount());
                                intent.putExtra("following", user.getFollowingCount());
                                intent.putExtra("open", user.getOpenID());
                                intent.putExtra("bio", user.getBio());
                                intent.putExtra("targetUserID", user.getUserID());
                                intent.putExtra("web", user.getWebsite());
                                startActivity(intent);
                            }
                        }
                    });
                }

                @Override
                public void handleEmail(String email) {

                }

                @Override
                public void handleUrl(String url) {
                    try
                    {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {
                        try{
//                                                showToast(getString(R.error_failed.open_uri_error));
                            Toast.makeText(mCtx, getString(R.string.open_uri_error), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception x){
                        }
                    }
                }
            });

            removeLine(holder.dio);

            return convertView;
        }
    }

    private class ViewHolder
    {
        ImageView self;
        TextView name;
        TextView day;
        TextView view_text;
        TextView money_text;

        TouchImage photo;
        FastVideoView video;
        ImageView vcon;
        ImageView live;
        FeedTagTextView dio;
        TextView like_text;
        TextView comment_text;
        ImageView verifie;

        ImageView btn_like;
        ImageView btn_comment;
        ImageView btn_more;

        LinearLayout down_layout;
    }

    private void removeLine(FeedTagTextView text)
    {
        Spannable s = (Spannable) text.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanline_none(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        text.setText(s);
    }

    private class URLSpanline_none extends URLSpan {
        public URLSpanline_none(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mApplication.getshowToast())
        {
            try{
//                                                showToast(getString(R.error_failed.liveend));
                Toast.makeText(mCtx, getString(R.string.liveend), Toast.LENGTH_SHORT).show();
            }
            catch (Exception x){
            }
            mApplication.setshowToast(false);
        }

        if(mApplication.getReLoad())
        {
//            mProgress.setVisibility(View.VISIBLE);
            ApiManager.getHotLiveStreams(mCtx, mUserId, new ApiManager.GetHotLiveStreamsCallback() {
                @Override
                public void onResult(boolean success, String message, ArrayList<LiveModel> liveModels) {
                    mProgress.setVisibility(View.GONE);
                    mApplication.setReLoad(false);
                    if (success && liveModels != null) {
                        if (liveModels.size() != 0) {
                            mLiveModels.clear();
                            mLiveModels.addAll(liveModels);

                            if (mFriendAdapter == null) {
                                mFriendAdapter = new FriendAdapter();
                                mList.setAdapter(mFriendAdapter);
                            }
                            else{
                                mFriendAdapter.notifyDataSetChanged();
                            }

                        } else {
                            mLiveModels.clear();
                            if (mFriendAdapter == null) {
                                mFriendAdapter = new FriendAdapter();
                                mList.setAdapter(mFriendAdapter);
                            }
                            else{
                                mFriendAdapter.notifyDataSetChanged();
                            }
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mLiveModels.clear();
                        if (mFriendAdapter == null) {
                            mFriendAdapter = new FriendAdapter();
                            mList.setAdapter(mFriendAdapter);
                        }
                        else{
                            mFriendAdapter.notifyDataSetChanged();
                        }
                        mNoData.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        MobclickAgent.onPageStart(mCtx.getClass().getSimpleName());
    }
}
