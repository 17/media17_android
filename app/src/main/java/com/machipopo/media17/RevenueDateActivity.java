package com.machipopo.media17;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

/**
 * Created by POPO on 6/23/15.
 */
public class RevenueDateActivity extends BaseFragmentActivity
{
    private RevenueDateActivity mCtx = this;
    private Story17Application mApplication;

    private ViewPager mViewPager;
    private TextView mTab1,mTab2,mTab3;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.revenue_date_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(mCtx.getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) getApplication();

        initTitleBar();

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyPagerAdapter(mCtx.getSupportFragmentManager()));

        SmartTabLayout mPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        mPagerTab.setViewPager(mViewPager);
        mPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                switch (position)
                {
                    case 0 :
                        mTab1.setTextColor(getResources().getColor(R.color.main_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
                    case 1 :
                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.main_color));
                        mTab3.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        break;
                    case 2 :
                        mTab1.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab2.setTextColor(getResources().getColor(R.color.second_main_text_color));
                        mTab3.setTextColor(getResources().getColor(R.color.main_color));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        mTab1 = (TextView) findViewById(R.id.tab1);
        mTab1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mViewPager.setCurrentItem(0);
            }
        });
        mTab2 = (TextView) findViewById(R.id.tab2);
        mTab2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mViewPager.setCurrentItem(1);
            }
        });
        mTab3 = (TextView) findViewById(R.id.tab3);
        mTab3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mViewPager.setCurrentItem(2);
            }
        });
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.revenue_data));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    public class MyPagerAdapter extends FragmentPagerAdapter
    {
        public MyPagerAdapter(FragmentManager fragmentManager)
        {
            super(fragmentManager);
        }

        @Override
        public int getCount()
        {
            return 3;
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
                case 0:
                    return new RevenueDate7DFragment();
                case 1:
                    return new RevenueDate30DFragment();
                case 2:
                    return new RevenueDate12MFragment();
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return "";
        }
    }
}
