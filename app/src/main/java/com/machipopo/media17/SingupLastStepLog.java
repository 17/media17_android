package com.machipopo.media17;

/**
 * Created by POPO_INNOVATION_IMAC on 16/2/18.
 */
public class SingupLastStepLog {

    private static SingupLastStepLog mLog = new SingupLastStepLog();

    public static SingupLastStepLog getInstance(){
      return mLog;
    }
    //event tracking
    public enum SignupLastStep {LoginPage,UserName,Password,PhoneVerification,SMScode};

    private SignupLastStep mSignupLastStep = SignupLastStep.LoginPage;

    public SignupLastStep getmSignupLastStep() {
        return mSignupLastStep;
    }

    public void setmSignupLastStep(SignupLastStep mSignupLastStep) {
        this.mSignupLastStep = mSignupLastStep;
    }

}
