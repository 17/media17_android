package com.machipopo.media17;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.machipopo.media17.model.LiveRegionModel;

import java.util.ArrayList;

/**
 * Created by POPO on 15/10/6.
 */
public class LiveHotCountryActivity extends BaseNewActivity
{
    private LiveHotCountryActivity mCtx = this;
    private Story17Application mApplication;
    private LayoutInflater inflater;

    private PullToRefreshListView mList;
    private ProgressBar mProgress;
    private ImageView mNoData;

    private ArrayList<LiveRegionModel> mLiveRegionModels = new ArrayList<LiveRegionModel>();
    private MyAdapter mMyAdapter;

    private String NowRegion = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_liker_activity);

        try
        {
            if(Build.VERSION.SDK_INT >= 21)
            {
                Window window = mCtx.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.status_bar_color));
            }
        }
        catch (Exception e)
        {
        }

        mApplication = (Story17Application) mCtx.getApplication();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initTitleBar();

        mList = (PullToRefreshListView) findViewById(R.id.list);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNoData = (ImageView) findViewById(R.id.nodata);

        mProgress.setVisibility(View.VISIBLE);

        LiveRegionModel mGloble = new LiveRegionModel();
        mGloble.setRegion(getString(R.string.global));
        mGloble.setLiveCount(0);
        mLiveRegionModels.add(mGloble);

        LiveRegionModel mLocal = new LiveRegionModel();
        mLocal.setRegion(getString(R.string.local));
        mLocal.setLiveCount(0);
        mLiveRegionModels.add(mLocal);

        ApiManager.getLiveStreamHotCountryList(mCtx, new ApiManager.LiveRegionCallback()
        {
            @Override
            public void onResult(boolean success, ArrayList<LiveRegionModel> liveRegionModels)
            {
                mProgress.setVisibility(View.GONE);

                if (success && liveRegionModels != null)
                {
                    if (liveRegionModels.size() != 0)
                    {
                        mLiveRegionModels.addAll(liveRegionModels);

                        for(int i = 0 ; i < mLiveRegionModels.size() ; i++)
                        {
                            if(mLiveRegionModels.get(i).getRegion().compareTo(mApplication.getNowRegion())==0)
                            {
                                NowRegion = mLiveRegionModels.get(i).getRegion();
                                break;
                            }
                        }

                        if(NowRegion.length()==0) NowRegion = getString(R.string.local);

                        mMyAdapter = new MyAdapter();
                        mList.setAdapter(mMyAdapter);
                    }
                }
            }
        });
    }

    private void initTitleBar()
    {
        RelativeLayout mTitleBar = (RelativeLayout) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundResource(R.drawable.actionbar_normal);

        TextView mTitle = (TextView) findViewById(R.id.title_name);
        mTitle.setText(getString(R.string.region));
        mTitle.setTextColor(Color.WHITE);

        ImageView img = (ImageView) findViewById(R.id.img_left);
        img.setImageResource(R.drawable.btn_rrow_selector);
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                mCtx.finish();
            }
        });
    }

    private class MyAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return mLiveRegionModels.size();
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolderGrid holder = new ViewHolderGrid();

            if(convertView==null)
            {
                convertView = inflater.inflate(R.layout.live_region_row, null);
                holder.layout = (LinearLayout) convertView.findViewById(R.id.layout);
                holder.img = (ImageView) convertView.findViewById(R.id.img);
                holder.country = (TextView) convertView.findViewById(R.id.country);
                holder.count = (TextView) convertView.findViewById(R.id.count);
                holder.check = (ImageView) convertView.findViewById(R.id.check);
                convertView.setTag(holder);
            }
            else holder = (ViewHolderGrid) convertView.getTag();

            if(mLiveRegionModels.get(position).getRegion().compareTo(getString(R.string.global))==0 || mLiveRegionModels.get(position).getRegion().compareTo(getString(R.string.local))==0)
            {
                holder.count.setVisibility(View.GONE);
                holder.country.setText(mLiveRegionModels.get(position).getRegion());
            }
            else
            {
                holder.count.setVisibility(View.VISIBLE);
                holder.country.setText(getResources().getString(getResources().getIdentifier(mLiveRegionModels.get(position).getRegion(), "string", getPackageName())));
            }

            if(mLiveRegionModels.get(position).getRegion().compareTo(NowRegion)==0) holder.check.setVisibility(View.VISIBLE);
            else holder.check.setVisibility(View.INVISIBLE);

            holder.count.setText(String.valueOf(mLiveRegionModels.get(position).getLiveCount()));
            holder.layout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    mApplication.setNowRegion(mLiveRegionModels.get(position).getRegion());
                    mApplication.setReLoadRegion(true);
                    NowRegion = mLiveRegionModels.get(position).getRegion();
                    mMyAdapter.notifyDataSetChanged();
                    mCtx.finish();
                }
            });

            return convertView;
        }
    }

    private class ViewHolderGrid
    {
        LinearLayout layout;
        ImageView img;
        TextView country;
        TextView count;
        ImageView check;
    }
}
