package com.machipopo.media17;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ItemRow extends RelativeLayout {

	public ImageView selectionIndicatorImageView;
	public TextView nameTextView;

	public ItemRow(Context context) {
		super(context);

		LayoutInflater.from(context).inflate(R.layout.item_row, this);
		selectionIndicatorImageView = (ImageView) findViewById(R.id.selectionIndicatorImageView);
		nameTextView = (TextView) findViewById(R.id.nameTextView);
	}

	public ItemRow(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ItemRow(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}
