package com.squareup.okhttp.internal;

/**
 * Created by POPO on 15/9/18.
 */
public final class Version {
    public static String userAgent() {
        return "okhttp/${project.version}";
    }

    private Version() {
    }
}